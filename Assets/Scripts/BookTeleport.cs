using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BookTeleport : MonoBehaviour
{
    // public float delayTime = 5.0f;
    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Debug.Log("Collided with book. Waiting to transport to other scene...");
            SceneManager.LoadScene("SampleScene");
        }
    }
}
