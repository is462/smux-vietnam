﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.String Meta.Conduit.ConduitActionAttribute::get_Intent()
extern void ConduitActionAttribute_get_Intent_m84D1C30D06B22872CEB2FB3E81E8E64407E47E04 (void);
// 0x00000002 System.Void Meta.Conduit.ConduitActionAttribute::set_Intent(System.String)
extern void ConduitActionAttribute_set_Intent_m263CF9EFFE775B902A5FF7B333D11625C5962CFA (void);
// 0x00000003 System.Single Meta.Conduit.ConduitActionAttribute::get_MinConfidence()
extern void ConduitActionAttribute_get_MinConfidence_mB80E03D4881A88E8F5BB25FB621C9AC73897228A (void);
// 0x00000004 System.Void Meta.Conduit.ConduitActionAttribute::set_MinConfidence(System.Single)
extern void ConduitActionAttribute_set_MinConfidence_m04C5C6D94AEE7AFB00EA4B8D794AE5ED126CE136 (void);
// 0x00000005 System.Single Meta.Conduit.ConduitActionAttribute::get_MaxConfidence()
extern void ConduitActionAttribute_get_MaxConfidence_mB20819CD2B5B00EA7893A91326C4ED774FC9CCBC (void);
// 0x00000006 System.Void Meta.Conduit.ConduitActionAttribute::set_MaxConfidence(System.Single)
extern void ConduitActionAttribute_set_MaxConfidence_mB79851401D1D596F9F136D5E3AABC368B45F9CA3 (void);
// 0x00000007 System.Collections.Generic.List`1<System.String> Meta.Conduit.ConduitActionAttribute::get_Aliases()
extern void ConduitActionAttribute_get_Aliases_m7F14239441AE67F6AE68768BE3DCE5D263772DF0 (void);
// 0x00000008 System.Void Meta.Conduit.ConduitActionAttribute::set_Aliases(System.Collections.Generic.List`1<System.String>)
extern void ConduitActionAttribute_set_Aliases_m9937DAF4FEFE0EC7FEFCC7A982229111E9033816 (void);
// 0x00000009 System.Boolean Meta.Conduit.ConduitActionAttribute::get_ValidatePartial()
extern void ConduitActionAttribute_get_ValidatePartial_mEC2E91582DF23F6016B4B14D6560CC501C8BAA6C (void);
// 0x0000000A System.Void Meta.Conduit.ConduitActionAttribute::set_ValidatePartial(System.Boolean)
extern void ConduitActionAttribute_set_ValidatePartial_m33404154DB6082F0BF07F46DE3D431B7F39B3E60 (void);
// 0x0000000B System.Void Meta.Conduit.ConduitActionAttribute::.ctor(System.String,System.String[])
extern void ConduitActionAttribute__ctor_m18E2CF3064E7C73B1A4CB2A02DD1491C25278839 (void);
// 0x0000000C System.Void Meta.Conduit.ConduitActionAttribute::.ctor(System.String,System.Single,System.Single,System.Boolean,System.String[])
extern void ConduitActionAttribute__ctor_m1C16B0DECDE370CD70BE8340276FBAA7D5A8F69D (void);
// 0x0000000D System.Void Meta.Conduit.ConduitAssemblyAttribute::.ctor()
extern void ConduitAssemblyAttribute__ctor_m2F65A7E2DFE83EE810C4AC8D54EDA18D7D4BE87A (void);
// 0x0000000E System.String Meta.Conduit.ConduitEntityAttribute::get_Name()
extern void ConduitEntityAttribute_get_Name_mCB81A6848A69CC424ECA2DA9F628904CDD0A4294 (void);
// 0x0000000F System.String Meta.Conduit.ConduitEntityAttribute::get_ID()
extern void ConduitEntityAttribute_get_ID_mBDFD7ACA389703E54EE0B9D81D0BE984B81EDEE4 (void);
// 0x00000010 System.Void Meta.Conduit.ConduitEntityAttribute::.ctor(System.String,System.String)
extern void ConduitEntityAttribute__ctor_mC9336CC9FD1D2FABC208497F198957FD6C7EBD97 (void);
// 0x00000011 System.Collections.Generic.List`1<System.String> Meta.Conduit.ConduitParameterAttribute::get_Examples()
extern void ConduitParameterAttribute_get_Examples_m5EB712E2D6BBA2B8990A966B7427584F763C19D2 (void);
// 0x00000012 System.Collections.Generic.List`1<System.String> Meta.Conduit.ConduitParameterAttribute::get_Aliases()
extern void ConduitParameterAttribute_get_Aliases_m0042E9D8A133FBD6C659B84727CDAE39613359F3 (void);
// 0x00000013 System.Void Meta.Conduit.ConduitParameterAttribute::.ctor(System.String[])
extern void ConduitParameterAttribute__ctor_mA69D8E435C952AE369B5F7D5C66E673032FE08C0 (void);
// 0x00000014 System.Void Meta.Conduit.ConduitParameterAttribute::.ctor(System.String[],System.String[])
extern void ConduitParameterAttribute__ctor_m8A8BC0AD91ECA3BF11A82F938409DFDFE87F42C1 (void);
// 0x00000015 System.Void Meta.Conduit.ConduitValueAttribute::.ctor(System.String[])
extern void ConduitValueAttribute__ctor_m9A246F124FE27D76D1DB0DDE27E4F861C024B4DE (void);
// 0x00000016 System.String[] Meta.Conduit.ConduitValueAttribute::get_Aliases()
extern void ConduitValueAttribute_get_Aliases_m389CCC0D7D48EDFD3F3C29BCF0CF21FC58470B91 (void);
// 0x00000017 System.Void Meta.Conduit.ConduitDispatcher::.ctor(Meta.Conduit.IManifestLoader,Meta.Conduit.IInstanceResolver,Meta.Conduit.IParameterProvider)
extern void ConduitDispatcher__ctor_m7535A16FA252722E9DF5C79546C77775687B8B61 (void);
// 0x00000018 System.Void Meta.Conduit.ConduitDispatcher::Initialize(System.String)
extern void ConduitDispatcher_Initialize_m4B9361622EE8F1C53293A9FFD4246D2D2B154603 (void);
// 0x00000019 System.Collections.Generic.List`1<Meta.Conduit.InvocationContext> Meta.Conduit.ConduitDispatcher::ResolveInvocationContexts(System.String,System.Single,System.Boolean)
extern void ConduitDispatcher_ResolveInvocationContexts_m333FF48DDEAC51BC7CF69B0EC08F5780D51A0010 (void);
// 0x0000001A System.Boolean Meta.Conduit.ConduitDispatcher::CompatibleInvocationContext(Meta.Conduit.InvocationContext,System.Single,System.Boolean)
extern void ConduitDispatcher_CompatibleInvocationContext_m9FEC3D536DF537CDC1D9750E7F84C4AC3D274BD8 (void);
// 0x0000001B System.Boolean Meta.Conduit.ConduitDispatcher::InvokeAction(System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>,System.Single,System.Boolean)
extern void ConduitDispatcher_InvokeAction_m0D2FE4B170E96E730B1DB2D7E4DFDAE32E27EC7C (void);
// 0x0000001C System.Boolean Meta.Conduit.ConduitDispatcher::InvokeMethod(Meta.Conduit.InvocationContext)
extern void ConduitDispatcher_InvokeMethod_m670291FEA95AFCD8E176658B84096A67C127F445 (void);
// 0x0000001D System.Void Meta.Conduit.ConduitDispatcher/<>c__DisplayClass7_0::.ctor()
extern void U3CU3Ec__DisplayClass7_0__ctor_m6B4D0FADC48ACF2B35BCC052CE3314927F150326 (void);
// 0x0000001E System.Boolean Meta.Conduit.ConduitDispatcher/<>c__DisplayClass7_0::<ResolveInvocationContexts>b__0(Meta.Conduit.InvocationContext)
extern void U3CU3Ec__DisplayClass7_0_U3CResolveInvocationContextsU3Eb__0_mEE92120B09D48F5C5A7CFE1F820A787D71484156 (void);
// 0x0000001F System.Void Meta.Conduit.ConduitDispatcher/<>c__DisplayClass8_0::.ctor()
extern void U3CU3Ec__DisplayClass8_0__ctor_m8DE009C665C7EDA90E8B5B790FB457D3C95A1CDF (void);
// 0x00000020 System.Boolean Meta.Conduit.ConduitDispatcher/<>c__DisplayClass8_0::<CompatibleInvocationContext>b__0(System.Reflection.ParameterInfo)
extern void U3CU3Ec__DisplayClass8_0_U3CCompatibleInvocationContextU3Eb__0_mE92E27C55F82DD2F38787D5B0BE09B72BD19B072 (void);
// 0x00000021 System.Void Meta.Conduit.ConduitDispatcherFactory::.ctor(Meta.Conduit.IInstanceResolver,Meta.Conduit.IParameterProvider)
extern void ConduitDispatcherFactory__ctor_m49499E8211793AC34D65916715C6EBADE345C2E6 (void);
// 0x00000022 Meta.Conduit.IConduitDispatcher Meta.Conduit.ConduitDispatcherFactory::GetDispatcher()
extern void ConduitDispatcherFactory_GetDispatcher_m603B1B328101784064F51DC9D1B306035182CCB3 (void);
// 0x00000023 System.String Meta.Conduit.ConduitUtilities::DelimitWithUnderscores(System.String)
extern void ConduitUtilities_DelimitWithUnderscores_m7947C6CCF6F9D7695E1784C057200667EFE72F5A (void);
// 0x00000024 System.Boolean Meta.Conduit.ConduitUtilities::ContainsIgnoringWhitespace(System.String,System.String)
extern void ConduitUtilities_ContainsIgnoringWhitespace_m0DCD5D8B725964A9337252A894673297E16E7D78 (void);
// 0x00000025 System.String Meta.Conduit.ConduitUtilities::StripWhiteSpace(System.String)
extern void ConduitUtilities_StripWhiteSpace_mD5FEEFC5B9A3084F4B09A6E4FE0211E9675A0EE8 (void);
// 0x00000026 System.String Meta.Conduit.ConduitUtilities::GetEntityEnumName(System.String)
extern void ConduitUtilities_GetEntityEnumName_mAB56F092E2565CCE95074826A66E3479C995D178 (void);
// 0x00000027 System.String Meta.Conduit.ConduitUtilities::GetEntityEnumValue(System.String)
extern void ConduitUtilities_GetEntityEnumValue_mB65B5B0619E75ABA029021A05A4E98F9ADA1A1BD (void);
// 0x00000028 System.String Meta.Conduit.ConduitUtilities::SanitizeName(System.String)
extern void ConduitUtilities_SanitizeName_m711F0200DD27438001CAC6D160CE4B7F4D82911D (void);
// 0x00000029 System.String Meta.Conduit.ConduitUtilities::SanitizeString(System.String)
extern void ConduitUtilities_SanitizeString_m3F1A9EFF4AFDBEEBECE8171FA807BA76A82A8528 (void);
// 0x0000002A System.Void Meta.Conduit.ConduitUtilities::.ctor()
extern void ConduitUtilities__ctor_m82E380C7D102784AAA0322CEEBB811974250EF39 (void);
// 0x0000002B System.Void Meta.Conduit.ConduitUtilities::.cctor()
extern void ConduitUtilities__cctor_m39E10EBB38A31D9E5D21E355A973940C931A75DB (void);
// 0x0000002C System.Void Meta.Conduit.ConduitUtilities/ProgressDelegate::.ctor(System.Object,System.IntPtr)
extern void ProgressDelegate__ctor_mDE85BBB85E3B47FD3F45B8EECBF636CE0CBB4CF5 (void);
// 0x0000002D System.Void Meta.Conduit.ConduitUtilities/ProgressDelegate::Invoke(System.String,System.Single)
extern void ProgressDelegate_Invoke_mD904245282FDA0CF965F8C557112FA942525A8A3 (void);
// 0x0000002E System.IAsyncResult Meta.Conduit.ConduitUtilities/ProgressDelegate::BeginInvoke(System.String,System.Single,System.AsyncCallback,System.Object)
extern void ProgressDelegate_BeginInvoke_m1C23F1EE76D7F9169A64018AFD0CF93E4F311229 (void);
// 0x0000002F System.Void Meta.Conduit.ConduitUtilities/ProgressDelegate::EndInvoke(System.IAsyncResult)
extern void ProgressDelegate_EndInvoke_mB26A27584E0DDE132C34026197900EA3242DCE4C (void);
// 0x00000030 System.Collections.Generic.HashSet`1<T> Meta.Conduit.ListExtensions::ToHashSet(System.Collections.Generic.List`1<T>)
// 0x00000031 Meta.Conduit.Manifest Meta.Conduit.IManifestLoader::LoadManifest(System.String)
// 0x00000032 Meta.Conduit.Manifest Meta.Conduit.IManifestLoader::LoadManifestFromString(System.String)
// 0x00000033 System.Type Meta.Conduit.InvocationContext::get_Type()
extern void InvocationContext_get_Type_m94663BEC59E45CCA072A6F895FF56EE1FEBB793B (void);
// 0x00000034 System.Void Meta.Conduit.InvocationContext::set_Type(System.Type)
extern void InvocationContext_set_Type_m91583D68A2D990F3A048E11204058D1BC98220A2 (void);
// 0x00000035 System.Reflection.MethodInfo Meta.Conduit.InvocationContext::get_MethodInfo()
extern void InvocationContext_get_MethodInfo_m2D90A4508D1FF72455B08A101724A2B400999C83 (void);
// 0x00000036 System.Void Meta.Conduit.InvocationContext::set_MethodInfo(System.Reflection.MethodInfo)
extern void InvocationContext_set_MethodInfo_m6BB1EE81B39EB5123E85B883D08CED3CB227ADCD (void);
// 0x00000037 System.Single Meta.Conduit.InvocationContext::get_MinConfidence()
extern void InvocationContext_get_MinConfidence_m51849B1141BEC4FE9A78A91ECC79C7A887F2FE6F (void);
// 0x00000038 System.Void Meta.Conduit.InvocationContext::set_MinConfidence(System.Single)
extern void InvocationContext_set_MinConfidence_m1A8927D1523786E08BE1CBEC9EE0826968DE6AB7 (void);
// 0x00000039 System.Single Meta.Conduit.InvocationContext::get_MaxConfidence()
extern void InvocationContext_get_MaxConfidence_m744A5CA4356E4636030A1785C0D93D1CF1A52215 (void);
// 0x0000003A System.Void Meta.Conduit.InvocationContext::set_MaxConfidence(System.Single)
extern void InvocationContext_set_MaxConfidence_m1FA275B7FCBDE491E03DAC59BC99C230F1D7F0A0 (void);
// 0x0000003B System.Boolean Meta.Conduit.InvocationContext::get_ValidatePartial()
extern void InvocationContext_get_ValidatePartial_m28E929E67B00C21745F3A352C1C3DBC37191FEF7 (void);
// 0x0000003C System.Void Meta.Conduit.InvocationContext::set_ValidatePartial(System.Boolean)
extern void InvocationContext_set_ValidatePartial_m3C83D984891A67A8E2661C5BB0BA021EE78B7964 (void);
// 0x0000003D System.Void Meta.Conduit.InvocationContext::.ctor()
extern void InvocationContext__ctor_mA76EE5D311EA14BD541DF075484F584A658E7E02 (void);
// 0x0000003E System.Void Meta.Conduit.Manifest::.ctor()
extern void Manifest__ctor_m5EE9B425D6A237BA381885339009A0F2E27E4AF6 (void);
// 0x0000003F System.String Meta.Conduit.Manifest::get_ID()
extern void Manifest_get_ID_m3024465C37DE5BCEE80933529FE6B93A6BDC4D92 (void);
// 0x00000040 System.Void Meta.Conduit.Manifest::set_ID(System.String)
extern void Manifest_set_ID_m93D9E8BFD72D5E1E683D5D2DB420B40F0D3ECE97 (void);
// 0x00000041 System.String Meta.Conduit.Manifest::get_Version()
extern void Manifest_get_Version_m5F47B5A959CD45A5F1CE5BBA9083C5EC84E6F248 (void);
// 0x00000042 System.Void Meta.Conduit.Manifest::set_Version(System.String)
extern void Manifest_set_Version_mE6025C151F8BD6E8150F9BEF057226D552DA4FCE (void);
// 0x00000043 System.String Meta.Conduit.Manifest::get_Domain()
extern void Manifest_get_Domain_mAA24580D53D38C1BE18F1BEB5FB7826E9D04E93C (void);
// 0x00000044 System.Void Meta.Conduit.Manifest::set_Domain(System.String)
extern void Manifest_set_Domain_mFCC8FB48CD1242D930CC2193E3CD4BB1D5ECEF4A (void);
// 0x00000045 System.Collections.Generic.List`1<Meta.Conduit.ManifestEntity> Meta.Conduit.Manifest::get_Entities()
extern void Manifest_get_Entities_mF45F3CF715A5EC40BE253EC1397FC058CD387D65 (void);
// 0x00000046 System.Void Meta.Conduit.Manifest::set_Entities(System.Collections.Generic.List`1<Meta.Conduit.ManifestEntity>)
extern void Manifest_set_Entities_m571B306731A0D5C392EE51652F9491E2D1A15152 (void);
// 0x00000047 System.Collections.Generic.List`1<Meta.Conduit.ManifestAction> Meta.Conduit.Manifest::get_Actions()
extern void Manifest_get_Actions_mD8705A97A545D14BEFA977C6EC90F0135D4F8AC1 (void);
// 0x00000048 System.Void Meta.Conduit.Manifest::set_Actions(System.Collections.Generic.List`1<Meta.Conduit.ManifestAction>)
extern void Manifest_set_Actions_m9598E8D75278EA81E1611096659DFC7CA778F32A (void);
// 0x00000049 System.Boolean Meta.Conduit.Manifest::ResolveActions()
extern void Manifest_ResolveActions_m8F30EE572C6406B267DC6317B54426F2FA110E45 (void);
// 0x0000004A System.Reflection.MethodInfo Meta.Conduit.Manifest::GetBestMethodMatch(System.Type,System.String,System.Type[])
extern void Manifest_GetBestMethodMatch_mD83612F9ED012FBBAB2927DD7025A0E820D88A2A (void);
// 0x0000004B System.Boolean Meta.Conduit.Manifest::ContainsAction(System.String)
extern void Manifest_ContainsAction_mCF03300D0B05617962CC35EDFBC991763B31F050 (void);
// 0x0000004C System.Collections.Generic.List`1<Meta.Conduit.InvocationContext> Meta.Conduit.Manifest::GetInvocationContexts(System.String)
extern void Manifest_GetInvocationContexts_m271750391476AF7F224F56757986724385A4AB73 (void);
// 0x0000004D System.Void Meta.Conduit.Manifest/<>c::.cctor()
extern void U3CU3Ec__cctor_mA8A57E286D93E6D5B2CCA1E4DC3086665B522C9C (void);
// 0x0000004E System.Void Meta.Conduit.Manifest/<>c::.ctor()
extern void U3CU3Ec__ctor_m06DE2518A2516E8CDC24C942D734FB372A4BCAFD (void);
// 0x0000004F System.Boolean Meta.Conduit.Manifest/<>c::<ResolveActions>b__22_0(System.Collections.Generic.List`1<Meta.Conduit.InvocationContext>)
extern void U3CU3Ec_U3CResolveActionsU3Eb__22_0_m6FA2123E99D272484DA6766A69E6178AC2ED23C8 (void);
// 0x00000050 System.Int32 Meta.Conduit.Manifest/<>c::<ResolveActions>b__22_1(Meta.Conduit.InvocationContext,Meta.Conduit.InvocationContext)
extern void U3CU3Ec_U3CResolveActionsU3Eb__22_1_m4F8E437CAEB7D91F5F2C5B577779272A90D569D4 (void);
// 0x00000051 System.Void Meta.Conduit.ManifestAction::.ctor()
extern void ManifestAction__ctor_mBD486DB41CA3BF1669B90E88F63C14EAD8786392 (void);
// 0x00000052 System.String Meta.Conduit.ManifestAction::get_ID()
extern void ManifestAction_get_ID_mA8C0AA2B8E0AD6997D82E9CD729C5ECED0029FE9 (void);
// 0x00000053 System.Void Meta.Conduit.ManifestAction::set_ID(System.String)
extern void ManifestAction_set_ID_m6CADD5DB2F7D43E2F31C683F47B1FC215654520D (void);
// 0x00000054 System.String Meta.Conduit.ManifestAction::get_Assembly()
extern void ManifestAction_get_Assembly_m5C563ECB155985C292593A42FE94B9146EAFDF0C (void);
// 0x00000055 System.Void Meta.Conduit.ManifestAction::set_Assembly(System.String)
extern void ManifestAction_set_Assembly_mB8033D761C97E13B956AE81BD6662FD4F4FD3249 (void);
// 0x00000056 System.String Meta.Conduit.ManifestAction::get_Name()
extern void ManifestAction_get_Name_mA0A17B5F92AD9040023F72D3FF8FA976E7194E33 (void);
// 0x00000057 System.Void Meta.Conduit.ManifestAction::set_Name(System.String)
extern void ManifestAction_set_Name_m7FFF6562E6F6DC655F1DF3593682D4D75968289A (void);
// 0x00000058 System.Collections.Generic.List`1<Meta.Conduit.ManifestParameter> Meta.Conduit.ManifestAction::get_Parameters()
extern void ManifestAction_get_Parameters_m20870BE54DBD68586A57573B431585871B05B7AE (void);
// 0x00000059 System.Void Meta.Conduit.ManifestAction::set_Parameters(System.Collections.Generic.List`1<Meta.Conduit.ManifestParameter>)
extern void ManifestAction_set_Parameters_m47332CA890C1793F21E8215057A26518822C070D (void);
// 0x0000005A System.String Meta.Conduit.ManifestAction::get_DeclaringTypeName()
extern void ManifestAction_get_DeclaringTypeName_m0357AB06F7A0B23268D3D8658F993A32FA8C8A42 (void);
// 0x0000005B System.Collections.Generic.List`1<System.String> Meta.Conduit.ManifestAction::get_Aliases()
extern void ManifestAction_get_Aliases_m09A1420BDCF18F4E0B7DE5E14EE8B94DC682488B (void);
// 0x0000005C System.Void Meta.Conduit.ManifestAction::set_Aliases(System.Collections.Generic.List`1<System.String>)
extern void ManifestAction_set_Aliases_m07C567DA991B205A2F31415A1B6B33816028ADAB (void);
// 0x0000005D System.Boolean Meta.Conduit.ManifestAction::Equals(System.Object)
extern void ManifestAction_Equals_m5125DCAD8F70A3F8E36C69F0C64AF5EF5331F0E9 (void);
// 0x0000005E System.Int32 Meta.Conduit.ManifestAction::GetHashCode()
extern void ManifestAction_GetHashCode_m54BBE3647F5E823125433874258B1EBF87DCAD81 (void);
// 0x0000005F System.Boolean Meta.Conduit.ManifestAction::Equals(Meta.Conduit.ManifestAction)
extern void ManifestAction_Equals_m8099A8FA2624F0E99C5F6DCF85D3043CD760FF3B (void);
// 0x00000060 System.Void Meta.Conduit.ManifestEntity::.ctor()
extern void ManifestEntity__ctor_m66F53E41520C28C673C7E92361DFC32D9E4CE928 (void);
// 0x00000061 System.String Meta.Conduit.ManifestEntity::get_ID()
extern void ManifestEntity_get_ID_m3B9958F8C96A39013CBC45DE69AC6CD8978A44EA (void);
// 0x00000062 System.Void Meta.Conduit.ManifestEntity::set_ID(System.String)
extern void ManifestEntity_set_ID_mD81F45837BBEC591E922D15E64B666D29FC3B934 (void);
// 0x00000063 System.String Meta.Conduit.ManifestEntity::get_Namespace()
extern void ManifestEntity_get_Namespace_m9F470413C55992958A2C08F80963486BA8567172 (void);
// 0x00000064 System.Void Meta.Conduit.ManifestEntity::set_Namespace(System.String)
extern void ManifestEntity_set_Namespace_m53308E6C2BB0F64A7C7CD84371760A2A20754619 (void);
// 0x00000065 System.String Meta.Conduit.ManifestEntity::get_Type()
extern void ManifestEntity_get_Type_mF29EDA2CAB1D4CA52EEEF9698033233BBE42057A (void);
// 0x00000066 System.Void Meta.Conduit.ManifestEntity::set_Type(System.String)
extern void ManifestEntity_set_Type_mC2B60CEDD86AA42AA505EBC5C3C8459AA62D3CFD (void);
// 0x00000067 System.String Meta.Conduit.ManifestEntity::get_Name()
extern void ManifestEntity_get_Name_mB4D88A1CDB8DED1F4E5976249A7B50D81B119B5E (void);
// 0x00000068 System.Void Meta.Conduit.ManifestEntity::set_Name(System.String)
extern void ManifestEntity_set_Name_m70662A558EC71DB2EB79BEAEFDF0E54CE334669A (void);
// 0x00000069 System.Collections.Generic.List`1<Meta.Conduit.WitKeyword> Meta.Conduit.ManifestEntity::get_Values()
extern void ManifestEntity_get_Values_mF1AA90A3B30EAD99ACB8827089305AE2DB2092C7 (void);
// 0x0000006A System.Void Meta.Conduit.ManifestEntity::set_Values(System.Collections.Generic.List`1<Meta.Conduit.WitKeyword>)
extern void ManifestEntity_set_Values_m23816C58376B0ED48271F055462A15E1384461C5 (void);
// 0x0000006B System.String Meta.Conduit.ManifestEntity::get_Assembly()
extern void ManifestEntity_get_Assembly_m2E977627A6E8FE674FB8A354989EAC3F14EAEB06 (void);
// 0x0000006C System.Void Meta.Conduit.ManifestEntity::set_Assembly(System.String)
extern void ManifestEntity_set_Assembly_mD6261D660050EE1365EEA2319609D86294A7CADC (void);
// 0x0000006D Meta.WitAi.Data.Info.WitEntityInfo Meta.Conduit.ManifestEntity::GetAsInfo()
extern void ManifestEntity_GetAsInfo_mE742C19AA49C3BA12B3CC860DB8E1DD7A1FDEB6C (void);
// 0x0000006E System.Boolean Meta.Conduit.ManifestEntity::Equals(System.Object)
extern void ManifestEntity_Equals_mCCFECE96B4AFAE31FDA38E16532D1979C15885E5 (void);
// 0x0000006F System.Int32 Meta.Conduit.ManifestEntity::GetHashCode()
extern void ManifestEntity_GetHashCode_m39C3F2CCD83F369642D89A156406E015CBC057F1 (void);
// 0x00000070 System.Boolean Meta.Conduit.ManifestEntity::Equals(Meta.Conduit.ManifestEntity)
extern void ManifestEntity_Equals_mE390697D4BC7FDEE97F71AD95C032B828D682EB7 (void);
// 0x00000071 Meta.Conduit.Manifest Meta.Conduit.ManifestLoader::LoadManifest(System.String)
extern void ManifestLoader_LoadManifest_m5BA61228731F9D4B47051315A0D3A0F76728857E (void);
// 0x00000072 Meta.Conduit.Manifest Meta.Conduit.ManifestLoader::LoadManifestFromString(System.String)
extern void ManifestLoader_LoadManifestFromString_m5BB338371F64DB1629DE4FB06F4B4931594CD06F (void);
// 0x00000073 System.Void Meta.Conduit.ManifestLoader::.ctor()
extern void ManifestLoader__ctor_mFA981E01FDE27FA33200E2471DE9E5EC9F0C4241 (void);
// 0x00000074 System.Void Meta.Conduit.ManifestParameter::.ctor()
extern void ManifestParameter__ctor_mBCDAE5DD90205CC64CBDC70AE9AC9449B7B47F8B (void);
// 0x00000075 System.String Meta.Conduit.ManifestParameter::get_Name()
extern void ManifestParameter_get_Name_mD4158A3289CED1ACB68454747F3C1EA871ED9AC3 (void);
// 0x00000076 System.Void Meta.Conduit.ManifestParameter::set_Name(System.String)
extern void ManifestParameter_set_Name_mCD30D8B54F62D1C910492F8931A2A2F0022AD5F3 (void);
// 0x00000077 System.String Meta.Conduit.ManifestParameter::get_InternalName()
extern void ManifestParameter_get_InternalName_m6A6ED9811F9520A02CE5D944BC6B50622DB0E7E1 (void);
// 0x00000078 System.Void Meta.Conduit.ManifestParameter::set_InternalName(System.String)
extern void ManifestParameter_set_InternalName_m0E0549A6F5DB2FBD37C320E7EC9E69A506E921C8 (void);
// 0x00000079 System.String Meta.Conduit.ManifestParameter::get_QualifiedName()
extern void ManifestParameter_get_QualifiedName_m69BDD38050AC8A6478C08E6119EC84294764FB1A (void);
// 0x0000007A System.Void Meta.Conduit.ManifestParameter::set_QualifiedName(System.String)
extern void ManifestParameter_set_QualifiedName_m24E6E99E9FD8949B08C954758DA8011C6CB5DFBF (void);
// 0x0000007B System.String Meta.Conduit.ManifestParameter::get_EntityType()
extern void ManifestParameter_get_EntityType_mC1B6C48D44C1B7D526CFDF20E3DB58C557C0DE9D (void);
// 0x0000007C System.String Meta.Conduit.ManifestParameter::get_TypeAssembly()
extern void ManifestParameter_get_TypeAssembly_mB8CCA15F9B2593A3AEF8A242950D4F097E3CFB18 (void);
// 0x0000007D System.Void Meta.Conduit.ManifestParameter::set_TypeAssembly(System.String)
extern void ManifestParameter_set_TypeAssembly_m94F527762251ECACB0C978E0CEA2F7C8513BBA59 (void);
// 0x0000007E System.String Meta.Conduit.ManifestParameter::get_QualifiedTypeName()
extern void ManifestParameter_get_QualifiedTypeName_mB9D2527C2D0820F22F46AE03A82AFC75584B8D64 (void);
// 0x0000007F System.Void Meta.Conduit.ManifestParameter::set_QualifiedTypeName(System.String)
extern void ManifestParameter_set_QualifiedTypeName_m0DF238D95D264D39661907411ABED38E3309E6D9 (void);
// 0x00000080 System.Collections.Generic.List`1<System.String> Meta.Conduit.ManifestParameter::get_Aliases()
extern void ManifestParameter_get_Aliases_mEFC955CEBE01F9F22EE08C426C3CC8CC793B06CB (void);
// 0x00000081 System.Void Meta.Conduit.ManifestParameter::set_Aliases(System.Collections.Generic.List`1<System.String>)
extern void ManifestParameter_set_Aliases_mA25C7F207BEDBB83F06577D88260CB9BC7D37E47 (void);
// 0x00000082 System.Collections.Generic.List`1<System.String> Meta.Conduit.ManifestParameter::get_Examples()
extern void ManifestParameter_get_Examples_m335253557ACF245D37D72EC1E6395D2115A7DA9F (void);
// 0x00000083 System.Void Meta.Conduit.ManifestParameter::set_Examples(System.Collections.Generic.List`1<System.String>)
extern void ManifestParameter_set_Examples_mC3BEE6E90FF521172E06B5252455171327326DD9 (void);
// 0x00000084 System.Boolean Meta.Conduit.ManifestParameter::Equals(System.Object)
extern void ManifestParameter_Equals_m02F081B9B9F331CA19B3A45D4B0A1DEB648A087F (void);
// 0x00000085 System.Int32 Meta.Conduit.ManifestParameter::GetHashCode()
extern void ManifestParameter_GetHashCode_m2F6EC55132F3744937BF5D4C8CF6766380E682D7 (void);
// 0x00000086 System.Boolean Meta.Conduit.ManifestParameter::Equals(Meta.Conduit.ManifestParameter)
extern void ManifestParameter_Equals_mE8A904569240E6322DF3D963402C68F9FFEA6C5E (void);
// 0x00000087 System.Void Meta.Conduit.WitKeyword::.ctor()
extern void WitKeyword__ctor_m13351C55E289FC80144D9FCD977D609B3DFAB4B9 (void);
// 0x00000088 System.Void Meta.Conduit.WitKeyword::.ctor(System.String,System.Collections.Generic.List`1<System.String>)
extern void WitKeyword__ctor_mA24856B88B54C49C4ED1B11D89714F8EF5C643B8 (void);
// 0x00000089 System.Void Meta.Conduit.WitKeyword::.ctor(Meta.WitAi.Data.Info.WitEntityKeywordInfo)
extern void WitKeyword__ctor_mE7A636FB824A679BB9C5CA7C538F80965000EA36 (void);
// 0x0000008A Meta.WitAi.Data.Info.WitEntityKeywordInfo Meta.Conduit.WitKeyword::GetAsInfo()
extern void WitKeyword_GetAsInfo_mD3F1F9FF0538EDD39B731CDF48761C09CAB37894 (void);
// 0x0000008B System.Boolean Meta.Conduit.WitKeyword::Equals(System.Object)
extern void WitKeyword_Equals_mF8DC72966E4C19E90951E59197675AD5F13A2E54 (void);
// 0x0000008C System.Boolean Meta.Conduit.WitKeyword::Equals(Meta.Conduit.WitKeyword)
extern void WitKeyword_Equals_m340843728FA629787497D02B62E6707CE88FA2B3 (void);
// 0x0000008D System.Int32 Meta.Conduit.WitKeyword::GetHashCode()
extern void WitKeyword_GetHashCode_m5D2AFE1A89CC3C25092A6F59554A58452C753BAC (void);
// 0x0000008E System.Void Meta.Conduit.IConduitDispatcher::Initialize(System.String)
// 0x0000008F System.Boolean Meta.Conduit.IConduitDispatcher::InvokeAction(System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>,System.Single,System.Boolean)
// 0x00000090 System.Collections.Generic.IEnumerable`1<System.Object> Meta.Conduit.IInstanceResolver::GetObjectsOfType(System.Type)
// 0x00000091 System.Void Meta.Conduit.IParameterProvider::Populate(System.Collections.Generic.Dictionary`2<System.String,System.Object>,System.Collections.Generic.Dictionary`2<System.String,System.String>)
// 0x00000092 System.Boolean Meta.Conduit.IParameterProvider::ContainsParameter(System.Reflection.ParameterInfo,System.Text.StringBuilder)
// 0x00000093 System.Object Meta.Conduit.IParameterProvider::GetParameterValue(System.Reflection.ParameterInfo)
// 0x00000094 System.Void Meta.Conduit.ParameterProvider::Populate(System.Collections.Generic.Dictionary`2<System.String,System.Object>,System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void ParameterProvider_Populate_m699EFABB776BB2031F2BDA887FA1A50DA9EC7BCD (void);
// 0x00000095 System.Boolean Meta.Conduit.ParameterProvider::ContainsParameter(System.Reflection.ParameterInfo,System.Text.StringBuilder)
extern void ParameterProvider_ContainsParameter_mC31F9ADDCA19471BF378D8F6670D8B32A621F3A8 (void);
// 0x00000096 System.Object Meta.Conduit.ParameterProvider::GetParameterValue(System.Reflection.ParameterInfo)
extern void ParameterProvider_GetParameterValue_m1E7D6ABC6A73D40F509B78A0D14DF453F3CA6442 (void);
// 0x00000097 System.Boolean Meta.Conduit.ParameterProvider::SupportedSpecializedParameter(System.Reflection.ParameterInfo)
extern void ParameterProvider_SupportedSpecializedParameter_m94193795A128972521C9F17F7A21247E799D047E (void);
// 0x00000098 System.Object Meta.Conduit.ParameterProvider::GetSpecializedParameter(System.Reflection.ParameterInfo)
extern void ParameterProvider_GetSpecializedParameter_mC2FF6D746B56338F63B550A00B81336BB0A9529A (void);
// 0x00000099 System.Void Meta.Conduit.ParameterProvider::.ctor()
extern void ParameterProvider__ctor_mF151418D3C103A98058D3E8F7DE16C7A92B614A8 (void);
static Il2CppMethodPointer s_methodPointers[153] = 
{
	ConduitActionAttribute_get_Intent_m84D1C30D06B22872CEB2FB3E81E8E64407E47E04,
	ConduitActionAttribute_set_Intent_m263CF9EFFE775B902A5FF7B333D11625C5962CFA,
	ConduitActionAttribute_get_MinConfidence_mB80E03D4881A88E8F5BB25FB621C9AC73897228A,
	ConduitActionAttribute_set_MinConfidence_m04C5C6D94AEE7AFB00EA4B8D794AE5ED126CE136,
	ConduitActionAttribute_get_MaxConfidence_mB20819CD2B5B00EA7893A91326C4ED774FC9CCBC,
	ConduitActionAttribute_set_MaxConfidence_mB79851401D1D596F9F136D5E3AABC368B45F9CA3,
	ConduitActionAttribute_get_Aliases_m7F14239441AE67F6AE68768BE3DCE5D263772DF0,
	ConduitActionAttribute_set_Aliases_m9937DAF4FEFE0EC7FEFCC7A982229111E9033816,
	ConduitActionAttribute_get_ValidatePartial_mEC2E91582DF23F6016B4B14D6560CC501C8BAA6C,
	ConduitActionAttribute_set_ValidatePartial_m33404154DB6082F0BF07F46DE3D431B7F39B3E60,
	ConduitActionAttribute__ctor_m18E2CF3064E7C73B1A4CB2A02DD1491C25278839,
	ConduitActionAttribute__ctor_m1C16B0DECDE370CD70BE8340276FBAA7D5A8F69D,
	ConduitAssemblyAttribute__ctor_m2F65A7E2DFE83EE810C4AC8D54EDA18D7D4BE87A,
	ConduitEntityAttribute_get_Name_mCB81A6848A69CC424ECA2DA9F628904CDD0A4294,
	ConduitEntityAttribute_get_ID_mBDFD7ACA389703E54EE0B9D81D0BE984B81EDEE4,
	ConduitEntityAttribute__ctor_mC9336CC9FD1D2FABC208497F198957FD6C7EBD97,
	ConduitParameterAttribute_get_Examples_m5EB712E2D6BBA2B8990A966B7427584F763C19D2,
	ConduitParameterAttribute_get_Aliases_m0042E9D8A133FBD6C659B84727CDAE39613359F3,
	ConduitParameterAttribute__ctor_mA69D8E435C952AE369B5F7D5C66E673032FE08C0,
	ConduitParameterAttribute__ctor_m8A8BC0AD91ECA3BF11A82F938409DFDFE87F42C1,
	ConduitValueAttribute__ctor_m9A246F124FE27D76D1DB0DDE27E4F861C024B4DE,
	ConduitValueAttribute_get_Aliases_m389CCC0D7D48EDFD3F3C29BCF0CF21FC58470B91,
	ConduitDispatcher__ctor_m7535A16FA252722E9DF5C79546C77775687B8B61,
	ConduitDispatcher_Initialize_m4B9361622EE8F1C53293A9FFD4246D2D2B154603,
	ConduitDispatcher_ResolveInvocationContexts_m333FF48DDEAC51BC7CF69B0EC08F5780D51A0010,
	ConduitDispatcher_CompatibleInvocationContext_m9FEC3D536DF537CDC1D9750E7F84C4AC3D274BD8,
	ConduitDispatcher_InvokeAction_m0D2FE4B170E96E730B1DB2D7E4DFDAE32E27EC7C,
	ConduitDispatcher_InvokeMethod_m670291FEA95AFCD8E176658B84096A67C127F445,
	U3CU3Ec__DisplayClass7_0__ctor_m6B4D0FADC48ACF2B35BCC052CE3314927F150326,
	U3CU3Ec__DisplayClass7_0_U3CResolveInvocationContextsU3Eb__0_mEE92120B09D48F5C5A7CFE1F820A787D71484156,
	U3CU3Ec__DisplayClass8_0__ctor_m8DE009C665C7EDA90E8B5B790FB457D3C95A1CDF,
	U3CU3Ec__DisplayClass8_0_U3CCompatibleInvocationContextU3Eb__0_mE92E27C55F82DD2F38787D5B0BE09B72BD19B072,
	ConduitDispatcherFactory__ctor_m49499E8211793AC34D65916715C6EBADE345C2E6,
	ConduitDispatcherFactory_GetDispatcher_m603B1B328101784064F51DC9D1B306035182CCB3,
	ConduitUtilities_DelimitWithUnderscores_m7947C6CCF6F9D7695E1784C057200667EFE72F5A,
	ConduitUtilities_ContainsIgnoringWhitespace_m0DCD5D8B725964A9337252A894673297E16E7D78,
	ConduitUtilities_StripWhiteSpace_mD5FEEFC5B9A3084F4B09A6E4FE0211E9675A0EE8,
	ConduitUtilities_GetEntityEnumName_mAB56F092E2565CCE95074826A66E3479C995D178,
	ConduitUtilities_GetEntityEnumValue_mB65B5B0619E75ABA029021A05A4E98F9ADA1A1BD,
	ConduitUtilities_SanitizeName_m711F0200DD27438001CAC6D160CE4B7F4D82911D,
	ConduitUtilities_SanitizeString_m3F1A9EFF4AFDBEEBECE8171FA807BA76A82A8528,
	ConduitUtilities__ctor_m82E380C7D102784AAA0322CEEBB811974250EF39,
	ConduitUtilities__cctor_m39E10EBB38A31D9E5D21E355A973940C931A75DB,
	ProgressDelegate__ctor_mDE85BBB85E3B47FD3F45B8EECBF636CE0CBB4CF5,
	ProgressDelegate_Invoke_mD904245282FDA0CF965F8C557112FA942525A8A3,
	ProgressDelegate_BeginInvoke_m1C23F1EE76D7F9169A64018AFD0CF93E4F311229,
	ProgressDelegate_EndInvoke_mB26A27584E0DDE132C34026197900EA3242DCE4C,
	NULL,
	NULL,
	NULL,
	InvocationContext_get_Type_m94663BEC59E45CCA072A6F895FF56EE1FEBB793B,
	InvocationContext_set_Type_m91583D68A2D990F3A048E11204058D1BC98220A2,
	InvocationContext_get_MethodInfo_m2D90A4508D1FF72455B08A101724A2B400999C83,
	InvocationContext_set_MethodInfo_m6BB1EE81B39EB5123E85B883D08CED3CB227ADCD,
	InvocationContext_get_MinConfidence_m51849B1141BEC4FE9A78A91ECC79C7A887F2FE6F,
	InvocationContext_set_MinConfidence_m1A8927D1523786E08BE1CBEC9EE0826968DE6AB7,
	InvocationContext_get_MaxConfidence_m744A5CA4356E4636030A1785C0D93D1CF1A52215,
	InvocationContext_set_MaxConfidence_m1FA275B7FCBDE491E03DAC59BC99C230F1D7F0A0,
	InvocationContext_get_ValidatePartial_m28E929E67B00C21745F3A352C1C3DBC37191FEF7,
	InvocationContext_set_ValidatePartial_m3C83D984891A67A8E2661C5BB0BA021EE78B7964,
	InvocationContext__ctor_mA76EE5D311EA14BD541DF075484F584A658E7E02,
	Manifest__ctor_m5EE9B425D6A237BA381885339009A0F2E27E4AF6,
	Manifest_get_ID_m3024465C37DE5BCEE80933529FE6B93A6BDC4D92,
	Manifest_set_ID_m93D9E8BFD72D5E1E683D5D2DB420B40F0D3ECE97,
	Manifest_get_Version_m5F47B5A959CD45A5F1CE5BBA9083C5EC84E6F248,
	Manifest_set_Version_mE6025C151F8BD6E8150F9BEF057226D552DA4FCE,
	Manifest_get_Domain_mAA24580D53D38C1BE18F1BEB5FB7826E9D04E93C,
	Manifest_set_Domain_mFCC8FB48CD1242D930CC2193E3CD4BB1D5ECEF4A,
	Manifest_get_Entities_mF45F3CF715A5EC40BE253EC1397FC058CD387D65,
	Manifest_set_Entities_m571B306731A0D5C392EE51652F9491E2D1A15152,
	Manifest_get_Actions_mD8705A97A545D14BEFA977C6EC90F0135D4F8AC1,
	Manifest_set_Actions_m9598E8D75278EA81E1611096659DFC7CA778F32A,
	Manifest_ResolveActions_m8F30EE572C6406B267DC6317B54426F2FA110E45,
	Manifest_GetBestMethodMatch_mD83612F9ED012FBBAB2927DD7025A0E820D88A2A,
	Manifest_ContainsAction_mCF03300D0B05617962CC35EDFBC991763B31F050,
	Manifest_GetInvocationContexts_m271750391476AF7F224F56757986724385A4AB73,
	U3CU3Ec__cctor_mA8A57E286D93E6D5B2CCA1E4DC3086665B522C9C,
	U3CU3Ec__ctor_m06DE2518A2516E8CDC24C942D734FB372A4BCAFD,
	U3CU3Ec_U3CResolveActionsU3Eb__22_0_m6FA2123E99D272484DA6766A69E6178AC2ED23C8,
	U3CU3Ec_U3CResolveActionsU3Eb__22_1_m4F8E437CAEB7D91F5F2C5B577779272A90D569D4,
	ManifestAction__ctor_mBD486DB41CA3BF1669B90E88F63C14EAD8786392,
	ManifestAction_get_ID_mA8C0AA2B8E0AD6997D82E9CD729C5ECED0029FE9,
	ManifestAction_set_ID_m6CADD5DB2F7D43E2F31C683F47B1FC215654520D,
	ManifestAction_get_Assembly_m5C563ECB155985C292593A42FE94B9146EAFDF0C,
	ManifestAction_set_Assembly_mB8033D761C97E13B956AE81BD6662FD4F4FD3249,
	ManifestAction_get_Name_mA0A17B5F92AD9040023F72D3FF8FA976E7194E33,
	ManifestAction_set_Name_m7FFF6562E6F6DC655F1DF3593682D4D75968289A,
	ManifestAction_get_Parameters_m20870BE54DBD68586A57573B431585871B05B7AE,
	ManifestAction_set_Parameters_m47332CA890C1793F21E8215057A26518822C070D,
	ManifestAction_get_DeclaringTypeName_m0357AB06F7A0B23268D3D8658F993A32FA8C8A42,
	ManifestAction_get_Aliases_m09A1420BDCF18F4E0B7DE5E14EE8B94DC682488B,
	ManifestAction_set_Aliases_m07C567DA991B205A2F31415A1B6B33816028ADAB,
	ManifestAction_Equals_m5125DCAD8F70A3F8E36C69F0C64AF5EF5331F0E9,
	ManifestAction_GetHashCode_m54BBE3647F5E823125433874258B1EBF87DCAD81,
	ManifestAction_Equals_m8099A8FA2624F0E99C5F6DCF85D3043CD760FF3B,
	ManifestEntity__ctor_m66F53E41520C28C673C7E92361DFC32D9E4CE928,
	ManifestEntity_get_ID_m3B9958F8C96A39013CBC45DE69AC6CD8978A44EA,
	ManifestEntity_set_ID_mD81F45837BBEC591E922D15E64B666D29FC3B934,
	ManifestEntity_get_Namespace_m9F470413C55992958A2C08F80963486BA8567172,
	ManifestEntity_set_Namespace_m53308E6C2BB0F64A7C7CD84371760A2A20754619,
	ManifestEntity_get_Type_mF29EDA2CAB1D4CA52EEEF9698033233BBE42057A,
	ManifestEntity_set_Type_mC2B60CEDD86AA42AA505EBC5C3C8459AA62D3CFD,
	ManifestEntity_get_Name_mB4D88A1CDB8DED1F4E5976249A7B50D81B119B5E,
	ManifestEntity_set_Name_m70662A558EC71DB2EB79BEAEFDF0E54CE334669A,
	ManifestEntity_get_Values_mF1AA90A3B30EAD99ACB8827089305AE2DB2092C7,
	ManifestEntity_set_Values_m23816C58376B0ED48271F055462A15E1384461C5,
	ManifestEntity_get_Assembly_m2E977627A6E8FE674FB8A354989EAC3F14EAEB06,
	ManifestEntity_set_Assembly_mD6261D660050EE1365EEA2319609D86294A7CADC,
	ManifestEntity_GetAsInfo_mE742C19AA49C3BA12B3CC860DB8E1DD7A1FDEB6C,
	ManifestEntity_Equals_mCCFECE96B4AFAE31FDA38E16532D1979C15885E5,
	ManifestEntity_GetHashCode_m39C3F2CCD83F369642D89A156406E015CBC057F1,
	ManifestEntity_Equals_mE390697D4BC7FDEE97F71AD95C032B828D682EB7,
	ManifestLoader_LoadManifest_m5BA61228731F9D4B47051315A0D3A0F76728857E,
	ManifestLoader_LoadManifestFromString_m5BB338371F64DB1629DE4FB06F4B4931594CD06F,
	ManifestLoader__ctor_mFA981E01FDE27FA33200E2471DE9E5EC9F0C4241,
	ManifestParameter__ctor_mBCDAE5DD90205CC64CBDC70AE9AC9449B7B47F8B,
	ManifestParameter_get_Name_mD4158A3289CED1ACB68454747F3C1EA871ED9AC3,
	ManifestParameter_set_Name_mCD30D8B54F62D1C910492F8931A2A2F0022AD5F3,
	ManifestParameter_get_InternalName_m6A6ED9811F9520A02CE5D944BC6B50622DB0E7E1,
	ManifestParameter_set_InternalName_m0E0549A6F5DB2FBD37C320E7EC9E69A506E921C8,
	ManifestParameter_get_QualifiedName_m69BDD38050AC8A6478C08E6119EC84294764FB1A,
	ManifestParameter_set_QualifiedName_m24E6E99E9FD8949B08C954758DA8011C6CB5DFBF,
	ManifestParameter_get_EntityType_mC1B6C48D44C1B7D526CFDF20E3DB58C557C0DE9D,
	ManifestParameter_get_TypeAssembly_mB8CCA15F9B2593A3AEF8A242950D4F097E3CFB18,
	ManifestParameter_set_TypeAssembly_m94F527762251ECACB0C978E0CEA2F7C8513BBA59,
	ManifestParameter_get_QualifiedTypeName_mB9D2527C2D0820F22F46AE03A82AFC75584B8D64,
	ManifestParameter_set_QualifiedTypeName_m0DF238D95D264D39661907411ABED38E3309E6D9,
	ManifestParameter_get_Aliases_mEFC955CEBE01F9F22EE08C426C3CC8CC793B06CB,
	ManifestParameter_set_Aliases_mA25C7F207BEDBB83F06577D88260CB9BC7D37E47,
	ManifestParameter_get_Examples_m335253557ACF245D37D72EC1E6395D2115A7DA9F,
	ManifestParameter_set_Examples_mC3BEE6E90FF521172E06B5252455171327326DD9,
	ManifestParameter_Equals_m02F081B9B9F331CA19B3A45D4B0A1DEB648A087F,
	ManifestParameter_GetHashCode_m2F6EC55132F3744937BF5D4C8CF6766380E682D7,
	ManifestParameter_Equals_mE8A904569240E6322DF3D963402C68F9FFEA6C5E,
	WitKeyword__ctor_m13351C55E289FC80144D9FCD977D609B3DFAB4B9,
	WitKeyword__ctor_mA24856B88B54C49C4ED1B11D89714F8EF5C643B8,
	WitKeyword__ctor_mE7A636FB824A679BB9C5CA7C538F80965000EA36,
	WitKeyword_GetAsInfo_mD3F1F9FF0538EDD39B731CDF48761C09CAB37894,
	WitKeyword_Equals_mF8DC72966E4C19E90951E59197675AD5F13A2E54,
	WitKeyword_Equals_m340843728FA629787497D02B62E6707CE88FA2B3,
	WitKeyword_GetHashCode_m5D2AFE1A89CC3C25092A6F59554A58452C753BAC,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ParameterProvider_Populate_m699EFABB776BB2031F2BDA887FA1A50DA9EC7BCD,
	ParameterProvider_ContainsParameter_mC31F9ADDCA19471BF378D8F6670D8B32A621F3A8,
	ParameterProvider_GetParameterValue_m1E7D6ABC6A73D40F509B78A0D14DF453F3CA6442,
	ParameterProvider_SupportedSpecializedParameter_m94193795A128972521C9F17F7A21247E799D047E,
	ParameterProvider_GetSpecializedParameter_mC2FF6D746B56338F63B550A00B81336BB0A9529A,
	ParameterProvider__ctor_mF151418D3C103A98058D3E8F7DE16C7A92B614A8,
};
static const int32_t s_InvokerIndices[153] = 
{
	5077,
	4101,
	5110,
	4133,
	5110,
	4133,
	5077,
	4101,
	4989,
	4012,
	2446,
	479,
	5151,
	5077,
	5077,
	2446,
	5077,
	5077,
	4101,
	2446,
	4101,
	5077,
	1507,
	4101,
	1277,
	1116,
	522,
	2993,
	5151,
	2993,
	5151,
	2993,
	2446,
	5077,
	7528,
	6734,
	7528,
	7528,
	7528,
	7528,
	7528,
	5151,
	7879,
	2443,
	2450,
	754,
	4101,
	-1,
	3696,
	3696,
	5077,
	4101,
	5077,
	4101,
	5110,
	4133,
	5110,
	4133,
	4989,
	4012,
	5151,
	5151,
	5077,
	4101,
	5077,
	4101,
	5077,
	4101,
	5077,
	4101,
	5077,
	4101,
	4989,
	1274,
	2993,
	3696,
	7879,
	5151,
	2993,
	1824,
	5151,
	5077,
	4101,
	5077,
	4101,
	5077,
	4101,
	5077,
	4101,
	5077,
	5077,
	4101,
	2993,
	5045,
	2993,
	5151,
	5077,
	4101,
	5077,
	4101,
	5077,
	4101,
	5077,
	4101,
	5077,
	4101,
	5077,
	4101,
	5154,
	2993,
	5045,
	2993,
	3696,
	3696,
	5151,
	5151,
	5077,
	4101,
	5077,
	4101,
	5077,
	4101,
	5077,
	5077,
	4101,
	5077,
	4101,
	5077,
	4101,
	5077,
	4101,
	2993,
	5045,
	2993,
	5151,
	2446,
	4174,
	5155,
	2993,
	2993,
	5045,
	4101,
	522,
	3696,
	2446,
	1656,
	3696,
	2446,
	1656,
	3696,
	2993,
	3696,
	5151,
};
static const Il2CppTokenRangePair s_rgctxIndices[1] = 
{
	{ 0x06000030, { 0, 7 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[7] = 
{
	{ (Il2CppRGCTXDataType)2, 2933 },
	{ (Il2CppRGCTXDataType)3, 14078 },
	{ (Il2CppRGCTXDataType)3, 20316 },
	{ (Il2CppRGCTXDataType)3, 9523 },
	{ (Il2CppRGCTXDataType)3, 14079 },
	{ (Il2CppRGCTXDataType)3, 9522 },
	{ (Il2CppRGCTXDataType)2, 2344 },
};
extern const CustomAttributesCacheGenerator g_Meta_WitAi_Conduit_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_Meta_WitAi_Conduit_CodeGenModule;
const Il2CppCodeGenModule g_Meta_WitAi_Conduit_CodeGenModule = 
{
	"Meta.WitAi.Conduit.dll",
	153,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	1,
	s_rgctxIndices,
	7,
	s_rgctxValues,
	NULL,
	g_Meta_WitAi_Conduit_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
