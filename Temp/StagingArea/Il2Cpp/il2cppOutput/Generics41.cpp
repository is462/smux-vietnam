﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>


struct VirtActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
struct InterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R>
struct InterfaceFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};

// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>
struct Dictionary_2_t7A371082270CA8FD6BC79B5622CCD5E1C87A6E42;
// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,System.Boolean>
struct Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590;
// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,UnityEngine.InputSystem.Utilities.InternedString>
struct Func_2_tB181F4C9A24C0634DF4A10CFBFFE6B45B3796BBC;
// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,System.Object>
struct Func_2_t711512467B454C91EB8F3FB19CD6D141AAB5B6B1;
// System.Func`2<Oculus.Interaction.Input.HandSkeletonJoint,System.Boolean>
struct Func_2_t31607A88702EFEFC5B33245A610C64EA7D6ED9F7;
// System.Func`2<Oculus.Interaction.Input.HandSkeletonJoint,Oculus.Interaction.Input.HandSkeletonJoint>
struct Func_2_tB339A94B37E01030C823BB96512593067BEF80DC;
// System.Func`2<System.Int32Enum,System.Boolean>
struct Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E;
// System.Func`2<System.Int32Enum,UnityEngine.InputSystem.Utilities.InternedString>
struct Func_2_t043042489DBF19074B5B434F090F7B8AAE97E138;
// System.Func`2<System.Int32Enum,System.Object>
struct Func_2_tF916A06E36F9340A8A2E0053D6A8F0737C5FD2EB;
// System.Func`2<UnityEngine.InputSystem.Utilities.InternedString,System.Boolean>
struct Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1;
// System.Func`2<UnityEngine.InputSystem.Utilities.InternedString,UnityEngine.InputSystem.Utilities.InternedString>
struct Func_2_t1AF155D27A4D78882342D697E4A6DE7CE594545B;
// System.Func`2<UnityEngine.InputSystem.Utilities.InternedString,System.Object>
struct Func_2_tA2BD34BB8D97AD282016BD4CB5B8675E4AF85B45;
// System.Func`2<UnityEngine.InputSystem.Utilities.NameAndParameters,System.Boolean>
struct Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02;
// System.Func`2<UnityEngine.InputSystem.Utilities.NameAndParameters,UnityEngine.InputSystem.Utilities.InternedString>
struct Func_2_tEA2235809AB0E1AB8425745179691D76B519B0C5;
// System.Func`2<UnityEngine.InputSystem.Utilities.NameAndParameters,System.Object>
struct Func_2_tF7304610FB007335AC870B51C8FE2092628DC0DB;
// System.Func`2<UnityEngine.InputSystem.Utilities.NamedValue,System.Boolean>
struct Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5;
// System.Func`2<UnityEngine.InputSystem.Utilities.NamedValue,UnityEngine.InputSystem.Utilities.InternedString>
struct Func_2_tA21678F1D9F9F4617D645755285D28EDE5F9AE44;
// System.Func`2<UnityEngine.InputSystem.Utilities.NamedValue,System.Object>
struct Func_2_t5B101C0B26ADAA3B1A77BC50D51CC42364E5409E;
// System.Func`2<System.Object,System.Boolean>
struct Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8;
// System.Func`2<System.Object,UnityEngine.InputSystem.Utilities.InternedString>
struct Func_2_t964229B6ADD2B008268547ED2BC3C090AF218065;
// System.Func`2<System.Object,System.Object>
struct Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436;
// System.Func`2<UnityEngine.Pose,System.Boolean>
struct Func_2_t288BC2D8C12E52B8F2E7F675CFB740C44E7E813F;
// System.Func`2<UnityEngine.Pose,UnityEngine.Quaternion>
struct Func_2_t6170D516A1F6F9B6BE1BDC5379D0A5CC320AF0F8;
// System.Func`2<UnityEngine.Quaternion,System.Boolean>
struct Func_2_t34B33A6CBEAEB1EF3A60338636A0DC782C33A7BE;
// System.Func`2<System.Single,System.Boolean>
struct Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A;
// System.Func`2<System.Single,UnityEngine.InputSystem.Utilities.InternedString>
struct Func_2_t4E06D96B2978B60DA267963F96583CE3BF82EA84;
// System.Func`2<System.Single,System.Object>
struct Func_2_tE11BDF71BCB314E58F1A9CFC9DAE08A5F717E2A0;
// System.Func`2<UnityEngine.InputSystem.Utilities.Substring,System.Boolean>
struct Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9;
// System.Func`2<UnityEngine.InputSystem.Utilities.Substring,UnityEngine.InputSystem.Utilities.InternedString>
struct Func_2_t14F6F4D7FBF558A20782EDC48D80473E4800048E;
// System.Func`2<UnityEngine.InputSystem.Utilities.Substring,System.Object>
struct Func_2_t4EC2F095EDDC8079537CA0FEDC463635E599C9E4;
// System.Func`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,System.Boolean>
struct Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847;
// System.Func`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,UnityEngine.InputSystem.Utilities.InternedString>
struct Func_2_t48DD1C8E439811C492BAFE220A679DB57D7C9370;
// System.Func`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,System.Object>
struct Func_2_t1628B2E4EEC7A32D5B688E54415BC42EDA9D2899;
// System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>>
struct IEnumerable_1_t4DAB7002E0E04175D7AB0251D5A8284277618627;
// System.Collections.Generic.IEnumerable`1<Oculus.Interaction.Input.HandSkeletonJoint>
struct IEnumerable_1_tFB2584C832E0FF3308C31F32A5F7963DB0D54107;
// System.Collections.Generic.IEnumerable`1<System.Int32Enum>
struct IEnumerable_1_t28FB40D8E33C5846AB04F37C78130A4948569C7C;
// System.Collections.Generic.IEnumerable`1<UnityEngine.InputSystem.Utilities.InternedString>
struct IEnumerable_1_t8A4B908F54ED8D008831DEF6F9A949FFA981E6BB;
// System.Collections.Generic.IEnumerable`1<UnityEngine.InputSystem.Utilities.NameAndParameters>
struct IEnumerable_1_tE4E960C53664FB9663CC2261AEBCA61C5EDB579D;
// System.Collections.Generic.IEnumerable`1<UnityEngine.InputSystem.Utilities.NamedValue>
struct IEnumerable_1_t3E9A1977DB64BEFC51E9B02E141FFD28CCCAFF2B;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t52B1AC8D9E5E1ED28DF6C46A37C9A1B00B394F9D;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Pose>
struct IEnumerable_1_tC0E566D9CB3758029A7BD9306840D835C27EEDAA;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Quaternion>
struct IEnumerable_1_t00E4D64E0BBE3D90A042908257A1AEC6A6CFB4BA;
// System.Collections.Generic.IEnumerable`1<System.Single>
struct IEnumerable_1_t673DFF64E51C18A8B1287F4BD988966CE3B15A45;
// System.Collections.Generic.IEnumerable`1<UnityEngine.InputSystem.Utilities.Substring>
struct IEnumerable_1_t316B1044EDC4315DB26041243EABD2C79E26C97D;
// System.Collections.Generic.IEnumerable`1<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>
struct IEnumerable_1_tDECAFAC2EA60F65D233E35C09DC8E49592E90780;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>>
struct IEnumerator_1_tFBEC9463ED361F2291493660A5217929F0BD545B;
// System.Collections.Generic.IEnumerator`1<Oculus.Interaction.Input.HandSkeletonJoint>
struct IEnumerator_1_tFE7DBDE0D5EFFBB7EB2335075F2150A2B831A42C;
// System.Collections.Generic.IEnumerator`1<System.Int32Enum>
struct IEnumerator_1_tD8D5B0A7736D9FAFB606AC36B0CAD1353B84C3BD;
// System.Collections.Generic.IEnumerator`1<UnityEngine.InputSystem.Utilities.InternedString>
struct IEnumerator_1_tB0283BFB1D45052C90A9ACF8B98A8B1B22B5E19B;
// System.Collections.Generic.IEnumerator`1<UnityEngine.InputSystem.Utilities.NameAndParameters>
struct IEnumerator_1_t11A4DD8C8A897B31E9F14F85734A35486782D576;
// System.Collections.Generic.IEnumerator`1<UnityEngine.InputSystem.Utilities.NamedValue>
struct IEnumerator_1_t06B2E5EA7C5F4B43FEEC71A595B1517272B448FD;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t2DC97C7D486BF9E077C2BC2E517E434F393AA76E;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Pose>
struct IEnumerator_1_t11AAF64F10DC2FFEBB16A1A8F10725A45D921C29;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Quaternion>
struct IEnumerator_1_tC0C0975A58A10C33B0978B5E9C98B5CDC1C5F3A2;
// System.Collections.Generic.IEnumerator`1<System.Single>
struct IEnumerator_1_t5918C99D6FA69C530D0287467B91ADE56FA3D7AF;
// System.Collections.Generic.IEnumerator`1<UnityEngine.InputSystem.Utilities.Substring>
struct IEnumerator_1_t89ADD2D5401AA240C4CE71F52C93E029D4B57F00;
// System.Collections.Generic.IEnumerator`1<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>
struct IEnumerator_1_tDB5F036395ADFF08AD1585AC3A7BE899CF28B360;
// System.Linq.Enumerable/Iterator`1<Oculus.Interaction.Input.HandSkeletonJoint>
struct Iterator_1_tBD3F91668D911F5945632A9FDF70A5848BD6CC26;
// System.Linq.Enumerable/Iterator`1<UnityEngine.InputSystem.Utilities.InternedString>
struct Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5;
// System.Linq.Enumerable/Iterator`1<System.Object>
struct Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279;
// System.Linq.Enumerable/Iterator`1<UnityEngine.Quaternion>
struct Iterator_1_t4BD4EE08E1E7E81949C3754EC2969154580F4C65;
// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>>
struct List_1_t9B1CABCBD4B6B7781E348FF332B32B333B543BA8;
// System.Collections.Generic.List`1<Oculus.Interaction.Input.HandSkeletonJoint>
struct List_1_t92F561CC77DE516A0293B29C982ACDC3391E1658;
// System.Collections.Generic.List`1<System.Int32Enum>
struct List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A;
// System.Collections.Generic.List`1<UnityEngine.InputSystem.Utilities.InternedString>
struct List_1_tFD37D052760A236906215AF581C58EE13DE6A5DC;
// System.Collections.Generic.List`1<UnityEngine.InputSystem.Utilities.NameAndParameters>
struct List_1_tCBF5EB99F2E538DCB47A8C835DD4B9C024879393;
// System.Collections.Generic.List`1<UnityEngine.InputSystem.Utilities.NamedValue>
struct List_1_tDC9592305A8D24F0DBC4A23F1719292FCB2887C1;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5;
// System.Collections.Generic.List`1<UnityEngine.Pose>
struct List_1_tDB586987B7A2D2B13A903004C86E3C3FBD061C1C;
// System.Collections.Generic.List`1<System.Single>
struct List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA;
// System.Collections.Generic.List`1<UnityEngine.InputSystem.Utilities.Substring>
struct List_1_tEAD6E3282E028927B32F56E7892994D90D512467;
// System.Collections.Generic.List`1<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>
struct List_1_tEC0B302C3EF23400C3EC92ED7672A715CA602068;
// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectEnumerableIterator_2_t5171F478E1C976F4C353EA25EC64567E44C86CA0;
// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,System.Object>
struct WhereSelectEnumerableIterator_2_t0ACED5698603E2D2B4071096A021E0FA6102F459;
// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<Oculus.Interaction.Input.HandSkeletonJoint,Oculus.Interaction.Input.HandSkeletonJoint>
struct WhereSelectEnumerableIterator_2_t04AB31927A6E89520049EC6E92923D1FBAC994AB;
// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Int32Enum,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectEnumerableIterator_2_t4F5D6E88F86A03D2A74E02E6BC9FBD56A793C28A;
// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Int32Enum,System.Object>
struct WhereSelectEnumerableIterator_2_tE7CB1EB0F91FF8330C8F18AFE379A77EDF9F91F6;
// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.InternedString,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectEnumerableIterator_2_t0EB79FD57DE6939EBF78FA1D92312CB62DD3F2C6;
// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.InternedString,System.Object>
struct WhereSelectEnumerableIterator_2_t02B3FDD3CA4282B70A77A6A52D6AADC70EF74E8B;
// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectEnumerableIterator_2_t3B8EA5ED092FA87DEAECF94C466A54501B692B0B;
// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,System.Object>
struct WhereSelectEnumerableIterator_2_t9C8232C608C379652355BB860B0A6074ABF36E09;
// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectEnumerableIterator_2_t382E0095C883AA01020D10E08F7F38BBA26A3EC5;
// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,System.Object>
struct WhereSelectEnumerableIterator_2_tEBF9E61EFDABBBB02113A82648E32AC170921957;
// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Object,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectEnumerableIterator_2_tE487654732A8A2F1E3A2D57412C08036EE861F38;
// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Object,System.Object>
struct WhereSelectEnumerableIterator_2_tDAA8BFBEA68F81670F3F51C6200EBD26D7A8FBAB;
// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.Pose,UnityEngine.Quaternion>
struct WhereSelectEnumerableIterator_2_t3529F4FEC1C961CCD3F23682094813DA27D0ADA3;
// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Single,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectEnumerableIterator_2_tB68F25B00798F106AD2C511CF10D0289252107C8;
// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Single,System.Object>
struct WhereSelectEnumerableIterator_2_t8330901F0811E7C54CAD7107B2350ACFCBF3859A;
// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.Substring,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectEnumerableIterator_2_tFDFC244728E14BAC93BE9869A64080C38A0B7F1D;
// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.Substring,System.Object>
struct WhereSelectEnumerableIterator_2_t5E3BB9AFBEC27ED62FC129516C2D3E9FC81641B6;
// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectEnumerableIterator_2_t6D595CFCE85C8B8D77C138F46CCF2608E20474FE;
// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,System.Object>
struct WhereSelectEnumerableIterator_2_t4DAE42F4C708266EAC938F1C6E7FB762CDF50463;
// System.Linq.Enumerable/WhereSelectListIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectListIterator_2_tF2F8745320F9D29534105CFDF2FADABD42A2D3F7;
// System.Linq.Enumerable/WhereSelectListIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,System.Object>
struct WhereSelectListIterator_2_t15BF3EF128C005C26DE8132D49E71B3C1FE6E211;
// System.Linq.Enumerable/WhereSelectListIterator`2<Oculus.Interaction.Input.HandSkeletonJoint,Oculus.Interaction.Input.HandSkeletonJoint>
struct WhereSelectListIterator_2_t912D2CA762EC2D53D104D6B1FFE8BCEF3FA7319F;
// System.Linq.Enumerable/WhereSelectListIterator`2<System.Int32Enum,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectListIterator_2_t75D7B928E94826A5A6BBCB97842E5B2C3BBDF5F5;
// System.Linq.Enumerable/WhereSelectListIterator`2<System.Int32Enum,System.Object>
struct WhereSelectListIterator_2_t5F082584C1A03211ACA908D2D073C1D6149B14A8;
// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.InternedString,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectListIterator_2_t7BDF9DF4ED7F42E4D8139C4BA35A0F055024799E;
// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.InternedString,System.Object>
struct WhereSelectListIterator_2_t1C93CE1377887193667292BEED953C31167E2F75;
// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectListIterator_2_t97CD9F77D3626E616F5290B44E8448C47A64E44C;
// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,System.Object>
struct WhereSelectListIterator_2_tDFC8E5CA0DE13C88A58AD7BCD17F1FC274A3F95D;
// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectListIterator_2_t84216899116C33A67088960F25CA2D6E1DDC786D;
// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,System.Object>
struct WhereSelectListIterator_2_tA4D5B2B639DCB29B4C84236B3E2651F17B522430;
// System.Linq.Enumerable/WhereSelectListIterator`2<System.Object,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectListIterator_2_t4F7AA31B4289096C5D625263B8BBDA051331BE94;
// System.Linq.Enumerable/WhereSelectListIterator`2<System.Object,System.Object>
struct WhereSelectListIterator_2_t85B78DFF0573BC95A62C79D6088FA39DFEBE1AF2;
// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.Pose,UnityEngine.Quaternion>
struct WhereSelectListIterator_2_tA4AFE0F933BE35E59757614CBEDAA04AEBC6AD3D;
// System.Linq.Enumerable/WhereSelectListIterator`2<System.Single,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectListIterator_2_t75E7B369D9A91D962C4DAFB13EE84B0CCE07D9E3;
// System.Linq.Enumerable/WhereSelectListIterator`2<System.Single,System.Object>
struct WhereSelectListIterator_2_t77C04C3B7AD0F11B52AC864B3B1635FBFC5D2259;
// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.Substring,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectListIterator_2_t59101674E6772D817227A6D5C222AF34A4E0BBAD;
// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.Substring,System.Object>
struct WhereSelectListIterator_2_t0CEFF5458EED043DC70F7CC9B97B5AB4D45324EB;
// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectListIterator_2_tF2D97D886F080B26C19947C7C6A56563B86B14D7;
// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,System.Object>
struct WhereSelectListIterator_2_t2F7487B4BF5A9E71CBE06091C45AF7D98CF9F1AC;
// Meta.WitAi.Data.Entities.WitEntityDataBase`1<System.Int32>
struct WitEntityDataBase_1_tC39032EBC060512BB263163466C00E02FE49885F;
// Meta.WitAi.Data.Entities.WitEntityDataBase`1<System.Object>
struct WitEntityDataBase_1_t6ABD82324321B4C13B04F50C98D9489EAA384031;
// Meta.WitAi.Data.Entities.WitEntityDataBase`1<System.Single>
struct WitEntityDataBase_1_t1215DD3B64DF55B5D59B3E81E489EBD1B9353BBE;
// System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>[]
struct KeyValuePair_2U5BU5D_tC4ED6A2024348F6B2CB712DC491F7E57AEF32630;
// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Delegate[]
struct DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8;
// Oculus.Interaction.Input.HandSkeletonJoint[]
struct HandSkeletonJointU5BU5D_t4B699B1E9A4171D2EE43B044CB583B5C1EB64D95;
// System.Int32Enum[]
struct Int32EnumU5BU5D_t9327F857579EE00EB201E1913599094BF837D3CD;
// UnityEngine.InputSystem.Utilities.InternedString[]
struct InternedStringU5BU5D_t1EAE8433C8EF7C76894673801EE0EE9F6833E9A4;
// UnityEngine.InputSystem.Utilities.NameAndParameters[]
struct NameAndParametersU5BU5D_t960DDB52F44DD1231B0C626E9C859F1E0B2F5EAD;
// UnityEngine.InputSystem.Utilities.NamedValue[]
struct NamedValueU5BU5D_t16F5D30B70B5FEDA89AE16CFD61FACEC7DF85391;
// System.Object[]
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE;
// UnityEngine.Pose[]
struct PoseU5BU5D_t45D2BAE8FDADEBC22E30236BB871C3E59C3A455A;
// System.Single[]
struct SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA;
// UnityEngine.InputSystem.Utilities.Substring[]
struct SubstringU5BU5D_tE332884993470F5CF7BAAA950414E4E24691A8D0;
// UnityEngine.InputSystem.Utilities.JsonParser/JsonValue[]
struct JsonValueU5BU5D_t2E1CBAA099182C024E4921379907D707DA049098;
// System.AsyncCallback
struct AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA;
// System.DelegateData
struct DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288;
// System.IAsyncResult
struct IAsyncResult_tC9F97BF36FCF122D29D3101D80642278297BF370;
// Meta.WitAi.Json.JsonConverter
struct JsonConverter_t190B6A25CC7F4B027121414C24452DF540968C7F;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;
// Meta.WitAi.Json.WitResponseArray
struct WitResponseArray_t4432EED47CFE14ABC15E8E96E49D8D7DF4D763C2;
// Meta.WitAi.Json.WitResponseNode
struct WitResponseNode_t2B7283524E11C5C916811576FFFAEBF7FEF27537;

IL2CPP_EXTERN_C RuntimeClass* IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* JsonConvert_t3E03C6491535D0515C23DC9BCF3B620DB0063098_il2cpp_TypeInfo_var;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;

struct JsonConverterU5BU5D_t8327F3FEA5C837FBE4A4A8503B5C0B1AD46A5154;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Linq.Enumerable/Iterator`1<System.Object>
struct Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279  : public RuntimeObject
{
public:
	// System.Int32 System.Linq.Enumerable/Iterator`1::threadId
	int32_t ___threadId_0;
	// System.Int32 System.Linq.Enumerable/Iterator`1::state
	int32_t ___state_1;
	// TSource System.Linq.Enumerable/Iterator`1::current
	RuntimeObject * ___current_2;

public:
	inline static int32_t get_offset_of_threadId_0() { return static_cast<int32_t>(offsetof(Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279, ___threadId_0)); }
	inline int32_t get_threadId_0() const { return ___threadId_0; }
	inline int32_t* get_address_of_threadId_0() { return &___threadId_0; }
	inline void set_threadId_0(int32_t value)
	{
		___threadId_0 = value;
	}

	inline static int32_t get_offset_of_state_1() { return static_cast<int32_t>(offsetof(Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279, ___state_1)); }
	inline int32_t get_state_1() const { return ___state_1; }
	inline int32_t* get_address_of_state_1() { return &___state_1; }
	inline void set_state_1(int32_t value)
	{
		___state_1 = value;
	}

	inline static int32_t get_offset_of_current_2() { return static_cast<int32_t>(offsetof(Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279, ___current_2)); }
	inline RuntimeObject * get_current_2() const { return ___current_2; }
	inline RuntimeObject ** get_address_of_current_2() { return &___current_2; }
	inline void set_current_2(RuntimeObject * value)
	{
		___current_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___current_2), (void*)value);
	}
};


// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>>
struct List_1_t9B1CABCBD4B6B7781E348FF332B32B333B543BA8  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	KeyValuePair_2U5BU5D_tC4ED6A2024348F6B2CB712DC491F7E57AEF32630* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t9B1CABCBD4B6B7781E348FF332B32B333B543BA8, ____items_1)); }
	inline KeyValuePair_2U5BU5D_tC4ED6A2024348F6B2CB712DC491F7E57AEF32630* get__items_1() const { return ____items_1; }
	inline KeyValuePair_2U5BU5D_tC4ED6A2024348F6B2CB712DC491F7E57AEF32630** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(KeyValuePair_2U5BU5D_tC4ED6A2024348F6B2CB712DC491F7E57AEF32630* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t9B1CABCBD4B6B7781E348FF332B32B333B543BA8, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t9B1CABCBD4B6B7781E348FF332B32B333B543BA8, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t9B1CABCBD4B6B7781E348FF332B32B333B543BA8, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};


// System.Collections.Generic.List`1<Oculus.Interaction.Input.HandSkeletonJoint>
struct List_1_t92F561CC77DE516A0293B29C982ACDC3391E1658  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	HandSkeletonJointU5BU5D_t4B699B1E9A4171D2EE43B044CB583B5C1EB64D95* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t92F561CC77DE516A0293B29C982ACDC3391E1658, ____items_1)); }
	inline HandSkeletonJointU5BU5D_t4B699B1E9A4171D2EE43B044CB583B5C1EB64D95* get__items_1() const { return ____items_1; }
	inline HandSkeletonJointU5BU5D_t4B699B1E9A4171D2EE43B044CB583B5C1EB64D95** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(HandSkeletonJointU5BU5D_t4B699B1E9A4171D2EE43B044CB583B5C1EB64D95* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t92F561CC77DE516A0293B29C982ACDC3391E1658, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t92F561CC77DE516A0293B29C982ACDC3391E1658, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t92F561CC77DE516A0293B29C982ACDC3391E1658, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};


// System.Collections.Generic.List`1<System.Int32Enum>
struct List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Int32EnumU5BU5D_t9327F857579EE00EB201E1913599094BF837D3CD* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A, ____items_1)); }
	inline Int32EnumU5BU5D_t9327F857579EE00EB201E1913599094BF837D3CD* get__items_1() const { return ____items_1; }
	inline Int32EnumU5BU5D_t9327F857579EE00EB201E1913599094BF837D3CD** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Int32EnumU5BU5D_t9327F857579EE00EB201E1913599094BF837D3CD* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};


// System.Collections.Generic.List`1<UnityEngine.InputSystem.Utilities.InternedString>
struct List_1_tFD37D052760A236906215AF581C58EE13DE6A5DC  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	InternedStringU5BU5D_t1EAE8433C8EF7C76894673801EE0EE9F6833E9A4* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_tFD37D052760A236906215AF581C58EE13DE6A5DC, ____items_1)); }
	inline InternedStringU5BU5D_t1EAE8433C8EF7C76894673801EE0EE9F6833E9A4* get__items_1() const { return ____items_1; }
	inline InternedStringU5BU5D_t1EAE8433C8EF7C76894673801EE0EE9F6833E9A4** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(InternedStringU5BU5D_t1EAE8433C8EF7C76894673801EE0EE9F6833E9A4* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_tFD37D052760A236906215AF581C58EE13DE6A5DC, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_tFD37D052760A236906215AF581C58EE13DE6A5DC, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_tFD37D052760A236906215AF581C58EE13DE6A5DC, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};


// System.Collections.Generic.List`1<UnityEngine.InputSystem.Utilities.NameAndParameters>
struct List_1_tCBF5EB99F2E538DCB47A8C835DD4B9C024879393  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	NameAndParametersU5BU5D_t960DDB52F44DD1231B0C626E9C859F1E0B2F5EAD* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_tCBF5EB99F2E538DCB47A8C835DD4B9C024879393, ____items_1)); }
	inline NameAndParametersU5BU5D_t960DDB52F44DD1231B0C626E9C859F1E0B2F5EAD* get__items_1() const { return ____items_1; }
	inline NameAndParametersU5BU5D_t960DDB52F44DD1231B0C626E9C859F1E0B2F5EAD** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(NameAndParametersU5BU5D_t960DDB52F44DD1231B0C626E9C859F1E0B2F5EAD* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_tCBF5EB99F2E538DCB47A8C835DD4B9C024879393, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_tCBF5EB99F2E538DCB47A8C835DD4B9C024879393, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_tCBF5EB99F2E538DCB47A8C835DD4B9C024879393, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};


// System.Collections.Generic.List`1<UnityEngine.InputSystem.Utilities.NamedValue>
struct List_1_tDC9592305A8D24F0DBC4A23F1719292FCB2887C1  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	NamedValueU5BU5D_t16F5D30B70B5FEDA89AE16CFD61FACEC7DF85391* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_tDC9592305A8D24F0DBC4A23F1719292FCB2887C1, ____items_1)); }
	inline NamedValueU5BU5D_t16F5D30B70B5FEDA89AE16CFD61FACEC7DF85391* get__items_1() const { return ____items_1; }
	inline NamedValueU5BU5D_t16F5D30B70B5FEDA89AE16CFD61FACEC7DF85391** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(NamedValueU5BU5D_t16F5D30B70B5FEDA89AE16CFD61FACEC7DF85391* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_tDC9592305A8D24F0DBC4A23F1719292FCB2887C1, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_tDC9592305A8D24F0DBC4A23F1719292FCB2887C1, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_tDC9592305A8D24F0DBC4A23F1719292FCB2887C1, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};


// System.Collections.Generic.List`1<System.Object>
struct List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____items_1)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get__items_1() const { return ____items_1; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};


// System.Collections.Generic.List`1<UnityEngine.Pose>
struct List_1_tDB586987B7A2D2B13A903004C86E3C3FBD061C1C  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	PoseU5BU5D_t45D2BAE8FDADEBC22E30236BB871C3E59C3A455A* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_tDB586987B7A2D2B13A903004C86E3C3FBD061C1C, ____items_1)); }
	inline PoseU5BU5D_t45D2BAE8FDADEBC22E30236BB871C3E59C3A455A* get__items_1() const { return ____items_1; }
	inline PoseU5BU5D_t45D2BAE8FDADEBC22E30236BB871C3E59C3A455A** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(PoseU5BU5D_t45D2BAE8FDADEBC22E30236BB871C3E59C3A455A* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_tDB586987B7A2D2B13A903004C86E3C3FBD061C1C, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_tDB586987B7A2D2B13A903004C86E3C3FBD061C1C, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_tDB586987B7A2D2B13A903004C86E3C3FBD061C1C, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};


// System.Collections.Generic.List`1<System.Single>
struct List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA, ____items_1)); }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* get__items_1() const { return ____items_1; }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};


// System.Collections.Generic.List`1<UnityEngine.InputSystem.Utilities.Substring>
struct List_1_tEAD6E3282E028927B32F56E7892994D90D512467  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	SubstringU5BU5D_tE332884993470F5CF7BAAA950414E4E24691A8D0* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_tEAD6E3282E028927B32F56E7892994D90D512467, ____items_1)); }
	inline SubstringU5BU5D_tE332884993470F5CF7BAAA950414E4E24691A8D0* get__items_1() const { return ____items_1; }
	inline SubstringU5BU5D_tE332884993470F5CF7BAAA950414E4E24691A8D0** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(SubstringU5BU5D_tE332884993470F5CF7BAAA950414E4E24691A8D0* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_tEAD6E3282E028927B32F56E7892994D90D512467, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_tEAD6E3282E028927B32F56E7892994D90D512467, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_tEAD6E3282E028927B32F56E7892994D90D512467, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};


// System.Collections.Generic.List`1<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>
struct List_1_tEC0B302C3EF23400C3EC92ED7672A715CA602068  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	JsonValueU5BU5D_t2E1CBAA099182C024E4921379907D707DA049098* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_tEC0B302C3EF23400C3EC92ED7672A715CA602068, ____items_1)); }
	inline JsonValueU5BU5D_t2E1CBAA099182C024E4921379907D707DA049098* get__items_1() const { return ____items_1; }
	inline JsonValueU5BU5D_t2E1CBAA099182C024E4921379907D707DA049098** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(JsonValueU5BU5D_t2E1CBAA099182C024E4921379907D707DA049098* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_tEC0B302C3EF23400C3EC92ED7672A715CA602068, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_tEC0B302C3EF23400C3EC92ED7672A715CA602068, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_tEC0B302C3EF23400C3EC92ED7672A715CA602068, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};


// Meta.WitAi.Data.Entities.WitEntityDataBase`1<System.Int32>
struct WitEntityDataBase_1_tC39032EBC060512BB263163466C00E02FE49885F  : public RuntimeObject
{
public:
	// Meta.WitAi.Json.WitResponseNode Meta.WitAi.Data.Entities.WitEntityDataBase`1::responseNode
	WitResponseNode_t2B7283524E11C5C916811576FFFAEBF7FEF27537 * ___responseNode_0;
	// System.String Meta.WitAi.Data.Entities.WitEntityDataBase`1::id
	String_t* ___id_1;
	// System.String Meta.WitAi.Data.Entities.WitEntityDataBase`1::name
	String_t* ___name_2;
	// System.String Meta.WitAi.Data.Entities.WitEntityDataBase`1::role
	String_t* ___role_3;
	// System.Int32 Meta.WitAi.Data.Entities.WitEntityDataBase`1::start
	int32_t ___start_4;
	// System.Int32 Meta.WitAi.Data.Entities.WitEntityDataBase`1::end
	int32_t ___end_5;
	// System.String Meta.WitAi.Data.Entities.WitEntityDataBase`1::type
	String_t* ___type_6;
	// System.String Meta.WitAi.Data.Entities.WitEntityDataBase`1::body
	String_t* ___body_7;
	// T Meta.WitAi.Data.Entities.WitEntityDataBase`1::value
	int32_t ___value_8;
	// System.Single Meta.WitAi.Data.Entities.WitEntityDataBase`1::confidence
	float ___confidence_9;
	// System.Boolean Meta.WitAi.Data.Entities.WitEntityDataBase`1::hasData
	bool ___hasData_10;
	// Meta.WitAi.Json.WitResponseArray Meta.WitAi.Data.Entities.WitEntityDataBase`1::entities
	WitResponseArray_t4432EED47CFE14ABC15E8E96E49D8D7DF4D763C2 * ___entities_11;

public:
	inline static int32_t get_offset_of_responseNode_0() { return static_cast<int32_t>(offsetof(WitEntityDataBase_1_tC39032EBC060512BB263163466C00E02FE49885F, ___responseNode_0)); }
	inline WitResponseNode_t2B7283524E11C5C916811576FFFAEBF7FEF27537 * get_responseNode_0() const { return ___responseNode_0; }
	inline WitResponseNode_t2B7283524E11C5C916811576FFFAEBF7FEF27537 ** get_address_of_responseNode_0() { return &___responseNode_0; }
	inline void set_responseNode_0(WitResponseNode_t2B7283524E11C5C916811576FFFAEBF7FEF27537 * value)
	{
		___responseNode_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___responseNode_0), (void*)value);
	}

	inline static int32_t get_offset_of_id_1() { return static_cast<int32_t>(offsetof(WitEntityDataBase_1_tC39032EBC060512BB263163466C00E02FE49885F, ___id_1)); }
	inline String_t* get_id_1() const { return ___id_1; }
	inline String_t** get_address_of_id_1() { return &___id_1; }
	inline void set_id_1(String_t* value)
	{
		___id_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___id_1), (void*)value);
	}

	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(WitEntityDataBase_1_tC39032EBC060512BB263163466C00E02FE49885F, ___name_2)); }
	inline String_t* get_name_2() const { return ___name_2; }
	inline String_t** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(String_t* value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___name_2), (void*)value);
	}

	inline static int32_t get_offset_of_role_3() { return static_cast<int32_t>(offsetof(WitEntityDataBase_1_tC39032EBC060512BB263163466C00E02FE49885F, ___role_3)); }
	inline String_t* get_role_3() const { return ___role_3; }
	inline String_t** get_address_of_role_3() { return &___role_3; }
	inline void set_role_3(String_t* value)
	{
		___role_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___role_3), (void*)value);
	}

	inline static int32_t get_offset_of_start_4() { return static_cast<int32_t>(offsetof(WitEntityDataBase_1_tC39032EBC060512BB263163466C00E02FE49885F, ___start_4)); }
	inline int32_t get_start_4() const { return ___start_4; }
	inline int32_t* get_address_of_start_4() { return &___start_4; }
	inline void set_start_4(int32_t value)
	{
		___start_4 = value;
	}

	inline static int32_t get_offset_of_end_5() { return static_cast<int32_t>(offsetof(WitEntityDataBase_1_tC39032EBC060512BB263163466C00E02FE49885F, ___end_5)); }
	inline int32_t get_end_5() const { return ___end_5; }
	inline int32_t* get_address_of_end_5() { return &___end_5; }
	inline void set_end_5(int32_t value)
	{
		___end_5 = value;
	}

	inline static int32_t get_offset_of_type_6() { return static_cast<int32_t>(offsetof(WitEntityDataBase_1_tC39032EBC060512BB263163466C00E02FE49885F, ___type_6)); }
	inline String_t* get_type_6() const { return ___type_6; }
	inline String_t** get_address_of_type_6() { return &___type_6; }
	inline void set_type_6(String_t* value)
	{
		___type_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___type_6), (void*)value);
	}

	inline static int32_t get_offset_of_body_7() { return static_cast<int32_t>(offsetof(WitEntityDataBase_1_tC39032EBC060512BB263163466C00E02FE49885F, ___body_7)); }
	inline String_t* get_body_7() const { return ___body_7; }
	inline String_t** get_address_of_body_7() { return &___body_7; }
	inline void set_body_7(String_t* value)
	{
		___body_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___body_7), (void*)value);
	}

	inline static int32_t get_offset_of_value_8() { return static_cast<int32_t>(offsetof(WitEntityDataBase_1_tC39032EBC060512BB263163466C00E02FE49885F, ___value_8)); }
	inline int32_t get_value_8() const { return ___value_8; }
	inline int32_t* get_address_of_value_8() { return &___value_8; }
	inline void set_value_8(int32_t value)
	{
		___value_8 = value;
	}

	inline static int32_t get_offset_of_confidence_9() { return static_cast<int32_t>(offsetof(WitEntityDataBase_1_tC39032EBC060512BB263163466C00E02FE49885F, ___confidence_9)); }
	inline float get_confidence_9() const { return ___confidence_9; }
	inline float* get_address_of_confidence_9() { return &___confidence_9; }
	inline void set_confidence_9(float value)
	{
		___confidence_9 = value;
	}

	inline static int32_t get_offset_of_hasData_10() { return static_cast<int32_t>(offsetof(WitEntityDataBase_1_tC39032EBC060512BB263163466C00E02FE49885F, ___hasData_10)); }
	inline bool get_hasData_10() const { return ___hasData_10; }
	inline bool* get_address_of_hasData_10() { return &___hasData_10; }
	inline void set_hasData_10(bool value)
	{
		___hasData_10 = value;
	}

	inline static int32_t get_offset_of_entities_11() { return static_cast<int32_t>(offsetof(WitEntityDataBase_1_tC39032EBC060512BB263163466C00E02FE49885F, ___entities_11)); }
	inline WitResponseArray_t4432EED47CFE14ABC15E8E96E49D8D7DF4D763C2 * get_entities_11() const { return ___entities_11; }
	inline WitResponseArray_t4432EED47CFE14ABC15E8E96E49D8D7DF4D763C2 ** get_address_of_entities_11() { return &___entities_11; }
	inline void set_entities_11(WitResponseArray_t4432EED47CFE14ABC15E8E96E49D8D7DF4D763C2 * value)
	{
		___entities_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___entities_11), (void*)value);
	}
};


// Meta.WitAi.Data.Entities.WitEntityDataBase`1<System.Object>
struct WitEntityDataBase_1_t6ABD82324321B4C13B04F50C98D9489EAA384031  : public RuntimeObject
{
public:
	// Meta.WitAi.Json.WitResponseNode Meta.WitAi.Data.Entities.WitEntityDataBase`1::responseNode
	WitResponseNode_t2B7283524E11C5C916811576FFFAEBF7FEF27537 * ___responseNode_0;
	// System.String Meta.WitAi.Data.Entities.WitEntityDataBase`1::id
	String_t* ___id_1;
	// System.String Meta.WitAi.Data.Entities.WitEntityDataBase`1::name
	String_t* ___name_2;
	// System.String Meta.WitAi.Data.Entities.WitEntityDataBase`1::role
	String_t* ___role_3;
	// System.Int32 Meta.WitAi.Data.Entities.WitEntityDataBase`1::start
	int32_t ___start_4;
	// System.Int32 Meta.WitAi.Data.Entities.WitEntityDataBase`1::end
	int32_t ___end_5;
	// System.String Meta.WitAi.Data.Entities.WitEntityDataBase`1::type
	String_t* ___type_6;
	// System.String Meta.WitAi.Data.Entities.WitEntityDataBase`1::body
	String_t* ___body_7;
	// T Meta.WitAi.Data.Entities.WitEntityDataBase`1::value
	RuntimeObject * ___value_8;
	// System.Single Meta.WitAi.Data.Entities.WitEntityDataBase`1::confidence
	float ___confidence_9;
	// System.Boolean Meta.WitAi.Data.Entities.WitEntityDataBase`1::hasData
	bool ___hasData_10;
	// Meta.WitAi.Json.WitResponseArray Meta.WitAi.Data.Entities.WitEntityDataBase`1::entities
	WitResponseArray_t4432EED47CFE14ABC15E8E96E49D8D7DF4D763C2 * ___entities_11;

public:
	inline static int32_t get_offset_of_responseNode_0() { return static_cast<int32_t>(offsetof(WitEntityDataBase_1_t6ABD82324321B4C13B04F50C98D9489EAA384031, ___responseNode_0)); }
	inline WitResponseNode_t2B7283524E11C5C916811576FFFAEBF7FEF27537 * get_responseNode_0() const { return ___responseNode_0; }
	inline WitResponseNode_t2B7283524E11C5C916811576FFFAEBF7FEF27537 ** get_address_of_responseNode_0() { return &___responseNode_0; }
	inline void set_responseNode_0(WitResponseNode_t2B7283524E11C5C916811576FFFAEBF7FEF27537 * value)
	{
		___responseNode_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___responseNode_0), (void*)value);
	}

	inline static int32_t get_offset_of_id_1() { return static_cast<int32_t>(offsetof(WitEntityDataBase_1_t6ABD82324321B4C13B04F50C98D9489EAA384031, ___id_1)); }
	inline String_t* get_id_1() const { return ___id_1; }
	inline String_t** get_address_of_id_1() { return &___id_1; }
	inline void set_id_1(String_t* value)
	{
		___id_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___id_1), (void*)value);
	}

	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(WitEntityDataBase_1_t6ABD82324321B4C13B04F50C98D9489EAA384031, ___name_2)); }
	inline String_t* get_name_2() const { return ___name_2; }
	inline String_t** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(String_t* value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___name_2), (void*)value);
	}

	inline static int32_t get_offset_of_role_3() { return static_cast<int32_t>(offsetof(WitEntityDataBase_1_t6ABD82324321B4C13B04F50C98D9489EAA384031, ___role_3)); }
	inline String_t* get_role_3() const { return ___role_3; }
	inline String_t** get_address_of_role_3() { return &___role_3; }
	inline void set_role_3(String_t* value)
	{
		___role_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___role_3), (void*)value);
	}

	inline static int32_t get_offset_of_start_4() { return static_cast<int32_t>(offsetof(WitEntityDataBase_1_t6ABD82324321B4C13B04F50C98D9489EAA384031, ___start_4)); }
	inline int32_t get_start_4() const { return ___start_4; }
	inline int32_t* get_address_of_start_4() { return &___start_4; }
	inline void set_start_4(int32_t value)
	{
		___start_4 = value;
	}

	inline static int32_t get_offset_of_end_5() { return static_cast<int32_t>(offsetof(WitEntityDataBase_1_t6ABD82324321B4C13B04F50C98D9489EAA384031, ___end_5)); }
	inline int32_t get_end_5() const { return ___end_5; }
	inline int32_t* get_address_of_end_5() { return &___end_5; }
	inline void set_end_5(int32_t value)
	{
		___end_5 = value;
	}

	inline static int32_t get_offset_of_type_6() { return static_cast<int32_t>(offsetof(WitEntityDataBase_1_t6ABD82324321B4C13B04F50C98D9489EAA384031, ___type_6)); }
	inline String_t* get_type_6() const { return ___type_6; }
	inline String_t** get_address_of_type_6() { return &___type_6; }
	inline void set_type_6(String_t* value)
	{
		___type_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___type_6), (void*)value);
	}

	inline static int32_t get_offset_of_body_7() { return static_cast<int32_t>(offsetof(WitEntityDataBase_1_t6ABD82324321B4C13B04F50C98D9489EAA384031, ___body_7)); }
	inline String_t* get_body_7() const { return ___body_7; }
	inline String_t** get_address_of_body_7() { return &___body_7; }
	inline void set_body_7(String_t* value)
	{
		___body_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___body_7), (void*)value);
	}

	inline static int32_t get_offset_of_value_8() { return static_cast<int32_t>(offsetof(WitEntityDataBase_1_t6ABD82324321B4C13B04F50C98D9489EAA384031, ___value_8)); }
	inline RuntimeObject * get_value_8() const { return ___value_8; }
	inline RuntimeObject ** get_address_of_value_8() { return &___value_8; }
	inline void set_value_8(RuntimeObject * value)
	{
		___value_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___value_8), (void*)value);
	}

	inline static int32_t get_offset_of_confidence_9() { return static_cast<int32_t>(offsetof(WitEntityDataBase_1_t6ABD82324321B4C13B04F50C98D9489EAA384031, ___confidence_9)); }
	inline float get_confidence_9() const { return ___confidence_9; }
	inline float* get_address_of_confidence_9() { return &___confidence_9; }
	inline void set_confidence_9(float value)
	{
		___confidence_9 = value;
	}

	inline static int32_t get_offset_of_hasData_10() { return static_cast<int32_t>(offsetof(WitEntityDataBase_1_t6ABD82324321B4C13B04F50C98D9489EAA384031, ___hasData_10)); }
	inline bool get_hasData_10() const { return ___hasData_10; }
	inline bool* get_address_of_hasData_10() { return &___hasData_10; }
	inline void set_hasData_10(bool value)
	{
		___hasData_10 = value;
	}

	inline static int32_t get_offset_of_entities_11() { return static_cast<int32_t>(offsetof(WitEntityDataBase_1_t6ABD82324321B4C13B04F50C98D9489EAA384031, ___entities_11)); }
	inline WitResponseArray_t4432EED47CFE14ABC15E8E96E49D8D7DF4D763C2 * get_entities_11() const { return ___entities_11; }
	inline WitResponseArray_t4432EED47CFE14ABC15E8E96E49D8D7DF4D763C2 ** get_address_of_entities_11() { return &___entities_11; }
	inline void set_entities_11(WitResponseArray_t4432EED47CFE14ABC15E8E96E49D8D7DF4D763C2 * value)
	{
		___entities_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___entities_11), (void*)value);
	}
};


// Meta.WitAi.Data.Entities.WitEntityDataBase`1<System.Single>
struct WitEntityDataBase_1_t1215DD3B64DF55B5D59B3E81E489EBD1B9353BBE  : public RuntimeObject
{
public:
	// Meta.WitAi.Json.WitResponseNode Meta.WitAi.Data.Entities.WitEntityDataBase`1::responseNode
	WitResponseNode_t2B7283524E11C5C916811576FFFAEBF7FEF27537 * ___responseNode_0;
	// System.String Meta.WitAi.Data.Entities.WitEntityDataBase`1::id
	String_t* ___id_1;
	// System.String Meta.WitAi.Data.Entities.WitEntityDataBase`1::name
	String_t* ___name_2;
	// System.String Meta.WitAi.Data.Entities.WitEntityDataBase`1::role
	String_t* ___role_3;
	// System.Int32 Meta.WitAi.Data.Entities.WitEntityDataBase`1::start
	int32_t ___start_4;
	// System.Int32 Meta.WitAi.Data.Entities.WitEntityDataBase`1::end
	int32_t ___end_5;
	// System.String Meta.WitAi.Data.Entities.WitEntityDataBase`1::type
	String_t* ___type_6;
	// System.String Meta.WitAi.Data.Entities.WitEntityDataBase`1::body
	String_t* ___body_7;
	// T Meta.WitAi.Data.Entities.WitEntityDataBase`1::value
	float ___value_8;
	// System.Single Meta.WitAi.Data.Entities.WitEntityDataBase`1::confidence
	float ___confidence_9;
	// System.Boolean Meta.WitAi.Data.Entities.WitEntityDataBase`1::hasData
	bool ___hasData_10;
	// Meta.WitAi.Json.WitResponseArray Meta.WitAi.Data.Entities.WitEntityDataBase`1::entities
	WitResponseArray_t4432EED47CFE14ABC15E8E96E49D8D7DF4D763C2 * ___entities_11;

public:
	inline static int32_t get_offset_of_responseNode_0() { return static_cast<int32_t>(offsetof(WitEntityDataBase_1_t1215DD3B64DF55B5D59B3E81E489EBD1B9353BBE, ___responseNode_0)); }
	inline WitResponseNode_t2B7283524E11C5C916811576FFFAEBF7FEF27537 * get_responseNode_0() const { return ___responseNode_0; }
	inline WitResponseNode_t2B7283524E11C5C916811576FFFAEBF7FEF27537 ** get_address_of_responseNode_0() { return &___responseNode_0; }
	inline void set_responseNode_0(WitResponseNode_t2B7283524E11C5C916811576FFFAEBF7FEF27537 * value)
	{
		___responseNode_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___responseNode_0), (void*)value);
	}

	inline static int32_t get_offset_of_id_1() { return static_cast<int32_t>(offsetof(WitEntityDataBase_1_t1215DD3B64DF55B5D59B3E81E489EBD1B9353BBE, ___id_1)); }
	inline String_t* get_id_1() const { return ___id_1; }
	inline String_t** get_address_of_id_1() { return &___id_1; }
	inline void set_id_1(String_t* value)
	{
		___id_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___id_1), (void*)value);
	}

	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(WitEntityDataBase_1_t1215DD3B64DF55B5D59B3E81E489EBD1B9353BBE, ___name_2)); }
	inline String_t* get_name_2() const { return ___name_2; }
	inline String_t** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(String_t* value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___name_2), (void*)value);
	}

	inline static int32_t get_offset_of_role_3() { return static_cast<int32_t>(offsetof(WitEntityDataBase_1_t1215DD3B64DF55B5D59B3E81E489EBD1B9353BBE, ___role_3)); }
	inline String_t* get_role_3() const { return ___role_3; }
	inline String_t** get_address_of_role_3() { return &___role_3; }
	inline void set_role_3(String_t* value)
	{
		___role_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___role_3), (void*)value);
	}

	inline static int32_t get_offset_of_start_4() { return static_cast<int32_t>(offsetof(WitEntityDataBase_1_t1215DD3B64DF55B5D59B3E81E489EBD1B9353BBE, ___start_4)); }
	inline int32_t get_start_4() const { return ___start_4; }
	inline int32_t* get_address_of_start_4() { return &___start_4; }
	inline void set_start_4(int32_t value)
	{
		___start_4 = value;
	}

	inline static int32_t get_offset_of_end_5() { return static_cast<int32_t>(offsetof(WitEntityDataBase_1_t1215DD3B64DF55B5D59B3E81E489EBD1B9353BBE, ___end_5)); }
	inline int32_t get_end_5() const { return ___end_5; }
	inline int32_t* get_address_of_end_5() { return &___end_5; }
	inline void set_end_5(int32_t value)
	{
		___end_5 = value;
	}

	inline static int32_t get_offset_of_type_6() { return static_cast<int32_t>(offsetof(WitEntityDataBase_1_t1215DD3B64DF55B5D59B3E81E489EBD1B9353BBE, ___type_6)); }
	inline String_t* get_type_6() const { return ___type_6; }
	inline String_t** get_address_of_type_6() { return &___type_6; }
	inline void set_type_6(String_t* value)
	{
		___type_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___type_6), (void*)value);
	}

	inline static int32_t get_offset_of_body_7() { return static_cast<int32_t>(offsetof(WitEntityDataBase_1_t1215DD3B64DF55B5D59B3E81E489EBD1B9353BBE, ___body_7)); }
	inline String_t* get_body_7() const { return ___body_7; }
	inline String_t** get_address_of_body_7() { return &___body_7; }
	inline void set_body_7(String_t* value)
	{
		___body_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___body_7), (void*)value);
	}

	inline static int32_t get_offset_of_value_8() { return static_cast<int32_t>(offsetof(WitEntityDataBase_1_t1215DD3B64DF55B5D59B3E81E489EBD1B9353BBE, ___value_8)); }
	inline float get_value_8() const { return ___value_8; }
	inline float* get_address_of_value_8() { return &___value_8; }
	inline void set_value_8(float value)
	{
		___value_8 = value;
	}

	inline static int32_t get_offset_of_confidence_9() { return static_cast<int32_t>(offsetof(WitEntityDataBase_1_t1215DD3B64DF55B5D59B3E81E489EBD1B9353BBE, ___confidence_9)); }
	inline float get_confidence_9() const { return ___confidence_9; }
	inline float* get_address_of_confidence_9() { return &___confidence_9; }
	inline void set_confidence_9(float value)
	{
		___confidence_9 = value;
	}

	inline static int32_t get_offset_of_hasData_10() { return static_cast<int32_t>(offsetof(WitEntityDataBase_1_t1215DD3B64DF55B5D59B3E81E489EBD1B9353BBE, ___hasData_10)); }
	inline bool get_hasData_10() const { return ___hasData_10; }
	inline bool* get_address_of_hasData_10() { return &___hasData_10; }
	inline void set_hasData_10(bool value)
	{
		___hasData_10 = value;
	}

	inline static int32_t get_offset_of_entities_11() { return static_cast<int32_t>(offsetof(WitEntityDataBase_1_t1215DD3B64DF55B5D59B3E81E489EBD1B9353BBE, ___entities_11)); }
	inline WitResponseArray_t4432EED47CFE14ABC15E8E96E49D8D7DF4D763C2 * get_entities_11() const { return ___entities_11; }
	inline WitResponseArray_t4432EED47CFE14ABC15E8E96E49D8D7DF4D763C2 ** get_address_of_entities_11() { return &___entities_11; }
	inline void set_entities_11(WitResponseArray_t4432EED47CFE14ABC15E8E96E49D8D7DF4D763C2 * value)
	{
		___entities_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___entities_11), (void*)value);
	}
};

struct Il2CppArrayBounds;

// System.Array


// Meta.WitAi.Json.JsonConverter
struct JsonConverter_t190B6A25CC7F4B027121414C24452DF540968C7F  : public RuntimeObject
{
public:
	// System.Boolean Meta.WitAi.Json.JsonConverter::<CanRead>k__BackingField
	bool ___U3CCanReadU3Ek__BackingField_0;
	// System.Boolean Meta.WitAi.Json.JsonConverter::<CanWrite>k__BackingField
	bool ___U3CCanWriteU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CCanReadU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(JsonConverter_t190B6A25CC7F4B027121414C24452DF540968C7F, ___U3CCanReadU3Ek__BackingField_0)); }
	inline bool get_U3CCanReadU3Ek__BackingField_0() const { return ___U3CCanReadU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CCanReadU3Ek__BackingField_0() { return &___U3CCanReadU3Ek__BackingField_0; }
	inline void set_U3CCanReadU3Ek__BackingField_0(bool value)
	{
		___U3CCanReadU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CCanWriteU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(JsonConverter_t190B6A25CC7F4B027121414C24452DF540968C7F, ___U3CCanWriteU3Ek__BackingField_1)); }
	inline bool get_U3CCanWriteU3Ek__BackingField_1() const { return ___U3CCanWriteU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CCanWriteU3Ek__BackingField_1() { return &___U3CCanWriteU3Ek__BackingField_1; }
	inline void set_U3CCanWriteU3Ek__BackingField_1(bool value)
	{
		___U3CCanWriteU3Ek__BackingField_1 = value;
	}
};


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// Meta.WitAi.Json.WitResponseNode
struct WitResponseNode_t2B7283524E11C5C916811576FFFAEBF7FEF27537  : public RuntimeObject
{
public:

public:
};


// System.Collections.Generic.List`1/Enumerator<System.Object>
struct Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::list
	List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	RuntimeObject * ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6, ___list_0)); }
	inline List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * get_list_0() const { return ___list_0; }
	inline List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6, ___current_3)); }
	inline RuntimeObject * get_current_3() const { return ___current_3; }
	inline RuntimeObject ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(RuntimeObject * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___current_3), (void*)value);
	}
};


// System.Collections.Generic.List`1/Enumerator<System.Single>
struct Enumerator_tC1FD01C8BB5327D442D71FD022B4338ACD701783 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::list
	List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA * ___list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	float ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_tC1FD01C8BB5327D442D71FD022B4338ACD701783, ___list_0)); }
	inline List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA * get_list_0() const { return ___list_0; }
	inline List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_tC1FD01C8BB5327D442D71FD022B4338ACD701783, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_tC1FD01C8BB5327D442D71FD022B4338ACD701783, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_tC1FD01C8BB5327D442D71FD022B4338ACD701783, ___current_3)); }
	inline float get_current_3() const { return ___current_3; }
	inline float* get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(float value)
	{
		___current_3 = value;
	}
};


// UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Utilities.NamedValue>
struct ReadOnlyArray_1_t644486DA5FB2B247FC64733F192E2894F46CFCF5 
{
public:
	// TValue[] UnityEngine.InputSystem.Utilities.ReadOnlyArray`1::m_Array
	NamedValueU5BU5D_t16F5D30B70B5FEDA89AE16CFD61FACEC7DF85391* ___m_Array_0;
	// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1::m_StartIndex
	int32_t ___m_StartIndex_1;
	// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1::m_Length
	int32_t ___m_Length_2;

public:
	inline static int32_t get_offset_of_m_Array_0() { return static_cast<int32_t>(offsetof(ReadOnlyArray_1_t644486DA5FB2B247FC64733F192E2894F46CFCF5, ___m_Array_0)); }
	inline NamedValueU5BU5D_t16F5D30B70B5FEDA89AE16CFD61FACEC7DF85391* get_m_Array_0() const { return ___m_Array_0; }
	inline NamedValueU5BU5D_t16F5D30B70B5FEDA89AE16CFD61FACEC7DF85391** get_address_of_m_Array_0() { return &___m_Array_0; }
	inline void set_m_Array_0(NamedValueU5BU5D_t16F5D30B70B5FEDA89AE16CFD61FACEC7DF85391* value)
	{
		___m_Array_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Array_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_StartIndex_1() { return static_cast<int32_t>(offsetof(ReadOnlyArray_1_t644486DA5FB2B247FC64733F192E2894F46CFCF5, ___m_StartIndex_1)); }
	inline int32_t get_m_StartIndex_1() const { return ___m_StartIndex_1; }
	inline int32_t* get_address_of_m_StartIndex_1() { return &___m_StartIndex_1; }
	inline void set_m_StartIndex_1(int32_t value)
	{
		___m_StartIndex_1 = value;
	}

	inline static int32_t get_offset_of_m_Length_2() { return static_cast<int32_t>(offsetof(ReadOnlyArray_1_t644486DA5FB2B247FC64733F192E2894F46CFCF5, ___m_Length_2)); }
	inline int32_t get_m_Length_2() const { return ___m_Length_2; }
	inline int32_t* get_address_of_m_Length_2() { return &___m_Length_2; }
	inline void set_m_Length_2(int32_t value)
	{
		___m_Length_2 = value;
	}
};


// System.Linq.Enumerable/WhereEnumerableIterator`1<System.Object>
struct WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0  : public Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereEnumerableIterator`1::source
	RuntimeObject* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereEnumerableIterator`1::predicate
	Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate_4;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/WhereEnumerableIterator`1::enumerator
	RuntimeObject* ___enumerator_5;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0, ___source_3)); }
	inline RuntimeObject* get_source_3() const { return ___source_3; }
	inline RuntimeObject** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(RuntimeObject* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0, ___predicate_4)); }
	inline Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_5() { return static_cast<int32_t>(offsetof(WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0, ___enumerator_5)); }
	inline RuntimeObject* get_enumerator_5() const { return ___enumerator_5; }
	inline RuntimeObject** get_address_of_enumerator_5() { return &___enumerator_5; }
	inline void set_enumerator_5(RuntimeObject* value)
	{
		___enumerator_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumerator_5), (void*)value);
	}
};


// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,System.Object>
struct WhereSelectEnumerableIterator_2_t0ACED5698603E2D2B4071096A021E0FA6102F459  : public Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::source
	RuntimeObject* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::predicate
	Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::selector
	Func_2_t711512467B454C91EB8F3FB19CD6D141AAB5B6B1 * ___selector_5;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::enumerator
	RuntimeObject* ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t0ACED5698603E2D2B4071096A021E0FA6102F459, ___source_3)); }
	inline RuntimeObject* get_source_3() const { return ___source_3; }
	inline RuntimeObject** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(RuntimeObject* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t0ACED5698603E2D2B4071096A021E0FA6102F459, ___predicate_4)); }
	inline Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t0ACED5698603E2D2B4071096A021E0FA6102F459, ___selector_5)); }
	inline Func_2_t711512467B454C91EB8F3FB19CD6D141AAB5B6B1 * get_selector_5() const { return ___selector_5; }
	inline Func_2_t711512467B454C91EB8F3FB19CD6D141AAB5B6B1 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t711512467B454C91EB8F3FB19CD6D141AAB5B6B1 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t0ACED5698603E2D2B4071096A021E0FA6102F459, ___enumerator_6)); }
	inline RuntimeObject* get_enumerator_6() const { return ___enumerator_6; }
	inline RuntimeObject** get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(RuntimeObject* value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumerator_6), (void*)value);
	}
};


// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Int32Enum,System.Object>
struct WhereSelectEnumerableIterator_2_tE7CB1EB0F91FF8330C8F18AFE379A77EDF9F91F6  : public Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::source
	RuntimeObject* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::predicate
	Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::selector
	Func_2_tF916A06E36F9340A8A2E0053D6A8F0737C5FD2EB * ___selector_5;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::enumerator
	RuntimeObject* ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tE7CB1EB0F91FF8330C8F18AFE379A77EDF9F91F6, ___source_3)); }
	inline RuntimeObject* get_source_3() const { return ___source_3; }
	inline RuntimeObject** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(RuntimeObject* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tE7CB1EB0F91FF8330C8F18AFE379A77EDF9F91F6, ___predicate_4)); }
	inline Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tE7CB1EB0F91FF8330C8F18AFE379A77EDF9F91F6, ___selector_5)); }
	inline Func_2_tF916A06E36F9340A8A2E0053D6A8F0737C5FD2EB * get_selector_5() const { return ___selector_5; }
	inline Func_2_tF916A06E36F9340A8A2E0053D6A8F0737C5FD2EB ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_tF916A06E36F9340A8A2E0053D6A8F0737C5FD2EB * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tE7CB1EB0F91FF8330C8F18AFE379A77EDF9F91F6, ___enumerator_6)); }
	inline RuntimeObject* get_enumerator_6() const { return ___enumerator_6; }
	inline RuntimeObject** get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(RuntimeObject* value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumerator_6), (void*)value);
	}
};


// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.InternedString,System.Object>
struct WhereSelectEnumerableIterator_2_t02B3FDD3CA4282B70A77A6A52D6AADC70EF74E8B  : public Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::source
	RuntimeObject* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::predicate
	Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::selector
	Func_2_tA2BD34BB8D97AD282016BD4CB5B8675E4AF85B45 * ___selector_5;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::enumerator
	RuntimeObject* ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t02B3FDD3CA4282B70A77A6A52D6AADC70EF74E8B, ___source_3)); }
	inline RuntimeObject* get_source_3() const { return ___source_3; }
	inline RuntimeObject** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(RuntimeObject* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t02B3FDD3CA4282B70A77A6A52D6AADC70EF74E8B, ___predicate_4)); }
	inline Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t02B3FDD3CA4282B70A77A6A52D6AADC70EF74E8B, ___selector_5)); }
	inline Func_2_tA2BD34BB8D97AD282016BD4CB5B8675E4AF85B45 * get_selector_5() const { return ___selector_5; }
	inline Func_2_tA2BD34BB8D97AD282016BD4CB5B8675E4AF85B45 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_tA2BD34BB8D97AD282016BD4CB5B8675E4AF85B45 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t02B3FDD3CA4282B70A77A6A52D6AADC70EF74E8B, ___enumerator_6)); }
	inline RuntimeObject* get_enumerator_6() const { return ___enumerator_6; }
	inline RuntimeObject** get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(RuntimeObject* value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumerator_6), (void*)value);
	}
};


// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,System.Object>
struct WhereSelectEnumerableIterator_2_t9C8232C608C379652355BB860B0A6074ABF36E09  : public Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::source
	RuntimeObject* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::predicate
	Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::selector
	Func_2_tF7304610FB007335AC870B51C8FE2092628DC0DB * ___selector_5;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::enumerator
	RuntimeObject* ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t9C8232C608C379652355BB860B0A6074ABF36E09, ___source_3)); }
	inline RuntimeObject* get_source_3() const { return ___source_3; }
	inline RuntimeObject** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(RuntimeObject* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t9C8232C608C379652355BB860B0A6074ABF36E09, ___predicate_4)); }
	inline Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t9C8232C608C379652355BB860B0A6074ABF36E09, ___selector_5)); }
	inline Func_2_tF7304610FB007335AC870B51C8FE2092628DC0DB * get_selector_5() const { return ___selector_5; }
	inline Func_2_tF7304610FB007335AC870B51C8FE2092628DC0DB ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_tF7304610FB007335AC870B51C8FE2092628DC0DB * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t9C8232C608C379652355BB860B0A6074ABF36E09, ___enumerator_6)); }
	inline RuntimeObject* get_enumerator_6() const { return ___enumerator_6; }
	inline RuntimeObject** get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(RuntimeObject* value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumerator_6), (void*)value);
	}
};


// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,System.Object>
struct WhereSelectEnumerableIterator_2_tEBF9E61EFDABBBB02113A82648E32AC170921957  : public Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::source
	RuntimeObject* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::predicate
	Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::selector
	Func_2_t5B101C0B26ADAA3B1A77BC50D51CC42364E5409E * ___selector_5;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::enumerator
	RuntimeObject* ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tEBF9E61EFDABBBB02113A82648E32AC170921957, ___source_3)); }
	inline RuntimeObject* get_source_3() const { return ___source_3; }
	inline RuntimeObject** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(RuntimeObject* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tEBF9E61EFDABBBB02113A82648E32AC170921957, ___predicate_4)); }
	inline Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tEBF9E61EFDABBBB02113A82648E32AC170921957, ___selector_5)); }
	inline Func_2_t5B101C0B26ADAA3B1A77BC50D51CC42364E5409E * get_selector_5() const { return ___selector_5; }
	inline Func_2_t5B101C0B26ADAA3B1A77BC50D51CC42364E5409E ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t5B101C0B26ADAA3B1A77BC50D51CC42364E5409E * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tEBF9E61EFDABBBB02113A82648E32AC170921957, ___enumerator_6)); }
	inline RuntimeObject* get_enumerator_6() const { return ___enumerator_6; }
	inline RuntimeObject** get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(RuntimeObject* value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumerator_6), (void*)value);
	}
};


// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Object,System.Object>
struct WhereSelectEnumerableIterator_2_tDAA8BFBEA68F81670F3F51C6200EBD26D7A8FBAB  : public Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::source
	RuntimeObject* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::predicate
	Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::selector
	Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 * ___selector_5;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::enumerator
	RuntimeObject* ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tDAA8BFBEA68F81670F3F51C6200EBD26D7A8FBAB, ___source_3)); }
	inline RuntimeObject* get_source_3() const { return ___source_3; }
	inline RuntimeObject** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(RuntimeObject* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tDAA8BFBEA68F81670F3F51C6200EBD26D7A8FBAB, ___predicate_4)); }
	inline Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tDAA8BFBEA68F81670F3F51C6200EBD26D7A8FBAB, ___selector_5)); }
	inline Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 * get_selector_5() const { return ___selector_5; }
	inline Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tDAA8BFBEA68F81670F3F51C6200EBD26D7A8FBAB, ___enumerator_6)); }
	inline RuntimeObject* get_enumerator_6() const { return ___enumerator_6; }
	inline RuntimeObject** get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(RuntimeObject* value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumerator_6), (void*)value);
	}
};


// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Single,System.Object>
struct WhereSelectEnumerableIterator_2_t8330901F0811E7C54CAD7107B2350ACFCBF3859A  : public Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::source
	RuntimeObject* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::predicate
	Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::selector
	Func_2_tE11BDF71BCB314E58F1A9CFC9DAE08A5F717E2A0 * ___selector_5;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::enumerator
	RuntimeObject* ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t8330901F0811E7C54CAD7107B2350ACFCBF3859A, ___source_3)); }
	inline RuntimeObject* get_source_3() const { return ___source_3; }
	inline RuntimeObject** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(RuntimeObject* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t8330901F0811E7C54CAD7107B2350ACFCBF3859A, ___predicate_4)); }
	inline Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t8330901F0811E7C54CAD7107B2350ACFCBF3859A, ___selector_5)); }
	inline Func_2_tE11BDF71BCB314E58F1A9CFC9DAE08A5F717E2A0 * get_selector_5() const { return ___selector_5; }
	inline Func_2_tE11BDF71BCB314E58F1A9CFC9DAE08A5F717E2A0 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_tE11BDF71BCB314E58F1A9CFC9DAE08A5F717E2A0 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t8330901F0811E7C54CAD7107B2350ACFCBF3859A, ___enumerator_6)); }
	inline RuntimeObject* get_enumerator_6() const { return ___enumerator_6; }
	inline RuntimeObject** get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(RuntimeObject* value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumerator_6), (void*)value);
	}
};


// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.Substring,System.Object>
struct WhereSelectEnumerableIterator_2_t5E3BB9AFBEC27ED62FC129516C2D3E9FC81641B6  : public Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::source
	RuntimeObject* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::predicate
	Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::selector
	Func_2_t4EC2F095EDDC8079537CA0FEDC463635E599C9E4 * ___selector_5;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::enumerator
	RuntimeObject* ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t5E3BB9AFBEC27ED62FC129516C2D3E9FC81641B6, ___source_3)); }
	inline RuntimeObject* get_source_3() const { return ___source_3; }
	inline RuntimeObject** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(RuntimeObject* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t5E3BB9AFBEC27ED62FC129516C2D3E9FC81641B6, ___predicate_4)); }
	inline Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t5E3BB9AFBEC27ED62FC129516C2D3E9FC81641B6, ___selector_5)); }
	inline Func_2_t4EC2F095EDDC8079537CA0FEDC463635E599C9E4 * get_selector_5() const { return ___selector_5; }
	inline Func_2_t4EC2F095EDDC8079537CA0FEDC463635E599C9E4 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t4EC2F095EDDC8079537CA0FEDC463635E599C9E4 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t5E3BB9AFBEC27ED62FC129516C2D3E9FC81641B6, ___enumerator_6)); }
	inline RuntimeObject* get_enumerator_6() const { return ___enumerator_6; }
	inline RuntimeObject** get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(RuntimeObject* value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumerator_6), (void*)value);
	}
};


// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,System.Object>
struct WhereSelectEnumerableIterator_2_t4DAE42F4C708266EAC938F1C6E7FB762CDF50463  : public Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::source
	RuntimeObject* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::predicate
	Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::selector
	Func_2_t1628B2E4EEC7A32D5B688E54415BC42EDA9D2899 * ___selector_5;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::enumerator
	RuntimeObject* ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t4DAE42F4C708266EAC938F1C6E7FB762CDF50463, ___source_3)); }
	inline RuntimeObject* get_source_3() const { return ___source_3; }
	inline RuntimeObject** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(RuntimeObject* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t4DAE42F4C708266EAC938F1C6E7FB762CDF50463, ___predicate_4)); }
	inline Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t4DAE42F4C708266EAC938F1C6E7FB762CDF50463, ___selector_5)); }
	inline Func_2_t1628B2E4EEC7A32D5B688E54415BC42EDA9D2899 * get_selector_5() const { return ___selector_5; }
	inline Func_2_t1628B2E4EEC7A32D5B688E54415BC42EDA9D2899 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t1628B2E4EEC7A32D5B688E54415BC42EDA9D2899 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t4DAE42F4C708266EAC938F1C6E7FB762CDF50463, ___enumerator_6)); }
	inline RuntimeObject* get_enumerator_6() const { return ___enumerator_6; }
	inline RuntimeObject** get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(RuntimeObject* value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumerator_6), (void*)value);
	}
};


// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Int32
struct Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};


// UnityEngine.InputSystem.Utilities.InternedString
struct InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED 
{
public:
	// System.String UnityEngine.InputSystem.Utilities.InternedString::m_StringOriginalCase
	String_t* ___m_StringOriginalCase_0;
	// System.String UnityEngine.InputSystem.Utilities.InternedString::m_StringLowerCase
	String_t* ___m_StringLowerCase_1;

public:
	inline static int32_t get_offset_of_m_StringOriginalCase_0() { return static_cast<int32_t>(offsetof(InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED, ___m_StringOriginalCase_0)); }
	inline String_t* get_m_StringOriginalCase_0() const { return ___m_StringOriginalCase_0; }
	inline String_t** get_address_of_m_StringOriginalCase_0() { return &___m_StringOriginalCase_0; }
	inline void set_m_StringOriginalCase_0(String_t* value)
	{
		___m_StringOriginalCase_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_StringOriginalCase_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_StringLowerCase_1() { return static_cast<int32_t>(offsetof(InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED, ___m_StringLowerCase_1)); }
	inline String_t* get_m_StringLowerCase_1() const { return ___m_StringLowerCase_1; }
	inline String_t** get_address_of_m_StringLowerCase_1() { return &___m_StringLowerCase_1; }
	inline void set_m_StringLowerCase_1(String_t* value)
	{
		___m_StringLowerCase_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_StringLowerCase_1), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.InputSystem.Utilities.InternedString
struct InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED_marshaled_pinvoke
{
	char* ___m_StringOriginalCase_0;
	char* ___m_StringLowerCase_1;
};
// Native definition for COM marshalling of UnityEngine.InputSystem.Utilities.InternedString
struct InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED_marshaled_com
{
	Il2CppChar* ___m_StringOriginalCase_0;
	Il2CppChar* ___m_StringLowerCase_1;
};

// UnityEngine.Quaternion
struct Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};


// System.Single
struct Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};


// UnityEngine.InputSystem.Utilities.Substring
struct Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F 
{
public:
	// System.String UnityEngine.InputSystem.Utilities.Substring::m_String
	String_t* ___m_String_0;
	// System.Int32 UnityEngine.InputSystem.Utilities.Substring::m_Index
	int32_t ___m_Index_1;
	// System.Int32 UnityEngine.InputSystem.Utilities.Substring::m_Length
	int32_t ___m_Length_2;

public:
	inline static int32_t get_offset_of_m_String_0() { return static_cast<int32_t>(offsetof(Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F, ___m_String_0)); }
	inline String_t* get_m_String_0() const { return ___m_String_0; }
	inline String_t** get_address_of_m_String_0() { return &___m_String_0; }
	inline void set_m_String_0(String_t* value)
	{
		___m_String_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_String_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Index_1() { return static_cast<int32_t>(offsetof(Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F, ___m_Index_1)); }
	inline int32_t get_m_Index_1() const { return ___m_Index_1; }
	inline int32_t* get_address_of_m_Index_1() { return &___m_Index_1; }
	inline void set_m_Index_1(int32_t value)
	{
		___m_Index_1 = value;
	}

	inline static int32_t get_offset_of_m_Length_2() { return static_cast<int32_t>(offsetof(Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F, ___m_Length_2)); }
	inline int32_t get_m_Length_2() const { return ___m_Length_2; }
	inline int32_t* get_address_of_m_Length_2() { return &___m_Length_2; }
	inline void set_m_Length_2(int32_t value)
	{
		___m_Length_2 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.InputSystem.Utilities.Substring
struct Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F_marshaled_pinvoke
{
	char* ___m_String_0;
	int32_t ___m_Index_1;
	int32_t ___m_Length_2;
};
// Native definition for COM marshalling of UnityEngine.InputSystem.Utilities.Substring
struct Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F_marshaled_com
{
	Il2CppChar* ___m_String_0;
	int32_t ___m_Index_1;
	int32_t ___m_Length_2;
};

// UnityEngine.Vector3
struct Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.InternedString>
struct Enumerator_tD1ECE103520161C1BD172331D6968D3D184B3B6F 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::list
	List_1_tFD37D052760A236906215AF581C58EE13DE6A5DC * ___list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_tD1ECE103520161C1BD172331D6968D3D184B3B6F, ___list_0)); }
	inline List_1_tFD37D052760A236906215AF581C58EE13DE6A5DC * get_list_0() const { return ___list_0; }
	inline List_1_tFD37D052760A236906215AF581C58EE13DE6A5DC ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_tFD37D052760A236906215AF581C58EE13DE6A5DC * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_tD1ECE103520161C1BD172331D6968D3D184B3B6F, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_tD1ECE103520161C1BD172331D6968D3D184B3B6F, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_tD1ECE103520161C1BD172331D6968D3D184B3B6F, ___current_3)); }
	inline InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  get_current_3() const { return ___current_3; }
	inline InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___current_3))->___m_StringOriginalCase_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___current_3))->___m_StringLowerCase_1), (void*)NULL);
		#endif
	}
};


// System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.Substring>
struct Enumerator_t7C514ACB3FF8E3EFB210F5F45C9F18A715BB4F80 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::list
	List_1_tEAD6E3282E028927B32F56E7892994D90D512467 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F  ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_t7C514ACB3FF8E3EFB210F5F45C9F18A715BB4F80, ___list_0)); }
	inline List_1_tEAD6E3282E028927B32F56E7892994D90D512467 * get_list_0() const { return ___list_0; }
	inline List_1_tEAD6E3282E028927B32F56E7892994D90D512467 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_tEAD6E3282E028927B32F56E7892994D90D512467 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_t7C514ACB3FF8E3EFB210F5F45C9F18A715BB4F80, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_t7C514ACB3FF8E3EFB210F5F45C9F18A715BB4F80, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t7C514ACB3FF8E3EFB210F5F45C9F18A715BB4F80, ___current_3)); }
	inline Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F  get_current_3() const { return ___current_3; }
	inline Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F  value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___current_3))->___m_String_0), (void*)NULL);
	}
};


// System.Linq.Enumerable/Iterator`1<UnityEngine.InputSystem.Utilities.InternedString>
struct Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5  : public RuntimeObject
{
public:
	// System.Int32 System.Linq.Enumerable/Iterator`1::threadId
	int32_t ___threadId_0;
	// System.Int32 System.Linq.Enumerable/Iterator`1::state
	int32_t ___state_1;
	// TSource System.Linq.Enumerable/Iterator`1::current
	InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  ___current_2;

public:
	inline static int32_t get_offset_of_threadId_0() { return static_cast<int32_t>(offsetof(Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5, ___threadId_0)); }
	inline int32_t get_threadId_0() const { return ___threadId_0; }
	inline int32_t* get_address_of_threadId_0() { return &___threadId_0; }
	inline void set_threadId_0(int32_t value)
	{
		___threadId_0 = value;
	}

	inline static int32_t get_offset_of_state_1() { return static_cast<int32_t>(offsetof(Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5, ___state_1)); }
	inline int32_t get_state_1() const { return ___state_1; }
	inline int32_t* get_address_of_state_1() { return &___state_1; }
	inline void set_state_1(int32_t value)
	{
		___state_1 = value;
	}

	inline static int32_t get_offset_of_current_2() { return static_cast<int32_t>(offsetof(Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5, ___current_2)); }
	inline InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  get_current_2() const { return ___current_2; }
	inline InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED * get_address_of_current_2() { return &___current_2; }
	inline void set_current_2(InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  value)
	{
		___current_2 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___current_2))->___m_StringOriginalCase_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___current_2))->___m_StringLowerCase_1), (void*)NULL);
		#endif
	}
};


// System.Linq.Enumerable/Iterator`1<UnityEngine.Quaternion>
struct Iterator_1_t4BD4EE08E1E7E81949C3754EC2969154580F4C65  : public RuntimeObject
{
public:
	// System.Int32 System.Linq.Enumerable/Iterator`1::threadId
	int32_t ___threadId_0;
	// System.Int32 System.Linq.Enumerable/Iterator`1::state
	int32_t ___state_1;
	// TSource System.Linq.Enumerable/Iterator`1::current
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___current_2;

public:
	inline static int32_t get_offset_of_threadId_0() { return static_cast<int32_t>(offsetof(Iterator_1_t4BD4EE08E1E7E81949C3754EC2969154580F4C65, ___threadId_0)); }
	inline int32_t get_threadId_0() const { return ___threadId_0; }
	inline int32_t* get_address_of_threadId_0() { return &___threadId_0; }
	inline void set_threadId_0(int32_t value)
	{
		___threadId_0 = value;
	}

	inline static int32_t get_offset_of_state_1() { return static_cast<int32_t>(offsetof(Iterator_1_t4BD4EE08E1E7E81949C3754EC2969154580F4C65, ___state_1)); }
	inline int32_t get_state_1() const { return ___state_1; }
	inline int32_t* get_address_of_state_1() { return &___state_1; }
	inline void set_state_1(int32_t value)
	{
		___state_1 = value;
	}

	inline static int32_t get_offset_of_current_2() { return static_cast<int32_t>(offsetof(Iterator_1_t4BD4EE08E1E7E81949C3754EC2969154580F4C65, ___current_2)); }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  get_current_2() const { return ___current_2; }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 * get_address_of_current_2() { return &___current_2; }
	inline void set_current_2(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  value)
	{
		___current_2 = value;
	}
};


// System.Linq.Enumerable/WhereSelectListIterator`2<System.Object,System.Object>
struct WhereSelectListIterator_2_t85B78DFF0573BC95A62C79D6088FA39DFEBE1AF2  : public Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::source
	List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectListIterator`2::predicate
	Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectListIterator`2::selector
	Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 * ___selector_5;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::enumerator
	Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t85B78DFF0573BC95A62C79D6088FA39DFEBE1AF2, ___source_3)); }
	inline List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * get_source_3() const { return ___source_3; }
	inline List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t85B78DFF0573BC95A62C79D6088FA39DFEBE1AF2, ___predicate_4)); }
	inline Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t85B78DFF0573BC95A62C79D6088FA39DFEBE1AF2, ___selector_5)); }
	inline Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 * get_selector_5() const { return ___selector_5; }
	inline Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t85B78DFF0573BC95A62C79D6088FA39DFEBE1AF2, ___enumerator_6)); }
	inline Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___current_3), (void*)NULL);
		#endif
	}
};


// System.Linq.Enumerable/WhereSelectListIterator`2<System.Single,System.Object>
struct WhereSelectListIterator_2_t77C04C3B7AD0F11B52AC864B3B1635FBFC5D2259  : public Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::source
	List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectListIterator`2::predicate
	Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectListIterator`2::selector
	Func_2_tE11BDF71BCB314E58F1A9CFC9DAE08A5F717E2A0 * ___selector_5;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::enumerator
	Enumerator_tC1FD01C8BB5327D442D71FD022B4338ACD701783  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t77C04C3B7AD0F11B52AC864B3B1635FBFC5D2259, ___source_3)); }
	inline List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA * get_source_3() const { return ___source_3; }
	inline List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t77C04C3B7AD0F11B52AC864B3B1635FBFC5D2259, ___predicate_4)); }
	inline Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t77C04C3B7AD0F11B52AC864B3B1635FBFC5D2259, ___selector_5)); }
	inline Func_2_tE11BDF71BCB314E58F1A9CFC9DAE08A5F717E2A0 * get_selector_5() const { return ___selector_5; }
	inline Func_2_tE11BDF71BCB314E58F1A9CFC9DAE08A5F717E2A0 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_tE11BDF71BCB314E58F1A9CFC9DAE08A5F717E2A0 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t77C04C3B7AD0F11B52AC864B3B1635FBFC5D2259, ___enumerator_6)); }
	inline Enumerator_tC1FD01C8BB5327D442D71FD022B4338ACD701783  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_tC1FD01C8BB5327D442D71FD022B4338ACD701783 * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_tC1FD01C8BB5327D442D71FD022B4338ACD701783  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
	}
};


// System.Delegate
struct Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_target_2), (void*)value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___method_info_7), (void*)value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___original_method_info_8), (void*)value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * get_data_9() const { return ___data_9; }
	inline DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___data_9), (void*)value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	int32_t ___method_is_virtual_10;
};

// System.Int32Enum
struct Int32Enum_t9B63F771913F2B6D586F1173B44A41FBE26F6B5C 
{
public:
	// System.Int32 System.Int32Enum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Int32Enum_t9B63F771913F2B6D586F1173B44A41FBE26F6B5C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.InputSystem.Utilities.NameAndParameters
struct NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA 
{
public:
	// System.String UnityEngine.InputSystem.Utilities.NameAndParameters::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_0;
	// UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Utilities.NamedValue> UnityEngine.InputSystem.Utilities.NameAndParameters::<parameters>k__BackingField
	ReadOnlyArray_1_t644486DA5FB2B247FC64733F192E2894F46CFCF5  ___U3CparametersU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA, ___U3CnameU3Ek__BackingField_0)); }
	inline String_t* get_U3CnameU3Ek__BackingField_0() const { return ___U3CnameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_0() { return &___U3CnameU3Ek__BackingField_0; }
	inline void set_U3CnameU3Ek__BackingField_0(String_t* value)
	{
		___U3CnameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CnameU3Ek__BackingField_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CparametersU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA, ___U3CparametersU3Ek__BackingField_1)); }
	inline ReadOnlyArray_1_t644486DA5FB2B247FC64733F192E2894F46CFCF5  get_U3CparametersU3Ek__BackingField_1() const { return ___U3CparametersU3Ek__BackingField_1; }
	inline ReadOnlyArray_1_t644486DA5FB2B247FC64733F192E2894F46CFCF5 * get_address_of_U3CparametersU3Ek__BackingField_1() { return &___U3CparametersU3Ek__BackingField_1; }
	inline void set_U3CparametersU3Ek__BackingField_1(ReadOnlyArray_1_t644486DA5FB2B247FC64733F192E2894F46CFCF5  value)
	{
		___U3CparametersU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CparametersU3Ek__BackingField_1))->___m_Array_0), (void*)NULL);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.InputSystem.Utilities.NameAndParameters
struct NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA_marshaled_pinvoke
{
	char* ___U3CnameU3Ek__BackingField_0;
	ReadOnlyArray_1_t644486DA5FB2B247FC64733F192E2894F46CFCF5  ___U3CparametersU3Ek__BackingField_1;
};
// Native definition for COM marshalling of UnityEngine.InputSystem.Utilities.NameAndParameters
struct NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA_marshaled_com
{
	Il2CppChar* ___U3CnameU3Ek__BackingField_0;
	ReadOnlyArray_1_t644486DA5FB2B247FC64733F192E2894F46CFCF5  ___U3CparametersU3Ek__BackingField_1;
};

// UnityEngine.Pose
struct Pose_t9F30358E65733E60A1DC8682FDB7104F40C9434A 
{
public:
	// UnityEngine.Vector3 UnityEngine.Pose::position
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___position_0;
	// UnityEngine.Quaternion UnityEngine.Pose::rotation
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___rotation_1;

public:
	inline static int32_t get_offset_of_position_0() { return static_cast<int32_t>(offsetof(Pose_t9F30358E65733E60A1DC8682FDB7104F40C9434A, ___position_0)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_position_0() const { return ___position_0; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_position_0() { return &___position_0; }
	inline void set_position_0(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___position_0 = value;
	}

	inline static int32_t get_offset_of_rotation_1() { return static_cast<int32_t>(offsetof(Pose_t9F30358E65733E60A1DC8682FDB7104F40C9434A, ___rotation_1)); }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  get_rotation_1() const { return ___rotation_1; }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 * get_address_of_rotation_1() { return &___rotation_1; }
	inline void set_rotation_1(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  value)
	{
		___rotation_1 = value;
	}
};


// System.TypeCode
struct TypeCode_tCB39BAB5CFB7A1E0BCB521413E3C46B81C31AA7C 
{
public:
	// System.Int32 System.TypeCode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TypeCode_tCB39BAB5CFB7A1E0BCB521413E3C46B81C31AA7C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.InputSystem.Utilities.JsonParser/JsonString
struct JsonString_tB3A1938903F2377F780FF4C4FE7CC6EA81C0D301 
{
public:
	// UnityEngine.InputSystem.Utilities.Substring UnityEngine.InputSystem.Utilities.JsonParser/JsonString::text
	Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F  ___text_0;
	// System.Boolean UnityEngine.InputSystem.Utilities.JsonParser/JsonString::hasEscapes
	bool ___hasEscapes_1;

public:
	inline static int32_t get_offset_of_text_0() { return static_cast<int32_t>(offsetof(JsonString_tB3A1938903F2377F780FF4C4FE7CC6EA81C0D301, ___text_0)); }
	inline Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F  get_text_0() const { return ___text_0; }
	inline Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F * get_address_of_text_0() { return &___text_0; }
	inline void set_text_0(Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F  value)
	{
		___text_0 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___text_0))->___m_String_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_hasEscapes_1() { return static_cast<int32_t>(offsetof(JsonString_tB3A1938903F2377F780FF4C4FE7CC6EA81C0D301, ___hasEscapes_1)); }
	inline bool get_hasEscapes_1() const { return ___hasEscapes_1; }
	inline bool* get_address_of_hasEscapes_1() { return &___hasEscapes_1; }
	inline void set_hasEscapes_1(bool value)
	{
		___hasEscapes_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.InputSystem.Utilities.JsonParser/JsonString
struct JsonString_tB3A1938903F2377F780FF4C4FE7CC6EA81C0D301_marshaled_pinvoke
{
	Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F_marshaled_pinvoke ___text_0;
	int32_t ___hasEscapes_1;
};
// Native definition for COM marshalling of UnityEngine.InputSystem.Utilities.JsonParser/JsonString
struct JsonString_tB3A1938903F2377F780FF4C4FE7CC6EA81C0D301_marshaled_com
{
	Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F_marshaled_com ___text_0;
	int32_t ___hasEscapes_1;
};

// UnityEngine.InputSystem.Utilities.JsonParser/JsonValueType
struct JsonValueType_t536726C35383E9602EC7AE443D8CFC95C5F35540 
{
public:
	// System.Int32 UnityEngine.InputSystem.Utilities.JsonParser/JsonValueType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(JsonValueType_t536726C35383E9602EC7AE443D8CFC95C5F35540, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Collections.Generic.List`1/Enumerator<System.Int32Enum>
struct Enumerator_t3A873D53A96DA0AC1C030CB5A0BF2D0F57FCC984 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::list
	List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A * ___list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	int32_t ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_t3A873D53A96DA0AC1C030CB5A0BF2D0F57FCC984, ___list_0)); }
	inline List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A * get_list_0() const { return ___list_0; }
	inline List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_t3A873D53A96DA0AC1C030CB5A0BF2D0F57FCC984, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_t3A873D53A96DA0AC1C030CB5A0BF2D0F57FCC984, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t3A873D53A96DA0AC1C030CB5A0BF2D0F57FCC984, ___current_3)); }
	inline int32_t get_current_3() const { return ___current_3; }
	inline int32_t* get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(int32_t value)
	{
		___current_3 = value;
	}
};


// System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.NameAndParameters>
struct Enumerator_tA2E0F83E15EA46A2B4B08AFB750700C6E1387FC0 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::list
	List_1_tCBF5EB99F2E538DCB47A8C835DD4B9C024879393 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA  ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_tA2E0F83E15EA46A2B4B08AFB750700C6E1387FC0, ___list_0)); }
	inline List_1_tCBF5EB99F2E538DCB47A8C835DD4B9C024879393 * get_list_0() const { return ___list_0; }
	inline List_1_tCBF5EB99F2E538DCB47A8C835DD4B9C024879393 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_tCBF5EB99F2E538DCB47A8C835DD4B9C024879393 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_tA2E0F83E15EA46A2B4B08AFB750700C6E1387FC0, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_tA2E0F83E15EA46A2B4B08AFB750700C6E1387FC0, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_tA2E0F83E15EA46A2B4B08AFB750700C6E1387FC0, ___current_3)); }
	inline NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA  get_current_3() const { return ___current_3; }
	inline NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA  value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___current_3))->___U3CnameU3Ek__BackingField_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___current_3))->___U3CparametersU3Ek__BackingField_1))->___m_Array_0), (void*)NULL);
		#endif
	}
};


// System.Collections.Generic.List`1/Enumerator<UnityEngine.Pose>
struct Enumerator_tA7E1DE1C5B2833EE935A19CAF705B2C053EBC9AB 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::list
	List_1_tDB586987B7A2D2B13A903004C86E3C3FBD061C1C * ___list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	Pose_t9F30358E65733E60A1DC8682FDB7104F40C9434A  ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_tA7E1DE1C5B2833EE935A19CAF705B2C053EBC9AB, ___list_0)); }
	inline List_1_tDB586987B7A2D2B13A903004C86E3C3FBD061C1C * get_list_0() const { return ___list_0; }
	inline List_1_tDB586987B7A2D2B13A903004C86E3C3FBD061C1C ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_tDB586987B7A2D2B13A903004C86E3C3FBD061C1C * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_tA7E1DE1C5B2833EE935A19CAF705B2C053EBC9AB, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_tA7E1DE1C5B2833EE935A19CAF705B2C053EBC9AB, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_tA7E1DE1C5B2833EE935A19CAF705B2C053EBC9AB, ___current_3)); }
	inline Pose_t9F30358E65733E60A1DC8682FDB7104F40C9434A  get_current_3() const { return ___current_3; }
	inline Pose_t9F30358E65733E60A1DC8682FDB7104F40C9434A * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(Pose_t9F30358E65733E60A1DC8682FDB7104F40C9434A  value)
	{
		___current_3 = value;
	}
};


// System.Linq.Enumerable/WhereEnumerableIterator`1<UnityEngine.InputSystem.Utilities.InternedString>
struct WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B  : public Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereEnumerableIterator`1::source
	RuntimeObject* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereEnumerableIterator`1::predicate
	Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * ___predicate_4;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/WhereEnumerableIterator`1::enumerator
	RuntimeObject* ___enumerator_5;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B, ___source_3)); }
	inline RuntimeObject* get_source_3() const { return ___source_3; }
	inline RuntimeObject** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(RuntimeObject* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B, ___predicate_4)); }
	inline Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_5() { return static_cast<int32_t>(offsetof(WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B, ___enumerator_5)); }
	inline RuntimeObject* get_enumerator_5() const { return ___enumerator_5; }
	inline RuntimeObject** get_address_of_enumerator_5() { return &___enumerator_5; }
	inline void set_enumerator_5(RuntimeObject* value)
	{
		___enumerator_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumerator_5), (void*)value);
	}
};


// System.Linq.Enumerable/WhereEnumerableIterator`1<UnityEngine.Quaternion>
struct WhereEnumerableIterator_1_t6F3A191533D782BF1D9D4D3C5E1B535EC187EA9F  : public Iterator_1_t4BD4EE08E1E7E81949C3754EC2969154580F4C65
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereEnumerableIterator`1::source
	RuntimeObject* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereEnumerableIterator`1::predicate
	Func_2_t34B33A6CBEAEB1EF3A60338636A0DC782C33A7BE * ___predicate_4;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/WhereEnumerableIterator`1::enumerator
	RuntimeObject* ___enumerator_5;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereEnumerableIterator_1_t6F3A191533D782BF1D9D4D3C5E1B535EC187EA9F, ___source_3)); }
	inline RuntimeObject* get_source_3() const { return ___source_3; }
	inline RuntimeObject** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(RuntimeObject* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereEnumerableIterator_1_t6F3A191533D782BF1D9D4D3C5E1B535EC187EA9F, ___predicate_4)); }
	inline Func_2_t34B33A6CBEAEB1EF3A60338636A0DC782C33A7BE * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t34B33A6CBEAEB1EF3A60338636A0DC782C33A7BE ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t34B33A6CBEAEB1EF3A60338636A0DC782C33A7BE * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_5() { return static_cast<int32_t>(offsetof(WhereEnumerableIterator_1_t6F3A191533D782BF1D9D4D3C5E1B535EC187EA9F, ___enumerator_5)); }
	inline RuntimeObject* get_enumerator_5() const { return ___enumerator_5; }
	inline RuntimeObject** get_address_of_enumerator_5() { return &___enumerator_5; }
	inline void set_enumerator_5(RuntimeObject* value)
	{
		___enumerator_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumerator_5), (void*)value);
	}
};


// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectEnumerableIterator_2_t5171F478E1C976F4C353EA25EC64567E44C86CA0  : public Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::source
	RuntimeObject* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::predicate
	Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::selector
	Func_2_tB181F4C9A24C0634DF4A10CFBFFE6B45B3796BBC * ___selector_5;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::enumerator
	RuntimeObject* ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t5171F478E1C976F4C353EA25EC64567E44C86CA0, ___source_3)); }
	inline RuntimeObject* get_source_3() const { return ___source_3; }
	inline RuntimeObject** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(RuntimeObject* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t5171F478E1C976F4C353EA25EC64567E44C86CA0, ___predicate_4)); }
	inline Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t5171F478E1C976F4C353EA25EC64567E44C86CA0, ___selector_5)); }
	inline Func_2_tB181F4C9A24C0634DF4A10CFBFFE6B45B3796BBC * get_selector_5() const { return ___selector_5; }
	inline Func_2_tB181F4C9A24C0634DF4A10CFBFFE6B45B3796BBC ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_tB181F4C9A24C0634DF4A10CFBFFE6B45B3796BBC * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t5171F478E1C976F4C353EA25EC64567E44C86CA0, ___enumerator_6)); }
	inline RuntimeObject* get_enumerator_6() const { return ___enumerator_6; }
	inline RuntimeObject** get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(RuntimeObject* value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumerator_6), (void*)value);
	}
};


// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Int32Enum,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectEnumerableIterator_2_t4F5D6E88F86A03D2A74E02E6BC9FBD56A793C28A  : public Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::source
	RuntimeObject* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::predicate
	Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::selector
	Func_2_t043042489DBF19074B5B434F090F7B8AAE97E138 * ___selector_5;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::enumerator
	RuntimeObject* ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t4F5D6E88F86A03D2A74E02E6BC9FBD56A793C28A, ___source_3)); }
	inline RuntimeObject* get_source_3() const { return ___source_3; }
	inline RuntimeObject** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(RuntimeObject* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t4F5D6E88F86A03D2A74E02E6BC9FBD56A793C28A, ___predicate_4)); }
	inline Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t4F5D6E88F86A03D2A74E02E6BC9FBD56A793C28A, ___selector_5)); }
	inline Func_2_t043042489DBF19074B5B434F090F7B8AAE97E138 * get_selector_5() const { return ___selector_5; }
	inline Func_2_t043042489DBF19074B5B434F090F7B8AAE97E138 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t043042489DBF19074B5B434F090F7B8AAE97E138 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t4F5D6E88F86A03D2A74E02E6BC9FBD56A793C28A, ___enumerator_6)); }
	inline RuntimeObject* get_enumerator_6() const { return ___enumerator_6; }
	inline RuntimeObject** get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(RuntimeObject* value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumerator_6), (void*)value);
	}
};


// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.InternedString,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectEnumerableIterator_2_t0EB79FD57DE6939EBF78FA1D92312CB62DD3F2C6  : public Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::source
	RuntimeObject* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::predicate
	Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::selector
	Func_2_t1AF155D27A4D78882342D697E4A6DE7CE594545B * ___selector_5;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::enumerator
	RuntimeObject* ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t0EB79FD57DE6939EBF78FA1D92312CB62DD3F2C6, ___source_3)); }
	inline RuntimeObject* get_source_3() const { return ___source_3; }
	inline RuntimeObject** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(RuntimeObject* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t0EB79FD57DE6939EBF78FA1D92312CB62DD3F2C6, ___predicate_4)); }
	inline Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t0EB79FD57DE6939EBF78FA1D92312CB62DD3F2C6, ___selector_5)); }
	inline Func_2_t1AF155D27A4D78882342D697E4A6DE7CE594545B * get_selector_5() const { return ___selector_5; }
	inline Func_2_t1AF155D27A4D78882342D697E4A6DE7CE594545B ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t1AF155D27A4D78882342D697E4A6DE7CE594545B * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t0EB79FD57DE6939EBF78FA1D92312CB62DD3F2C6, ___enumerator_6)); }
	inline RuntimeObject* get_enumerator_6() const { return ___enumerator_6; }
	inline RuntimeObject** get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(RuntimeObject* value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumerator_6), (void*)value);
	}
};


// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectEnumerableIterator_2_t3B8EA5ED092FA87DEAECF94C466A54501B692B0B  : public Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::source
	RuntimeObject* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::predicate
	Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::selector
	Func_2_tEA2235809AB0E1AB8425745179691D76B519B0C5 * ___selector_5;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::enumerator
	RuntimeObject* ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t3B8EA5ED092FA87DEAECF94C466A54501B692B0B, ___source_3)); }
	inline RuntimeObject* get_source_3() const { return ___source_3; }
	inline RuntimeObject** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(RuntimeObject* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t3B8EA5ED092FA87DEAECF94C466A54501B692B0B, ___predicate_4)); }
	inline Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t3B8EA5ED092FA87DEAECF94C466A54501B692B0B, ___selector_5)); }
	inline Func_2_tEA2235809AB0E1AB8425745179691D76B519B0C5 * get_selector_5() const { return ___selector_5; }
	inline Func_2_tEA2235809AB0E1AB8425745179691D76B519B0C5 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_tEA2235809AB0E1AB8425745179691D76B519B0C5 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t3B8EA5ED092FA87DEAECF94C466A54501B692B0B, ___enumerator_6)); }
	inline RuntimeObject* get_enumerator_6() const { return ___enumerator_6; }
	inline RuntimeObject** get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(RuntimeObject* value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumerator_6), (void*)value);
	}
};


// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectEnumerableIterator_2_t382E0095C883AA01020D10E08F7F38BBA26A3EC5  : public Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::source
	RuntimeObject* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::predicate
	Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::selector
	Func_2_tA21678F1D9F9F4617D645755285D28EDE5F9AE44 * ___selector_5;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::enumerator
	RuntimeObject* ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t382E0095C883AA01020D10E08F7F38BBA26A3EC5, ___source_3)); }
	inline RuntimeObject* get_source_3() const { return ___source_3; }
	inline RuntimeObject** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(RuntimeObject* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t382E0095C883AA01020D10E08F7F38BBA26A3EC5, ___predicate_4)); }
	inline Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t382E0095C883AA01020D10E08F7F38BBA26A3EC5, ___selector_5)); }
	inline Func_2_tA21678F1D9F9F4617D645755285D28EDE5F9AE44 * get_selector_5() const { return ___selector_5; }
	inline Func_2_tA21678F1D9F9F4617D645755285D28EDE5F9AE44 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_tA21678F1D9F9F4617D645755285D28EDE5F9AE44 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t382E0095C883AA01020D10E08F7F38BBA26A3EC5, ___enumerator_6)); }
	inline RuntimeObject* get_enumerator_6() const { return ___enumerator_6; }
	inline RuntimeObject** get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(RuntimeObject* value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumerator_6), (void*)value);
	}
};


// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Object,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectEnumerableIterator_2_tE487654732A8A2F1E3A2D57412C08036EE861F38  : public Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::source
	RuntimeObject* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::predicate
	Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::selector
	Func_2_t964229B6ADD2B008268547ED2BC3C090AF218065 * ___selector_5;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::enumerator
	RuntimeObject* ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tE487654732A8A2F1E3A2D57412C08036EE861F38, ___source_3)); }
	inline RuntimeObject* get_source_3() const { return ___source_3; }
	inline RuntimeObject** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(RuntimeObject* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tE487654732A8A2F1E3A2D57412C08036EE861F38, ___predicate_4)); }
	inline Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tE487654732A8A2F1E3A2D57412C08036EE861F38, ___selector_5)); }
	inline Func_2_t964229B6ADD2B008268547ED2BC3C090AF218065 * get_selector_5() const { return ___selector_5; }
	inline Func_2_t964229B6ADD2B008268547ED2BC3C090AF218065 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t964229B6ADD2B008268547ED2BC3C090AF218065 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tE487654732A8A2F1E3A2D57412C08036EE861F38, ___enumerator_6)); }
	inline RuntimeObject* get_enumerator_6() const { return ___enumerator_6; }
	inline RuntimeObject** get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(RuntimeObject* value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumerator_6), (void*)value);
	}
};


// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.Pose,UnityEngine.Quaternion>
struct WhereSelectEnumerableIterator_2_t3529F4FEC1C961CCD3F23682094813DA27D0ADA3  : public Iterator_1_t4BD4EE08E1E7E81949C3754EC2969154580F4C65
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::source
	RuntimeObject* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::predicate
	Func_2_t288BC2D8C12E52B8F2E7F675CFB740C44E7E813F * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::selector
	Func_2_t6170D516A1F6F9B6BE1BDC5379D0A5CC320AF0F8 * ___selector_5;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::enumerator
	RuntimeObject* ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t3529F4FEC1C961CCD3F23682094813DA27D0ADA3, ___source_3)); }
	inline RuntimeObject* get_source_3() const { return ___source_3; }
	inline RuntimeObject** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(RuntimeObject* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t3529F4FEC1C961CCD3F23682094813DA27D0ADA3, ___predicate_4)); }
	inline Func_2_t288BC2D8C12E52B8F2E7F675CFB740C44E7E813F * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t288BC2D8C12E52B8F2E7F675CFB740C44E7E813F ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t288BC2D8C12E52B8F2E7F675CFB740C44E7E813F * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t3529F4FEC1C961CCD3F23682094813DA27D0ADA3, ___selector_5)); }
	inline Func_2_t6170D516A1F6F9B6BE1BDC5379D0A5CC320AF0F8 * get_selector_5() const { return ___selector_5; }
	inline Func_2_t6170D516A1F6F9B6BE1BDC5379D0A5CC320AF0F8 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t6170D516A1F6F9B6BE1BDC5379D0A5CC320AF0F8 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t3529F4FEC1C961CCD3F23682094813DA27D0ADA3, ___enumerator_6)); }
	inline RuntimeObject* get_enumerator_6() const { return ___enumerator_6; }
	inline RuntimeObject** get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(RuntimeObject* value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumerator_6), (void*)value);
	}
};


// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Single,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectEnumerableIterator_2_tB68F25B00798F106AD2C511CF10D0289252107C8  : public Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::source
	RuntimeObject* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::predicate
	Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::selector
	Func_2_t4E06D96B2978B60DA267963F96583CE3BF82EA84 * ___selector_5;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::enumerator
	RuntimeObject* ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tB68F25B00798F106AD2C511CF10D0289252107C8, ___source_3)); }
	inline RuntimeObject* get_source_3() const { return ___source_3; }
	inline RuntimeObject** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(RuntimeObject* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tB68F25B00798F106AD2C511CF10D0289252107C8, ___predicate_4)); }
	inline Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tB68F25B00798F106AD2C511CF10D0289252107C8, ___selector_5)); }
	inline Func_2_t4E06D96B2978B60DA267963F96583CE3BF82EA84 * get_selector_5() const { return ___selector_5; }
	inline Func_2_t4E06D96B2978B60DA267963F96583CE3BF82EA84 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t4E06D96B2978B60DA267963F96583CE3BF82EA84 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tB68F25B00798F106AD2C511CF10D0289252107C8, ___enumerator_6)); }
	inline RuntimeObject* get_enumerator_6() const { return ___enumerator_6; }
	inline RuntimeObject** get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(RuntimeObject* value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumerator_6), (void*)value);
	}
};


// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.Substring,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectEnumerableIterator_2_tFDFC244728E14BAC93BE9869A64080C38A0B7F1D  : public Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::source
	RuntimeObject* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::predicate
	Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::selector
	Func_2_t14F6F4D7FBF558A20782EDC48D80473E4800048E * ___selector_5;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::enumerator
	RuntimeObject* ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tFDFC244728E14BAC93BE9869A64080C38A0B7F1D, ___source_3)); }
	inline RuntimeObject* get_source_3() const { return ___source_3; }
	inline RuntimeObject** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(RuntimeObject* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tFDFC244728E14BAC93BE9869A64080C38A0B7F1D, ___predicate_4)); }
	inline Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tFDFC244728E14BAC93BE9869A64080C38A0B7F1D, ___selector_5)); }
	inline Func_2_t14F6F4D7FBF558A20782EDC48D80473E4800048E * get_selector_5() const { return ___selector_5; }
	inline Func_2_t14F6F4D7FBF558A20782EDC48D80473E4800048E ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t14F6F4D7FBF558A20782EDC48D80473E4800048E * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tFDFC244728E14BAC93BE9869A64080C38A0B7F1D, ___enumerator_6)); }
	inline RuntimeObject* get_enumerator_6() const { return ___enumerator_6; }
	inline RuntimeObject** get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(RuntimeObject* value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumerator_6), (void*)value);
	}
};


// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectEnumerableIterator_2_t6D595CFCE85C8B8D77C138F46CCF2608E20474FE  : public Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::source
	RuntimeObject* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::predicate
	Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::selector
	Func_2_t48DD1C8E439811C492BAFE220A679DB57D7C9370 * ___selector_5;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::enumerator
	RuntimeObject* ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t6D595CFCE85C8B8D77C138F46CCF2608E20474FE, ___source_3)); }
	inline RuntimeObject* get_source_3() const { return ___source_3; }
	inline RuntimeObject** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(RuntimeObject* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t6D595CFCE85C8B8D77C138F46CCF2608E20474FE, ___predicate_4)); }
	inline Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t6D595CFCE85C8B8D77C138F46CCF2608E20474FE, ___selector_5)); }
	inline Func_2_t48DD1C8E439811C492BAFE220A679DB57D7C9370 * get_selector_5() const { return ___selector_5; }
	inline Func_2_t48DD1C8E439811C492BAFE220A679DB57D7C9370 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t48DD1C8E439811C492BAFE220A679DB57D7C9370 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t6D595CFCE85C8B8D77C138F46CCF2608E20474FE, ___enumerator_6)); }
	inline RuntimeObject* get_enumerator_6() const { return ___enumerator_6; }
	inline RuntimeObject** get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(RuntimeObject* value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumerator_6), (void*)value);
	}
};


// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.InternedString,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectListIterator_2_t7BDF9DF4ED7F42E4D8139C4BA35A0F055024799E  : public Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::source
	List_1_tFD37D052760A236906215AF581C58EE13DE6A5DC * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectListIterator`2::predicate
	Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectListIterator`2::selector
	Func_2_t1AF155D27A4D78882342D697E4A6DE7CE594545B * ___selector_5;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::enumerator
	Enumerator_tD1ECE103520161C1BD172331D6968D3D184B3B6F  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t7BDF9DF4ED7F42E4D8139C4BA35A0F055024799E, ___source_3)); }
	inline List_1_tFD37D052760A236906215AF581C58EE13DE6A5DC * get_source_3() const { return ___source_3; }
	inline List_1_tFD37D052760A236906215AF581C58EE13DE6A5DC ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_tFD37D052760A236906215AF581C58EE13DE6A5DC * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t7BDF9DF4ED7F42E4D8139C4BA35A0F055024799E, ___predicate_4)); }
	inline Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t7BDF9DF4ED7F42E4D8139C4BA35A0F055024799E, ___selector_5)); }
	inline Func_2_t1AF155D27A4D78882342D697E4A6DE7CE594545B * get_selector_5() const { return ___selector_5; }
	inline Func_2_t1AF155D27A4D78882342D697E4A6DE7CE594545B ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t1AF155D27A4D78882342D697E4A6DE7CE594545B * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t7BDF9DF4ED7F42E4D8139C4BA35A0F055024799E, ___enumerator_6)); }
	inline Enumerator_tD1ECE103520161C1BD172331D6968D3D184B3B6F  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_tD1ECE103520161C1BD172331D6968D3D184B3B6F * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_tD1ECE103520161C1BD172331D6968D3D184B3B6F  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___m_StringOriginalCase_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___m_StringLowerCase_1), (void*)NULL);
		#endif
	}
};


// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.InternedString,System.Object>
struct WhereSelectListIterator_2_t1C93CE1377887193667292BEED953C31167E2F75  : public Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::source
	List_1_tFD37D052760A236906215AF581C58EE13DE6A5DC * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectListIterator`2::predicate
	Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectListIterator`2::selector
	Func_2_tA2BD34BB8D97AD282016BD4CB5B8675E4AF85B45 * ___selector_5;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::enumerator
	Enumerator_tD1ECE103520161C1BD172331D6968D3D184B3B6F  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t1C93CE1377887193667292BEED953C31167E2F75, ___source_3)); }
	inline List_1_tFD37D052760A236906215AF581C58EE13DE6A5DC * get_source_3() const { return ___source_3; }
	inline List_1_tFD37D052760A236906215AF581C58EE13DE6A5DC ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_tFD37D052760A236906215AF581C58EE13DE6A5DC * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t1C93CE1377887193667292BEED953C31167E2F75, ___predicate_4)); }
	inline Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t1C93CE1377887193667292BEED953C31167E2F75, ___selector_5)); }
	inline Func_2_tA2BD34BB8D97AD282016BD4CB5B8675E4AF85B45 * get_selector_5() const { return ___selector_5; }
	inline Func_2_tA2BD34BB8D97AD282016BD4CB5B8675E4AF85B45 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_tA2BD34BB8D97AD282016BD4CB5B8675E4AF85B45 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t1C93CE1377887193667292BEED953C31167E2F75, ___enumerator_6)); }
	inline Enumerator_tD1ECE103520161C1BD172331D6968D3D184B3B6F  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_tD1ECE103520161C1BD172331D6968D3D184B3B6F * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_tD1ECE103520161C1BD172331D6968D3D184B3B6F  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___m_StringOriginalCase_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___m_StringLowerCase_1), (void*)NULL);
		#endif
	}
};


// System.Linq.Enumerable/WhereSelectListIterator`2<System.Object,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectListIterator_2_t4F7AA31B4289096C5D625263B8BBDA051331BE94  : public Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::source
	List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectListIterator`2::predicate
	Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectListIterator`2::selector
	Func_2_t964229B6ADD2B008268547ED2BC3C090AF218065 * ___selector_5;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::enumerator
	Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t4F7AA31B4289096C5D625263B8BBDA051331BE94, ___source_3)); }
	inline List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * get_source_3() const { return ___source_3; }
	inline List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t4F7AA31B4289096C5D625263B8BBDA051331BE94, ___predicate_4)); }
	inline Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t4F7AA31B4289096C5D625263B8BBDA051331BE94, ___selector_5)); }
	inline Func_2_t964229B6ADD2B008268547ED2BC3C090AF218065 * get_selector_5() const { return ___selector_5; }
	inline Func_2_t964229B6ADD2B008268547ED2BC3C090AF218065 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t964229B6ADD2B008268547ED2BC3C090AF218065 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t4F7AA31B4289096C5D625263B8BBDA051331BE94, ___enumerator_6)); }
	inline Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___current_3), (void*)NULL);
		#endif
	}
};


// System.Linq.Enumerable/WhereSelectListIterator`2<System.Single,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectListIterator_2_t75E7B369D9A91D962C4DAFB13EE84B0CCE07D9E3  : public Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::source
	List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectListIterator`2::predicate
	Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectListIterator`2::selector
	Func_2_t4E06D96B2978B60DA267963F96583CE3BF82EA84 * ___selector_5;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::enumerator
	Enumerator_tC1FD01C8BB5327D442D71FD022B4338ACD701783  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t75E7B369D9A91D962C4DAFB13EE84B0CCE07D9E3, ___source_3)); }
	inline List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA * get_source_3() const { return ___source_3; }
	inline List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t75E7B369D9A91D962C4DAFB13EE84B0CCE07D9E3, ___predicate_4)); }
	inline Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t75E7B369D9A91D962C4DAFB13EE84B0CCE07D9E3, ___selector_5)); }
	inline Func_2_t4E06D96B2978B60DA267963F96583CE3BF82EA84 * get_selector_5() const { return ___selector_5; }
	inline Func_2_t4E06D96B2978B60DA267963F96583CE3BF82EA84 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t4E06D96B2978B60DA267963F96583CE3BF82EA84 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t75E7B369D9A91D962C4DAFB13EE84B0CCE07D9E3, ___enumerator_6)); }
	inline Enumerator_tC1FD01C8BB5327D442D71FD022B4338ACD701783  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_tC1FD01C8BB5327D442D71FD022B4338ACD701783 * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_tC1FD01C8BB5327D442D71FD022B4338ACD701783  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
	}
};


// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.Substring,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectListIterator_2_t59101674E6772D817227A6D5C222AF34A4E0BBAD  : public Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::source
	List_1_tEAD6E3282E028927B32F56E7892994D90D512467 * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectListIterator`2::predicate
	Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectListIterator`2::selector
	Func_2_t14F6F4D7FBF558A20782EDC48D80473E4800048E * ___selector_5;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::enumerator
	Enumerator_t7C514ACB3FF8E3EFB210F5F45C9F18A715BB4F80  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t59101674E6772D817227A6D5C222AF34A4E0BBAD, ___source_3)); }
	inline List_1_tEAD6E3282E028927B32F56E7892994D90D512467 * get_source_3() const { return ___source_3; }
	inline List_1_tEAD6E3282E028927B32F56E7892994D90D512467 ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_tEAD6E3282E028927B32F56E7892994D90D512467 * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t59101674E6772D817227A6D5C222AF34A4E0BBAD, ___predicate_4)); }
	inline Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t59101674E6772D817227A6D5C222AF34A4E0BBAD, ___selector_5)); }
	inline Func_2_t14F6F4D7FBF558A20782EDC48D80473E4800048E * get_selector_5() const { return ___selector_5; }
	inline Func_2_t14F6F4D7FBF558A20782EDC48D80473E4800048E ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t14F6F4D7FBF558A20782EDC48D80473E4800048E * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t59101674E6772D817227A6D5C222AF34A4E0BBAD, ___enumerator_6)); }
	inline Enumerator_t7C514ACB3FF8E3EFB210F5F45C9F18A715BB4F80  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_t7C514ACB3FF8E3EFB210F5F45C9F18A715BB4F80 * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_t7C514ACB3FF8E3EFB210F5F45C9F18A715BB4F80  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___m_String_0), (void*)NULL);
		#endif
	}
};


// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.Substring,System.Object>
struct WhereSelectListIterator_2_t0CEFF5458EED043DC70F7CC9B97B5AB4D45324EB  : public Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::source
	List_1_tEAD6E3282E028927B32F56E7892994D90D512467 * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectListIterator`2::predicate
	Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectListIterator`2::selector
	Func_2_t4EC2F095EDDC8079537CA0FEDC463635E599C9E4 * ___selector_5;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::enumerator
	Enumerator_t7C514ACB3FF8E3EFB210F5F45C9F18A715BB4F80  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t0CEFF5458EED043DC70F7CC9B97B5AB4D45324EB, ___source_3)); }
	inline List_1_tEAD6E3282E028927B32F56E7892994D90D512467 * get_source_3() const { return ___source_3; }
	inline List_1_tEAD6E3282E028927B32F56E7892994D90D512467 ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_tEAD6E3282E028927B32F56E7892994D90D512467 * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t0CEFF5458EED043DC70F7CC9B97B5AB4D45324EB, ___predicate_4)); }
	inline Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t0CEFF5458EED043DC70F7CC9B97B5AB4D45324EB, ___selector_5)); }
	inline Func_2_t4EC2F095EDDC8079537CA0FEDC463635E599C9E4 * get_selector_5() const { return ___selector_5; }
	inline Func_2_t4EC2F095EDDC8079537CA0FEDC463635E599C9E4 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t4EC2F095EDDC8079537CA0FEDC463635E599C9E4 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t0CEFF5458EED043DC70F7CC9B97B5AB4D45324EB, ___enumerator_6)); }
	inline Enumerator_t7C514ACB3FF8E3EFB210F5F45C9F18A715BB4F80  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_t7C514ACB3FF8E3EFB210F5F45C9F18A715BB4F80 * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_t7C514ACB3FF8E3EFB210F5F45C9F18A715BB4F80  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___m_String_0), (void*)NULL);
		#endif
	}
};


// Oculus.Interaction.Input.HandSkeletonJoint
struct HandSkeletonJoint_t5B4E2A23FDAC8BC4C8751E5FBC7C96A1056582F3 
{
public:
	// System.Int32 Oculus.Interaction.Input.HandSkeletonJoint::parent
	int32_t ___parent_0;
	// UnityEngine.Pose Oculus.Interaction.Input.HandSkeletonJoint::pose
	Pose_t9F30358E65733E60A1DC8682FDB7104F40C9434A  ___pose_1;
	// System.Single Oculus.Interaction.Input.HandSkeletonJoint::radius
	float ___radius_2;

public:
	inline static int32_t get_offset_of_parent_0() { return static_cast<int32_t>(offsetof(HandSkeletonJoint_t5B4E2A23FDAC8BC4C8751E5FBC7C96A1056582F3, ___parent_0)); }
	inline int32_t get_parent_0() const { return ___parent_0; }
	inline int32_t* get_address_of_parent_0() { return &___parent_0; }
	inline void set_parent_0(int32_t value)
	{
		___parent_0 = value;
	}

	inline static int32_t get_offset_of_pose_1() { return static_cast<int32_t>(offsetof(HandSkeletonJoint_t5B4E2A23FDAC8BC4C8751E5FBC7C96A1056582F3, ___pose_1)); }
	inline Pose_t9F30358E65733E60A1DC8682FDB7104F40C9434A  get_pose_1() const { return ___pose_1; }
	inline Pose_t9F30358E65733E60A1DC8682FDB7104F40C9434A * get_address_of_pose_1() { return &___pose_1; }
	inline void set_pose_1(Pose_t9F30358E65733E60A1DC8682FDB7104F40C9434A  value)
	{
		___pose_1 = value;
	}

	inline static int32_t get_offset_of_radius_2() { return static_cast<int32_t>(offsetof(HandSkeletonJoint_t5B4E2A23FDAC8BC4C8751E5FBC7C96A1056582F3, ___radius_2)); }
	inline float get_radius_2() const { return ___radius_2; }
	inline float* get_address_of_radius_2() { return &___radius_2; }
	inline void set_radius_2(float value)
	{
		___radius_2 = value;
	}
};


// System.MulticastDelegate
struct MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___delegates_11), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};

// UnityEngine.InputSystem.Utilities.PrimitiveValue
struct PrimitiveValue_t70D8365A1CB2217C466D27D6820362EF4331A0C8 
{
public:
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.TypeCode UnityEngine.InputSystem.Utilities.PrimitiveValue::m_Type
			int32_t ___m_Type_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___m_Type_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_BoolValue_1_OffsetPadding[4];
			// System.Boolean UnityEngine.InputSystem.Utilities.PrimitiveValue::m_BoolValue
			bool ___m_BoolValue_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_BoolValue_1_OffsetPadding_forAlignmentOnly[4];
			bool ___m_BoolValue_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_CharValue_2_OffsetPadding[4];
			// System.Char UnityEngine.InputSystem.Utilities.PrimitiveValue::m_CharValue
			Il2CppChar ___m_CharValue_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_CharValue_2_OffsetPadding_forAlignmentOnly[4];
			Il2CppChar ___m_CharValue_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_ByteValue_3_OffsetPadding[4];
			// System.Byte UnityEngine.InputSystem.Utilities.PrimitiveValue::m_ByteValue
			uint8_t ___m_ByteValue_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_ByteValue_3_OffsetPadding_forAlignmentOnly[4];
			uint8_t ___m_ByteValue_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_SByteValue_4_OffsetPadding[4];
			// System.SByte UnityEngine.InputSystem.Utilities.PrimitiveValue::m_SByteValue
			int8_t ___m_SByteValue_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_SByteValue_4_OffsetPadding_forAlignmentOnly[4];
			int8_t ___m_SByteValue_4_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_ShortValue_5_OffsetPadding[4];
			// System.Int16 UnityEngine.InputSystem.Utilities.PrimitiveValue::m_ShortValue
			int16_t ___m_ShortValue_5;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_ShortValue_5_OffsetPadding_forAlignmentOnly[4];
			int16_t ___m_ShortValue_5_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_UShortValue_6_OffsetPadding[4];
			// System.UInt16 UnityEngine.InputSystem.Utilities.PrimitiveValue::m_UShortValue
			uint16_t ___m_UShortValue_6;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_UShortValue_6_OffsetPadding_forAlignmentOnly[4];
			uint16_t ___m_UShortValue_6_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_IntValue_7_OffsetPadding[4];
			// System.Int32 UnityEngine.InputSystem.Utilities.PrimitiveValue::m_IntValue
			int32_t ___m_IntValue_7;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_IntValue_7_OffsetPadding_forAlignmentOnly[4];
			int32_t ___m_IntValue_7_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_UIntValue_8_OffsetPadding[4];
			// System.UInt32 UnityEngine.InputSystem.Utilities.PrimitiveValue::m_UIntValue
			uint32_t ___m_UIntValue_8;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_UIntValue_8_OffsetPadding_forAlignmentOnly[4];
			uint32_t ___m_UIntValue_8_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_LongValue_9_OffsetPadding[4];
			// System.Int64 UnityEngine.InputSystem.Utilities.PrimitiveValue::m_LongValue
			int64_t ___m_LongValue_9;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_LongValue_9_OffsetPadding_forAlignmentOnly[4];
			int64_t ___m_LongValue_9_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_ULongValue_10_OffsetPadding[4];
			// System.UInt64 UnityEngine.InputSystem.Utilities.PrimitiveValue::m_ULongValue
			uint64_t ___m_ULongValue_10;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_ULongValue_10_OffsetPadding_forAlignmentOnly[4];
			uint64_t ___m_ULongValue_10_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_FloatValue_11_OffsetPadding[4];
			// System.Single UnityEngine.InputSystem.Utilities.PrimitiveValue::m_FloatValue
			float ___m_FloatValue_11;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_FloatValue_11_OffsetPadding_forAlignmentOnly[4];
			float ___m_FloatValue_11_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_DoubleValue_12_OffsetPadding[4];
			// System.Double UnityEngine.InputSystem.Utilities.PrimitiveValue::m_DoubleValue
			double ___m_DoubleValue_12;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_DoubleValue_12_OffsetPadding_forAlignmentOnly[4];
			double ___m_DoubleValue_12_forAlignmentOnly;
		};
	};

public:
	inline static int32_t get_offset_of_m_Type_0() { return static_cast<int32_t>(offsetof(PrimitiveValue_t70D8365A1CB2217C466D27D6820362EF4331A0C8, ___m_Type_0)); }
	inline int32_t get_m_Type_0() const { return ___m_Type_0; }
	inline int32_t* get_address_of_m_Type_0() { return &___m_Type_0; }
	inline void set_m_Type_0(int32_t value)
	{
		___m_Type_0 = value;
	}

	inline static int32_t get_offset_of_m_BoolValue_1() { return static_cast<int32_t>(offsetof(PrimitiveValue_t70D8365A1CB2217C466D27D6820362EF4331A0C8, ___m_BoolValue_1)); }
	inline bool get_m_BoolValue_1() const { return ___m_BoolValue_1; }
	inline bool* get_address_of_m_BoolValue_1() { return &___m_BoolValue_1; }
	inline void set_m_BoolValue_1(bool value)
	{
		___m_BoolValue_1 = value;
	}

	inline static int32_t get_offset_of_m_CharValue_2() { return static_cast<int32_t>(offsetof(PrimitiveValue_t70D8365A1CB2217C466D27D6820362EF4331A0C8, ___m_CharValue_2)); }
	inline Il2CppChar get_m_CharValue_2() const { return ___m_CharValue_2; }
	inline Il2CppChar* get_address_of_m_CharValue_2() { return &___m_CharValue_2; }
	inline void set_m_CharValue_2(Il2CppChar value)
	{
		___m_CharValue_2 = value;
	}

	inline static int32_t get_offset_of_m_ByteValue_3() { return static_cast<int32_t>(offsetof(PrimitiveValue_t70D8365A1CB2217C466D27D6820362EF4331A0C8, ___m_ByteValue_3)); }
	inline uint8_t get_m_ByteValue_3() const { return ___m_ByteValue_3; }
	inline uint8_t* get_address_of_m_ByteValue_3() { return &___m_ByteValue_3; }
	inline void set_m_ByteValue_3(uint8_t value)
	{
		___m_ByteValue_3 = value;
	}

	inline static int32_t get_offset_of_m_SByteValue_4() { return static_cast<int32_t>(offsetof(PrimitiveValue_t70D8365A1CB2217C466D27D6820362EF4331A0C8, ___m_SByteValue_4)); }
	inline int8_t get_m_SByteValue_4() const { return ___m_SByteValue_4; }
	inline int8_t* get_address_of_m_SByteValue_4() { return &___m_SByteValue_4; }
	inline void set_m_SByteValue_4(int8_t value)
	{
		___m_SByteValue_4 = value;
	}

	inline static int32_t get_offset_of_m_ShortValue_5() { return static_cast<int32_t>(offsetof(PrimitiveValue_t70D8365A1CB2217C466D27D6820362EF4331A0C8, ___m_ShortValue_5)); }
	inline int16_t get_m_ShortValue_5() const { return ___m_ShortValue_5; }
	inline int16_t* get_address_of_m_ShortValue_5() { return &___m_ShortValue_5; }
	inline void set_m_ShortValue_5(int16_t value)
	{
		___m_ShortValue_5 = value;
	}

	inline static int32_t get_offset_of_m_UShortValue_6() { return static_cast<int32_t>(offsetof(PrimitiveValue_t70D8365A1CB2217C466D27D6820362EF4331A0C8, ___m_UShortValue_6)); }
	inline uint16_t get_m_UShortValue_6() const { return ___m_UShortValue_6; }
	inline uint16_t* get_address_of_m_UShortValue_6() { return &___m_UShortValue_6; }
	inline void set_m_UShortValue_6(uint16_t value)
	{
		___m_UShortValue_6 = value;
	}

	inline static int32_t get_offset_of_m_IntValue_7() { return static_cast<int32_t>(offsetof(PrimitiveValue_t70D8365A1CB2217C466D27D6820362EF4331A0C8, ___m_IntValue_7)); }
	inline int32_t get_m_IntValue_7() const { return ___m_IntValue_7; }
	inline int32_t* get_address_of_m_IntValue_7() { return &___m_IntValue_7; }
	inline void set_m_IntValue_7(int32_t value)
	{
		___m_IntValue_7 = value;
	}

	inline static int32_t get_offset_of_m_UIntValue_8() { return static_cast<int32_t>(offsetof(PrimitiveValue_t70D8365A1CB2217C466D27D6820362EF4331A0C8, ___m_UIntValue_8)); }
	inline uint32_t get_m_UIntValue_8() const { return ___m_UIntValue_8; }
	inline uint32_t* get_address_of_m_UIntValue_8() { return &___m_UIntValue_8; }
	inline void set_m_UIntValue_8(uint32_t value)
	{
		___m_UIntValue_8 = value;
	}

	inline static int32_t get_offset_of_m_LongValue_9() { return static_cast<int32_t>(offsetof(PrimitiveValue_t70D8365A1CB2217C466D27D6820362EF4331A0C8, ___m_LongValue_9)); }
	inline int64_t get_m_LongValue_9() const { return ___m_LongValue_9; }
	inline int64_t* get_address_of_m_LongValue_9() { return &___m_LongValue_9; }
	inline void set_m_LongValue_9(int64_t value)
	{
		___m_LongValue_9 = value;
	}

	inline static int32_t get_offset_of_m_ULongValue_10() { return static_cast<int32_t>(offsetof(PrimitiveValue_t70D8365A1CB2217C466D27D6820362EF4331A0C8, ___m_ULongValue_10)); }
	inline uint64_t get_m_ULongValue_10() const { return ___m_ULongValue_10; }
	inline uint64_t* get_address_of_m_ULongValue_10() { return &___m_ULongValue_10; }
	inline void set_m_ULongValue_10(uint64_t value)
	{
		___m_ULongValue_10 = value;
	}

	inline static int32_t get_offset_of_m_FloatValue_11() { return static_cast<int32_t>(offsetof(PrimitiveValue_t70D8365A1CB2217C466D27D6820362EF4331A0C8, ___m_FloatValue_11)); }
	inline float get_m_FloatValue_11() const { return ___m_FloatValue_11; }
	inline float* get_address_of_m_FloatValue_11() { return &___m_FloatValue_11; }
	inline void set_m_FloatValue_11(float value)
	{
		___m_FloatValue_11 = value;
	}

	inline static int32_t get_offset_of_m_DoubleValue_12() { return static_cast<int32_t>(offsetof(PrimitiveValue_t70D8365A1CB2217C466D27D6820362EF4331A0C8, ___m_DoubleValue_12)); }
	inline double get_m_DoubleValue_12() const { return ___m_DoubleValue_12; }
	inline double* get_address_of_m_DoubleValue_12() { return &___m_DoubleValue_12; }
	inline void set_m_DoubleValue_12(double value)
	{
		___m_DoubleValue_12 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.InputSystem.Utilities.PrimitiveValue
struct PrimitiveValue_t70D8365A1CB2217C466D27D6820362EF4331A0C8_marshaled_pinvoke
{
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			int32_t ___m_Type_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___m_Type_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_BoolValue_1_OffsetPadding[4];
			int32_t ___m_BoolValue_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_BoolValue_1_OffsetPadding_forAlignmentOnly[4];
			int32_t ___m_BoolValue_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_CharValue_2_OffsetPadding[4];
			uint8_t ___m_CharValue_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_CharValue_2_OffsetPadding_forAlignmentOnly[4];
			uint8_t ___m_CharValue_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_ByteValue_3_OffsetPadding[4];
			uint8_t ___m_ByteValue_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_ByteValue_3_OffsetPadding_forAlignmentOnly[4];
			uint8_t ___m_ByteValue_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_SByteValue_4_OffsetPadding[4];
			int8_t ___m_SByteValue_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_SByteValue_4_OffsetPadding_forAlignmentOnly[4];
			int8_t ___m_SByteValue_4_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_ShortValue_5_OffsetPadding[4];
			int16_t ___m_ShortValue_5;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_ShortValue_5_OffsetPadding_forAlignmentOnly[4];
			int16_t ___m_ShortValue_5_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_UShortValue_6_OffsetPadding[4];
			uint16_t ___m_UShortValue_6;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_UShortValue_6_OffsetPadding_forAlignmentOnly[4];
			uint16_t ___m_UShortValue_6_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_IntValue_7_OffsetPadding[4];
			int32_t ___m_IntValue_7;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_IntValue_7_OffsetPadding_forAlignmentOnly[4];
			int32_t ___m_IntValue_7_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_UIntValue_8_OffsetPadding[4];
			uint32_t ___m_UIntValue_8;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_UIntValue_8_OffsetPadding_forAlignmentOnly[4];
			uint32_t ___m_UIntValue_8_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_LongValue_9_OffsetPadding[4];
			int64_t ___m_LongValue_9;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_LongValue_9_OffsetPadding_forAlignmentOnly[4];
			int64_t ___m_LongValue_9_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_ULongValue_10_OffsetPadding[4];
			uint64_t ___m_ULongValue_10;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_ULongValue_10_OffsetPadding_forAlignmentOnly[4];
			uint64_t ___m_ULongValue_10_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_FloatValue_11_OffsetPadding[4];
			float ___m_FloatValue_11;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_FloatValue_11_OffsetPadding_forAlignmentOnly[4];
			float ___m_FloatValue_11_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_DoubleValue_12_OffsetPadding[4];
			double ___m_DoubleValue_12;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_DoubleValue_12_OffsetPadding_forAlignmentOnly[4];
			double ___m_DoubleValue_12_forAlignmentOnly;
		};
	};
};
// Native definition for COM marshalling of UnityEngine.InputSystem.Utilities.PrimitiveValue
struct PrimitiveValue_t70D8365A1CB2217C466D27D6820362EF4331A0C8_marshaled_com
{
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			int32_t ___m_Type_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___m_Type_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_BoolValue_1_OffsetPadding[4];
			int32_t ___m_BoolValue_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_BoolValue_1_OffsetPadding_forAlignmentOnly[4];
			int32_t ___m_BoolValue_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_CharValue_2_OffsetPadding[4];
			uint8_t ___m_CharValue_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_CharValue_2_OffsetPadding_forAlignmentOnly[4];
			uint8_t ___m_CharValue_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_ByteValue_3_OffsetPadding[4];
			uint8_t ___m_ByteValue_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_ByteValue_3_OffsetPadding_forAlignmentOnly[4];
			uint8_t ___m_ByteValue_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_SByteValue_4_OffsetPadding[4];
			int8_t ___m_SByteValue_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_SByteValue_4_OffsetPadding_forAlignmentOnly[4];
			int8_t ___m_SByteValue_4_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_ShortValue_5_OffsetPadding[4];
			int16_t ___m_ShortValue_5;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_ShortValue_5_OffsetPadding_forAlignmentOnly[4];
			int16_t ___m_ShortValue_5_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_UShortValue_6_OffsetPadding[4];
			uint16_t ___m_UShortValue_6;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_UShortValue_6_OffsetPadding_forAlignmentOnly[4];
			uint16_t ___m_UShortValue_6_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_IntValue_7_OffsetPadding[4];
			int32_t ___m_IntValue_7;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_IntValue_7_OffsetPadding_forAlignmentOnly[4];
			int32_t ___m_IntValue_7_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_UIntValue_8_OffsetPadding[4];
			uint32_t ___m_UIntValue_8;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_UIntValue_8_OffsetPadding_forAlignmentOnly[4];
			uint32_t ___m_UIntValue_8_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_LongValue_9_OffsetPadding[4];
			int64_t ___m_LongValue_9;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_LongValue_9_OffsetPadding_forAlignmentOnly[4];
			int64_t ___m_LongValue_9_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_ULongValue_10_OffsetPadding[4];
			uint64_t ___m_ULongValue_10;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_ULongValue_10_OffsetPadding_forAlignmentOnly[4];
			uint64_t ___m_ULongValue_10_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_FloatValue_11_OffsetPadding[4];
			float ___m_FloatValue_11;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_FloatValue_11_OffsetPadding_forAlignmentOnly[4];
			float ___m_FloatValue_11_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_DoubleValue_12_OffsetPadding[4];
			double ___m_DoubleValue_12;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_DoubleValue_12_OffsetPadding_forAlignmentOnly[4];
			double ___m_DoubleValue_12_forAlignmentOnly;
		};
	};
};

// UnityEngine.InputSystem.Utilities.JsonParser/JsonValue
struct JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB 
{
public:
	// UnityEngine.InputSystem.Utilities.JsonParser/JsonValueType UnityEngine.InputSystem.Utilities.JsonParser/JsonValue::type
	int32_t ___type_0;
	// System.Boolean UnityEngine.InputSystem.Utilities.JsonParser/JsonValue::boolValue
	bool ___boolValue_1;
	// System.Double UnityEngine.InputSystem.Utilities.JsonParser/JsonValue::realValue
	double ___realValue_2;
	// System.Int64 UnityEngine.InputSystem.Utilities.JsonParser/JsonValue::integerValue
	int64_t ___integerValue_3;
	// UnityEngine.InputSystem.Utilities.JsonParser/JsonString UnityEngine.InputSystem.Utilities.JsonParser/JsonValue::stringValue
	JsonString_tB3A1938903F2377F780FF4C4FE7CC6EA81C0D301  ___stringValue_4;
	// System.Collections.Generic.List`1<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue> UnityEngine.InputSystem.Utilities.JsonParser/JsonValue::arrayValue
	List_1_tEC0B302C3EF23400C3EC92ED7672A715CA602068 * ___arrayValue_5;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue> UnityEngine.InputSystem.Utilities.JsonParser/JsonValue::objectValue
	Dictionary_2_t7A371082270CA8FD6BC79B5622CCD5E1C87A6E42 * ___objectValue_6;
	// System.Object UnityEngine.InputSystem.Utilities.JsonParser/JsonValue::anyValue
	RuntimeObject * ___anyValue_7;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB, ___type_0)); }
	inline int32_t get_type_0() const { return ___type_0; }
	inline int32_t* get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(int32_t value)
	{
		___type_0 = value;
	}

	inline static int32_t get_offset_of_boolValue_1() { return static_cast<int32_t>(offsetof(JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB, ___boolValue_1)); }
	inline bool get_boolValue_1() const { return ___boolValue_1; }
	inline bool* get_address_of_boolValue_1() { return &___boolValue_1; }
	inline void set_boolValue_1(bool value)
	{
		___boolValue_1 = value;
	}

	inline static int32_t get_offset_of_realValue_2() { return static_cast<int32_t>(offsetof(JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB, ___realValue_2)); }
	inline double get_realValue_2() const { return ___realValue_2; }
	inline double* get_address_of_realValue_2() { return &___realValue_2; }
	inline void set_realValue_2(double value)
	{
		___realValue_2 = value;
	}

	inline static int32_t get_offset_of_integerValue_3() { return static_cast<int32_t>(offsetof(JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB, ___integerValue_3)); }
	inline int64_t get_integerValue_3() const { return ___integerValue_3; }
	inline int64_t* get_address_of_integerValue_3() { return &___integerValue_3; }
	inline void set_integerValue_3(int64_t value)
	{
		___integerValue_3 = value;
	}

	inline static int32_t get_offset_of_stringValue_4() { return static_cast<int32_t>(offsetof(JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB, ___stringValue_4)); }
	inline JsonString_tB3A1938903F2377F780FF4C4FE7CC6EA81C0D301  get_stringValue_4() const { return ___stringValue_4; }
	inline JsonString_tB3A1938903F2377F780FF4C4FE7CC6EA81C0D301 * get_address_of_stringValue_4() { return &___stringValue_4; }
	inline void set_stringValue_4(JsonString_tB3A1938903F2377F780FF4C4FE7CC6EA81C0D301  value)
	{
		___stringValue_4 = value;
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___stringValue_4))->___text_0))->___m_String_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_arrayValue_5() { return static_cast<int32_t>(offsetof(JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB, ___arrayValue_5)); }
	inline List_1_tEC0B302C3EF23400C3EC92ED7672A715CA602068 * get_arrayValue_5() const { return ___arrayValue_5; }
	inline List_1_tEC0B302C3EF23400C3EC92ED7672A715CA602068 ** get_address_of_arrayValue_5() { return &___arrayValue_5; }
	inline void set_arrayValue_5(List_1_tEC0B302C3EF23400C3EC92ED7672A715CA602068 * value)
	{
		___arrayValue_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___arrayValue_5), (void*)value);
	}

	inline static int32_t get_offset_of_objectValue_6() { return static_cast<int32_t>(offsetof(JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB, ___objectValue_6)); }
	inline Dictionary_2_t7A371082270CA8FD6BC79B5622CCD5E1C87A6E42 * get_objectValue_6() const { return ___objectValue_6; }
	inline Dictionary_2_t7A371082270CA8FD6BC79B5622CCD5E1C87A6E42 ** get_address_of_objectValue_6() { return &___objectValue_6; }
	inline void set_objectValue_6(Dictionary_2_t7A371082270CA8FD6BC79B5622CCD5E1C87A6E42 * value)
	{
		___objectValue_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___objectValue_6), (void*)value);
	}

	inline static int32_t get_offset_of_anyValue_7() { return static_cast<int32_t>(offsetof(JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB, ___anyValue_7)); }
	inline RuntimeObject * get_anyValue_7() const { return ___anyValue_7; }
	inline RuntimeObject ** get_address_of_anyValue_7() { return &___anyValue_7; }
	inline void set_anyValue_7(RuntimeObject * value)
	{
		___anyValue_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___anyValue_7), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.InputSystem.Utilities.JsonParser/JsonValue
struct JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB_marshaled_pinvoke
{
	int32_t ___type_0;
	int32_t ___boolValue_1;
	double ___realValue_2;
	int64_t ___integerValue_3;
	JsonString_tB3A1938903F2377F780FF4C4FE7CC6EA81C0D301_marshaled_pinvoke ___stringValue_4;
	List_1_tEC0B302C3EF23400C3EC92ED7672A715CA602068 * ___arrayValue_5;
	Dictionary_2_t7A371082270CA8FD6BC79B5622CCD5E1C87A6E42 * ___objectValue_6;
	Il2CppIUnknown* ___anyValue_7;
};
// Native definition for COM marshalling of UnityEngine.InputSystem.Utilities.JsonParser/JsonValue
struct JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB_marshaled_com
{
	int32_t ___type_0;
	int32_t ___boolValue_1;
	double ___realValue_2;
	int64_t ___integerValue_3;
	JsonString_tB3A1938903F2377F780FF4C4FE7CC6EA81C0D301_marshaled_com ___stringValue_4;
	List_1_tEC0B302C3EF23400C3EC92ED7672A715CA602068 * ___arrayValue_5;
	Dictionary_2_t7A371082270CA8FD6BC79B5622CCD5E1C87A6E42 * ___objectValue_6;
	Il2CppIUnknown* ___anyValue_7;
};

// System.Collections.Generic.List`1/Enumerator<Oculus.Interaction.Input.HandSkeletonJoint>
struct Enumerator_t749ECD21102D25154785738AC993322CE5A9A3D6 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::list
	List_1_t92F561CC77DE516A0293B29C982ACDC3391E1658 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	HandSkeletonJoint_t5B4E2A23FDAC8BC4C8751E5FBC7C96A1056582F3  ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_t749ECD21102D25154785738AC993322CE5A9A3D6, ___list_0)); }
	inline List_1_t92F561CC77DE516A0293B29C982ACDC3391E1658 * get_list_0() const { return ___list_0; }
	inline List_1_t92F561CC77DE516A0293B29C982ACDC3391E1658 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t92F561CC77DE516A0293B29C982ACDC3391E1658 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_t749ECD21102D25154785738AC993322CE5A9A3D6, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_t749ECD21102D25154785738AC993322CE5A9A3D6, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t749ECD21102D25154785738AC993322CE5A9A3D6, ___current_3)); }
	inline HandSkeletonJoint_t5B4E2A23FDAC8BC4C8751E5FBC7C96A1056582F3  get_current_3() const { return ___current_3; }
	inline HandSkeletonJoint_t5B4E2A23FDAC8BC4C8751E5FBC7C96A1056582F3 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(HandSkeletonJoint_t5B4E2A23FDAC8BC4C8751E5FBC7C96A1056582F3  value)
	{
		___current_3 = value;
	}
};


// System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>
struct Enumerator_tF2D95CC18A66F3DE682EAC9B88E865750DFAEF60 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::list
	List_1_tEC0B302C3EF23400C3EC92ED7672A715CA602068 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB  ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_tF2D95CC18A66F3DE682EAC9B88E865750DFAEF60, ___list_0)); }
	inline List_1_tEC0B302C3EF23400C3EC92ED7672A715CA602068 * get_list_0() const { return ___list_0; }
	inline List_1_tEC0B302C3EF23400C3EC92ED7672A715CA602068 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_tEC0B302C3EF23400C3EC92ED7672A715CA602068 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_tF2D95CC18A66F3DE682EAC9B88E865750DFAEF60, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_tF2D95CC18A66F3DE682EAC9B88E865750DFAEF60, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_tF2D95CC18A66F3DE682EAC9B88E865750DFAEF60, ___current_3)); }
	inline JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB  get_current_3() const { return ___current_3; }
	inline JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB  value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___current_3))->___stringValue_4))->___text_0))->___m_String_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___current_3))->___arrayValue_5), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___current_3))->___objectValue_6), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___current_3))->___anyValue_7), (void*)NULL);
		#endif
	}
};


// System.Func`2<Oculus.Interaction.Input.HandSkeletonJoint,System.Boolean>
struct Func_2_t31607A88702EFEFC5B33245A610C64EA7D6ED9F7  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<Oculus.Interaction.Input.HandSkeletonJoint,Oculus.Interaction.Input.HandSkeletonJoint>
struct Func_2_tB339A94B37E01030C823BB96512593067BEF80DC  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<System.Int32Enum,System.Boolean>
struct Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<System.Int32Enum,UnityEngine.InputSystem.Utilities.InternedString>
struct Func_2_t043042489DBF19074B5B434F090F7B8AAE97E138  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<System.Int32Enum,System.Object>
struct Func_2_tF916A06E36F9340A8A2E0053D6A8F0737C5FD2EB  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.InputSystem.Utilities.InternedString,System.Boolean>
struct Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.InputSystem.Utilities.InternedString,UnityEngine.InputSystem.Utilities.InternedString>
struct Func_2_t1AF155D27A4D78882342D697E4A6DE7CE594545B  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.InputSystem.Utilities.InternedString,System.Object>
struct Func_2_tA2BD34BB8D97AD282016BD4CB5B8675E4AF85B45  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.InputSystem.Utilities.NameAndParameters,System.Boolean>
struct Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.InputSystem.Utilities.NameAndParameters,UnityEngine.InputSystem.Utilities.InternedString>
struct Func_2_tEA2235809AB0E1AB8425745179691D76B519B0C5  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.InputSystem.Utilities.NameAndParameters,System.Object>
struct Func_2_tF7304610FB007335AC870B51C8FE2092628DC0DB  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<System.Object,System.Boolean>
struct Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<System.Object,UnityEngine.InputSystem.Utilities.InternedString>
struct Func_2_t964229B6ADD2B008268547ED2BC3C090AF218065  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<System.Object,System.Object>
struct Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.Pose,System.Boolean>
struct Func_2_t288BC2D8C12E52B8F2E7F675CFB740C44E7E813F  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.Pose,UnityEngine.Quaternion>
struct Func_2_t6170D516A1F6F9B6BE1BDC5379D0A5CC320AF0F8  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.Quaternion,System.Boolean>
struct Func_2_t34B33A6CBEAEB1EF3A60338636A0DC782C33A7BE  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<System.Single,System.Boolean>
struct Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<System.Single,UnityEngine.InputSystem.Utilities.InternedString>
struct Func_2_t4E06D96B2978B60DA267963F96583CE3BF82EA84  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<System.Single,System.Object>
struct Func_2_tE11BDF71BCB314E58F1A9CFC9DAE08A5F717E2A0  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.InputSystem.Utilities.Substring,System.Boolean>
struct Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.InputSystem.Utilities.Substring,UnityEngine.InputSystem.Utilities.InternedString>
struct Func_2_t14F6F4D7FBF558A20782EDC48D80473E4800048E  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.InputSystem.Utilities.Substring,System.Object>
struct Func_2_t4EC2F095EDDC8079537CA0FEDC463635E599C9E4  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,System.Boolean>
struct Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,UnityEngine.InputSystem.Utilities.InternedString>
struct Func_2_t48DD1C8E439811C492BAFE220A679DB57D7C9370  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,System.Object>
struct Func_2_t1628B2E4EEC7A32D5B688E54415BC42EDA9D2899  : public MulticastDelegate_t
{
public:

public:
};


// System.Linq.Enumerable/Iterator`1<Oculus.Interaction.Input.HandSkeletonJoint>
struct Iterator_1_tBD3F91668D911F5945632A9FDF70A5848BD6CC26  : public RuntimeObject
{
public:
	// System.Int32 System.Linq.Enumerable/Iterator`1::threadId
	int32_t ___threadId_0;
	// System.Int32 System.Linq.Enumerable/Iterator`1::state
	int32_t ___state_1;
	// TSource System.Linq.Enumerable/Iterator`1::current
	HandSkeletonJoint_t5B4E2A23FDAC8BC4C8751E5FBC7C96A1056582F3  ___current_2;

public:
	inline static int32_t get_offset_of_threadId_0() { return static_cast<int32_t>(offsetof(Iterator_1_tBD3F91668D911F5945632A9FDF70A5848BD6CC26, ___threadId_0)); }
	inline int32_t get_threadId_0() const { return ___threadId_0; }
	inline int32_t* get_address_of_threadId_0() { return &___threadId_0; }
	inline void set_threadId_0(int32_t value)
	{
		___threadId_0 = value;
	}

	inline static int32_t get_offset_of_state_1() { return static_cast<int32_t>(offsetof(Iterator_1_tBD3F91668D911F5945632A9FDF70A5848BD6CC26, ___state_1)); }
	inline int32_t get_state_1() const { return ___state_1; }
	inline int32_t* get_address_of_state_1() { return &___state_1; }
	inline void set_state_1(int32_t value)
	{
		___state_1 = value;
	}

	inline static int32_t get_offset_of_current_2() { return static_cast<int32_t>(offsetof(Iterator_1_tBD3F91668D911F5945632A9FDF70A5848BD6CC26, ___current_2)); }
	inline HandSkeletonJoint_t5B4E2A23FDAC8BC4C8751E5FBC7C96A1056582F3  get_current_2() const { return ___current_2; }
	inline HandSkeletonJoint_t5B4E2A23FDAC8BC4C8751E5FBC7C96A1056582F3 * get_address_of_current_2() { return &___current_2; }
	inline void set_current_2(HandSkeletonJoint_t5B4E2A23FDAC8BC4C8751E5FBC7C96A1056582F3  value)
	{
		___current_2 = value;
	}
};


// System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>
struct KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	RuntimeObject * ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB  ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184, ___key_0)); }
	inline RuntimeObject * get_key_0() const { return ___key_0; }
	inline RuntimeObject ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(RuntimeObject * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___key_0), (void*)value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184, ___value_1)); }
	inline JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB  get_value_1() const { return ___value_1; }
	inline JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB * get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB  value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___value_1))->___stringValue_4))->___text_0))->___m_String_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___value_1))->___arrayValue_5), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___value_1))->___objectValue_6), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___value_1))->___anyValue_7), (void*)NULL);
		#endif
	}
};


// System.Linq.Enumerable/WhereSelectListIterator`2<System.Int32Enum,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectListIterator_2_t75D7B928E94826A5A6BBCB97842E5B2C3BBDF5F5  : public Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::source
	List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectListIterator`2::predicate
	Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectListIterator`2::selector
	Func_2_t043042489DBF19074B5B434F090F7B8AAE97E138 * ___selector_5;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::enumerator
	Enumerator_t3A873D53A96DA0AC1C030CB5A0BF2D0F57FCC984  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t75D7B928E94826A5A6BBCB97842E5B2C3BBDF5F5, ___source_3)); }
	inline List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A * get_source_3() const { return ___source_3; }
	inline List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t75D7B928E94826A5A6BBCB97842E5B2C3BBDF5F5, ___predicate_4)); }
	inline Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t75D7B928E94826A5A6BBCB97842E5B2C3BBDF5F5, ___selector_5)); }
	inline Func_2_t043042489DBF19074B5B434F090F7B8AAE97E138 * get_selector_5() const { return ___selector_5; }
	inline Func_2_t043042489DBF19074B5B434F090F7B8AAE97E138 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t043042489DBF19074B5B434F090F7B8AAE97E138 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t75D7B928E94826A5A6BBCB97842E5B2C3BBDF5F5, ___enumerator_6)); }
	inline Enumerator_t3A873D53A96DA0AC1C030CB5A0BF2D0F57FCC984  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_t3A873D53A96DA0AC1C030CB5A0BF2D0F57FCC984 * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_t3A873D53A96DA0AC1C030CB5A0BF2D0F57FCC984  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
	}
};


// System.Linq.Enumerable/WhereSelectListIterator`2<System.Int32Enum,System.Object>
struct WhereSelectListIterator_2_t5F082584C1A03211ACA908D2D073C1D6149B14A8  : public Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::source
	List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectListIterator`2::predicate
	Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectListIterator`2::selector
	Func_2_tF916A06E36F9340A8A2E0053D6A8F0737C5FD2EB * ___selector_5;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::enumerator
	Enumerator_t3A873D53A96DA0AC1C030CB5A0BF2D0F57FCC984  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t5F082584C1A03211ACA908D2D073C1D6149B14A8, ___source_3)); }
	inline List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A * get_source_3() const { return ___source_3; }
	inline List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t5F082584C1A03211ACA908D2D073C1D6149B14A8, ___predicate_4)); }
	inline Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t5F082584C1A03211ACA908D2D073C1D6149B14A8, ___selector_5)); }
	inline Func_2_tF916A06E36F9340A8A2E0053D6A8F0737C5FD2EB * get_selector_5() const { return ___selector_5; }
	inline Func_2_tF916A06E36F9340A8A2E0053D6A8F0737C5FD2EB ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_tF916A06E36F9340A8A2E0053D6A8F0737C5FD2EB * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t5F082584C1A03211ACA908D2D073C1D6149B14A8, ___enumerator_6)); }
	inline Enumerator_t3A873D53A96DA0AC1C030CB5A0BF2D0F57FCC984  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_t3A873D53A96DA0AC1C030CB5A0BF2D0F57FCC984 * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_t3A873D53A96DA0AC1C030CB5A0BF2D0F57FCC984  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
	}
};


// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectListIterator_2_t97CD9F77D3626E616F5290B44E8448C47A64E44C  : public Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::source
	List_1_tCBF5EB99F2E538DCB47A8C835DD4B9C024879393 * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectListIterator`2::predicate
	Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectListIterator`2::selector
	Func_2_tEA2235809AB0E1AB8425745179691D76B519B0C5 * ___selector_5;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::enumerator
	Enumerator_tA2E0F83E15EA46A2B4B08AFB750700C6E1387FC0  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t97CD9F77D3626E616F5290B44E8448C47A64E44C, ___source_3)); }
	inline List_1_tCBF5EB99F2E538DCB47A8C835DD4B9C024879393 * get_source_3() const { return ___source_3; }
	inline List_1_tCBF5EB99F2E538DCB47A8C835DD4B9C024879393 ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_tCBF5EB99F2E538DCB47A8C835DD4B9C024879393 * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t97CD9F77D3626E616F5290B44E8448C47A64E44C, ___predicate_4)); }
	inline Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t97CD9F77D3626E616F5290B44E8448C47A64E44C, ___selector_5)); }
	inline Func_2_tEA2235809AB0E1AB8425745179691D76B519B0C5 * get_selector_5() const { return ___selector_5; }
	inline Func_2_tEA2235809AB0E1AB8425745179691D76B519B0C5 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_tEA2235809AB0E1AB8425745179691D76B519B0C5 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t97CD9F77D3626E616F5290B44E8448C47A64E44C, ___enumerator_6)); }
	inline Enumerator_tA2E0F83E15EA46A2B4B08AFB750700C6E1387FC0  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_tA2E0F83E15EA46A2B4B08AFB750700C6E1387FC0 * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_tA2E0F83E15EA46A2B4B08AFB750700C6E1387FC0  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___U3CnameU3Ek__BackingField_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___enumerator_6))->___current_3))->___U3CparametersU3Ek__BackingField_1))->___m_Array_0), (void*)NULL);
		#endif
	}
};


// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,System.Object>
struct WhereSelectListIterator_2_tDFC8E5CA0DE13C88A58AD7BCD17F1FC274A3F95D  : public Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::source
	List_1_tCBF5EB99F2E538DCB47A8C835DD4B9C024879393 * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectListIterator`2::predicate
	Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectListIterator`2::selector
	Func_2_tF7304610FB007335AC870B51C8FE2092628DC0DB * ___selector_5;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::enumerator
	Enumerator_tA2E0F83E15EA46A2B4B08AFB750700C6E1387FC0  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tDFC8E5CA0DE13C88A58AD7BCD17F1FC274A3F95D, ___source_3)); }
	inline List_1_tCBF5EB99F2E538DCB47A8C835DD4B9C024879393 * get_source_3() const { return ___source_3; }
	inline List_1_tCBF5EB99F2E538DCB47A8C835DD4B9C024879393 ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_tCBF5EB99F2E538DCB47A8C835DD4B9C024879393 * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tDFC8E5CA0DE13C88A58AD7BCD17F1FC274A3F95D, ___predicate_4)); }
	inline Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tDFC8E5CA0DE13C88A58AD7BCD17F1FC274A3F95D, ___selector_5)); }
	inline Func_2_tF7304610FB007335AC870B51C8FE2092628DC0DB * get_selector_5() const { return ___selector_5; }
	inline Func_2_tF7304610FB007335AC870B51C8FE2092628DC0DB ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_tF7304610FB007335AC870B51C8FE2092628DC0DB * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tDFC8E5CA0DE13C88A58AD7BCD17F1FC274A3F95D, ___enumerator_6)); }
	inline Enumerator_tA2E0F83E15EA46A2B4B08AFB750700C6E1387FC0  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_tA2E0F83E15EA46A2B4B08AFB750700C6E1387FC0 * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_tA2E0F83E15EA46A2B4B08AFB750700C6E1387FC0  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___U3CnameU3Ek__BackingField_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___enumerator_6))->___current_3))->___U3CparametersU3Ek__BackingField_1))->___m_Array_0), (void*)NULL);
		#endif
	}
};


// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.Pose,UnityEngine.Quaternion>
struct WhereSelectListIterator_2_tA4AFE0F933BE35E59757614CBEDAA04AEBC6AD3D  : public Iterator_1_t4BD4EE08E1E7E81949C3754EC2969154580F4C65
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::source
	List_1_tDB586987B7A2D2B13A903004C86E3C3FBD061C1C * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectListIterator`2::predicate
	Func_2_t288BC2D8C12E52B8F2E7F675CFB740C44E7E813F * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectListIterator`2::selector
	Func_2_t6170D516A1F6F9B6BE1BDC5379D0A5CC320AF0F8 * ___selector_5;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::enumerator
	Enumerator_tA7E1DE1C5B2833EE935A19CAF705B2C053EBC9AB  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tA4AFE0F933BE35E59757614CBEDAA04AEBC6AD3D, ___source_3)); }
	inline List_1_tDB586987B7A2D2B13A903004C86E3C3FBD061C1C * get_source_3() const { return ___source_3; }
	inline List_1_tDB586987B7A2D2B13A903004C86E3C3FBD061C1C ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_tDB586987B7A2D2B13A903004C86E3C3FBD061C1C * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tA4AFE0F933BE35E59757614CBEDAA04AEBC6AD3D, ___predicate_4)); }
	inline Func_2_t288BC2D8C12E52B8F2E7F675CFB740C44E7E813F * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t288BC2D8C12E52B8F2E7F675CFB740C44E7E813F ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t288BC2D8C12E52B8F2E7F675CFB740C44E7E813F * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tA4AFE0F933BE35E59757614CBEDAA04AEBC6AD3D, ___selector_5)); }
	inline Func_2_t6170D516A1F6F9B6BE1BDC5379D0A5CC320AF0F8 * get_selector_5() const { return ___selector_5; }
	inline Func_2_t6170D516A1F6F9B6BE1BDC5379D0A5CC320AF0F8 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t6170D516A1F6F9B6BE1BDC5379D0A5CC320AF0F8 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tA4AFE0F933BE35E59757614CBEDAA04AEBC6AD3D, ___enumerator_6)); }
	inline Enumerator_tA7E1DE1C5B2833EE935A19CAF705B2C053EBC9AB  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_tA7E1DE1C5B2833EE935A19CAF705B2C053EBC9AB * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_tA7E1DE1C5B2833EE935A19CAF705B2C053EBC9AB  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
	}
};


// UnityEngine.InputSystem.Utilities.NamedValue
struct NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077 
{
public:
	// System.String UnityEngine.InputSystem.Utilities.NamedValue::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_1;
	// UnityEngine.InputSystem.Utilities.PrimitiveValue UnityEngine.InputSystem.Utilities.NamedValue::<value>k__BackingField
	PrimitiveValue_t70D8365A1CB2217C466D27D6820362EF4331A0C8  ___U3CvalueU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077, ___U3CnameU3Ek__BackingField_1)); }
	inline String_t* get_U3CnameU3Ek__BackingField_1() const { return ___U3CnameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_1() { return &___U3CnameU3Ek__BackingField_1; }
	inline void set_U3CnameU3Ek__BackingField_1(String_t* value)
	{
		___U3CnameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CnameU3Ek__BackingField_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CvalueU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077, ___U3CvalueU3Ek__BackingField_2)); }
	inline PrimitiveValue_t70D8365A1CB2217C466D27D6820362EF4331A0C8  get_U3CvalueU3Ek__BackingField_2() const { return ___U3CvalueU3Ek__BackingField_2; }
	inline PrimitiveValue_t70D8365A1CB2217C466D27D6820362EF4331A0C8 * get_address_of_U3CvalueU3Ek__BackingField_2() { return &___U3CvalueU3Ek__BackingField_2; }
	inline void set_U3CvalueU3Ek__BackingField_2(PrimitiveValue_t70D8365A1CB2217C466D27D6820362EF4331A0C8  value)
	{
		___U3CvalueU3Ek__BackingField_2 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.InputSystem.Utilities.NamedValue
struct NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077_marshaled_pinvoke
{
	char* ___U3CnameU3Ek__BackingField_1;
	PrimitiveValue_t70D8365A1CB2217C466D27D6820362EF4331A0C8_marshaled_pinvoke ___U3CvalueU3Ek__BackingField_2;
};
// Native definition for COM marshalling of UnityEngine.InputSystem.Utilities.NamedValue
struct NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077_marshaled_com
{
	Il2CppChar* ___U3CnameU3Ek__BackingField_1;
	PrimitiveValue_t70D8365A1CB2217C466D27D6820362EF4331A0C8_marshaled_com ___U3CvalueU3Ek__BackingField_2;
};

// System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>>
struct Enumerator_tF2D0EEBD6B861D9127DD6DC36B733FD42070858C 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::list
	List_1_t9B1CABCBD4B6B7781E348FF332B32B333B543BA8 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184  ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_tF2D0EEBD6B861D9127DD6DC36B733FD42070858C, ___list_0)); }
	inline List_1_t9B1CABCBD4B6B7781E348FF332B32B333B543BA8 * get_list_0() const { return ___list_0; }
	inline List_1_t9B1CABCBD4B6B7781E348FF332B32B333B543BA8 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t9B1CABCBD4B6B7781E348FF332B32B333B543BA8 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_tF2D0EEBD6B861D9127DD6DC36B733FD42070858C, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_tF2D0EEBD6B861D9127DD6DC36B733FD42070858C, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_tF2D0EEBD6B861D9127DD6DC36B733FD42070858C, ___current_3)); }
	inline KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184  value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___current_3))->___key_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&((&(((&___current_3))->___value_1))->___stringValue_4))->___text_0))->___m_String_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___current_3))->___value_1))->___arrayValue_5), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___current_3))->___value_1))->___objectValue_6), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___current_3))->___value_1))->___anyValue_7), (void*)NULL);
		#endif
	}
};


// System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.NamedValue>
struct Enumerator_tDEE846E92D2B22781FB09541CB1600E5398D3A20 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::list
	List_1_tDC9592305A8D24F0DBC4A23F1719292FCB2887C1 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077  ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_tDEE846E92D2B22781FB09541CB1600E5398D3A20, ___list_0)); }
	inline List_1_tDC9592305A8D24F0DBC4A23F1719292FCB2887C1 * get_list_0() const { return ___list_0; }
	inline List_1_tDC9592305A8D24F0DBC4A23F1719292FCB2887C1 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_tDC9592305A8D24F0DBC4A23F1719292FCB2887C1 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_tDEE846E92D2B22781FB09541CB1600E5398D3A20, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_tDEE846E92D2B22781FB09541CB1600E5398D3A20, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_tDEE846E92D2B22781FB09541CB1600E5398D3A20, ___current_3)); }
	inline NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077  get_current_3() const { return ___current_3; }
	inline NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077  value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___current_3))->___U3CnameU3Ek__BackingField_1), (void*)NULL);
	}
};


// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,System.Boolean>
struct Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,UnityEngine.InputSystem.Utilities.InternedString>
struct Func_2_tB181F4C9A24C0634DF4A10CFBFFE6B45B3796BBC  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,System.Object>
struct Func_2_t711512467B454C91EB8F3FB19CD6D141AAB5B6B1  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.InputSystem.Utilities.NamedValue,System.Boolean>
struct Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.InputSystem.Utilities.NamedValue,UnityEngine.InputSystem.Utilities.InternedString>
struct Func_2_tA21678F1D9F9F4617D645755285D28EDE5F9AE44  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.InputSystem.Utilities.NamedValue,System.Object>
struct Func_2_t5B101C0B26ADAA3B1A77BC50D51CC42364E5409E  : public MulticastDelegate_t
{
public:

public:
};


// System.Linq.Enumerable/WhereEnumerableIterator`1<Oculus.Interaction.Input.HandSkeletonJoint>
struct WhereEnumerableIterator_1_t7E214A8B0456E617DE0100DC5CA366442CB22EBD  : public Iterator_1_tBD3F91668D911F5945632A9FDF70A5848BD6CC26
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereEnumerableIterator`1::source
	RuntimeObject* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereEnumerableIterator`1::predicate
	Func_2_t31607A88702EFEFC5B33245A610C64EA7D6ED9F7 * ___predicate_4;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/WhereEnumerableIterator`1::enumerator
	RuntimeObject* ___enumerator_5;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereEnumerableIterator_1_t7E214A8B0456E617DE0100DC5CA366442CB22EBD, ___source_3)); }
	inline RuntimeObject* get_source_3() const { return ___source_3; }
	inline RuntimeObject** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(RuntimeObject* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereEnumerableIterator_1_t7E214A8B0456E617DE0100DC5CA366442CB22EBD, ___predicate_4)); }
	inline Func_2_t31607A88702EFEFC5B33245A610C64EA7D6ED9F7 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t31607A88702EFEFC5B33245A610C64EA7D6ED9F7 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t31607A88702EFEFC5B33245A610C64EA7D6ED9F7 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_5() { return static_cast<int32_t>(offsetof(WhereEnumerableIterator_1_t7E214A8B0456E617DE0100DC5CA366442CB22EBD, ___enumerator_5)); }
	inline RuntimeObject* get_enumerator_5() const { return ___enumerator_5; }
	inline RuntimeObject** get_address_of_enumerator_5() { return &___enumerator_5; }
	inline void set_enumerator_5(RuntimeObject* value)
	{
		___enumerator_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumerator_5), (void*)value);
	}
};


// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<Oculus.Interaction.Input.HandSkeletonJoint,Oculus.Interaction.Input.HandSkeletonJoint>
struct WhereSelectEnumerableIterator_2_t04AB31927A6E89520049EC6E92923D1FBAC994AB  : public Iterator_1_tBD3F91668D911F5945632A9FDF70A5848BD6CC26
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::source
	RuntimeObject* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::predicate
	Func_2_t31607A88702EFEFC5B33245A610C64EA7D6ED9F7 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::selector
	Func_2_tB339A94B37E01030C823BB96512593067BEF80DC * ___selector_5;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::enumerator
	RuntimeObject* ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t04AB31927A6E89520049EC6E92923D1FBAC994AB, ___source_3)); }
	inline RuntimeObject* get_source_3() const { return ___source_3; }
	inline RuntimeObject** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(RuntimeObject* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t04AB31927A6E89520049EC6E92923D1FBAC994AB, ___predicate_4)); }
	inline Func_2_t31607A88702EFEFC5B33245A610C64EA7D6ED9F7 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t31607A88702EFEFC5B33245A610C64EA7D6ED9F7 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t31607A88702EFEFC5B33245A610C64EA7D6ED9F7 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t04AB31927A6E89520049EC6E92923D1FBAC994AB, ___selector_5)); }
	inline Func_2_tB339A94B37E01030C823BB96512593067BEF80DC * get_selector_5() const { return ___selector_5; }
	inline Func_2_tB339A94B37E01030C823BB96512593067BEF80DC ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_tB339A94B37E01030C823BB96512593067BEF80DC * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t04AB31927A6E89520049EC6E92923D1FBAC994AB, ___enumerator_6)); }
	inline RuntimeObject* get_enumerator_6() const { return ___enumerator_6; }
	inline RuntimeObject** get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(RuntimeObject* value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumerator_6), (void*)value);
	}
};


// System.Linq.Enumerable/WhereSelectListIterator`2<Oculus.Interaction.Input.HandSkeletonJoint,Oculus.Interaction.Input.HandSkeletonJoint>
struct WhereSelectListIterator_2_t912D2CA762EC2D53D104D6B1FFE8BCEF3FA7319F  : public Iterator_1_tBD3F91668D911F5945632A9FDF70A5848BD6CC26
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::source
	List_1_t92F561CC77DE516A0293B29C982ACDC3391E1658 * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectListIterator`2::predicate
	Func_2_t31607A88702EFEFC5B33245A610C64EA7D6ED9F7 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectListIterator`2::selector
	Func_2_tB339A94B37E01030C823BB96512593067BEF80DC * ___selector_5;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::enumerator
	Enumerator_t749ECD21102D25154785738AC993322CE5A9A3D6  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t912D2CA762EC2D53D104D6B1FFE8BCEF3FA7319F, ___source_3)); }
	inline List_1_t92F561CC77DE516A0293B29C982ACDC3391E1658 * get_source_3() const { return ___source_3; }
	inline List_1_t92F561CC77DE516A0293B29C982ACDC3391E1658 ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_t92F561CC77DE516A0293B29C982ACDC3391E1658 * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t912D2CA762EC2D53D104D6B1FFE8BCEF3FA7319F, ___predicate_4)); }
	inline Func_2_t31607A88702EFEFC5B33245A610C64EA7D6ED9F7 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t31607A88702EFEFC5B33245A610C64EA7D6ED9F7 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t31607A88702EFEFC5B33245A610C64EA7D6ED9F7 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t912D2CA762EC2D53D104D6B1FFE8BCEF3FA7319F, ___selector_5)); }
	inline Func_2_tB339A94B37E01030C823BB96512593067BEF80DC * get_selector_5() const { return ___selector_5; }
	inline Func_2_tB339A94B37E01030C823BB96512593067BEF80DC ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_tB339A94B37E01030C823BB96512593067BEF80DC * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t912D2CA762EC2D53D104D6B1FFE8BCEF3FA7319F, ___enumerator_6)); }
	inline Enumerator_t749ECD21102D25154785738AC993322CE5A9A3D6  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_t749ECD21102D25154785738AC993322CE5A9A3D6 * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_t749ECD21102D25154785738AC993322CE5A9A3D6  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
	}
};


// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectListIterator_2_tF2D97D886F080B26C19947C7C6A56563B86B14D7  : public Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::source
	List_1_tEC0B302C3EF23400C3EC92ED7672A715CA602068 * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectListIterator`2::predicate
	Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectListIterator`2::selector
	Func_2_t48DD1C8E439811C492BAFE220A679DB57D7C9370 * ___selector_5;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::enumerator
	Enumerator_tF2D95CC18A66F3DE682EAC9B88E865750DFAEF60  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tF2D97D886F080B26C19947C7C6A56563B86B14D7, ___source_3)); }
	inline List_1_tEC0B302C3EF23400C3EC92ED7672A715CA602068 * get_source_3() const { return ___source_3; }
	inline List_1_tEC0B302C3EF23400C3EC92ED7672A715CA602068 ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_tEC0B302C3EF23400C3EC92ED7672A715CA602068 * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tF2D97D886F080B26C19947C7C6A56563B86B14D7, ___predicate_4)); }
	inline Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tF2D97D886F080B26C19947C7C6A56563B86B14D7, ___selector_5)); }
	inline Func_2_t48DD1C8E439811C492BAFE220A679DB57D7C9370 * get_selector_5() const { return ___selector_5; }
	inline Func_2_t48DD1C8E439811C492BAFE220A679DB57D7C9370 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t48DD1C8E439811C492BAFE220A679DB57D7C9370 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tF2D97D886F080B26C19947C7C6A56563B86B14D7, ___enumerator_6)); }
	inline Enumerator_tF2D95CC18A66F3DE682EAC9B88E865750DFAEF60  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_tF2D95CC18A66F3DE682EAC9B88E865750DFAEF60 * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_tF2D95CC18A66F3DE682EAC9B88E865750DFAEF60  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&((&(((&___enumerator_6))->___current_3))->___stringValue_4))->___text_0))->___m_String_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___arrayValue_5), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___objectValue_6), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___anyValue_7), (void*)NULL);
		#endif
	}
};


// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,System.Object>
struct WhereSelectListIterator_2_t2F7487B4BF5A9E71CBE06091C45AF7D98CF9F1AC  : public Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::source
	List_1_tEC0B302C3EF23400C3EC92ED7672A715CA602068 * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectListIterator`2::predicate
	Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectListIterator`2::selector
	Func_2_t1628B2E4EEC7A32D5B688E54415BC42EDA9D2899 * ___selector_5;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::enumerator
	Enumerator_tF2D95CC18A66F3DE682EAC9B88E865750DFAEF60  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t2F7487B4BF5A9E71CBE06091C45AF7D98CF9F1AC, ___source_3)); }
	inline List_1_tEC0B302C3EF23400C3EC92ED7672A715CA602068 * get_source_3() const { return ___source_3; }
	inline List_1_tEC0B302C3EF23400C3EC92ED7672A715CA602068 ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_tEC0B302C3EF23400C3EC92ED7672A715CA602068 * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t2F7487B4BF5A9E71CBE06091C45AF7D98CF9F1AC, ___predicate_4)); }
	inline Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t2F7487B4BF5A9E71CBE06091C45AF7D98CF9F1AC, ___selector_5)); }
	inline Func_2_t1628B2E4EEC7A32D5B688E54415BC42EDA9D2899 * get_selector_5() const { return ___selector_5; }
	inline Func_2_t1628B2E4EEC7A32D5B688E54415BC42EDA9D2899 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t1628B2E4EEC7A32D5B688E54415BC42EDA9D2899 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t2F7487B4BF5A9E71CBE06091C45AF7D98CF9F1AC, ___enumerator_6)); }
	inline Enumerator_tF2D95CC18A66F3DE682EAC9B88E865750DFAEF60  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_tF2D95CC18A66F3DE682EAC9B88E865750DFAEF60 * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_tF2D95CC18A66F3DE682EAC9B88E865750DFAEF60  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&((&(((&___enumerator_6))->___current_3))->___stringValue_4))->___text_0))->___m_String_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___arrayValue_5), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___objectValue_6), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___anyValue_7), (void*)NULL);
		#endif
	}
};


// System.Linq.Enumerable/WhereSelectListIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectListIterator_2_tF2F8745320F9D29534105CFDF2FADABD42A2D3F7  : public Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::source
	List_1_t9B1CABCBD4B6B7781E348FF332B32B333B543BA8 * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectListIterator`2::predicate
	Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectListIterator`2::selector
	Func_2_tB181F4C9A24C0634DF4A10CFBFFE6B45B3796BBC * ___selector_5;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::enumerator
	Enumerator_tF2D0EEBD6B861D9127DD6DC36B733FD42070858C  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tF2F8745320F9D29534105CFDF2FADABD42A2D3F7, ___source_3)); }
	inline List_1_t9B1CABCBD4B6B7781E348FF332B32B333B543BA8 * get_source_3() const { return ___source_3; }
	inline List_1_t9B1CABCBD4B6B7781E348FF332B32B333B543BA8 ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_t9B1CABCBD4B6B7781E348FF332B32B333B543BA8 * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tF2F8745320F9D29534105CFDF2FADABD42A2D3F7, ___predicate_4)); }
	inline Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tF2F8745320F9D29534105CFDF2FADABD42A2D3F7, ___selector_5)); }
	inline Func_2_tB181F4C9A24C0634DF4A10CFBFFE6B45B3796BBC * get_selector_5() const { return ___selector_5; }
	inline Func_2_tB181F4C9A24C0634DF4A10CFBFFE6B45B3796BBC ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_tB181F4C9A24C0634DF4A10CFBFFE6B45B3796BBC * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tF2F8745320F9D29534105CFDF2FADABD42A2D3F7, ___enumerator_6)); }
	inline Enumerator_tF2D0EEBD6B861D9127DD6DC36B733FD42070858C  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_tF2D0EEBD6B861D9127DD6DC36B733FD42070858C * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_tF2D0EEBD6B861D9127DD6DC36B733FD42070858C  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___key_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&((&((&(((&___enumerator_6))->___current_3))->___value_1))->___stringValue_4))->___text_0))->___m_String_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___enumerator_6))->___current_3))->___value_1))->___arrayValue_5), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___enumerator_6))->___current_3))->___value_1))->___objectValue_6), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___enumerator_6))->___current_3))->___value_1))->___anyValue_7), (void*)NULL);
		#endif
	}
};


// System.Linq.Enumerable/WhereSelectListIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,System.Object>
struct WhereSelectListIterator_2_t15BF3EF128C005C26DE8132D49E71B3C1FE6E211  : public Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::source
	List_1_t9B1CABCBD4B6B7781E348FF332B32B333B543BA8 * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectListIterator`2::predicate
	Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectListIterator`2::selector
	Func_2_t711512467B454C91EB8F3FB19CD6D141AAB5B6B1 * ___selector_5;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::enumerator
	Enumerator_tF2D0EEBD6B861D9127DD6DC36B733FD42070858C  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t15BF3EF128C005C26DE8132D49E71B3C1FE6E211, ___source_3)); }
	inline List_1_t9B1CABCBD4B6B7781E348FF332B32B333B543BA8 * get_source_3() const { return ___source_3; }
	inline List_1_t9B1CABCBD4B6B7781E348FF332B32B333B543BA8 ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_t9B1CABCBD4B6B7781E348FF332B32B333B543BA8 * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t15BF3EF128C005C26DE8132D49E71B3C1FE6E211, ___predicate_4)); }
	inline Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t15BF3EF128C005C26DE8132D49E71B3C1FE6E211, ___selector_5)); }
	inline Func_2_t711512467B454C91EB8F3FB19CD6D141AAB5B6B1 * get_selector_5() const { return ___selector_5; }
	inline Func_2_t711512467B454C91EB8F3FB19CD6D141AAB5B6B1 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t711512467B454C91EB8F3FB19CD6D141AAB5B6B1 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t15BF3EF128C005C26DE8132D49E71B3C1FE6E211, ___enumerator_6)); }
	inline Enumerator_tF2D0EEBD6B861D9127DD6DC36B733FD42070858C  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_tF2D0EEBD6B861D9127DD6DC36B733FD42070858C * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_tF2D0EEBD6B861D9127DD6DC36B733FD42070858C  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___key_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&((&((&(((&___enumerator_6))->___current_3))->___value_1))->___stringValue_4))->___text_0))->___m_String_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___enumerator_6))->___current_3))->___value_1))->___arrayValue_5), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___enumerator_6))->___current_3))->___value_1))->___objectValue_6), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___enumerator_6))->___current_3))->___value_1))->___anyValue_7), (void*)NULL);
		#endif
	}
};


// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectListIterator_2_t84216899116C33A67088960F25CA2D6E1DDC786D  : public Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::source
	List_1_tDC9592305A8D24F0DBC4A23F1719292FCB2887C1 * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectListIterator`2::predicate
	Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectListIterator`2::selector
	Func_2_tA21678F1D9F9F4617D645755285D28EDE5F9AE44 * ___selector_5;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::enumerator
	Enumerator_tDEE846E92D2B22781FB09541CB1600E5398D3A20  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t84216899116C33A67088960F25CA2D6E1DDC786D, ___source_3)); }
	inline List_1_tDC9592305A8D24F0DBC4A23F1719292FCB2887C1 * get_source_3() const { return ___source_3; }
	inline List_1_tDC9592305A8D24F0DBC4A23F1719292FCB2887C1 ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_tDC9592305A8D24F0DBC4A23F1719292FCB2887C1 * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t84216899116C33A67088960F25CA2D6E1DDC786D, ___predicate_4)); }
	inline Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t84216899116C33A67088960F25CA2D6E1DDC786D, ___selector_5)); }
	inline Func_2_tA21678F1D9F9F4617D645755285D28EDE5F9AE44 * get_selector_5() const { return ___selector_5; }
	inline Func_2_tA21678F1D9F9F4617D645755285D28EDE5F9AE44 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_tA21678F1D9F9F4617D645755285D28EDE5F9AE44 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t84216899116C33A67088960F25CA2D6E1DDC786D, ___enumerator_6)); }
	inline Enumerator_tDEE846E92D2B22781FB09541CB1600E5398D3A20  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_tDEE846E92D2B22781FB09541CB1600E5398D3A20 * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_tDEE846E92D2B22781FB09541CB1600E5398D3A20  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___U3CnameU3Ek__BackingField_1), (void*)NULL);
		#endif
	}
};


// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,System.Object>
struct WhereSelectListIterator_2_tA4D5B2B639DCB29B4C84236B3E2651F17B522430  : public Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::source
	List_1_tDC9592305A8D24F0DBC4A23F1719292FCB2887C1 * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectListIterator`2::predicate
	Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectListIterator`2::selector
	Func_2_t5B101C0B26ADAA3B1A77BC50D51CC42364E5409E * ___selector_5;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::enumerator
	Enumerator_tDEE846E92D2B22781FB09541CB1600E5398D3A20  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tA4D5B2B639DCB29B4C84236B3E2651F17B522430, ___source_3)); }
	inline List_1_tDC9592305A8D24F0DBC4A23F1719292FCB2887C1 * get_source_3() const { return ___source_3; }
	inline List_1_tDC9592305A8D24F0DBC4A23F1719292FCB2887C1 ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_tDC9592305A8D24F0DBC4A23F1719292FCB2887C1 * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tA4D5B2B639DCB29B4C84236B3E2651F17B522430, ___predicate_4)); }
	inline Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tA4D5B2B639DCB29B4C84236B3E2651F17B522430, ___selector_5)); }
	inline Func_2_t5B101C0B26ADAA3B1A77BC50D51CC42364E5409E * get_selector_5() const { return ___selector_5; }
	inline Func_2_t5B101C0B26ADAA3B1A77BC50D51CC42364E5409E ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t5B101C0B26ADAA3B1A77BC50D51CC42364E5409E * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tA4D5B2B639DCB29B4C84236B3E2651F17B522430, ___enumerator_6)); }
	inline Enumerator_tDEE846E92D2B22781FB09541CB1600E5398D3A20  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_tDEE846E92D2B22781FB09541CB1600E5398D3A20 * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_tDEE846E92D2B22781FB09541CB1600E5398D3A20  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___U3CnameU3Ek__BackingField_1), (void*)NULL);
		#endif
	}
};


// System.Object


// System.Object


// System.Linq.Enumerable/Iterator`1<System.Object>


// System.Linq.Enumerable/Iterator`1<System.Object>


// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>>

struct List_1_t9B1CABCBD4B6B7781E348FF332B32B333B543BA8_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	KeyValuePair_2U5BU5D_tC4ED6A2024348F6B2CB712DC491F7E57AEF32630* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t9B1CABCBD4B6B7781E348FF332B32B333B543BA8_StaticFields, ____emptyArray_5)); }
	inline KeyValuePair_2U5BU5D_tC4ED6A2024348F6B2CB712DC491F7E57AEF32630* get__emptyArray_5() const { return ____emptyArray_5; }
	inline KeyValuePair_2U5BU5D_tC4ED6A2024348F6B2CB712DC491F7E57AEF32630** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(KeyValuePair_2U5BU5D_tC4ED6A2024348F6B2CB712DC491F7E57AEF32630* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>>


// System.Collections.Generic.List`1<Oculus.Interaction.Input.HandSkeletonJoint>

struct List_1_t92F561CC77DE516A0293B29C982ACDC3391E1658_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	HandSkeletonJointU5BU5D_t4B699B1E9A4171D2EE43B044CB583B5C1EB64D95* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t92F561CC77DE516A0293B29C982ACDC3391E1658_StaticFields, ____emptyArray_5)); }
	inline HandSkeletonJointU5BU5D_t4B699B1E9A4171D2EE43B044CB583B5C1EB64D95* get__emptyArray_5() const { return ____emptyArray_5; }
	inline HandSkeletonJointU5BU5D_t4B699B1E9A4171D2EE43B044CB583B5C1EB64D95** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(HandSkeletonJointU5BU5D_t4B699B1E9A4171D2EE43B044CB583B5C1EB64D95* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<Oculus.Interaction.Input.HandSkeletonJoint>


// System.Collections.Generic.List`1<System.Int32Enum>

struct List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	Int32EnumU5BU5D_t9327F857579EE00EB201E1913599094BF837D3CD* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A_StaticFields, ____emptyArray_5)); }
	inline Int32EnumU5BU5D_t9327F857579EE00EB201E1913599094BF837D3CD* get__emptyArray_5() const { return ____emptyArray_5; }
	inline Int32EnumU5BU5D_t9327F857579EE00EB201E1913599094BF837D3CD** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(Int32EnumU5BU5D_t9327F857579EE00EB201E1913599094BF837D3CD* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<System.Int32Enum>


// System.Collections.Generic.List`1<UnityEngine.InputSystem.Utilities.InternedString>

struct List_1_tFD37D052760A236906215AF581C58EE13DE6A5DC_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	InternedStringU5BU5D_t1EAE8433C8EF7C76894673801EE0EE9F6833E9A4* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_tFD37D052760A236906215AF581C58EE13DE6A5DC_StaticFields, ____emptyArray_5)); }
	inline InternedStringU5BU5D_t1EAE8433C8EF7C76894673801EE0EE9F6833E9A4* get__emptyArray_5() const { return ____emptyArray_5; }
	inline InternedStringU5BU5D_t1EAE8433C8EF7C76894673801EE0EE9F6833E9A4** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(InternedStringU5BU5D_t1EAE8433C8EF7C76894673801EE0EE9F6833E9A4* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<UnityEngine.InputSystem.Utilities.InternedString>


// System.Collections.Generic.List`1<UnityEngine.InputSystem.Utilities.NameAndParameters>

struct List_1_tCBF5EB99F2E538DCB47A8C835DD4B9C024879393_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	NameAndParametersU5BU5D_t960DDB52F44DD1231B0C626E9C859F1E0B2F5EAD* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_tCBF5EB99F2E538DCB47A8C835DD4B9C024879393_StaticFields, ____emptyArray_5)); }
	inline NameAndParametersU5BU5D_t960DDB52F44DD1231B0C626E9C859F1E0B2F5EAD* get__emptyArray_5() const { return ____emptyArray_5; }
	inline NameAndParametersU5BU5D_t960DDB52F44DD1231B0C626E9C859F1E0B2F5EAD** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(NameAndParametersU5BU5D_t960DDB52F44DD1231B0C626E9C859F1E0B2F5EAD* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<UnityEngine.InputSystem.Utilities.NameAndParameters>


// System.Collections.Generic.List`1<UnityEngine.InputSystem.Utilities.NamedValue>

struct List_1_tDC9592305A8D24F0DBC4A23F1719292FCB2887C1_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	NamedValueU5BU5D_t16F5D30B70B5FEDA89AE16CFD61FACEC7DF85391* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_tDC9592305A8D24F0DBC4A23F1719292FCB2887C1_StaticFields, ____emptyArray_5)); }
	inline NamedValueU5BU5D_t16F5D30B70B5FEDA89AE16CFD61FACEC7DF85391* get__emptyArray_5() const { return ____emptyArray_5; }
	inline NamedValueU5BU5D_t16F5D30B70B5FEDA89AE16CFD61FACEC7DF85391** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(NamedValueU5BU5D_t16F5D30B70B5FEDA89AE16CFD61FACEC7DF85391* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<UnityEngine.InputSystem.Utilities.NamedValue>


// System.Collections.Generic.List`1<System.Object>

struct List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5_StaticFields, ____emptyArray_5)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get__emptyArray_5() const { return ____emptyArray_5; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<System.Object>


// System.Collections.Generic.List`1<UnityEngine.Pose>

struct List_1_tDB586987B7A2D2B13A903004C86E3C3FBD061C1C_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	PoseU5BU5D_t45D2BAE8FDADEBC22E30236BB871C3E59C3A455A* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_tDB586987B7A2D2B13A903004C86E3C3FBD061C1C_StaticFields, ____emptyArray_5)); }
	inline PoseU5BU5D_t45D2BAE8FDADEBC22E30236BB871C3E59C3A455A* get__emptyArray_5() const { return ____emptyArray_5; }
	inline PoseU5BU5D_t45D2BAE8FDADEBC22E30236BB871C3E59C3A455A** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(PoseU5BU5D_t45D2BAE8FDADEBC22E30236BB871C3E59C3A455A* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<UnityEngine.Pose>


// System.Collections.Generic.List`1<System.Single>

struct List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA_StaticFields, ____emptyArray_5)); }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* get__emptyArray_5() const { return ____emptyArray_5; }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<System.Single>


// System.Collections.Generic.List`1<UnityEngine.InputSystem.Utilities.Substring>

struct List_1_tEAD6E3282E028927B32F56E7892994D90D512467_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	SubstringU5BU5D_tE332884993470F5CF7BAAA950414E4E24691A8D0* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_tEAD6E3282E028927B32F56E7892994D90D512467_StaticFields, ____emptyArray_5)); }
	inline SubstringU5BU5D_tE332884993470F5CF7BAAA950414E4E24691A8D0* get__emptyArray_5() const { return ____emptyArray_5; }
	inline SubstringU5BU5D_tE332884993470F5CF7BAAA950414E4E24691A8D0** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(SubstringU5BU5D_tE332884993470F5CF7BAAA950414E4E24691A8D0* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<UnityEngine.InputSystem.Utilities.Substring>


// System.Collections.Generic.List`1<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>

struct List_1_tEC0B302C3EF23400C3EC92ED7672A715CA602068_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	JsonValueU5BU5D_t2E1CBAA099182C024E4921379907D707DA049098* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_tEC0B302C3EF23400C3EC92ED7672A715CA602068_StaticFields, ____emptyArray_5)); }
	inline JsonValueU5BU5D_t2E1CBAA099182C024E4921379907D707DA049098* get__emptyArray_5() const { return ____emptyArray_5; }
	inline JsonValueU5BU5D_t2E1CBAA099182C024E4921379907D707DA049098** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(JsonValueU5BU5D_t2E1CBAA099182C024E4921379907D707DA049098* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>


// Meta.WitAi.Data.Entities.WitEntityDataBase`1<System.Int32>


// Meta.WitAi.Data.Entities.WitEntityDataBase`1<System.Int32>


// Meta.WitAi.Data.Entities.WitEntityDataBase`1<System.Object>


// Meta.WitAi.Data.Entities.WitEntityDataBase`1<System.Object>


// Meta.WitAi.Data.Entities.WitEntityDataBase`1<System.Single>


// Meta.WitAi.Data.Entities.WitEntityDataBase`1<System.Single>

struct Il2CppArrayBounds;

// System.Array

struct Il2CppArrayBounds;

// System.Array


// Meta.WitAi.Json.JsonConverter


// Meta.WitAi.Json.JsonConverter


// System.String

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.String


// System.ValueType


// System.ValueType


// Meta.WitAi.Json.WitResponseNode


// Meta.WitAi.Json.WitResponseNode


// System.Collections.Generic.List`1/Enumerator<System.Object>


// System.Collections.Generic.List`1/Enumerator<System.Object>


// System.Collections.Generic.List`1/Enumerator<System.Single>


// System.Collections.Generic.List`1/Enumerator<System.Single>


// UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Utilities.NamedValue>


// UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Utilities.NamedValue>


// System.Linq.Enumerable/WhereEnumerableIterator`1<System.Object>


// System.Linq.Enumerable/WhereEnumerableIterator`1<System.Object>


// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,System.Object>


// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,System.Object>


// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Int32Enum,System.Object>


// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Int32Enum,System.Object>


// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.InternedString,System.Object>


// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.InternedString,System.Object>


// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,System.Object>


// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,System.Object>


// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,System.Object>


// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,System.Object>


// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Object,System.Object>


// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Object,System.Object>


// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Single,System.Object>


// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Single,System.Object>


// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.Substring,System.Object>


// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.Substring,System.Object>


// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,System.Object>


// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,System.Object>


// System.Boolean

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Boolean


// System.Enum

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};


// System.Enum


// System.Int32


// System.Int32


// System.IntPtr

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.IntPtr


// UnityEngine.InputSystem.Utilities.InternedString


// UnityEngine.InputSystem.Utilities.InternedString


// UnityEngine.Quaternion

struct Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  value)
	{
		___identityQuaternion_4 = value;
	}
};


// UnityEngine.Quaternion


// System.Single


// System.Single


// UnityEngine.InputSystem.Utilities.Substring


// UnityEngine.InputSystem.Utilities.Substring


// UnityEngine.Vector3

struct Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___zeroVector_5)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___oneVector_6)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___upVector_7)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___downVector_8)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___leftVector_9)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___rightVector_10)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___forwardVector_11)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___backVector_12)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___negativeInfinityVector_14 = value;
	}
};


// UnityEngine.Vector3


// System.Void


// System.Void


// System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.InternedString>


// System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.InternedString>


// System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.Substring>


// System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.Substring>


// System.Linq.Enumerable/Iterator`1<UnityEngine.InputSystem.Utilities.InternedString>


// System.Linq.Enumerable/Iterator`1<UnityEngine.InputSystem.Utilities.InternedString>


// System.Linq.Enumerable/Iterator`1<UnityEngine.Quaternion>


// System.Linq.Enumerable/Iterator`1<UnityEngine.Quaternion>


// System.Linq.Enumerable/WhereSelectListIterator`2<System.Object,System.Object>


// System.Linq.Enumerable/WhereSelectListIterator`2<System.Object,System.Object>


// System.Linq.Enumerable/WhereSelectListIterator`2<System.Single,System.Object>


// System.Linq.Enumerable/WhereSelectListIterator`2<System.Single,System.Object>


// System.Delegate


// System.Delegate


// System.Int32Enum


// System.Int32Enum


// UnityEngine.InputSystem.Utilities.NameAndParameters


// UnityEngine.InputSystem.Utilities.NameAndParameters


// UnityEngine.Pose

struct Pose_t9F30358E65733E60A1DC8682FDB7104F40C9434A_StaticFields
{
public:
	// UnityEngine.Pose UnityEngine.Pose::k_Identity
	Pose_t9F30358E65733E60A1DC8682FDB7104F40C9434A  ___k_Identity_2;

public:
	inline static int32_t get_offset_of_k_Identity_2() { return static_cast<int32_t>(offsetof(Pose_t9F30358E65733E60A1DC8682FDB7104F40C9434A_StaticFields, ___k_Identity_2)); }
	inline Pose_t9F30358E65733E60A1DC8682FDB7104F40C9434A  get_k_Identity_2() const { return ___k_Identity_2; }
	inline Pose_t9F30358E65733E60A1DC8682FDB7104F40C9434A * get_address_of_k_Identity_2() { return &___k_Identity_2; }
	inline void set_k_Identity_2(Pose_t9F30358E65733E60A1DC8682FDB7104F40C9434A  value)
	{
		___k_Identity_2 = value;
	}
};


// UnityEngine.Pose


// System.TypeCode


// System.TypeCode


// UnityEngine.InputSystem.Utilities.JsonParser/JsonString


// UnityEngine.InputSystem.Utilities.JsonParser/JsonString


// UnityEngine.InputSystem.Utilities.JsonParser/JsonValueType


// UnityEngine.InputSystem.Utilities.JsonParser/JsonValueType


// System.Collections.Generic.List`1/Enumerator<System.Int32Enum>


// System.Collections.Generic.List`1/Enumerator<System.Int32Enum>


// System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.NameAndParameters>


// System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.NameAndParameters>


// System.Collections.Generic.List`1/Enumerator<UnityEngine.Pose>


// System.Collections.Generic.List`1/Enumerator<UnityEngine.Pose>


// System.Linq.Enumerable/WhereEnumerableIterator`1<UnityEngine.InputSystem.Utilities.InternedString>


// System.Linq.Enumerable/WhereEnumerableIterator`1<UnityEngine.InputSystem.Utilities.InternedString>


// System.Linq.Enumerable/WhereEnumerableIterator`1<UnityEngine.Quaternion>


// System.Linq.Enumerable/WhereEnumerableIterator`1<UnityEngine.Quaternion>


// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,UnityEngine.InputSystem.Utilities.InternedString>


// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,UnityEngine.InputSystem.Utilities.InternedString>


// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Int32Enum,UnityEngine.InputSystem.Utilities.InternedString>


// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Int32Enum,UnityEngine.InputSystem.Utilities.InternedString>


// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.InternedString,UnityEngine.InputSystem.Utilities.InternedString>


// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.InternedString,UnityEngine.InputSystem.Utilities.InternedString>


// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,UnityEngine.InputSystem.Utilities.InternedString>


// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,UnityEngine.InputSystem.Utilities.InternedString>


// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,UnityEngine.InputSystem.Utilities.InternedString>


// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,UnityEngine.InputSystem.Utilities.InternedString>


// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Object,UnityEngine.InputSystem.Utilities.InternedString>


// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Object,UnityEngine.InputSystem.Utilities.InternedString>


// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.Pose,UnityEngine.Quaternion>


// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.Pose,UnityEngine.Quaternion>


// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Single,UnityEngine.InputSystem.Utilities.InternedString>


// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Single,UnityEngine.InputSystem.Utilities.InternedString>


// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.Substring,UnityEngine.InputSystem.Utilities.InternedString>


// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.Substring,UnityEngine.InputSystem.Utilities.InternedString>


// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,UnityEngine.InputSystem.Utilities.InternedString>


// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,UnityEngine.InputSystem.Utilities.InternedString>


// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.InternedString,UnityEngine.InputSystem.Utilities.InternedString>


// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.InternedString,UnityEngine.InputSystem.Utilities.InternedString>


// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.InternedString,System.Object>


// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.InternedString,System.Object>


// System.Linq.Enumerable/WhereSelectListIterator`2<System.Object,UnityEngine.InputSystem.Utilities.InternedString>


// System.Linq.Enumerable/WhereSelectListIterator`2<System.Object,UnityEngine.InputSystem.Utilities.InternedString>


// System.Linq.Enumerable/WhereSelectListIterator`2<System.Single,UnityEngine.InputSystem.Utilities.InternedString>


// System.Linq.Enumerable/WhereSelectListIterator`2<System.Single,UnityEngine.InputSystem.Utilities.InternedString>


// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.Substring,UnityEngine.InputSystem.Utilities.InternedString>


// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.Substring,UnityEngine.InputSystem.Utilities.InternedString>


// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.Substring,System.Object>


// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.Substring,System.Object>


// Oculus.Interaction.Input.HandSkeletonJoint


// Oculus.Interaction.Input.HandSkeletonJoint


// System.MulticastDelegate


// System.MulticastDelegate


// UnityEngine.InputSystem.Utilities.PrimitiveValue


// UnityEngine.InputSystem.Utilities.PrimitiveValue


// UnityEngine.InputSystem.Utilities.JsonParser/JsonValue


// UnityEngine.InputSystem.Utilities.JsonParser/JsonValue


// System.Collections.Generic.List`1/Enumerator<Oculus.Interaction.Input.HandSkeletonJoint>


// System.Collections.Generic.List`1/Enumerator<Oculus.Interaction.Input.HandSkeletonJoint>


// System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>


// System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>


// System.Func`2<Oculus.Interaction.Input.HandSkeletonJoint,System.Boolean>


// System.Func`2<Oculus.Interaction.Input.HandSkeletonJoint,System.Boolean>


// System.Func`2<Oculus.Interaction.Input.HandSkeletonJoint,Oculus.Interaction.Input.HandSkeletonJoint>


// System.Func`2<Oculus.Interaction.Input.HandSkeletonJoint,Oculus.Interaction.Input.HandSkeletonJoint>


// System.Func`2<System.Int32Enum,System.Boolean>


// System.Func`2<System.Int32Enum,System.Boolean>


// System.Func`2<System.Int32Enum,UnityEngine.InputSystem.Utilities.InternedString>


// System.Func`2<System.Int32Enum,UnityEngine.InputSystem.Utilities.InternedString>


// System.Func`2<System.Int32Enum,System.Object>


// System.Func`2<System.Int32Enum,System.Object>


// System.Func`2<UnityEngine.InputSystem.Utilities.InternedString,System.Boolean>


// System.Func`2<UnityEngine.InputSystem.Utilities.InternedString,System.Boolean>


// System.Func`2<UnityEngine.InputSystem.Utilities.InternedString,UnityEngine.InputSystem.Utilities.InternedString>


// System.Func`2<UnityEngine.InputSystem.Utilities.InternedString,UnityEngine.InputSystem.Utilities.InternedString>


// System.Func`2<UnityEngine.InputSystem.Utilities.InternedString,System.Object>


// System.Func`2<UnityEngine.InputSystem.Utilities.InternedString,System.Object>


// System.Func`2<UnityEngine.InputSystem.Utilities.NameAndParameters,System.Boolean>


// System.Func`2<UnityEngine.InputSystem.Utilities.NameAndParameters,System.Boolean>


// System.Func`2<UnityEngine.InputSystem.Utilities.NameAndParameters,UnityEngine.InputSystem.Utilities.InternedString>


// System.Func`2<UnityEngine.InputSystem.Utilities.NameAndParameters,UnityEngine.InputSystem.Utilities.InternedString>


// System.Func`2<UnityEngine.InputSystem.Utilities.NameAndParameters,System.Object>


// System.Func`2<UnityEngine.InputSystem.Utilities.NameAndParameters,System.Object>


// System.Func`2<System.Object,System.Boolean>


// System.Func`2<System.Object,System.Boolean>


// System.Func`2<System.Object,UnityEngine.InputSystem.Utilities.InternedString>


// System.Func`2<System.Object,UnityEngine.InputSystem.Utilities.InternedString>


// System.Func`2<System.Object,System.Object>


// System.Func`2<System.Object,System.Object>


// System.Func`2<UnityEngine.Pose,System.Boolean>


// System.Func`2<UnityEngine.Pose,System.Boolean>


// System.Func`2<UnityEngine.Pose,UnityEngine.Quaternion>


// System.Func`2<UnityEngine.Pose,UnityEngine.Quaternion>


// System.Func`2<UnityEngine.Quaternion,System.Boolean>


// System.Func`2<UnityEngine.Quaternion,System.Boolean>


// System.Func`2<System.Single,System.Boolean>


// System.Func`2<System.Single,System.Boolean>


// System.Func`2<System.Single,UnityEngine.InputSystem.Utilities.InternedString>


// System.Func`2<System.Single,UnityEngine.InputSystem.Utilities.InternedString>


// System.Func`2<System.Single,System.Object>


// System.Func`2<System.Single,System.Object>


// System.Func`2<UnityEngine.InputSystem.Utilities.Substring,System.Boolean>


// System.Func`2<UnityEngine.InputSystem.Utilities.Substring,System.Boolean>


// System.Func`2<UnityEngine.InputSystem.Utilities.Substring,UnityEngine.InputSystem.Utilities.InternedString>


// System.Func`2<UnityEngine.InputSystem.Utilities.Substring,UnityEngine.InputSystem.Utilities.InternedString>


// System.Func`2<UnityEngine.InputSystem.Utilities.Substring,System.Object>


// System.Func`2<UnityEngine.InputSystem.Utilities.Substring,System.Object>


// System.Func`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,System.Boolean>


// System.Func`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,System.Boolean>


// System.Func`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,UnityEngine.InputSystem.Utilities.InternedString>


// System.Func`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,UnityEngine.InputSystem.Utilities.InternedString>


// System.Func`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,System.Object>


// System.Func`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,System.Object>


// System.Linq.Enumerable/Iterator`1<Oculus.Interaction.Input.HandSkeletonJoint>


// System.Linq.Enumerable/Iterator`1<Oculus.Interaction.Input.HandSkeletonJoint>


// System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>


// System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>


// System.Linq.Enumerable/WhereSelectListIterator`2<System.Int32Enum,UnityEngine.InputSystem.Utilities.InternedString>


// System.Linq.Enumerable/WhereSelectListIterator`2<System.Int32Enum,UnityEngine.InputSystem.Utilities.InternedString>


// System.Linq.Enumerable/WhereSelectListIterator`2<System.Int32Enum,System.Object>


// System.Linq.Enumerable/WhereSelectListIterator`2<System.Int32Enum,System.Object>


// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,UnityEngine.InputSystem.Utilities.InternedString>


// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,UnityEngine.InputSystem.Utilities.InternedString>


// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,System.Object>


// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,System.Object>


// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.Pose,UnityEngine.Quaternion>


// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.Pose,UnityEngine.Quaternion>


// UnityEngine.InputSystem.Utilities.NamedValue


// UnityEngine.InputSystem.Utilities.NamedValue


// System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>>


// System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>>


// System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.NamedValue>


// System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.NamedValue>


// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,System.Boolean>


// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,System.Boolean>


// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,UnityEngine.InputSystem.Utilities.InternedString>


// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,UnityEngine.InputSystem.Utilities.InternedString>


// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,System.Object>


// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,System.Object>


// System.Func`2<UnityEngine.InputSystem.Utilities.NamedValue,System.Boolean>


// System.Func`2<UnityEngine.InputSystem.Utilities.NamedValue,System.Boolean>


// System.Func`2<UnityEngine.InputSystem.Utilities.NamedValue,UnityEngine.InputSystem.Utilities.InternedString>


// System.Func`2<UnityEngine.InputSystem.Utilities.NamedValue,UnityEngine.InputSystem.Utilities.InternedString>


// System.Func`2<UnityEngine.InputSystem.Utilities.NamedValue,System.Object>


// System.Func`2<UnityEngine.InputSystem.Utilities.NamedValue,System.Object>


// System.Linq.Enumerable/WhereEnumerableIterator`1<Oculus.Interaction.Input.HandSkeletonJoint>


// System.Linq.Enumerable/WhereEnumerableIterator`1<Oculus.Interaction.Input.HandSkeletonJoint>


// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<Oculus.Interaction.Input.HandSkeletonJoint,Oculus.Interaction.Input.HandSkeletonJoint>


// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<Oculus.Interaction.Input.HandSkeletonJoint,Oculus.Interaction.Input.HandSkeletonJoint>


// System.Linq.Enumerable/WhereSelectListIterator`2<Oculus.Interaction.Input.HandSkeletonJoint,Oculus.Interaction.Input.HandSkeletonJoint>


// System.Linq.Enumerable/WhereSelectListIterator`2<Oculus.Interaction.Input.HandSkeletonJoint,Oculus.Interaction.Input.HandSkeletonJoint>


// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,UnityEngine.InputSystem.Utilities.InternedString>


// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,UnityEngine.InputSystem.Utilities.InternedString>


// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,System.Object>


// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,System.Object>


// System.Linq.Enumerable/WhereSelectListIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,UnityEngine.InputSystem.Utilities.InternedString>


// System.Linq.Enumerable/WhereSelectListIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,UnityEngine.InputSystem.Utilities.InternedString>


// System.Linq.Enumerable/WhereSelectListIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,System.Object>


// System.Linq.Enumerable/WhereSelectListIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,System.Object>


// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,UnityEngine.InputSystem.Utilities.InternedString>


// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,UnityEngine.InputSystem.Utilities.InternedString>


// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,System.Object>


// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,System.Object>

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Meta.WitAi.Json.JsonConverter[]
struct JsonConverterU5BU5D_t8327F3FEA5C837FBE4A4A8503B5C0B1AD46A5154  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) JsonConverter_t190B6A25CC7F4B027121414C24452DF540968C7F * m_Items[1];

public:
	inline JsonConverter_t190B6A25CC7F4B027121414C24452DF540968C7F * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline JsonConverter_t190B6A25CC7F4B027121414C24452DF540968C7F ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, JsonConverter_t190B6A25CC7F4B027121414C24452DF540968C7F * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline JsonConverter_t190B6A25CC7F4B027121414C24452DF540968C7F * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline JsonConverter_t190B6A25CC7F4B027121414C24452DF540968C7F ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, JsonConverter_t190B6A25CC7F4B027121414C24452DF540968C7F * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};


// !0 System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>>::get_Current()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184  Enumerator_get_Current_mD55E3F632B5C19FA05221B587346BA3268167712_gshared_inline (Enumerator_tF2D0EEBD6B861D9127DD6DC36B733FD42070858C * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_m6A3C20CEFE29B5B761E6C9DBA053A2CE897E2515_gshared (Enumerator_tF2D0EEBD6B861D9127DD6DC36B733FD42070858C * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<Oculus.Interaction.Input.HandSkeletonJoint>::get_Current()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR HandSkeletonJoint_t5B4E2A23FDAC8BC4C8751E5FBC7C96A1056582F3  Enumerator_get_Current_mFF6988CD9E5309A0577F8335E26AA7191198035F_gshared_inline (Enumerator_t749ECD21102D25154785738AC993322CE5A9A3D6 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<Oculus.Interaction.Input.HandSkeletonJoint>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_m7104D812814B404A855DFD8BCE0C67FD044F6377_gshared (Enumerator_t749ECD21102D25154785738AC993322CE5A9A3D6 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<System.Int32Enum>::get_Current()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t Enumerator_get_Current_mDD503AFD786235D3B40842B0872AC17DC86EB040_gshared_inline (Enumerator_t3A873D53A96DA0AC1C030CB5A0BF2D0F57FCC984 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Int32Enum>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_mDECBD2FBFA44578D8E050CECB33BF350152E111A_gshared (Enumerator_t3A873D53A96DA0AC1C030CB5A0BF2D0F57FCC984 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.InternedString>::get_Current()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  Enumerator_get_Current_m3E41B27F955C5A287A6138E9D254833ACC2BC1B2_gshared_inline (Enumerator_tD1ECE103520161C1BD172331D6968D3D184B3B6F * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.InternedString>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_mC755E973A650FFFB74A2F3E332EC10C3F035F15D_gshared (Enumerator_tD1ECE103520161C1BD172331D6968D3D184B3B6F * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.NameAndParameters>::get_Current()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA  Enumerator_get_Current_m0D244550779CCA026ED61FE7767F255D97C413CD_gshared_inline (Enumerator_tA2E0F83E15EA46A2B4B08AFB750700C6E1387FC0 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.NameAndParameters>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_mB1DCA082AFD65B868138F31F1604376EB2580343_gshared (Enumerator_tA2E0F83E15EA46A2B4B08AFB750700C6E1387FC0 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.NamedValue>::get_Current()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077  Enumerator_get_Current_m310A8D3883A91C67B235D82B07D2B234536E2D39_gshared_inline (Enumerator_tDEE846E92D2B22781FB09541CB1600E5398D3A20 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.NamedValue>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_m90AD9DC23572A512C0626351F44778A476DE51B0_gshared (Enumerator_tDEE846E92D2B22781FB09541CB1600E5398D3A20 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject * Enumerator_get_Current_m9C4EBBD2108B51885E750F927D7936290C8E20EE_gshared_inline (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_m2E56233762839CE55C67E00AC8DD3D4D3F6C0DF0_gshared (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.Pose>::get_Current()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Pose_t9F30358E65733E60A1DC8682FDB7104F40C9434A  Enumerator_get_Current_mFDB2FBE724A02C5CAFAE108E12A75676EFE43A5F_gshared_inline (Enumerator_tA7E1DE1C5B2833EE935A19CAF705B2C053EBC9AB * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Pose>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_mF9007138A9A75F58C71B24612243436A5852F2E8_gshared (Enumerator_tA7E1DE1C5B2833EE935A19CAF705B2C053EBC9AB * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<System.Single>::get_Current()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Enumerator_get_Current_m1DC0B40110173B7E2D13319164F7657C3BE3536D_gshared_inline (Enumerator_tC1FD01C8BB5327D442D71FD022B4338ACD701783 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Single>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_mF6D031AEDDDEEAF750E0BFE7866FBBA9C9752C7E_gshared (Enumerator_tC1FD01C8BB5327D442D71FD022B4338ACD701783 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.Substring>::get_Current()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F  Enumerator_get_Current_mDAAD9332BFDBFF6FDFBCC73B8A70BF674E17FE61_gshared_inline (Enumerator_t7C514ACB3FF8E3EFB210F5F45C9F18A715BB4F80 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.Substring>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_mC5829956B29271280A19B86852D365AA6497AB41_gshared (Enumerator_t7C514ACB3FF8E3EFB210F5F45C9F18A715BB4F80 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>::get_Current()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB  Enumerator_get_Current_mCAE9D184024E7F116926D6D43483BF97BFDFA79A_gshared_inline (Enumerator_tF2D95CC18A66F3DE682EAC9B88E865750DFAEF60 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_m084C0187069AC47C975802B0EB3F7257DB6F8507_gshared (Enumerator_tF2D95CC18A66F3DE682EAC9B88E865750DFAEF60 * __this, const RuntimeMethod* method);

// !0 System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>>::get_Current()
inline KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184  Enumerator_get_Current_mD55E3F632B5C19FA05221B587346BA3268167712_inline (Enumerator_tF2D0EEBD6B861D9127DD6DC36B733FD42070858C * __this, const RuntimeMethod* method)
{
	return ((  KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184  (*) (Enumerator_tF2D0EEBD6B861D9127DD6DC36B733FD42070858C *, const RuntimeMethod*))Enumerator_get_Current_mD55E3F632B5C19FA05221B587346BA3268167712_gshared_inline)(__this, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>>::MoveNext()
inline bool Enumerator_MoveNext_m6A3C20CEFE29B5B761E6C9DBA053A2CE897E2515 (Enumerator_tF2D0EEBD6B861D9127DD6DC36B733FD42070858C * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_tF2D0EEBD6B861D9127DD6DC36B733FD42070858C *, const RuntimeMethod*))Enumerator_MoveNext_m6A3C20CEFE29B5B761E6C9DBA053A2CE897E2515_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<Oculus.Interaction.Input.HandSkeletonJoint>::get_Current()
inline HandSkeletonJoint_t5B4E2A23FDAC8BC4C8751E5FBC7C96A1056582F3  Enumerator_get_Current_mFF6988CD9E5309A0577F8335E26AA7191198035F_inline (Enumerator_t749ECD21102D25154785738AC993322CE5A9A3D6 * __this, const RuntimeMethod* method)
{
	return ((  HandSkeletonJoint_t5B4E2A23FDAC8BC4C8751E5FBC7C96A1056582F3  (*) (Enumerator_t749ECD21102D25154785738AC993322CE5A9A3D6 *, const RuntimeMethod*))Enumerator_get_Current_mFF6988CD9E5309A0577F8335E26AA7191198035F_gshared_inline)(__this, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<Oculus.Interaction.Input.HandSkeletonJoint>::MoveNext()
inline bool Enumerator_MoveNext_m7104D812814B404A855DFD8BCE0C67FD044F6377 (Enumerator_t749ECD21102D25154785738AC993322CE5A9A3D6 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_t749ECD21102D25154785738AC993322CE5A9A3D6 *, const RuntimeMethod*))Enumerator_MoveNext_m7104D812814B404A855DFD8BCE0C67FD044F6377_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<System.Int32Enum>::get_Current()
inline int32_t Enumerator_get_Current_mDD503AFD786235D3B40842B0872AC17DC86EB040_inline (Enumerator_t3A873D53A96DA0AC1C030CB5A0BF2D0F57FCC984 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (Enumerator_t3A873D53A96DA0AC1C030CB5A0BF2D0F57FCC984 *, const RuntimeMethod*))Enumerator_get_Current_mDD503AFD786235D3B40842B0872AC17DC86EB040_gshared_inline)(__this, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Int32Enum>::MoveNext()
inline bool Enumerator_MoveNext_mDECBD2FBFA44578D8E050CECB33BF350152E111A (Enumerator_t3A873D53A96DA0AC1C030CB5A0BF2D0F57FCC984 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_t3A873D53A96DA0AC1C030CB5A0BF2D0F57FCC984 *, const RuntimeMethod*))Enumerator_MoveNext_mDECBD2FBFA44578D8E050CECB33BF350152E111A_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.InternedString>::get_Current()
inline InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  Enumerator_get_Current_m3E41B27F955C5A287A6138E9D254833ACC2BC1B2_inline (Enumerator_tD1ECE103520161C1BD172331D6968D3D184B3B6F * __this, const RuntimeMethod* method)
{
	return ((  InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  (*) (Enumerator_tD1ECE103520161C1BD172331D6968D3D184B3B6F *, const RuntimeMethod*))Enumerator_get_Current_m3E41B27F955C5A287A6138E9D254833ACC2BC1B2_gshared_inline)(__this, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.InternedString>::MoveNext()
inline bool Enumerator_MoveNext_mC755E973A650FFFB74A2F3E332EC10C3F035F15D (Enumerator_tD1ECE103520161C1BD172331D6968D3D184B3B6F * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_tD1ECE103520161C1BD172331D6968D3D184B3B6F *, const RuntimeMethod*))Enumerator_MoveNext_mC755E973A650FFFB74A2F3E332EC10C3F035F15D_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.NameAndParameters>::get_Current()
inline NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA  Enumerator_get_Current_m0D244550779CCA026ED61FE7767F255D97C413CD_inline (Enumerator_tA2E0F83E15EA46A2B4B08AFB750700C6E1387FC0 * __this, const RuntimeMethod* method)
{
	return ((  NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA  (*) (Enumerator_tA2E0F83E15EA46A2B4B08AFB750700C6E1387FC0 *, const RuntimeMethod*))Enumerator_get_Current_m0D244550779CCA026ED61FE7767F255D97C413CD_gshared_inline)(__this, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.NameAndParameters>::MoveNext()
inline bool Enumerator_MoveNext_mB1DCA082AFD65B868138F31F1604376EB2580343 (Enumerator_tA2E0F83E15EA46A2B4B08AFB750700C6E1387FC0 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_tA2E0F83E15EA46A2B4B08AFB750700C6E1387FC0 *, const RuntimeMethod*))Enumerator_MoveNext_mB1DCA082AFD65B868138F31F1604376EB2580343_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.NamedValue>::get_Current()
inline NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077  Enumerator_get_Current_m310A8D3883A91C67B235D82B07D2B234536E2D39_inline (Enumerator_tDEE846E92D2B22781FB09541CB1600E5398D3A20 * __this, const RuntimeMethod* method)
{
	return ((  NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077  (*) (Enumerator_tDEE846E92D2B22781FB09541CB1600E5398D3A20 *, const RuntimeMethod*))Enumerator_get_Current_m310A8D3883A91C67B235D82B07D2B234536E2D39_gshared_inline)(__this, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.NamedValue>::MoveNext()
inline bool Enumerator_MoveNext_m90AD9DC23572A512C0626351F44778A476DE51B0 (Enumerator_tDEE846E92D2B22781FB09541CB1600E5398D3A20 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_tDEE846E92D2B22781FB09541CB1600E5398D3A20 *, const RuntimeMethod*))Enumerator_MoveNext_m90AD9DC23572A512C0626351F44778A476DE51B0_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
inline RuntimeObject * Enumerator_get_Current_m9C4EBBD2108B51885E750F927D7936290C8E20EE_inline (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * __this, const RuntimeMethod* method)
{
	return ((  RuntimeObject * (*) (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *, const RuntimeMethod*))Enumerator_get_Current_m9C4EBBD2108B51885E750F927D7936290C8E20EE_gshared_inline)(__this, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
inline bool Enumerator_MoveNext_m2E56233762839CE55C67E00AC8DD3D4D3F6C0DF0 (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *, const RuntimeMethod*))Enumerator_MoveNext_m2E56233762839CE55C67E00AC8DD3D4D3F6C0DF0_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.Pose>::get_Current()
inline Pose_t9F30358E65733E60A1DC8682FDB7104F40C9434A  Enumerator_get_Current_mFDB2FBE724A02C5CAFAE108E12A75676EFE43A5F_inline (Enumerator_tA7E1DE1C5B2833EE935A19CAF705B2C053EBC9AB * __this, const RuntimeMethod* method)
{
	return ((  Pose_t9F30358E65733E60A1DC8682FDB7104F40C9434A  (*) (Enumerator_tA7E1DE1C5B2833EE935A19CAF705B2C053EBC9AB *, const RuntimeMethod*))Enumerator_get_Current_mFDB2FBE724A02C5CAFAE108E12A75676EFE43A5F_gshared_inline)(__this, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Pose>::MoveNext()
inline bool Enumerator_MoveNext_mF9007138A9A75F58C71B24612243436A5852F2E8 (Enumerator_tA7E1DE1C5B2833EE935A19CAF705B2C053EBC9AB * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_tA7E1DE1C5B2833EE935A19CAF705B2C053EBC9AB *, const RuntimeMethod*))Enumerator_MoveNext_mF9007138A9A75F58C71B24612243436A5852F2E8_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<System.Single>::get_Current()
inline float Enumerator_get_Current_m1DC0B40110173B7E2D13319164F7657C3BE3536D_inline (Enumerator_tC1FD01C8BB5327D442D71FD022B4338ACD701783 * __this, const RuntimeMethod* method)
{
	return ((  float (*) (Enumerator_tC1FD01C8BB5327D442D71FD022B4338ACD701783 *, const RuntimeMethod*))Enumerator_get_Current_m1DC0B40110173B7E2D13319164F7657C3BE3536D_gshared_inline)(__this, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Single>::MoveNext()
inline bool Enumerator_MoveNext_mF6D031AEDDDEEAF750E0BFE7866FBBA9C9752C7E (Enumerator_tC1FD01C8BB5327D442D71FD022B4338ACD701783 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_tC1FD01C8BB5327D442D71FD022B4338ACD701783 *, const RuntimeMethod*))Enumerator_MoveNext_mF6D031AEDDDEEAF750E0BFE7866FBBA9C9752C7E_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.Substring>::get_Current()
inline Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F  Enumerator_get_Current_mDAAD9332BFDBFF6FDFBCC73B8A70BF674E17FE61_inline (Enumerator_t7C514ACB3FF8E3EFB210F5F45C9F18A715BB4F80 * __this, const RuntimeMethod* method)
{
	return ((  Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F  (*) (Enumerator_t7C514ACB3FF8E3EFB210F5F45C9F18A715BB4F80 *, const RuntimeMethod*))Enumerator_get_Current_mDAAD9332BFDBFF6FDFBCC73B8A70BF674E17FE61_gshared_inline)(__this, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.Substring>::MoveNext()
inline bool Enumerator_MoveNext_mC5829956B29271280A19B86852D365AA6497AB41 (Enumerator_t7C514ACB3FF8E3EFB210F5F45C9F18A715BB4F80 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_t7C514ACB3FF8E3EFB210F5F45C9F18A715BB4F80 *, const RuntimeMethod*))Enumerator_MoveNext_mC5829956B29271280A19B86852D365AA6497AB41_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>::get_Current()
inline JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB  Enumerator_get_Current_mCAE9D184024E7F116926D6D43483BF97BFDFA79A_inline (Enumerator_tF2D95CC18A66F3DE682EAC9B88E865750DFAEF60 * __this, const RuntimeMethod* method)
{
	return ((  JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB  (*) (Enumerator_tF2D95CC18A66F3DE682EAC9B88E865750DFAEF60 *, const RuntimeMethod*))Enumerator_get_Current_mCAE9D184024E7F116926D6D43483BF97BFDFA79A_gshared_inline)(__this, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>::MoveNext()
inline bool Enumerator_MoveNext_m084C0187069AC47C975802B0EB3F7257DB6F8507 (Enumerator_tF2D95CC18A66F3DE682EAC9B88E865750DFAEF60 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_tF2D95CC18A66F3DE682EAC9B88E865750DFAEF60 *, const RuntimeMethod*))Enumerator_MoveNext_m084C0187069AC47C975802B0EB3F7257DB6F8507_gshared)(__this, method);
}
// System.String System.Int32::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411 (int32_t* __this, const RuntimeMethod* method);
// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405 (RuntimeObject * __this, const RuntimeMethod* method);
// System.String System.Single::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Single_ToString_m80E7ABED4F4D73F2BE19DDB80D3D92FCD8DFA010 (float* __this, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,UnityEngine.InputSystem.Utilities.InternedString>::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2__ctor_mCE6FF531B9E6C7942D9AB99C4DE8A563FF1B686B_gshared (WhereSelectEnumerableIterator_2_t5171F478E1C976F4C353EA25EC64567E44C86CA0 * __this, RuntimeObject* ___source0, Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 * ___predicate1, Func_2_tB181F4C9A24C0634DF4A10CFBFFE6B45B3796BBC * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		((  void (*) (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		RuntimeObject* L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_tB181F4C9A24C0634DF4A10CFBFFE6B45B3796BBC * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,UnityEngine.InputSystem.Utilities.InternedString>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 * WhereSelectEnumerableIterator_2_Clone_m1A04384C10810F8E2559DBD0004219CECDD65953_gshared (WhereSelectEnumerableIterator_2_t5171F478E1C976F4C353EA25EC64567E44C86CA0 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_source_3();
		Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 * L_1 = (Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 *)__this->get_predicate_4();
		Func_2_tB181F4C9A24C0634DF4A10CFBFFE6B45B3796BBC * L_2 = (Func_2_tB181F4C9A24C0634DF4A10CFBFFE6B45B3796BBC *)__this->get_selector_5();
		WhereSelectEnumerableIterator_2_t5171F478E1C976F4C353EA25EC64567E44C86CA0 * L_3 = (WhereSelectEnumerableIterator_2_t5171F478E1C976F4C353EA25EC64567E44C86CA0 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectEnumerableIterator_2_t5171F478E1C976F4C353EA25EC64567E44C86CA0 *, RuntimeObject*, Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 *, Func_2_tB181F4C9A24C0634DF4A10CFBFFE6B45B3796BBC *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (RuntimeObject*)L_0, (Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 *)L_1, (Func_2_tB181F4C9A24C0634DF4A10CFBFFE6B45B3796BBC *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)L_3;
	}
}
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,UnityEngine.InputSystem.Utilities.InternedString>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2_Dispose_mE75D949168D88B07AA3F34D49CC94AB44AE57674_gshared (WhereSelectEnumerableIterator_2_t5171F478E1C976F4C353EA25EC64567E44C86CA0 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_enumerator_6();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		RuntimeObject* L_1 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, (RuntimeObject*)L_1);
	}

IL_0013:
	{
		__this->set_enumerator_6((RuntimeObject*)NULL);
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		((  void (*) (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		return;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,UnityEngine.InputSystem.Utilities.InternedString>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectEnumerableIterator_2_MoveNext_m5692381D325B5682E63435202054A5169B54AFB6_gshared (WhereSelectEnumerableIterator_2_t5171F478E1C976F4C353EA25EC64567E44C86CA0 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		RuntimeObject* L_3 = (RuntimeObject*)__this->get_source_3();
		NullCheck((RuntimeObject*)L_3);
		RuntimeObject* L_4;
		L_4 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 5), (RuntimeObject*)L_3);
		__this->set_enumerator_6(L_4);
		((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		RuntimeObject* L_5 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_5);
		KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184  L_6;
		L_6 = InterfaceFuncInvoker0< KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>>::get_Current() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 6), (RuntimeObject*)L_5);
		V_1 = (KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184 )L_6;
		Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 * L_7 = (Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 * L_8 = (Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 *)__this->get_predicate_4();
		KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184  L_9 = V_1;
		NullCheck((Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 *, KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 *)L_8, (KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184 )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_tB181F4C9A24C0634DF4A10CFBFFE6B45B3796BBC * L_11 = (Func_2_tB181F4C9A24C0634DF4A10CFBFFE6B45B3796BBC *)__this->get_selector_5();
		KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184  L_12 = V_1;
		NullCheck((Func_2_tB181F4C9A24C0634DF4A10CFBFFE6B45B3796BBC *)L_11);
		InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  L_13;
		L_13 = ((  InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  (*) (Func_2_tB181F4C9A24C0634DF4A10CFBFFE6B45B3796BBC *, KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((Func_2_tB181F4C9A24C0634DF4A10CFBFFE6B45B3796BBC *)L_11, (KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184 )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		RuntimeObject* L_14 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_14);
		bool L_15;
		L_15 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, (RuntimeObject*)L_14);
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<UnityEngine.InputSystem.Utilities.InternedString>::Dispose() */, (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,UnityEngine.InputSystem.Utilities.InternedString>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectEnumerableIterator_2_Where_m580ADE679A9E8BD04C7288A50D41B7402D80FAEC_gshared (WhereSelectEnumerableIterator_2_t5171F478E1C976F4C353EA25EC64567E44C86CA0 * __this, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B * L_1 = (WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 9));
		((  void (*) (WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B *, RuntimeObject*, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,System.Object>::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2__ctor_m3F22AE927F7706C1BD22F3EC5E7E471F41F529B4_gshared (WhereSelectEnumerableIterator_2_t0ACED5698603E2D2B4071096A021E0FA6102F459 * __this, RuntimeObject* ___source0, Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 * ___predicate1, Func_2_t711512467B454C91EB8F3FB19CD6D141AAB5B6B1 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		RuntimeObject* L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t711512467B454C91EB8F3FB19CD6D141AAB5B6B1 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,System.Object>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 * WhereSelectEnumerableIterator_2_Clone_mF267F91ABE1011FD3ADD1B0BC5C76AC1E77F6262_gshared (WhereSelectEnumerableIterator_2_t0ACED5698603E2D2B4071096A021E0FA6102F459 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_source_3();
		Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 * L_1 = (Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 *)__this->get_predicate_4();
		Func_2_t711512467B454C91EB8F3FB19CD6D141AAB5B6B1 * L_2 = (Func_2_t711512467B454C91EB8F3FB19CD6D141AAB5B6B1 *)__this->get_selector_5();
		WhereSelectEnumerableIterator_2_t0ACED5698603E2D2B4071096A021E0FA6102F459 * L_3 = (WhereSelectEnumerableIterator_2_t0ACED5698603E2D2B4071096A021E0FA6102F459 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectEnumerableIterator_2_t0ACED5698603E2D2B4071096A021E0FA6102F459 *, RuntimeObject*, Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 *, Func_2_t711512467B454C91EB8F3FB19CD6D141AAB5B6B1 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (RuntimeObject*)L_0, (Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 *)L_1, (Func_2_t711512467B454C91EB8F3FB19CD6D141AAB5B6B1 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)L_3;
	}
}
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,System.Object>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2_Dispose_m9D3DD396CB8ED1F29264B395F3E371FFD2AA382F_gshared (WhereSelectEnumerableIterator_2_t0ACED5698603E2D2B4071096A021E0FA6102F459 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_enumerator_6();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		RuntimeObject* L_1 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, (RuntimeObject*)L_1);
	}

IL_0013:
	{
		__this->set_enumerator_6((RuntimeObject*)NULL);
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		return;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectEnumerableIterator_2_MoveNext_mEE8CA6C28994E2E78429899A99B8EB3C35A52E6E_gshared (WhereSelectEnumerableIterator_2_t0ACED5698603E2D2B4071096A021E0FA6102F459 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		RuntimeObject* L_3 = (RuntimeObject*)__this->get_source_3();
		NullCheck((RuntimeObject*)L_3);
		RuntimeObject* L_4;
		L_4 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 5), (RuntimeObject*)L_3);
		__this->set_enumerator_6(L_4);
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		RuntimeObject* L_5 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_5);
		KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184  L_6;
		L_6 = InterfaceFuncInvoker0< KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>>::get_Current() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 6), (RuntimeObject*)L_5);
		V_1 = (KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184 )L_6;
		Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 * L_7 = (Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 * L_8 = (Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 *)__this->get_predicate_4();
		KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184  L_9 = V_1;
		NullCheck((Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 *, KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 *)L_8, (KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184 )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t711512467B454C91EB8F3FB19CD6D141AAB5B6B1 * L_11 = (Func_2_t711512467B454C91EB8F3FB19CD6D141AAB5B6B1 *)__this->get_selector_5();
		KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184  L_12 = V_1;
		NullCheck((Func_2_t711512467B454C91EB8F3FB19CD6D141AAB5B6B1 *)L_11);
		RuntimeObject * L_13;
		L_13 = ((  RuntimeObject * (*) (Func_2_t711512467B454C91EB8F3FB19CD6D141AAB5B6B1 *, KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((Func_2_t711512467B454C91EB8F3FB19CD6D141AAB5B6B1 *)L_11, (KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184 )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		RuntimeObject* L_14 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_14);
		bool L_15;
		L_15 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, (RuntimeObject*)L_14);
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Object>::Dispose() */, (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,System.Object>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectEnumerableIterator_2_Where_m77DCF85BB2A929D85209E0449F244B60D00C1558_gshared (WhereSelectEnumerableIterator_2_t0ACED5698603E2D2B4071096A021E0FA6102F459 * __this, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 * L_1 = (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 9));
		((  void (*) (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *, RuntimeObject*, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<Oculus.Interaction.Input.HandSkeletonJoint,Oculus.Interaction.Input.HandSkeletonJoint>::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2__ctor_mE674D71BB7643F2087791DC98229F50951123628_gshared (WhereSelectEnumerableIterator_2_t04AB31927A6E89520049EC6E92923D1FBAC994AB * __this, RuntimeObject* ___source0, Func_2_t31607A88702EFEFC5B33245A610C64EA7D6ED9F7 * ___predicate1, Func_2_tB339A94B37E01030C823BB96512593067BEF80DC * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_tBD3F91668D911F5945632A9FDF70A5848BD6CC26 *)__this);
		((  void (*) (Iterator_1_tBD3F91668D911F5945632A9FDF70A5848BD6CC26 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_tBD3F91668D911F5945632A9FDF70A5848BD6CC26 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		RuntimeObject* L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t31607A88702EFEFC5B33245A610C64EA7D6ED9F7 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_tB339A94B37E01030C823BB96512593067BEF80DC * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<Oculus.Interaction.Input.HandSkeletonJoint,Oculus.Interaction.Input.HandSkeletonJoint>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_tBD3F91668D911F5945632A9FDF70A5848BD6CC26 * WhereSelectEnumerableIterator_2_Clone_m6382C1E0D8D3836AC95B4FB10D3DEC6CF522E341_gshared (WhereSelectEnumerableIterator_2_t04AB31927A6E89520049EC6E92923D1FBAC994AB * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_source_3();
		Func_2_t31607A88702EFEFC5B33245A610C64EA7D6ED9F7 * L_1 = (Func_2_t31607A88702EFEFC5B33245A610C64EA7D6ED9F7 *)__this->get_predicate_4();
		Func_2_tB339A94B37E01030C823BB96512593067BEF80DC * L_2 = (Func_2_tB339A94B37E01030C823BB96512593067BEF80DC *)__this->get_selector_5();
		WhereSelectEnumerableIterator_2_t04AB31927A6E89520049EC6E92923D1FBAC994AB * L_3 = (WhereSelectEnumerableIterator_2_t04AB31927A6E89520049EC6E92923D1FBAC994AB *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectEnumerableIterator_2_t04AB31927A6E89520049EC6E92923D1FBAC994AB *, RuntimeObject*, Func_2_t31607A88702EFEFC5B33245A610C64EA7D6ED9F7 *, Func_2_tB339A94B37E01030C823BB96512593067BEF80DC *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (RuntimeObject*)L_0, (Func_2_t31607A88702EFEFC5B33245A610C64EA7D6ED9F7 *)L_1, (Func_2_tB339A94B37E01030C823BB96512593067BEF80DC *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_tBD3F91668D911F5945632A9FDF70A5848BD6CC26 *)L_3;
	}
}
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<Oculus.Interaction.Input.HandSkeletonJoint,Oculus.Interaction.Input.HandSkeletonJoint>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2_Dispose_m1FFB738E338A7CFDD08292150B606A11C85E3668_gshared (WhereSelectEnumerableIterator_2_t04AB31927A6E89520049EC6E92923D1FBAC994AB * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_enumerator_6();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		RuntimeObject* L_1 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, (RuntimeObject*)L_1);
	}

IL_0013:
	{
		__this->set_enumerator_6((RuntimeObject*)NULL);
		NullCheck((Iterator_1_tBD3F91668D911F5945632A9FDF70A5848BD6CC26 *)__this);
		((  void (*) (Iterator_1_tBD3F91668D911F5945632A9FDF70A5848BD6CC26 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Iterator_1_tBD3F91668D911F5945632A9FDF70A5848BD6CC26 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		return;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectEnumerableIterator`2<Oculus.Interaction.Input.HandSkeletonJoint,Oculus.Interaction.Input.HandSkeletonJoint>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectEnumerableIterator_2_MoveNext_m28625AB323BF25AE86EE0C8F16C080E910467E01_gshared (WhereSelectEnumerableIterator_2_t04AB31927A6E89520049EC6E92923D1FBAC994AB * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	HandSkeletonJoint_t5B4E2A23FDAC8BC4C8751E5FBC7C96A1056582F3  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_tBD3F91668D911F5945632A9FDF70A5848BD6CC26 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		RuntimeObject* L_3 = (RuntimeObject*)__this->get_source_3();
		NullCheck((RuntimeObject*)L_3);
		RuntimeObject* L_4;
		L_4 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<Oculus.Interaction.Input.HandSkeletonJoint>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 5), (RuntimeObject*)L_3);
		__this->set_enumerator_6(L_4);
		((Iterator_1_tBD3F91668D911F5945632A9FDF70A5848BD6CC26 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		RuntimeObject* L_5 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_5);
		HandSkeletonJoint_t5B4E2A23FDAC8BC4C8751E5FBC7C96A1056582F3  L_6;
		L_6 = InterfaceFuncInvoker0< HandSkeletonJoint_t5B4E2A23FDAC8BC4C8751E5FBC7C96A1056582F3  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<Oculus.Interaction.Input.HandSkeletonJoint>::get_Current() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 6), (RuntimeObject*)L_5);
		V_1 = (HandSkeletonJoint_t5B4E2A23FDAC8BC4C8751E5FBC7C96A1056582F3 )L_6;
		Func_2_t31607A88702EFEFC5B33245A610C64EA7D6ED9F7 * L_7 = (Func_2_t31607A88702EFEFC5B33245A610C64EA7D6ED9F7 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t31607A88702EFEFC5B33245A610C64EA7D6ED9F7 * L_8 = (Func_2_t31607A88702EFEFC5B33245A610C64EA7D6ED9F7 *)__this->get_predicate_4();
		HandSkeletonJoint_t5B4E2A23FDAC8BC4C8751E5FBC7C96A1056582F3  L_9 = V_1;
		NullCheck((Func_2_t31607A88702EFEFC5B33245A610C64EA7D6ED9F7 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t31607A88702EFEFC5B33245A610C64EA7D6ED9F7 *, HandSkeletonJoint_t5B4E2A23FDAC8BC4C8751E5FBC7C96A1056582F3 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t31607A88702EFEFC5B33245A610C64EA7D6ED9F7 *)L_8, (HandSkeletonJoint_t5B4E2A23FDAC8BC4C8751E5FBC7C96A1056582F3 )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_tB339A94B37E01030C823BB96512593067BEF80DC * L_11 = (Func_2_tB339A94B37E01030C823BB96512593067BEF80DC *)__this->get_selector_5();
		HandSkeletonJoint_t5B4E2A23FDAC8BC4C8751E5FBC7C96A1056582F3  L_12 = V_1;
		NullCheck((Func_2_tB339A94B37E01030C823BB96512593067BEF80DC *)L_11);
		HandSkeletonJoint_t5B4E2A23FDAC8BC4C8751E5FBC7C96A1056582F3  L_13;
		L_13 = ((  HandSkeletonJoint_t5B4E2A23FDAC8BC4C8751E5FBC7C96A1056582F3  (*) (Func_2_tB339A94B37E01030C823BB96512593067BEF80DC *, HandSkeletonJoint_t5B4E2A23FDAC8BC4C8751E5FBC7C96A1056582F3 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((Func_2_tB339A94B37E01030C823BB96512593067BEF80DC *)L_11, (HandSkeletonJoint_t5B4E2A23FDAC8BC4C8751E5FBC7C96A1056582F3 )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		((Iterator_1_tBD3F91668D911F5945632A9FDF70A5848BD6CC26 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		RuntimeObject* L_14 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_14);
		bool L_15;
		L_15 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, (RuntimeObject*)L_14);
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_tBD3F91668D911F5945632A9FDF70A5848BD6CC26 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<Oculus.Interaction.Input.HandSkeletonJoint>::Dispose() */, (Iterator_1_tBD3F91668D911F5945632A9FDF70A5848BD6CC26 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<Oculus.Interaction.Input.HandSkeletonJoint,Oculus.Interaction.Input.HandSkeletonJoint>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectEnumerableIterator_2_Where_m439F98BD24C0DFC4E3974B375DFDF8BEC8AED1F3_gshared (WhereSelectEnumerableIterator_2_t04AB31927A6E89520049EC6E92923D1FBAC994AB * __this, Func_2_t31607A88702EFEFC5B33245A610C64EA7D6ED9F7 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t31607A88702EFEFC5B33245A610C64EA7D6ED9F7 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t7E214A8B0456E617DE0100DC5CA366442CB22EBD * L_1 = (WhereEnumerableIterator_1_t7E214A8B0456E617DE0100DC5CA366442CB22EBD *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 9));
		((  void (*) (WhereEnumerableIterator_1_t7E214A8B0456E617DE0100DC5CA366442CB22EBD *, RuntimeObject*, Func_2_t31607A88702EFEFC5B33245A610C64EA7D6ED9F7 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t31607A88702EFEFC5B33245A610C64EA7D6ED9F7 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Int32Enum,UnityEngine.InputSystem.Utilities.InternedString>::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2__ctor_m9F128BB2F5AE30D038DC18A6C942EA0E6A4909B5_gshared (WhereSelectEnumerableIterator_2_t4F5D6E88F86A03D2A74E02E6BC9FBD56A793C28A * __this, RuntimeObject* ___source0, Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E * ___predicate1, Func_2_t043042489DBF19074B5B434F090F7B8AAE97E138 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		((  void (*) (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		RuntimeObject* L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t043042489DBF19074B5B434F090F7B8AAE97E138 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Int32Enum,UnityEngine.InputSystem.Utilities.InternedString>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 * WhereSelectEnumerableIterator_2_Clone_m4517C05C17D9E2F8749CD64A816F44BA0837DDA0_gshared (WhereSelectEnumerableIterator_2_t4F5D6E88F86A03D2A74E02E6BC9FBD56A793C28A * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_source_3();
		Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E * L_1 = (Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E *)__this->get_predicate_4();
		Func_2_t043042489DBF19074B5B434F090F7B8AAE97E138 * L_2 = (Func_2_t043042489DBF19074B5B434F090F7B8AAE97E138 *)__this->get_selector_5();
		WhereSelectEnumerableIterator_2_t4F5D6E88F86A03D2A74E02E6BC9FBD56A793C28A * L_3 = (WhereSelectEnumerableIterator_2_t4F5D6E88F86A03D2A74E02E6BC9FBD56A793C28A *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectEnumerableIterator_2_t4F5D6E88F86A03D2A74E02E6BC9FBD56A793C28A *, RuntimeObject*, Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E *, Func_2_t043042489DBF19074B5B434F090F7B8AAE97E138 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (RuntimeObject*)L_0, (Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E *)L_1, (Func_2_t043042489DBF19074B5B434F090F7B8AAE97E138 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)L_3;
	}
}
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Int32Enum,UnityEngine.InputSystem.Utilities.InternedString>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2_Dispose_mE37C1FBE5E6A0C1A12E1261BDE67E3B3EBD6A8FD_gshared (WhereSelectEnumerableIterator_2_t4F5D6E88F86A03D2A74E02E6BC9FBD56A793C28A * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_enumerator_6();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		RuntimeObject* L_1 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, (RuntimeObject*)L_1);
	}

IL_0013:
	{
		__this->set_enumerator_6((RuntimeObject*)NULL);
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		((  void (*) (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		return;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Int32Enum,UnityEngine.InputSystem.Utilities.InternedString>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectEnumerableIterator_2_MoveNext_mF505C51B6F20BFCCA27C6EAC9ADE053B27E3E7F8_gshared (WhereSelectEnumerableIterator_2_t4F5D6E88F86A03D2A74E02E6BC9FBD56A793C28A * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = (int32_t)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		RuntimeObject* L_3 = (RuntimeObject*)__this->get_source_3();
		NullCheck((RuntimeObject*)L_3);
		RuntimeObject* L_4;
		L_4 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Int32Enum>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 5), (RuntimeObject*)L_3);
		__this->set_enumerator_6(L_4);
		((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		RuntimeObject* L_5 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_5);
		int32_t L_6;
		L_6 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Int32Enum>::get_Current() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 6), (RuntimeObject*)L_5);
		V_1 = (int32_t)L_6;
		Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E * L_7 = (Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E * L_8 = (Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E *)__this->get_predicate_4();
		int32_t L_9 = V_1;
		NullCheck((Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E *)L_8, (int32_t)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t043042489DBF19074B5B434F090F7B8AAE97E138 * L_11 = (Func_2_t043042489DBF19074B5B434F090F7B8AAE97E138 *)__this->get_selector_5();
		int32_t L_12 = V_1;
		NullCheck((Func_2_t043042489DBF19074B5B434F090F7B8AAE97E138 *)L_11);
		InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  L_13;
		L_13 = ((  InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  (*) (Func_2_t043042489DBF19074B5B434F090F7B8AAE97E138 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((Func_2_t043042489DBF19074B5B434F090F7B8AAE97E138 *)L_11, (int32_t)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		RuntimeObject* L_14 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_14);
		bool L_15;
		L_15 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, (RuntimeObject*)L_14);
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<UnityEngine.InputSystem.Utilities.InternedString>::Dispose() */, (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Int32Enum,UnityEngine.InputSystem.Utilities.InternedString>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectEnumerableIterator_2_Where_mFF94921DC8FFBCF38A1ABA4205E054CBB29237D0_gshared (WhereSelectEnumerableIterator_2_t4F5D6E88F86A03D2A74E02E6BC9FBD56A793C28A * __this, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B * L_1 = (WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 9));
		((  void (*) (WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B *, RuntimeObject*, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Int32Enum,System.Object>::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2__ctor_m47635847AC02479D7C58622F296DCBF88E18C4B0_gshared (WhereSelectEnumerableIterator_2_tE7CB1EB0F91FF8330C8F18AFE379A77EDF9F91F6 * __this, RuntimeObject* ___source0, Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E * ___predicate1, Func_2_tF916A06E36F9340A8A2E0053D6A8F0737C5FD2EB * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		RuntimeObject* L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_tF916A06E36F9340A8A2E0053D6A8F0737C5FD2EB * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Int32Enum,System.Object>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 * WhereSelectEnumerableIterator_2_Clone_mF150C4600D50245151AE0E8029CC7CBAB303E754_gshared (WhereSelectEnumerableIterator_2_tE7CB1EB0F91FF8330C8F18AFE379A77EDF9F91F6 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_source_3();
		Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E * L_1 = (Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E *)__this->get_predicate_4();
		Func_2_tF916A06E36F9340A8A2E0053D6A8F0737C5FD2EB * L_2 = (Func_2_tF916A06E36F9340A8A2E0053D6A8F0737C5FD2EB *)__this->get_selector_5();
		WhereSelectEnumerableIterator_2_tE7CB1EB0F91FF8330C8F18AFE379A77EDF9F91F6 * L_3 = (WhereSelectEnumerableIterator_2_tE7CB1EB0F91FF8330C8F18AFE379A77EDF9F91F6 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectEnumerableIterator_2_tE7CB1EB0F91FF8330C8F18AFE379A77EDF9F91F6 *, RuntimeObject*, Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E *, Func_2_tF916A06E36F9340A8A2E0053D6A8F0737C5FD2EB *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (RuntimeObject*)L_0, (Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E *)L_1, (Func_2_tF916A06E36F9340A8A2E0053D6A8F0737C5FD2EB *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)L_3;
	}
}
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Int32Enum,System.Object>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2_Dispose_m71E88D558F1C66319831455654381D3FD5E6FE94_gshared (WhereSelectEnumerableIterator_2_tE7CB1EB0F91FF8330C8F18AFE379A77EDF9F91F6 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_enumerator_6();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		RuntimeObject* L_1 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, (RuntimeObject*)L_1);
	}

IL_0013:
	{
		__this->set_enumerator_6((RuntimeObject*)NULL);
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		return;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Int32Enum,System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectEnumerableIterator_2_MoveNext_m8A98578C5E3B29ACA85548212356E9619C83FF3C_gshared (WhereSelectEnumerableIterator_2_tE7CB1EB0F91FF8330C8F18AFE379A77EDF9F91F6 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = (int32_t)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		RuntimeObject* L_3 = (RuntimeObject*)__this->get_source_3();
		NullCheck((RuntimeObject*)L_3);
		RuntimeObject* L_4;
		L_4 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Int32Enum>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 5), (RuntimeObject*)L_3);
		__this->set_enumerator_6(L_4);
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		RuntimeObject* L_5 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_5);
		int32_t L_6;
		L_6 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Int32Enum>::get_Current() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 6), (RuntimeObject*)L_5);
		V_1 = (int32_t)L_6;
		Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E * L_7 = (Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E * L_8 = (Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E *)__this->get_predicate_4();
		int32_t L_9 = V_1;
		NullCheck((Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E *)L_8, (int32_t)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_tF916A06E36F9340A8A2E0053D6A8F0737C5FD2EB * L_11 = (Func_2_tF916A06E36F9340A8A2E0053D6A8F0737C5FD2EB *)__this->get_selector_5();
		int32_t L_12 = V_1;
		NullCheck((Func_2_tF916A06E36F9340A8A2E0053D6A8F0737C5FD2EB *)L_11);
		RuntimeObject * L_13;
		L_13 = ((  RuntimeObject * (*) (Func_2_tF916A06E36F9340A8A2E0053D6A8F0737C5FD2EB *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((Func_2_tF916A06E36F9340A8A2E0053D6A8F0737C5FD2EB *)L_11, (int32_t)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		RuntimeObject* L_14 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_14);
		bool L_15;
		L_15 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, (RuntimeObject*)L_14);
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Object>::Dispose() */, (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Int32Enum,System.Object>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectEnumerableIterator_2_Where_m02212A04F4819B1D035DD77EAFBF32B448CB8F14_gshared (WhereSelectEnumerableIterator_2_tE7CB1EB0F91FF8330C8F18AFE379A77EDF9F91F6 * __this, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 * L_1 = (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 9));
		((  void (*) (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *, RuntimeObject*, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.InternedString,UnityEngine.InputSystem.Utilities.InternedString>::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2__ctor_mAFFDB224C4F7BE6864A8BB7E7A6C2F0ED67E942E_gshared (WhereSelectEnumerableIterator_2_t0EB79FD57DE6939EBF78FA1D92312CB62DD3F2C6 * __this, RuntimeObject* ___source0, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * ___predicate1, Func_2_t1AF155D27A4D78882342D697E4A6DE7CE594545B * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		((  void (*) (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		RuntimeObject* L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t1AF155D27A4D78882342D697E4A6DE7CE594545B * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.InternedString,UnityEngine.InputSystem.Utilities.InternedString>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 * WhereSelectEnumerableIterator_2_Clone_mEB2DEA193B6BE4D45F986EB587B7C5341811A67D_gshared (WhereSelectEnumerableIterator_2_t0EB79FD57DE6939EBF78FA1D92312CB62DD3F2C6 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_source_3();
		Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * L_1 = (Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)__this->get_predicate_4();
		Func_2_t1AF155D27A4D78882342D697E4A6DE7CE594545B * L_2 = (Func_2_t1AF155D27A4D78882342D697E4A6DE7CE594545B *)__this->get_selector_5();
		WhereSelectEnumerableIterator_2_t0EB79FD57DE6939EBF78FA1D92312CB62DD3F2C6 * L_3 = (WhereSelectEnumerableIterator_2_t0EB79FD57DE6939EBF78FA1D92312CB62DD3F2C6 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectEnumerableIterator_2_t0EB79FD57DE6939EBF78FA1D92312CB62DD3F2C6 *, RuntimeObject*, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *, Func_2_t1AF155D27A4D78882342D697E4A6DE7CE594545B *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (RuntimeObject*)L_0, (Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)L_1, (Func_2_t1AF155D27A4D78882342D697E4A6DE7CE594545B *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)L_3;
	}
}
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.InternedString,UnityEngine.InputSystem.Utilities.InternedString>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2_Dispose_mBE8FA402107BBA57A0E4E27EAF99DA825506A209_gshared (WhereSelectEnumerableIterator_2_t0EB79FD57DE6939EBF78FA1D92312CB62DD3F2C6 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_enumerator_6();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		RuntimeObject* L_1 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, (RuntimeObject*)L_1);
	}

IL_0013:
	{
		__this->set_enumerator_6((RuntimeObject*)NULL);
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		((  void (*) (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		return;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.InternedString,UnityEngine.InputSystem.Utilities.InternedString>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectEnumerableIterator_2_MoveNext_m63C0EC5F395EF1117EF74EC428A2B114BF889A6A_gshared (WhereSelectEnumerableIterator_2_t0EB79FD57DE6939EBF78FA1D92312CB62DD3F2C6 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		RuntimeObject* L_3 = (RuntimeObject*)__this->get_source_3();
		NullCheck((RuntimeObject*)L_3);
		RuntimeObject* L_4;
		L_4 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<UnityEngine.InputSystem.Utilities.InternedString>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 5), (RuntimeObject*)L_3);
		__this->set_enumerator_6(L_4);
		((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		RuntimeObject* L_5 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_5);
		InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  L_6;
		L_6 = InterfaceFuncInvoker0< InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<UnityEngine.InputSystem.Utilities.InternedString>::get_Current() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 6), (RuntimeObject*)L_5);
		V_1 = (InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED )L_6;
		Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * L_7 = (Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * L_8 = (Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)__this->get_predicate_4();
		InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  L_9 = V_1;
		NullCheck((Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *, InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)L_8, (InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t1AF155D27A4D78882342D697E4A6DE7CE594545B * L_11 = (Func_2_t1AF155D27A4D78882342D697E4A6DE7CE594545B *)__this->get_selector_5();
		InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  L_12 = V_1;
		NullCheck((Func_2_t1AF155D27A4D78882342D697E4A6DE7CE594545B *)L_11);
		InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  L_13;
		L_13 = ((  InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  (*) (Func_2_t1AF155D27A4D78882342D697E4A6DE7CE594545B *, InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((Func_2_t1AF155D27A4D78882342D697E4A6DE7CE594545B *)L_11, (InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		RuntimeObject* L_14 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_14);
		bool L_15;
		L_15 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, (RuntimeObject*)L_14);
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<UnityEngine.InputSystem.Utilities.InternedString>::Dispose() */, (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.InternedString,UnityEngine.InputSystem.Utilities.InternedString>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectEnumerableIterator_2_Where_m2DBD0AC7DE2DC395FD77071E998A9F42C0A7E5D6_gshared (WhereSelectEnumerableIterator_2_t0EB79FD57DE6939EBF78FA1D92312CB62DD3F2C6 * __this, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B * L_1 = (WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 9));
		((  void (*) (WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B *, RuntimeObject*, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.InternedString,System.Object>::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2__ctor_mF80B7D228218FC1824B701584A41992AF2896644_gshared (WhereSelectEnumerableIterator_2_t02B3FDD3CA4282B70A77A6A52D6AADC70EF74E8B * __this, RuntimeObject* ___source0, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * ___predicate1, Func_2_tA2BD34BB8D97AD282016BD4CB5B8675E4AF85B45 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		RuntimeObject* L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_tA2BD34BB8D97AD282016BD4CB5B8675E4AF85B45 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.InternedString,System.Object>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 * WhereSelectEnumerableIterator_2_Clone_mD22315F299A9FC00905C1C170601CF09EF8E77C2_gshared (WhereSelectEnumerableIterator_2_t02B3FDD3CA4282B70A77A6A52D6AADC70EF74E8B * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_source_3();
		Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * L_1 = (Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)__this->get_predicate_4();
		Func_2_tA2BD34BB8D97AD282016BD4CB5B8675E4AF85B45 * L_2 = (Func_2_tA2BD34BB8D97AD282016BD4CB5B8675E4AF85B45 *)__this->get_selector_5();
		WhereSelectEnumerableIterator_2_t02B3FDD3CA4282B70A77A6A52D6AADC70EF74E8B * L_3 = (WhereSelectEnumerableIterator_2_t02B3FDD3CA4282B70A77A6A52D6AADC70EF74E8B *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectEnumerableIterator_2_t02B3FDD3CA4282B70A77A6A52D6AADC70EF74E8B *, RuntimeObject*, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *, Func_2_tA2BD34BB8D97AD282016BD4CB5B8675E4AF85B45 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (RuntimeObject*)L_0, (Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)L_1, (Func_2_tA2BD34BB8D97AD282016BD4CB5B8675E4AF85B45 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)L_3;
	}
}
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.InternedString,System.Object>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2_Dispose_m409DA5CCF563EBBA46DBCDA4F89CC00A7609B963_gshared (WhereSelectEnumerableIterator_2_t02B3FDD3CA4282B70A77A6A52D6AADC70EF74E8B * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_enumerator_6();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		RuntimeObject* L_1 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, (RuntimeObject*)L_1);
	}

IL_0013:
	{
		__this->set_enumerator_6((RuntimeObject*)NULL);
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		return;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.InternedString,System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectEnumerableIterator_2_MoveNext_m9ECF3AF97756B1A4D98D95DAF9A652824D5306E3_gshared (WhereSelectEnumerableIterator_2_t02B3FDD3CA4282B70A77A6A52D6AADC70EF74E8B * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		RuntimeObject* L_3 = (RuntimeObject*)__this->get_source_3();
		NullCheck((RuntimeObject*)L_3);
		RuntimeObject* L_4;
		L_4 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<UnityEngine.InputSystem.Utilities.InternedString>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 5), (RuntimeObject*)L_3);
		__this->set_enumerator_6(L_4);
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		RuntimeObject* L_5 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_5);
		InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  L_6;
		L_6 = InterfaceFuncInvoker0< InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<UnityEngine.InputSystem.Utilities.InternedString>::get_Current() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 6), (RuntimeObject*)L_5);
		V_1 = (InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED )L_6;
		Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * L_7 = (Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * L_8 = (Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)__this->get_predicate_4();
		InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  L_9 = V_1;
		NullCheck((Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *, InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)L_8, (InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_tA2BD34BB8D97AD282016BD4CB5B8675E4AF85B45 * L_11 = (Func_2_tA2BD34BB8D97AD282016BD4CB5B8675E4AF85B45 *)__this->get_selector_5();
		InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  L_12 = V_1;
		NullCheck((Func_2_tA2BD34BB8D97AD282016BD4CB5B8675E4AF85B45 *)L_11);
		RuntimeObject * L_13;
		L_13 = ((  RuntimeObject * (*) (Func_2_tA2BD34BB8D97AD282016BD4CB5B8675E4AF85B45 *, InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((Func_2_tA2BD34BB8D97AD282016BD4CB5B8675E4AF85B45 *)L_11, (InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		RuntimeObject* L_14 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_14);
		bool L_15;
		L_15 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, (RuntimeObject*)L_14);
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Object>::Dispose() */, (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.InternedString,System.Object>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectEnumerableIterator_2_Where_mC7F4A23C633975967983B86D9964AD45F4DEED6A_gshared (WhereSelectEnumerableIterator_2_t02B3FDD3CA4282B70A77A6A52D6AADC70EF74E8B * __this, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 * L_1 = (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 9));
		((  void (*) (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *, RuntimeObject*, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,UnityEngine.InputSystem.Utilities.InternedString>::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2__ctor_m9E4F0AFA2F6FB6949F2B65D82D768473554861D7_gshared (WhereSelectEnumerableIterator_2_t3B8EA5ED092FA87DEAECF94C466A54501B692B0B * __this, RuntimeObject* ___source0, Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 * ___predicate1, Func_2_tEA2235809AB0E1AB8425745179691D76B519B0C5 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		((  void (*) (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		RuntimeObject* L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_tEA2235809AB0E1AB8425745179691D76B519B0C5 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,UnityEngine.InputSystem.Utilities.InternedString>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 * WhereSelectEnumerableIterator_2_Clone_m0261D6201344D7750174D02E9BD7151879BBA11C_gshared (WhereSelectEnumerableIterator_2_t3B8EA5ED092FA87DEAECF94C466A54501B692B0B * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_source_3();
		Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 * L_1 = (Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 *)__this->get_predicate_4();
		Func_2_tEA2235809AB0E1AB8425745179691D76B519B0C5 * L_2 = (Func_2_tEA2235809AB0E1AB8425745179691D76B519B0C5 *)__this->get_selector_5();
		WhereSelectEnumerableIterator_2_t3B8EA5ED092FA87DEAECF94C466A54501B692B0B * L_3 = (WhereSelectEnumerableIterator_2_t3B8EA5ED092FA87DEAECF94C466A54501B692B0B *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectEnumerableIterator_2_t3B8EA5ED092FA87DEAECF94C466A54501B692B0B *, RuntimeObject*, Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 *, Func_2_tEA2235809AB0E1AB8425745179691D76B519B0C5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (RuntimeObject*)L_0, (Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 *)L_1, (Func_2_tEA2235809AB0E1AB8425745179691D76B519B0C5 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)L_3;
	}
}
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,UnityEngine.InputSystem.Utilities.InternedString>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2_Dispose_m636EAFD5143B6B31E8E967BE4C441F29FACDE9F9_gshared (WhereSelectEnumerableIterator_2_t3B8EA5ED092FA87DEAECF94C466A54501B692B0B * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_enumerator_6();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		RuntimeObject* L_1 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, (RuntimeObject*)L_1);
	}

IL_0013:
	{
		__this->set_enumerator_6((RuntimeObject*)NULL);
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		((  void (*) (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		return;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,UnityEngine.InputSystem.Utilities.InternedString>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectEnumerableIterator_2_MoveNext_m14F6C6C3DAE3AFC31E57F39E6B7258CD45E393D1_gshared (WhereSelectEnumerableIterator_2_t3B8EA5ED092FA87DEAECF94C466A54501B692B0B * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		RuntimeObject* L_3 = (RuntimeObject*)__this->get_source_3();
		NullCheck((RuntimeObject*)L_3);
		RuntimeObject* L_4;
		L_4 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<UnityEngine.InputSystem.Utilities.NameAndParameters>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 5), (RuntimeObject*)L_3);
		__this->set_enumerator_6(L_4);
		((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		RuntimeObject* L_5 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_5);
		NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA  L_6;
		L_6 = InterfaceFuncInvoker0< NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<UnityEngine.InputSystem.Utilities.NameAndParameters>::get_Current() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 6), (RuntimeObject*)L_5);
		V_1 = (NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA )L_6;
		Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 * L_7 = (Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 * L_8 = (Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 *)__this->get_predicate_4();
		NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA  L_9 = V_1;
		NullCheck((Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 *, NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 *)L_8, (NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_tEA2235809AB0E1AB8425745179691D76B519B0C5 * L_11 = (Func_2_tEA2235809AB0E1AB8425745179691D76B519B0C5 *)__this->get_selector_5();
		NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA  L_12 = V_1;
		NullCheck((Func_2_tEA2235809AB0E1AB8425745179691D76B519B0C5 *)L_11);
		InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  L_13;
		L_13 = ((  InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  (*) (Func_2_tEA2235809AB0E1AB8425745179691D76B519B0C5 *, NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((Func_2_tEA2235809AB0E1AB8425745179691D76B519B0C5 *)L_11, (NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		RuntimeObject* L_14 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_14);
		bool L_15;
		L_15 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, (RuntimeObject*)L_14);
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<UnityEngine.InputSystem.Utilities.InternedString>::Dispose() */, (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,UnityEngine.InputSystem.Utilities.InternedString>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectEnumerableIterator_2_Where_mE56AE8567DD2B7C3805040721D74F3F92E4B6B3E_gshared (WhereSelectEnumerableIterator_2_t3B8EA5ED092FA87DEAECF94C466A54501B692B0B * __this, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B * L_1 = (WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 9));
		((  void (*) (WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B *, RuntimeObject*, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,System.Object>::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2__ctor_mB57E48F47CF48F30AE7069CFE56D901695CC0A40_gshared (WhereSelectEnumerableIterator_2_t9C8232C608C379652355BB860B0A6074ABF36E09 * __this, RuntimeObject* ___source0, Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 * ___predicate1, Func_2_tF7304610FB007335AC870B51C8FE2092628DC0DB * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		RuntimeObject* L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_tF7304610FB007335AC870B51C8FE2092628DC0DB * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,System.Object>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 * WhereSelectEnumerableIterator_2_Clone_mB6B456C410C736171614B5D183BBE2AC04A8DFA8_gshared (WhereSelectEnumerableIterator_2_t9C8232C608C379652355BB860B0A6074ABF36E09 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_source_3();
		Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 * L_1 = (Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 *)__this->get_predicate_4();
		Func_2_tF7304610FB007335AC870B51C8FE2092628DC0DB * L_2 = (Func_2_tF7304610FB007335AC870B51C8FE2092628DC0DB *)__this->get_selector_5();
		WhereSelectEnumerableIterator_2_t9C8232C608C379652355BB860B0A6074ABF36E09 * L_3 = (WhereSelectEnumerableIterator_2_t9C8232C608C379652355BB860B0A6074ABF36E09 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectEnumerableIterator_2_t9C8232C608C379652355BB860B0A6074ABF36E09 *, RuntimeObject*, Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 *, Func_2_tF7304610FB007335AC870B51C8FE2092628DC0DB *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (RuntimeObject*)L_0, (Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 *)L_1, (Func_2_tF7304610FB007335AC870B51C8FE2092628DC0DB *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)L_3;
	}
}
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,System.Object>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2_Dispose_m736AE762C4A83A5BD995F757275214ADD2DDF3DA_gshared (WhereSelectEnumerableIterator_2_t9C8232C608C379652355BB860B0A6074ABF36E09 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_enumerator_6();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		RuntimeObject* L_1 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, (RuntimeObject*)L_1);
	}

IL_0013:
	{
		__this->set_enumerator_6((RuntimeObject*)NULL);
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		return;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectEnumerableIterator_2_MoveNext_mE572A528E5B2307AF20CA76CF260D192CE310BFD_gshared (WhereSelectEnumerableIterator_2_t9C8232C608C379652355BB860B0A6074ABF36E09 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		RuntimeObject* L_3 = (RuntimeObject*)__this->get_source_3();
		NullCheck((RuntimeObject*)L_3);
		RuntimeObject* L_4;
		L_4 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<UnityEngine.InputSystem.Utilities.NameAndParameters>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 5), (RuntimeObject*)L_3);
		__this->set_enumerator_6(L_4);
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		RuntimeObject* L_5 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_5);
		NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA  L_6;
		L_6 = InterfaceFuncInvoker0< NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<UnityEngine.InputSystem.Utilities.NameAndParameters>::get_Current() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 6), (RuntimeObject*)L_5);
		V_1 = (NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA )L_6;
		Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 * L_7 = (Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 * L_8 = (Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 *)__this->get_predicate_4();
		NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA  L_9 = V_1;
		NullCheck((Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 *, NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 *)L_8, (NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_tF7304610FB007335AC870B51C8FE2092628DC0DB * L_11 = (Func_2_tF7304610FB007335AC870B51C8FE2092628DC0DB *)__this->get_selector_5();
		NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA  L_12 = V_1;
		NullCheck((Func_2_tF7304610FB007335AC870B51C8FE2092628DC0DB *)L_11);
		RuntimeObject * L_13;
		L_13 = ((  RuntimeObject * (*) (Func_2_tF7304610FB007335AC870B51C8FE2092628DC0DB *, NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((Func_2_tF7304610FB007335AC870B51C8FE2092628DC0DB *)L_11, (NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		RuntimeObject* L_14 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_14);
		bool L_15;
		L_15 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, (RuntimeObject*)L_14);
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Object>::Dispose() */, (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,System.Object>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectEnumerableIterator_2_Where_m1A1F695AAEA536C0DAC30409DBFE6083B423FE60_gshared (WhereSelectEnumerableIterator_2_t9C8232C608C379652355BB860B0A6074ABF36E09 * __this, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 * L_1 = (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 9));
		((  void (*) (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *, RuntimeObject*, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,UnityEngine.InputSystem.Utilities.InternedString>::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2__ctor_m9D6B784F57BC5E3F595FEDC75360CE5C7362A853_gshared (WhereSelectEnumerableIterator_2_t382E0095C883AA01020D10E08F7F38BBA26A3EC5 * __this, RuntimeObject* ___source0, Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 * ___predicate1, Func_2_tA21678F1D9F9F4617D645755285D28EDE5F9AE44 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		((  void (*) (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		RuntimeObject* L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_tA21678F1D9F9F4617D645755285D28EDE5F9AE44 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,UnityEngine.InputSystem.Utilities.InternedString>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 * WhereSelectEnumerableIterator_2_Clone_mB0CA85DEB2481E8207B63464EA1061FD10C6DDA7_gshared (WhereSelectEnumerableIterator_2_t382E0095C883AA01020D10E08F7F38BBA26A3EC5 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_source_3();
		Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 * L_1 = (Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 *)__this->get_predicate_4();
		Func_2_tA21678F1D9F9F4617D645755285D28EDE5F9AE44 * L_2 = (Func_2_tA21678F1D9F9F4617D645755285D28EDE5F9AE44 *)__this->get_selector_5();
		WhereSelectEnumerableIterator_2_t382E0095C883AA01020D10E08F7F38BBA26A3EC5 * L_3 = (WhereSelectEnumerableIterator_2_t382E0095C883AA01020D10E08F7F38BBA26A3EC5 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectEnumerableIterator_2_t382E0095C883AA01020D10E08F7F38BBA26A3EC5 *, RuntimeObject*, Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 *, Func_2_tA21678F1D9F9F4617D645755285D28EDE5F9AE44 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (RuntimeObject*)L_0, (Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 *)L_1, (Func_2_tA21678F1D9F9F4617D645755285D28EDE5F9AE44 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)L_3;
	}
}
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,UnityEngine.InputSystem.Utilities.InternedString>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2_Dispose_m60E0BB66B459009D14D056C5503EA295B3F8A593_gshared (WhereSelectEnumerableIterator_2_t382E0095C883AA01020D10E08F7F38BBA26A3EC5 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_enumerator_6();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		RuntimeObject* L_1 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, (RuntimeObject*)L_1);
	}

IL_0013:
	{
		__this->set_enumerator_6((RuntimeObject*)NULL);
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		((  void (*) (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		return;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,UnityEngine.InputSystem.Utilities.InternedString>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectEnumerableIterator_2_MoveNext_m0AF7CC8342FBF822F9CCE844742A3DD50D1E7955_gshared (WhereSelectEnumerableIterator_2_t382E0095C883AA01020D10E08F7F38BBA26A3EC5 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		RuntimeObject* L_3 = (RuntimeObject*)__this->get_source_3();
		NullCheck((RuntimeObject*)L_3);
		RuntimeObject* L_4;
		L_4 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<UnityEngine.InputSystem.Utilities.NamedValue>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 5), (RuntimeObject*)L_3);
		__this->set_enumerator_6(L_4);
		((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		RuntimeObject* L_5 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_5);
		NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077  L_6;
		L_6 = InterfaceFuncInvoker0< NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<UnityEngine.InputSystem.Utilities.NamedValue>::get_Current() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 6), (RuntimeObject*)L_5);
		V_1 = (NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077 )L_6;
		Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 * L_7 = (Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 * L_8 = (Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 *)__this->get_predicate_4();
		NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077  L_9 = V_1;
		NullCheck((Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 *, NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 *)L_8, (NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077 )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_tA21678F1D9F9F4617D645755285D28EDE5F9AE44 * L_11 = (Func_2_tA21678F1D9F9F4617D645755285D28EDE5F9AE44 *)__this->get_selector_5();
		NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077  L_12 = V_1;
		NullCheck((Func_2_tA21678F1D9F9F4617D645755285D28EDE5F9AE44 *)L_11);
		InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  L_13;
		L_13 = ((  InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  (*) (Func_2_tA21678F1D9F9F4617D645755285D28EDE5F9AE44 *, NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((Func_2_tA21678F1D9F9F4617D645755285D28EDE5F9AE44 *)L_11, (NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077 )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		RuntimeObject* L_14 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_14);
		bool L_15;
		L_15 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, (RuntimeObject*)L_14);
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<UnityEngine.InputSystem.Utilities.InternedString>::Dispose() */, (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,UnityEngine.InputSystem.Utilities.InternedString>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectEnumerableIterator_2_Where_mDE780AD453ED9A744EBDC6437897EEFAC42D1C77_gshared (WhereSelectEnumerableIterator_2_t382E0095C883AA01020D10E08F7F38BBA26A3EC5 * __this, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B * L_1 = (WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 9));
		((  void (*) (WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B *, RuntimeObject*, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,System.Object>::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2__ctor_m9B9366B823B3FB97EE3B099806F08D2FA3C4D623_gshared (WhereSelectEnumerableIterator_2_tEBF9E61EFDABBBB02113A82648E32AC170921957 * __this, RuntimeObject* ___source0, Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 * ___predicate1, Func_2_t5B101C0B26ADAA3B1A77BC50D51CC42364E5409E * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		RuntimeObject* L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t5B101C0B26ADAA3B1A77BC50D51CC42364E5409E * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,System.Object>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 * WhereSelectEnumerableIterator_2_Clone_mCC0D8DDB4D096BEF20A3821C0BD72F589AA26B22_gshared (WhereSelectEnumerableIterator_2_tEBF9E61EFDABBBB02113A82648E32AC170921957 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_source_3();
		Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 * L_1 = (Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 *)__this->get_predicate_4();
		Func_2_t5B101C0B26ADAA3B1A77BC50D51CC42364E5409E * L_2 = (Func_2_t5B101C0B26ADAA3B1A77BC50D51CC42364E5409E *)__this->get_selector_5();
		WhereSelectEnumerableIterator_2_tEBF9E61EFDABBBB02113A82648E32AC170921957 * L_3 = (WhereSelectEnumerableIterator_2_tEBF9E61EFDABBBB02113A82648E32AC170921957 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectEnumerableIterator_2_tEBF9E61EFDABBBB02113A82648E32AC170921957 *, RuntimeObject*, Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 *, Func_2_t5B101C0B26ADAA3B1A77BC50D51CC42364E5409E *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (RuntimeObject*)L_0, (Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 *)L_1, (Func_2_t5B101C0B26ADAA3B1A77BC50D51CC42364E5409E *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)L_3;
	}
}
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,System.Object>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2_Dispose_m3227B92605007A7258A7EF9DB10356FEE2BECBD0_gshared (WhereSelectEnumerableIterator_2_tEBF9E61EFDABBBB02113A82648E32AC170921957 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_enumerator_6();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		RuntimeObject* L_1 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, (RuntimeObject*)L_1);
	}

IL_0013:
	{
		__this->set_enumerator_6((RuntimeObject*)NULL);
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		return;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectEnumerableIterator_2_MoveNext_m1E2FACAEEE841F6C0552D3B6716DD2F988B2D757_gshared (WhereSelectEnumerableIterator_2_tEBF9E61EFDABBBB02113A82648E32AC170921957 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		RuntimeObject* L_3 = (RuntimeObject*)__this->get_source_3();
		NullCheck((RuntimeObject*)L_3);
		RuntimeObject* L_4;
		L_4 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<UnityEngine.InputSystem.Utilities.NamedValue>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 5), (RuntimeObject*)L_3);
		__this->set_enumerator_6(L_4);
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		RuntimeObject* L_5 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_5);
		NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077  L_6;
		L_6 = InterfaceFuncInvoker0< NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<UnityEngine.InputSystem.Utilities.NamedValue>::get_Current() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 6), (RuntimeObject*)L_5);
		V_1 = (NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077 )L_6;
		Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 * L_7 = (Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 * L_8 = (Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 *)__this->get_predicate_4();
		NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077  L_9 = V_1;
		NullCheck((Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 *, NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 *)L_8, (NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077 )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t5B101C0B26ADAA3B1A77BC50D51CC42364E5409E * L_11 = (Func_2_t5B101C0B26ADAA3B1A77BC50D51CC42364E5409E *)__this->get_selector_5();
		NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077  L_12 = V_1;
		NullCheck((Func_2_t5B101C0B26ADAA3B1A77BC50D51CC42364E5409E *)L_11);
		RuntimeObject * L_13;
		L_13 = ((  RuntimeObject * (*) (Func_2_t5B101C0B26ADAA3B1A77BC50D51CC42364E5409E *, NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((Func_2_t5B101C0B26ADAA3B1A77BC50D51CC42364E5409E *)L_11, (NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077 )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		RuntimeObject* L_14 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_14);
		bool L_15;
		L_15 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, (RuntimeObject*)L_14);
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Object>::Dispose() */, (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,System.Object>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectEnumerableIterator_2_Where_m1A59DCED9B25CB0DC072FFB8CE2F7B2E7A33C08F_gshared (WhereSelectEnumerableIterator_2_tEBF9E61EFDABBBB02113A82648E32AC170921957 * __this, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 * L_1 = (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 9));
		((  void (*) (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *, RuntimeObject*, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Object,UnityEngine.InputSystem.Utilities.InternedString>::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2__ctor_mDC1C0BAAE3DC9997EEF3A78A0C945072A2613A37_gshared (WhereSelectEnumerableIterator_2_tE487654732A8A2F1E3A2D57412C08036EE861F38 * __this, RuntimeObject* ___source0, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate1, Func_2_t964229B6ADD2B008268547ED2BC3C090AF218065 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		((  void (*) (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		RuntimeObject* L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t964229B6ADD2B008268547ED2BC3C090AF218065 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Object,UnityEngine.InputSystem.Utilities.InternedString>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 * WhereSelectEnumerableIterator_2_Clone_m6BB70F38D6AAEA7CC3D56D23AEAE5680D24EF676_gshared (WhereSelectEnumerableIterator_2_tE487654732A8A2F1E3A2D57412C08036EE861F38 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_source_3();
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_1 = (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)__this->get_predicate_4();
		Func_2_t964229B6ADD2B008268547ED2BC3C090AF218065 * L_2 = (Func_2_t964229B6ADD2B008268547ED2BC3C090AF218065 *)__this->get_selector_5();
		WhereSelectEnumerableIterator_2_tE487654732A8A2F1E3A2D57412C08036EE861F38 * L_3 = (WhereSelectEnumerableIterator_2_tE487654732A8A2F1E3A2D57412C08036EE861F38 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectEnumerableIterator_2_tE487654732A8A2F1E3A2D57412C08036EE861F38 *, RuntimeObject*, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, Func_2_t964229B6ADD2B008268547ED2BC3C090AF218065 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (RuntimeObject*)L_0, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_1, (Func_2_t964229B6ADD2B008268547ED2BC3C090AF218065 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)L_3;
	}
}
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Object,UnityEngine.InputSystem.Utilities.InternedString>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2_Dispose_m8C429758299D47FE1C7540A09508694C4937A0CF_gshared (WhereSelectEnumerableIterator_2_tE487654732A8A2F1E3A2D57412C08036EE861F38 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_enumerator_6();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		RuntimeObject* L_1 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, (RuntimeObject*)L_1);
	}

IL_0013:
	{
		__this->set_enumerator_6((RuntimeObject*)NULL);
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		((  void (*) (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		return;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Object,UnityEngine.InputSystem.Utilities.InternedString>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectEnumerableIterator_2_MoveNext_m8AB7D24A2E80EACC10188BA191189BDF6E632D40_gshared (WhereSelectEnumerableIterator_2_tE487654732A8A2F1E3A2D57412C08036EE861F38 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	RuntimeObject * V_1 = NULL;
	{
		int32_t L_0 = (int32_t)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		RuntimeObject* L_3 = (RuntimeObject*)__this->get_source_3();
		NullCheck((RuntimeObject*)L_3);
		RuntimeObject* L_4;
		L_4 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 5), (RuntimeObject*)L_3);
		__this->set_enumerator_6(L_4);
		((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		RuntimeObject* L_5 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_5);
		RuntimeObject * L_6;
		L_6 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Object>::get_Current() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 6), (RuntimeObject*)L_5);
		V_1 = (RuntimeObject *)L_6;
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_7 = (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_8 = (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)__this->get_predicate_4();
		RuntimeObject * L_9 = V_1;
		NullCheck((Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_8, (RuntimeObject *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t964229B6ADD2B008268547ED2BC3C090AF218065 * L_11 = (Func_2_t964229B6ADD2B008268547ED2BC3C090AF218065 *)__this->get_selector_5();
		RuntimeObject * L_12 = V_1;
		NullCheck((Func_2_t964229B6ADD2B008268547ED2BC3C090AF218065 *)L_11);
		InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  L_13;
		L_13 = ((  InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  (*) (Func_2_t964229B6ADD2B008268547ED2BC3C090AF218065 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((Func_2_t964229B6ADD2B008268547ED2BC3C090AF218065 *)L_11, (RuntimeObject *)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		RuntimeObject* L_14 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_14);
		bool L_15;
		L_15 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, (RuntimeObject*)L_14);
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<UnityEngine.InputSystem.Utilities.InternedString>::Dispose() */, (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Object,UnityEngine.InputSystem.Utilities.InternedString>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectEnumerableIterator_2_Where_m88220E7155D4802F0EE34B5FA47A1A0D1849D5C8_gshared (WhereSelectEnumerableIterator_2_tE487654732A8A2F1E3A2D57412C08036EE861F38 * __this, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B * L_1 = (WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 9));
		((  void (*) (WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B *, RuntimeObject*, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Object,System.Object>::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2__ctor_m6DFD3E949A8FA5121F520D501B78C17E84EBDFAC_gshared (WhereSelectEnumerableIterator_2_tDAA8BFBEA68F81670F3F51C6200EBD26D7A8FBAB * __this, RuntimeObject* ___source0, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate1, Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		RuntimeObject* L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Object,System.Object>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 * WhereSelectEnumerableIterator_2_Clone_mD5F17A02281E6D1529D117CFF2E0F8C347D1B13F_gshared (WhereSelectEnumerableIterator_2_tDAA8BFBEA68F81670F3F51C6200EBD26D7A8FBAB * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_source_3();
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_1 = (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)__this->get_predicate_4();
		Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 * L_2 = (Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 *)__this->get_selector_5();
		WhereSelectEnumerableIterator_2_tDAA8BFBEA68F81670F3F51C6200EBD26D7A8FBAB * L_3 = (WhereSelectEnumerableIterator_2_tDAA8BFBEA68F81670F3F51C6200EBD26D7A8FBAB *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectEnumerableIterator_2_tDAA8BFBEA68F81670F3F51C6200EBD26D7A8FBAB *, RuntimeObject*, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (RuntimeObject*)L_0, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_1, (Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)L_3;
	}
}
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Object,System.Object>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2_Dispose_mAA70577DEF67CEC98FE677984AE2175B7D4E4D00_gshared (WhereSelectEnumerableIterator_2_tDAA8BFBEA68F81670F3F51C6200EBD26D7A8FBAB * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_enumerator_6();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		RuntimeObject* L_1 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, (RuntimeObject*)L_1);
	}

IL_0013:
	{
		__this->set_enumerator_6((RuntimeObject*)NULL);
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		return;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Object,System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectEnumerableIterator_2_MoveNext_m95AEE737A22EFFFE6557F448BF5AFCC6241D0BD7_gshared (WhereSelectEnumerableIterator_2_tDAA8BFBEA68F81670F3F51C6200EBD26D7A8FBAB * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	RuntimeObject * V_1 = NULL;
	{
		int32_t L_0 = (int32_t)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		RuntimeObject* L_3 = (RuntimeObject*)__this->get_source_3();
		NullCheck((RuntimeObject*)L_3);
		RuntimeObject* L_4;
		L_4 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 5), (RuntimeObject*)L_3);
		__this->set_enumerator_6(L_4);
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		RuntimeObject* L_5 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_5);
		RuntimeObject * L_6;
		L_6 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Object>::get_Current() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 6), (RuntimeObject*)L_5);
		V_1 = (RuntimeObject *)L_6;
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_7 = (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_8 = (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)__this->get_predicate_4();
		RuntimeObject * L_9 = V_1;
		NullCheck((Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_8, (RuntimeObject *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 * L_11 = (Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 *)__this->get_selector_5();
		RuntimeObject * L_12 = V_1;
		NullCheck((Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 *)L_11);
		RuntimeObject * L_13;
		L_13 = ((  RuntimeObject * (*) (Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 *)L_11, (RuntimeObject *)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		RuntimeObject* L_14 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_14);
		bool L_15;
		L_15 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, (RuntimeObject*)L_14);
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Object>::Dispose() */, (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Object,System.Object>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectEnumerableIterator_2_Where_m72A38A8170E8B837F5C090642BE08E3845A8EB37_gshared (WhereSelectEnumerableIterator_2_tDAA8BFBEA68F81670F3F51C6200EBD26D7A8FBAB * __this, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 * L_1 = (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 9));
		((  void (*) (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *, RuntimeObject*, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.Pose,UnityEngine.Quaternion>::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2__ctor_m8C938FC8F8791C34B47D2036A30B94E4923B0D15_gshared (WhereSelectEnumerableIterator_2_t3529F4FEC1C961CCD3F23682094813DA27D0ADA3 * __this, RuntimeObject* ___source0, Func_2_t288BC2D8C12E52B8F2E7F675CFB740C44E7E813F * ___predicate1, Func_2_t6170D516A1F6F9B6BE1BDC5379D0A5CC320AF0F8 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t4BD4EE08E1E7E81949C3754EC2969154580F4C65 *)__this);
		((  void (*) (Iterator_1_t4BD4EE08E1E7E81949C3754EC2969154580F4C65 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t4BD4EE08E1E7E81949C3754EC2969154580F4C65 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		RuntimeObject* L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t288BC2D8C12E52B8F2E7F675CFB740C44E7E813F * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t6170D516A1F6F9B6BE1BDC5379D0A5CC320AF0F8 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.Pose,UnityEngine.Quaternion>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t4BD4EE08E1E7E81949C3754EC2969154580F4C65 * WhereSelectEnumerableIterator_2_Clone_mE337AF45DF10A0D8CDB309A8033F48490177A9AE_gshared (WhereSelectEnumerableIterator_2_t3529F4FEC1C961CCD3F23682094813DA27D0ADA3 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_source_3();
		Func_2_t288BC2D8C12E52B8F2E7F675CFB740C44E7E813F * L_1 = (Func_2_t288BC2D8C12E52B8F2E7F675CFB740C44E7E813F *)__this->get_predicate_4();
		Func_2_t6170D516A1F6F9B6BE1BDC5379D0A5CC320AF0F8 * L_2 = (Func_2_t6170D516A1F6F9B6BE1BDC5379D0A5CC320AF0F8 *)__this->get_selector_5();
		WhereSelectEnumerableIterator_2_t3529F4FEC1C961CCD3F23682094813DA27D0ADA3 * L_3 = (WhereSelectEnumerableIterator_2_t3529F4FEC1C961CCD3F23682094813DA27D0ADA3 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectEnumerableIterator_2_t3529F4FEC1C961CCD3F23682094813DA27D0ADA3 *, RuntimeObject*, Func_2_t288BC2D8C12E52B8F2E7F675CFB740C44E7E813F *, Func_2_t6170D516A1F6F9B6BE1BDC5379D0A5CC320AF0F8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (RuntimeObject*)L_0, (Func_2_t288BC2D8C12E52B8F2E7F675CFB740C44E7E813F *)L_1, (Func_2_t6170D516A1F6F9B6BE1BDC5379D0A5CC320AF0F8 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t4BD4EE08E1E7E81949C3754EC2969154580F4C65 *)L_3;
	}
}
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.Pose,UnityEngine.Quaternion>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2_Dispose_m8ADA1D81B6928C8E46693489A7A03240F8950A96_gshared (WhereSelectEnumerableIterator_2_t3529F4FEC1C961CCD3F23682094813DA27D0ADA3 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_enumerator_6();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		RuntimeObject* L_1 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, (RuntimeObject*)L_1);
	}

IL_0013:
	{
		__this->set_enumerator_6((RuntimeObject*)NULL);
		NullCheck((Iterator_1_t4BD4EE08E1E7E81949C3754EC2969154580F4C65 *)__this);
		((  void (*) (Iterator_1_t4BD4EE08E1E7E81949C3754EC2969154580F4C65 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Iterator_1_t4BD4EE08E1E7E81949C3754EC2969154580F4C65 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		return;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.Pose,UnityEngine.Quaternion>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectEnumerableIterator_2_MoveNext_m3D7FB806ECD2F1B8B6841977ACC0879B81B0206D_gshared (WhereSelectEnumerableIterator_2_t3529F4FEC1C961CCD3F23682094813DA27D0ADA3 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Pose_t9F30358E65733E60A1DC8682FDB7104F40C9434A  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t4BD4EE08E1E7E81949C3754EC2969154580F4C65 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		RuntimeObject* L_3 = (RuntimeObject*)__this->get_source_3();
		NullCheck((RuntimeObject*)L_3);
		RuntimeObject* L_4;
		L_4 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<UnityEngine.Pose>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 5), (RuntimeObject*)L_3);
		__this->set_enumerator_6(L_4);
		((Iterator_1_t4BD4EE08E1E7E81949C3754EC2969154580F4C65 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		RuntimeObject* L_5 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_5);
		Pose_t9F30358E65733E60A1DC8682FDB7104F40C9434A  L_6;
		L_6 = InterfaceFuncInvoker0< Pose_t9F30358E65733E60A1DC8682FDB7104F40C9434A  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<UnityEngine.Pose>::get_Current() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 6), (RuntimeObject*)L_5);
		V_1 = (Pose_t9F30358E65733E60A1DC8682FDB7104F40C9434A )L_6;
		Func_2_t288BC2D8C12E52B8F2E7F675CFB740C44E7E813F * L_7 = (Func_2_t288BC2D8C12E52B8F2E7F675CFB740C44E7E813F *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t288BC2D8C12E52B8F2E7F675CFB740C44E7E813F * L_8 = (Func_2_t288BC2D8C12E52B8F2E7F675CFB740C44E7E813F *)__this->get_predicate_4();
		Pose_t9F30358E65733E60A1DC8682FDB7104F40C9434A  L_9 = V_1;
		NullCheck((Func_2_t288BC2D8C12E52B8F2E7F675CFB740C44E7E813F *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t288BC2D8C12E52B8F2E7F675CFB740C44E7E813F *, Pose_t9F30358E65733E60A1DC8682FDB7104F40C9434A , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t288BC2D8C12E52B8F2E7F675CFB740C44E7E813F *)L_8, (Pose_t9F30358E65733E60A1DC8682FDB7104F40C9434A )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t6170D516A1F6F9B6BE1BDC5379D0A5CC320AF0F8 * L_11 = (Func_2_t6170D516A1F6F9B6BE1BDC5379D0A5CC320AF0F8 *)__this->get_selector_5();
		Pose_t9F30358E65733E60A1DC8682FDB7104F40C9434A  L_12 = V_1;
		NullCheck((Func_2_t6170D516A1F6F9B6BE1BDC5379D0A5CC320AF0F8 *)L_11);
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_13;
		L_13 = ((  Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  (*) (Func_2_t6170D516A1F6F9B6BE1BDC5379D0A5CC320AF0F8 *, Pose_t9F30358E65733E60A1DC8682FDB7104F40C9434A , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((Func_2_t6170D516A1F6F9B6BE1BDC5379D0A5CC320AF0F8 *)L_11, (Pose_t9F30358E65733E60A1DC8682FDB7104F40C9434A )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		((Iterator_1_t4BD4EE08E1E7E81949C3754EC2969154580F4C65 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		RuntimeObject* L_14 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_14);
		bool L_15;
		L_15 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, (RuntimeObject*)L_14);
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t4BD4EE08E1E7E81949C3754EC2969154580F4C65 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<UnityEngine.Quaternion>::Dispose() */, (Iterator_1_t4BD4EE08E1E7E81949C3754EC2969154580F4C65 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.Pose,UnityEngine.Quaternion>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectEnumerableIterator_2_Where_m8473DB263151A7B8C89F0006DD06D20F013B5283_gshared (WhereSelectEnumerableIterator_2_t3529F4FEC1C961CCD3F23682094813DA27D0ADA3 * __this, Func_2_t34B33A6CBEAEB1EF3A60338636A0DC782C33A7BE * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t34B33A6CBEAEB1EF3A60338636A0DC782C33A7BE * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t6F3A191533D782BF1D9D4D3C5E1B535EC187EA9F * L_1 = (WhereEnumerableIterator_1_t6F3A191533D782BF1D9D4D3C5E1B535EC187EA9F *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 9));
		((  void (*) (WhereEnumerableIterator_1_t6F3A191533D782BF1D9D4D3C5E1B535EC187EA9F *, RuntimeObject*, Func_2_t34B33A6CBEAEB1EF3A60338636A0DC782C33A7BE *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t34B33A6CBEAEB1EF3A60338636A0DC782C33A7BE *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Single,UnityEngine.InputSystem.Utilities.InternedString>::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2__ctor_mDEAEBEE8F52AB1A6BB08934ABF1CC156D2F154FE_gshared (WhereSelectEnumerableIterator_2_tB68F25B00798F106AD2C511CF10D0289252107C8 * __this, RuntimeObject* ___source0, Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * ___predicate1, Func_2_t4E06D96B2978B60DA267963F96583CE3BF82EA84 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		((  void (*) (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		RuntimeObject* L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t4E06D96B2978B60DA267963F96583CE3BF82EA84 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Single,UnityEngine.InputSystem.Utilities.InternedString>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 * WhereSelectEnumerableIterator_2_Clone_mA94EC5BEAB160CE036822EE9FAF419963611C421_gshared (WhereSelectEnumerableIterator_2_tB68F25B00798F106AD2C511CF10D0289252107C8 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_source_3();
		Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * L_1 = (Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *)__this->get_predicate_4();
		Func_2_t4E06D96B2978B60DA267963F96583CE3BF82EA84 * L_2 = (Func_2_t4E06D96B2978B60DA267963F96583CE3BF82EA84 *)__this->get_selector_5();
		WhereSelectEnumerableIterator_2_tB68F25B00798F106AD2C511CF10D0289252107C8 * L_3 = (WhereSelectEnumerableIterator_2_tB68F25B00798F106AD2C511CF10D0289252107C8 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectEnumerableIterator_2_tB68F25B00798F106AD2C511CF10D0289252107C8 *, RuntimeObject*, Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *, Func_2_t4E06D96B2978B60DA267963F96583CE3BF82EA84 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (RuntimeObject*)L_0, (Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *)L_1, (Func_2_t4E06D96B2978B60DA267963F96583CE3BF82EA84 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)L_3;
	}
}
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Single,UnityEngine.InputSystem.Utilities.InternedString>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2_Dispose_m4B4F376C274A7050E21CEED971F8EA6BC971622B_gshared (WhereSelectEnumerableIterator_2_tB68F25B00798F106AD2C511CF10D0289252107C8 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_enumerator_6();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		RuntimeObject* L_1 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, (RuntimeObject*)L_1);
	}

IL_0013:
	{
		__this->set_enumerator_6((RuntimeObject*)NULL);
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		((  void (*) (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		return;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Single,UnityEngine.InputSystem.Utilities.InternedString>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectEnumerableIterator_2_MoveNext_m1CF232A147B40EFBADD3DF870E4B356DF541F5C7_gshared (WhereSelectEnumerableIterator_2_tB68F25B00798F106AD2C511CF10D0289252107C8 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	float V_1 = 0.0f;
	{
		int32_t L_0 = (int32_t)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		RuntimeObject* L_3 = (RuntimeObject*)__this->get_source_3();
		NullCheck((RuntimeObject*)L_3);
		RuntimeObject* L_4;
		L_4 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Single>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 5), (RuntimeObject*)L_3);
		__this->set_enumerator_6(L_4);
		((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		RuntimeObject* L_5 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_5);
		float L_6;
		L_6 = InterfaceFuncInvoker0< float >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Single>::get_Current() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 6), (RuntimeObject*)L_5);
		V_1 = (float)L_6;
		Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * L_7 = (Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * L_8 = (Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *)__this->get_predicate_4();
		float L_9 = V_1;
		NullCheck((Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *, float, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *)L_8, (float)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t4E06D96B2978B60DA267963F96583CE3BF82EA84 * L_11 = (Func_2_t4E06D96B2978B60DA267963F96583CE3BF82EA84 *)__this->get_selector_5();
		float L_12 = V_1;
		NullCheck((Func_2_t4E06D96B2978B60DA267963F96583CE3BF82EA84 *)L_11);
		InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  L_13;
		L_13 = ((  InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  (*) (Func_2_t4E06D96B2978B60DA267963F96583CE3BF82EA84 *, float, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((Func_2_t4E06D96B2978B60DA267963F96583CE3BF82EA84 *)L_11, (float)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		RuntimeObject* L_14 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_14);
		bool L_15;
		L_15 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, (RuntimeObject*)L_14);
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<UnityEngine.InputSystem.Utilities.InternedString>::Dispose() */, (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Single,UnityEngine.InputSystem.Utilities.InternedString>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectEnumerableIterator_2_Where_m5C7212627A37F66EBAD0E85300447239BCDDB2EE_gshared (WhereSelectEnumerableIterator_2_tB68F25B00798F106AD2C511CF10D0289252107C8 * __this, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B * L_1 = (WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 9));
		((  void (*) (WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B *, RuntimeObject*, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Single,System.Object>::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2__ctor_m965328D2ECA16C2BE75E3E223BDDFA35731CC5BB_gshared (WhereSelectEnumerableIterator_2_t8330901F0811E7C54CAD7107B2350ACFCBF3859A * __this, RuntimeObject* ___source0, Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * ___predicate1, Func_2_tE11BDF71BCB314E58F1A9CFC9DAE08A5F717E2A0 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		RuntimeObject* L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_tE11BDF71BCB314E58F1A9CFC9DAE08A5F717E2A0 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Single,System.Object>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 * WhereSelectEnumerableIterator_2_Clone_m34512788F517D64AF88551951B2D75A4CA0DD066_gshared (WhereSelectEnumerableIterator_2_t8330901F0811E7C54CAD7107B2350ACFCBF3859A * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_source_3();
		Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * L_1 = (Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *)__this->get_predicate_4();
		Func_2_tE11BDF71BCB314E58F1A9CFC9DAE08A5F717E2A0 * L_2 = (Func_2_tE11BDF71BCB314E58F1A9CFC9DAE08A5F717E2A0 *)__this->get_selector_5();
		WhereSelectEnumerableIterator_2_t8330901F0811E7C54CAD7107B2350ACFCBF3859A * L_3 = (WhereSelectEnumerableIterator_2_t8330901F0811E7C54CAD7107B2350ACFCBF3859A *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectEnumerableIterator_2_t8330901F0811E7C54CAD7107B2350ACFCBF3859A *, RuntimeObject*, Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *, Func_2_tE11BDF71BCB314E58F1A9CFC9DAE08A5F717E2A0 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (RuntimeObject*)L_0, (Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *)L_1, (Func_2_tE11BDF71BCB314E58F1A9CFC9DAE08A5F717E2A0 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)L_3;
	}
}
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Single,System.Object>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2_Dispose_m03C13B47EFC2336E91F60AAEC480568F289F373A_gshared (WhereSelectEnumerableIterator_2_t8330901F0811E7C54CAD7107B2350ACFCBF3859A * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_enumerator_6();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		RuntimeObject* L_1 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, (RuntimeObject*)L_1);
	}

IL_0013:
	{
		__this->set_enumerator_6((RuntimeObject*)NULL);
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		return;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Single,System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectEnumerableIterator_2_MoveNext_mAD7BCF14B4CD4C276B3D022E0EE2E390E2672FB6_gshared (WhereSelectEnumerableIterator_2_t8330901F0811E7C54CAD7107B2350ACFCBF3859A * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	float V_1 = 0.0f;
	{
		int32_t L_0 = (int32_t)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		RuntimeObject* L_3 = (RuntimeObject*)__this->get_source_3();
		NullCheck((RuntimeObject*)L_3);
		RuntimeObject* L_4;
		L_4 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Single>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 5), (RuntimeObject*)L_3);
		__this->set_enumerator_6(L_4);
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		RuntimeObject* L_5 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_5);
		float L_6;
		L_6 = InterfaceFuncInvoker0< float >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Single>::get_Current() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 6), (RuntimeObject*)L_5);
		V_1 = (float)L_6;
		Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * L_7 = (Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * L_8 = (Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *)__this->get_predicate_4();
		float L_9 = V_1;
		NullCheck((Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *, float, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *)L_8, (float)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_tE11BDF71BCB314E58F1A9CFC9DAE08A5F717E2A0 * L_11 = (Func_2_tE11BDF71BCB314E58F1A9CFC9DAE08A5F717E2A0 *)__this->get_selector_5();
		float L_12 = V_1;
		NullCheck((Func_2_tE11BDF71BCB314E58F1A9CFC9DAE08A5F717E2A0 *)L_11);
		RuntimeObject * L_13;
		L_13 = ((  RuntimeObject * (*) (Func_2_tE11BDF71BCB314E58F1A9CFC9DAE08A5F717E2A0 *, float, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((Func_2_tE11BDF71BCB314E58F1A9CFC9DAE08A5F717E2A0 *)L_11, (float)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		RuntimeObject* L_14 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_14);
		bool L_15;
		L_15 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, (RuntimeObject*)L_14);
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Object>::Dispose() */, (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Single,System.Object>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectEnumerableIterator_2_Where_m78EC6F7DCD8C5E50C5FF4CCD1F89CF89491CB627_gshared (WhereSelectEnumerableIterator_2_t8330901F0811E7C54CAD7107B2350ACFCBF3859A * __this, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 * L_1 = (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 9));
		((  void (*) (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *, RuntimeObject*, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.Substring,UnityEngine.InputSystem.Utilities.InternedString>::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2__ctor_mBFA8D3D3BA93FCA0903391FB55A24860B6211CD4_gshared (WhereSelectEnumerableIterator_2_tFDFC244728E14BAC93BE9869A64080C38A0B7F1D * __this, RuntimeObject* ___source0, Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 * ___predicate1, Func_2_t14F6F4D7FBF558A20782EDC48D80473E4800048E * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		((  void (*) (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		RuntimeObject* L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t14F6F4D7FBF558A20782EDC48D80473E4800048E * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.Substring,UnityEngine.InputSystem.Utilities.InternedString>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 * WhereSelectEnumerableIterator_2_Clone_m18294E8DDCC492E800E13251CE500104102F5571_gshared (WhereSelectEnumerableIterator_2_tFDFC244728E14BAC93BE9869A64080C38A0B7F1D * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_source_3();
		Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 * L_1 = (Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 *)__this->get_predicate_4();
		Func_2_t14F6F4D7FBF558A20782EDC48D80473E4800048E * L_2 = (Func_2_t14F6F4D7FBF558A20782EDC48D80473E4800048E *)__this->get_selector_5();
		WhereSelectEnumerableIterator_2_tFDFC244728E14BAC93BE9869A64080C38A0B7F1D * L_3 = (WhereSelectEnumerableIterator_2_tFDFC244728E14BAC93BE9869A64080C38A0B7F1D *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectEnumerableIterator_2_tFDFC244728E14BAC93BE9869A64080C38A0B7F1D *, RuntimeObject*, Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 *, Func_2_t14F6F4D7FBF558A20782EDC48D80473E4800048E *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (RuntimeObject*)L_0, (Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 *)L_1, (Func_2_t14F6F4D7FBF558A20782EDC48D80473E4800048E *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)L_3;
	}
}
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.Substring,UnityEngine.InputSystem.Utilities.InternedString>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2_Dispose_m5E0D3576F68517F538B8877D37526BC5DC3938D3_gshared (WhereSelectEnumerableIterator_2_tFDFC244728E14BAC93BE9869A64080C38A0B7F1D * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_enumerator_6();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		RuntimeObject* L_1 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, (RuntimeObject*)L_1);
	}

IL_0013:
	{
		__this->set_enumerator_6((RuntimeObject*)NULL);
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		((  void (*) (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		return;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.Substring,UnityEngine.InputSystem.Utilities.InternedString>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectEnumerableIterator_2_MoveNext_mE1401B0580A1237F67DEBBDB8E524C2CBA277BE7_gshared (WhereSelectEnumerableIterator_2_tFDFC244728E14BAC93BE9869A64080C38A0B7F1D * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		RuntimeObject* L_3 = (RuntimeObject*)__this->get_source_3();
		NullCheck((RuntimeObject*)L_3);
		RuntimeObject* L_4;
		L_4 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<UnityEngine.InputSystem.Utilities.Substring>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 5), (RuntimeObject*)L_3);
		__this->set_enumerator_6(L_4);
		((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		RuntimeObject* L_5 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_5);
		Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F  L_6;
		L_6 = InterfaceFuncInvoker0< Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<UnityEngine.InputSystem.Utilities.Substring>::get_Current() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 6), (RuntimeObject*)L_5);
		V_1 = (Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F )L_6;
		Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 * L_7 = (Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 * L_8 = (Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 *)__this->get_predicate_4();
		Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F  L_9 = V_1;
		NullCheck((Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 *, Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 *)L_8, (Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t14F6F4D7FBF558A20782EDC48D80473E4800048E * L_11 = (Func_2_t14F6F4D7FBF558A20782EDC48D80473E4800048E *)__this->get_selector_5();
		Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F  L_12 = V_1;
		NullCheck((Func_2_t14F6F4D7FBF558A20782EDC48D80473E4800048E *)L_11);
		InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  L_13;
		L_13 = ((  InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  (*) (Func_2_t14F6F4D7FBF558A20782EDC48D80473E4800048E *, Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((Func_2_t14F6F4D7FBF558A20782EDC48D80473E4800048E *)L_11, (Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		RuntimeObject* L_14 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_14);
		bool L_15;
		L_15 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, (RuntimeObject*)L_14);
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<UnityEngine.InputSystem.Utilities.InternedString>::Dispose() */, (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.Substring,UnityEngine.InputSystem.Utilities.InternedString>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectEnumerableIterator_2_Where_mC892FB08719A2CCD93945767DCC4C39C30084447_gshared (WhereSelectEnumerableIterator_2_tFDFC244728E14BAC93BE9869A64080C38A0B7F1D * __this, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B * L_1 = (WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 9));
		((  void (*) (WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B *, RuntimeObject*, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.Substring,System.Object>::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2__ctor_m83E62C9CA6B313F274AF1B6BA57E010DE69AF8B2_gshared (WhereSelectEnumerableIterator_2_t5E3BB9AFBEC27ED62FC129516C2D3E9FC81641B6 * __this, RuntimeObject* ___source0, Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 * ___predicate1, Func_2_t4EC2F095EDDC8079537CA0FEDC463635E599C9E4 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		RuntimeObject* L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t4EC2F095EDDC8079537CA0FEDC463635E599C9E4 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.Substring,System.Object>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 * WhereSelectEnumerableIterator_2_Clone_mE7DA8409BBD86CD802389261B4666600D0C3E8C3_gshared (WhereSelectEnumerableIterator_2_t5E3BB9AFBEC27ED62FC129516C2D3E9FC81641B6 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_source_3();
		Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 * L_1 = (Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 *)__this->get_predicate_4();
		Func_2_t4EC2F095EDDC8079537CA0FEDC463635E599C9E4 * L_2 = (Func_2_t4EC2F095EDDC8079537CA0FEDC463635E599C9E4 *)__this->get_selector_5();
		WhereSelectEnumerableIterator_2_t5E3BB9AFBEC27ED62FC129516C2D3E9FC81641B6 * L_3 = (WhereSelectEnumerableIterator_2_t5E3BB9AFBEC27ED62FC129516C2D3E9FC81641B6 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectEnumerableIterator_2_t5E3BB9AFBEC27ED62FC129516C2D3E9FC81641B6 *, RuntimeObject*, Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 *, Func_2_t4EC2F095EDDC8079537CA0FEDC463635E599C9E4 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (RuntimeObject*)L_0, (Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 *)L_1, (Func_2_t4EC2F095EDDC8079537CA0FEDC463635E599C9E4 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)L_3;
	}
}
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.Substring,System.Object>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2_Dispose_mEB51A4C958B4C9198E3CEA72900154448940EECA_gshared (WhereSelectEnumerableIterator_2_t5E3BB9AFBEC27ED62FC129516C2D3E9FC81641B6 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_enumerator_6();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		RuntimeObject* L_1 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, (RuntimeObject*)L_1);
	}

IL_0013:
	{
		__this->set_enumerator_6((RuntimeObject*)NULL);
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		return;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.Substring,System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectEnumerableIterator_2_MoveNext_mD60FF0AB4AC763AE4BA6EAE348FDC7EA509AEAE9_gshared (WhereSelectEnumerableIterator_2_t5E3BB9AFBEC27ED62FC129516C2D3E9FC81641B6 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		RuntimeObject* L_3 = (RuntimeObject*)__this->get_source_3();
		NullCheck((RuntimeObject*)L_3);
		RuntimeObject* L_4;
		L_4 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<UnityEngine.InputSystem.Utilities.Substring>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 5), (RuntimeObject*)L_3);
		__this->set_enumerator_6(L_4);
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		RuntimeObject* L_5 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_5);
		Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F  L_6;
		L_6 = InterfaceFuncInvoker0< Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<UnityEngine.InputSystem.Utilities.Substring>::get_Current() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 6), (RuntimeObject*)L_5);
		V_1 = (Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F )L_6;
		Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 * L_7 = (Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 * L_8 = (Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 *)__this->get_predicate_4();
		Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F  L_9 = V_1;
		NullCheck((Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 *, Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 *)L_8, (Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t4EC2F095EDDC8079537CA0FEDC463635E599C9E4 * L_11 = (Func_2_t4EC2F095EDDC8079537CA0FEDC463635E599C9E4 *)__this->get_selector_5();
		Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F  L_12 = V_1;
		NullCheck((Func_2_t4EC2F095EDDC8079537CA0FEDC463635E599C9E4 *)L_11);
		RuntimeObject * L_13;
		L_13 = ((  RuntimeObject * (*) (Func_2_t4EC2F095EDDC8079537CA0FEDC463635E599C9E4 *, Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((Func_2_t4EC2F095EDDC8079537CA0FEDC463635E599C9E4 *)L_11, (Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		RuntimeObject* L_14 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_14);
		bool L_15;
		L_15 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, (RuntimeObject*)L_14);
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Object>::Dispose() */, (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.Substring,System.Object>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectEnumerableIterator_2_Where_mBF1A5FD064FF1F3B1AB04FF8C356B1B518C27AB9_gshared (WhereSelectEnumerableIterator_2_t5E3BB9AFBEC27ED62FC129516C2D3E9FC81641B6 * __this, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 * L_1 = (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 9));
		((  void (*) (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *, RuntimeObject*, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,UnityEngine.InputSystem.Utilities.InternedString>::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2__ctor_mEB1554887BB44846ADDBE693A7CB91D7060C9283_gshared (WhereSelectEnumerableIterator_2_t6D595CFCE85C8B8D77C138F46CCF2608E20474FE * __this, RuntimeObject* ___source0, Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 * ___predicate1, Func_2_t48DD1C8E439811C492BAFE220A679DB57D7C9370 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		((  void (*) (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		RuntimeObject* L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t48DD1C8E439811C492BAFE220A679DB57D7C9370 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,UnityEngine.InputSystem.Utilities.InternedString>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 * WhereSelectEnumerableIterator_2_Clone_mA0CE7A47D92F90755812C8F979FAE2CF74D59260_gshared (WhereSelectEnumerableIterator_2_t6D595CFCE85C8B8D77C138F46CCF2608E20474FE * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_source_3();
		Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 * L_1 = (Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 *)__this->get_predicate_4();
		Func_2_t48DD1C8E439811C492BAFE220A679DB57D7C9370 * L_2 = (Func_2_t48DD1C8E439811C492BAFE220A679DB57D7C9370 *)__this->get_selector_5();
		WhereSelectEnumerableIterator_2_t6D595CFCE85C8B8D77C138F46CCF2608E20474FE * L_3 = (WhereSelectEnumerableIterator_2_t6D595CFCE85C8B8D77C138F46CCF2608E20474FE *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectEnumerableIterator_2_t6D595CFCE85C8B8D77C138F46CCF2608E20474FE *, RuntimeObject*, Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 *, Func_2_t48DD1C8E439811C492BAFE220A679DB57D7C9370 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (RuntimeObject*)L_0, (Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 *)L_1, (Func_2_t48DD1C8E439811C492BAFE220A679DB57D7C9370 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)L_3;
	}
}
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,UnityEngine.InputSystem.Utilities.InternedString>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2_Dispose_m97919E49A44E867E1572B62076D9B0C8D450899C_gshared (WhereSelectEnumerableIterator_2_t6D595CFCE85C8B8D77C138F46CCF2608E20474FE * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_enumerator_6();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		RuntimeObject* L_1 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, (RuntimeObject*)L_1);
	}

IL_0013:
	{
		__this->set_enumerator_6((RuntimeObject*)NULL);
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		((  void (*) (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		return;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,UnityEngine.InputSystem.Utilities.InternedString>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectEnumerableIterator_2_MoveNext_m6CFFBD72246D7FFFB93493009D52CE53C3043BA5_gshared (WhereSelectEnumerableIterator_2_t6D595CFCE85C8B8D77C138F46CCF2608E20474FE * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		RuntimeObject* L_3 = (RuntimeObject*)__this->get_source_3();
		NullCheck((RuntimeObject*)L_3);
		RuntimeObject* L_4;
		L_4 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 5), (RuntimeObject*)L_3);
		__this->set_enumerator_6(L_4);
		((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		RuntimeObject* L_5 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_5);
		JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB  L_6;
		L_6 = InterfaceFuncInvoker0< JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>::get_Current() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 6), (RuntimeObject*)L_5);
		V_1 = (JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB )L_6;
		Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 * L_7 = (Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 * L_8 = (Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 *)__this->get_predicate_4();
		JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB  L_9 = V_1;
		NullCheck((Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 *, JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 *)L_8, (JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t48DD1C8E439811C492BAFE220A679DB57D7C9370 * L_11 = (Func_2_t48DD1C8E439811C492BAFE220A679DB57D7C9370 *)__this->get_selector_5();
		JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB  L_12 = V_1;
		NullCheck((Func_2_t48DD1C8E439811C492BAFE220A679DB57D7C9370 *)L_11);
		InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  L_13;
		L_13 = ((  InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  (*) (Func_2_t48DD1C8E439811C492BAFE220A679DB57D7C9370 *, JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((Func_2_t48DD1C8E439811C492BAFE220A679DB57D7C9370 *)L_11, (JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		RuntimeObject* L_14 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_14);
		bool L_15;
		L_15 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, (RuntimeObject*)L_14);
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<UnityEngine.InputSystem.Utilities.InternedString>::Dispose() */, (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,UnityEngine.InputSystem.Utilities.InternedString>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectEnumerableIterator_2_Where_m83D9F471AEB6628EEED86E1D33C477D708DED682_gshared (WhereSelectEnumerableIterator_2_t6D595CFCE85C8B8D77C138F46CCF2608E20474FE * __this, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B * L_1 = (WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 9));
		((  void (*) (WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B *, RuntimeObject*, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,System.Object>::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2__ctor_mB64E44E1E8B15B323EAC0392F3F958BB88A225EC_gshared (WhereSelectEnumerableIterator_2_t4DAE42F4C708266EAC938F1C6E7FB762CDF50463 * __this, RuntimeObject* ___source0, Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 * ___predicate1, Func_2_t1628B2E4EEC7A32D5B688E54415BC42EDA9D2899 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		RuntimeObject* L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t1628B2E4EEC7A32D5B688E54415BC42EDA9D2899 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,System.Object>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 * WhereSelectEnumerableIterator_2_Clone_mEC8A264661441A761F128395B2FE29D20FB36568_gshared (WhereSelectEnumerableIterator_2_t4DAE42F4C708266EAC938F1C6E7FB762CDF50463 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_source_3();
		Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 * L_1 = (Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 *)__this->get_predicate_4();
		Func_2_t1628B2E4EEC7A32D5B688E54415BC42EDA9D2899 * L_2 = (Func_2_t1628B2E4EEC7A32D5B688E54415BC42EDA9D2899 *)__this->get_selector_5();
		WhereSelectEnumerableIterator_2_t4DAE42F4C708266EAC938F1C6E7FB762CDF50463 * L_3 = (WhereSelectEnumerableIterator_2_t4DAE42F4C708266EAC938F1C6E7FB762CDF50463 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectEnumerableIterator_2_t4DAE42F4C708266EAC938F1C6E7FB762CDF50463 *, RuntimeObject*, Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 *, Func_2_t1628B2E4EEC7A32D5B688E54415BC42EDA9D2899 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (RuntimeObject*)L_0, (Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 *)L_1, (Func_2_t1628B2E4EEC7A32D5B688E54415BC42EDA9D2899 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)L_3;
	}
}
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,System.Object>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2_Dispose_mE2C310B640FAE6578FCDC4DEDAD4649A92761F95_gshared (WhereSelectEnumerableIterator_2_t4DAE42F4C708266EAC938F1C6E7FB762CDF50463 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_enumerator_6();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		RuntimeObject* L_1 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, (RuntimeObject*)L_1);
	}

IL_0013:
	{
		__this->set_enumerator_6((RuntimeObject*)NULL);
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		return;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectEnumerableIterator_2_MoveNext_m83E7C067395255D79680E4A165CF10B744DEE224_gshared (WhereSelectEnumerableIterator_2_t4DAE42F4C708266EAC938F1C6E7FB762CDF50463 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		RuntimeObject* L_3 = (RuntimeObject*)__this->get_source_3();
		NullCheck((RuntimeObject*)L_3);
		RuntimeObject* L_4;
		L_4 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 5), (RuntimeObject*)L_3);
		__this->set_enumerator_6(L_4);
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		RuntimeObject* L_5 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_5);
		JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB  L_6;
		L_6 = InterfaceFuncInvoker0< JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>::get_Current() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 6), (RuntimeObject*)L_5);
		V_1 = (JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB )L_6;
		Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 * L_7 = (Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 * L_8 = (Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 *)__this->get_predicate_4();
		JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB  L_9 = V_1;
		NullCheck((Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 *, JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 *)L_8, (JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t1628B2E4EEC7A32D5B688E54415BC42EDA9D2899 * L_11 = (Func_2_t1628B2E4EEC7A32D5B688E54415BC42EDA9D2899 *)__this->get_selector_5();
		JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB  L_12 = V_1;
		NullCheck((Func_2_t1628B2E4EEC7A32D5B688E54415BC42EDA9D2899 *)L_11);
		RuntimeObject * L_13;
		L_13 = ((  RuntimeObject * (*) (Func_2_t1628B2E4EEC7A32D5B688E54415BC42EDA9D2899 *, JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((Func_2_t1628B2E4EEC7A32D5B688E54415BC42EDA9D2899 *)L_11, (JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		RuntimeObject* L_14 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_14);
		bool L_15;
		L_15 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, (RuntimeObject*)L_14);
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Object>::Dispose() */, (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,System.Object>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectEnumerableIterator_2_Where_m64C3EA1953B1BD16F315D6ED43AB686D3E58AF3F_gshared (WhereSelectEnumerableIterator_2_t4DAE42F4C708266EAC938F1C6E7FB762CDF50463 * __this, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 * L_1 = (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 9));
		((  void (*) (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *, RuntimeObject*, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectListIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,UnityEngine.InputSystem.Utilities.InternedString>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_m836BE8633678001AF517F14BCE81216A6D7CD395_gshared (WhereSelectListIterator_2_tF2F8745320F9D29534105CFDF2FADABD42A2D3F7 * __this, List_1_t9B1CABCBD4B6B7781E348FF332B32B333B543BA8 * ___source0, Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 * ___predicate1, Func_2_tB181F4C9A24C0634DF4A10CFBFFE6B45B3796BBC * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		((  void (*) (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_t9B1CABCBD4B6B7781E348FF332B32B333B543BA8 * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_tB181F4C9A24C0634DF4A10CFBFFE6B45B3796BBC * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,UnityEngine.InputSystem.Utilities.InternedString>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 * WhereSelectListIterator_2_Clone_mF78BE624C6FAA04AFA8F86E043EA6480F485AF4F_gshared (WhereSelectListIterator_2_tF2F8745320F9D29534105CFDF2FADABD42A2D3F7 * __this, const RuntimeMethod* method)
{
	{
		List_1_t9B1CABCBD4B6B7781E348FF332B32B333B543BA8 * L_0 = (List_1_t9B1CABCBD4B6B7781E348FF332B32B333B543BA8 *)__this->get_source_3();
		Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 * L_1 = (Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 *)__this->get_predicate_4();
		Func_2_tB181F4C9A24C0634DF4A10CFBFFE6B45B3796BBC * L_2 = (Func_2_tB181F4C9A24C0634DF4A10CFBFFE6B45B3796BBC *)__this->get_selector_5();
		WhereSelectListIterator_2_tF2F8745320F9D29534105CFDF2FADABD42A2D3F7 * L_3 = (WhereSelectListIterator_2_tF2F8745320F9D29534105CFDF2FADABD42A2D3F7 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_tF2F8745320F9D29534105CFDF2FADABD42A2D3F7 *, List_1_t9B1CABCBD4B6B7781E348FF332B32B333B543BA8 *, Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 *, Func_2_tB181F4C9A24C0634DF4A10CFBFFE6B45B3796BBC *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_t9B1CABCBD4B6B7781E348FF332B32B333B543BA8 *)L_0, (Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 *)L_1, (Func_2_tB181F4C9A24C0634DF4A10CFBFFE6B45B3796BBC *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,UnityEngine.InputSystem.Utilities.InternedString>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_m1A03C3D41BAF23708560F81A7A75B6683F092FEA_gshared (WhereSelectListIterator_2_tF2F8745320F9D29534105CFDF2FADABD42A2D3F7 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_t9B1CABCBD4B6B7781E348FF332B32B333B543BA8 * L_3 = (List_1_t9B1CABCBD4B6B7781E348FF332B32B333B543BA8 *)__this->get_source_3();
		NullCheck((List_1_t9B1CABCBD4B6B7781E348FF332B32B333B543BA8 *)L_3);
		Enumerator_tF2D0EEBD6B861D9127DD6DC36B733FD42070858C  L_4;
		L_4 = ((  Enumerator_tF2D0EEBD6B861D9127DD6DC36B733FD42070858C  (*) (List_1_t9B1CABCBD4B6B7781E348FF332B32B333B543BA8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_t9B1CABCBD4B6B7781E348FF332B32B333B543BA8 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_tF2D0EEBD6B861D9127DD6DC36B733FD42070858C * L_5 = (Enumerator_tF2D0EEBD6B861D9127DD6DC36B733FD42070858C *)__this->get_address_of_enumerator_6();
		KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184  L_6;
		L_6 = Enumerator_get_Current_mD55E3F632B5C19FA05221B587346BA3268167712_inline((Enumerator_tF2D0EEBD6B861D9127DD6DC36B733FD42070858C *)(Enumerator_tF2D0EEBD6B861D9127DD6DC36B733FD42070858C *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184 )L_6;
		Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 * L_7 = (Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 * L_8 = (Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 *)__this->get_predicate_4();
		KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184  L_9 = V_1;
		NullCheck((Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 *, KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 *)L_8, (KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184 )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_tB181F4C9A24C0634DF4A10CFBFFE6B45B3796BBC * L_11 = (Func_2_tB181F4C9A24C0634DF4A10CFBFFE6B45B3796BBC *)__this->get_selector_5();
		KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184  L_12 = V_1;
		NullCheck((Func_2_tB181F4C9A24C0634DF4A10CFBFFE6B45B3796BBC *)L_11);
		InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  L_13;
		L_13 = ((  InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  (*) (Func_2_tB181F4C9A24C0634DF4A10CFBFFE6B45B3796BBC *, KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_tB181F4C9A24C0634DF4A10CFBFFE6B45B3796BBC *)L_11, (KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184 )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_tF2D0EEBD6B861D9127DD6DC36B733FD42070858C * L_14 = (Enumerator_tF2D0EEBD6B861D9127DD6DC36B733FD42070858C *)__this->get_address_of_enumerator_6();
		bool L_15;
		L_15 = Enumerator_MoveNext_m6A3C20CEFE29B5B761E6C9DBA053A2CE897E2515((Enumerator_tF2D0EEBD6B861D9127DD6DC36B733FD42070858C *)(Enumerator_tF2D0EEBD6B861D9127DD6DC36B733FD42070858C *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<UnityEngine.InputSystem.Utilities.InternedString>::Dispose() */, (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,UnityEngine.InputSystem.Utilities.InternedString>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_m6957A7D9886D7DC345273320A8C776E25EC0988F_gshared (WhereSelectListIterator_2_tF2F8745320F9D29534105CFDF2FADABD42A2D3F7 * __this, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B * L_1 = (WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B *, RuntimeObject*, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectListIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,System.Object>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_mB716CAA33E53CB04DDB01C6EBD655146004006ED_gshared (WhereSelectListIterator_2_t15BF3EF128C005C26DE8132D49E71B3C1FE6E211 * __this, List_1_t9B1CABCBD4B6B7781E348FF332B32B333B543BA8 * ___source0, Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 * ___predicate1, Func_2_t711512467B454C91EB8F3FB19CD6D141AAB5B6B1 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_t9B1CABCBD4B6B7781E348FF332B32B333B543BA8 * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t711512467B454C91EB8F3FB19CD6D141AAB5B6B1 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,System.Object>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 * WhereSelectListIterator_2_Clone_mCFED812B078E7D31F56513331A7E6A2D775C902B_gshared (WhereSelectListIterator_2_t15BF3EF128C005C26DE8132D49E71B3C1FE6E211 * __this, const RuntimeMethod* method)
{
	{
		List_1_t9B1CABCBD4B6B7781E348FF332B32B333B543BA8 * L_0 = (List_1_t9B1CABCBD4B6B7781E348FF332B32B333B543BA8 *)__this->get_source_3();
		Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 * L_1 = (Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 *)__this->get_predicate_4();
		Func_2_t711512467B454C91EB8F3FB19CD6D141AAB5B6B1 * L_2 = (Func_2_t711512467B454C91EB8F3FB19CD6D141AAB5B6B1 *)__this->get_selector_5();
		WhereSelectListIterator_2_t15BF3EF128C005C26DE8132D49E71B3C1FE6E211 * L_3 = (WhereSelectListIterator_2_t15BF3EF128C005C26DE8132D49E71B3C1FE6E211 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_t15BF3EF128C005C26DE8132D49E71B3C1FE6E211 *, List_1_t9B1CABCBD4B6B7781E348FF332B32B333B543BA8 *, Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 *, Func_2_t711512467B454C91EB8F3FB19CD6D141AAB5B6B1 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_t9B1CABCBD4B6B7781E348FF332B32B333B543BA8 *)L_0, (Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 *)L_1, (Func_2_t711512467B454C91EB8F3FB19CD6D141AAB5B6B1 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_m176A20043994757122A68EC28CC9E0EE00E09233_gshared (WhereSelectListIterator_2_t15BF3EF128C005C26DE8132D49E71B3C1FE6E211 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_t9B1CABCBD4B6B7781E348FF332B32B333B543BA8 * L_3 = (List_1_t9B1CABCBD4B6B7781E348FF332B32B333B543BA8 *)__this->get_source_3();
		NullCheck((List_1_t9B1CABCBD4B6B7781E348FF332B32B333B543BA8 *)L_3);
		Enumerator_tF2D0EEBD6B861D9127DD6DC36B733FD42070858C  L_4;
		L_4 = ((  Enumerator_tF2D0EEBD6B861D9127DD6DC36B733FD42070858C  (*) (List_1_t9B1CABCBD4B6B7781E348FF332B32B333B543BA8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_t9B1CABCBD4B6B7781E348FF332B32B333B543BA8 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_tF2D0EEBD6B861D9127DD6DC36B733FD42070858C * L_5 = (Enumerator_tF2D0EEBD6B861D9127DD6DC36B733FD42070858C *)__this->get_address_of_enumerator_6();
		KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184  L_6;
		L_6 = Enumerator_get_Current_mD55E3F632B5C19FA05221B587346BA3268167712_inline((Enumerator_tF2D0EEBD6B861D9127DD6DC36B733FD42070858C *)(Enumerator_tF2D0EEBD6B861D9127DD6DC36B733FD42070858C *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184 )L_6;
		Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 * L_7 = (Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 * L_8 = (Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 *)__this->get_predicate_4();
		KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184  L_9 = V_1;
		NullCheck((Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 *, KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 *)L_8, (KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184 )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t711512467B454C91EB8F3FB19CD6D141AAB5B6B1 * L_11 = (Func_2_t711512467B454C91EB8F3FB19CD6D141AAB5B6B1 *)__this->get_selector_5();
		KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184  L_12 = V_1;
		NullCheck((Func_2_t711512467B454C91EB8F3FB19CD6D141AAB5B6B1 *)L_11);
		RuntimeObject * L_13;
		L_13 = ((  RuntimeObject * (*) (Func_2_t711512467B454C91EB8F3FB19CD6D141AAB5B6B1 *, KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t711512467B454C91EB8F3FB19CD6D141AAB5B6B1 *)L_11, (KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184 )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_tF2D0EEBD6B861D9127DD6DC36B733FD42070858C * L_14 = (Enumerator_tF2D0EEBD6B861D9127DD6DC36B733FD42070858C *)__this->get_address_of_enumerator_6();
		bool L_15;
		L_15 = Enumerator_MoveNext_m6A3C20CEFE29B5B761E6C9DBA053A2CE897E2515((Enumerator_tF2D0EEBD6B861D9127DD6DC36B733FD42070858C *)(Enumerator_tF2D0EEBD6B861D9127DD6DC36B733FD42070858C *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Object>::Dispose() */, (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,System.Object>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_m2E9DAB15C4F608A7EA705DBDC57A5CCA3ECB8C86_gshared (WhereSelectListIterator_2_t15BF3EF128C005C26DE8132D49E71B3C1FE6E211 * __this, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 * L_1 = (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *, RuntimeObject*, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectListIterator`2<Oculus.Interaction.Input.HandSkeletonJoint,Oculus.Interaction.Input.HandSkeletonJoint>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_mB62C5110089E532E5A02FC5E2FB02B86D28E89D6_gshared (WhereSelectListIterator_2_t912D2CA762EC2D53D104D6B1FFE8BCEF3FA7319F * __this, List_1_t92F561CC77DE516A0293B29C982ACDC3391E1658 * ___source0, Func_2_t31607A88702EFEFC5B33245A610C64EA7D6ED9F7 * ___predicate1, Func_2_tB339A94B37E01030C823BB96512593067BEF80DC * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_tBD3F91668D911F5945632A9FDF70A5848BD6CC26 *)__this);
		((  void (*) (Iterator_1_tBD3F91668D911F5945632A9FDF70A5848BD6CC26 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_tBD3F91668D911F5945632A9FDF70A5848BD6CC26 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_t92F561CC77DE516A0293B29C982ACDC3391E1658 * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t31607A88702EFEFC5B33245A610C64EA7D6ED9F7 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_tB339A94B37E01030C823BB96512593067BEF80DC * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<Oculus.Interaction.Input.HandSkeletonJoint,Oculus.Interaction.Input.HandSkeletonJoint>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_tBD3F91668D911F5945632A9FDF70A5848BD6CC26 * WhereSelectListIterator_2_Clone_m27C5EAA1307A48F8CB899B7DA0A82EF3E688884A_gshared (WhereSelectListIterator_2_t912D2CA762EC2D53D104D6B1FFE8BCEF3FA7319F * __this, const RuntimeMethod* method)
{
	{
		List_1_t92F561CC77DE516A0293B29C982ACDC3391E1658 * L_0 = (List_1_t92F561CC77DE516A0293B29C982ACDC3391E1658 *)__this->get_source_3();
		Func_2_t31607A88702EFEFC5B33245A610C64EA7D6ED9F7 * L_1 = (Func_2_t31607A88702EFEFC5B33245A610C64EA7D6ED9F7 *)__this->get_predicate_4();
		Func_2_tB339A94B37E01030C823BB96512593067BEF80DC * L_2 = (Func_2_tB339A94B37E01030C823BB96512593067BEF80DC *)__this->get_selector_5();
		WhereSelectListIterator_2_t912D2CA762EC2D53D104D6B1FFE8BCEF3FA7319F * L_3 = (WhereSelectListIterator_2_t912D2CA762EC2D53D104D6B1FFE8BCEF3FA7319F *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_t912D2CA762EC2D53D104D6B1FFE8BCEF3FA7319F *, List_1_t92F561CC77DE516A0293B29C982ACDC3391E1658 *, Func_2_t31607A88702EFEFC5B33245A610C64EA7D6ED9F7 *, Func_2_tB339A94B37E01030C823BB96512593067BEF80DC *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_t92F561CC77DE516A0293B29C982ACDC3391E1658 *)L_0, (Func_2_t31607A88702EFEFC5B33245A610C64EA7D6ED9F7 *)L_1, (Func_2_tB339A94B37E01030C823BB96512593067BEF80DC *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_tBD3F91668D911F5945632A9FDF70A5848BD6CC26 *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2<Oculus.Interaction.Input.HandSkeletonJoint,Oculus.Interaction.Input.HandSkeletonJoint>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_m155CB4CA63388EC1D4F62D6C9BC9F16D7C44C6C3_gshared (WhereSelectListIterator_2_t912D2CA762EC2D53D104D6B1FFE8BCEF3FA7319F * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	HandSkeletonJoint_t5B4E2A23FDAC8BC4C8751E5FBC7C96A1056582F3  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_tBD3F91668D911F5945632A9FDF70A5848BD6CC26 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_t92F561CC77DE516A0293B29C982ACDC3391E1658 * L_3 = (List_1_t92F561CC77DE516A0293B29C982ACDC3391E1658 *)__this->get_source_3();
		NullCheck((List_1_t92F561CC77DE516A0293B29C982ACDC3391E1658 *)L_3);
		Enumerator_t749ECD21102D25154785738AC993322CE5A9A3D6  L_4;
		L_4 = ((  Enumerator_t749ECD21102D25154785738AC993322CE5A9A3D6  (*) (List_1_t92F561CC77DE516A0293B29C982ACDC3391E1658 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_t92F561CC77DE516A0293B29C982ACDC3391E1658 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_tBD3F91668D911F5945632A9FDF70A5848BD6CC26 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_t749ECD21102D25154785738AC993322CE5A9A3D6 * L_5 = (Enumerator_t749ECD21102D25154785738AC993322CE5A9A3D6 *)__this->get_address_of_enumerator_6();
		HandSkeletonJoint_t5B4E2A23FDAC8BC4C8751E5FBC7C96A1056582F3  L_6;
		L_6 = Enumerator_get_Current_mFF6988CD9E5309A0577F8335E26AA7191198035F_inline((Enumerator_t749ECD21102D25154785738AC993322CE5A9A3D6 *)(Enumerator_t749ECD21102D25154785738AC993322CE5A9A3D6 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (HandSkeletonJoint_t5B4E2A23FDAC8BC4C8751E5FBC7C96A1056582F3 )L_6;
		Func_2_t31607A88702EFEFC5B33245A610C64EA7D6ED9F7 * L_7 = (Func_2_t31607A88702EFEFC5B33245A610C64EA7D6ED9F7 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t31607A88702EFEFC5B33245A610C64EA7D6ED9F7 * L_8 = (Func_2_t31607A88702EFEFC5B33245A610C64EA7D6ED9F7 *)__this->get_predicate_4();
		HandSkeletonJoint_t5B4E2A23FDAC8BC4C8751E5FBC7C96A1056582F3  L_9 = V_1;
		NullCheck((Func_2_t31607A88702EFEFC5B33245A610C64EA7D6ED9F7 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t31607A88702EFEFC5B33245A610C64EA7D6ED9F7 *, HandSkeletonJoint_t5B4E2A23FDAC8BC4C8751E5FBC7C96A1056582F3 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t31607A88702EFEFC5B33245A610C64EA7D6ED9F7 *)L_8, (HandSkeletonJoint_t5B4E2A23FDAC8BC4C8751E5FBC7C96A1056582F3 )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_tB339A94B37E01030C823BB96512593067BEF80DC * L_11 = (Func_2_tB339A94B37E01030C823BB96512593067BEF80DC *)__this->get_selector_5();
		HandSkeletonJoint_t5B4E2A23FDAC8BC4C8751E5FBC7C96A1056582F3  L_12 = V_1;
		NullCheck((Func_2_tB339A94B37E01030C823BB96512593067BEF80DC *)L_11);
		HandSkeletonJoint_t5B4E2A23FDAC8BC4C8751E5FBC7C96A1056582F3  L_13;
		L_13 = ((  HandSkeletonJoint_t5B4E2A23FDAC8BC4C8751E5FBC7C96A1056582F3  (*) (Func_2_tB339A94B37E01030C823BB96512593067BEF80DC *, HandSkeletonJoint_t5B4E2A23FDAC8BC4C8751E5FBC7C96A1056582F3 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_tB339A94B37E01030C823BB96512593067BEF80DC *)L_11, (HandSkeletonJoint_t5B4E2A23FDAC8BC4C8751E5FBC7C96A1056582F3 )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_tBD3F91668D911F5945632A9FDF70A5848BD6CC26 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_t749ECD21102D25154785738AC993322CE5A9A3D6 * L_14 = (Enumerator_t749ECD21102D25154785738AC993322CE5A9A3D6 *)__this->get_address_of_enumerator_6();
		bool L_15;
		L_15 = Enumerator_MoveNext_m7104D812814B404A855DFD8BCE0C67FD044F6377((Enumerator_t749ECD21102D25154785738AC993322CE5A9A3D6 *)(Enumerator_t749ECD21102D25154785738AC993322CE5A9A3D6 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_tBD3F91668D911F5945632A9FDF70A5848BD6CC26 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<Oculus.Interaction.Input.HandSkeletonJoint>::Dispose() */, (Iterator_1_tBD3F91668D911F5945632A9FDF70A5848BD6CC26 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<Oculus.Interaction.Input.HandSkeletonJoint,Oculus.Interaction.Input.HandSkeletonJoint>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_m2030D885386103FDA9172672D90671ED5741C957_gshared (WhereSelectListIterator_2_t912D2CA762EC2D53D104D6B1FFE8BCEF3FA7319F * __this, Func_2_t31607A88702EFEFC5B33245A610C64EA7D6ED9F7 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t31607A88702EFEFC5B33245A610C64EA7D6ED9F7 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t7E214A8B0456E617DE0100DC5CA366442CB22EBD * L_1 = (WhereEnumerableIterator_1_t7E214A8B0456E617DE0100DC5CA366442CB22EBD *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t7E214A8B0456E617DE0100DC5CA366442CB22EBD *, RuntimeObject*, Func_2_t31607A88702EFEFC5B33245A610C64EA7D6ED9F7 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t31607A88702EFEFC5B33245A610C64EA7D6ED9F7 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectListIterator`2<System.Int32Enum,UnityEngine.InputSystem.Utilities.InternedString>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_m27431381D20B04B080CAF59D3E75DE63B2BEC156_gshared (WhereSelectListIterator_2_t75D7B928E94826A5A6BBCB97842E5B2C3BBDF5F5 * __this, List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A * ___source0, Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E * ___predicate1, Func_2_t043042489DBF19074B5B434F090F7B8AAE97E138 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		((  void (*) (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t043042489DBF19074B5B434F090F7B8AAE97E138 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<System.Int32Enum,UnityEngine.InputSystem.Utilities.InternedString>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 * WhereSelectListIterator_2_Clone_m71476D5AD6107BDA5360EBA1FB86ADD381DAF2D9_gshared (WhereSelectListIterator_2_t75D7B928E94826A5A6BBCB97842E5B2C3BBDF5F5 * __this, const RuntimeMethod* method)
{
	{
		List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A * L_0 = (List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A *)__this->get_source_3();
		Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E * L_1 = (Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E *)__this->get_predicate_4();
		Func_2_t043042489DBF19074B5B434F090F7B8AAE97E138 * L_2 = (Func_2_t043042489DBF19074B5B434F090F7B8AAE97E138 *)__this->get_selector_5();
		WhereSelectListIterator_2_t75D7B928E94826A5A6BBCB97842E5B2C3BBDF5F5 * L_3 = (WhereSelectListIterator_2_t75D7B928E94826A5A6BBCB97842E5B2C3BBDF5F5 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_t75D7B928E94826A5A6BBCB97842E5B2C3BBDF5F5 *, List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A *, Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E *, Func_2_t043042489DBF19074B5B434F090F7B8AAE97E138 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A *)L_0, (Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E *)L_1, (Func_2_t043042489DBF19074B5B434F090F7B8AAE97E138 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2<System.Int32Enum,UnityEngine.InputSystem.Utilities.InternedString>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_m5B6877E5436191CB1DDB0C9369ECD7E31BF3114D_gshared (WhereSelectListIterator_2_t75D7B928E94826A5A6BBCB97842E5B2C3BBDF5F5 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = (int32_t)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A * L_3 = (List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A *)__this->get_source_3();
		NullCheck((List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A *)L_3);
		Enumerator_t3A873D53A96DA0AC1C030CB5A0BF2D0F57FCC984  L_4;
		L_4 = ((  Enumerator_t3A873D53A96DA0AC1C030CB5A0BF2D0F57FCC984  (*) (List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_t3A873D53A96DA0AC1C030CB5A0BF2D0F57FCC984 * L_5 = (Enumerator_t3A873D53A96DA0AC1C030CB5A0BF2D0F57FCC984 *)__this->get_address_of_enumerator_6();
		int32_t L_6;
		L_6 = Enumerator_get_Current_mDD503AFD786235D3B40842B0872AC17DC86EB040_inline((Enumerator_t3A873D53A96DA0AC1C030CB5A0BF2D0F57FCC984 *)(Enumerator_t3A873D53A96DA0AC1C030CB5A0BF2D0F57FCC984 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (int32_t)L_6;
		Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E * L_7 = (Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E * L_8 = (Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E *)__this->get_predicate_4();
		int32_t L_9 = V_1;
		NullCheck((Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E *)L_8, (int32_t)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t043042489DBF19074B5B434F090F7B8AAE97E138 * L_11 = (Func_2_t043042489DBF19074B5B434F090F7B8AAE97E138 *)__this->get_selector_5();
		int32_t L_12 = V_1;
		NullCheck((Func_2_t043042489DBF19074B5B434F090F7B8AAE97E138 *)L_11);
		InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  L_13;
		L_13 = ((  InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  (*) (Func_2_t043042489DBF19074B5B434F090F7B8AAE97E138 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t043042489DBF19074B5B434F090F7B8AAE97E138 *)L_11, (int32_t)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_t3A873D53A96DA0AC1C030CB5A0BF2D0F57FCC984 * L_14 = (Enumerator_t3A873D53A96DA0AC1C030CB5A0BF2D0F57FCC984 *)__this->get_address_of_enumerator_6();
		bool L_15;
		L_15 = Enumerator_MoveNext_mDECBD2FBFA44578D8E050CECB33BF350152E111A((Enumerator_t3A873D53A96DA0AC1C030CB5A0BF2D0F57FCC984 *)(Enumerator_t3A873D53A96DA0AC1C030CB5A0BF2D0F57FCC984 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<UnityEngine.InputSystem.Utilities.InternedString>::Dispose() */, (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<System.Int32Enum,UnityEngine.InputSystem.Utilities.InternedString>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_mA15A6C6F560F84ACC687948C9AE627EC427AC7EA_gshared (WhereSelectListIterator_2_t75D7B928E94826A5A6BBCB97842E5B2C3BBDF5F5 * __this, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B * L_1 = (WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B *, RuntimeObject*, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectListIterator`2<System.Int32Enum,System.Object>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_mC542F38EC17CA13C41DB738F0D825E697982CA4E_gshared (WhereSelectListIterator_2_t5F082584C1A03211ACA908D2D073C1D6149B14A8 * __this, List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A * ___source0, Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E * ___predicate1, Func_2_tF916A06E36F9340A8A2E0053D6A8F0737C5FD2EB * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_tF916A06E36F9340A8A2E0053D6A8F0737C5FD2EB * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<System.Int32Enum,System.Object>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 * WhereSelectListIterator_2_Clone_m9A196A73CDF63D17862E2CA0BFF6FDCA3D0E8E47_gshared (WhereSelectListIterator_2_t5F082584C1A03211ACA908D2D073C1D6149B14A8 * __this, const RuntimeMethod* method)
{
	{
		List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A * L_0 = (List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A *)__this->get_source_3();
		Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E * L_1 = (Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E *)__this->get_predicate_4();
		Func_2_tF916A06E36F9340A8A2E0053D6A8F0737C5FD2EB * L_2 = (Func_2_tF916A06E36F9340A8A2E0053D6A8F0737C5FD2EB *)__this->get_selector_5();
		WhereSelectListIterator_2_t5F082584C1A03211ACA908D2D073C1D6149B14A8 * L_3 = (WhereSelectListIterator_2_t5F082584C1A03211ACA908D2D073C1D6149B14A8 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_t5F082584C1A03211ACA908D2D073C1D6149B14A8 *, List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A *, Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E *, Func_2_tF916A06E36F9340A8A2E0053D6A8F0737C5FD2EB *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A *)L_0, (Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E *)L_1, (Func_2_tF916A06E36F9340A8A2E0053D6A8F0737C5FD2EB *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2<System.Int32Enum,System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_m10D83660FAC86C1CBD7ED4E1368F14B5478499A5_gshared (WhereSelectListIterator_2_t5F082584C1A03211ACA908D2D073C1D6149B14A8 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = (int32_t)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A * L_3 = (List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A *)__this->get_source_3();
		NullCheck((List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A *)L_3);
		Enumerator_t3A873D53A96DA0AC1C030CB5A0BF2D0F57FCC984  L_4;
		L_4 = ((  Enumerator_t3A873D53A96DA0AC1C030CB5A0BF2D0F57FCC984  (*) (List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_t3A873D53A96DA0AC1C030CB5A0BF2D0F57FCC984 * L_5 = (Enumerator_t3A873D53A96DA0AC1C030CB5A0BF2D0F57FCC984 *)__this->get_address_of_enumerator_6();
		int32_t L_6;
		L_6 = Enumerator_get_Current_mDD503AFD786235D3B40842B0872AC17DC86EB040_inline((Enumerator_t3A873D53A96DA0AC1C030CB5A0BF2D0F57FCC984 *)(Enumerator_t3A873D53A96DA0AC1C030CB5A0BF2D0F57FCC984 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (int32_t)L_6;
		Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E * L_7 = (Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E * L_8 = (Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E *)__this->get_predicate_4();
		int32_t L_9 = V_1;
		NullCheck((Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E *)L_8, (int32_t)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_tF916A06E36F9340A8A2E0053D6A8F0737C5FD2EB * L_11 = (Func_2_tF916A06E36F9340A8A2E0053D6A8F0737C5FD2EB *)__this->get_selector_5();
		int32_t L_12 = V_1;
		NullCheck((Func_2_tF916A06E36F9340A8A2E0053D6A8F0737C5FD2EB *)L_11);
		RuntimeObject * L_13;
		L_13 = ((  RuntimeObject * (*) (Func_2_tF916A06E36F9340A8A2E0053D6A8F0737C5FD2EB *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_tF916A06E36F9340A8A2E0053D6A8F0737C5FD2EB *)L_11, (int32_t)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_t3A873D53A96DA0AC1C030CB5A0BF2D0F57FCC984 * L_14 = (Enumerator_t3A873D53A96DA0AC1C030CB5A0BF2D0F57FCC984 *)__this->get_address_of_enumerator_6();
		bool L_15;
		L_15 = Enumerator_MoveNext_mDECBD2FBFA44578D8E050CECB33BF350152E111A((Enumerator_t3A873D53A96DA0AC1C030CB5A0BF2D0F57FCC984 *)(Enumerator_t3A873D53A96DA0AC1C030CB5A0BF2D0F57FCC984 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Object>::Dispose() */, (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<System.Int32Enum,System.Object>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_m1E339E047A89DE7D5B31A0AE1A48944B1C933300_gshared (WhereSelectListIterator_2_t5F082584C1A03211ACA908D2D073C1D6149B14A8 * __this, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 * L_1 = (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *, RuntimeObject*, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.InternedString,UnityEngine.InputSystem.Utilities.InternedString>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_mAAC40497EC0DBDB002649E268CF2BDCE278389B1_gshared (WhereSelectListIterator_2_t7BDF9DF4ED7F42E4D8139C4BA35A0F055024799E * __this, List_1_tFD37D052760A236906215AF581C58EE13DE6A5DC * ___source0, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * ___predicate1, Func_2_t1AF155D27A4D78882342D697E4A6DE7CE594545B * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		((  void (*) (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_tFD37D052760A236906215AF581C58EE13DE6A5DC * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t1AF155D27A4D78882342D697E4A6DE7CE594545B * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.InternedString,UnityEngine.InputSystem.Utilities.InternedString>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 * WhereSelectListIterator_2_Clone_mC612A0592479FA931B0A0F19A18BE55E1F73ED3C_gshared (WhereSelectListIterator_2_t7BDF9DF4ED7F42E4D8139C4BA35A0F055024799E * __this, const RuntimeMethod* method)
{
	{
		List_1_tFD37D052760A236906215AF581C58EE13DE6A5DC * L_0 = (List_1_tFD37D052760A236906215AF581C58EE13DE6A5DC *)__this->get_source_3();
		Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * L_1 = (Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)__this->get_predicate_4();
		Func_2_t1AF155D27A4D78882342D697E4A6DE7CE594545B * L_2 = (Func_2_t1AF155D27A4D78882342D697E4A6DE7CE594545B *)__this->get_selector_5();
		WhereSelectListIterator_2_t7BDF9DF4ED7F42E4D8139C4BA35A0F055024799E * L_3 = (WhereSelectListIterator_2_t7BDF9DF4ED7F42E4D8139C4BA35A0F055024799E *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_t7BDF9DF4ED7F42E4D8139C4BA35A0F055024799E *, List_1_tFD37D052760A236906215AF581C58EE13DE6A5DC *, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *, Func_2_t1AF155D27A4D78882342D697E4A6DE7CE594545B *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_tFD37D052760A236906215AF581C58EE13DE6A5DC *)L_0, (Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)L_1, (Func_2_t1AF155D27A4D78882342D697E4A6DE7CE594545B *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.InternedString,UnityEngine.InputSystem.Utilities.InternedString>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_m828CFE7C90058763749CC34BE6FC3884C1511B76_gshared (WhereSelectListIterator_2_t7BDF9DF4ED7F42E4D8139C4BA35A0F055024799E * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_tFD37D052760A236906215AF581C58EE13DE6A5DC * L_3 = (List_1_tFD37D052760A236906215AF581C58EE13DE6A5DC *)__this->get_source_3();
		NullCheck((List_1_tFD37D052760A236906215AF581C58EE13DE6A5DC *)L_3);
		Enumerator_tD1ECE103520161C1BD172331D6968D3D184B3B6F  L_4;
		L_4 = ((  Enumerator_tD1ECE103520161C1BD172331D6968D3D184B3B6F  (*) (List_1_tFD37D052760A236906215AF581C58EE13DE6A5DC *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_tFD37D052760A236906215AF581C58EE13DE6A5DC *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_tD1ECE103520161C1BD172331D6968D3D184B3B6F * L_5 = (Enumerator_tD1ECE103520161C1BD172331D6968D3D184B3B6F *)__this->get_address_of_enumerator_6();
		InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  L_6;
		L_6 = Enumerator_get_Current_m3E41B27F955C5A287A6138E9D254833ACC2BC1B2_inline((Enumerator_tD1ECE103520161C1BD172331D6968D3D184B3B6F *)(Enumerator_tD1ECE103520161C1BD172331D6968D3D184B3B6F *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED )L_6;
		Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * L_7 = (Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * L_8 = (Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)__this->get_predicate_4();
		InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  L_9 = V_1;
		NullCheck((Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *, InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)L_8, (InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t1AF155D27A4D78882342D697E4A6DE7CE594545B * L_11 = (Func_2_t1AF155D27A4D78882342D697E4A6DE7CE594545B *)__this->get_selector_5();
		InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  L_12 = V_1;
		NullCheck((Func_2_t1AF155D27A4D78882342D697E4A6DE7CE594545B *)L_11);
		InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  L_13;
		L_13 = ((  InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  (*) (Func_2_t1AF155D27A4D78882342D697E4A6DE7CE594545B *, InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t1AF155D27A4D78882342D697E4A6DE7CE594545B *)L_11, (InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_tD1ECE103520161C1BD172331D6968D3D184B3B6F * L_14 = (Enumerator_tD1ECE103520161C1BD172331D6968D3D184B3B6F *)__this->get_address_of_enumerator_6();
		bool L_15;
		L_15 = Enumerator_MoveNext_mC755E973A650FFFB74A2F3E332EC10C3F035F15D((Enumerator_tD1ECE103520161C1BD172331D6968D3D184B3B6F *)(Enumerator_tD1ECE103520161C1BD172331D6968D3D184B3B6F *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<UnityEngine.InputSystem.Utilities.InternedString>::Dispose() */, (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.InternedString,UnityEngine.InputSystem.Utilities.InternedString>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_m083D1CB30DDAE12C1853DF7DDC0D600F07197F94_gshared (WhereSelectListIterator_2_t7BDF9DF4ED7F42E4D8139C4BA35A0F055024799E * __this, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B * L_1 = (WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B *, RuntimeObject*, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.InternedString,System.Object>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_m82FA65B16309AF7E65F6D14677673668A619BDB1_gshared (WhereSelectListIterator_2_t1C93CE1377887193667292BEED953C31167E2F75 * __this, List_1_tFD37D052760A236906215AF581C58EE13DE6A5DC * ___source0, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * ___predicate1, Func_2_tA2BD34BB8D97AD282016BD4CB5B8675E4AF85B45 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_tFD37D052760A236906215AF581C58EE13DE6A5DC * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_tA2BD34BB8D97AD282016BD4CB5B8675E4AF85B45 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.InternedString,System.Object>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 * WhereSelectListIterator_2_Clone_mF146681D405FD0CB3EACC00EB4A8EA93423514C0_gshared (WhereSelectListIterator_2_t1C93CE1377887193667292BEED953C31167E2F75 * __this, const RuntimeMethod* method)
{
	{
		List_1_tFD37D052760A236906215AF581C58EE13DE6A5DC * L_0 = (List_1_tFD37D052760A236906215AF581C58EE13DE6A5DC *)__this->get_source_3();
		Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * L_1 = (Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)__this->get_predicate_4();
		Func_2_tA2BD34BB8D97AD282016BD4CB5B8675E4AF85B45 * L_2 = (Func_2_tA2BD34BB8D97AD282016BD4CB5B8675E4AF85B45 *)__this->get_selector_5();
		WhereSelectListIterator_2_t1C93CE1377887193667292BEED953C31167E2F75 * L_3 = (WhereSelectListIterator_2_t1C93CE1377887193667292BEED953C31167E2F75 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_t1C93CE1377887193667292BEED953C31167E2F75 *, List_1_tFD37D052760A236906215AF581C58EE13DE6A5DC *, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *, Func_2_tA2BD34BB8D97AD282016BD4CB5B8675E4AF85B45 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_tFD37D052760A236906215AF581C58EE13DE6A5DC *)L_0, (Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)L_1, (Func_2_tA2BD34BB8D97AD282016BD4CB5B8675E4AF85B45 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.InternedString,System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_m05A521A2DB97CA5ED55BC44A411E2C680D0FD34C_gshared (WhereSelectListIterator_2_t1C93CE1377887193667292BEED953C31167E2F75 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_tFD37D052760A236906215AF581C58EE13DE6A5DC * L_3 = (List_1_tFD37D052760A236906215AF581C58EE13DE6A5DC *)__this->get_source_3();
		NullCheck((List_1_tFD37D052760A236906215AF581C58EE13DE6A5DC *)L_3);
		Enumerator_tD1ECE103520161C1BD172331D6968D3D184B3B6F  L_4;
		L_4 = ((  Enumerator_tD1ECE103520161C1BD172331D6968D3D184B3B6F  (*) (List_1_tFD37D052760A236906215AF581C58EE13DE6A5DC *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_tFD37D052760A236906215AF581C58EE13DE6A5DC *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_tD1ECE103520161C1BD172331D6968D3D184B3B6F * L_5 = (Enumerator_tD1ECE103520161C1BD172331D6968D3D184B3B6F *)__this->get_address_of_enumerator_6();
		InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  L_6;
		L_6 = Enumerator_get_Current_m3E41B27F955C5A287A6138E9D254833ACC2BC1B2_inline((Enumerator_tD1ECE103520161C1BD172331D6968D3D184B3B6F *)(Enumerator_tD1ECE103520161C1BD172331D6968D3D184B3B6F *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED )L_6;
		Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * L_7 = (Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * L_8 = (Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)__this->get_predicate_4();
		InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  L_9 = V_1;
		NullCheck((Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *, InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)L_8, (InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_tA2BD34BB8D97AD282016BD4CB5B8675E4AF85B45 * L_11 = (Func_2_tA2BD34BB8D97AD282016BD4CB5B8675E4AF85B45 *)__this->get_selector_5();
		InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  L_12 = V_1;
		NullCheck((Func_2_tA2BD34BB8D97AD282016BD4CB5B8675E4AF85B45 *)L_11);
		RuntimeObject * L_13;
		L_13 = ((  RuntimeObject * (*) (Func_2_tA2BD34BB8D97AD282016BD4CB5B8675E4AF85B45 *, InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_tA2BD34BB8D97AD282016BD4CB5B8675E4AF85B45 *)L_11, (InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_tD1ECE103520161C1BD172331D6968D3D184B3B6F * L_14 = (Enumerator_tD1ECE103520161C1BD172331D6968D3D184B3B6F *)__this->get_address_of_enumerator_6();
		bool L_15;
		L_15 = Enumerator_MoveNext_mC755E973A650FFFB74A2F3E332EC10C3F035F15D((Enumerator_tD1ECE103520161C1BD172331D6968D3D184B3B6F *)(Enumerator_tD1ECE103520161C1BD172331D6968D3D184B3B6F *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Object>::Dispose() */, (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.InternedString,System.Object>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_m1BE2A93162F747EC074416349C8D1B8BDE1F9F69_gshared (WhereSelectListIterator_2_t1C93CE1377887193667292BEED953C31167E2F75 * __this, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 * L_1 = (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *, RuntimeObject*, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,UnityEngine.InputSystem.Utilities.InternedString>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_m3717A4DEF6EC7F4B05B7B00729558162F6D2B9C7_gshared (WhereSelectListIterator_2_t97CD9F77D3626E616F5290B44E8448C47A64E44C * __this, List_1_tCBF5EB99F2E538DCB47A8C835DD4B9C024879393 * ___source0, Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 * ___predicate1, Func_2_tEA2235809AB0E1AB8425745179691D76B519B0C5 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		((  void (*) (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_tCBF5EB99F2E538DCB47A8C835DD4B9C024879393 * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_tEA2235809AB0E1AB8425745179691D76B519B0C5 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,UnityEngine.InputSystem.Utilities.InternedString>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 * WhereSelectListIterator_2_Clone_mB8DF2EFF26EBDDE79FD083CBB686CE6A45D0CBFC_gshared (WhereSelectListIterator_2_t97CD9F77D3626E616F5290B44E8448C47A64E44C * __this, const RuntimeMethod* method)
{
	{
		List_1_tCBF5EB99F2E538DCB47A8C835DD4B9C024879393 * L_0 = (List_1_tCBF5EB99F2E538DCB47A8C835DD4B9C024879393 *)__this->get_source_3();
		Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 * L_1 = (Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 *)__this->get_predicate_4();
		Func_2_tEA2235809AB0E1AB8425745179691D76B519B0C5 * L_2 = (Func_2_tEA2235809AB0E1AB8425745179691D76B519B0C5 *)__this->get_selector_5();
		WhereSelectListIterator_2_t97CD9F77D3626E616F5290B44E8448C47A64E44C * L_3 = (WhereSelectListIterator_2_t97CD9F77D3626E616F5290B44E8448C47A64E44C *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_t97CD9F77D3626E616F5290B44E8448C47A64E44C *, List_1_tCBF5EB99F2E538DCB47A8C835DD4B9C024879393 *, Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 *, Func_2_tEA2235809AB0E1AB8425745179691D76B519B0C5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_tCBF5EB99F2E538DCB47A8C835DD4B9C024879393 *)L_0, (Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 *)L_1, (Func_2_tEA2235809AB0E1AB8425745179691D76B519B0C5 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,UnityEngine.InputSystem.Utilities.InternedString>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_m4777D4A5C41C8DE4E39F25E670BB727771649BBC_gshared (WhereSelectListIterator_2_t97CD9F77D3626E616F5290B44E8448C47A64E44C * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_tCBF5EB99F2E538DCB47A8C835DD4B9C024879393 * L_3 = (List_1_tCBF5EB99F2E538DCB47A8C835DD4B9C024879393 *)__this->get_source_3();
		NullCheck((List_1_tCBF5EB99F2E538DCB47A8C835DD4B9C024879393 *)L_3);
		Enumerator_tA2E0F83E15EA46A2B4B08AFB750700C6E1387FC0  L_4;
		L_4 = ((  Enumerator_tA2E0F83E15EA46A2B4B08AFB750700C6E1387FC0  (*) (List_1_tCBF5EB99F2E538DCB47A8C835DD4B9C024879393 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_tCBF5EB99F2E538DCB47A8C835DD4B9C024879393 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_tA2E0F83E15EA46A2B4B08AFB750700C6E1387FC0 * L_5 = (Enumerator_tA2E0F83E15EA46A2B4B08AFB750700C6E1387FC0 *)__this->get_address_of_enumerator_6();
		NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA  L_6;
		L_6 = Enumerator_get_Current_m0D244550779CCA026ED61FE7767F255D97C413CD_inline((Enumerator_tA2E0F83E15EA46A2B4B08AFB750700C6E1387FC0 *)(Enumerator_tA2E0F83E15EA46A2B4B08AFB750700C6E1387FC0 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA )L_6;
		Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 * L_7 = (Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 * L_8 = (Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 *)__this->get_predicate_4();
		NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA  L_9 = V_1;
		NullCheck((Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 *, NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 *)L_8, (NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_tEA2235809AB0E1AB8425745179691D76B519B0C5 * L_11 = (Func_2_tEA2235809AB0E1AB8425745179691D76B519B0C5 *)__this->get_selector_5();
		NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA  L_12 = V_1;
		NullCheck((Func_2_tEA2235809AB0E1AB8425745179691D76B519B0C5 *)L_11);
		InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  L_13;
		L_13 = ((  InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  (*) (Func_2_tEA2235809AB0E1AB8425745179691D76B519B0C5 *, NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_tEA2235809AB0E1AB8425745179691D76B519B0C5 *)L_11, (NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_tA2E0F83E15EA46A2B4B08AFB750700C6E1387FC0 * L_14 = (Enumerator_tA2E0F83E15EA46A2B4B08AFB750700C6E1387FC0 *)__this->get_address_of_enumerator_6();
		bool L_15;
		L_15 = Enumerator_MoveNext_mB1DCA082AFD65B868138F31F1604376EB2580343((Enumerator_tA2E0F83E15EA46A2B4B08AFB750700C6E1387FC0 *)(Enumerator_tA2E0F83E15EA46A2B4B08AFB750700C6E1387FC0 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<UnityEngine.InputSystem.Utilities.InternedString>::Dispose() */, (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,UnityEngine.InputSystem.Utilities.InternedString>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_m0E89D899DEF511235BF26CB479CD86A40AD5FC99_gshared (WhereSelectListIterator_2_t97CD9F77D3626E616F5290B44E8448C47A64E44C * __this, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B * L_1 = (WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B *, RuntimeObject*, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,System.Object>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_m7F601E76107640C8DFCCCA8B93DE67F2EF3F360B_gshared (WhereSelectListIterator_2_tDFC8E5CA0DE13C88A58AD7BCD17F1FC274A3F95D * __this, List_1_tCBF5EB99F2E538DCB47A8C835DD4B9C024879393 * ___source0, Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 * ___predicate1, Func_2_tF7304610FB007335AC870B51C8FE2092628DC0DB * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_tCBF5EB99F2E538DCB47A8C835DD4B9C024879393 * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_tF7304610FB007335AC870B51C8FE2092628DC0DB * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,System.Object>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 * WhereSelectListIterator_2_Clone_mE847E0EA6B445AFFBAC2B4EF8AAF764E9E6DBF2D_gshared (WhereSelectListIterator_2_tDFC8E5CA0DE13C88A58AD7BCD17F1FC274A3F95D * __this, const RuntimeMethod* method)
{
	{
		List_1_tCBF5EB99F2E538DCB47A8C835DD4B9C024879393 * L_0 = (List_1_tCBF5EB99F2E538DCB47A8C835DD4B9C024879393 *)__this->get_source_3();
		Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 * L_1 = (Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 *)__this->get_predicate_4();
		Func_2_tF7304610FB007335AC870B51C8FE2092628DC0DB * L_2 = (Func_2_tF7304610FB007335AC870B51C8FE2092628DC0DB *)__this->get_selector_5();
		WhereSelectListIterator_2_tDFC8E5CA0DE13C88A58AD7BCD17F1FC274A3F95D * L_3 = (WhereSelectListIterator_2_tDFC8E5CA0DE13C88A58AD7BCD17F1FC274A3F95D *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_tDFC8E5CA0DE13C88A58AD7BCD17F1FC274A3F95D *, List_1_tCBF5EB99F2E538DCB47A8C835DD4B9C024879393 *, Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 *, Func_2_tF7304610FB007335AC870B51C8FE2092628DC0DB *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_tCBF5EB99F2E538DCB47A8C835DD4B9C024879393 *)L_0, (Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 *)L_1, (Func_2_tF7304610FB007335AC870B51C8FE2092628DC0DB *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_mE0B90B840655F15B6B0C666C9499E597EF53B2B8_gshared (WhereSelectListIterator_2_tDFC8E5CA0DE13C88A58AD7BCD17F1FC274A3F95D * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_tCBF5EB99F2E538DCB47A8C835DD4B9C024879393 * L_3 = (List_1_tCBF5EB99F2E538DCB47A8C835DD4B9C024879393 *)__this->get_source_3();
		NullCheck((List_1_tCBF5EB99F2E538DCB47A8C835DD4B9C024879393 *)L_3);
		Enumerator_tA2E0F83E15EA46A2B4B08AFB750700C6E1387FC0  L_4;
		L_4 = ((  Enumerator_tA2E0F83E15EA46A2B4B08AFB750700C6E1387FC0  (*) (List_1_tCBF5EB99F2E538DCB47A8C835DD4B9C024879393 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_tCBF5EB99F2E538DCB47A8C835DD4B9C024879393 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_tA2E0F83E15EA46A2B4B08AFB750700C6E1387FC0 * L_5 = (Enumerator_tA2E0F83E15EA46A2B4B08AFB750700C6E1387FC0 *)__this->get_address_of_enumerator_6();
		NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA  L_6;
		L_6 = Enumerator_get_Current_m0D244550779CCA026ED61FE7767F255D97C413CD_inline((Enumerator_tA2E0F83E15EA46A2B4B08AFB750700C6E1387FC0 *)(Enumerator_tA2E0F83E15EA46A2B4B08AFB750700C6E1387FC0 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA )L_6;
		Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 * L_7 = (Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 * L_8 = (Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 *)__this->get_predicate_4();
		NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA  L_9 = V_1;
		NullCheck((Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 *, NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 *)L_8, (NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_tF7304610FB007335AC870B51C8FE2092628DC0DB * L_11 = (Func_2_tF7304610FB007335AC870B51C8FE2092628DC0DB *)__this->get_selector_5();
		NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA  L_12 = V_1;
		NullCheck((Func_2_tF7304610FB007335AC870B51C8FE2092628DC0DB *)L_11);
		RuntimeObject * L_13;
		L_13 = ((  RuntimeObject * (*) (Func_2_tF7304610FB007335AC870B51C8FE2092628DC0DB *, NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_tF7304610FB007335AC870B51C8FE2092628DC0DB *)L_11, (NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_tA2E0F83E15EA46A2B4B08AFB750700C6E1387FC0 * L_14 = (Enumerator_tA2E0F83E15EA46A2B4B08AFB750700C6E1387FC0 *)__this->get_address_of_enumerator_6();
		bool L_15;
		L_15 = Enumerator_MoveNext_mB1DCA082AFD65B868138F31F1604376EB2580343((Enumerator_tA2E0F83E15EA46A2B4B08AFB750700C6E1387FC0 *)(Enumerator_tA2E0F83E15EA46A2B4B08AFB750700C6E1387FC0 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Object>::Dispose() */, (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,System.Object>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_m33B7341FDDBC50BCCBBD378067BD53A3F8283114_gshared (WhereSelectListIterator_2_tDFC8E5CA0DE13C88A58AD7BCD17F1FC274A3F95D * __this, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 * L_1 = (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *, RuntimeObject*, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,UnityEngine.InputSystem.Utilities.InternedString>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_m6668E1D45B4A1D57E567596F7B00F2D8D4F8EF53_gshared (WhereSelectListIterator_2_t84216899116C33A67088960F25CA2D6E1DDC786D * __this, List_1_tDC9592305A8D24F0DBC4A23F1719292FCB2887C1 * ___source0, Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 * ___predicate1, Func_2_tA21678F1D9F9F4617D645755285D28EDE5F9AE44 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		((  void (*) (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_tDC9592305A8D24F0DBC4A23F1719292FCB2887C1 * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_tA21678F1D9F9F4617D645755285D28EDE5F9AE44 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,UnityEngine.InputSystem.Utilities.InternedString>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 * WhereSelectListIterator_2_Clone_m46197F59C2FE65292667E3321E2EB51F9BAAACEF_gshared (WhereSelectListIterator_2_t84216899116C33A67088960F25CA2D6E1DDC786D * __this, const RuntimeMethod* method)
{
	{
		List_1_tDC9592305A8D24F0DBC4A23F1719292FCB2887C1 * L_0 = (List_1_tDC9592305A8D24F0DBC4A23F1719292FCB2887C1 *)__this->get_source_3();
		Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 * L_1 = (Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 *)__this->get_predicate_4();
		Func_2_tA21678F1D9F9F4617D645755285D28EDE5F9AE44 * L_2 = (Func_2_tA21678F1D9F9F4617D645755285D28EDE5F9AE44 *)__this->get_selector_5();
		WhereSelectListIterator_2_t84216899116C33A67088960F25CA2D6E1DDC786D * L_3 = (WhereSelectListIterator_2_t84216899116C33A67088960F25CA2D6E1DDC786D *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_t84216899116C33A67088960F25CA2D6E1DDC786D *, List_1_tDC9592305A8D24F0DBC4A23F1719292FCB2887C1 *, Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 *, Func_2_tA21678F1D9F9F4617D645755285D28EDE5F9AE44 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_tDC9592305A8D24F0DBC4A23F1719292FCB2887C1 *)L_0, (Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 *)L_1, (Func_2_tA21678F1D9F9F4617D645755285D28EDE5F9AE44 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,UnityEngine.InputSystem.Utilities.InternedString>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_m134796012884D59C6F5206A56BFD1CE69A5D87E9_gshared (WhereSelectListIterator_2_t84216899116C33A67088960F25CA2D6E1DDC786D * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_tDC9592305A8D24F0DBC4A23F1719292FCB2887C1 * L_3 = (List_1_tDC9592305A8D24F0DBC4A23F1719292FCB2887C1 *)__this->get_source_3();
		NullCheck((List_1_tDC9592305A8D24F0DBC4A23F1719292FCB2887C1 *)L_3);
		Enumerator_tDEE846E92D2B22781FB09541CB1600E5398D3A20  L_4;
		L_4 = ((  Enumerator_tDEE846E92D2B22781FB09541CB1600E5398D3A20  (*) (List_1_tDC9592305A8D24F0DBC4A23F1719292FCB2887C1 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_tDC9592305A8D24F0DBC4A23F1719292FCB2887C1 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_tDEE846E92D2B22781FB09541CB1600E5398D3A20 * L_5 = (Enumerator_tDEE846E92D2B22781FB09541CB1600E5398D3A20 *)__this->get_address_of_enumerator_6();
		NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077  L_6;
		L_6 = Enumerator_get_Current_m310A8D3883A91C67B235D82B07D2B234536E2D39_inline((Enumerator_tDEE846E92D2B22781FB09541CB1600E5398D3A20 *)(Enumerator_tDEE846E92D2B22781FB09541CB1600E5398D3A20 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077 )L_6;
		Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 * L_7 = (Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 * L_8 = (Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 *)__this->get_predicate_4();
		NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077  L_9 = V_1;
		NullCheck((Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 *, NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 *)L_8, (NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077 )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_tA21678F1D9F9F4617D645755285D28EDE5F9AE44 * L_11 = (Func_2_tA21678F1D9F9F4617D645755285D28EDE5F9AE44 *)__this->get_selector_5();
		NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077  L_12 = V_1;
		NullCheck((Func_2_tA21678F1D9F9F4617D645755285D28EDE5F9AE44 *)L_11);
		InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  L_13;
		L_13 = ((  InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  (*) (Func_2_tA21678F1D9F9F4617D645755285D28EDE5F9AE44 *, NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_tA21678F1D9F9F4617D645755285D28EDE5F9AE44 *)L_11, (NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077 )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_tDEE846E92D2B22781FB09541CB1600E5398D3A20 * L_14 = (Enumerator_tDEE846E92D2B22781FB09541CB1600E5398D3A20 *)__this->get_address_of_enumerator_6();
		bool L_15;
		L_15 = Enumerator_MoveNext_m90AD9DC23572A512C0626351F44778A476DE51B0((Enumerator_tDEE846E92D2B22781FB09541CB1600E5398D3A20 *)(Enumerator_tDEE846E92D2B22781FB09541CB1600E5398D3A20 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<UnityEngine.InputSystem.Utilities.InternedString>::Dispose() */, (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,UnityEngine.InputSystem.Utilities.InternedString>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_m04EA6CFC5E32CDB7DD3B0D2D91013089BBBA5B11_gshared (WhereSelectListIterator_2_t84216899116C33A67088960F25CA2D6E1DDC786D * __this, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B * L_1 = (WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B *, RuntimeObject*, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,System.Object>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_mE11F9A42829545BEB57EE51DE13072A2CAAC1635_gshared (WhereSelectListIterator_2_tA4D5B2B639DCB29B4C84236B3E2651F17B522430 * __this, List_1_tDC9592305A8D24F0DBC4A23F1719292FCB2887C1 * ___source0, Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 * ___predicate1, Func_2_t5B101C0B26ADAA3B1A77BC50D51CC42364E5409E * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_tDC9592305A8D24F0DBC4A23F1719292FCB2887C1 * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t5B101C0B26ADAA3B1A77BC50D51CC42364E5409E * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,System.Object>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 * WhereSelectListIterator_2_Clone_m6D48ED52304D5758B90195E45BC1176BC96EE7DA_gshared (WhereSelectListIterator_2_tA4D5B2B639DCB29B4C84236B3E2651F17B522430 * __this, const RuntimeMethod* method)
{
	{
		List_1_tDC9592305A8D24F0DBC4A23F1719292FCB2887C1 * L_0 = (List_1_tDC9592305A8D24F0DBC4A23F1719292FCB2887C1 *)__this->get_source_3();
		Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 * L_1 = (Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 *)__this->get_predicate_4();
		Func_2_t5B101C0B26ADAA3B1A77BC50D51CC42364E5409E * L_2 = (Func_2_t5B101C0B26ADAA3B1A77BC50D51CC42364E5409E *)__this->get_selector_5();
		WhereSelectListIterator_2_tA4D5B2B639DCB29B4C84236B3E2651F17B522430 * L_3 = (WhereSelectListIterator_2_tA4D5B2B639DCB29B4C84236B3E2651F17B522430 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_tA4D5B2B639DCB29B4C84236B3E2651F17B522430 *, List_1_tDC9592305A8D24F0DBC4A23F1719292FCB2887C1 *, Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 *, Func_2_t5B101C0B26ADAA3B1A77BC50D51CC42364E5409E *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_tDC9592305A8D24F0DBC4A23F1719292FCB2887C1 *)L_0, (Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 *)L_1, (Func_2_t5B101C0B26ADAA3B1A77BC50D51CC42364E5409E *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_m77A19246B97C7498CBBCBFE79DF6D9F38E285CC8_gshared (WhereSelectListIterator_2_tA4D5B2B639DCB29B4C84236B3E2651F17B522430 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_tDC9592305A8D24F0DBC4A23F1719292FCB2887C1 * L_3 = (List_1_tDC9592305A8D24F0DBC4A23F1719292FCB2887C1 *)__this->get_source_3();
		NullCheck((List_1_tDC9592305A8D24F0DBC4A23F1719292FCB2887C1 *)L_3);
		Enumerator_tDEE846E92D2B22781FB09541CB1600E5398D3A20  L_4;
		L_4 = ((  Enumerator_tDEE846E92D2B22781FB09541CB1600E5398D3A20  (*) (List_1_tDC9592305A8D24F0DBC4A23F1719292FCB2887C1 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_tDC9592305A8D24F0DBC4A23F1719292FCB2887C1 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_tDEE846E92D2B22781FB09541CB1600E5398D3A20 * L_5 = (Enumerator_tDEE846E92D2B22781FB09541CB1600E5398D3A20 *)__this->get_address_of_enumerator_6();
		NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077  L_6;
		L_6 = Enumerator_get_Current_m310A8D3883A91C67B235D82B07D2B234536E2D39_inline((Enumerator_tDEE846E92D2B22781FB09541CB1600E5398D3A20 *)(Enumerator_tDEE846E92D2B22781FB09541CB1600E5398D3A20 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077 )L_6;
		Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 * L_7 = (Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 * L_8 = (Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 *)__this->get_predicate_4();
		NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077  L_9 = V_1;
		NullCheck((Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 *, NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 *)L_8, (NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077 )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t5B101C0B26ADAA3B1A77BC50D51CC42364E5409E * L_11 = (Func_2_t5B101C0B26ADAA3B1A77BC50D51CC42364E5409E *)__this->get_selector_5();
		NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077  L_12 = V_1;
		NullCheck((Func_2_t5B101C0B26ADAA3B1A77BC50D51CC42364E5409E *)L_11);
		RuntimeObject * L_13;
		L_13 = ((  RuntimeObject * (*) (Func_2_t5B101C0B26ADAA3B1A77BC50D51CC42364E5409E *, NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t5B101C0B26ADAA3B1A77BC50D51CC42364E5409E *)L_11, (NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077 )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_tDEE846E92D2B22781FB09541CB1600E5398D3A20 * L_14 = (Enumerator_tDEE846E92D2B22781FB09541CB1600E5398D3A20 *)__this->get_address_of_enumerator_6();
		bool L_15;
		L_15 = Enumerator_MoveNext_m90AD9DC23572A512C0626351F44778A476DE51B0((Enumerator_tDEE846E92D2B22781FB09541CB1600E5398D3A20 *)(Enumerator_tDEE846E92D2B22781FB09541CB1600E5398D3A20 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Object>::Dispose() */, (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,System.Object>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_m51B5B699FC184A78FDB9FB4949A01460B2ED5DAC_gshared (WhereSelectListIterator_2_tA4D5B2B639DCB29B4C84236B3E2651F17B522430 * __this, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 * L_1 = (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *, RuntimeObject*, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectListIterator`2<System.Object,UnityEngine.InputSystem.Utilities.InternedString>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_m53D3B0BD253F18C36AD32A08CD60E47EC17F3D44_gshared (WhereSelectListIterator_2_t4F7AA31B4289096C5D625263B8BBDA051331BE94 * __this, List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * ___source0, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate1, Func_2_t964229B6ADD2B008268547ED2BC3C090AF218065 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		((  void (*) (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t964229B6ADD2B008268547ED2BC3C090AF218065 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<System.Object,UnityEngine.InputSystem.Utilities.InternedString>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 * WhereSelectListIterator_2_Clone_m337F75FE943B119BF54BC61E5A81E3D9FA1DE7B3_gshared (WhereSelectListIterator_2_t4F7AA31B4289096C5D625263B8BBDA051331BE94 * __this, const RuntimeMethod* method)
{
	{
		List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * L_0 = (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *)__this->get_source_3();
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_1 = (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)__this->get_predicate_4();
		Func_2_t964229B6ADD2B008268547ED2BC3C090AF218065 * L_2 = (Func_2_t964229B6ADD2B008268547ED2BC3C090AF218065 *)__this->get_selector_5();
		WhereSelectListIterator_2_t4F7AA31B4289096C5D625263B8BBDA051331BE94 * L_3 = (WhereSelectListIterator_2_t4F7AA31B4289096C5D625263B8BBDA051331BE94 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_t4F7AA31B4289096C5D625263B8BBDA051331BE94 *, List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, Func_2_t964229B6ADD2B008268547ED2BC3C090AF218065 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *)L_0, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_1, (Func_2_t964229B6ADD2B008268547ED2BC3C090AF218065 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2<System.Object,UnityEngine.InputSystem.Utilities.InternedString>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_mD6A4B29BF27C22CDE2D2B2CDA66FFD1FDB6E9A3F_gshared (WhereSelectListIterator_2_t4F7AA31B4289096C5D625263B8BBDA051331BE94 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	RuntimeObject * V_1 = NULL;
	{
		int32_t L_0 = (int32_t)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * L_3 = (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *)__this->get_source_3();
		NullCheck((List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *)L_3);
		Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6  L_4;
		L_4 = ((  Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6  (*) (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * L_5 = (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)__this->get_address_of_enumerator_6();
		RuntimeObject * L_6;
		L_6 = Enumerator_get_Current_m9C4EBBD2108B51885E750F927D7936290C8E20EE_inline((Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (RuntimeObject *)L_6;
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_7 = (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_8 = (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)__this->get_predicate_4();
		RuntimeObject * L_9 = V_1;
		NullCheck((Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_8, (RuntimeObject *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t964229B6ADD2B008268547ED2BC3C090AF218065 * L_11 = (Func_2_t964229B6ADD2B008268547ED2BC3C090AF218065 *)__this->get_selector_5();
		RuntimeObject * L_12 = V_1;
		NullCheck((Func_2_t964229B6ADD2B008268547ED2BC3C090AF218065 *)L_11);
		InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  L_13;
		L_13 = ((  InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  (*) (Func_2_t964229B6ADD2B008268547ED2BC3C090AF218065 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t964229B6ADD2B008268547ED2BC3C090AF218065 *)L_11, (RuntimeObject *)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * L_14 = (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)__this->get_address_of_enumerator_6();
		bool L_15;
		L_15 = Enumerator_MoveNext_m2E56233762839CE55C67E00AC8DD3D4D3F6C0DF0((Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<UnityEngine.InputSystem.Utilities.InternedString>::Dispose() */, (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<System.Object,UnityEngine.InputSystem.Utilities.InternedString>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_mE5F153A4B870B03A6D272E9E2DEBB71BBDB1DDE9_gshared (WhereSelectListIterator_2_t4F7AA31B4289096C5D625263B8BBDA051331BE94 * __this, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B * L_1 = (WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B *, RuntimeObject*, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectListIterator`2<System.Object,System.Object>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_mCF313A191371C8CCC2E79D89A3BF21714EFDB20E_gshared (WhereSelectListIterator_2_t85B78DFF0573BC95A62C79D6088FA39DFEBE1AF2 * __this, List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * ___source0, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate1, Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<System.Object,System.Object>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 * WhereSelectListIterator_2_Clone_m667BCD94E83BB3A02AF2D66E07B089FA86971342_gshared (WhereSelectListIterator_2_t85B78DFF0573BC95A62C79D6088FA39DFEBE1AF2 * __this, const RuntimeMethod* method)
{
	{
		List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * L_0 = (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *)__this->get_source_3();
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_1 = (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)__this->get_predicate_4();
		Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 * L_2 = (Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 *)__this->get_selector_5();
		WhereSelectListIterator_2_t85B78DFF0573BC95A62C79D6088FA39DFEBE1AF2 * L_3 = (WhereSelectListIterator_2_t85B78DFF0573BC95A62C79D6088FA39DFEBE1AF2 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_t85B78DFF0573BC95A62C79D6088FA39DFEBE1AF2 *, List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *)L_0, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_1, (Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2<System.Object,System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_mEE0E8B173345B059100E0736D106FFAE0C2D29CA_gshared (WhereSelectListIterator_2_t85B78DFF0573BC95A62C79D6088FA39DFEBE1AF2 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	RuntimeObject * V_1 = NULL;
	{
		int32_t L_0 = (int32_t)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * L_3 = (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *)__this->get_source_3();
		NullCheck((List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *)L_3);
		Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6  L_4;
		L_4 = ((  Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6  (*) (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * L_5 = (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)__this->get_address_of_enumerator_6();
		RuntimeObject * L_6;
		L_6 = Enumerator_get_Current_m9C4EBBD2108B51885E750F927D7936290C8E20EE_inline((Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (RuntimeObject *)L_6;
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_7 = (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_8 = (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)__this->get_predicate_4();
		RuntimeObject * L_9 = V_1;
		NullCheck((Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_8, (RuntimeObject *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 * L_11 = (Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 *)__this->get_selector_5();
		RuntimeObject * L_12 = V_1;
		NullCheck((Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 *)L_11);
		RuntimeObject * L_13;
		L_13 = ((  RuntimeObject * (*) (Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 *)L_11, (RuntimeObject *)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * L_14 = (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)__this->get_address_of_enumerator_6();
		bool L_15;
		L_15 = Enumerator_MoveNext_m2E56233762839CE55C67E00AC8DD3D4D3F6C0DF0((Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Object>::Dispose() */, (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<System.Object,System.Object>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_mAC87184664F73DD7F3EC4AB4CE2BDE71BE76249D_gshared (WhereSelectListIterator_2_t85B78DFF0573BC95A62C79D6088FA39DFEBE1AF2 * __this, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 * L_1 = (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *, RuntimeObject*, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.Pose,UnityEngine.Quaternion>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_mCA2789E32566824C2033ABB0567D8BB7B1D1CFF7_gshared (WhereSelectListIterator_2_tA4AFE0F933BE35E59757614CBEDAA04AEBC6AD3D * __this, List_1_tDB586987B7A2D2B13A903004C86E3C3FBD061C1C * ___source0, Func_2_t288BC2D8C12E52B8F2E7F675CFB740C44E7E813F * ___predicate1, Func_2_t6170D516A1F6F9B6BE1BDC5379D0A5CC320AF0F8 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t4BD4EE08E1E7E81949C3754EC2969154580F4C65 *)__this);
		((  void (*) (Iterator_1_t4BD4EE08E1E7E81949C3754EC2969154580F4C65 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t4BD4EE08E1E7E81949C3754EC2969154580F4C65 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_tDB586987B7A2D2B13A903004C86E3C3FBD061C1C * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t288BC2D8C12E52B8F2E7F675CFB740C44E7E813F * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t6170D516A1F6F9B6BE1BDC5379D0A5CC320AF0F8 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.Pose,UnityEngine.Quaternion>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t4BD4EE08E1E7E81949C3754EC2969154580F4C65 * WhereSelectListIterator_2_Clone_mA62FF15FF8FA0738A92A62144F6232E8A520D69C_gshared (WhereSelectListIterator_2_tA4AFE0F933BE35E59757614CBEDAA04AEBC6AD3D * __this, const RuntimeMethod* method)
{
	{
		List_1_tDB586987B7A2D2B13A903004C86E3C3FBD061C1C * L_0 = (List_1_tDB586987B7A2D2B13A903004C86E3C3FBD061C1C *)__this->get_source_3();
		Func_2_t288BC2D8C12E52B8F2E7F675CFB740C44E7E813F * L_1 = (Func_2_t288BC2D8C12E52B8F2E7F675CFB740C44E7E813F *)__this->get_predicate_4();
		Func_2_t6170D516A1F6F9B6BE1BDC5379D0A5CC320AF0F8 * L_2 = (Func_2_t6170D516A1F6F9B6BE1BDC5379D0A5CC320AF0F8 *)__this->get_selector_5();
		WhereSelectListIterator_2_tA4AFE0F933BE35E59757614CBEDAA04AEBC6AD3D * L_3 = (WhereSelectListIterator_2_tA4AFE0F933BE35E59757614CBEDAA04AEBC6AD3D *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_tA4AFE0F933BE35E59757614CBEDAA04AEBC6AD3D *, List_1_tDB586987B7A2D2B13A903004C86E3C3FBD061C1C *, Func_2_t288BC2D8C12E52B8F2E7F675CFB740C44E7E813F *, Func_2_t6170D516A1F6F9B6BE1BDC5379D0A5CC320AF0F8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_tDB586987B7A2D2B13A903004C86E3C3FBD061C1C *)L_0, (Func_2_t288BC2D8C12E52B8F2E7F675CFB740C44E7E813F *)L_1, (Func_2_t6170D516A1F6F9B6BE1BDC5379D0A5CC320AF0F8 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t4BD4EE08E1E7E81949C3754EC2969154580F4C65 *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.Pose,UnityEngine.Quaternion>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_mDF3F7587A8D3FDDA83759D3BC1EE4679B0AA0D4D_gshared (WhereSelectListIterator_2_tA4AFE0F933BE35E59757614CBEDAA04AEBC6AD3D * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	Pose_t9F30358E65733E60A1DC8682FDB7104F40C9434A  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t4BD4EE08E1E7E81949C3754EC2969154580F4C65 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_tDB586987B7A2D2B13A903004C86E3C3FBD061C1C * L_3 = (List_1_tDB586987B7A2D2B13A903004C86E3C3FBD061C1C *)__this->get_source_3();
		NullCheck((List_1_tDB586987B7A2D2B13A903004C86E3C3FBD061C1C *)L_3);
		Enumerator_tA7E1DE1C5B2833EE935A19CAF705B2C053EBC9AB  L_4;
		L_4 = ((  Enumerator_tA7E1DE1C5B2833EE935A19CAF705B2C053EBC9AB  (*) (List_1_tDB586987B7A2D2B13A903004C86E3C3FBD061C1C *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_tDB586987B7A2D2B13A903004C86E3C3FBD061C1C *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_t4BD4EE08E1E7E81949C3754EC2969154580F4C65 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_tA7E1DE1C5B2833EE935A19CAF705B2C053EBC9AB * L_5 = (Enumerator_tA7E1DE1C5B2833EE935A19CAF705B2C053EBC9AB *)__this->get_address_of_enumerator_6();
		Pose_t9F30358E65733E60A1DC8682FDB7104F40C9434A  L_6;
		L_6 = Enumerator_get_Current_mFDB2FBE724A02C5CAFAE108E12A75676EFE43A5F_inline((Enumerator_tA7E1DE1C5B2833EE935A19CAF705B2C053EBC9AB *)(Enumerator_tA7E1DE1C5B2833EE935A19CAF705B2C053EBC9AB *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (Pose_t9F30358E65733E60A1DC8682FDB7104F40C9434A )L_6;
		Func_2_t288BC2D8C12E52B8F2E7F675CFB740C44E7E813F * L_7 = (Func_2_t288BC2D8C12E52B8F2E7F675CFB740C44E7E813F *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t288BC2D8C12E52B8F2E7F675CFB740C44E7E813F * L_8 = (Func_2_t288BC2D8C12E52B8F2E7F675CFB740C44E7E813F *)__this->get_predicate_4();
		Pose_t9F30358E65733E60A1DC8682FDB7104F40C9434A  L_9 = V_1;
		NullCheck((Func_2_t288BC2D8C12E52B8F2E7F675CFB740C44E7E813F *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t288BC2D8C12E52B8F2E7F675CFB740C44E7E813F *, Pose_t9F30358E65733E60A1DC8682FDB7104F40C9434A , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t288BC2D8C12E52B8F2E7F675CFB740C44E7E813F *)L_8, (Pose_t9F30358E65733E60A1DC8682FDB7104F40C9434A )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t6170D516A1F6F9B6BE1BDC5379D0A5CC320AF0F8 * L_11 = (Func_2_t6170D516A1F6F9B6BE1BDC5379D0A5CC320AF0F8 *)__this->get_selector_5();
		Pose_t9F30358E65733E60A1DC8682FDB7104F40C9434A  L_12 = V_1;
		NullCheck((Func_2_t6170D516A1F6F9B6BE1BDC5379D0A5CC320AF0F8 *)L_11);
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_13;
		L_13 = ((  Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  (*) (Func_2_t6170D516A1F6F9B6BE1BDC5379D0A5CC320AF0F8 *, Pose_t9F30358E65733E60A1DC8682FDB7104F40C9434A , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t6170D516A1F6F9B6BE1BDC5379D0A5CC320AF0F8 *)L_11, (Pose_t9F30358E65733E60A1DC8682FDB7104F40C9434A )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_t4BD4EE08E1E7E81949C3754EC2969154580F4C65 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_tA7E1DE1C5B2833EE935A19CAF705B2C053EBC9AB * L_14 = (Enumerator_tA7E1DE1C5B2833EE935A19CAF705B2C053EBC9AB *)__this->get_address_of_enumerator_6();
		bool L_15;
		L_15 = Enumerator_MoveNext_mF9007138A9A75F58C71B24612243436A5852F2E8((Enumerator_tA7E1DE1C5B2833EE935A19CAF705B2C053EBC9AB *)(Enumerator_tA7E1DE1C5B2833EE935A19CAF705B2C053EBC9AB *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t4BD4EE08E1E7E81949C3754EC2969154580F4C65 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<UnityEngine.Quaternion>::Dispose() */, (Iterator_1_t4BD4EE08E1E7E81949C3754EC2969154580F4C65 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.Pose,UnityEngine.Quaternion>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_m192B3594DBDAA23DD28A42C54BC39CA8FD411CA7_gshared (WhereSelectListIterator_2_tA4AFE0F933BE35E59757614CBEDAA04AEBC6AD3D * __this, Func_2_t34B33A6CBEAEB1EF3A60338636A0DC782C33A7BE * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t34B33A6CBEAEB1EF3A60338636A0DC782C33A7BE * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t6F3A191533D782BF1D9D4D3C5E1B535EC187EA9F * L_1 = (WhereEnumerableIterator_1_t6F3A191533D782BF1D9D4D3C5E1B535EC187EA9F *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t6F3A191533D782BF1D9D4D3C5E1B535EC187EA9F *, RuntimeObject*, Func_2_t34B33A6CBEAEB1EF3A60338636A0DC782C33A7BE *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t34B33A6CBEAEB1EF3A60338636A0DC782C33A7BE *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectListIterator`2<System.Single,UnityEngine.InputSystem.Utilities.InternedString>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_m73C73AECEE45458B9E472604125295D17DD2F2B1_gshared (WhereSelectListIterator_2_t75E7B369D9A91D962C4DAFB13EE84B0CCE07D9E3 * __this, List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA * ___source0, Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * ___predicate1, Func_2_t4E06D96B2978B60DA267963F96583CE3BF82EA84 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		((  void (*) (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t4E06D96B2978B60DA267963F96583CE3BF82EA84 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<System.Single,UnityEngine.InputSystem.Utilities.InternedString>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 * WhereSelectListIterator_2_Clone_m5E236A9A94820E9A52EEFBCF38F022430BAD61DE_gshared (WhereSelectListIterator_2_t75E7B369D9A91D962C4DAFB13EE84B0CCE07D9E3 * __this, const RuntimeMethod* method)
{
	{
		List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA * L_0 = (List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA *)__this->get_source_3();
		Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * L_1 = (Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *)__this->get_predicate_4();
		Func_2_t4E06D96B2978B60DA267963F96583CE3BF82EA84 * L_2 = (Func_2_t4E06D96B2978B60DA267963F96583CE3BF82EA84 *)__this->get_selector_5();
		WhereSelectListIterator_2_t75E7B369D9A91D962C4DAFB13EE84B0CCE07D9E3 * L_3 = (WhereSelectListIterator_2_t75E7B369D9A91D962C4DAFB13EE84B0CCE07D9E3 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_t75E7B369D9A91D962C4DAFB13EE84B0CCE07D9E3 *, List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA *, Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *, Func_2_t4E06D96B2978B60DA267963F96583CE3BF82EA84 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA *)L_0, (Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *)L_1, (Func_2_t4E06D96B2978B60DA267963F96583CE3BF82EA84 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2<System.Single,UnityEngine.InputSystem.Utilities.InternedString>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_mFC62F55FE28BEEA23550B996C2A2FEF2E4AE886E_gshared (WhereSelectListIterator_2_t75E7B369D9A91D962C4DAFB13EE84B0CCE07D9E3 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	float V_1 = 0.0f;
	{
		int32_t L_0 = (int32_t)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA * L_3 = (List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA *)__this->get_source_3();
		NullCheck((List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA *)L_3);
		Enumerator_tC1FD01C8BB5327D442D71FD022B4338ACD701783  L_4;
		L_4 = ((  Enumerator_tC1FD01C8BB5327D442D71FD022B4338ACD701783  (*) (List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_tC1FD01C8BB5327D442D71FD022B4338ACD701783 * L_5 = (Enumerator_tC1FD01C8BB5327D442D71FD022B4338ACD701783 *)__this->get_address_of_enumerator_6();
		float L_6;
		L_6 = Enumerator_get_Current_m1DC0B40110173B7E2D13319164F7657C3BE3536D_inline((Enumerator_tC1FD01C8BB5327D442D71FD022B4338ACD701783 *)(Enumerator_tC1FD01C8BB5327D442D71FD022B4338ACD701783 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (float)L_6;
		Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * L_7 = (Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * L_8 = (Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *)__this->get_predicate_4();
		float L_9 = V_1;
		NullCheck((Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *, float, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *)L_8, (float)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t4E06D96B2978B60DA267963F96583CE3BF82EA84 * L_11 = (Func_2_t4E06D96B2978B60DA267963F96583CE3BF82EA84 *)__this->get_selector_5();
		float L_12 = V_1;
		NullCheck((Func_2_t4E06D96B2978B60DA267963F96583CE3BF82EA84 *)L_11);
		InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  L_13;
		L_13 = ((  InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  (*) (Func_2_t4E06D96B2978B60DA267963F96583CE3BF82EA84 *, float, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t4E06D96B2978B60DA267963F96583CE3BF82EA84 *)L_11, (float)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_tC1FD01C8BB5327D442D71FD022B4338ACD701783 * L_14 = (Enumerator_tC1FD01C8BB5327D442D71FD022B4338ACD701783 *)__this->get_address_of_enumerator_6();
		bool L_15;
		L_15 = Enumerator_MoveNext_mF6D031AEDDDEEAF750E0BFE7866FBBA9C9752C7E((Enumerator_tC1FD01C8BB5327D442D71FD022B4338ACD701783 *)(Enumerator_tC1FD01C8BB5327D442D71FD022B4338ACD701783 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<UnityEngine.InputSystem.Utilities.InternedString>::Dispose() */, (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<System.Single,UnityEngine.InputSystem.Utilities.InternedString>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_mB257DBBAC6EB73D63FA2A44EEC3BE48341CA6628_gshared (WhereSelectListIterator_2_t75E7B369D9A91D962C4DAFB13EE84B0CCE07D9E3 * __this, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B * L_1 = (WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B *, RuntimeObject*, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectListIterator`2<System.Single,System.Object>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_mE72A3E981B94D2502A4AFDA08A6E9867C64ED651_gshared (WhereSelectListIterator_2_t77C04C3B7AD0F11B52AC864B3B1635FBFC5D2259 * __this, List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA * ___source0, Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * ___predicate1, Func_2_tE11BDF71BCB314E58F1A9CFC9DAE08A5F717E2A0 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_tE11BDF71BCB314E58F1A9CFC9DAE08A5F717E2A0 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<System.Single,System.Object>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 * WhereSelectListIterator_2_Clone_m932F0C8E73F1307B320D9321DECE070701BB780C_gshared (WhereSelectListIterator_2_t77C04C3B7AD0F11B52AC864B3B1635FBFC5D2259 * __this, const RuntimeMethod* method)
{
	{
		List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA * L_0 = (List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA *)__this->get_source_3();
		Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * L_1 = (Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *)__this->get_predicate_4();
		Func_2_tE11BDF71BCB314E58F1A9CFC9DAE08A5F717E2A0 * L_2 = (Func_2_tE11BDF71BCB314E58F1A9CFC9DAE08A5F717E2A0 *)__this->get_selector_5();
		WhereSelectListIterator_2_t77C04C3B7AD0F11B52AC864B3B1635FBFC5D2259 * L_3 = (WhereSelectListIterator_2_t77C04C3B7AD0F11B52AC864B3B1635FBFC5D2259 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_t77C04C3B7AD0F11B52AC864B3B1635FBFC5D2259 *, List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA *, Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *, Func_2_tE11BDF71BCB314E58F1A9CFC9DAE08A5F717E2A0 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA *)L_0, (Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *)L_1, (Func_2_tE11BDF71BCB314E58F1A9CFC9DAE08A5F717E2A0 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2<System.Single,System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_m786886FF98DCF38B8D4705E8E73EDA2EFC9FCF46_gshared (WhereSelectListIterator_2_t77C04C3B7AD0F11B52AC864B3B1635FBFC5D2259 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	float V_1 = 0.0f;
	{
		int32_t L_0 = (int32_t)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA * L_3 = (List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA *)__this->get_source_3();
		NullCheck((List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA *)L_3);
		Enumerator_tC1FD01C8BB5327D442D71FD022B4338ACD701783  L_4;
		L_4 = ((  Enumerator_tC1FD01C8BB5327D442D71FD022B4338ACD701783  (*) (List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_tC1FD01C8BB5327D442D71FD022B4338ACD701783 * L_5 = (Enumerator_tC1FD01C8BB5327D442D71FD022B4338ACD701783 *)__this->get_address_of_enumerator_6();
		float L_6;
		L_6 = Enumerator_get_Current_m1DC0B40110173B7E2D13319164F7657C3BE3536D_inline((Enumerator_tC1FD01C8BB5327D442D71FD022B4338ACD701783 *)(Enumerator_tC1FD01C8BB5327D442D71FD022B4338ACD701783 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (float)L_6;
		Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * L_7 = (Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * L_8 = (Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *)__this->get_predicate_4();
		float L_9 = V_1;
		NullCheck((Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *, float, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *)L_8, (float)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_tE11BDF71BCB314E58F1A9CFC9DAE08A5F717E2A0 * L_11 = (Func_2_tE11BDF71BCB314E58F1A9CFC9DAE08A5F717E2A0 *)__this->get_selector_5();
		float L_12 = V_1;
		NullCheck((Func_2_tE11BDF71BCB314E58F1A9CFC9DAE08A5F717E2A0 *)L_11);
		RuntimeObject * L_13;
		L_13 = ((  RuntimeObject * (*) (Func_2_tE11BDF71BCB314E58F1A9CFC9DAE08A5F717E2A0 *, float, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_tE11BDF71BCB314E58F1A9CFC9DAE08A5F717E2A0 *)L_11, (float)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_tC1FD01C8BB5327D442D71FD022B4338ACD701783 * L_14 = (Enumerator_tC1FD01C8BB5327D442D71FD022B4338ACD701783 *)__this->get_address_of_enumerator_6();
		bool L_15;
		L_15 = Enumerator_MoveNext_mF6D031AEDDDEEAF750E0BFE7866FBBA9C9752C7E((Enumerator_tC1FD01C8BB5327D442D71FD022B4338ACD701783 *)(Enumerator_tC1FD01C8BB5327D442D71FD022B4338ACD701783 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Object>::Dispose() */, (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<System.Single,System.Object>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_m0CA9E839CED94CB32A63000C2B9318E221D5C580_gshared (WhereSelectListIterator_2_t77C04C3B7AD0F11B52AC864B3B1635FBFC5D2259 * __this, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 * L_1 = (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *, RuntimeObject*, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.Substring,UnityEngine.InputSystem.Utilities.InternedString>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_m07215C20E5E80B007CA3DCF220611E9FE6E59E8A_gshared (WhereSelectListIterator_2_t59101674E6772D817227A6D5C222AF34A4E0BBAD * __this, List_1_tEAD6E3282E028927B32F56E7892994D90D512467 * ___source0, Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 * ___predicate1, Func_2_t14F6F4D7FBF558A20782EDC48D80473E4800048E * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		((  void (*) (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_tEAD6E3282E028927B32F56E7892994D90D512467 * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t14F6F4D7FBF558A20782EDC48D80473E4800048E * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.Substring,UnityEngine.InputSystem.Utilities.InternedString>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 * WhereSelectListIterator_2_Clone_mC088640FC428434B6A233FDE2F32DD2617E4A968_gshared (WhereSelectListIterator_2_t59101674E6772D817227A6D5C222AF34A4E0BBAD * __this, const RuntimeMethod* method)
{
	{
		List_1_tEAD6E3282E028927B32F56E7892994D90D512467 * L_0 = (List_1_tEAD6E3282E028927B32F56E7892994D90D512467 *)__this->get_source_3();
		Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 * L_1 = (Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 *)__this->get_predicate_4();
		Func_2_t14F6F4D7FBF558A20782EDC48D80473E4800048E * L_2 = (Func_2_t14F6F4D7FBF558A20782EDC48D80473E4800048E *)__this->get_selector_5();
		WhereSelectListIterator_2_t59101674E6772D817227A6D5C222AF34A4E0BBAD * L_3 = (WhereSelectListIterator_2_t59101674E6772D817227A6D5C222AF34A4E0BBAD *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_t59101674E6772D817227A6D5C222AF34A4E0BBAD *, List_1_tEAD6E3282E028927B32F56E7892994D90D512467 *, Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 *, Func_2_t14F6F4D7FBF558A20782EDC48D80473E4800048E *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_tEAD6E3282E028927B32F56E7892994D90D512467 *)L_0, (Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 *)L_1, (Func_2_t14F6F4D7FBF558A20782EDC48D80473E4800048E *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.Substring,UnityEngine.InputSystem.Utilities.InternedString>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_mD5555094E5DCBC5500AB258020155C83DF223930_gshared (WhereSelectListIterator_2_t59101674E6772D817227A6D5C222AF34A4E0BBAD * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_tEAD6E3282E028927B32F56E7892994D90D512467 * L_3 = (List_1_tEAD6E3282E028927B32F56E7892994D90D512467 *)__this->get_source_3();
		NullCheck((List_1_tEAD6E3282E028927B32F56E7892994D90D512467 *)L_3);
		Enumerator_t7C514ACB3FF8E3EFB210F5F45C9F18A715BB4F80  L_4;
		L_4 = ((  Enumerator_t7C514ACB3FF8E3EFB210F5F45C9F18A715BB4F80  (*) (List_1_tEAD6E3282E028927B32F56E7892994D90D512467 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_tEAD6E3282E028927B32F56E7892994D90D512467 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_t7C514ACB3FF8E3EFB210F5F45C9F18A715BB4F80 * L_5 = (Enumerator_t7C514ACB3FF8E3EFB210F5F45C9F18A715BB4F80 *)__this->get_address_of_enumerator_6();
		Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F  L_6;
		L_6 = Enumerator_get_Current_mDAAD9332BFDBFF6FDFBCC73B8A70BF674E17FE61_inline((Enumerator_t7C514ACB3FF8E3EFB210F5F45C9F18A715BB4F80 *)(Enumerator_t7C514ACB3FF8E3EFB210F5F45C9F18A715BB4F80 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F )L_6;
		Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 * L_7 = (Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 * L_8 = (Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 *)__this->get_predicate_4();
		Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F  L_9 = V_1;
		NullCheck((Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 *, Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 *)L_8, (Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t14F6F4D7FBF558A20782EDC48D80473E4800048E * L_11 = (Func_2_t14F6F4D7FBF558A20782EDC48D80473E4800048E *)__this->get_selector_5();
		Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F  L_12 = V_1;
		NullCheck((Func_2_t14F6F4D7FBF558A20782EDC48D80473E4800048E *)L_11);
		InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  L_13;
		L_13 = ((  InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  (*) (Func_2_t14F6F4D7FBF558A20782EDC48D80473E4800048E *, Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t14F6F4D7FBF558A20782EDC48D80473E4800048E *)L_11, (Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_t7C514ACB3FF8E3EFB210F5F45C9F18A715BB4F80 * L_14 = (Enumerator_t7C514ACB3FF8E3EFB210F5F45C9F18A715BB4F80 *)__this->get_address_of_enumerator_6();
		bool L_15;
		L_15 = Enumerator_MoveNext_mC5829956B29271280A19B86852D365AA6497AB41((Enumerator_t7C514ACB3FF8E3EFB210F5F45C9F18A715BB4F80 *)(Enumerator_t7C514ACB3FF8E3EFB210F5F45C9F18A715BB4F80 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<UnityEngine.InputSystem.Utilities.InternedString>::Dispose() */, (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.Substring,UnityEngine.InputSystem.Utilities.InternedString>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_m876D80C06FEBB8D4BE3417217F60BB745FB53DD6_gshared (WhereSelectListIterator_2_t59101674E6772D817227A6D5C222AF34A4E0BBAD * __this, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B * L_1 = (WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B *, RuntimeObject*, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.Substring,System.Object>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_mD5A3AF5147063891AEC8406A98BC652D0C736EFE_gshared (WhereSelectListIterator_2_t0CEFF5458EED043DC70F7CC9B97B5AB4D45324EB * __this, List_1_tEAD6E3282E028927B32F56E7892994D90D512467 * ___source0, Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 * ___predicate1, Func_2_t4EC2F095EDDC8079537CA0FEDC463635E599C9E4 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_tEAD6E3282E028927B32F56E7892994D90D512467 * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t4EC2F095EDDC8079537CA0FEDC463635E599C9E4 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.Substring,System.Object>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 * WhereSelectListIterator_2_Clone_m770B882B76CF3CD8B4D7EFE4581E54EC83DE59DA_gshared (WhereSelectListIterator_2_t0CEFF5458EED043DC70F7CC9B97B5AB4D45324EB * __this, const RuntimeMethod* method)
{
	{
		List_1_tEAD6E3282E028927B32F56E7892994D90D512467 * L_0 = (List_1_tEAD6E3282E028927B32F56E7892994D90D512467 *)__this->get_source_3();
		Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 * L_1 = (Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 *)__this->get_predicate_4();
		Func_2_t4EC2F095EDDC8079537CA0FEDC463635E599C9E4 * L_2 = (Func_2_t4EC2F095EDDC8079537CA0FEDC463635E599C9E4 *)__this->get_selector_5();
		WhereSelectListIterator_2_t0CEFF5458EED043DC70F7CC9B97B5AB4D45324EB * L_3 = (WhereSelectListIterator_2_t0CEFF5458EED043DC70F7CC9B97B5AB4D45324EB *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_t0CEFF5458EED043DC70F7CC9B97B5AB4D45324EB *, List_1_tEAD6E3282E028927B32F56E7892994D90D512467 *, Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 *, Func_2_t4EC2F095EDDC8079537CA0FEDC463635E599C9E4 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_tEAD6E3282E028927B32F56E7892994D90D512467 *)L_0, (Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 *)L_1, (Func_2_t4EC2F095EDDC8079537CA0FEDC463635E599C9E4 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.Substring,System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_m972DF209FA452810F4297BB14A1FCE26068498C2_gshared (WhereSelectListIterator_2_t0CEFF5458EED043DC70F7CC9B97B5AB4D45324EB * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_tEAD6E3282E028927B32F56E7892994D90D512467 * L_3 = (List_1_tEAD6E3282E028927B32F56E7892994D90D512467 *)__this->get_source_3();
		NullCheck((List_1_tEAD6E3282E028927B32F56E7892994D90D512467 *)L_3);
		Enumerator_t7C514ACB3FF8E3EFB210F5F45C9F18A715BB4F80  L_4;
		L_4 = ((  Enumerator_t7C514ACB3FF8E3EFB210F5F45C9F18A715BB4F80  (*) (List_1_tEAD6E3282E028927B32F56E7892994D90D512467 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_tEAD6E3282E028927B32F56E7892994D90D512467 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_t7C514ACB3FF8E3EFB210F5F45C9F18A715BB4F80 * L_5 = (Enumerator_t7C514ACB3FF8E3EFB210F5F45C9F18A715BB4F80 *)__this->get_address_of_enumerator_6();
		Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F  L_6;
		L_6 = Enumerator_get_Current_mDAAD9332BFDBFF6FDFBCC73B8A70BF674E17FE61_inline((Enumerator_t7C514ACB3FF8E3EFB210F5F45C9F18A715BB4F80 *)(Enumerator_t7C514ACB3FF8E3EFB210F5F45C9F18A715BB4F80 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F )L_6;
		Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 * L_7 = (Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 * L_8 = (Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 *)__this->get_predicate_4();
		Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F  L_9 = V_1;
		NullCheck((Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 *, Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 *)L_8, (Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t4EC2F095EDDC8079537CA0FEDC463635E599C9E4 * L_11 = (Func_2_t4EC2F095EDDC8079537CA0FEDC463635E599C9E4 *)__this->get_selector_5();
		Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F  L_12 = V_1;
		NullCheck((Func_2_t4EC2F095EDDC8079537CA0FEDC463635E599C9E4 *)L_11);
		RuntimeObject * L_13;
		L_13 = ((  RuntimeObject * (*) (Func_2_t4EC2F095EDDC8079537CA0FEDC463635E599C9E4 *, Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t4EC2F095EDDC8079537CA0FEDC463635E599C9E4 *)L_11, (Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_t7C514ACB3FF8E3EFB210F5F45C9F18A715BB4F80 * L_14 = (Enumerator_t7C514ACB3FF8E3EFB210F5F45C9F18A715BB4F80 *)__this->get_address_of_enumerator_6();
		bool L_15;
		L_15 = Enumerator_MoveNext_mC5829956B29271280A19B86852D365AA6497AB41((Enumerator_t7C514ACB3FF8E3EFB210F5F45C9F18A715BB4F80 *)(Enumerator_t7C514ACB3FF8E3EFB210F5F45C9F18A715BB4F80 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Object>::Dispose() */, (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.Substring,System.Object>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_m2F1D10E8C72A475C6501254E358FF5A7C5128679_gshared (WhereSelectListIterator_2_t0CEFF5458EED043DC70F7CC9B97B5AB4D45324EB * __this, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 * L_1 = (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *, RuntimeObject*, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,UnityEngine.InputSystem.Utilities.InternedString>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_mE4E3BB2F19AFC923BA9CA8217AF05B105C6BD77C_gshared (WhereSelectListIterator_2_tF2D97D886F080B26C19947C7C6A56563B86B14D7 * __this, List_1_tEC0B302C3EF23400C3EC92ED7672A715CA602068 * ___source0, Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 * ___predicate1, Func_2_t48DD1C8E439811C492BAFE220A679DB57D7C9370 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		((  void (*) (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_tEC0B302C3EF23400C3EC92ED7672A715CA602068 * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t48DD1C8E439811C492BAFE220A679DB57D7C9370 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,UnityEngine.InputSystem.Utilities.InternedString>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 * WhereSelectListIterator_2_Clone_m75166B79A92AD49E21B7577E9C8170EA83EA0262_gshared (WhereSelectListIterator_2_tF2D97D886F080B26C19947C7C6A56563B86B14D7 * __this, const RuntimeMethod* method)
{
	{
		List_1_tEC0B302C3EF23400C3EC92ED7672A715CA602068 * L_0 = (List_1_tEC0B302C3EF23400C3EC92ED7672A715CA602068 *)__this->get_source_3();
		Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 * L_1 = (Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 *)__this->get_predicate_4();
		Func_2_t48DD1C8E439811C492BAFE220A679DB57D7C9370 * L_2 = (Func_2_t48DD1C8E439811C492BAFE220A679DB57D7C9370 *)__this->get_selector_5();
		WhereSelectListIterator_2_tF2D97D886F080B26C19947C7C6A56563B86B14D7 * L_3 = (WhereSelectListIterator_2_tF2D97D886F080B26C19947C7C6A56563B86B14D7 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_tF2D97D886F080B26C19947C7C6A56563B86B14D7 *, List_1_tEC0B302C3EF23400C3EC92ED7672A715CA602068 *, Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 *, Func_2_t48DD1C8E439811C492BAFE220A679DB57D7C9370 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_tEC0B302C3EF23400C3EC92ED7672A715CA602068 *)L_0, (Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 *)L_1, (Func_2_t48DD1C8E439811C492BAFE220A679DB57D7C9370 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,UnityEngine.InputSystem.Utilities.InternedString>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_m384329556CF83AB5951B336E016F47587A4F8758_gshared (WhereSelectListIterator_2_tF2D97D886F080B26C19947C7C6A56563B86B14D7 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_tEC0B302C3EF23400C3EC92ED7672A715CA602068 * L_3 = (List_1_tEC0B302C3EF23400C3EC92ED7672A715CA602068 *)__this->get_source_3();
		NullCheck((List_1_tEC0B302C3EF23400C3EC92ED7672A715CA602068 *)L_3);
		Enumerator_tF2D95CC18A66F3DE682EAC9B88E865750DFAEF60  L_4;
		L_4 = ((  Enumerator_tF2D95CC18A66F3DE682EAC9B88E865750DFAEF60  (*) (List_1_tEC0B302C3EF23400C3EC92ED7672A715CA602068 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_tEC0B302C3EF23400C3EC92ED7672A715CA602068 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_tF2D95CC18A66F3DE682EAC9B88E865750DFAEF60 * L_5 = (Enumerator_tF2D95CC18A66F3DE682EAC9B88E865750DFAEF60 *)__this->get_address_of_enumerator_6();
		JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB  L_6;
		L_6 = Enumerator_get_Current_mCAE9D184024E7F116926D6D43483BF97BFDFA79A_inline((Enumerator_tF2D95CC18A66F3DE682EAC9B88E865750DFAEF60 *)(Enumerator_tF2D95CC18A66F3DE682EAC9B88E865750DFAEF60 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB )L_6;
		Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 * L_7 = (Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 * L_8 = (Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 *)__this->get_predicate_4();
		JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB  L_9 = V_1;
		NullCheck((Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 *, JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 *)L_8, (JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t48DD1C8E439811C492BAFE220A679DB57D7C9370 * L_11 = (Func_2_t48DD1C8E439811C492BAFE220A679DB57D7C9370 *)__this->get_selector_5();
		JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB  L_12 = V_1;
		NullCheck((Func_2_t48DD1C8E439811C492BAFE220A679DB57D7C9370 *)L_11);
		InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  L_13;
		L_13 = ((  InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  (*) (Func_2_t48DD1C8E439811C492BAFE220A679DB57D7C9370 *, JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t48DD1C8E439811C492BAFE220A679DB57D7C9370 *)L_11, (JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_tF2D95CC18A66F3DE682EAC9B88E865750DFAEF60 * L_14 = (Enumerator_tF2D95CC18A66F3DE682EAC9B88E865750DFAEF60 *)__this->get_address_of_enumerator_6();
		bool L_15;
		L_15 = Enumerator_MoveNext_m084C0187069AC47C975802B0EB3F7257DB6F8507((Enumerator_tF2D95CC18A66F3DE682EAC9B88E865750DFAEF60 *)(Enumerator_tF2D95CC18A66F3DE682EAC9B88E865750DFAEF60 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<UnityEngine.InputSystem.Utilities.InternedString>::Dispose() */, (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,UnityEngine.InputSystem.Utilities.InternedString>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_m5012B88B61B08DB53F3A9BAA927D121F1D884B43_gshared (WhereSelectListIterator_2_tF2D97D886F080B26C19947C7C6A56563B86B14D7 * __this, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B * L_1 = (WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B *, RuntimeObject*, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,System.Object>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_m56C16E2A88726E1764A052A74470F97AFA426B59_gshared (WhereSelectListIterator_2_t2F7487B4BF5A9E71CBE06091C45AF7D98CF9F1AC * __this, List_1_tEC0B302C3EF23400C3EC92ED7672A715CA602068 * ___source0, Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 * ___predicate1, Func_2_t1628B2E4EEC7A32D5B688E54415BC42EDA9D2899 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_tEC0B302C3EF23400C3EC92ED7672A715CA602068 * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t1628B2E4EEC7A32D5B688E54415BC42EDA9D2899 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,System.Object>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 * WhereSelectListIterator_2_Clone_mC179591128EAFDDA93CCE0287CA25E4BE3740D55_gshared (WhereSelectListIterator_2_t2F7487B4BF5A9E71CBE06091C45AF7D98CF9F1AC * __this, const RuntimeMethod* method)
{
	{
		List_1_tEC0B302C3EF23400C3EC92ED7672A715CA602068 * L_0 = (List_1_tEC0B302C3EF23400C3EC92ED7672A715CA602068 *)__this->get_source_3();
		Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 * L_1 = (Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 *)__this->get_predicate_4();
		Func_2_t1628B2E4EEC7A32D5B688E54415BC42EDA9D2899 * L_2 = (Func_2_t1628B2E4EEC7A32D5B688E54415BC42EDA9D2899 *)__this->get_selector_5();
		WhereSelectListIterator_2_t2F7487B4BF5A9E71CBE06091C45AF7D98CF9F1AC * L_3 = (WhereSelectListIterator_2_t2F7487B4BF5A9E71CBE06091C45AF7D98CF9F1AC *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_t2F7487B4BF5A9E71CBE06091C45AF7D98CF9F1AC *, List_1_tEC0B302C3EF23400C3EC92ED7672A715CA602068 *, Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 *, Func_2_t1628B2E4EEC7A32D5B688E54415BC42EDA9D2899 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_tEC0B302C3EF23400C3EC92ED7672A715CA602068 *)L_0, (Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 *)L_1, (Func_2_t1628B2E4EEC7A32D5B688E54415BC42EDA9D2899 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_mFC90941FA2CA1C494E34BBDBE9B3AB8F50871AAD_gshared (WhereSelectListIterator_2_t2F7487B4BF5A9E71CBE06091C45AF7D98CF9F1AC * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_tEC0B302C3EF23400C3EC92ED7672A715CA602068 * L_3 = (List_1_tEC0B302C3EF23400C3EC92ED7672A715CA602068 *)__this->get_source_3();
		NullCheck((List_1_tEC0B302C3EF23400C3EC92ED7672A715CA602068 *)L_3);
		Enumerator_tF2D95CC18A66F3DE682EAC9B88E865750DFAEF60  L_4;
		L_4 = ((  Enumerator_tF2D95CC18A66F3DE682EAC9B88E865750DFAEF60  (*) (List_1_tEC0B302C3EF23400C3EC92ED7672A715CA602068 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_tEC0B302C3EF23400C3EC92ED7672A715CA602068 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_tF2D95CC18A66F3DE682EAC9B88E865750DFAEF60 * L_5 = (Enumerator_tF2D95CC18A66F3DE682EAC9B88E865750DFAEF60 *)__this->get_address_of_enumerator_6();
		JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB  L_6;
		L_6 = Enumerator_get_Current_mCAE9D184024E7F116926D6D43483BF97BFDFA79A_inline((Enumerator_tF2D95CC18A66F3DE682EAC9B88E865750DFAEF60 *)(Enumerator_tF2D95CC18A66F3DE682EAC9B88E865750DFAEF60 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB )L_6;
		Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 * L_7 = (Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 * L_8 = (Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 *)__this->get_predicate_4();
		JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB  L_9 = V_1;
		NullCheck((Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 *, JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 *)L_8, (JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t1628B2E4EEC7A32D5B688E54415BC42EDA9D2899 * L_11 = (Func_2_t1628B2E4EEC7A32D5B688E54415BC42EDA9D2899 *)__this->get_selector_5();
		JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB  L_12 = V_1;
		NullCheck((Func_2_t1628B2E4EEC7A32D5B688E54415BC42EDA9D2899 *)L_11);
		RuntimeObject * L_13;
		L_13 = ((  RuntimeObject * (*) (Func_2_t1628B2E4EEC7A32D5B688E54415BC42EDA9D2899 *, JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t1628B2E4EEC7A32D5B688E54415BC42EDA9D2899 *)L_11, (JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_tF2D95CC18A66F3DE682EAC9B88E865750DFAEF60 * L_14 = (Enumerator_tF2D95CC18A66F3DE682EAC9B88E865750DFAEF60 *)__this->get_address_of_enumerator_6();
		bool L_15;
		L_15 = Enumerator_MoveNext_m084C0187069AC47C975802B0EB3F7257DB6F8507((Enumerator_tF2D95CC18A66F3DE682EAC9B88E865750DFAEF60 *)(Enumerator_tF2D95CC18A66F3DE682EAC9B88E865750DFAEF60 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Object>::Dispose() */, (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,System.Object>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_m6647DB6F32441A99937DA867204D6B1357201999_gshared (WhereSelectListIterator_2_t2F7487B4BF5A9E71CBE06091C45AF7D98CF9F1AC * __this, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 * L_1 = (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *, RuntimeObject*, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Meta.WitAi.Data.Entities.WitEntityDataBase`1<T> Meta.WitAi.Data.Entities.WitEntityDataBase`1<System.Int32>::FromEntityWitResponseNode(Meta.WitAi.Json.WitResponseNode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR WitEntityDataBase_1_tC39032EBC060512BB263163466C00E02FE49885F * WitEntityDataBase_1_FromEntityWitResponseNode_mADCB83014666AC84A29A1EDCA00002F24CB10B57_gshared (WitEntityDataBase_1_tC39032EBC060512BB263163466C00E02FE49885F * __this, WitResponseNode_t2B7283524E11C5C916811576FFFAEBF7FEF27537 * ___node0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&JsonConvert_t3E03C6491535D0515C23DC9BCF3B620DB0063098_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	WitEntityDataBase_1_tC39032EBC060512BB263163466C00E02FE49885F * V_0 = NULL;
	{
		// responseNode = node;
		WitResponseNode_t2B7283524E11C5C916811576FFFAEBF7FEF27537 * L_0 = ___node0;
		__this->set_responseNode_0(L_0);
		// WitEntityDataBase<T> result = this;
		V_0 = (WitEntityDataBase_1_tC39032EBC060512BB263163466C00E02FE49885F *)__this;
		// JsonConvert.DeserializeIntoObject(ref result, node);
		WitResponseNode_t2B7283524E11C5C916811576FFFAEBF7FEF27537 * L_1 = ___node0;
		IL2CPP_RUNTIME_CLASS_INIT(JsonConvert_t3E03C6491535D0515C23DC9BCF3B620DB0063098_il2cpp_TypeInfo_var);
		bool L_2;
		L_2 = ((  bool (*) (WitEntityDataBase_1_tC39032EBC060512BB263163466C00E02FE49885F **, WitResponseNode_t2B7283524E11C5C916811576FFFAEBF7FEF27537 *, JsonConverterU5BU5D_t8327F3FEA5C837FBE4A4A8503B5C0B1AD46A5154*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((WitEntityDataBase_1_tC39032EBC060512BB263163466C00E02FE49885F **)(WitEntityDataBase_1_tC39032EBC060512BB263163466C00E02FE49885F **)(&V_0), (WitResponseNode_t2B7283524E11C5C916811576FFFAEBF7FEF27537 *)L_1, (JsonConverterU5BU5D_t8327F3FEA5C837FBE4A4A8503B5C0B1AD46A5154*)(JsonConverterU5BU5D_t8327F3FEA5C837FBE4A4A8503B5C0B1AD46A5154*)NULL, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		// return result;
		WitEntityDataBase_1_tC39032EBC060512BB263163466C00E02FE49885F * L_3 = V_0;
		return (WitEntityDataBase_1_tC39032EBC060512BB263163466C00E02FE49885F *)L_3;
	}
}
// System.String Meta.WitAi.Data.Entities.WitEntityDataBase`1<System.Int32>::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* WitEntityDataBase_1_ToString_m6E7B0B2B955CF430103EE948A8E0BFA3A2F2A6C9_gshared (WitEntityDataBase_1_tC39032EBC060512BB263163466C00E02FE49885F * __this, const RuntimeMethod* method)
{
	{
		// return value.ToString();
		int32_t* L_0 = (int32_t*)__this->get_address_of_value_8();
		String_t* L_1;
		L_1 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)(int32_t*)L_0, /*hidden argument*/NULL);
		return (String_t*)L_1;
	}
}
// System.Void Meta.WitAi.Data.Entities.WitEntityDataBase`1<System.Int32>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WitEntityDataBase_1__ctor_mDC859120A1BD4AA9900465DA1918B882CFEBCF03_gshared (WitEntityDataBase_1_tC39032EBC060512BB263163466C00E02FE49885F * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Meta.WitAi.Data.Entities.WitEntityDataBase`1<T> Meta.WitAi.Data.Entities.WitEntityDataBase`1<System.Object>::FromEntityWitResponseNode(Meta.WitAi.Json.WitResponseNode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR WitEntityDataBase_1_t6ABD82324321B4C13B04F50C98D9489EAA384031 * WitEntityDataBase_1_FromEntityWitResponseNode_mC256D44AF807F86A0F6C7E6BC82D64ED269DCE64_gshared (WitEntityDataBase_1_t6ABD82324321B4C13B04F50C98D9489EAA384031 * __this, WitResponseNode_t2B7283524E11C5C916811576FFFAEBF7FEF27537 * ___node0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&JsonConvert_t3E03C6491535D0515C23DC9BCF3B620DB0063098_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	WitEntityDataBase_1_t6ABD82324321B4C13B04F50C98D9489EAA384031 * V_0 = NULL;
	{
		// responseNode = node;
		WitResponseNode_t2B7283524E11C5C916811576FFFAEBF7FEF27537 * L_0 = ___node0;
		__this->set_responseNode_0(L_0);
		// WitEntityDataBase<T> result = this;
		V_0 = (WitEntityDataBase_1_t6ABD82324321B4C13B04F50C98D9489EAA384031 *)__this;
		// JsonConvert.DeserializeIntoObject(ref result, node);
		WitResponseNode_t2B7283524E11C5C916811576FFFAEBF7FEF27537 * L_1 = ___node0;
		IL2CPP_RUNTIME_CLASS_INIT(JsonConvert_t3E03C6491535D0515C23DC9BCF3B620DB0063098_il2cpp_TypeInfo_var);
		bool L_2;
		L_2 = ((  bool (*) (WitEntityDataBase_1_t6ABD82324321B4C13B04F50C98D9489EAA384031 **, WitResponseNode_t2B7283524E11C5C916811576FFFAEBF7FEF27537 *, JsonConverterU5BU5D_t8327F3FEA5C837FBE4A4A8503B5C0B1AD46A5154*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((WitEntityDataBase_1_t6ABD82324321B4C13B04F50C98D9489EAA384031 **)(WitEntityDataBase_1_t6ABD82324321B4C13B04F50C98D9489EAA384031 **)(&V_0), (WitResponseNode_t2B7283524E11C5C916811576FFFAEBF7FEF27537 *)L_1, (JsonConverterU5BU5D_t8327F3FEA5C837FBE4A4A8503B5C0B1AD46A5154*)(JsonConverterU5BU5D_t8327F3FEA5C837FBE4A4A8503B5C0B1AD46A5154*)NULL, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		// return result;
		WitEntityDataBase_1_t6ABD82324321B4C13B04F50C98D9489EAA384031 * L_3 = V_0;
		return (WitEntityDataBase_1_t6ABD82324321B4C13B04F50C98D9489EAA384031 *)L_3;
	}
}
// System.String Meta.WitAi.Data.Entities.WitEntityDataBase`1<System.Object>::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* WitEntityDataBase_1_ToString_m9C80C8F7BBC6EBF25C89233DB83DCD091575837E_gshared (WitEntityDataBase_1_t6ABD82324321B4C13B04F50C98D9489EAA384031 * __this, const RuntimeMethod* method)
{
	{
		// return value.ToString();
		RuntimeObject ** L_0 = (RuntimeObject **)__this->get_address_of_value_8();
		NullCheck((RuntimeObject *)(*L_0));
		String_t* L_1;
		L_1 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (RuntimeObject *)(*L_0));
		return (String_t*)L_1;
	}
}
// System.Void Meta.WitAi.Data.Entities.WitEntityDataBase`1<System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WitEntityDataBase_1__ctor_mA76C8A8697376A159874D2DA871FC30606AAB1C8_gshared (WitEntityDataBase_1_t6ABD82324321B4C13B04F50C98D9489EAA384031 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Meta.WitAi.Data.Entities.WitEntityDataBase`1<T> Meta.WitAi.Data.Entities.WitEntityDataBase`1<System.Single>::FromEntityWitResponseNode(Meta.WitAi.Json.WitResponseNode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR WitEntityDataBase_1_t1215DD3B64DF55B5D59B3E81E489EBD1B9353BBE * WitEntityDataBase_1_FromEntityWitResponseNode_m683B2B87A5A874CCD9F66AE000AEAE1993DE3CFA_gshared (WitEntityDataBase_1_t1215DD3B64DF55B5D59B3E81E489EBD1B9353BBE * __this, WitResponseNode_t2B7283524E11C5C916811576FFFAEBF7FEF27537 * ___node0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&JsonConvert_t3E03C6491535D0515C23DC9BCF3B620DB0063098_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	WitEntityDataBase_1_t1215DD3B64DF55B5D59B3E81E489EBD1B9353BBE * V_0 = NULL;
	{
		// responseNode = node;
		WitResponseNode_t2B7283524E11C5C916811576FFFAEBF7FEF27537 * L_0 = ___node0;
		__this->set_responseNode_0(L_0);
		// WitEntityDataBase<T> result = this;
		V_0 = (WitEntityDataBase_1_t1215DD3B64DF55B5D59B3E81E489EBD1B9353BBE *)__this;
		// JsonConvert.DeserializeIntoObject(ref result, node);
		WitResponseNode_t2B7283524E11C5C916811576FFFAEBF7FEF27537 * L_1 = ___node0;
		IL2CPP_RUNTIME_CLASS_INIT(JsonConvert_t3E03C6491535D0515C23DC9BCF3B620DB0063098_il2cpp_TypeInfo_var);
		bool L_2;
		L_2 = ((  bool (*) (WitEntityDataBase_1_t1215DD3B64DF55B5D59B3E81E489EBD1B9353BBE **, WitResponseNode_t2B7283524E11C5C916811576FFFAEBF7FEF27537 *, JsonConverterU5BU5D_t8327F3FEA5C837FBE4A4A8503B5C0B1AD46A5154*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((WitEntityDataBase_1_t1215DD3B64DF55B5D59B3E81E489EBD1B9353BBE **)(WitEntityDataBase_1_t1215DD3B64DF55B5D59B3E81E489EBD1B9353BBE **)(&V_0), (WitResponseNode_t2B7283524E11C5C916811576FFFAEBF7FEF27537 *)L_1, (JsonConverterU5BU5D_t8327F3FEA5C837FBE4A4A8503B5C0B1AD46A5154*)(JsonConverterU5BU5D_t8327F3FEA5C837FBE4A4A8503B5C0B1AD46A5154*)NULL, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		// return result;
		WitEntityDataBase_1_t1215DD3B64DF55B5D59B3E81E489EBD1B9353BBE * L_3 = V_0;
		return (WitEntityDataBase_1_t1215DD3B64DF55B5D59B3E81E489EBD1B9353BBE *)L_3;
	}
}
// System.String Meta.WitAi.Data.Entities.WitEntityDataBase`1<System.Single>::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* WitEntityDataBase_1_ToString_m52EDED033C34A626EE3DC7065FC259BC4465A7AA_gshared (WitEntityDataBase_1_t1215DD3B64DF55B5D59B3E81E489EBD1B9353BBE * __this, const RuntimeMethod* method)
{
	{
		// return value.ToString();
		float* L_0 = (float*)__this->get_address_of_value_8();
		String_t* L_1;
		L_1 = Single_ToString_m80E7ABED4F4D73F2BE19DDB80D3D92FCD8DFA010((float*)(float*)L_0, /*hidden argument*/NULL);
		return (String_t*)L_1;
	}
}
// System.Void Meta.WitAi.Data.Entities.WitEntityDataBase`1<System.Single>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WitEntityDataBase_1__ctor_m0A30016EA44991B703FEEE96F2D0EEAD1650264C_gshared (WitEntityDataBase_1_t1215DD3B64DF55B5D59B3E81E489EBD1B9353BBE * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184  Enumerator_get_Current_mD55E3F632B5C19FA05221B587346BA3268167712_gshared_inline (Enumerator_tF2D0EEBD6B861D9127DD6DC36B733FD42070858C * __this, const RuntimeMethod* method)
{
	{
		KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184  L_0 = (KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184 )__this->get_current_3();
		return (KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184 )L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR HandSkeletonJoint_t5B4E2A23FDAC8BC4C8751E5FBC7C96A1056582F3  Enumerator_get_Current_mFF6988CD9E5309A0577F8335E26AA7191198035F_gshared_inline (Enumerator_t749ECD21102D25154785738AC993322CE5A9A3D6 * __this, const RuntimeMethod* method)
{
	{
		HandSkeletonJoint_t5B4E2A23FDAC8BC4C8751E5FBC7C96A1056582F3  L_0 = (HandSkeletonJoint_t5B4E2A23FDAC8BC4C8751E5FBC7C96A1056582F3 )__this->get_current_3();
		return (HandSkeletonJoint_t5B4E2A23FDAC8BC4C8751E5FBC7C96A1056582F3 )L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t Enumerator_get_Current_mDD503AFD786235D3B40842B0872AC17DC86EB040_gshared_inline (Enumerator_t3A873D53A96DA0AC1C030CB5A0BF2D0F57FCC984 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_current_3();
		return (int32_t)L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  Enumerator_get_Current_m3E41B27F955C5A287A6138E9D254833ACC2BC1B2_gshared_inline (Enumerator_tD1ECE103520161C1BD172331D6968D3D184B3B6F * __this, const RuntimeMethod* method)
{
	{
		InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  L_0 = (InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED )__this->get_current_3();
		return (InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED )L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA  Enumerator_get_Current_m0D244550779CCA026ED61FE7767F255D97C413CD_gshared_inline (Enumerator_tA2E0F83E15EA46A2B4B08AFB750700C6E1387FC0 * __this, const RuntimeMethod* method)
{
	{
		NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA  L_0 = (NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA )__this->get_current_3();
		return (NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA )L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077  Enumerator_get_Current_m310A8D3883A91C67B235D82B07D2B234536E2D39_gshared_inline (Enumerator_tDEE846E92D2B22781FB09541CB1600E5398D3A20 * __this, const RuntimeMethod* method)
{
	{
		NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077  L_0 = (NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077 )__this->get_current_3();
		return (NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077 )L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject * Enumerator_get_Current_m9C4EBBD2108B51885E750F927D7936290C8E20EE_gshared_inline (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_current_3();
		return (RuntimeObject *)L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Pose_t9F30358E65733E60A1DC8682FDB7104F40C9434A  Enumerator_get_Current_mFDB2FBE724A02C5CAFAE108E12A75676EFE43A5F_gshared_inline (Enumerator_tA7E1DE1C5B2833EE935A19CAF705B2C053EBC9AB * __this, const RuntimeMethod* method)
{
	{
		Pose_t9F30358E65733E60A1DC8682FDB7104F40C9434A  L_0 = (Pose_t9F30358E65733E60A1DC8682FDB7104F40C9434A )__this->get_current_3();
		return (Pose_t9F30358E65733E60A1DC8682FDB7104F40C9434A )L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Enumerator_get_Current_m1DC0B40110173B7E2D13319164F7657C3BE3536D_gshared_inline (Enumerator_tC1FD01C8BB5327D442D71FD022B4338ACD701783 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = (float)__this->get_current_3();
		return (float)L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F  Enumerator_get_Current_mDAAD9332BFDBFF6FDFBCC73B8A70BF674E17FE61_gshared_inline (Enumerator_t7C514ACB3FF8E3EFB210F5F45C9F18A715BB4F80 * __this, const RuntimeMethod* method)
{
	{
		Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F  L_0 = (Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F )__this->get_current_3();
		return (Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F )L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB  Enumerator_get_Current_mCAE9D184024E7F116926D6D43483BF97BFDFA79A_gshared_inline (Enumerator_tF2D95CC18A66F3DE682EAC9B88E865750DFAEF60 * __this, const RuntimeMethod* method)
{
	{
		JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB  L_0 = (JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB )__this->get_current_3();
		return (JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB )L_0;
	}
}
