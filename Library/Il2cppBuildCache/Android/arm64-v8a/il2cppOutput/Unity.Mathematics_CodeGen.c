﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void Unity.IL2CPP.CompilerServices.Il2CppEagerStaticClassConstructionAttribute::.ctor()
extern void Il2CppEagerStaticClassConstructionAttribute__ctor_m1E49CFE0D3ED26EB0C73B0E11FBFD9EED2C4BF81 (void);
// 0x00000002 System.UInt32 Unity.Mathematics.math::hash(Unity.Mathematics.float2)
extern void math_hash_m8CCDB68745E0F719EFBE1F7243F5D96456423CA5 (void);
// 0x00000003 System.UInt32 Unity.Mathematics.math::hash(Unity.Mathematics.float3)
extern void math_hash_m7815DEF81451705FB9962A21D85AF7BD67CA1548 (void);
// 0x00000004 System.UInt32 Unity.Mathematics.math::hash(Unity.Mathematics.float4)
extern void math_hash_mAD97D79195C24A4A0556C9DB839FBDEEB1C05F4A (void);
// 0x00000005 System.Int32 Unity.Mathematics.math::asint(System.Single)
extern void math_asint_m8FCE7F451B5018A55B67FAF4371CD62B0F5A1849 (void);
// 0x00000006 System.UInt32 Unity.Mathematics.math::asuint(System.Single)
extern void math_asuint_m9F6DEA16C65215E4A7371504A429B243FF1CDBF8 (void);
// 0x00000007 Unity.Mathematics.uint2 Unity.Mathematics.math::asuint(Unity.Mathematics.float2)
extern void math_asuint_m2AC97544ECCD77033410798462D8C186EBC63CC2 (void);
// 0x00000008 Unity.Mathematics.uint3 Unity.Mathematics.math::asuint(Unity.Mathematics.float3)
extern void math_asuint_m318F6402E3C0D633BF5D509BBBF31D941FBFE92A (void);
// 0x00000009 Unity.Mathematics.uint4 Unity.Mathematics.math::asuint(Unity.Mathematics.float4)
extern void math_asuint_mD4A54552D559A70071475766BEC190F1E4963888 (void);
// 0x0000000A System.Single Unity.Mathematics.math::asfloat(System.Int32)
extern void math_asfloat_mCBAE16570D68CD2DA88354AEC3CB245DEFCD5BA2 (void);
// 0x0000000B System.Single Unity.Mathematics.math::asfloat(System.UInt32)
extern void math_asfloat_m1E04583A662D10FF3D3B5F5DE60E857FC52C8290 (void);
// 0x0000000C System.Single Unity.Mathematics.math::min(System.Single,System.Single)
extern void math_min_m1ABE3D06A7BF422498FBF240CDCDC6D11273DD8B (void);
// 0x0000000D System.Single Unity.Mathematics.math::max(System.Single,System.Single)
extern void math_max_mF7C9D30DE6384D5DE84917F51E4D4D8010460932 (void);
// 0x0000000E System.Single Unity.Mathematics.math::lerp(System.Single,System.Single,System.Single)
extern void math_lerp_m373C36BD2152A1CE24359750ED1ABDE3FFE70000 (void);
// 0x0000000F Unity.Mathematics.float2 Unity.Mathematics.math::lerp(Unity.Mathematics.float2,Unity.Mathematics.float2,System.Single)
extern void math_lerp_mC3449FE2CBEB79190BF722AAAD9979F1AE61C6A2 (void);
// 0x00000010 Unity.Mathematics.float3 Unity.Mathematics.math::lerp(Unity.Mathematics.float3,Unity.Mathematics.float3,System.Single)
extern void math_lerp_m4E2C78F125D14EC7476C45708A206DD69C3B82A4 (void);
// 0x00000011 Unity.Mathematics.float4 Unity.Mathematics.math::lerp(Unity.Mathematics.float4,Unity.Mathematics.float4,System.Single)
extern void math_lerp_mB47979A95FE6478DCA26C327074491D61266C8AD (void);
// 0x00000012 System.Single Unity.Mathematics.math::clamp(System.Single,System.Single,System.Single)
extern void math_clamp_m1F61B5D20891B377EB3AB93B7432C023EA2A7514 (void);
// 0x00000013 System.Single Unity.Mathematics.math::abs(System.Single)
extern void math_abs_m21BB6D1489DC3A2AF7942244A41B39EF26031AB8 (void);
// 0x00000014 System.Single Unity.Mathematics.math::dot(Unity.Mathematics.float2,Unity.Mathematics.float2)
extern void math_dot_mAE4F4CB702904786287A2C2A64E23D07E9AD4A6E (void);
// 0x00000015 System.Single Unity.Mathematics.math::dot(Unity.Mathematics.float3,Unity.Mathematics.float3)
extern void math_dot_mBD43287353A047EEFD1E728CB7A4E9C89A87778E (void);
// 0x00000016 System.Single Unity.Mathematics.math::dot(Unity.Mathematics.float4,Unity.Mathematics.float4)
extern void math_dot_m00E3616ACE8D9ACFEB722A2F9485BDD105B5AB95 (void);
// 0x00000017 System.Single Unity.Mathematics.math::floor(System.Single)
extern void math_floor_m1967459EF7EAB1EF62537F5F9DED7459797D20EA (void);
// 0x00000018 System.Single Unity.Mathematics.math::ceil(System.Single)
extern void math_ceil_m11AA9693F4C38B56E8BE3AB40900DD660A89B2F0 (void);
// 0x00000019 System.Single Unity.Mathematics.math::lengthsq(Unity.Mathematics.float2)
extern void math_lengthsq_m0D2D7F7412B852564DC950DDE5261B089AB0DAA0 (void);
// 0x0000001A System.Single Unity.Mathematics.math::lengthsq(Unity.Mathematics.float3)
extern void math_lengthsq_m1EACA00B994E33AE1D88A05A39A85BABB4B5A726 (void);
// 0x0000001B System.Single Unity.Mathematics.math::lengthsq(Unity.Mathematics.float4)
extern void math_lengthsq_mD73F2E4C971CC3DC14C8605A3BE158195E53164C (void);
// 0x0000001C System.Single Unity.Mathematics.math::distancesq(System.Single,System.Single)
extern void math_distancesq_m58600816D7DB6E470C3DF697B4C0CE63134A6BD1 (void);
// 0x0000001D System.Single Unity.Mathematics.math::distancesq(Unity.Mathematics.float2,Unity.Mathematics.float2)
extern void math_distancesq_m18F497591541BABA88E2448BC908ED5A3681970A (void);
// 0x0000001E System.Single Unity.Mathematics.math::distancesq(Unity.Mathematics.float3,Unity.Mathematics.float3)
extern void math_distancesq_m8292B092EC4B6AC609C92F29FF5B7AE8D9E7EE74 (void);
// 0x0000001F System.Single Unity.Mathematics.math::distancesq(Unity.Mathematics.float4,Unity.Mathematics.float4)
extern void math_distancesq_mECB50A017D398A1DE8BA36240C75386A658BAC21 (void);
// 0x00000020 System.UInt32 Unity.Mathematics.math::csum(Unity.Mathematics.uint2)
extern void math_csum_m5FEF094CBBA9529BF38B1C5E9E37D61A355F4E6B (void);
// 0x00000021 System.UInt32 Unity.Mathematics.math::csum(Unity.Mathematics.uint3)
extern void math_csum_mE551D40C1A80EF769E48359F0892146ADD596B15 (void);
// 0x00000022 System.UInt32 Unity.Mathematics.math::csum(Unity.Mathematics.uint4)
extern void math_csum_m2CEC45B544D5B9EFB714CC5325CB951CDC435753 (void);
// 0x00000023 Unity.Mathematics.uint2 Unity.Mathematics.math::uint2(System.UInt32,System.UInt32)
extern void math_uint2_m9BB58BBC62A71CFD2D3704FE44E37CEBE766767C (void);
// 0x00000024 System.UInt32 Unity.Mathematics.math::hash(Unity.Mathematics.uint2)
extern void math_hash_m8295F93F93F0E5907F8F9DA098C4D38956B49DE9 (void);
// 0x00000025 Unity.Mathematics.uint3 Unity.Mathematics.math::uint3(System.UInt32,System.UInt32,System.UInt32)
extern void math_uint3_m558D7B725E08B0063F9E311DACA5AB1693FA18D4 (void);
// 0x00000026 System.UInt32 Unity.Mathematics.math::hash(Unity.Mathematics.uint3)
extern void math_hash_m762A1D05870D2F55D9ECB11378D169B08BB04BB9 (void);
// 0x00000027 Unity.Mathematics.uint4 Unity.Mathematics.math::uint4(System.UInt32,System.UInt32,System.UInt32,System.UInt32)
extern void math_uint4_m677BF0973E67EDD7890759AAA4301CD886065F59 (void);
// 0x00000028 System.UInt32 Unity.Mathematics.math::hash(Unity.Mathematics.uint4)
extern void math_hash_m7A24547FED62DAF7EBE18E04CF867809F48F4C08 (void);
// 0x00000029 System.Void Unity.Mathematics.float2::.ctor(System.Single,System.Single)
extern void float2__ctor_mDEC8A4039E029926E0424F0FB614C7F679AE7449 (void);
// 0x0000002A Unity.Mathematics.float2 Unity.Mathematics.float2::op_Multiply(System.Single,Unity.Mathematics.float2)
extern void float2_op_Multiply_mBD0A466E241FC2001B52D2851901C772538FCFD4 (void);
// 0x0000002B Unity.Mathematics.float2 Unity.Mathematics.float2::op_Addition(Unity.Mathematics.float2,Unity.Mathematics.float2)
extern void float2_op_Addition_m7BF3D8B765D963BA2EF831D444FF5A2149978D3C (void);
// 0x0000002C Unity.Mathematics.float2 Unity.Mathematics.float2::op_Subtraction(Unity.Mathematics.float2,Unity.Mathematics.float2)
extern void float2_op_Subtraction_mABAE62700AB469FF2F150C2B3C2BC8927B36BCC4 (void);
// 0x0000002D System.Boolean Unity.Mathematics.float2::Equals(Unity.Mathematics.float2)
extern void float2_Equals_mD7E0010E86764F768D155F084C5049B1A37451AB (void);
// 0x0000002E System.Boolean Unity.Mathematics.float2::Equals(System.Object)
extern void float2_Equals_m2B929D2B1750063ED9C8F71F517E707629A2865D (void);
// 0x0000002F System.Int32 Unity.Mathematics.float2::GetHashCode()
extern void float2_GetHashCode_m14F0A1D75CEB912B8D368074F8EC66E768800FE9 (void);
// 0x00000030 System.String Unity.Mathematics.float2::ToString()
extern void float2_ToString_mDD9456E5C3F28889E650CED5533DCD06219F3A28 (void);
// 0x00000031 System.String Unity.Mathematics.float2::ToString(System.String,System.IFormatProvider)
extern void float2_ToString_mCCF46926AC2D48D3344DC9373B33F0E005071C78 (void);
// 0x00000032 UnityEngine.Vector2 Unity.Mathematics.float2::op_Implicit(Unity.Mathematics.float2)
extern void float2_op_Implicit_mB136BDC4677B5FE6130CA815A737E7738546D119 (void);
// 0x00000033 Unity.Mathematics.float2 Unity.Mathematics.float2::op_Implicit(UnityEngine.Vector2)
extern void float2_op_Implicit_mFE7C6B558CB9ACCDF08F4B7A37231E89F431623A (void);
// 0x00000034 System.Void Unity.Mathematics.float3::.ctor(System.Single,System.Single,System.Single)
extern void float3__ctor_mA1B0F2B6874F0DEDFC715C334892EB2FAB31B40A (void);
// 0x00000035 Unity.Mathematics.float3 Unity.Mathematics.float3::op_Multiply(System.Single,Unity.Mathematics.float3)
extern void float3_op_Multiply_m3FBEB6858C438A95F84FF3CC6B09D17396A8884E (void);
// 0x00000036 Unity.Mathematics.float3 Unity.Mathematics.float3::op_Addition(Unity.Mathematics.float3,Unity.Mathematics.float3)
extern void float3_op_Addition_m43A3E7B9C99751652618541F0E6A04EE14570B68 (void);
// 0x00000037 Unity.Mathematics.float3 Unity.Mathematics.float3::op_Subtraction(Unity.Mathematics.float3,Unity.Mathematics.float3)
extern void float3_op_Subtraction_mE9924904DB860665A62840BED263EBEEDF70423F (void);
// 0x00000038 System.Boolean Unity.Mathematics.float3::Equals(Unity.Mathematics.float3)
extern void float3_Equals_m21D80AD2B9F37B1210C6F36BE86F76AA527BF2CF (void);
// 0x00000039 System.Boolean Unity.Mathematics.float3::Equals(System.Object)
extern void float3_Equals_m3C3B40E46D39C1FC39D250269E33EFC955235B2B (void);
// 0x0000003A System.Int32 Unity.Mathematics.float3::GetHashCode()
extern void float3_GetHashCode_m127685CE1D48644B6DC250A3CA9B989A54BA1AF3 (void);
// 0x0000003B System.String Unity.Mathematics.float3::ToString()
extern void float3_ToString_mB8035CAC163C19E11A35ADFE448E5B362A56D8AF (void);
// 0x0000003C System.String Unity.Mathematics.float3::ToString(System.String,System.IFormatProvider)
extern void float3_ToString_mAA3FEBFB0473D5C7F22CB3B6B56E52BEE6054F5F (void);
// 0x0000003D UnityEngine.Vector3 Unity.Mathematics.float3::op_Implicit(Unity.Mathematics.float3)
extern void float3_op_Implicit_mF3897B2F87EEB82C8D46016ADADD80B4279D9F8B (void);
// 0x0000003E Unity.Mathematics.float3 Unity.Mathematics.float3::op_Implicit(UnityEngine.Vector3)
extern void float3_op_Implicit_m52E7BAC4F651B42D13F635E5942FA9FD73F9084C (void);
// 0x0000003F System.Void Unity.Mathematics.float4::.ctor(System.Single,System.Single,System.Single,System.Single)
extern void float4__ctor_mD7DD9759C791823116719CAE8EE693E9C173E241 (void);
// 0x00000040 Unity.Mathematics.float4 Unity.Mathematics.float4::op_Multiply(System.Single,Unity.Mathematics.float4)
extern void float4_op_Multiply_mECCB11405184D4A1579030A1E7CB6CA41B267217 (void);
// 0x00000041 Unity.Mathematics.float4 Unity.Mathematics.float4::op_Addition(Unity.Mathematics.float4,Unity.Mathematics.float4)
extern void float4_op_Addition_mCE615C711B80C9F104005D53359304816E6CA051 (void);
// 0x00000042 Unity.Mathematics.float4 Unity.Mathematics.float4::op_Subtraction(Unity.Mathematics.float4,Unity.Mathematics.float4)
extern void float4_op_Subtraction_m33241A3298AFAADF2BEF985A5C70FAE5E70FB561 (void);
// 0x00000043 System.Boolean Unity.Mathematics.float4::Equals(Unity.Mathematics.float4)
extern void float4_Equals_mC17658EE14E0595E33E170A1CF5325AD7240B508 (void);
// 0x00000044 System.Boolean Unity.Mathematics.float4::Equals(System.Object)
extern void float4_Equals_m6247D066192E1714DDC2CC001D0D20D10D60228B (void);
// 0x00000045 System.Int32 Unity.Mathematics.float4::GetHashCode()
extern void float4_GetHashCode_mDAE54F7570659BE4963BEA7F347A137B7EFC5230 (void);
// 0x00000046 System.String Unity.Mathematics.float4::ToString()
extern void float4_ToString_mF8E4242AD739E6BC3CFDAF96C26236D385DF7850 (void);
// 0x00000047 System.String Unity.Mathematics.float4::ToString(System.String,System.IFormatProvider)
extern void float4_ToString_m643BEBFFE6AE7B5E0366C777BEE1178924D5374B (void);
// 0x00000048 Unity.Mathematics.float4 Unity.Mathematics.float4::op_Implicit(UnityEngine.Vector4)
extern void float4_op_Implicit_m9E3CAA6CAB5E9EAF52037E2D3906C7DAC46624DB (void);
// 0x00000049 UnityEngine.Vector4 Unity.Mathematics.float4::op_Implicit(Unity.Mathematics.float4)
extern void float4_op_Implicit_m4DE78B67B42EE7149183D09445B6622CC30679C7 (void);
// 0x0000004A System.Void Unity.Mathematics.uint2::.ctor(System.UInt32,System.UInt32)
extern void uint2__ctor_m15E587DF7FC128857586EA962472021625CB0727 (void);
// 0x0000004B Unity.Mathematics.uint2 Unity.Mathematics.uint2::op_Multiply(Unity.Mathematics.uint2,Unity.Mathematics.uint2)
extern void uint2_op_Multiply_m064F9A426727F973DBE586E0210134B53C38E190 (void);
// 0x0000004C System.Boolean Unity.Mathematics.uint2::Equals(Unity.Mathematics.uint2)
extern void uint2_Equals_m4D6B7E58271563553964C88B0C6C11B11E9DDEFF (void);
// 0x0000004D System.Boolean Unity.Mathematics.uint2::Equals(System.Object)
extern void uint2_Equals_mDC5AF9975359EB05CA526D9D132038CC07BA610C (void);
// 0x0000004E System.Int32 Unity.Mathematics.uint2::GetHashCode()
extern void uint2_GetHashCode_m1AA42656371544BEFEF58990F0F5D3375771BDED (void);
// 0x0000004F System.String Unity.Mathematics.uint2::ToString()
extern void uint2_ToString_m11C8B65BDF3EE9F3E076886108F29C7085311CD8 (void);
// 0x00000050 System.String Unity.Mathematics.uint2::ToString(System.String,System.IFormatProvider)
extern void uint2_ToString_m82A6C25A6E380499B47B6D45037EC327449303A1 (void);
// 0x00000051 System.Void Unity.Mathematics.uint3::.ctor(System.UInt32,System.UInt32,System.UInt32)
extern void uint3__ctor_m6000FC36B0AD34E3897EBB27D734326F63D5198E (void);
// 0x00000052 Unity.Mathematics.uint3 Unity.Mathematics.uint3::op_Multiply(Unity.Mathematics.uint3,Unity.Mathematics.uint3)
extern void uint3_op_Multiply_m2586E7297C34E588128E9A04495534B076A3A047 (void);
// 0x00000053 System.Boolean Unity.Mathematics.uint3::Equals(Unity.Mathematics.uint3)
extern void uint3_Equals_m13DA44E1022044FEBBA2066ECD1A8AC1CB53DF91 (void);
// 0x00000054 System.Boolean Unity.Mathematics.uint3::Equals(System.Object)
extern void uint3_Equals_mAAD3238F59F9CA833513E94E33A329834CB205FE (void);
// 0x00000055 System.Int32 Unity.Mathematics.uint3::GetHashCode()
extern void uint3_GetHashCode_mD983DD667EC5C064E237BFA773C185AB36599B76 (void);
// 0x00000056 System.String Unity.Mathematics.uint3::ToString()
extern void uint3_ToString_mEADE36DE83569B87E7DF9B37EB4A04357B83B277 (void);
// 0x00000057 System.String Unity.Mathematics.uint3::ToString(System.String,System.IFormatProvider)
extern void uint3_ToString_mF1C800E07DAF3C5478742C64CD0C4C76C7E63167 (void);
// 0x00000058 System.Void Unity.Mathematics.uint4::.ctor(System.UInt32,System.UInt32,System.UInt32,System.UInt32)
extern void uint4__ctor_mE2EF35B487246E995E6B5226C41B2ABF7D695852 (void);
// 0x00000059 Unity.Mathematics.uint4 Unity.Mathematics.uint4::op_Multiply(Unity.Mathematics.uint4,Unity.Mathematics.uint4)
extern void uint4_op_Multiply_mF8519CAB503E1A8FF0AFF2084D591321C33CEA15 (void);
// 0x0000005A System.Boolean Unity.Mathematics.uint4::Equals(Unity.Mathematics.uint4)
extern void uint4_Equals_mA01BD6C642C7AA15938773343109B65A7017A8E5 (void);
// 0x0000005B System.Boolean Unity.Mathematics.uint4::Equals(System.Object)
extern void uint4_Equals_mA36D75D93A16B63C63FF5556877325DE7209CA0C (void);
// 0x0000005C System.Int32 Unity.Mathematics.uint4::GetHashCode()
extern void uint4_GetHashCode_m0ACFBA8EEEBE72824B814A9147C62BA2DAD67E04 (void);
// 0x0000005D System.String Unity.Mathematics.uint4::ToString()
extern void uint4_ToString_m88626950AE1516CF95F414CFC9D3328308CFFD21 (void);
// 0x0000005E System.String Unity.Mathematics.uint4::ToString(System.String,System.IFormatProvider)
extern void uint4_ToString_m89A2419938888294462214E4F1B77BFB2A6D6622 (void);
static Il2CppMethodPointer s_methodPointers[94] = 
{
	Il2CppEagerStaticClassConstructionAttribute__ctor_m1E49CFE0D3ED26EB0C73B0E11FBFD9EED2C4BF81,
	math_hash_m8CCDB68745E0F719EFBE1F7243F5D96456423CA5,
	math_hash_m7815DEF81451705FB9962A21D85AF7BD67CA1548,
	math_hash_mAD97D79195C24A4A0556C9DB839FBDEEB1C05F4A,
	math_asint_m8FCE7F451B5018A55B67FAF4371CD62B0F5A1849,
	math_asuint_m9F6DEA16C65215E4A7371504A429B243FF1CDBF8,
	math_asuint_m2AC97544ECCD77033410798462D8C186EBC63CC2,
	math_asuint_m318F6402E3C0D633BF5D509BBBF31D941FBFE92A,
	math_asuint_mD4A54552D559A70071475766BEC190F1E4963888,
	math_asfloat_mCBAE16570D68CD2DA88354AEC3CB245DEFCD5BA2,
	math_asfloat_m1E04583A662D10FF3D3B5F5DE60E857FC52C8290,
	math_min_m1ABE3D06A7BF422498FBF240CDCDC6D11273DD8B,
	math_max_mF7C9D30DE6384D5DE84917F51E4D4D8010460932,
	math_lerp_m373C36BD2152A1CE24359750ED1ABDE3FFE70000,
	math_lerp_mC3449FE2CBEB79190BF722AAAD9979F1AE61C6A2,
	math_lerp_m4E2C78F125D14EC7476C45708A206DD69C3B82A4,
	math_lerp_mB47979A95FE6478DCA26C327074491D61266C8AD,
	math_clamp_m1F61B5D20891B377EB3AB93B7432C023EA2A7514,
	math_abs_m21BB6D1489DC3A2AF7942244A41B39EF26031AB8,
	math_dot_mAE4F4CB702904786287A2C2A64E23D07E9AD4A6E,
	math_dot_mBD43287353A047EEFD1E728CB7A4E9C89A87778E,
	math_dot_m00E3616ACE8D9ACFEB722A2F9485BDD105B5AB95,
	math_floor_m1967459EF7EAB1EF62537F5F9DED7459797D20EA,
	math_ceil_m11AA9693F4C38B56E8BE3AB40900DD660A89B2F0,
	math_lengthsq_m0D2D7F7412B852564DC950DDE5261B089AB0DAA0,
	math_lengthsq_m1EACA00B994E33AE1D88A05A39A85BABB4B5A726,
	math_lengthsq_mD73F2E4C971CC3DC14C8605A3BE158195E53164C,
	math_distancesq_m58600816D7DB6E470C3DF697B4C0CE63134A6BD1,
	math_distancesq_m18F497591541BABA88E2448BC908ED5A3681970A,
	math_distancesq_m8292B092EC4B6AC609C92F29FF5B7AE8D9E7EE74,
	math_distancesq_mECB50A017D398A1DE8BA36240C75386A658BAC21,
	math_csum_m5FEF094CBBA9529BF38B1C5E9E37D61A355F4E6B,
	math_csum_mE551D40C1A80EF769E48359F0892146ADD596B15,
	math_csum_m2CEC45B544D5B9EFB714CC5325CB951CDC435753,
	math_uint2_m9BB58BBC62A71CFD2D3704FE44E37CEBE766767C,
	math_hash_m8295F93F93F0E5907F8F9DA098C4D38956B49DE9,
	math_uint3_m558D7B725E08B0063F9E311DACA5AB1693FA18D4,
	math_hash_m762A1D05870D2F55D9ECB11378D169B08BB04BB9,
	math_uint4_m677BF0973E67EDD7890759AAA4301CD886065F59,
	math_hash_m7A24547FED62DAF7EBE18E04CF867809F48F4C08,
	float2__ctor_mDEC8A4039E029926E0424F0FB614C7F679AE7449,
	float2_op_Multiply_mBD0A466E241FC2001B52D2851901C772538FCFD4,
	float2_op_Addition_m7BF3D8B765D963BA2EF831D444FF5A2149978D3C,
	float2_op_Subtraction_mABAE62700AB469FF2F150C2B3C2BC8927B36BCC4,
	float2_Equals_mD7E0010E86764F768D155F084C5049B1A37451AB,
	float2_Equals_m2B929D2B1750063ED9C8F71F517E707629A2865D,
	float2_GetHashCode_m14F0A1D75CEB912B8D368074F8EC66E768800FE9,
	float2_ToString_mDD9456E5C3F28889E650CED5533DCD06219F3A28,
	float2_ToString_mCCF46926AC2D48D3344DC9373B33F0E005071C78,
	float2_op_Implicit_mB136BDC4677B5FE6130CA815A737E7738546D119,
	float2_op_Implicit_mFE7C6B558CB9ACCDF08F4B7A37231E89F431623A,
	float3__ctor_mA1B0F2B6874F0DEDFC715C334892EB2FAB31B40A,
	float3_op_Multiply_m3FBEB6858C438A95F84FF3CC6B09D17396A8884E,
	float3_op_Addition_m43A3E7B9C99751652618541F0E6A04EE14570B68,
	float3_op_Subtraction_mE9924904DB860665A62840BED263EBEEDF70423F,
	float3_Equals_m21D80AD2B9F37B1210C6F36BE86F76AA527BF2CF,
	float3_Equals_m3C3B40E46D39C1FC39D250269E33EFC955235B2B,
	float3_GetHashCode_m127685CE1D48644B6DC250A3CA9B989A54BA1AF3,
	float3_ToString_mB8035CAC163C19E11A35ADFE448E5B362A56D8AF,
	float3_ToString_mAA3FEBFB0473D5C7F22CB3B6B56E52BEE6054F5F,
	float3_op_Implicit_mF3897B2F87EEB82C8D46016ADADD80B4279D9F8B,
	float3_op_Implicit_m52E7BAC4F651B42D13F635E5942FA9FD73F9084C,
	float4__ctor_mD7DD9759C791823116719CAE8EE693E9C173E241,
	float4_op_Multiply_mECCB11405184D4A1579030A1E7CB6CA41B267217,
	float4_op_Addition_mCE615C711B80C9F104005D53359304816E6CA051,
	float4_op_Subtraction_m33241A3298AFAADF2BEF985A5C70FAE5E70FB561,
	float4_Equals_mC17658EE14E0595E33E170A1CF5325AD7240B508,
	float4_Equals_m6247D066192E1714DDC2CC001D0D20D10D60228B,
	float4_GetHashCode_mDAE54F7570659BE4963BEA7F347A137B7EFC5230,
	float4_ToString_mF8E4242AD739E6BC3CFDAF96C26236D385DF7850,
	float4_ToString_m643BEBFFE6AE7B5E0366C777BEE1178924D5374B,
	float4_op_Implicit_m9E3CAA6CAB5E9EAF52037E2D3906C7DAC46624DB,
	float4_op_Implicit_m4DE78B67B42EE7149183D09445B6622CC30679C7,
	uint2__ctor_m15E587DF7FC128857586EA962472021625CB0727,
	uint2_op_Multiply_m064F9A426727F973DBE586E0210134B53C38E190,
	uint2_Equals_m4D6B7E58271563553964C88B0C6C11B11E9DDEFF,
	uint2_Equals_mDC5AF9975359EB05CA526D9D132038CC07BA610C,
	uint2_GetHashCode_m1AA42656371544BEFEF58990F0F5D3375771BDED,
	uint2_ToString_m11C8B65BDF3EE9F3E076886108F29C7085311CD8,
	uint2_ToString_m82A6C25A6E380499B47B6D45037EC327449303A1,
	uint3__ctor_m6000FC36B0AD34E3897EBB27D734326F63D5198E,
	uint3_op_Multiply_m2586E7297C34E588128E9A04495534B076A3A047,
	uint3_Equals_m13DA44E1022044FEBBA2066ECD1A8AC1CB53DF91,
	uint3_Equals_mAAD3238F59F9CA833513E94E33A329834CB205FE,
	uint3_GetHashCode_mD983DD667EC5C064E237BFA773C185AB36599B76,
	uint3_ToString_mEADE36DE83569B87E7DF9B37EB4A04357B83B277,
	uint3_ToString_mF1C800E07DAF3C5478742C64CD0C4C76C7E63167,
	uint4__ctor_mE2EF35B487246E995E6B5226C41B2ABF7D695852,
	uint4_op_Multiply_mF8519CAB503E1A8FF0AFF2084D591321C33CEA15,
	uint4_Equals_mA01BD6C642C7AA15938773343109B65A7017A8E5,
	uint4_Equals_mA36D75D93A16B63C63FF5556877325DE7209CA0C,
	uint4_GetHashCode_m0ACFBA8EEEBE72824B814A9147C62BA2DAD67E04,
	uint4_ToString_m88626950AE1516CF95F414CFC9D3328308CFFD21,
	uint4_ToString_m89A2419938888294462214E4F1B77BFB2A6D6622,
};
extern void float2__ctor_mDEC8A4039E029926E0424F0FB614C7F679AE7449_AdjustorThunk (void);
extern void float2_Equals_mD7E0010E86764F768D155F084C5049B1A37451AB_AdjustorThunk (void);
extern void float2_Equals_m2B929D2B1750063ED9C8F71F517E707629A2865D_AdjustorThunk (void);
extern void float2_GetHashCode_m14F0A1D75CEB912B8D368074F8EC66E768800FE9_AdjustorThunk (void);
extern void float2_ToString_mDD9456E5C3F28889E650CED5533DCD06219F3A28_AdjustorThunk (void);
extern void float2_ToString_mCCF46926AC2D48D3344DC9373B33F0E005071C78_AdjustorThunk (void);
extern void float3__ctor_mA1B0F2B6874F0DEDFC715C334892EB2FAB31B40A_AdjustorThunk (void);
extern void float3_Equals_m21D80AD2B9F37B1210C6F36BE86F76AA527BF2CF_AdjustorThunk (void);
extern void float3_Equals_m3C3B40E46D39C1FC39D250269E33EFC955235B2B_AdjustorThunk (void);
extern void float3_GetHashCode_m127685CE1D48644B6DC250A3CA9B989A54BA1AF3_AdjustorThunk (void);
extern void float3_ToString_mB8035CAC163C19E11A35ADFE448E5B362A56D8AF_AdjustorThunk (void);
extern void float3_ToString_mAA3FEBFB0473D5C7F22CB3B6B56E52BEE6054F5F_AdjustorThunk (void);
extern void float4__ctor_mD7DD9759C791823116719CAE8EE693E9C173E241_AdjustorThunk (void);
extern void float4_Equals_mC17658EE14E0595E33E170A1CF5325AD7240B508_AdjustorThunk (void);
extern void float4_Equals_m6247D066192E1714DDC2CC001D0D20D10D60228B_AdjustorThunk (void);
extern void float4_GetHashCode_mDAE54F7570659BE4963BEA7F347A137B7EFC5230_AdjustorThunk (void);
extern void float4_ToString_mF8E4242AD739E6BC3CFDAF96C26236D385DF7850_AdjustorThunk (void);
extern void float4_ToString_m643BEBFFE6AE7B5E0366C777BEE1178924D5374B_AdjustorThunk (void);
extern void uint2__ctor_m15E587DF7FC128857586EA962472021625CB0727_AdjustorThunk (void);
extern void uint2_Equals_m4D6B7E58271563553964C88B0C6C11B11E9DDEFF_AdjustorThunk (void);
extern void uint2_Equals_mDC5AF9975359EB05CA526D9D132038CC07BA610C_AdjustorThunk (void);
extern void uint2_GetHashCode_m1AA42656371544BEFEF58990F0F5D3375771BDED_AdjustorThunk (void);
extern void uint2_ToString_m11C8B65BDF3EE9F3E076886108F29C7085311CD8_AdjustorThunk (void);
extern void uint2_ToString_m82A6C25A6E380499B47B6D45037EC327449303A1_AdjustorThunk (void);
extern void uint3__ctor_m6000FC36B0AD34E3897EBB27D734326F63D5198E_AdjustorThunk (void);
extern void uint3_Equals_m13DA44E1022044FEBBA2066ECD1A8AC1CB53DF91_AdjustorThunk (void);
extern void uint3_Equals_mAAD3238F59F9CA833513E94E33A329834CB205FE_AdjustorThunk (void);
extern void uint3_GetHashCode_mD983DD667EC5C064E237BFA773C185AB36599B76_AdjustorThunk (void);
extern void uint3_ToString_mEADE36DE83569B87E7DF9B37EB4A04357B83B277_AdjustorThunk (void);
extern void uint3_ToString_mF1C800E07DAF3C5478742C64CD0C4C76C7E63167_AdjustorThunk (void);
extern void uint4__ctor_mE2EF35B487246E995E6B5226C41B2ABF7D695852_AdjustorThunk (void);
extern void uint4_Equals_mA01BD6C642C7AA15938773343109B65A7017A8E5_AdjustorThunk (void);
extern void uint4_Equals_mA36D75D93A16B63C63FF5556877325DE7209CA0C_AdjustorThunk (void);
extern void uint4_GetHashCode_m0ACFBA8EEEBE72824B814A9147C62BA2DAD67E04_AdjustorThunk (void);
extern void uint4_ToString_m88626950AE1516CF95F414CFC9D3328308CFFD21_AdjustorThunk (void);
extern void uint4_ToString_m89A2419938888294462214E4F1B77BFB2A6D6622_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[36] = 
{
	{ 0x06000029, float2__ctor_mDEC8A4039E029926E0424F0FB614C7F679AE7449_AdjustorThunk },
	{ 0x0600002D, float2_Equals_mD7E0010E86764F768D155F084C5049B1A37451AB_AdjustorThunk },
	{ 0x0600002E, float2_Equals_m2B929D2B1750063ED9C8F71F517E707629A2865D_AdjustorThunk },
	{ 0x0600002F, float2_GetHashCode_m14F0A1D75CEB912B8D368074F8EC66E768800FE9_AdjustorThunk },
	{ 0x06000030, float2_ToString_mDD9456E5C3F28889E650CED5533DCD06219F3A28_AdjustorThunk },
	{ 0x06000031, float2_ToString_mCCF46926AC2D48D3344DC9373B33F0E005071C78_AdjustorThunk },
	{ 0x06000034, float3__ctor_mA1B0F2B6874F0DEDFC715C334892EB2FAB31B40A_AdjustorThunk },
	{ 0x06000038, float3_Equals_m21D80AD2B9F37B1210C6F36BE86F76AA527BF2CF_AdjustorThunk },
	{ 0x06000039, float3_Equals_m3C3B40E46D39C1FC39D250269E33EFC955235B2B_AdjustorThunk },
	{ 0x0600003A, float3_GetHashCode_m127685CE1D48644B6DC250A3CA9B989A54BA1AF3_AdjustorThunk },
	{ 0x0600003B, float3_ToString_mB8035CAC163C19E11A35ADFE448E5B362A56D8AF_AdjustorThunk },
	{ 0x0600003C, float3_ToString_mAA3FEBFB0473D5C7F22CB3B6B56E52BEE6054F5F_AdjustorThunk },
	{ 0x0600003F, float4__ctor_mD7DD9759C791823116719CAE8EE693E9C173E241_AdjustorThunk },
	{ 0x06000043, float4_Equals_mC17658EE14E0595E33E170A1CF5325AD7240B508_AdjustorThunk },
	{ 0x06000044, float4_Equals_m6247D066192E1714DDC2CC001D0D20D10D60228B_AdjustorThunk },
	{ 0x06000045, float4_GetHashCode_mDAE54F7570659BE4963BEA7F347A137B7EFC5230_AdjustorThunk },
	{ 0x06000046, float4_ToString_mF8E4242AD739E6BC3CFDAF96C26236D385DF7850_AdjustorThunk },
	{ 0x06000047, float4_ToString_m643BEBFFE6AE7B5E0366C777BEE1178924D5374B_AdjustorThunk },
	{ 0x0600004A, uint2__ctor_m15E587DF7FC128857586EA962472021625CB0727_AdjustorThunk },
	{ 0x0600004C, uint2_Equals_m4D6B7E58271563553964C88B0C6C11B11E9DDEFF_AdjustorThunk },
	{ 0x0600004D, uint2_Equals_mDC5AF9975359EB05CA526D9D132038CC07BA610C_AdjustorThunk },
	{ 0x0600004E, uint2_GetHashCode_m1AA42656371544BEFEF58990F0F5D3375771BDED_AdjustorThunk },
	{ 0x0600004F, uint2_ToString_m11C8B65BDF3EE9F3E076886108F29C7085311CD8_AdjustorThunk },
	{ 0x06000050, uint2_ToString_m82A6C25A6E380499B47B6D45037EC327449303A1_AdjustorThunk },
	{ 0x06000051, uint3__ctor_m6000FC36B0AD34E3897EBB27D734326F63D5198E_AdjustorThunk },
	{ 0x06000053, uint3_Equals_m13DA44E1022044FEBBA2066ECD1A8AC1CB53DF91_AdjustorThunk },
	{ 0x06000054, uint3_Equals_mAAD3238F59F9CA833513E94E33A329834CB205FE_AdjustorThunk },
	{ 0x06000055, uint3_GetHashCode_mD983DD667EC5C064E237BFA773C185AB36599B76_AdjustorThunk },
	{ 0x06000056, uint3_ToString_mEADE36DE83569B87E7DF9B37EB4A04357B83B277_AdjustorThunk },
	{ 0x06000057, uint3_ToString_mF1C800E07DAF3C5478742C64CD0C4C76C7E63167_AdjustorThunk },
	{ 0x06000058, uint4__ctor_mE2EF35B487246E995E6B5226C41B2ABF7D695852_AdjustorThunk },
	{ 0x0600005A, uint4_Equals_mA01BD6C642C7AA15938773343109B65A7017A8E5_AdjustorThunk },
	{ 0x0600005B, uint4_Equals_mA36D75D93A16B63C63FF5556877325DE7209CA0C_AdjustorThunk },
	{ 0x0600005C, uint4_GetHashCode_m0ACFBA8EEEBE72824B814A9147C62BA2DAD67E04_AdjustorThunk },
	{ 0x0600005D, uint4_ToString_m88626950AE1516CF95F414CFC9D3328308CFFD21_AdjustorThunk },
	{ 0x0600005E, uint4_ToString_m89A2419938888294462214E4F1B77BFB2A6D6622_AdjustorThunk },
};
static const int32_t s_InvokerIndices[94] = 
{
	5151,
	7650,
	7651,
	7652,
	7434,
	7644,
	7734,
	7735,
	7736,
	7603,
	7610,
	7013,
	7013,
	6434,
	6647,
	6648,
	6649,
	6434,
	7608,
	7017,
	7018,
	7019,
	7608,
	7608,
	7614,
	7615,
	7616,
	7013,
	7017,
	7018,
	7019,
	7653,
	7654,
	7655,
	7206,
	7653,
	6650,
	7654,
	6159,
	7655,
	2484,
	7198,
	7199,
	7199,
	3069,
	2993,
	5045,
	5077,
	1939,
	7683,
	7731,
	1535,
	7201,
	7202,
	7202,
	3070,
	2993,
	5045,
	5077,
	1939,
	7692,
	7732,
	1031,
	7204,
	7205,
	7205,
	3071,
	2993,
	5045,
	5077,
	1939,
	7733,
	7699,
	2499,
	7207,
	3073,
	2993,
	5045,
	5077,
	1939,
	1542,
	7208,
	3074,
	2993,
	5045,
	5077,
	1939,
	1038,
	7209,
	3075,
	2993,
	5045,
	5077,
	1939,
};
extern const CustomAttributesCacheGenerator g_Unity_Mathematics_AttributeGenerators[];
static TypeDefinitionIndex s_staticConstructorsToRunAtStartup[8] = 
{
	4153,
	4155,
	4157,
	4159,
	4161,
	4163,
	4165,
	0,
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_Unity_Mathematics_CodeGenModule;
const Il2CppCodeGenModule g_Unity_Mathematics_CodeGenModule = 
{
	"Unity.Mathematics.dll",
	94,
	s_methodPointers,
	36,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_Unity_Mathematics_AttributeGenerators,
	NULL, // module initializer,
	s_staticConstructorsToRunAtStartup,
	NULL,
	NULL,
};
