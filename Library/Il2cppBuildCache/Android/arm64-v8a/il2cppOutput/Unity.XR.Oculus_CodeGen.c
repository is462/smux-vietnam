﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void Unity.XR.Oculus.Utils::SetColorScaleAndOffset(UnityEngine.Vector4,UnityEngine.Vector4)
extern void Utils_SetColorScaleAndOffset_m6434F29CDE9823397828ECE9F9A2817D44D48D84 (void);
// 0x00000002 Unity.XR.Oculus.SystemHeadset Unity.XR.Oculus.Utils::GetSystemHeadsetType()
extern void Utils_GetSystemHeadsetType_m383047E3555F08E216DD00B53BFE5F3C016FAFE9 (void);
// 0x00000003 System.Void Unity.XR.Oculus.Utils::SetFoveationLevel(System.Int32)
extern void Utils_SetFoveationLevel_m16335000BA8172A666725F27407B2FEC32EB43D0 (void);
// 0x00000004 System.Boolean Unity.XR.Oculus.Utils::EnableDynamicFFR(System.Boolean)
extern void Utils_EnableDynamicFFR_mAFC73962AAB99074606ED7D65ECC9D35B41C430A (void);
// 0x00000005 System.Int32 Unity.XR.Oculus.Utils::GetFoveationLevel()
extern void Utils_GetFoveationLevel_m922BB229AFE1AA65CDD41B454CCD930F0D93522B (void);
// 0x00000006 System.Void Unity.XR.Oculus.InputFocus::add_InputFocusAcquired(System.Action)
extern void InputFocus_add_InputFocusAcquired_m792AC75AEC3F732433E7490A43EDD5ED153F6094 (void);
// 0x00000007 System.Void Unity.XR.Oculus.InputFocus::remove_InputFocusAcquired(System.Action)
extern void InputFocus_remove_InputFocusAcquired_m19273BB66624D67CE968DEC379569ECA0AA7D43A (void);
// 0x00000008 System.Void Unity.XR.Oculus.InputFocus::add_InputFocusLost(System.Action)
extern void InputFocus_add_InputFocusLost_m11782A9ADE16C0FE65636052AEEACA5BF4BE07C1 (void);
// 0x00000009 System.Void Unity.XR.Oculus.InputFocus::remove_InputFocusLost(System.Action)
extern void InputFocus_remove_InputFocusLost_m381E9CF962F5C1CBB6E737334ED070A356C02582 (void);
// 0x0000000A System.Boolean Unity.XR.Oculus.InputFocus::get_hasInputFocus()
extern void InputFocus_get_hasInputFocus_m89BC1D49BF5D1BD9207D5D32780FA93CEC6AD793 (void);
// 0x0000000B System.Void Unity.XR.Oculus.InputFocus::Update()
extern void InputFocus_Update_mA29F59D3148E351431EF53DEEE921320DC66945E (void);
// 0x0000000C System.Void Unity.XR.Oculus.InputFocus::.ctor()
extern void InputFocus__ctor_m3EC50CAA3520F2BB04B7258680BD4B4081B9C9F3 (void);
// 0x0000000D System.Void Unity.XR.Oculus.InputFocus::.cctor()
extern void InputFocus__cctor_mB6300AFCE681F96FFBB24D1AAB76E815716B8C95 (void);
// 0x0000000E System.Boolean Unity.XR.Oculus.Boundary::GetBoundaryConfigured()
extern void Boundary_GetBoundaryConfigured_m3084C1D2ED0E1456840215C4D022838D34A51CAB (void);
// 0x0000000F System.Boolean Unity.XR.Oculus.Boundary::GetBoundaryDimensions(Unity.XR.Oculus.Boundary/BoundaryType,UnityEngine.Vector3&)
extern void Boundary_GetBoundaryDimensions_m687BD292B02EC436F995EECDF128F88366575FE1 (void);
// 0x00000010 System.Boolean Unity.XR.Oculus.Boundary::GetBoundaryVisible()
extern void Boundary_GetBoundaryVisible_m29E7D20DA141AA1666466B54833DFC1990338C69 (void);
// 0x00000011 System.Void Unity.XR.Oculus.Boundary::SetBoundaryVisible(System.Boolean)
extern void Boundary_SetBoundaryVisible_mAE47C1076E3C28BB7A80CB3169C3DA411BEB347C (void);
// 0x00000012 System.Void Unity.XR.Oculus.Development::TrySetDeveloperMode(System.Boolean)
extern void Development_TrySetDeveloperMode_m42C081BDF42482D60F1FE1FCDEB1D4CA9E72B320 (void);
// 0x00000013 System.Void Unity.XR.Oculus.Development::OverrideDeveloperModeStart()
extern void Development_OverrideDeveloperModeStart_mC9E24212121397B0C37A27904F5B95B4FD871C88 (void);
// 0x00000014 System.Void Unity.XR.Oculus.Development::OverrideDeveloperModeStop()
extern void Development_OverrideDeveloperModeStop_mF62FEF98A60806E453F85B7419FE8A726A54A621 (void);
// 0x00000015 System.Void Unity.XR.Oculus.Development::.cctor()
extern void Development__cctor_mBFD72BE5D5819D1EEBCB7D11CCD064B26AC4518C (void);
// 0x00000016 System.Void Unity.XR.Oculus.InputLayoutLoader::.cctor()
extern void InputLayoutLoader__cctor_m7BFAE3F72F69BA6A7FA77F14FFD935C18DF5530D (void);
// 0x00000017 System.Void Unity.XR.Oculus.InputLayoutLoader::RegisterInputLayouts()
extern void InputLayoutLoader_RegisterInputLayouts_m7D259AB887FFDEB3E4FA2F7D991F498BDC00DE15 (void);
// 0x00000018 Unity.XR.Oculus.OculusLoader/DeviceSupportedResult Unity.XR.Oculus.OculusLoader::IsDeviceSupported()
extern void OculusLoader_IsDeviceSupported_mC2E5A317811DBD9EB506037FD358CF7F64814286 (void);
// 0x00000019 UnityEngine.XR.XRDisplaySubsystem Unity.XR.Oculus.OculusLoader::get_displaySubsystem()
extern void OculusLoader_get_displaySubsystem_mE2C42E00522835401105B4A163E0768AA433E71A (void);
// 0x0000001A UnityEngine.XR.XRInputSubsystem Unity.XR.Oculus.OculusLoader::get_inputSubsystem()
extern void OculusLoader_get_inputSubsystem_m8E87DAAF3BB44B2F3945232518EAF6862E9D1B95 (void);
// 0x0000001B System.Boolean Unity.XR.Oculus.OculusLoader::Initialize()
extern void OculusLoader_Initialize_m9F45EA7F9134BED3F23133D3B2A3467CF51B5561 (void);
// 0x0000001C System.Boolean Unity.XR.Oculus.OculusLoader::Start()
extern void OculusLoader_Start_m48219B334C6D430348D0A6CE4B0BB8E9493E29F5 (void);
// 0x0000001D System.Boolean Unity.XR.Oculus.OculusLoader::Stop()
extern void OculusLoader_Stop_m096A1F5907CFA0315161CDE8C28F889CC1709482 (void);
// 0x0000001E System.Boolean Unity.XR.Oculus.OculusLoader::Deinitialize()
extern void OculusLoader_Deinitialize_m1DBBE6B4710416D0793C6869DC3C17656431599F (void);
// 0x0000001F System.Void Unity.XR.Oculus.OculusLoader::RuntimeLoadOVRPlugin()
extern void OculusLoader_RuntimeLoadOVRPlugin_m2CA81B2C2F621A6BEF5495DF46AE5B2E2F04C042 (void);
// 0x00000020 Unity.XR.Oculus.OculusSettings Unity.XR.Oculus.OculusLoader::GetSettings()
extern void OculusLoader_GetSettings_mF0D5694BD84372039D4227997B00CAB33A419CAC (void);
// 0x00000021 System.Void Unity.XR.Oculus.OculusLoader::.ctor()
extern void OculusLoader__ctor_mF6A80167D63792923ADC3AB04D39BC1D0B3EB360 (void);
// 0x00000022 System.Void Unity.XR.Oculus.OculusLoader::.cctor()
extern void OculusLoader__cctor_mD99F1A4C2C8FA6EE03E450E22062EDC2AEF809BE (void);
// 0x00000023 System.Boolean Unity.XR.Oculus.Performance::TrySetCPULevel(System.Int32)
extern void Performance_TrySetCPULevel_m168A76958F71C27D5A17A346F699F874CD119D39 (void);
// 0x00000024 System.Boolean Unity.XR.Oculus.Performance::TrySetGPULevel(System.Int32)
extern void Performance_TrySetGPULevel_mE13008C6066A522BAEFE7B171A51AB78BDD81EDC (void);
// 0x00000025 System.Boolean Unity.XR.Oculus.Performance::TryGetAvailableDisplayRefreshRates(System.Single[]&)
extern void Performance_TryGetAvailableDisplayRefreshRates_m6322292C481D9F84FF1A56DCC33897047FF0AA42 (void);
// 0x00000026 System.Boolean Unity.XR.Oculus.Performance::TrySetDisplayRefreshRate(System.Single)
extern void Performance_TrySetDisplayRefreshRate_m08D4B41CCE153758A990FE1D45E90344B3AFB583 (void);
// 0x00000027 System.Boolean Unity.XR.Oculus.Performance::TryGetDisplayRefreshRate(System.Single&)
extern void Performance_TryGetDisplayRefreshRate_mE352668E6C1C1FF8B6184F23FF31C047B8F34C45 (void);
// 0x00000028 System.Void Unity.XR.Oculus.Performance::.cctor()
extern void Performance__cctor_m01F738B7F7C2CC8CFBFA44910D5BF0ABB33F820B (void);
// 0x00000029 System.String Unity.XR.Oculus.Stats::get_PluginVersion()
extern void Stats_get_PluginVersion_m66F99A5B04F4CADEE440E825BF1A29E927363674 (void);
// 0x0000002A UnityEngine.IntegratedSubsystem Unity.XR.Oculus.Stats::GetOculusDisplaySubsystem()
extern void Stats_GetOculusDisplaySubsystem_mC67658F860743C9A2AFA66A9C70A41446B309912 (void);
// 0x0000002B System.Void Unity.XR.Oculus.Stats::.ctor()
extern void Stats__ctor_mA96DB2D702AAB276E41CD7C005AB64B599C0B63C (void);
// 0x0000002C System.Void Unity.XR.Oculus.Stats::.cctor()
extern void Stats__cctor_mC93F4550849911809F978CF11798655CAAB0E528 (void);
// 0x0000002D System.Single Unity.XR.Oculus.Stats/AdaptivePerformance::get_GPUAppTime()
extern void AdaptivePerformance_get_GPUAppTime_mFEEFEEDF7634F4064C690F07B1DB26825692A238 (void);
// 0x0000002E System.Single Unity.XR.Oculus.Stats/AdaptivePerformance::get_GPUCompositorTime()
extern void AdaptivePerformance_get_GPUCompositorTime_mE88C5D2A9508EC775678D4AAFE172D644DD3B27E (void);
// 0x0000002F System.Single Unity.XR.Oculus.Stats/AdaptivePerformance::get_MotionToPhoton()
extern void AdaptivePerformance_get_MotionToPhoton_m80740922FD30B202610BDB102AD95D46D3FB9F7C (void);
// 0x00000030 System.Single Unity.XR.Oculus.Stats/AdaptivePerformance::get_RefreshRate()
extern void AdaptivePerformance_get_RefreshRate_mF58D1D911B6983F6C8E04DF0FD8D31863842F3D5 (void);
// 0x00000031 System.Single Unity.XR.Oculus.Stats/AdaptivePerformance::get_BatteryTemp()
extern void AdaptivePerformance_get_BatteryTemp_m47FB27D2D8E551DC256CDB9616F2142D2DEB2175 (void);
// 0x00000032 System.Single Unity.XR.Oculus.Stats/AdaptivePerformance::get_BatteryLevel()
extern void AdaptivePerformance_get_BatteryLevel_mAA3234732E5829EA40C79610386C23B00D5DF07C (void);
// 0x00000033 System.Boolean Unity.XR.Oculus.Stats/AdaptivePerformance::get_PowerSavingMode()
extern void AdaptivePerformance_get_PowerSavingMode_m99226E3F46426919FA68C75216FA229A8F7F9851 (void);
// 0x00000034 System.Single Unity.XR.Oculus.Stats/AdaptivePerformance::get_AdaptivePerformanceScale()
extern void AdaptivePerformance_get_AdaptivePerformanceScale_m9947FB5DB23E0092A71D3E08A775082FEC71B43E (void);
// 0x00000035 System.Int32 Unity.XR.Oculus.Stats/AdaptivePerformance::get_CPULevel()
extern void AdaptivePerformance_get_CPULevel_m1AD7E50E684841C20C52A92B65C516E112960847 (void);
// 0x00000036 System.Int32 Unity.XR.Oculus.Stats/AdaptivePerformance::get_GPULevel()
extern void AdaptivePerformance_get_GPULevel_mBEFC052317F45BB89DC42293713AD696D24CBF2A (void);
// 0x00000037 System.Single Unity.XR.Oculus.Stats/PerfMetrics::get_AppCPUTime()
extern void PerfMetrics_get_AppCPUTime_mE53A84B8202BCA2E221EA0297D6F4C067434344D (void);
// 0x00000038 System.Single Unity.XR.Oculus.Stats/PerfMetrics::get_AppGPUTime()
extern void PerfMetrics_get_AppGPUTime_m6055F2F6B1F987492D42B376AAF23F7DAB2CA9F9 (void);
// 0x00000039 System.Single Unity.XR.Oculus.Stats/PerfMetrics::get_CompositorCPUTime()
extern void PerfMetrics_get_CompositorCPUTime_mA25DE33A0AE7CE98363AD9CC589A275390602982 (void);
// 0x0000003A System.Single Unity.XR.Oculus.Stats/PerfMetrics::get_CompositorGPUTime()
extern void PerfMetrics_get_CompositorGPUTime_m42BD7C18C2BE1ABBAAEB0BA3A832A54C4B57478A (void);
// 0x0000003B System.Single Unity.XR.Oculus.Stats/PerfMetrics::get_GPUUtilization()
extern void PerfMetrics_get_GPUUtilization_m1D6176628FCB67FE5C1F4665D1BA7FB608184E4C (void);
// 0x0000003C System.Single Unity.XR.Oculus.Stats/PerfMetrics::get_CPUUtilizationAverage()
extern void PerfMetrics_get_CPUUtilizationAverage_mC7D22DDB555F09C483D19D7D6CA9EF911D8E0A8E (void);
// 0x0000003D System.Single Unity.XR.Oculus.Stats/PerfMetrics::get_CPUUtilizationWorst()
extern void PerfMetrics_get_CPUUtilizationWorst_m2F112911B6C7BB7A2D684B01E41292857D9E54FD (void);
// 0x0000003E System.Single Unity.XR.Oculus.Stats/PerfMetrics::get_CPUClockFrequency()
extern void PerfMetrics_get_CPUClockFrequency_m6A41A80961206C86DC37E23869EC04A4B506AC4B (void);
// 0x0000003F System.Single Unity.XR.Oculus.Stats/PerfMetrics::get_GPUClockFrequency()
extern void PerfMetrics_get_GPUClockFrequency_mE67C9D0558582243809FDFD11CEDBCE4B9C709E3 (void);
// 0x00000040 System.Void Unity.XR.Oculus.Stats/PerfMetrics::EnablePerfMetrics(System.Boolean)
extern void PerfMetrics_EnablePerfMetrics_m6DCAC8F8AAC829B357E7DA4876D7377FB3F45FAD (void);
// 0x00000041 System.Single Unity.XR.Oculus.Stats/AppMetrics::get_AppQueueAheadTime()
extern void AppMetrics_get_AppQueueAheadTime_m6269E5088701FEB294099FA3B9411C7E631529EE (void);
// 0x00000042 System.Single Unity.XR.Oculus.Stats/AppMetrics::get_AppCPUElapsedTime()
extern void AppMetrics_get_AppCPUElapsedTime_m27DCDA440E31783E74022FE2C088959ABA79CDA2 (void);
// 0x00000043 System.Single Unity.XR.Oculus.Stats/AppMetrics::get_CompositorDroppedFrames()
extern void AppMetrics_get_CompositorDroppedFrames_m76EAF296BC146D237038A450AE6616C2AD3E7547 (void);
// 0x00000044 System.Single Unity.XR.Oculus.Stats/AppMetrics::get_CompositorLatency()
extern void AppMetrics_get_CompositorLatency_m0C0237CDA7D800BF3151C8309B50F03ED194CEEA (void);
// 0x00000045 System.Single Unity.XR.Oculus.Stats/AppMetrics::get_CompositorCPUTime()
extern void AppMetrics_get_CompositorCPUTime_m75C30FD99DB6696C222044E0E4102007C15FB253 (void);
// 0x00000046 System.Single Unity.XR.Oculus.Stats/AppMetrics::get_CPUStartToGPUEnd()
extern void AppMetrics_get_CPUStartToGPUEnd_mA294A8E4B82FEA622DA1E042C70385655D45BD44 (void);
// 0x00000047 System.Single Unity.XR.Oculus.Stats/AppMetrics::get_GPUEndToVsync()
extern void AppMetrics_get_GPUEndToVsync_m38FA875E11D5A9E5C377C09C60030243E3872B6D (void);
// 0x00000048 System.Void Unity.XR.Oculus.Stats/AppMetrics::EnableAppMetrics(System.Boolean)
extern void AppMetrics_EnableAppMetrics_m3D99B6DC2574C718873D2DC9941FDD14049F6779 (void);
// 0x00000049 System.Void Unity.XR.Oculus.NativeMethods::SetColorScale(System.Single,System.Single,System.Single,System.Single)
extern void NativeMethods_SetColorScale_mABAA1FBFC163D8DE94AD47D7B8F848B78CD5E9A0 (void);
// 0x0000004A System.Void Unity.XR.Oculus.NativeMethods::SetColorOffset(System.Single,System.Single,System.Single,System.Single)
extern void NativeMethods_SetColorOffset_m62E7000E8A015D685D76D915941FDE958732213D (void);
// 0x0000004B System.Boolean Unity.XR.Oculus.NativeMethods::GetIsSupportedDevice()
extern void NativeMethods_GetIsSupportedDevice_mFC86118E0FC74ABE30C84639472ADE492083AFD2 (void);
// 0x0000004C System.Boolean Unity.XR.Oculus.NativeMethods::LoadOVRPlugin(System.String)
extern void NativeMethods_LoadOVRPlugin_m4D4C796D2F76D0CA13969C61603C01B0A26478E7 (void);
// 0x0000004D System.Void Unity.XR.Oculus.NativeMethods::UnloadOVRPlugin()
extern void NativeMethods_UnloadOVRPlugin_mF9B47E517AB3E99DA9AE3E712054150C04D7D7DA (void);
// 0x0000004E System.Void Unity.XR.Oculus.NativeMethods::SetUserDefinedSettings(Unity.XR.Oculus.NativeMethods/UserDefinedSettings)
extern void NativeMethods_SetUserDefinedSettings_m60831E50BCEB63E8B8E3A6421CE2D6BC21FB6FCD (void);
// 0x0000004F System.Int32 Unity.XR.Oculus.NativeMethods::SetCPULevel(System.Int32)
extern void NativeMethods_SetCPULevel_mABD8BE1F1CE41DC683BA43822DB456530FD6D929 (void);
// 0x00000050 System.Int32 Unity.XR.Oculus.NativeMethods::SetGPULevel(System.Int32)
extern void NativeMethods_SetGPULevel_m055FCE766C4C218F61A06ACA963F5ED637834DF7 (void);
// 0x00000051 System.Void Unity.XR.Oculus.NativeMethods::GetOVRPVersion(System.Byte[])
extern void NativeMethods_GetOVRPVersion_mB328CD13DA707D9B1B26A3EBC82A9F364F140BE9 (void);
// 0x00000052 System.Void Unity.XR.Oculus.NativeMethods::EnablePerfMetrics(System.Boolean)
extern void NativeMethods_EnablePerfMetrics_mB9E9467765E2141254EAC993FF87E6595831DC74 (void);
// 0x00000053 System.Void Unity.XR.Oculus.NativeMethods::EnableAppMetrics(System.Boolean)
extern void NativeMethods_EnableAppMetrics_mBFF69EB9C54389B25A9FC271819E9B3FB2A6CDB6 (void);
// 0x00000054 System.Boolean Unity.XR.Oculus.NativeMethods::SetDeveloperModeStrict(System.Boolean)
extern void NativeMethods_SetDeveloperModeStrict_mB7F4A67FF3C6396D8600290B3BBA555A6CF56767 (void);
// 0x00000055 System.Boolean Unity.XR.Oculus.NativeMethods::GetHasInputFocus()
extern void NativeMethods_GetHasInputFocus_mC5174AB5E09FCDEC72E3FE70EE041824A3328806 (void);
// 0x00000056 System.Boolean Unity.XR.Oculus.NativeMethods::GetBoundaryConfigured()
extern void NativeMethods_GetBoundaryConfigured_m63425B4A401546727492B96CEDDBDF636499D961 (void);
// 0x00000057 System.Boolean Unity.XR.Oculus.NativeMethods::GetBoundaryDimensions(Unity.XR.Oculus.Boundary/BoundaryType,UnityEngine.Vector3&)
extern void NativeMethods_GetBoundaryDimensions_m32B73039BE0E747DAFB358D71514D5E7C75153A6 (void);
// 0x00000058 System.Boolean Unity.XR.Oculus.NativeMethods::GetBoundaryVisible()
extern void NativeMethods_GetBoundaryVisible_m274038DDF26F9FE120C68CD993C31D751C18B369 (void);
// 0x00000059 System.Void Unity.XR.Oculus.NativeMethods::SetBoundaryVisible(System.Boolean)
extern void NativeMethods_SetBoundaryVisible_mC4CAB764018040FFB39B8034D86B8E282DB73FF1 (void);
// 0x0000005A System.Boolean Unity.XR.Oculus.NativeMethods::GetAppShouldQuit()
extern void NativeMethods_GetAppShouldQuit_m4A8AC6995227A1AA4F5EF087051DA57735D0C486 (void);
// 0x0000005B System.Boolean Unity.XR.Oculus.NativeMethods::GetDisplayAvailableFrequencies(System.IntPtr,System.Int32&)
extern void NativeMethods_GetDisplayAvailableFrequencies_m2E4C861D87369CCA6A40AAFF6EA5CF65093D4F39 (void);
// 0x0000005C System.Boolean Unity.XR.Oculus.NativeMethods::SetDisplayFrequency(System.Single)
extern void NativeMethods_SetDisplayFrequency_m0C913002249CA6C2FDAD4AB57A0957D4F5F8646C (void);
// 0x0000005D System.Boolean Unity.XR.Oculus.NativeMethods::GetDisplayFrequency(System.Single&)
extern void NativeMethods_GetDisplayFrequency_m5D89A6D7DF13E557DCF74B65770B5945A2C327BE (void);
// 0x0000005E Unity.XR.Oculus.SystemHeadset Unity.XR.Oculus.NativeMethods::GetSystemHeadsetType()
extern void NativeMethods_GetSystemHeadsetType_m5D2B6BFDE86FE8DB56A48641E1EA3B3AC788C0AE (void);
// 0x0000005F System.Boolean Unity.XR.Oculus.NativeMethods::GetTiledMultiResSupported()
extern void NativeMethods_GetTiledMultiResSupported_mA3B09B2A1F0299A3AAD86C0DEB36625D598CD871 (void);
// 0x00000060 System.Void Unity.XR.Oculus.NativeMethods::SetTiledMultiResLevel(System.Int32)
extern void NativeMethods_SetTiledMultiResLevel_mB738658807A7B692985B9CD55A0AEE8AAE58FF2D (void);
// 0x00000061 System.Int32 Unity.XR.Oculus.NativeMethods::GetTiledMultiResLevel()
extern void NativeMethods_GetTiledMultiResLevel_m8082A03420962D025A24554936BC8802D190F3F0 (void);
// 0x00000062 System.Void Unity.XR.Oculus.NativeMethods::SetTiledMultiResDynamic(System.Boolean)
extern void NativeMethods_SetTiledMultiResDynamic_mC79DF59B055FA02F3605881A38984454869DA66B (void);
// 0x00000063 System.Boolean Unity.XR.Oculus.NativeMethods::GetShouldRestartSession()
extern void NativeMethods_GetShouldRestartSession_m691BE303740AD0161244C7D02838D9A1397D8EA1 (void);
// 0x00000064 System.Void Unity.XR.Oculus.NativeMethods/Internal::SetColorScale(System.Single,System.Single,System.Single,System.Single)
extern void Internal_SetColorScale_mA8ED134FCC56CCD16F92734ADCBF2526342EB02C (void);
// 0x00000065 System.Void Unity.XR.Oculus.NativeMethods/Internal::SetColorOffset(System.Single,System.Single,System.Single,System.Single)
extern void Internal_SetColorOffset_m6765A7EC8EFFDB061AE48B3F53658BBD3FC26FBF (void);
// 0x00000066 System.Boolean Unity.XR.Oculus.NativeMethods/Internal::GetIsSupportedDevice()
extern void Internal_GetIsSupportedDevice_mA3EF73A5F9CA1AD2B6E9D8F1F560A62D14A19E92 (void);
// 0x00000067 System.Boolean Unity.XR.Oculus.NativeMethods/Internal::LoadOVRPlugin(System.String)
extern void Internal_LoadOVRPlugin_m20BF6ADFC47F043ED9CB9F86199E26C8A46AD5AF (void);
// 0x00000068 System.Void Unity.XR.Oculus.NativeMethods/Internal::UnloadOVRPlugin()
extern void Internal_UnloadOVRPlugin_m6C683304420498120BF38390A1129A5B01A67F1E (void);
// 0x00000069 System.Void Unity.XR.Oculus.NativeMethods/Internal::SetUserDefinedSettings(Unity.XR.Oculus.NativeMethods/UserDefinedSettings)
extern void Internal_SetUserDefinedSettings_mE187F03B58FEB8FE874B938E28401C06C919221E (void);
// 0x0000006A System.Int32 Unity.XR.Oculus.NativeMethods/Internal::SetCPULevel(System.Int32)
extern void Internal_SetCPULevel_m0568DA61DDC47E9F28D26C52EA4035A6A139CCA8 (void);
// 0x0000006B System.Int32 Unity.XR.Oculus.NativeMethods/Internal::SetGPULevel(System.Int32)
extern void Internal_SetGPULevel_mBC46AC72882E2FFD4358F5812B8F45329C62843A (void);
// 0x0000006C System.Void Unity.XR.Oculus.NativeMethods/Internal::GetOVRPVersion(System.Byte[])
extern void Internal_GetOVRPVersion_m1E2BB4B4CDC04F9DAD64DF67D3CFC76085AF305B (void);
// 0x0000006D System.Void Unity.XR.Oculus.NativeMethods/Internal::EnablePerfMetrics(System.Boolean)
extern void Internal_EnablePerfMetrics_m2572EAFC2373A4E10168EDD76D576A44212466D4 (void);
// 0x0000006E System.Void Unity.XR.Oculus.NativeMethods/Internal::EnableAppMetrics(System.Boolean)
extern void Internal_EnableAppMetrics_m327F6E0F90F1A962D000C105EFE540CD43E78D70 (void);
// 0x0000006F System.Boolean Unity.XR.Oculus.NativeMethods/Internal::SetDeveloperModeStrict(System.Boolean)
extern void Internal_SetDeveloperModeStrict_m0A2121361A4FFDA8D0EF716D2C011F0EF19A34F8 (void);
// 0x00000070 System.Boolean Unity.XR.Oculus.NativeMethods/Internal::GetAppHasInputFocus()
extern void Internal_GetAppHasInputFocus_mA679379FDE6F3AFAFBF35841818A0BD5A492579F (void);
// 0x00000071 System.Boolean Unity.XR.Oculus.NativeMethods/Internal::GetBoundaryConfigured()
extern void Internal_GetBoundaryConfigured_m38A8FB6A7F2DCE7882E9DF3707530E40D85E6492 (void);
// 0x00000072 System.Boolean Unity.XR.Oculus.NativeMethods/Internal::GetBoundaryDimensions(Unity.XR.Oculus.Boundary/BoundaryType,UnityEngine.Vector3&)
extern void Internal_GetBoundaryDimensions_m07ED6D4A7E3C368766B953282AE8FA76A257849B (void);
// 0x00000073 System.Boolean Unity.XR.Oculus.NativeMethods/Internal::GetBoundaryVisible()
extern void Internal_GetBoundaryVisible_mFFBE7E9B928B930A6C4B1F8F7391F3403CD9F72F (void);
// 0x00000074 System.Void Unity.XR.Oculus.NativeMethods/Internal::SetBoundaryVisible(System.Boolean)
extern void Internal_SetBoundaryVisible_m3F762D66ED2E7998D8EDD6B065398796C32B2139 (void);
// 0x00000075 System.Boolean Unity.XR.Oculus.NativeMethods/Internal::GetAppShouldQuit()
extern void Internal_GetAppShouldQuit_m2201DC6D753AE50E8E041C75816721CC622DA353 (void);
// 0x00000076 System.Boolean Unity.XR.Oculus.NativeMethods/Internal::GetDisplayAvailableFrequencies(System.IntPtr,System.Int32&)
extern void Internal_GetDisplayAvailableFrequencies_mA6A02093DF72E979557B913FEE19BA4B9CF61159 (void);
// 0x00000077 System.Boolean Unity.XR.Oculus.NativeMethods/Internal::SetDisplayFrequency(System.Single)
extern void Internal_SetDisplayFrequency_m3221766C0E2DF541A57A44508194B245E038A965 (void);
// 0x00000078 System.Boolean Unity.XR.Oculus.NativeMethods/Internal::GetDisplayFrequency(System.Single&)
extern void Internal_GetDisplayFrequency_m3264CC851EAF260CA1EFD7E4984362B1B7BC8676 (void);
// 0x00000079 Unity.XR.Oculus.SystemHeadset Unity.XR.Oculus.NativeMethods/Internal::GetSystemHeadsetType()
extern void Internal_GetSystemHeadsetType_mC0537BB4F5EA3EDD3993C7A05487B04A0FADAEFB (void);
// 0x0000007A System.Boolean Unity.XR.Oculus.NativeMethods/Internal::GetTiledMultiResSupported()
extern void Internal_GetTiledMultiResSupported_mEE711350D975E6C4FD4D0634AEDD64F3FEF2E0BB (void);
// 0x0000007B System.Void Unity.XR.Oculus.NativeMethods/Internal::SetTiledMultiResLevel(System.Int32)
extern void Internal_SetTiledMultiResLevel_mEEC4003CC9AEA6156EE976678E3409FCABE6EDD2 (void);
// 0x0000007C System.Int32 Unity.XR.Oculus.NativeMethods/Internal::GetTiledMultiResLevel()
extern void Internal_GetTiledMultiResLevel_m2BC392E4824622E98F610EC02CB4A8196FEF96CE (void);
// 0x0000007D System.Void Unity.XR.Oculus.NativeMethods/Internal::SetTiledMultiResDynamic(System.Boolean)
extern void Internal_SetTiledMultiResDynamic_m037817F3884FC015AC28AB27520A40B3511D2A10 (void);
// 0x0000007E System.Boolean Unity.XR.Oculus.NativeMethods/Internal::GetShouldRestartSession()
extern void Internal_GetShouldRestartSession_m232B73B6EB98D8E93F361FFBEDC386E93CD86E1E (void);
// 0x0000007F System.Void Unity.XR.Oculus.OculusRestarter::.cctor()
extern void OculusRestarter__cctor_m050787ED25CC19F82E8607F6A3DD74B6A92E7D57 (void);
// 0x00000080 System.Void Unity.XR.Oculus.OculusRestarter::ResetCallbacks()
extern void OculusRestarter_ResetCallbacks_mDFC9B5CDAF74BFE121D4F813709A09DF60E75CC3 (void);
// 0x00000081 System.Boolean Unity.XR.Oculus.OculusRestarter::get_isRunning()
extern void OculusRestarter_get_isRunning_mD3558955217FE9FEC976D5AE772617EBEB44E25E (void);
// 0x00000082 System.Single Unity.XR.Oculus.OculusRestarter::get_TimeBetweenRestartAttempts()
extern void OculusRestarter_get_TimeBetweenRestartAttempts_m4643B819097BB5929C5EA5AAC0BBD798817B2465 (void);
// 0x00000083 System.Void Unity.XR.Oculus.OculusRestarter::set_TimeBetweenRestartAttempts(System.Single)
extern void OculusRestarter_set_TimeBetweenRestartAttempts_m3FDA87CEAC42CF6AF6521DFF3E4EB1CC0BC9454F (void);
// 0x00000084 System.Int32 Unity.XR.Oculus.OculusRestarter::get_PauseAndRestartAttempts()
extern void OculusRestarter_get_PauseAndRestartAttempts_m5073862450728EBD76EDC01F37A6CCFEA52F0AA9 (void);
// 0x00000085 Unity.XR.Oculus.OculusRestarter Unity.XR.Oculus.OculusRestarter::get_Instance()
extern void OculusRestarter_get_Instance_m39A581FAA5C8E45A6603F7FA0EA6CAD784887796 (void);
// 0x00000086 System.Void Unity.XR.Oculus.OculusRestarter::PauseAndRestart()
extern void OculusRestarter_PauseAndRestart_m28727BF6DE2EB3868DBCE80CB9FFB05F11800C9B (void);
// 0x00000087 System.Collections.IEnumerator Unity.XR.Oculus.OculusRestarter::PauseAndRestartCoroutine(System.Single)
extern void OculusRestarter_PauseAndRestartCoroutine_mE17893C50E82B62DABC119C745041BD732A93E72 (void);
// 0x00000088 System.Collections.IEnumerator Unity.XR.Oculus.OculusRestarter::RestartCoroutine(System.Boolean)
extern void OculusRestarter_RestartCoroutine_mED386FBEC1602B3DB023E06F05EC3043E6FFC027 (void);
// 0x00000089 System.Void Unity.XR.Oculus.OculusRestarter::.ctor()
extern void OculusRestarter__ctor_mAFC467BA7F2B5C76CA9E93DF2B2FE5D4B991B05D (void);
// 0x0000008A System.Void Unity.XR.Oculus.OculusRestarter/<PauseAndRestartCoroutine>d__22::.ctor(System.Int32)
extern void U3CPauseAndRestartCoroutineU3Ed__22__ctor_mE9A0F9B54589637F39AD77E522D257AAF233F600 (void);
// 0x0000008B System.Void Unity.XR.Oculus.OculusRestarter/<PauseAndRestartCoroutine>d__22::System.IDisposable.Dispose()
extern void U3CPauseAndRestartCoroutineU3Ed__22_System_IDisposable_Dispose_m2D4C564714370227159B843E3E3FD992C00C00BC (void);
// 0x0000008C System.Boolean Unity.XR.Oculus.OculusRestarter/<PauseAndRestartCoroutine>d__22::MoveNext()
extern void U3CPauseAndRestartCoroutineU3Ed__22_MoveNext_m594251DEF3855E1BD0F30EB7B45CEF06C51E94D7 (void);
// 0x0000008D System.Void Unity.XR.Oculus.OculusRestarter/<PauseAndRestartCoroutine>d__22::<>m__Finally1()
extern void U3CPauseAndRestartCoroutineU3Ed__22_U3CU3Em__Finally1_m17836219B16F0E2F40AFE29DDADEACD9F0CEA00E (void);
// 0x0000008E System.Object Unity.XR.Oculus.OculusRestarter/<PauseAndRestartCoroutine>d__22::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CPauseAndRestartCoroutineU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5CE986377F44FF4B04030B2B1CB84A4630F803A9 (void);
// 0x0000008F System.Void Unity.XR.Oculus.OculusRestarter/<PauseAndRestartCoroutine>d__22::System.Collections.IEnumerator.Reset()
extern void U3CPauseAndRestartCoroutineU3Ed__22_System_Collections_IEnumerator_Reset_m437EE8315D091FAF8113083C558FB27ACDFD49AC (void);
// 0x00000090 System.Object Unity.XR.Oculus.OculusRestarter/<PauseAndRestartCoroutine>d__22::System.Collections.IEnumerator.get_Current()
extern void U3CPauseAndRestartCoroutineU3Ed__22_System_Collections_IEnumerator_get_Current_m1A0F4AF33C851DF304034CA195AB9A7DAA9E798A (void);
// 0x00000091 System.Void Unity.XR.Oculus.OculusRestarter/<RestartCoroutine>d__23::.ctor(System.Int32)
extern void U3CRestartCoroutineU3Ed__23__ctor_mFBCFBC73BF40550C580D2DBD6446E6765106D5DC (void);
// 0x00000092 System.Void Unity.XR.Oculus.OculusRestarter/<RestartCoroutine>d__23::System.IDisposable.Dispose()
extern void U3CRestartCoroutineU3Ed__23_System_IDisposable_Dispose_m3CB326B58BAA27EB44C80329052E1D2FBB7701B9 (void);
// 0x00000093 System.Boolean Unity.XR.Oculus.OculusRestarter/<RestartCoroutine>d__23::MoveNext()
extern void U3CRestartCoroutineU3Ed__23_MoveNext_mCF04F2683E7B1F137036D577AA4C7A54AD86E648 (void);
// 0x00000094 System.Void Unity.XR.Oculus.OculusRestarter/<RestartCoroutine>d__23::<>m__Finally1()
extern void U3CRestartCoroutineU3Ed__23_U3CU3Em__Finally1_m5D43CE3EF7B89A3C817C148215ABE10F5F221332 (void);
// 0x00000095 System.Object Unity.XR.Oculus.OculusRestarter/<RestartCoroutine>d__23::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRestartCoroutineU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC4D68BC3E723F9EA64A307AAAEBE92D15E9C7C10 (void);
// 0x00000096 System.Void Unity.XR.Oculus.OculusRestarter/<RestartCoroutine>d__23::System.Collections.IEnumerator.Reset()
extern void U3CRestartCoroutineU3Ed__23_System_Collections_IEnumerator_Reset_m8B74BC95BE0B29C902B5C7547EE3461E876BFC9F (void);
// 0x00000097 System.Object Unity.XR.Oculus.OculusRestarter/<RestartCoroutine>d__23::System.Collections.IEnumerator.get_Current()
extern void U3CRestartCoroutineU3Ed__23_System_Collections_IEnumerator_get_Current_m5154B4EB20D93035D0D588C0498EAFF2D0F2C986 (void);
// 0x00000098 System.Void Unity.XR.Oculus.OculusSession::Update()
extern void OculusSession_Update_m9956F648A7B5BE12D6D26ED0DEBFD7B20722E4C2 (void);
// 0x00000099 System.UInt16 Unity.XR.Oculus.OculusSettings::GetStereoRenderingMode()
extern void OculusSettings_GetStereoRenderingMode_mB4FF47E7831A653C90D7B08E336C40D66968F2FD (void);
// 0x0000009A System.Void Unity.XR.Oculus.OculusSettings::Awake()
extern void OculusSettings_Awake_m525FBEB4247E44DBC0D7E147581BD5EE751DCDB1 (void);
// 0x0000009B System.Void Unity.XR.Oculus.OculusSettings::.ctor()
extern void OculusSettings__ctor_mBD02A264D1619566E55F183F6BE3E2F9D8C9747B (void);
// 0x0000009C System.Void Unity.XR.Oculus.OculusUsages::.cctor()
extern void OculusUsages__cctor_m95A9C1418F76CD4C0603062DD186F0FC4362316C (void);
// 0x0000009D System.Void Unity.XR.Oculus.RegisterUpdateCallback::Initialize()
extern void RegisterUpdateCallback_Initialize_m009EBB49477FAD1D7E03DA45EB80F15E6D28F521 (void);
// 0x0000009E System.Void Unity.XR.Oculus.RegisterUpdateCallback::Deinitialize()
extern void RegisterUpdateCallback_Deinitialize_m5783A25B492DD2271AF20528CC7225AA69B48D2B (void);
// 0x0000009F System.Void Unity.XR.Oculus.RegisterUpdateCallback::Update()
extern void RegisterUpdateCallback_Update_m2DA82FDFC251E9F4D10F6F5ADB6A9F4B11BAD7ED (void);
// 0x000000A0 UnityEngine.InputSystem.Controls.ButtonControl Unity.XR.Oculus.Input.OculusHMD::get_userPresence()
extern void OculusHMD_get_userPresence_m7F39249DD91669248211F49FCE7CF0F2FB962CA9 (void);
// 0x000000A1 System.Void Unity.XR.Oculus.Input.OculusHMD::set_userPresence(UnityEngine.InputSystem.Controls.ButtonControl)
extern void OculusHMD_set_userPresence_m9290C2CA19C180C3F433C06C1387B793C377B1B1 (void);
// 0x000000A2 UnityEngine.InputSystem.Controls.IntegerControl Unity.XR.Oculus.Input.OculusHMD::get_trackingState()
extern void OculusHMD_get_trackingState_m522656F3B4FDE39D98AAAC5B0AEBF736EA2A99FE (void);
// 0x000000A3 System.Void Unity.XR.Oculus.Input.OculusHMD::set_trackingState(UnityEngine.InputSystem.Controls.IntegerControl)
extern void OculusHMD_set_trackingState_mF7C35D47F95EF4AB1BD2122794D656564DD53274 (void);
// 0x000000A4 UnityEngine.InputSystem.Controls.ButtonControl Unity.XR.Oculus.Input.OculusHMD::get_isTracked()
extern void OculusHMD_get_isTracked_m0B008C62DFA3BAFCC31078CFCE09C1CDAB56996A (void);
// 0x000000A5 System.Void Unity.XR.Oculus.Input.OculusHMD::set_isTracked(UnityEngine.InputSystem.Controls.ButtonControl)
extern void OculusHMD_set_isTracked_m1313DF189AC57BEA0C43BB0DF12750D276874D87 (void);
// 0x000000A6 UnityEngine.InputSystem.Controls.Vector3Control Unity.XR.Oculus.Input.OculusHMD::get_devicePosition()
extern void OculusHMD_get_devicePosition_mBA0C9580588FC2BEEC54686E49CC63813C92EAB9 (void);
// 0x000000A7 System.Void Unity.XR.Oculus.Input.OculusHMD::set_devicePosition(UnityEngine.InputSystem.Controls.Vector3Control)
extern void OculusHMD_set_devicePosition_m918A873FF37E4CCBEFC31CE9648C27477B8C7314 (void);
// 0x000000A8 UnityEngine.InputSystem.Controls.QuaternionControl Unity.XR.Oculus.Input.OculusHMD::get_deviceRotation()
extern void OculusHMD_get_deviceRotation_mB1F8E76DA8BE83A478F69D9FE290C3FB5C8B79B2 (void);
// 0x000000A9 System.Void Unity.XR.Oculus.Input.OculusHMD::set_deviceRotation(UnityEngine.InputSystem.Controls.QuaternionControl)
extern void OculusHMD_set_deviceRotation_m0AFE1471B78C206C82B8C633C7D14B7B0232A6A5 (void);
// 0x000000AA UnityEngine.InputSystem.Controls.Vector3Control Unity.XR.Oculus.Input.OculusHMD::get_deviceAngularVelocity()
extern void OculusHMD_get_deviceAngularVelocity_mBD840B48A888E7515D4FCC343F0BA967A878D1EC (void);
// 0x000000AB System.Void Unity.XR.Oculus.Input.OculusHMD::set_deviceAngularVelocity(UnityEngine.InputSystem.Controls.Vector3Control)
extern void OculusHMD_set_deviceAngularVelocity_m33600FC9A235F57CEBB940AEA15E633383BE05F6 (void);
// 0x000000AC UnityEngine.InputSystem.Controls.Vector3Control Unity.XR.Oculus.Input.OculusHMD::get_deviceAcceleration()
extern void OculusHMD_get_deviceAcceleration_m4D2C187908410B75C2ED86798FCD8137E0739CD2 (void);
// 0x000000AD System.Void Unity.XR.Oculus.Input.OculusHMD::set_deviceAcceleration(UnityEngine.InputSystem.Controls.Vector3Control)
extern void OculusHMD_set_deviceAcceleration_mAF2696135E63B9CCF6B45046DAA6DC3FC23ED78F (void);
// 0x000000AE UnityEngine.InputSystem.Controls.Vector3Control Unity.XR.Oculus.Input.OculusHMD::get_deviceAngularAcceleration()
extern void OculusHMD_get_deviceAngularAcceleration_mF3DD84A65ED8250E0264A74EA41AA549C3F44834 (void);
// 0x000000AF System.Void Unity.XR.Oculus.Input.OculusHMD::set_deviceAngularAcceleration(UnityEngine.InputSystem.Controls.Vector3Control)
extern void OculusHMD_set_deviceAngularAcceleration_mD0F233B29A6EC778895F269F2E3490925385FD9A (void);
// 0x000000B0 UnityEngine.InputSystem.Controls.Vector3Control Unity.XR.Oculus.Input.OculusHMD::get_leftEyePosition()
extern void OculusHMD_get_leftEyePosition_mBCFDDFC5B9315BEF3C05C2643470DF1B28C7FDB1 (void);
// 0x000000B1 System.Void Unity.XR.Oculus.Input.OculusHMD::set_leftEyePosition(UnityEngine.InputSystem.Controls.Vector3Control)
extern void OculusHMD_set_leftEyePosition_m7867F1827CDE0F4238E53D5BAD345E8BEA54B3C4 (void);
// 0x000000B2 UnityEngine.InputSystem.Controls.QuaternionControl Unity.XR.Oculus.Input.OculusHMD::get_leftEyeRotation()
extern void OculusHMD_get_leftEyeRotation_m1FB4F4FFE39E6D0C9143D359A8BFCB6CAE98C063 (void);
// 0x000000B3 System.Void Unity.XR.Oculus.Input.OculusHMD::set_leftEyeRotation(UnityEngine.InputSystem.Controls.QuaternionControl)
extern void OculusHMD_set_leftEyeRotation_m2E04AA2AE19E38541EE768517C4B255F66ADE7CD (void);
// 0x000000B4 UnityEngine.InputSystem.Controls.Vector3Control Unity.XR.Oculus.Input.OculusHMD::get_leftEyeAngularVelocity()
extern void OculusHMD_get_leftEyeAngularVelocity_m65669124F4A7F1C477523049BF699221DEA79AB1 (void);
// 0x000000B5 System.Void Unity.XR.Oculus.Input.OculusHMD::set_leftEyeAngularVelocity(UnityEngine.InputSystem.Controls.Vector3Control)
extern void OculusHMD_set_leftEyeAngularVelocity_m86434991A9C53F4ED9DBE90A13C92520518490F6 (void);
// 0x000000B6 UnityEngine.InputSystem.Controls.Vector3Control Unity.XR.Oculus.Input.OculusHMD::get_leftEyeAcceleration()
extern void OculusHMD_get_leftEyeAcceleration_m354A16525174408749E9DD3A99DBAF24AAAA92D9 (void);
// 0x000000B7 System.Void Unity.XR.Oculus.Input.OculusHMD::set_leftEyeAcceleration(UnityEngine.InputSystem.Controls.Vector3Control)
extern void OculusHMD_set_leftEyeAcceleration_mF59FBC6DED7CF7A71596AB5BB315D8F053006F51 (void);
// 0x000000B8 UnityEngine.InputSystem.Controls.Vector3Control Unity.XR.Oculus.Input.OculusHMD::get_leftEyeAngularAcceleration()
extern void OculusHMD_get_leftEyeAngularAcceleration_mFE7FBC6202754B59AD009943E1AA984AFA98CBD1 (void);
// 0x000000B9 System.Void Unity.XR.Oculus.Input.OculusHMD::set_leftEyeAngularAcceleration(UnityEngine.InputSystem.Controls.Vector3Control)
extern void OculusHMD_set_leftEyeAngularAcceleration_m927734C1C924AB419E01341BC804E6FF0DB683D7 (void);
// 0x000000BA UnityEngine.InputSystem.Controls.Vector3Control Unity.XR.Oculus.Input.OculusHMD::get_rightEyePosition()
extern void OculusHMD_get_rightEyePosition_mD7850E4E14B72F635234355B1E85AD8BE9B7611E (void);
// 0x000000BB System.Void Unity.XR.Oculus.Input.OculusHMD::set_rightEyePosition(UnityEngine.InputSystem.Controls.Vector3Control)
extern void OculusHMD_set_rightEyePosition_mB95F20F913814F66D3539E4607CCCEBCB9214F93 (void);
// 0x000000BC UnityEngine.InputSystem.Controls.QuaternionControl Unity.XR.Oculus.Input.OculusHMD::get_rightEyeRotation()
extern void OculusHMD_get_rightEyeRotation_m65C0E66081ECFCBDB1EF48CDDE388DC3212EDBC2 (void);
// 0x000000BD System.Void Unity.XR.Oculus.Input.OculusHMD::set_rightEyeRotation(UnityEngine.InputSystem.Controls.QuaternionControl)
extern void OculusHMD_set_rightEyeRotation_m71BDA4076F8966E2FEF066B7B52FDB2EFEB01153 (void);
// 0x000000BE UnityEngine.InputSystem.Controls.Vector3Control Unity.XR.Oculus.Input.OculusHMD::get_rightEyeAngularVelocity()
extern void OculusHMD_get_rightEyeAngularVelocity_m622FD771B66B29F4E1D60F4EACA5A8F3E80F37FB (void);
// 0x000000BF System.Void Unity.XR.Oculus.Input.OculusHMD::set_rightEyeAngularVelocity(UnityEngine.InputSystem.Controls.Vector3Control)
extern void OculusHMD_set_rightEyeAngularVelocity_mFC4796AD89DC62611C2E68DA3B277DE0AFC40E02 (void);
// 0x000000C0 UnityEngine.InputSystem.Controls.Vector3Control Unity.XR.Oculus.Input.OculusHMD::get_rightEyeAcceleration()
extern void OculusHMD_get_rightEyeAcceleration_m0528169F10236B496798D4E11BBFA17B9EDB33F3 (void);
// 0x000000C1 System.Void Unity.XR.Oculus.Input.OculusHMD::set_rightEyeAcceleration(UnityEngine.InputSystem.Controls.Vector3Control)
extern void OculusHMD_set_rightEyeAcceleration_m4138309C612132F26E395FC1BBFD4225DA1FF4A7 (void);
// 0x000000C2 UnityEngine.InputSystem.Controls.Vector3Control Unity.XR.Oculus.Input.OculusHMD::get_rightEyeAngularAcceleration()
extern void OculusHMD_get_rightEyeAngularAcceleration_m6AD0E2AB294227E6BFEF339BE60AA136B99483ED (void);
// 0x000000C3 System.Void Unity.XR.Oculus.Input.OculusHMD::set_rightEyeAngularAcceleration(UnityEngine.InputSystem.Controls.Vector3Control)
extern void OculusHMD_set_rightEyeAngularAcceleration_mD7816946FE952A923E73843F585C0595A4E7F056 (void);
// 0x000000C4 UnityEngine.InputSystem.Controls.Vector3Control Unity.XR.Oculus.Input.OculusHMD::get_centerEyePosition()
extern void OculusHMD_get_centerEyePosition_m67A05A1CE29590353AAD443F2A0D28580DBCAEAD (void);
// 0x000000C5 System.Void Unity.XR.Oculus.Input.OculusHMD::set_centerEyePosition(UnityEngine.InputSystem.Controls.Vector3Control)
extern void OculusHMD_set_centerEyePosition_mA45E2F647153F6E0554D0298B2B0B142D65ADA9D (void);
// 0x000000C6 UnityEngine.InputSystem.Controls.QuaternionControl Unity.XR.Oculus.Input.OculusHMD::get_centerEyeRotation()
extern void OculusHMD_get_centerEyeRotation_m6E7F88828E91303B0794E5544CE29B6D69342219 (void);
// 0x000000C7 System.Void Unity.XR.Oculus.Input.OculusHMD::set_centerEyeRotation(UnityEngine.InputSystem.Controls.QuaternionControl)
extern void OculusHMD_set_centerEyeRotation_m1C8D43D462011C04235B966F9112F2642099BCF0 (void);
// 0x000000C8 UnityEngine.InputSystem.Controls.Vector3Control Unity.XR.Oculus.Input.OculusHMD::get_centerEyeAngularVelocity()
extern void OculusHMD_get_centerEyeAngularVelocity_m1551B0571B764CEFE99E7722AB51B035161B0A4F (void);
// 0x000000C9 System.Void Unity.XR.Oculus.Input.OculusHMD::set_centerEyeAngularVelocity(UnityEngine.InputSystem.Controls.Vector3Control)
extern void OculusHMD_set_centerEyeAngularVelocity_mBDF3825C87D3AB4ABE0A9228209BDBFD0524C095 (void);
// 0x000000CA UnityEngine.InputSystem.Controls.Vector3Control Unity.XR.Oculus.Input.OculusHMD::get_centerEyeAcceleration()
extern void OculusHMD_get_centerEyeAcceleration_m77AD08FF91E43AADF8D59095FD5816919AE74CAA (void);
// 0x000000CB System.Void Unity.XR.Oculus.Input.OculusHMD::set_centerEyeAcceleration(UnityEngine.InputSystem.Controls.Vector3Control)
extern void OculusHMD_set_centerEyeAcceleration_mF1A58E3466531758291739C4FFF081D20BC88C99 (void);
// 0x000000CC UnityEngine.InputSystem.Controls.Vector3Control Unity.XR.Oculus.Input.OculusHMD::get_centerEyeAngularAcceleration()
extern void OculusHMD_get_centerEyeAngularAcceleration_m47448A353E1259982772BCB30CEAD55FB708C4BF (void);
// 0x000000CD System.Void Unity.XR.Oculus.Input.OculusHMD::set_centerEyeAngularAcceleration(UnityEngine.InputSystem.Controls.Vector3Control)
extern void OculusHMD_set_centerEyeAngularAcceleration_m40F38AC30FBE1E2CEB1E021017F87806DE82553F (void);
// 0x000000CE System.Void Unity.XR.Oculus.Input.OculusHMD::FinishSetup()
extern void OculusHMD_FinishSetup_m476E392C04F8BBB6E230BE6A37A90269820CA796 (void);
// 0x000000CF System.Void Unity.XR.Oculus.Input.OculusHMD::.ctor()
extern void OculusHMD__ctor_mE497EDF74203D170F59426A5FFA99DA118BA67FD (void);
// 0x000000D0 UnityEngine.InputSystem.Controls.Vector2Control Unity.XR.Oculus.Input.OculusTouchController::get_thumbstick()
extern void OculusTouchController_get_thumbstick_m939BFF8DCC43E193BC031B70362533A38010262A (void);
// 0x000000D1 System.Void Unity.XR.Oculus.Input.OculusTouchController::set_thumbstick(UnityEngine.InputSystem.Controls.Vector2Control)
extern void OculusTouchController_set_thumbstick_mB1DC0E2B0388C3E712E09FA31AD662AD3C78EB7B (void);
// 0x000000D2 UnityEngine.InputSystem.Controls.AxisControl Unity.XR.Oculus.Input.OculusTouchController::get_trigger()
extern void OculusTouchController_get_trigger_m38BF0A4024B1305D766846E3598AFE7F4A4E62E6 (void);
// 0x000000D3 System.Void Unity.XR.Oculus.Input.OculusTouchController::set_trigger(UnityEngine.InputSystem.Controls.AxisControl)
extern void OculusTouchController_set_trigger_m36A82C419138942FC5C7902C222EEC0245CE93D0 (void);
// 0x000000D4 UnityEngine.InputSystem.Controls.AxisControl Unity.XR.Oculus.Input.OculusTouchController::get_grip()
extern void OculusTouchController_get_grip_m4B860D39D4F84449CFBBDEA3467C166C873216B7 (void);
// 0x000000D5 System.Void Unity.XR.Oculus.Input.OculusTouchController::set_grip(UnityEngine.InputSystem.Controls.AxisControl)
extern void OculusTouchController_set_grip_m8B89F611E21C4E2B4F27BD9CD160F6D4DDE45C92 (void);
// 0x000000D6 UnityEngine.InputSystem.Controls.ButtonControl Unity.XR.Oculus.Input.OculusTouchController::get_primaryButton()
extern void OculusTouchController_get_primaryButton_m01427C69202849C92D72DE1478FA6FB5038036D7 (void);
// 0x000000D7 System.Void Unity.XR.Oculus.Input.OculusTouchController::set_primaryButton(UnityEngine.InputSystem.Controls.ButtonControl)
extern void OculusTouchController_set_primaryButton_mB6F2744464021A7A8FACADE2027D13E0B39DD825 (void);
// 0x000000D8 UnityEngine.InputSystem.Controls.ButtonControl Unity.XR.Oculus.Input.OculusTouchController::get_secondaryButton()
extern void OculusTouchController_get_secondaryButton_m26C2E19D5331735DAB1532DE315905C0661A8107 (void);
// 0x000000D9 System.Void Unity.XR.Oculus.Input.OculusTouchController::set_secondaryButton(UnityEngine.InputSystem.Controls.ButtonControl)
extern void OculusTouchController_set_secondaryButton_mB1BB18678178C2AF400E7093676A52BF1980716C (void);
// 0x000000DA UnityEngine.InputSystem.Controls.ButtonControl Unity.XR.Oculus.Input.OculusTouchController::get_gripPressed()
extern void OculusTouchController_get_gripPressed_mF5E4727862CE1342E0361ECEF1102E65B1DD26BC (void);
// 0x000000DB System.Void Unity.XR.Oculus.Input.OculusTouchController::set_gripPressed(UnityEngine.InputSystem.Controls.ButtonControl)
extern void OculusTouchController_set_gripPressed_mC8087872D17C17C53C1367FB2BC92CE390FAE633 (void);
// 0x000000DC UnityEngine.InputSystem.Controls.ButtonControl Unity.XR.Oculus.Input.OculusTouchController::get_start()
extern void OculusTouchController_get_start_m511FA9CD9C460F1B3769F07B93F29AF8D5125815 (void);
// 0x000000DD System.Void Unity.XR.Oculus.Input.OculusTouchController::set_start(UnityEngine.InputSystem.Controls.ButtonControl)
extern void OculusTouchController_set_start_m53BC9D38204387A4C5039F0DF973A9E210152209 (void);
// 0x000000DE UnityEngine.InputSystem.Controls.ButtonControl Unity.XR.Oculus.Input.OculusTouchController::get_thumbstickClicked()
extern void OculusTouchController_get_thumbstickClicked_m47426BF7B742628637449C6392E15CEC934E4FE8 (void);
// 0x000000DF System.Void Unity.XR.Oculus.Input.OculusTouchController::set_thumbstickClicked(UnityEngine.InputSystem.Controls.ButtonControl)
extern void OculusTouchController_set_thumbstickClicked_m53B0A4735B36B764BA5817A35098143A22BE35FD (void);
// 0x000000E0 UnityEngine.InputSystem.Controls.ButtonControl Unity.XR.Oculus.Input.OculusTouchController::get_primaryTouched()
extern void OculusTouchController_get_primaryTouched_mF429EF61215210E7F0ED7A11340CDDEBAA6715FB (void);
// 0x000000E1 System.Void Unity.XR.Oculus.Input.OculusTouchController::set_primaryTouched(UnityEngine.InputSystem.Controls.ButtonControl)
extern void OculusTouchController_set_primaryTouched_m4CE99A47839B2B61CA93707B032AD57497D37D99 (void);
// 0x000000E2 UnityEngine.InputSystem.Controls.ButtonControl Unity.XR.Oculus.Input.OculusTouchController::get_secondaryTouched()
extern void OculusTouchController_get_secondaryTouched_mD5D6FE26EA2DED25128C02B4E9A0E330AB71FBC3 (void);
// 0x000000E3 System.Void Unity.XR.Oculus.Input.OculusTouchController::set_secondaryTouched(UnityEngine.InputSystem.Controls.ButtonControl)
extern void OculusTouchController_set_secondaryTouched_mC746A8FC2F40014FFFB66DB3996CE3C2628EBD2E (void);
// 0x000000E4 UnityEngine.InputSystem.Controls.AxisControl Unity.XR.Oculus.Input.OculusTouchController::get_triggerTouched()
extern void OculusTouchController_get_triggerTouched_m6059D02224CCE182BA04B592E60034B0C831552E (void);
// 0x000000E5 System.Void Unity.XR.Oculus.Input.OculusTouchController::set_triggerTouched(UnityEngine.InputSystem.Controls.AxisControl)
extern void OculusTouchController_set_triggerTouched_mC9CC071C7987A20B56CB0C01BF5CBBFE590DD7DE (void);
// 0x000000E6 UnityEngine.InputSystem.Controls.ButtonControl Unity.XR.Oculus.Input.OculusTouchController::get_triggerPressed()
extern void OculusTouchController_get_triggerPressed_mE1870507BA8155BA0FF1531B73F5C0AA40EADA24 (void);
// 0x000000E7 System.Void Unity.XR.Oculus.Input.OculusTouchController::set_triggerPressed(UnityEngine.InputSystem.Controls.ButtonControl)
extern void OculusTouchController_set_triggerPressed_m4717577F155237F5DCA58FF8595976978F2E3555 (void);
// 0x000000E8 UnityEngine.InputSystem.Controls.ButtonControl Unity.XR.Oculus.Input.OculusTouchController::get_thumbstickTouched()
extern void OculusTouchController_get_thumbstickTouched_mE4A1D89F50E0762A00F4A75365A46DDA48769802 (void);
// 0x000000E9 System.Void Unity.XR.Oculus.Input.OculusTouchController::set_thumbstickTouched(UnityEngine.InputSystem.Controls.ButtonControl)
extern void OculusTouchController_set_thumbstickTouched_mD797890DA4D595468ABDEEBED7B3AF43DA6622F0 (void);
// 0x000000EA UnityEngine.InputSystem.Controls.IntegerControl Unity.XR.Oculus.Input.OculusTouchController::get_trackingState()
extern void OculusTouchController_get_trackingState_m2E380481C56A4DA04393D4C4F4F328A27936A0D4 (void);
// 0x000000EB System.Void Unity.XR.Oculus.Input.OculusTouchController::set_trackingState(UnityEngine.InputSystem.Controls.IntegerControl)
extern void OculusTouchController_set_trackingState_m56952CEA07A22BE8B2A1800B71AE6E275CAE4396 (void);
// 0x000000EC UnityEngine.InputSystem.Controls.ButtonControl Unity.XR.Oculus.Input.OculusTouchController::get_isTracked()
extern void OculusTouchController_get_isTracked_m30C41CD8955C5DDE32DBF9302325F12DF0BE73E2 (void);
// 0x000000ED System.Void Unity.XR.Oculus.Input.OculusTouchController::set_isTracked(UnityEngine.InputSystem.Controls.ButtonControl)
extern void OculusTouchController_set_isTracked_m7DED285C1364AEDDAF5EE3727424D1FAAEBD3218 (void);
// 0x000000EE UnityEngine.InputSystem.Controls.Vector3Control Unity.XR.Oculus.Input.OculusTouchController::get_devicePosition()
extern void OculusTouchController_get_devicePosition_m7929EA64D658AE437C9D31A9D803E48FD397A24C (void);
// 0x000000EF System.Void Unity.XR.Oculus.Input.OculusTouchController::set_devicePosition(UnityEngine.InputSystem.Controls.Vector3Control)
extern void OculusTouchController_set_devicePosition_m8F786136F2C3A7A0B69B8A3C776D16F22A6BC87B (void);
// 0x000000F0 UnityEngine.InputSystem.Controls.QuaternionControl Unity.XR.Oculus.Input.OculusTouchController::get_deviceRotation()
extern void OculusTouchController_get_deviceRotation_m286BC7BDF4583DC5D354921E1CAE96261D0CEA43 (void);
// 0x000000F1 System.Void Unity.XR.Oculus.Input.OculusTouchController::set_deviceRotation(UnityEngine.InputSystem.Controls.QuaternionControl)
extern void OculusTouchController_set_deviceRotation_mF0D9EB5B008A47FD93A8A4452043D4AE74C78A1D (void);
// 0x000000F2 UnityEngine.InputSystem.Controls.Vector3Control Unity.XR.Oculus.Input.OculusTouchController::get_deviceVelocity()
extern void OculusTouchController_get_deviceVelocity_m8339E69DA2AF4B8B9A35BC6CA322F393AA5AA290 (void);
// 0x000000F3 System.Void Unity.XR.Oculus.Input.OculusTouchController::set_deviceVelocity(UnityEngine.InputSystem.Controls.Vector3Control)
extern void OculusTouchController_set_deviceVelocity_m854AB855C5AB3CF32F5FAEEE599804EB5D365B62 (void);
// 0x000000F4 UnityEngine.InputSystem.Controls.Vector3Control Unity.XR.Oculus.Input.OculusTouchController::get_deviceAngularVelocity()
extern void OculusTouchController_get_deviceAngularVelocity_mD0EE32CDA1ABBC2346F474368EC15BF8A237431D (void);
// 0x000000F5 System.Void Unity.XR.Oculus.Input.OculusTouchController::set_deviceAngularVelocity(UnityEngine.InputSystem.Controls.Vector3Control)
extern void OculusTouchController_set_deviceAngularVelocity_m7D861B677871ECD4E2B81D0DE920851C95EEAE13 (void);
// 0x000000F6 UnityEngine.InputSystem.Controls.Vector3Control Unity.XR.Oculus.Input.OculusTouchController::get_deviceAcceleration()
extern void OculusTouchController_get_deviceAcceleration_m1633B2E2F9B5B9771A5F675582F9171EAB479328 (void);
// 0x000000F7 System.Void Unity.XR.Oculus.Input.OculusTouchController::set_deviceAcceleration(UnityEngine.InputSystem.Controls.Vector3Control)
extern void OculusTouchController_set_deviceAcceleration_mB1C384EE21F68ACBB926856A01CC6E84956986A9 (void);
// 0x000000F8 UnityEngine.InputSystem.Controls.Vector3Control Unity.XR.Oculus.Input.OculusTouchController::get_deviceAngularAcceleration()
extern void OculusTouchController_get_deviceAngularAcceleration_mDAC7A7A122090D72F0D46F18552E4651187CF865 (void);
// 0x000000F9 System.Void Unity.XR.Oculus.Input.OculusTouchController::set_deviceAngularAcceleration(UnityEngine.InputSystem.Controls.Vector3Control)
extern void OculusTouchController_set_deviceAngularAcceleration_m2F970918D95E3C1812A5D5926B4F83BB7CC0EDCF (void);
// 0x000000FA System.Void Unity.XR.Oculus.Input.OculusTouchController::FinishSetup()
extern void OculusTouchController_FinishSetup_mCFB90E6EBDEB8F694B926E12EDE38DA7D3BD394A (void);
// 0x000000FB System.Void Unity.XR.Oculus.Input.OculusTouchController::.ctor()
extern void OculusTouchController__ctor_m3DF25FC6EBC1D65AFDBDB011BCF9ABB1A85793C8 (void);
// 0x000000FC UnityEngine.InputSystem.Controls.IntegerControl Unity.XR.Oculus.Input.OculusTrackingReference::get_trackingState()
extern void OculusTrackingReference_get_trackingState_m85616C0F80EE9E39BFF3E5196DABBC01E3533B8C (void);
// 0x000000FD System.Void Unity.XR.Oculus.Input.OculusTrackingReference::set_trackingState(UnityEngine.InputSystem.Controls.IntegerControl)
extern void OculusTrackingReference_set_trackingState_mF946EDAC55B027ADB50C4BB7F45410583D77A574 (void);
// 0x000000FE UnityEngine.InputSystem.Controls.ButtonControl Unity.XR.Oculus.Input.OculusTrackingReference::get_isTracked()
extern void OculusTrackingReference_get_isTracked_m6EF555E8C3A27C52275A23C2E98A4E66D2218BD7 (void);
// 0x000000FF System.Void Unity.XR.Oculus.Input.OculusTrackingReference::set_isTracked(UnityEngine.InputSystem.Controls.ButtonControl)
extern void OculusTrackingReference_set_isTracked_m1C8935A8187AF11A15875029D6C9219BD8DF235E (void);
// 0x00000100 UnityEngine.InputSystem.Controls.Vector3Control Unity.XR.Oculus.Input.OculusTrackingReference::get_devicePosition()
extern void OculusTrackingReference_get_devicePosition_m2D81394973C28D465EA20F9A5F6C003D2ED67A02 (void);
// 0x00000101 System.Void Unity.XR.Oculus.Input.OculusTrackingReference::set_devicePosition(UnityEngine.InputSystem.Controls.Vector3Control)
extern void OculusTrackingReference_set_devicePosition_mFF59FEA48A8981918702BF796DDABB7D753C5C25 (void);
// 0x00000102 UnityEngine.InputSystem.Controls.QuaternionControl Unity.XR.Oculus.Input.OculusTrackingReference::get_deviceRotation()
extern void OculusTrackingReference_get_deviceRotation_m73C171560EEF442ECCF1639FAA89C86388F88A54 (void);
// 0x00000103 System.Void Unity.XR.Oculus.Input.OculusTrackingReference::set_deviceRotation(UnityEngine.InputSystem.Controls.QuaternionControl)
extern void OculusTrackingReference_set_deviceRotation_mCBB7204D4EEBBEAD6BB36DB51B38D2D114DEF919 (void);
// 0x00000104 System.Void Unity.XR.Oculus.Input.OculusTrackingReference::FinishSetup()
extern void OculusTrackingReference_FinishSetup_mC8CDDD455575F2A0FF9F9E3E996CDD200752BC33 (void);
// 0x00000105 System.Void Unity.XR.Oculus.Input.OculusTrackingReference::.ctor()
extern void OculusTrackingReference__ctor_m034388DDCC9F71833408E6C24C10F4F8C9FFE15E (void);
// 0x00000106 UnityEngine.InputSystem.Controls.ButtonControl Unity.XR.Oculus.Input.OculusRemote::get_back()
extern void OculusRemote_get_back_m9242761440A9EF3D714EE44A34FC959FF63FF09A (void);
// 0x00000107 System.Void Unity.XR.Oculus.Input.OculusRemote::set_back(UnityEngine.InputSystem.Controls.ButtonControl)
extern void OculusRemote_set_back_mE211D9B585E73299D9D43AC28BFADA7A3C9C772F (void);
// 0x00000108 UnityEngine.InputSystem.Controls.ButtonControl Unity.XR.Oculus.Input.OculusRemote::get_start()
extern void OculusRemote_get_start_mD5EBA7E3B635341556D55C2A8244613CDF368A17 (void);
// 0x00000109 System.Void Unity.XR.Oculus.Input.OculusRemote::set_start(UnityEngine.InputSystem.Controls.ButtonControl)
extern void OculusRemote_set_start_mC4D1037E74795A524395BDEFA7CA88CB474F33F1 (void);
// 0x0000010A UnityEngine.InputSystem.Controls.Vector2Control Unity.XR.Oculus.Input.OculusRemote::get_touchpad()
extern void OculusRemote_get_touchpad_mFC90C7ED51F2F2AFBD8435493978CE0C14A2BE9B (void);
// 0x0000010B System.Void Unity.XR.Oculus.Input.OculusRemote::set_touchpad(UnityEngine.InputSystem.Controls.Vector2Control)
extern void OculusRemote_set_touchpad_m174C971799B16F5032D979AC7BA2F8C8AEF020A9 (void);
// 0x0000010C System.Void Unity.XR.Oculus.Input.OculusRemote::FinishSetup()
extern void OculusRemote_FinishSetup_m5C9517EF579C721B0ADC03E00EBFF1F0C5BA7F7F (void);
// 0x0000010D System.Void Unity.XR.Oculus.Input.OculusRemote::.ctor()
extern void OculusRemote__ctor_m2BCE3ECEF5DBFEAD59595E747A26E7A314788A3C (void);
// 0x0000010E System.Void Unity.XR.Oculus.Input.OculusGoController::.ctor()
extern void OculusGoController__ctor_mA08B0DA17D85ACD7166DE362B280B0E019F02719 (void);
// 0x0000010F UnityEngine.InputSystem.Controls.ButtonControl Unity.XR.Oculus.Input.OculusHMDExtended::get_back()
extern void OculusHMDExtended_get_back_mFDBD32A74252025AA1F9DA1920F162FFF60AF777 (void);
// 0x00000110 System.Void Unity.XR.Oculus.Input.OculusHMDExtended::set_back(UnityEngine.InputSystem.Controls.ButtonControl)
extern void OculusHMDExtended_set_back_m0E37910E7CC993017981633932E6AF6C5602CFB3 (void);
// 0x00000111 UnityEngine.InputSystem.Controls.Vector2Control Unity.XR.Oculus.Input.OculusHMDExtended::get_touchpad()
extern void OculusHMDExtended_get_touchpad_m9DBF0B06449BFAD9A6237A237ACA2C2B277C1332 (void);
// 0x00000112 System.Void Unity.XR.Oculus.Input.OculusHMDExtended::set_touchpad(UnityEngine.InputSystem.Controls.Vector2Control)
extern void OculusHMDExtended_set_touchpad_mB34658748A3749934B64A0CC7477D62A30EDF04D (void);
// 0x00000113 System.Void Unity.XR.Oculus.Input.OculusHMDExtended::FinishSetup()
extern void OculusHMDExtended_FinishSetup_mA390A06FAC20D884C637738168848C44E5BBDD3C (void);
// 0x00000114 System.Void Unity.XR.Oculus.Input.OculusHMDExtended::.ctor()
extern void OculusHMDExtended__ctor_mB98D9DF9D7D27CD8EE8E72E0111C29E94D7FB64A (void);
// 0x00000115 UnityEngine.InputSystem.Controls.Vector2Control Unity.XR.Oculus.Input.GearVRTrackedController::get_touchpad()
extern void GearVRTrackedController_get_touchpad_m21DEB0B6AEBE75FE0614851648893C2F40FA75C9 (void);
// 0x00000116 System.Void Unity.XR.Oculus.Input.GearVRTrackedController::set_touchpad(UnityEngine.InputSystem.Controls.Vector2Control)
extern void GearVRTrackedController_set_touchpad_m11568D5395BF554EE37F2170411C726B9248304C (void);
// 0x00000117 UnityEngine.InputSystem.Controls.AxisControl Unity.XR.Oculus.Input.GearVRTrackedController::get_trigger()
extern void GearVRTrackedController_get_trigger_mD4688E98CE65F29BF18A00A5BB6E7C498C4B73A3 (void);
// 0x00000118 System.Void Unity.XR.Oculus.Input.GearVRTrackedController::set_trigger(UnityEngine.InputSystem.Controls.AxisControl)
extern void GearVRTrackedController_set_trigger_m51C37516D561D90C027C9B28089149F9DFED604C (void);
// 0x00000119 UnityEngine.InputSystem.Controls.ButtonControl Unity.XR.Oculus.Input.GearVRTrackedController::get_back()
extern void GearVRTrackedController_get_back_mC93C819F033288454AB46C03E649CB5FAA330DB7 (void);
// 0x0000011A System.Void Unity.XR.Oculus.Input.GearVRTrackedController::set_back(UnityEngine.InputSystem.Controls.ButtonControl)
extern void GearVRTrackedController_set_back_mE5906CC7A6A1A1526A3C67B1FD8222CCD36001B2 (void);
// 0x0000011B UnityEngine.InputSystem.Controls.ButtonControl Unity.XR.Oculus.Input.GearVRTrackedController::get_triggerPressed()
extern void GearVRTrackedController_get_triggerPressed_m1129EFEBFC62FCB0E3A7D9341CCCDC3CA763A3F5 (void);
// 0x0000011C System.Void Unity.XR.Oculus.Input.GearVRTrackedController::set_triggerPressed(UnityEngine.InputSystem.Controls.ButtonControl)
extern void GearVRTrackedController_set_triggerPressed_mEF11C6119612BF985138FC3A1B477DF5F2D9285E (void);
// 0x0000011D UnityEngine.InputSystem.Controls.ButtonControl Unity.XR.Oculus.Input.GearVRTrackedController::get_touchpadClicked()
extern void GearVRTrackedController_get_touchpadClicked_m752DB8A8A2304B4393C3C147BD0FBCAC57659EC0 (void);
// 0x0000011E System.Void Unity.XR.Oculus.Input.GearVRTrackedController::set_touchpadClicked(UnityEngine.InputSystem.Controls.ButtonControl)
extern void GearVRTrackedController_set_touchpadClicked_mD2C1482CAC771AEACF36BE35C37CF0D4029A74B4 (void);
// 0x0000011F UnityEngine.InputSystem.Controls.ButtonControl Unity.XR.Oculus.Input.GearVRTrackedController::get_touchpadTouched()
extern void GearVRTrackedController_get_touchpadTouched_m332094F49118746F89F231CA4BF30F9F6B4CBE98 (void);
// 0x00000120 System.Void Unity.XR.Oculus.Input.GearVRTrackedController::set_touchpadTouched(UnityEngine.InputSystem.Controls.ButtonControl)
extern void GearVRTrackedController_set_touchpadTouched_m217D58D7149896C50E7A891B7A73409006E850B5 (void);
// 0x00000121 UnityEngine.InputSystem.Controls.IntegerControl Unity.XR.Oculus.Input.GearVRTrackedController::get_trackingState()
extern void GearVRTrackedController_get_trackingState_m1593CF906B9901952CB4D38512C6A2651B0EA093 (void);
// 0x00000122 System.Void Unity.XR.Oculus.Input.GearVRTrackedController::set_trackingState(UnityEngine.InputSystem.Controls.IntegerControl)
extern void GearVRTrackedController_set_trackingState_m1E7F9A50F7E2AC02B6A39306FCBA27E274DF729A (void);
// 0x00000123 UnityEngine.InputSystem.Controls.ButtonControl Unity.XR.Oculus.Input.GearVRTrackedController::get_isTracked()
extern void GearVRTrackedController_get_isTracked_m75DA6218446F11E66470D5CCEFD2608A9E1EA664 (void);
// 0x00000124 System.Void Unity.XR.Oculus.Input.GearVRTrackedController::set_isTracked(UnityEngine.InputSystem.Controls.ButtonControl)
extern void GearVRTrackedController_set_isTracked_m87BE49E18745B46D49E3495951F824C1EDBBAF41 (void);
// 0x00000125 UnityEngine.InputSystem.Controls.Vector3Control Unity.XR.Oculus.Input.GearVRTrackedController::get_devicePosition()
extern void GearVRTrackedController_get_devicePosition_m9DD0861D1F1EAA406FB26900C725DEAD80A42B33 (void);
// 0x00000126 System.Void Unity.XR.Oculus.Input.GearVRTrackedController::set_devicePosition(UnityEngine.InputSystem.Controls.Vector3Control)
extern void GearVRTrackedController_set_devicePosition_m795223AB6F315C47E507787D0F29DFBD3B1F7E7E (void);
// 0x00000127 UnityEngine.InputSystem.Controls.QuaternionControl Unity.XR.Oculus.Input.GearVRTrackedController::get_deviceRotation()
extern void GearVRTrackedController_get_deviceRotation_m7EE51AFFA124012AEC747E92EE966840624A4C74 (void);
// 0x00000128 System.Void Unity.XR.Oculus.Input.GearVRTrackedController::set_deviceRotation(UnityEngine.InputSystem.Controls.QuaternionControl)
extern void GearVRTrackedController_set_deviceRotation_mFCB0BA94E4FB6D8DAB26B19B9408245C968D05B9 (void);
// 0x00000129 UnityEngine.InputSystem.Controls.Vector3Control Unity.XR.Oculus.Input.GearVRTrackedController::get_deviceAngularVelocity()
extern void GearVRTrackedController_get_deviceAngularVelocity_m90B562425563816EAFD84B256CECA612055758E4 (void);
// 0x0000012A System.Void Unity.XR.Oculus.Input.GearVRTrackedController::set_deviceAngularVelocity(UnityEngine.InputSystem.Controls.Vector3Control)
extern void GearVRTrackedController_set_deviceAngularVelocity_m5C22DE0A26841683FAC1C4FB9D4DD56641C9BD8C (void);
// 0x0000012B UnityEngine.InputSystem.Controls.Vector3Control Unity.XR.Oculus.Input.GearVRTrackedController::get_deviceAcceleration()
extern void GearVRTrackedController_get_deviceAcceleration_m6526C5074963749493C4524C9411828196D2B006 (void);
// 0x0000012C System.Void Unity.XR.Oculus.Input.GearVRTrackedController::set_deviceAcceleration(UnityEngine.InputSystem.Controls.Vector3Control)
extern void GearVRTrackedController_set_deviceAcceleration_m009452ABC6F9A06BF9F984623582ABB365CF845B (void);
// 0x0000012D UnityEngine.InputSystem.Controls.Vector3Control Unity.XR.Oculus.Input.GearVRTrackedController::get_deviceAngularAcceleration()
extern void GearVRTrackedController_get_deviceAngularAcceleration_m38754F45623C53D1913DC0A3E9DCAC9BEA0C5BF9 (void);
// 0x0000012E System.Void Unity.XR.Oculus.Input.GearVRTrackedController::set_deviceAngularAcceleration(UnityEngine.InputSystem.Controls.Vector3Control)
extern void GearVRTrackedController_set_deviceAngularAcceleration_mE3BFDC78B4C94A855790F2EE3DE26DA4F35BBB25 (void);
// 0x0000012F System.Void Unity.XR.Oculus.Input.GearVRTrackedController::FinishSetup()
extern void GearVRTrackedController_FinishSetup_m435CA1B37517D6EA68D43DDBBC4A334C1B5F955D (void);
// 0x00000130 System.Void Unity.XR.Oculus.Input.GearVRTrackedController::.ctor()
extern void GearVRTrackedController__ctor_m35059DBECC8470F3A2695CF33528C805722D6B6B (void);
static Il2CppMethodPointer s_methodPointers[304] = 
{
	Utils_SetColorScaleAndOffset_m6434F29CDE9823397828ECE9F9A2817D44D48D84,
	Utils_GetSystemHeadsetType_m383047E3555F08E216DD00B53BFE5F3C016FAFE9,
	Utils_SetFoveationLevel_m16335000BA8172A666725F27407B2FEC32EB43D0,
	Utils_EnableDynamicFFR_mAFC73962AAB99074606ED7D65ECC9D35B41C430A,
	Utils_GetFoveationLevel_m922BB229AFE1AA65CDD41B454CCD930F0D93522B,
	InputFocus_add_InputFocusAcquired_m792AC75AEC3F732433E7490A43EDD5ED153F6094,
	InputFocus_remove_InputFocusAcquired_m19273BB66624D67CE968DEC379569ECA0AA7D43A,
	InputFocus_add_InputFocusLost_m11782A9ADE16C0FE65636052AEEACA5BF4BE07C1,
	InputFocus_remove_InputFocusLost_m381E9CF962F5C1CBB6E737334ED070A356C02582,
	InputFocus_get_hasInputFocus_m89BC1D49BF5D1BD9207D5D32780FA93CEC6AD793,
	InputFocus_Update_mA29F59D3148E351431EF53DEEE921320DC66945E,
	InputFocus__ctor_m3EC50CAA3520F2BB04B7258680BD4B4081B9C9F3,
	InputFocus__cctor_mB6300AFCE681F96FFBB24D1AAB76E815716B8C95,
	Boundary_GetBoundaryConfigured_m3084C1D2ED0E1456840215C4D022838D34A51CAB,
	Boundary_GetBoundaryDimensions_m687BD292B02EC436F995EECDF128F88366575FE1,
	Boundary_GetBoundaryVisible_m29E7D20DA141AA1666466B54833DFC1990338C69,
	Boundary_SetBoundaryVisible_mAE47C1076E3C28BB7A80CB3169C3DA411BEB347C,
	Development_TrySetDeveloperMode_m42C081BDF42482D60F1FE1FCDEB1D4CA9E72B320,
	Development_OverrideDeveloperModeStart_mC9E24212121397B0C37A27904F5B95B4FD871C88,
	Development_OverrideDeveloperModeStop_mF62FEF98A60806E453F85B7419FE8A726A54A621,
	Development__cctor_mBFD72BE5D5819D1EEBCB7D11CCD064B26AC4518C,
	InputLayoutLoader__cctor_m7BFAE3F72F69BA6A7FA77F14FFD935C18DF5530D,
	InputLayoutLoader_RegisterInputLayouts_m7D259AB887FFDEB3E4FA2F7D991F498BDC00DE15,
	OculusLoader_IsDeviceSupported_mC2E5A317811DBD9EB506037FD358CF7F64814286,
	OculusLoader_get_displaySubsystem_mE2C42E00522835401105B4A163E0768AA433E71A,
	OculusLoader_get_inputSubsystem_m8E87DAAF3BB44B2F3945232518EAF6862E9D1B95,
	OculusLoader_Initialize_m9F45EA7F9134BED3F23133D3B2A3467CF51B5561,
	OculusLoader_Start_m48219B334C6D430348D0A6CE4B0BB8E9493E29F5,
	OculusLoader_Stop_m096A1F5907CFA0315161CDE8C28F889CC1709482,
	OculusLoader_Deinitialize_m1DBBE6B4710416D0793C6869DC3C17656431599F,
	OculusLoader_RuntimeLoadOVRPlugin_m2CA81B2C2F621A6BEF5495DF46AE5B2E2F04C042,
	OculusLoader_GetSettings_mF0D5694BD84372039D4227997B00CAB33A419CAC,
	OculusLoader__ctor_mF6A80167D63792923ADC3AB04D39BC1D0B3EB360,
	OculusLoader__cctor_mD99F1A4C2C8FA6EE03E450E22062EDC2AEF809BE,
	Performance_TrySetCPULevel_m168A76958F71C27D5A17A346F699F874CD119D39,
	Performance_TrySetGPULevel_mE13008C6066A522BAEFE7B171A51AB78BDD81EDC,
	Performance_TryGetAvailableDisplayRefreshRates_m6322292C481D9F84FF1A56DCC33897047FF0AA42,
	Performance_TrySetDisplayRefreshRate_m08D4B41CCE153758A990FE1D45E90344B3AFB583,
	Performance_TryGetDisplayRefreshRate_mE352668E6C1C1FF8B6184F23FF31C047B8F34C45,
	Performance__cctor_m01F738B7F7C2CC8CFBFA44910D5BF0ABB33F820B,
	Stats_get_PluginVersion_m66F99A5B04F4CADEE440E825BF1A29E927363674,
	Stats_GetOculusDisplaySubsystem_mC67658F860743C9A2AFA66A9C70A41446B309912,
	Stats__ctor_mA96DB2D702AAB276E41CD7C005AB64B599C0B63C,
	Stats__cctor_mC93F4550849911809F978CF11798655CAAB0E528,
	AdaptivePerformance_get_GPUAppTime_mFEEFEEDF7634F4064C690F07B1DB26825692A238,
	AdaptivePerformance_get_GPUCompositorTime_mE88C5D2A9508EC775678D4AAFE172D644DD3B27E,
	AdaptivePerformance_get_MotionToPhoton_m80740922FD30B202610BDB102AD95D46D3FB9F7C,
	AdaptivePerformance_get_RefreshRate_mF58D1D911B6983F6C8E04DF0FD8D31863842F3D5,
	AdaptivePerformance_get_BatteryTemp_m47FB27D2D8E551DC256CDB9616F2142D2DEB2175,
	AdaptivePerformance_get_BatteryLevel_mAA3234732E5829EA40C79610386C23B00D5DF07C,
	AdaptivePerformance_get_PowerSavingMode_m99226E3F46426919FA68C75216FA229A8F7F9851,
	AdaptivePerformance_get_AdaptivePerformanceScale_m9947FB5DB23E0092A71D3E08A775082FEC71B43E,
	AdaptivePerformance_get_CPULevel_m1AD7E50E684841C20C52A92B65C516E112960847,
	AdaptivePerformance_get_GPULevel_mBEFC052317F45BB89DC42293713AD696D24CBF2A,
	PerfMetrics_get_AppCPUTime_mE53A84B8202BCA2E221EA0297D6F4C067434344D,
	PerfMetrics_get_AppGPUTime_m6055F2F6B1F987492D42B376AAF23F7DAB2CA9F9,
	PerfMetrics_get_CompositorCPUTime_mA25DE33A0AE7CE98363AD9CC589A275390602982,
	PerfMetrics_get_CompositorGPUTime_m42BD7C18C2BE1ABBAAEB0BA3A832A54C4B57478A,
	PerfMetrics_get_GPUUtilization_m1D6176628FCB67FE5C1F4665D1BA7FB608184E4C,
	PerfMetrics_get_CPUUtilizationAverage_mC7D22DDB555F09C483D19D7D6CA9EF911D8E0A8E,
	PerfMetrics_get_CPUUtilizationWorst_m2F112911B6C7BB7A2D684B01E41292857D9E54FD,
	PerfMetrics_get_CPUClockFrequency_m6A41A80961206C86DC37E23869EC04A4B506AC4B,
	PerfMetrics_get_GPUClockFrequency_mE67C9D0558582243809FDFD11CEDBCE4B9C709E3,
	PerfMetrics_EnablePerfMetrics_m6DCAC8F8AAC829B357E7DA4876D7377FB3F45FAD,
	AppMetrics_get_AppQueueAheadTime_m6269E5088701FEB294099FA3B9411C7E631529EE,
	AppMetrics_get_AppCPUElapsedTime_m27DCDA440E31783E74022FE2C088959ABA79CDA2,
	AppMetrics_get_CompositorDroppedFrames_m76EAF296BC146D237038A450AE6616C2AD3E7547,
	AppMetrics_get_CompositorLatency_m0C0237CDA7D800BF3151C8309B50F03ED194CEEA,
	AppMetrics_get_CompositorCPUTime_m75C30FD99DB6696C222044E0E4102007C15FB253,
	AppMetrics_get_CPUStartToGPUEnd_mA294A8E4B82FEA622DA1E042C70385655D45BD44,
	AppMetrics_get_GPUEndToVsync_m38FA875E11D5A9E5C377C09C60030243E3872B6D,
	AppMetrics_EnableAppMetrics_m3D99B6DC2574C718873D2DC9941FDD14049F6779,
	NativeMethods_SetColorScale_mABAA1FBFC163D8DE94AD47D7B8F848B78CD5E9A0,
	NativeMethods_SetColorOffset_m62E7000E8A015D685D76D915941FDE958732213D,
	NativeMethods_GetIsSupportedDevice_mFC86118E0FC74ABE30C84639472ADE492083AFD2,
	NativeMethods_LoadOVRPlugin_m4D4C796D2F76D0CA13969C61603C01B0A26478E7,
	NativeMethods_UnloadOVRPlugin_mF9B47E517AB3E99DA9AE3E712054150C04D7D7DA,
	NativeMethods_SetUserDefinedSettings_m60831E50BCEB63E8B8E3A6421CE2D6BC21FB6FCD,
	NativeMethods_SetCPULevel_mABD8BE1F1CE41DC683BA43822DB456530FD6D929,
	NativeMethods_SetGPULevel_m055FCE766C4C218F61A06ACA963F5ED637834DF7,
	NativeMethods_GetOVRPVersion_mB328CD13DA707D9B1B26A3EBC82A9F364F140BE9,
	NativeMethods_EnablePerfMetrics_mB9E9467765E2141254EAC993FF87E6595831DC74,
	NativeMethods_EnableAppMetrics_mBFF69EB9C54389B25A9FC271819E9B3FB2A6CDB6,
	NativeMethods_SetDeveloperModeStrict_mB7F4A67FF3C6396D8600290B3BBA555A6CF56767,
	NativeMethods_GetHasInputFocus_mC5174AB5E09FCDEC72E3FE70EE041824A3328806,
	NativeMethods_GetBoundaryConfigured_m63425B4A401546727492B96CEDDBDF636499D961,
	NativeMethods_GetBoundaryDimensions_m32B73039BE0E747DAFB358D71514D5E7C75153A6,
	NativeMethods_GetBoundaryVisible_m274038DDF26F9FE120C68CD993C31D751C18B369,
	NativeMethods_SetBoundaryVisible_mC4CAB764018040FFB39B8034D86B8E282DB73FF1,
	NativeMethods_GetAppShouldQuit_m4A8AC6995227A1AA4F5EF087051DA57735D0C486,
	NativeMethods_GetDisplayAvailableFrequencies_m2E4C861D87369CCA6A40AAFF6EA5CF65093D4F39,
	NativeMethods_SetDisplayFrequency_m0C913002249CA6C2FDAD4AB57A0957D4F5F8646C,
	NativeMethods_GetDisplayFrequency_m5D89A6D7DF13E557DCF74B65770B5945A2C327BE,
	NativeMethods_GetSystemHeadsetType_m5D2B6BFDE86FE8DB56A48641E1EA3B3AC788C0AE,
	NativeMethods_GetTiledMultiResSupported_mA3B09B2A1F0299A3AAD86C0DEB36625D598CD871,
	NativeMethods_SetTiledMultiResLevel_mB738658807A7B692985B9CD55A0AEE8AAE58FF2D,
	NativeMethods_GetTiledMultiResLevel_m8082A03420962D025A24554936BC8802D190F3F0,
	NativeMethods_SetTiledMultiResDynamic_mC79DF59B055FA02F3605881A38984454869DA66B,
	NativeMethods_GetShouldRestartSession_m691BE303740AD0161244C7D02838D9A1397D8EA1,
	Internal_SetColorScale_mA8ED134FCC56CCD16F92734ADCBF2526342EB02C,
	Internal_SetColorOffset_m6765A7EC8EFFDB061AE48B3F53658BBD3FC26FBF,
	Internal_GetIsSupportedDevice_mA3EF73A5F9CA1AD2B6E9D8F1F560A62D14A19E92,
	Internal_LoadOVRPlugin_m20BF6ADFC47F043ED9CB9F86199E26C8A46AD5AF,
	Internal_UnloadOVRPlugin_m6C683304420498120BF38390A1129A5B01A67F1E,
	Internal_SetUserDefinedSettings_mE187F03B58FEB8FE874B938E28401C06C919221E,
	Internal_SetCPULevel_m0568DA61DDC47E9F28D26C52EA4035A6A139CCA8,
	Internal_SetGPULevel_mBC46AC72882E2FFD4358F5812B8F45329C62843A,
	Internal_GetOVRPVersion_m1E2BB4B4CDC04F9DAD64DF67D3CFC76085AF305B,
	Internal_EnablePerfMetrics_m2572EAFC2373A4E10168EDD76D576A44212466D4,
	Internal_EnableAppMetrics_m327F6E0F90F1A962D000C105EFE540CD43E78D70,
	Internal_SetDeveloperModeStrict_m0A2121361A4FFDA8D0EF716D2C011F0EF19A34F8,
	Internal_GetAppHasInputFocus_mA679379FDE6F3AFAFBF35841818A0BD5A492579F,
	Internal_GetBoundaryConfigured_m38A8FB6A7F2DCE7882E9DF3707530E40D85E6492,
	Internal_GetBoundaryDimensions_m07ED6D4A7E3C368766B953282AE8FA76A257849B,
	Internal_GetBoundaryVisible_mFFBE7E9B928B930A6C4B1F8F7391F3403CD9F72F,
	Internal_SetBoundaryVisible_m3F762D66ED2E7998D8EDD6B065398796C32B2139,
	Internal_GetAppShouldQuit_m2201DC6D753AE50E8E041C75816721CC622DA353,
	Internal_GetDisplayAvailableFrequencies_mA6A02093DF72E979557B913FEE19BA4B9CF61159,
	Internal_SetDisplayFrequency_m3221766C0E2DF541A57A44508194B245E038A965,
	Internal_GetDisplayFrequency_m3264CC851EAF260CA1EFD7E4984362B1B7BC8676,
	Internal_GetSystemHeadsetType_mC0537BB4F5EA3EDD3993C7A05487B04A0FADAEFB,
	Internal_GetTiledMultiResSupported_mEE711350D975E6C4FD4D0634AEDD64F3FEF2E0BB,
	Internal_SetTiledMultiResLevel_mEEC4003CC9AEA6156EE976678E3409FCABE6EDD2,
	Internal_GetTiledMultiResLevel_m2BC392E4824622E98F610EC02CB4A8196FEF96CE,
	Internal_SetTiledMultiResDynamic_m037817F3884FC015AC28AB27520A40B3511D2A10,
	Internal_GetShouldRestartSession_m232B73B6EB98D8E93F361FFBEDC386E93CD86E1E,
	OculusRestarter__cctor_m050787ED25CC19F82E8607F6A3DD74B6A92E7D57,
	OculusRestarter_ResetCallbacks_mDFC9B5CDAF74BFE121D4F813709A09DF60E75CC3,
	OculusRestarter_get_isRunning_mD3558955217FE9FEC976D5AE772617EBEB44E25E,
	OculusRestarter_get_TimeBetweenRestartAttempts_m4643B819097BB5929C5EA5AAC0BBD798817B2465,
	OculusRestarter_set_TimeBetweenRestartAttempts_m3FDA87CEAC42CF6AF6521DFF3E4EB1CC0BC9454F,
	OculusRestarter_get_PauseAndRestartAttempts_m5073862450728EBD76EDC01F37A6CCFEA52F0AA9,
	OculusRestarter_get_Instance_m39A581FAA5C8E45A6603F7FA0EA6CAD784887796,
	OculusRestarter_PauseAndRestart_m28727BF6DE2EB3868DBCE80CB9FFB05F11800C9B,
	OculusRestarter_PauseAndRestartCoroutine_mE17893C50E82B62DABC119C745041BD732A93E72,
	OculusRestarter_RestartCoroutine_mED386FBEC1602B3DB023E06F05EC3043E6FFC027,
	OculusRestarter__ctor_mAFC467BA7F2B5C76CA9E93DF2B2FE5D4B991B05D,
	U3CPauseAndRestartCoroutineU3Ed__22__ctor_mE9A0F9B54589637F39AD77E522D257AAF233F600,
	U3CPauseAndRestartCoroutineU3Ed__22_System_IDisposable_Dispose_m2D4C564714370227159B843E3E3FD992C00C00BC,
	U3CPauseAndRestartCoroutineU3Ed__22_MoveNext_m594251DEF3855E1BD0F30EB7B45CEF06C51E94D7,
	U3CPauseAndRestartCoroutineU3Ed__22_U3CU3Em__Finally1_m17836219B16F0E2F40AFE29DDADEACD9F0CEA00E,
	U3CPauseAndRestartCoroutineU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5CE986377F44FF4B04030B2B1CB84A4630F803A9,
	U3CPauseAndRestartCoroutineU3Ed__22_System_Collections_IEnumerator_Reset_m437EE8315D091FAF8113083C558FB27ACDFD49AC,
	U3CPauseAndRestartCoroutineU3Ed__22_System_Collections_IEnumerator_get_Current_m1A0F4AF33C851DF304034CA195AB9A7DAA9E798A,
	U3CRestartCoroutineU3Ed__23__ctor_mFBCFBC73BF40550C580D2DBD6446E6765106D5DC,
	U3CRestartCoroutineU3Ed__23_System_IDisposable_Dispose_m3CB326B58BAA27EB44C80329052E1D2FBB7701B9,
	U3CRestartCoroutineU3Ed__23_MoveNext_mCF04F2683E7B1F137036D577AA4C7A54AD86E648,
	U3CRestartCoroutineU3Ed__23_U3CU3Em__Finally1_m5D43CE3EF7B89A3C817C148215ABE10F5F221332,
	U3CRestartCoroutineU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC4D68BC3E723F9EA64A307AAAEBE92D15E9C7C10,
	U3CRestartCoroutineU3Ed__23_System_Collections_IEnumerator_Reset_m8B74BC95BE0B29C902B5C7547EE3461E876BFC9F,
	U3CRestartCoroutineU3Ed__23_System_Collections_IEnumerator_get_Current_m5154B4EB20D93035D0D588C0498EAFF2D0F2C986,
	OculusSession_Update_m9956F648A7B5BE12D6D26ED0DEBFD7B20722E4C2,
	OculusSettings_GetStereoRenderingMode_mB4FF47E7831A653C90D7B08E336C40D66968F2FD,
	OculusSettings_Awake_m525FBEB4247E44DBC0D7E147581BD5EE751DCDB1,
	OculusSettings__ctor_mBD02A264D1619566E55F183F6BE3E2F9D8C9747B,
	OculusUsages__cctor_m95A9C1418F76CD4C0603062DD186F0FC4362316C,
	RegisterUpdateCallback_Initialize_m009EBB49477FAD1D7E03DA45EB80F15E6D28F521,
	RegisterUpdateCallback_Deinitialize_m5783A25B492DD2271AF20528CC7225AA69B48D2B,
	RegisterUpdateCallback_Update_m2DA82FDFC251E9F4D10F6F5ADB6A9F4B11BAD7ED,
	OculusHMD_get_userPresence_m7F39249DD91669248211F49FCE7CF0F2FB962CA9,
	OculusHMD_set_userPresence_m9290C2CA19C180C3F433C06C1387B793C377B1B1,
	OculusHMD_get_trackingState_m522656F3B4FDE39D98AAAC5B0AEBF736EA2A99FE,
	OculusHMD_set_trackingState_mF7C35D47F95EF4AB1BD2122794D656564DD53274,
	OculusHMD_get_isTracked_m0B008C62DFA3BAFCC31078CFCE09C1CDAB56996A,
	OculusHMD_set_isTracked_m1313DF189AC57BEA0C43BB0DF12750D276874D87,
	OculusHMD_get_devicePosition_mBA0C9580588FC2BEEC54686E49CC63813C92EAB9,
	OculusHMD_set_devicePosition_m918A873FF37E4CCBEFC31CE9648C27477B8C7314,
	OculusHMD_get_deviceRotation_mB1F8E76DA8BE83A478F69D9FE290C3FB5C8B79B2,
	OculusHMD_set_deviceRotation_m0AFE1471B78C206C82B8C633C7D14B7B0232A6A5,
	OculusHMD_get_deviceAngularVelocity_mBD840B48A888E7515D4FCC343F0BA967A878D1EC,
	OculusHMD_set_deviceAngularVelocity_m33600FC9A235F57CEBB940AEA15E633383BE05F6,
	OculusHMD_get_deviceAcceleration_m4D2C187908410B75C2ED86798FCD8137E0739CD2,
	OculusHMD_set_deviceAcceleration_mAF2696135E63B9CCF6B45046DAA6DC3FC23ED78F,
	OculusHMD_get_deviceAngularAcceleration_mF3DD84A65ED8250E0264A74EA41AA549C3F44834,
	OculusHMD_set_deviceAngularAcceleration_mD0F233B29A6EC778895F269F2E3490925385FD9A,
	OculusHMD_get_leftEyePosition_mBCFDDFC5B9315BEF3C05C2643470DF1B28C7FDB1,
	OculusHMD_set_leftEyePosition_m7867F1827CDE0F4238E53D5BAD345E8BEA54B3C4,
	OculusHMD_get_leftEyeRotation_m1FB4F4FFE39E6D0C9143D359A8BFCB6CAE98C063,
	OculusHMD_set_leftEyeRotation_m2E04AA2AE19E38541EE768517C4B255F66ADE7CD,
	OculusHMD_get_leftEyeAngularVelocity_m65669124F4A7F1C477523049BF699221DEA79AB1,
	OculusHMD_set_leftEyeAngularVelocity_m86434991A9C53F4ED9DBE90A13C92520518490F6,
	OculusHMD_get_leftEyeAcceleration_m354A16525174408749E9DD3A99DBAF24AAAA92D9,
	OculusHMD_set_leftEyeAcceleration_mF59FBC6DED7CF7A71596AB5BB315D8F053006F51,
	OculusHMD_get_leftEyeAngularAcceleration_mFE7FBC6202754B59AD009943E1AA984AFA98CBD1,
	OculusHMD_set_leftEyeAngularAcceleration_m927734C1C924AB419E01341BC804E6FF0DB683D7,
	OculusHMD_get_rightEyePosition_mD7850E4E14B72F635234355B1E85AD8BE9B7611E,
	OculusHMD_set_rightEyePosition_mB95F20F913814F66D3539E4607CCCEBCB9214F93,
	OculusHMD_get_rightEyeRotation_m65C0E66081ECFCBDB1EF48CDDE388DC3212EDBC2,
	OculusHMD_set_rightEyeRotation_m71BDA4076F8966E2FEF066B7B52FDB2EFEB01153,
	OculusHMD_get_rightEyeAngularVelocity_m622FD771B66B29F4E1D60F4EACA5A8F3E80F37FB,
	OculusHMD_set_rightEyeAngularVelocity_mFC4796AD89DC62611C2E68DA3B277DE0AFC40E02,
	OculusHMD_get_rightEyeAcceleration_m0528169F10236B496798D4E11BBFA17B9EDB33F3,
	OculusHMD_set_rightEyeAcceleration_m4138309C612132F26E395FC1BBFD4225DA1FF4A7,
	OculusHMD_get_rightEyeAngularAcceleration_m6AD0E2AB294227E6BFEF339BE60AA136B99483ED,
	OculusHMD_set_rightEyeAngularAcceleration_mD7816946FE952A923E73843F585C0595A4E7F056,
	OculusHMD_get_centerEyePosition_m67A05A1CE29590353AAD443F2A0D28580DBCAEAD,
	OculusHMD_set_centerEyePosition_mA45E2F647153F6E0554D0298B2B0B142D65ADA9D,
	OculusHMD_get_centerEyeRotation_m6E7F88828E91303B0794E5544CE29B6D69342219,
	OculusHMD_set_centerEyeRotation_m1C8D43D462011C04235B966F9112F2642099BCF0,
	OculusHMD_get_centerEyeAngularVelocity_m1551B0571B764CEFE99E7722AB51B035161B0A4F,
	OculusHMD_set_centerEyeAngularVelocity_mBDF3825C87D3AB4ABE0A9228209BDBFD0524C095,
	OculusHMD_get_centerEyeAcceleration_m77AD08FF91E43AADF8D59095FD5816919AE74CAA,
	OculusHMD_set_centerEyeAcceleration_mF1A58E3466531758291739C4FFF081D20BC88C99,
	OculusHMD_get_centerEyeAngularAcceleration_m47448A353E1259982772BCB30CEAD55FB708C4BF,
	OculusHMD_set_centerEyeAngularAcceleration_m40F38AC30FBE1E2CEB1E021017F87806DE82553F,
	OculusHMD_FinishSetup_m476E392C04F8BBB6E230BE6A37A90269820CA796,
	OculusHMD__ctor_mE497EDF74203D170F59426A5FFA99DA118BA67FD,
	OculusTouchController_get_thumbstick_m939BFF8DCC43E193BC031B70362533A38010262A,
	OculusTouchController_set_thumbstick_mB1DC0E2B0388C3E712E09FA31AD662AD3C78EB7B,
	OculusTouchController_get_trigger_m38BF0A4024B1305D766846E3598AFE7F4A4E62E6,
	OculusTouchController_set_trigger_m36A82C419138942FC5C7902C222EEC0245CE93D0,
	OculusTouchController_get_grip_m4B860D39D4F84449CFBBDEA3467C166C873216B7,
	OculusTouchController_set_grip_m8B89F611E21C4E2B4F27BD9CD160F6D4DDE45C92,
	OculusTouchController_get_primaryButton_m01427C69202849C92D72DE1478FA6FB5038036D7,
	OculusTouchController_set_primaryButton_mB6F2744464021A7A8FACADE2027D13E0B39DD825,
	OculusTouchController_get_secondaryButton_m26C2E19D5331735DAB1532DE315905C0661A8107,
	OculusTouchController_set_secondaryButton_mB1BB18678178C2AF400E7093676A52BF1980716C,
	OculusTouchController_get_gripPressed_mF5E4727862CE1342E0361ECEF1102E65B1DD26BC,
	OculusTouchController_set_gripPressed_mC8087872D17C17C53C1367FB2BC92CE390FAE633,
	OculusTouchController_get_start_m511FA9CD9C460F1B3769F07B93F29AF8D5125815,
	OculusTouchController_set_start_m53BC9D38204387A4C5039F0DF973A9E210152209,
	OculusTouchController_get_thumbstickClicked_m47426BF7B742628637449C6392E15CEC934E4FE8,
	OculusTouchController_set_thumbstickClicked_m53B0A4735B36B764BA5817A35098143A22BE35FD,
	OculusTouchController_get_primaryTouched_mF429EF61215210E7F0ED7A11340CDDEBAA6715FB,
	OculusTouchController_set_primaryTouched_m4CE99A47839B2B61CA93707B032AD57497D37D99,
	OculusTouchController_get_secondaryTouched_mD5D6FE26EA2DED25128C02B4E9A0E330AB71FBC3,
	OculusTouchController_set_secondaryTouched_mC746A8FC2F40014FFFB66DB3996CE3C2628EBD2E,
	OculusTouchController_get_triggerTouched_m6059D02224CCE182BA04B592E60034B0C831552E,
	OculusTouchController_set_triggerTouched_mC9CC071C7987A20B56CB0C01BF5CBBFE590DD7DE,
	OculusTouchController_get_triggerPressed_mE1870507BA8155BA0FF1531B73F5C0AA40EADA24,
	OculusTouchController_set_triggerPressed_m4717577F155237F5DCA58FF8595976978F2E3555,
	OculusTouchController_get_thumbstickTouched_mE4A1D89F50E0762A00F4A75365A46DDA48769802,
	OculusTouchController_set_thumbstickTouched_mD797890DA4D595468ABDEEBED7B3AF43DA6622F0,
	OculusTouchController_get_trackingState_m2E380481C56A4DA04393D4C4F4F328A27936A0D4,
	OculusTouchController_set_trackingState_m56952CEA07A22BE8B2A1800B71AE6E275CAE4396,
	OculusTouchController_get_isTracked_m30C41CD8955C5DDE32DBF9302325F12DF0BE73E2,
	OculusTouchController_set_isTracked_m7DED285C1364AEDDAF5EE3727424D1FAAEBD3218,
	OculusTouchController_get_devicePosition_m7929EA64D658AE437C9D31A9D803E48FD397A24C,
	OculusTouchController_set_devicePosition_m8F786136F2C3A7A0B69B8A3C776D16F22A6BC87B,
	OculusTouchController_get_deviceRotation_m286BC7BDF4583DC5D354921E1CAE96261D0CEA43,
	OculusTouchController_set_deviceRotation_mF0D9EB5B008A47FD93A8A4452043D4AE74C78A1D,
	OculusTouchController_get_deviceVelocity_m8339E69DA2AF4B8B9A35BC6CA322F393AA5AA290,
	OculusTouchController_set_deviceVelocity_m854AB855C5AB3CF32F5FAEEE599804EB5D365B62,
	OculusTouchController_get_deviceAngularVelocity_mD0EE32CDA1ABBC2346F474368EC15BF8A237431D,
	OculusTouchController_set_deviceAngularVelocity_m7D861B677871ECD4E2B81D0DE920851C95EEAE13,
	OculusTouchController_get_deviceAcceleration_m1633B2E2F9B5B9771A5F675582F9171EAB479328,
	OculusTouchController_set_deviceAcceleration_mB1C384EE21F68ACBB926856A01CC6E84956986A9,
	OculusTouchController_get_deviceAngularAcceleration_mDAC7A7A122090D72F0D46F18552E4651187CF865,
	OculusTouchController_set_deviceAngularAcceleration_m2F970918D95E3C1812A5D5926B4F83BB7CC0EDCF,
	OculusTouchController_FinishSetup_mCFB90E6EBDEB8F694B926E12EDE38DA7D3BD394A,
	OculusTouchController__ctor_m3DF25FC6EBC1D65AFDBDB011BCF9ABB1A85793C8,
	OculusTrackingReference_get_trackingState_m85616C0F80EE9E39BFF3E5196DABBC01E3533B8C,
	OculusTrackingReference_set_trackingState_mF946EDAC55B027ADB50C4BB7F45410583D77A574,
	OculusTrackingReference_get_isTracked_m6EF555E8C3A27C52275A23C2E98A4E66D2218BD7,
	OculusTrackingReference_set_isTracked_m1C8935A8187AF11A15875029D6C9219BD8DF235E,
	OculusTrackingReference_get_devicePosition_m2D81394973C28D465EA20F9A5F6C003D2ED67A02,
	OculusTrackingReference_set_devicePosition_mFF59FEA48A8981918702BF796DDABB7D753C5C25,
	OculusTrackingReference_get_deviceRotation_m73C171560EEF442ECCF1639FAA89C86388F88A54,
	OculusTrackingReference_set_deviceRotation_mCBB7204D4EEBBEAD6BB36DB51B38D2D114DEF919,
	OculusTrackingReference_FinishSetup_mC8CDDD455575F2A0FF9F9E3E996CDD200752BC33,
	OculusTrackingReference__ctor_m034388DDCC9F71833408E6C24C10F4F8C9FFE15E,
	OculusRemote_get_back_m9242761440A9EF3D714EE44A34FC959FF63FF09A,
	OculusRemote_set_back_mE211D9B585E73299D9D43AC28BFADA7A3C9C772F,
	OculusRemote_get_start_mD5EBA7E3B635341556D55C2A8244613CDF368A17,
	OculusRemote_set_start_mC4D1037E74795A524395BDEFA7CA88CB474F33F1,
	OculusRemote_get_touchpad_mFC90C7ED51F2F2AFBD8435493978CE0C14A2BE9B,
	OculusRemote_set_touchpad_m174C971799B16F5032D979AC7BA2F8C8AEF020A9,
	OculusRemote_FinishSetup_m5C9517EF579C721B0ADC03E00EBFF1F0C5BA7F7F,
	OculusRemote__ctor_m2BCE3ECEF5DBFEAD59595E747A26E7A314788A3C,
	OculusGoController__ctor_mA08B0DA17D85ACD7166DE362B280B0E019F02719,
	OculusHMDExtended_get_back_mFDBD32A74252025AA1F9DA1920F162FFF60AF777,
	OculusHMDExtended_set_back_m0E37910E7CC993017981633932E6AF6C5602CFB3,
	OculusHMDExtended_get_touchpad_m9DBF0B06449BFAD9A6237A237ACA2C2B277C1332,
	OculusHMDExtended_set_touchpad_mB34658748A3749934B64A0CC7477D62A30EDF04D,
	OculusHMDExtended_FinishSetup_mA390A06FAC20D884C637738168848C44E5BBDD3C,
	OculusHMDExtended__ctor_mB98D9DF9D7D27CD8EE8E72E0111C29E94D7FB64A,
	GearVRTrackedController_get_touchpad_m21DEB0B6AEBE75FE0614851648893C2F40FA75C9,
	GearVRTrackedController_set_touchpad_m11568D5395BF554EE37F2170411C726B9248304C,
	GearVRTrackedController_get_trigger_mD4688E98CE65F29BF18A00A5BB6E7C498C4B73A3,
	GearVRTrackedController_set_trigger_m51C37516D561D90C027C9B28089149F9DFED604C,
	GearVRTrackedController_get_back_mC93C819F033288454AB46C03E649CB5FAA330DB7,
	GearVRTrackedController_set_back_mE5906CC7A6A1A1526A3C67B1FD8222CCD36001B2,
	GearVRTrackedController_get_triggerPressed_m1129EFEBFC62FCB0E3A7D9341CCCDC3CA763A3F5,
	GearVRTrackedController_set_triggerPressed_mEF11C6119612BF985138FC3A1B477DF5F2D9285E,
	GearVRTrackedController_get_touchpadClicked_m752DB8A8A2304B4393C3C147BD0FBCAC57659EC0,
	GearVRTrackedController_set_touchpadClicked_mD2C1482CAC771AEACF36BE35C37CF0D4029A74B4,
	GearVRTrackedController_get_touchpadTouched_m332094F49118746F89F231CA4BF30F9F6B4CBE98,
	GearVRTrackedController_set_touchpadTouched_m217D58D7149896C50E7A891B7A73409006E850B5,
	GearVRTrackedController_get_trackingState_m1593CF906B9901952CB4D38512C6A2651B0EA093,
	GearVRTrackedController_set_trackingState_m1E7F9A50F7E2AC02B6A39306FCBA27E274DF729A,
	GearVRTrackedController_get_isTracked_m75DA6218446F11E66470D5CCEFD2608A9E1EA664,
	GearVRTrackedController_set_isTracked_m87BE49E18745B46D49E3495951F824C1EDBBAF41,
	GearVRTrackedController_get_devicePosition_m9DD0861D1F1EAA406FB26900C725DEAD80A42B33,
	GearVRTrackedController_set_devicePosition_m795223AB6F315C47E507787D0F29DFBD3B1F7E7E,
	GearVRTrackedController_get_deviceRotation_m7EE51AFFA124012AEC747E92EE966840624A4C74,
	GearVRTrackedController_set_deviceRotation_mFCB0BA94E4FB6D8DAB26B19B9408245C968D05B9,
	GearVRTrackedController_get_deviceAngularVelocity_m90B562425563816EAFD84B256CECA612055758E4,
	GearVRTrackedController_set_deviceAngularVelocity_m5C22DE0A26841683FAC1C4FB9D4DD56641C9BD8C,
	GearVRTrackedController_get_deviceAcceleration_m6526C5074963749493C4524C9411828196D2B006,
	GearVRTrackedController_set_deviceAcceleration_m009452ABC6F9A06BF9F984623582ABB365CF845B,
	GearVRTrackedController_get_deviceAngularAcceleration_m38754F45623C53D1913DC0A3E9DCAC9BEA0C5BF9,
	GearVRTrackedController_set_deviceAngularAcceleration_mE3BFDC78B4C94A855790F2EE3DE26DA4F35BBB25,
	GearVRTrackedController_FinishSetup_m435CA1B37517D6EA68D43DDBBC4A334C1B5F955D,
	GearVRTrackedController__ctor_m35059DBECC8470F3A2695CF33528C805722D6B6B,
};
static const int32_t s_InvokerIndices[304] = 
{
	7191,
	7839,
	7712,
	7338,
	7839,
	7715,
	7715,
	7715,
	7715,
	7819,
	7879,
	5151,
	7879,
	7819,
	6705,
	7819,
	7704,
	7704,
	7879,
	7879,
	7879,
	7879,
	7879,
	7839,
	5077,
	5077,
	4989,
	4989,
	4989,
	4989,
	7879,
	5077,
	5151,
	7879,
	7344,
	7344,
	7337,
	7352,
	7337,
	7879,
	7847,
	7847,
	5151,
	7879,
	7868,
	7868,
	7868,
	7868,
	7868,
	7868,
	7819,
	7868,
	7839,
	7839,
	7868,
	7868,
	7868,
	7868,
	7868,
	7868,
	7868,
	7868,
	7868,
	7704,
	7868,
	7868,
	7868,
	7868,
	7868,
	7868,
	7868,
	7704,
	6149,
	6149,
	7819,
	7347,
	7879,
	7728,
	7428,
	7428,
	7715,
	7704,
	7704,
	7338,
	7819,
	7819,
	6705,
	7819,
	7704,
	7819,
	6714,
	7352,
	7337,
	7839,
	7819,
	7712,
	7839,
	7704,
	7819,
	6149,
	6149,
	7819,
	7347,
	7879,
	7728,
	7428,
	7428,
	7715,
	7704,
	7704,
	7338,
	7819,
	7819,
	6705,
	7819,
	7704,
	7819,
	6714,
	7352,
	7337,
	7839,
	7819,
	7712,
	7839,
	7704,
	7819,
	7879,
	5151,
	4989,
	7868,
	7721,
	7839,
	7847,
	5151,
	3699,
	3681,
	5151,
	4071,
	5151,
	4989,
	5151,
	5077,
	5151,
	5077,
	4071,
	5151,
	4989,
	5151,
	5077,
	5151,
	5077,
	7879,
	5138,
	5151,
	5151,
	7879,
	7879,
	7879,
	7879,
	5077,
	4101,
	5077,
	4101,
	5077,
	4101,
	5077,
	4101,
	5077,
	4101,
	5077,
	4101,
	5077,
	4101,
	5077,
	4101,
	5077,
	4101,
	5077,
	4101,
	5077,
	4101,
	5077,
	4101,
	5077,
	4101,
	5077,
	4101,
	5077,
	4101,
	5077,
	4101,
	5077,
	4101,
	5077,
	4101,
	5077,
	4101,
	5077,
	4101,
	5077,
	4101,
	5077,
	4101,
	5077,
	4101,
	5151,
	5151,
	5077,
	4101,
	5077,
	4101,
	5077,
	4101,
	5077,
	4101,
	5077,
	4101,
	5077,
	4101,
	5077,
	4101,
	5077,
	4101,
	5077,
	4101,
	5077,
	4101,
	5077,
	4101,
	5077,
	4101,
	5077,
	4101,
	5077,
	4101,
	5077,
	4101,
	5077,
	4101,
	5077,
	4101,
	5077,
	4101,
	5077,
	4101,
	5077,
	4101,
	5077,
	4101,
	5151,
	5151,
	5077,
	4101,
	5077,
	4101,
	5077,
	4101,
	5077,
	4101,
	5151,
	5151,
	5077,
	4101,
	5077,
	4101,
	5077,
	4101,
	5151,
	5151,
	5151,
	5077,
	4101,
	5077,
	4101,
	5151,
	5151,
	5077,
	4101,
	5077,
	4101,
	5077,
	4101,
	5077,
	4101,
	5077,
	4101,
	5077,
	4101,
	5077,
	4101,
	5077,
	4101,
	5077,
	4101,
	5077,
	4101,
	5077,
	4101,
	5077,
	4101,
	5077,
	4101,
	5151,
	5151,
};
extern const CustomAttributesCacheGenerator g_Unity_XR_Oculus_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_Unity_XR_Oculus_CodeGenModule;
const Il2CppCodeGenModule g_Unity_XR_Oculus_CodeGenModule = 
{
	"Unity.XR.Oculus.dll",
	304,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_Unity_XR_Oculus_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
