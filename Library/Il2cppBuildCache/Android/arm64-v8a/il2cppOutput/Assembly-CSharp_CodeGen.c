﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void FixNonUniformScale::OnDrawGizmosSelected()
extern void FixNonUniformScale_OnDrawGizmosSelected_mD129C37D8D8F97A675783E56CB5CE7961A8C5504 (void);
// 0x00000002 System.Void FixNonUniformScale::MakeUniform()
extern void FixNonUniformScale_MakeUniform_mE53E4A87F82BCFE5BD27F76F5F5E95EB4192A217 (void);
// 0x00000003 System.Void FixNonUniformScale::.ctor()
extern void FixNonUniformScale__ctor_m8EDCB00E217D23CE028EE27B8D08A7353A44575D (void);
// 0x00000004 System.Void DebugUIBuilder::Awake()
extern void DebugUIBuilder_Awake_m403F18586F94598A6A54C5B1899CF76032848602 (void);
// 0x00000005 System.Void DebugUIBuilder::Show()
extern void DebugUIBuilder_Show_mF284EB0ED0943893BD5799EBFCCB1DB6B192DC2E (void);
// 0x00000006 System.Void DebugUIBuilder::Hide()
extern void DebugUIBuilder_Hide_m23BD4DC6DB4E714215B97C9A37417D70356FD0C2 (void);
// 0x00000007 System.Void DebugUIBuilder::StackedRelayout()
extern void DebugUIBuilder_StackedRelayout_mDE795B974E8386D3A693380CCFE57E98E67AAC60 (void);
// 0x00000008 System.Void DebugUIBuilder::PanelCentricRelayout()
extern void DebugUIBuilder_PanelCentricRelayout_mCE6C50E7BA6C02D8CDD3F32E46A045B4CF2104B0 (void);
// 0x00000009 System.Void DebugUIBuilder::Relayout()
extern void DebugUIBuilder_Relayout_m3937EE57F0E4C6F007BE6CB4CF0940FBEC300C28 (void);
// 0x0000000A System.Void DebugUIBuilder::AddRect(UnityEngine.RectTransform,System.Int32)
extern void DebugUIBuilder_AddRect_m877BEB5A300FE474E3A148ABB8BA0C0FBB224EA9 (void);
// 0x0000000B UnityEngine.RectTransform DebugUIBuilder::AddButton(System.String,DebugUIBuilder/OnClick,System.Int32,System.Int32,System.Boolean)
extern void DebugUIBuilder_AddButton_m76A914A3B5BA3E19134F8E109BA557AA842E2014 (void);
// 0x0000000C UnityEngine.RectTransform DebugUIBuilder::AddLabel(System.String,System.Int32)
extern void DebugUIBuilder_AddLabel_m6A98E248939BD65B054A4B4C4420D950F4811CAD (void);
// 0x0000000D UnityEngine.RectTransform DebugUIBuilder::AddSlider(System.String,System.Single,System.Single,DebugUIBuilder/OnSlider,System.Boolean,System.Int32)
extern void DebugUIBuilder_AddSlider_mC4E1DB56081E390B06F2B7D02C67BA683D84686F (void);
// 0x0000000E UnityEngine.RectTransform DebugUIBuilder::AddDivider(System.Int32)
extern void DebugUIBuilder_AddDivider_m858451240D2EAE0D367F8145E2AC917929B15DB2 (void);
// 0x0000000F UnityEngine.RectTransform DebugUIBuilder::AddToggle(System.String,DebugUIBuilder/OnToggleValueChange,System.Int32)
extern void DebugUIBuilder_AddToggle_m3A79A54CC94114F24B771CD465F40B001CBFCAFD (void);
// 0x00000010 UnityEngine.RectTransform DebugUIBuilder::AddToggle(System.String,DebugUIBuilder/OnToggleValueChange,System.Boolean,System.Int32)
extern void DebugUIBuilder_AddToggle_mAA47BCD38AB222645D00709AFB422F4D5CF02DB8 (void);
// 0x00000011 UnityEngine.RectTransform DebugUIBuilder::AddRadio(System.String,System.String,DebugUIBuilder/OnToggleValueChange,System.Int32)
extern void DebugUIBuilder_AddRadio_m783EACB19FD91F15BD9BE7EDABCDD6B05D639639 (void);
// 0x00000012 UnityEngine.RectTransform DebugUIBuilder::AddTextField(System.String,System.Int32)
extern void DebugUIBuilder_AddTextField_m726C38770CC36156FC5B71739F63536EA55540A1 (void);
// 0x00000013 System.Void DebugUIBuilder::ToggleLaserPointer(System.Boolean)
extern void DebugUIBuilder_ToggleLaserPointer_m19D2F9E314DF85DEE576A53C5E719685B14A2B55 (void);
// 0x00000014 System.Void DebugUIBuilder::.ctor()
extern void DebugUIBuilder__ctor_m95BD850859B88DCA361D481FC4A0C998DB46496E (void);
// 0x00000015 System.Void DebugUIBuilder/OnClick::.ctor(System.Object,System.IntPtr)
extern void OnClick__ctor_mC5739B4F2FE456031D0F032D2B1335A8C7E07F76 (void);
// 0x00000016 System.Void DebugUIBuilder/OnClick::Invoke()
extern void OnClick_Invoke_mDC64AF5D49F6C27A1C15E0695D0B9B8A51958AC9 (void);
// 0x00000017 System.IAsyncResult DebugUIBuilder/OnClick::BeginInvoke(System.AsyncCallback,System.Object)
extern void OnClick_BeginInvoke_m26A6065C75CFBA6B575C90229E2689F93697C002 (void);
// 0x00000018 System.Void DebugUIBuilder/OnClick::EndInvoke(System.IAsyncResult)
extern void OnClick_EndInvoke_m9DC1A8BFEE9B5B1036401A570221ECB4C32AD697 (void);
// 0x00000019 System.Void DebugUIBuilder/OnToggleValueChange::.ctor(System.Object,System.IntPtr)
extern void OnToggleValueChange__ctor_mDED41EA7D290F566BBDE14F48F7DAB95A65513A3 (void);
// 0x0000001A System.Void DebugUIBuilder/OnToggleValueChange::Invoke(UnityEngine.UI.Toggle)
extern void OnToggleValueChange_Invoke_m7C6B23451E232EEEB6C2F95BAD41C5E5716303CE (void);
// 0x0000001B System.IAsyncResult DebugUIBuilder/OnToggleValueChange::BeginInvoke(UnityEngine.UI.Toggle,System.AsyncCallback,System.Object)
extern void OnToggleValueChange_BeginInvoke_m33F94F819A1EB1D23BC216963765EDB1740702C8 (void);
// 0x0000001C System.Void DebugUIBuilder/OnToggleValueChange::EndInvoke(System.IAsyncResult)
extern void OnToggleValueChange_EndInvoke_mA11DE32406ADE7239AD2833535BF1C49DB4E292B (void);
// 0x0000001D System.Void DebugUIBuilder/OnSlider::.ctor(System.Object,System.IntPtr)
extern void OnSlider__ctor_m748CDFE6C061BCC4EAB8C70EDEF6413DBC5748C8 (void);
// 0x0000001E System.Void DebugUIBuilder/OnSlider::Invoke(System.Single)
extern void OnSlider_Invoke_mC63EB553A881C3BD597451B10831BAD4EC03F5C9 (void);
// 0x0000001F System.IAsyncResult DebugUIBuilder/OnSlider::BeginInvoke(System.Single,System.AsyncCallback,System.Object)
extern void OnSlider_BeginInvoke_mFA997752B26047A4D133433B32C14383D0E1F7DB (void);
// 0x00000020 System.Void DebugUIBuilder/OnSlider::EndInvoke(System.IAsyncResult)
extern void OnSlider_EndInvoke_mCC34E0F20749EC0287A3FDC4FDC354A88202FEB2 (void);
// 0x00000021 System.Void DebugUIBuilder/ActiveUpdate::.ctor(System.Object,System.IntPtr)
extern void ActiveUpdate__ctor_mC45D2B4C2665FD2DC12090EAB9C0B5044514DF19 (void);
// 0x00000022 System.Boolean DebugUIBuilder/ActiveUpdate::Invoke()
extern void ActiveUpdate_Invoke_m769E06EC3AB44D98C5C452D5B2336809F87C1E18 (void);
// 0x00000023 System.IAsyncResult DebugUIBuilder/ActiveUpdate::BeginInvoke(System.AsyncCallback,System.Object)
extern void ActiveUpdate_BeginInvoke_m720D31DDBFECDC31563B554DE0450793E969F877 (void);
// 0x00000024 System.Boolean DebugUIBuilder/ActiveUpdate::EndInvoke(System.IAsyncResult)
extern void ActiveUpdate_EndInvoke_mFA8C1E60854662BE8B396F7BC93367E235ACB707 (void);
// 0x00000025 System.Void DebugUIBuilder/<>c__DisplayClass41_0::.ctor()
extern void U3CU3Ec__DisplayClass41_0__ctor_m6527E632BD200CEC2C12AA6E5775DD1A9B9385A2 (void);
// 0x00000026 System.Void DebugUIBuilder/<>c__DisplayClass41_0::<AddButton>b__0()
extern void U3CU3Ec__DisplayClass41_0_U3CAddButtonU3Eb__0_m75C21986A5DBE0F1E571B7F9AFF69149AB1AF492 (void);
// 0x00000027 System.Void DebugUIBuilder/<>c__DisplayClass43_0::.ctor()
extern void U3CU3Ec__DisplayClass43_0__ctor_m84C5DC7D04C357D12494C7BF4F2A5871F18BB3A5 (void);
// 0x00000028 System.Void DebugUIBuilder/<>c__DisplayClass43_0::<AddSlider>b__0(System.Single)
extern void U3CU3Ec__DisplayClass43_0_U3CAddSliderU3Eb__0_m8D2A0F94778703422A6F82B8A92A7AE0E9E56D28 (void);
// 0x00000029 System.Void DebugUIBuilder/<>c__DisplayClass45_0::.ctor()
extern void U3CU3Ec__DisplayClass45_0__ctor_m57D3B15225A9BC9E3DEE6B197DFC64DF0A3634E4 (void);
// 0x0000002A System.Void DebugUIBuilder/<>c__DisplayClass45_0::<AddToggle>b__0(System.Boolean)
extern void U3CU3Ec__DisplayClass45_0_U3CAddToggleU3Eb__0_m0B89042D3D5BFDF300DB436C5C1B9F2C8B3D4C2F (void);
// 0x0000002B System.Void DebugUIBuilder/<>c__DisplayClass46_0::.ctor()
extern void U3CU3Ec__DisplayClass46_0__ctor_m5B53CA1415680044F78C8FC6CA4D4B242D012563 (void);
// 0x0000002C System.Void DebugUIBuilder/<>c__DisplayClass46_0::<AddToggle>b__0(System.Boolean)
extern void U3CU3Ec__DisplayClass46_0_U3CAddToggleU3Eb__0_mC2735E33A83EA7341D913BD4001D5D977C935174 (void);
// 0x0000002D System.Void DebugUIBuilder/<>c__DisplayClass47_0::.ctor()
extern void U3CU3Ec__DisplayClass47_0__ctor_m2DBE10617ACA2335CE8026196DE0F91419618249 (void);
// 0x0000002E System.Void DebugUIBuilder/<>c__DisplayClass47_0::<AddRadio>b__0(System.Boolean)
extern void U3CU3Ec__DisplayClass47_0_U3CAddRadioU3Eb__0_mDD9D163D69AF5B03BEF971FE625C79630C300C89 (void);
// 0x0000002F System.Void HandedInputSelector::Start()
extern void HandedInputSelector_Start_m9F0E0845F78B86C943B82C6C00D00C2755379664 (void);
// 0x00000030 System.Void HandedInputSelector::Update()
extern void HandedInputSelector_Update_m4AD93E3F8125E142011798D2CABF248A3DFB73C6 (void);
// 0x00000031 System.Void HandedInputSelector::SetActiveController(OVRInput/Controller)
extern void HandedInputSelector_SetActiveController_m675CE04EEBD60BA97F10205CFCC8E0360A57640D (void);
// 0x00000032 System.Void HandedInputSelector::.ctor()
extern void HandedInputSelector__ctor_mFB9B45C64E23FAA0F3AC5FC734673E1A4EC7B6FD (void);
// 0x00000033 System.Void LaserPointer::set_laserBeamBehavior(LaserPointer/LaserBeamBehavior)
extern void LaserPointer_set_laserBeamBehavior_mAEDF3DABF1D4C13A02570DFB7539EA32D96C3EB7 (void);
// 0x00000034 LaserPointer/LaserBeamBehavior LaserPointer::get_laserBeamBehavior()
extern void LaserPointer_get_laserBeamBehavior_mC452C41CF65C2D1E3D13990D519BCE2B5459F4CE (void);
// 0x00000035 System.Void LaserPointer::Awake()
extern void LaserPointer_Awake_mB3F93300A58061D247AAD53BA961CA67148670CA (void);
// 0x00000036 System.Void LaserPointer::Start()
extern void LaserPointer_Start_mC859C4A4E40BCB5E1A39D8B6135D256178A1B0C1 (void);
// 0x00000037 System.Void LaserPointer::SetCursorStartDest(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern void LaserPointer_SetCursorStartDest_m58BE76CF5DC6A138D6F39812B510E58A8195CA18 (void);
// 0x00000038 System.Void LaserPointer::SetCursorRay(UnityEngine.Transform)
extern void LaserPointer_SetCursorRay_m73C5954CB43A8D590BF1FF9674846886C869E3CA (void);
// 0x00000039 System.Void LaserPointer::LateUpdate()
extern void LaserPointer_LateUpdate_m922E6CC571CF3BF292D1E4A89ABA3DE721940E40 (void);
// 0x0000003A System.Void LaserPointer::UpdateLaserBeam(UnityEngine.Vector3,UnityEngine.Vector3)
extern void LaserPointer_UpdateLaserBeam_m2EC759248E0200A9EDC06D995886AED2C3BBE84D (void);
// 0x0000003B System.Void LaserPointer::OnDisable()
extern void LaserPointer_OnDisable_m386D5CB4ED7CC015BDD625F9BB173D48A44963BC (void);
// 0x0000003C System.Void LaserPointer::OnInputFocusLost()
extern void LaserPointer_OnInputFocusLost_m03F3165CDC0823985D85DBDECE16679C4A574EC1 (void);
// 0x0000003D System.Void LaserPointer::OnInputFocusAcquired()
extern void LaserPointer_OnInputFocusAcquired_m253C322D76D9D1A00FD1041F30DE52EC189857D8 (void);
// 0x0000003E System.Void LaserPointer::OnDestroy()
extern void LaserPointer_OnDestroy_mA7F7B1BDDB6897671BC633CF81F15F3C2AC8A27E (void);
// 0x0000003F System.Void LaserPointer::.ctor()
extern void LaserPointer__ctor_m9834E61B31A7B83B51FC2BEB7B9F59FCE781B66A (void);
// 0x00000040 System.Void CharacterCameraConstraint::.ctor()
extern void CharacterCameraConstraint__ctor_mB4775E6A3FD15170E7EDA1FF7083E59DC9D9356B (void);
// 0x00000041 System.Void CharacterCameraConstraint::Awake()
extern void CharacterCameraConstraint_Awake_m11B961F3351B1DCF59D2F9F25F6129261B996D8C (void);
// 0x00000042 System.Void CharacterCameraConstraint::OnEnable()
extern void CharacterCameraConstraint_OnEnable_mA51E71B397BB043BDCDBD57881301ACEDBB7A937 (void);
// 0x00000043 System.Void CharacterCameraConstraint::OnDisable()
extern void CharacterCameraConstraint_OnDisable_m0C85B3913ABB8DD8EF6AB5A38003925365C97700 (void);
// 0x00000044 System.Void CharacterCameraConstraint::CameraUpdate()
extern void CharacterCameraConstraint_CameraUpdate_m7D846796F0337138A375E39F69E31C68B0EBA41B (void);
// 0x00000045 System.Boolean CharacterCameraConstraint::CheckCameraOverlapped()
extern void CharacterCameraConstraint_CheckCameraOverlapped_m7EF5BBE41ADD0F7610CE1CC6EE39CA631513C783 (void);
// 0x00000046 System.Boolean CharacterCameraConstraint::CheckCameraNearClipping(System.Single&)
extern void CharacterCameraConstraint_CheckCameraNearClipping_m3BD4B953D24FBF24CAB9E86824E600EAFE549483 (void);
// 0x00000047 System.Void LocomotionController::Start()
extern void LocomotionController_Start_mFF771607088B6AC1B9647FFD5262BD156C32990B (void);
// 0x00000048 System.Void LocomotionController::.ctor()
extern void LocomotionController__ctor_m5AA73F4FF00769A834D5732FC4BD08BDD4634BDF (void);
// 0x00000049 System.Void LocomotionTeleport::EnableMovement(System.Boolean,System.Boolean,System.Boolean,System.Boolean)
extern void LocomotionTeleport_EnableMovement_m8ADC24447284A9EE7AEECCED581CC2B51326FDC9 (void);
// 0x0000004A System.Void LocomotionTeleport::EnableRotation(System.Boolean,System.Boolean,System.Boolean,System.Boolean)
extern void LocomotionTeleport_EnableRotation_m1B04E1D2444E02AC4407BEE5362EF08441C2AFA5 (void);
// 0x0000004B LocomotionTeleport/States LocomotionTeleport::get_CurrentState()
extern void LocomotionTeleport_get_CurrentState_mCEDC22238BFC0EB60D9DE2616698C71D0CEF0BA8 (void);
// 0x0000004C System.Void LocomotionTeleport::set_CurrentState(LocomotionTeleport/States)
extern void LocomotionTeleport_set_CurrentState_m6D752383FDB712A2347A0CAC2F7734E26274DA77 (void);
// 0x0000004D System.Void LocomotionTeleport::add_UpdateTeleportDestination(System.Action`4<System.Boolean,System.Nullable`1<UnityEngine.Vector3>,System.Nullable`1<UnityEngine.Quaternion>,System.Nullable`1<UnityEngine.Quaternion>>)
extern void LocomotionTeleport_add_UpdateTeleportDestination_m5CF34E190EF7950C68B07AA59440C8904B000EDA (void);
// 0x0000004E System.Void LocomotionTeleport::remove_UpdateTeleportDestination(System.Action`4<System.Boolean,System.Nullable`1<UnityEngine.Vector3>,System.Nullable`1<UnityEngine.Quaternion>,System.Nullable`1<UnityEngine.Quaternion>>)
extern void LocomotionTeleport_remove_UpdateTeleportDestination_m6D4B9E576EAFC2F0F8812B24237AE65840368C8D (void);
// 0x0000004F System.Void LocomotionTeleport::OnUpdateTeleportDestination(System.Boolean,System.Nullable`1<UnityEngine.Vector3>,System.Nullable`1<UnityEngine.Quaternion>,System.Nullable`1<UnityEngine.Quaternion>)
extern void LocomotionTeleport_OnUpdateTeleportDestination_m14192CB82A0C6B54908D63B87428912B984459D1 (void);
// 0x00000050 UnityEngine.Quaternion LocomotionTeleport::get_DestinationRotation()
extern void LocomotionTeleport_get_DestinationRotation_m713A8A847E529FFF3636C695730856FF0F591CC6 (void);
// 0x00000051 LocomotionController LocomotionTeleport::get_LocomotionController()
extern void LocomotionTeleport_get_LocomotionController_mED79293C6EA765335D25527FC7111EF971257498 (void);
// 0x00000052 System.Void LocomotionTeleport::set_LocomotionController(LocomotionController)
extern void LocomotionTeleport_set_LocomotionController_m460991F216978FB103CB533BF9DEF4BC93DA814B (void);
// 0x00000053 System.Boolean LocomotionTeleport::AimCollisionTest(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.LayerMask,UnityEngine.RaycastHit&)
extern void LocomotionTeleport_AimCollisionTest_m230A548C1CE75228EFA5CA239FA7171FB0FB4D3E (void);
// 0x00000054 System.Void LocomotionTeleport::LogState(System.String)
extern void LocomotionTeleport_LogState_m2272C15F12A05D8C031A50FB08AE122C1FCA2EA7 (void);
// 0x00000055 System.Void LocomotionTeleport::CreateNewTeleportDestination()
extern void LocomotionTeleport_CreateNewTeleportDestination_m985E3B33C931F465655EF6156CC75DC3B7A1D99D (void);
// 0x00000056 System.Void LocomotionTeleport::DeactivateDestination()
extern void LocomotionTeleport_DeactivateDestination_m4F8704E73CA946449DF98D49763F10EF733987A0 (void);
// 0x00000057 System.Void LocomotionTeleport::RecycleTeleportDestination(TeleportDestination)
extern void LocomotionTeleport_RecycleTeleportDestination_m6DA8327336475283A325074C859CD4D281BA9879 (void);
// 0x00000058 System.Void LocomotionTeleport::EnableMotion(System.Boolean,System.Boolean)
extern void LocomotionTeleport_EnableMotion_m9ACD9EF8F110F90CD996939A058242FF89BCEA22 (void);
// 0x00000059 System.Void LocomotionTeleport::Awake()
extern void LocomotionTeleport_Awake_m9C80807870EFB91801F476BAE526C31D9E29E66E (void);
// 0x0000005A System.Void LocomotionTeleport::OnEnable()
extern void LocomotionTeleport_OnEnable_mFCFB80918BF2D1AF964CB043299BBE69CC83FDB5 (void);
// 0x0000005B System.Void LocomotionTeleport::OnDisable()
extern void LocomotionTeleport_OnDisable_mA887B447817410EA3AC80DF9C6B2390D0B26788D (void);
// 0x0000005C System.Void LocomotionTeleport::add_EnterStateReady(System.Action)
extern void LocomotionTeleport_add_EnterStateReady_m694F17237FD2AEEDCADA5B5BF9D32F91E249BD39 (void);
// 0x0000005D System.Void LocomotionTeleport::remove_EnterStateReady(System.Action)
extern void LocomotionTeleport_remove_EnterStateReady_m02A5BF8752C71DC2A54CFA7325F737BC3B6099C1 (void);
// 0x0000005E System.Collections.IEnumerator LocomotionTeleport::ReadyStateCoroutine()
extern void LocomotionTeleport_ReadyStateCoroutine_mEF056F1254CED1ABD142E517BDEB2815AAE24A0C (void);
// 0x0000005F System.Void LocomotionTeleport::add_EnterStateAim(System.Action)
extern void LocomotionTeleport_add_EnterStateAim_m9EEEAB1100A4CC7635EF023A0078BCE95434C5A2 (void);
// 0x00000060 System.Void LocomotionTeleport::remove_EnterStateAim(System.Action)
extern void LocomotionTeleport_remove_EnterStateAim_mBBF9AA5970D87E22D59FC766F8278643C61B6250 (void);
// 0x00000061 System.Void LocomotionTeleport::add_UpdateAimData(System.Action`1<LocomotionTeleport/AimData>)
extern void LocomotionTeleport_add_UpdateAimData_mF17892665B70341AB40666AA39187787B9E060E5 (void);
// 0x00000062 System.Void LocomotionTeleport::remove_UpdateAimData(System.Action`1<LocomotionTeleport/AimData>)
extern void LocomotionTeleport_remove_UpdateAimData_mBFAF399B9B0200FDC43ADEF36B4123F60941D24C (void);
// 0x00000063 System.Void LocomotionTeleport::OnUpdateAimData(LocomotionTeleport/AimData)
extern void LocomotionTeleport_OnUpdateAimData_m76ED9D517FF45B8A7CE7757BD0E8D147892F90A8 (void);
// 0x00000064 System.Void LocomotionTeleport::add_ExitStateAim(System.Action)
extern void LocomotionTeleport_add_ExitStateAim_m93E127BBB502440C22348EDF011EF6ED12E1E607 (void);
// 0x00000065 System.Void LocomotionTeleport::remove_ExitStateAim(System.Action)
extern void LocomotionTeleport_remove_ExitStateAim_m86CEE4BAF96D0CDAB9CEF69231DDB1A3FCE7964F (void);
// 0x00000066 System.Collections.IEnumerator LocomotionTeleport::AimStateCoroutine()
extern void LocomotionTeleport_AimStateCoroutine_mC98C40FA612A3DC181B2A622B2AFC2B78C72D182 (void);
// 0x00000067 System.Void LocomotionTeleport::add_EnterStateCancelAim(System.Action)
extern void LocomotionTeleport_add_EnterStateCancelAim_m98AE021C2A1274CE289ADBDC891DCC027AB06668 (void);
// 0x00000068 System.Void LocomotionTeleport::remove_EnterStateCancelAim(System.Action)
extern void LocomotionTeleport_remove_EnterStateCancelAim_mF547CBA5F10D4A7374318065008202266E14E729 (void);
// 0x00000069 System.Collections.IEnumerator LocomotionTeleport::CancelAimStateCoroutine()
extern void LocomotionTeleport_CancelAimStateCoroutine_m89E30D85AE2200D46C6B6CEC487CA86309336DFE (void);
// 0x0000006A System.Void LocomotionTeleport::add_EnterStatePreTeleport(System.Action)
extern void LocomotionTeleport_add_EnterStatePreTeleport_m5436C07729E835E670F3D438A7EAC8743646071F (void);
// 0x0000006B System.Void LocomotionTeleport::remove_EnterStatePreTeleport(System.Action)
extern void LocomotionTeleport_remove_EnterStatePreTeleport_m1BB7C07D45DA26BA3FC294749B845A6F04E9035D (void);
// 0x0000006C System.Collections.IEnumerator LocomotionTeleport::PreTeleportStateCoroutine()
extern void LocomotionTeleport_PreTeleportStateCoroutine_m4D4467A0B6570EEE09C5366CF0B443665718D27C (void);
// 0x0000006D System.Void LocomotionTeleport::add_EnterStateCancelTeleport(System.Action)
extern void LocomotionTeleport_add_EnterStateCancelTeleport_mE447D28CF848635B7BF086DA4CC7592E174225F5 (void);
// 0x0000006E System.Void LocomotionTeleport::remove_EnterStateCancelTeleport(System.Action)
extern void LocomotionTeleport_remove_EnterStateCancelTeleport_mD9C6A70C33006402CC14DF75C06AA68EE0CB38B7 (void);
// 0x0000006F System.Collections.IEnumerator LocomotionTeleport::CancelTeleportStateCoroutine()
extern void LocomotionTeleport_CancelTeleportStateCoroutine_m376EE91D4337D6E0DFE214FAF8277E4AD3A80F09 (void);
// 0x00000070 System.Void LocomotionTeleport::add_EnterStateTeleporting(System.Action)
extern void LocomotionTeleport_add_EnterStateTeleporting_m2939235BCF9A6D936A57686136AAF38FA38F7AE9 (void);
// 0x00000071 System.Void LocomotionTeleport::remove_EnterStateTeleporting(System.Action)
extern void LocomotionTeleport_remove_EnterStateTeleporting_mE99129EE268F102CFBF05222E234285FD41EA602 (void);
// 0x00000072 System.Collections.IEnumerator LocomotionTeleport::TeleportingStateCoroutine()
extern void LocomotionTeleport_TeleportingStateCoroutine_m29325603C91C678B7035FDA9AA324D657149A1C7 (void);
// 0x00000073 System.Void LocomotionTeleport::add_EnterStatePostTeleport(System.Action)
extern void LocomotionTeleport_add_EnterStatePostTeleport_m6352237DD5B10EB9C90693107DD8C33A6CE538FA (void);
// 0x00000074 System.Void LocomotionTeleport::remove_EnterStatePostTeleport(System.Action)
extern void LocomotionTeleport_remove_EnterStatePostTeleport_m9E53B5CBF3804AC61F9E38AEA246E2599341373D (void);
// 0x00000075 System.Collections.IEnumerator LocomotionTeleport::PostTeleportStateCoroutine()
extern void LocomotionTeleport_PostTeleportStateCoroutine_m55EFBB7452FA0FEF1F0C025CE559516ED977AA96 (void);
// 0x00000076 System.Void LocomotionTeleport::add_Teleported(System.Action`3<UnityEngine.Transform,UnityEngine.Vector3,UnityEngine.Quaternion>)
extern void LocomotionTeleport_add_Teleported_m8CAC3A947E40F5C79B38D5F059E94B75A6CE623A (void);
// 0x00000077 System.Void LocomotionTeleport::remove_Teleported(System.Action`3<UnityEngine.Transform,UnityEngine.Vector3,UnityEngine.Quaternion>)
extern void LocomotionTeleport_remove_Teleported_m27E84C43ACE915F7D5ADA07A974F7A76530BBA3E (void);
// 0x00000078 System.Void LocomotionTeleport::DoTeleport()
extern void LocomotionTeleport_DoTeleport_m1477EB38D2FD6E2AD4A7553EB3237B18D40C0B0C (void);
// 0x00000079 UnityEngine.Vector3 LocomotionTeleport::GetCharacterPosition()
extern void LocomotionTeleport_GetCharacterPosition_m30F7EB551F5787D2489CE2716284B848D2C11D26 (void);
// 0x0000007A UnityEngine.Quaternion LocomotionTeleport::GetHeadRotationY()
extern void LocomotionTeleport_GetHeadRotationY_m0F2078ED6649DCED3A8022BA2CD27BF02378639B (void);
// 0x0000007B System.Void LocomotionTeleport::DoWarp(UnityEngine.Vector3,System.Single)
extern void LocomotionTeleport_DoWarp_m2195B0DF360E9B04AD486855F66560DFC22EEB76 (void);
// 0x0000007C System.Void LocomotionTeleport::.ctor()
extern void LocomotionTeleport__ctor_m9AFDEE21E452CC311C904A90D753CB815495A59C (void);
// 0x0000007D System.Void LocomotionTeleport/AimData::.ctor()
extern void AimData__ctor_mDEB139E72E07987C9EC05AFFACE3A5C393EE56B8 (void);
// 0x0000007E System.Collections.Generic.List`1<UnityEngine.Vector3> LocomotionTeleport/AimData::get_Points()
extern void AimData_get_Points_m3169977C66C0D412F32CECB2B7FD471F65A191B2 (void);
// 0x0000007F System.Void LocomotionTeleport/AimData::set_Points(System.Collections.Generic.List`1<UnityEngine.Vector3>)
extern void AimData_set_Points_mDECAE723C539EE8F8513307662F0CCF743ED7BF4 (void);
// 0x00000080 System.Void LocomotionTeleport/AimData::Reset()
extern void AimData_Reset_mA22ED9AA08B2642374D760A48D198B98DA3D49E7 (void);
// 0x00000081 System.Void LocomotionTeleport/<ReadyStateCoroutine>d__52::.ctor(System.Int32)
extern void U3CReadyStateCoroutineU3Ed__52__ctor_m8DE1982B50214C0C02CF0920B2BD0C8503DD7BC6 (void);
// 0x00000082 System.Void LocomotionTeleport/<ReadyStateCoroutine>d__52::System.IDisposable.Dispose()
extern void U3CReadyStateCoroutineU3Ed__52_System_IDisposable_Dispose_m281149F5ECF711FBFD9262A09DA4339B2DB2F7FC (void);
// 0x00000083 System.Boolean LocomotionTeleport/<ReadyStateCoroutine>d__52::MoveNext()
extern void U3CReadyStateCoroutineU3Ed__52_MoveNext_mA3BE219EA412B9FF5022A2D51A8C98962618203D (void);
// 0x00000084 System.Object LocomotionTeleport/<ReadyStateCoroutine>d__52::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CReadyStateCoroutineU3Ed__52_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF049A6B9DF16EBDE92993801D9D70B3DF6724BB3 (void);
// 0x00000085 System.Void LocomotionTeleport/<ReadyStateCoroutine>d__52::System.Collections.IEnumerator.Reset()
extern void U3CReadyStateCoroutineU3Ed__52_System_Collections_IEnumerator_Reset_m57879FEF2CA0007DFA14EE7C948F128236C49F20 (void);
// 0x00000086 System.Object LocomotionTeleport/<ReadyStateCoroutine>d__52::System.Collections.IEnumerator.get_Current()
extern void U3CReadyStateCoroutineU3Ed__52_System_Collections_IEnumerator_get_Current_m8CFB46F37C1A1C464F6C813BD5B61E5E4D60D65B (void);
// 0x00000087 System.Void LocomotionTeleport/<AimStateCoroutine>d__64::.ctor(System.Int32)
extern void U3CAimStateCoroutineU3Ed__64__ctor_m58306B48D136424B1A697461F28CCD0112C0C386 (void);
// 0x00000088 System.Void LocomotionTeleport/<AimStateCoroutine>d__64::System.IDisposable.Dispose()
extern void U3CAimStateCoroutineU3Ed__64_System_IDisposable_Dispose_m1F7871364B779E25C381A3A485C5E8360E58BA33 (void);
// 0x00000089 System.Boolean LocomotionTeleport/<AimStateCoroutine>d__64::MoveNext()
extern void U3CAimStateCoroutineU3Ed__64_MoveNext_mF0CCCD83D0E4DDD4864A079C21C7423C75D51811 (void);
// 0x0000008A System.Object LocomotionTeleport/<AimStateCoroutine>d__64::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAimStateCoroutineU3Ed__64_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCEA3E5325E391A08ECF74573A3F1DA3F202A609F (void);
// 0x0000008B System.Void LocomotionTeleport/<AimStateCoroutine>d__64::System.Collections.IEnumerator.Reset()
extern void U3CAimStateCoroutineU3Ed__64_System_Collections_IEnumerator_Reset_m430B643E53905F6ECA3AFC5D72DF1F1C7431E345 (void);
// 0x0000008C System.Object LocomotionTeleport/<AimStateCoroutine>d__64::System.Collections.IEnumerator.get_Current()
extern void U3CAimStateCoroutineU3Ed__64_System_Collections_IEnumerator_get_Current_m800D35A57B1A8C7F13AB42BA27EC5C8C763D7EA8 (void);
// 0x0000008D System.Void LocomotionTeleport/<CancelAimStateCoroutine>d__68::.ctor(System.Int32)
extern void U3CCancelAimStateCoroutineU3Ed__68__ctor_m1DDF58501076B8077E3441AF06D8A6A7F13B6747 (void);
// 0x0000008E System.Void LocomotionTeleport/<CancelAimStateCoroutine>d__68::System.IDisposable.Dispose()
extern void U3CCancelAimStateCoroutineU3Ed__68_System_IDisposable_Dispose_mD7599C67A3F973C9B291989150D3639890B5BD84 (void);
// 0x0000008F System.Boolean LocomotionTeleport/<CancelAimStateCoroutine>d__68::MoveNext()
extern void U3CCancelAimStateCoroutineU3Ed__68_MoveNext_m0387258B4B35F7D9F037F52A915D247B2EBDF011 (void);
// 0x00000090 System.Object LocomotionTeleport/<CancelAimStateCoroutine>d__68::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCancelAimStateCoroutineU3Ed__68_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m422572D5779C1216188EDB8521144B4BD48335B0 (void);
// 0x00000091 System.Void LocomotionTeleport/<CancelAimStateCoroutine>d__68::System.Collections.IEnumerator.Reset()
extern void U3CCancelAimStateCoroutineU3Ed__68_System_Collections_IEnumerator_Reset_m537442ED372D14EA04A5967D6B7078DC83C32107 (void);
// 0x00000092 System.Object LocomotionTeleport/<CancelAimStateCoroutine>d__68::System.Collections.IEnumerator.get_Current()
extern void U3CCancelAimStateCoroutineU3Ed__68_System_Collections_IEnumerator_get_Current_mCA030531FABF8A358DCC082E6F56B591B13DC194 (void);
// 0x00000093 System.Void LocomotionTeleport/<PreTeleportStateCoroutine>d__72::.ctor(System.Int32)
extern void U3CPreTeleportStateCoroutineU3Ed__72__ctor_m9FCD57D81CBFA88D2D1B4A777B29A236034CAF8E (void);
// 0x00000094 System.Void LocomotionTeleport/<PreTeleportStateCoroutine>d__72::System.IDisposable.Dispose()
extern void U3CPreTeleportStateCoroutineU3Ed__72_System_IDisposable_Dispose_mBD12FF7C2306AAEF739BCCFF39B2C1F2C42D6ECD (void);
// 0x00000095 System.Boolean LocomotionTeleport/<PreTeleportStateCoroutine>d__72::MoveNext()
extern void U3CPreTeleportStateCoroutineU3Ed__72_MoveNext_mAB1EF7B08ADA03C55A05F1ADC5EF0628EAB6858B (void);
// 0x00000096 System.Object LocomotionTeleport/<PreTeleportStateCoroutine>d__72::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CPreTeleportStateCoroutineU3Ed__72_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m842BD3A40FAB08CE9ED7384F00A4035A6A8A6006 (void);
// 0x00000097 System.Void LocomotionTeleport/<PreTeleportStateCoroutine>d__72::System.Collections.IEnumerator.Reset()
extern void U3CPreTeleportStateCoroutineU3Ed__72_System_Collections_IEnumerator_Reset_m8AA3FEA6ABDAC9A68A96DD7A90B1190B504ED09A (void);
// 0x00000098 System.Object LocomotionTeleport/<PreTeleportStateCoroutine>d__72::System.Collections.IEnumerator.get_Current()
extern void U3CPreTeleportStateCoroutineU3Ed__72_System_Collections_IEnumerator_get_Current_m1FC972F30DE3D7BAB7166BB72A107D0FD9E1E49E (void);
// 0x00000099 System.Void LocomotionTeleport/<CancelTeleportStateCoroutine>d__76::.ctor(System.Int32)
extern void U3CCancelTeleportStateCoroutineU3Ed__76__ctor_m6BBBA231C275F8B93FEFD97A2279B18FED30AFFF (void);
// 0x0000009A System.Void LocomotionTeleport/<CancelTeleportStateCoroutine>d__76::System.IDisposable.Dispose()
extern void U3CCancelTeleportStateCoroutineU3Ed__76_System_IDisposable_Dispose_mBB05318F123DC133C386E503BD026985038DF644 (void);
// 0x0000009B System.Boolean LocomotionTeleport/<CancelTeleportStateCoroutine>d__76::MoveNext()
extern void U3CCancelTeleportStateCoroutineU3Ed__76_MoveNext_m02B6031D1BB5B1B0662A91F6E9B78231CDC0CBAE (void);
// 0x0000009C System.Object LocomotionTeleport/<CancelTeleportStateCoroutine>d__76::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCancelTeleportStateCoroutineU3Ed__76_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9B45046196D18E8F11B7FFCA3836CDD0E069BFB6 (void);
// 0x0000009D System.Void LocomotionTeleport/<CancelTeleportStateCoroutine>d__76::System.Collections.IEnumerator.Reset()
extern void U3CCancelTeleportStateCoroutineU3Ed__76_System_Collections_IEnumerator_Reset_m2FE09112ABFFCA00C122EFD44AE58DA551BD9817 (void);
// 0x0000009E System.Object LocomotionTeleport/<CancelTeleportStateCoroutine>d__76::System.Collections.IEnumerator.get_Current()
extern void U3CCancelTeleportStateCoroutineU3Ed__76_System_Collections_IEnumerator_get_Current_m888B89F4C478800F9FACD7F1EC137820F5094C57 (void);
// 0x0000009F System.Void LocomotionTeleport/<TeleportingStateCoroutine>d__80::.ctor(System.Int32)
extern void U3CTeleportingStateCoroutineU3Ed__80__ctor_m1A3D09A30548B419B3E2083990FF4DA65B66A1C2 (void);
// 0x000000A0 System.Void LocomotionTeleport/<TeleportingStateCoroutine>d__80::System.IDisposable.Dispose()
extern void U3CTeleportingStateCoroutineU3Ed__80_System_IDisposable_Dispose_m52588F27903FD706F72A84E217008CDCEA1CE19C (void);
// 0x000000A1 System.Boolean LocomotionTeleport/<TeleportingStateCoroutine>d__80::MoveNext()
extern void U3CTeleportingStateCoroutineU3Ed__80_MoveNext_m01613F55E14EE215BD297AA0E61DFD1B400B786E (void);
// 0x000000A2 System.Object LocomotionTeleport/<TeleportingStateCoroutine>d__80::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CTeleportingStateCoroutineU3Ed__80_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m59D9DB94A89EE8EC99B646C00995380363E17BA6 (void);
// 0x000000A3 System.Void LocomotionTeleport/<TeleportingStateCoroutine>d__80::System.Collections.IEnumerator.Reset()
extern void U3CTeleportingStateCoroutineU3Ed__80_System_Collections_IEnumerator_Reset_m87DE12198C0424F46FA9337E312A56AC85B48F34 (void);
// 0x000000A4 System.Object LocomotionTeleport/<TeleportingStateCoroutine>d__80::System.Collections.IEnumerator.get_Current()
extern void U3CTeleportingStateCoroutineU3Ed__80_System_Collections_IEnumerator_get_Current_mD05407DB2BA285B804EC4E4DF432340D1424DF0D (void);
// 0x000000A5 System.Void LocomotionTeleport/<PostTeleportStateCoroutine>d__84::.ctor(System.Int32)
extern void U3CPostTeleportStateCoroutineU3Ed__84__ctor_m73CB3DEE73F665F765BA80FDF21B564AE8F22CAB (void);
// 0x000000A6 System.Void LocomotionTeleport/<PostTeleportStateCoroutine>d__84::System.IDisposable.Dispose()
extern void U3CPostTeleportStateCoroutineU3Ed__84_System_IDisposable_Dispose_mDDC6BB7ECF31923D9B8AA237DFDC162642BE3F5E (void);
// 0x000000A7 System.Boolean LocomotionTeleport/<PostTeleportStateCoroutine>d__84::MoveNext()
extern void U3CPostTeleportStateCoroutineU3Ed__84_MoveNext_m5428FACDB4F6A7C6B9C2966106C9CA97A70CAFEC (void);
// 0x000000A8 System.Object LocomotionTeleport/<PostTeleportStateCoroutine>d__84::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CPostTeleportStateCoroutineU3Ed__84_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m50185F8995916556B5531091EAC1D796207EE4D6 (void);
// 0x000000A9 System.Void LocomotionTeleport/<PostTeleportStateCoroutine>d__84::System.Collections.IEnumerator.Reset()
extern void U3CPostTeleportStateCoroutineU3Ed__84_System_Collections_IEnumerator_Reset_m8E16BCCA8A3B018A2CAC418CDAA14DB7C9B3D897 (void);
// 0x000000AA System.Object LocomotionTeleport/<PostTeleportStateCoroutine>d__84::System.Collections.IEnumerator.get_Current()
extern void U3CPostTeleportStateCoroutineU3Ed__84_System_Collections_IEnumerator_get_Current_mA97CEA8F012879C52B5756B734B3AA464439F83F (void);
// 0x000000AB System.Void SimpleCapsuleWithStickMovement::add_CameraUpdated(System.Action)
extern void SimpleCapsuleWithStickMovement_add_CameraUpdated_m62D68F4E8BD481D3679FDB0979AE6CAC8DAFAC46 (void);
// 0x000000AC System.Void SimpleCapsuleWithStickMovement::remove_CameraUpdated(System.Action)
extern void SimpleCapsuleWithStickMovement_remove_CameraUpdated_mA39A1F5D5F0CA44D0B6EB9E9208FD39B7D0D3E06 (void);
// 0x000000AD System.Void SimpleCapsuleWithStickMovement::add_PreCharacterMove(System.Action)
extern void SimpleCapsuleWithStickMovement_add_PreCharacterMove_m682E5AAFBCDF0BF832F5447D69CC73DB39C096D2 (void);
// 0x000000AE System.Void SimpleCapsuleWithStickMovement::remove_PreCharacterMove(System.Action)
extern void SimpleCapsuleWithStickMovement_remove_PreCharacterMove_m57CBA41BD4CD9C1EA3864D4AD464C1255EA80B22 (void);
// 0x000000AF System.Void SimpleCapsuleWithStickMovement::Awake()
extern void SimpleCapsuleWithStickMovement_Awake_m0C068EE45D2BEDB94F8D1D289C04BF7480443D2E (void);
// 0x000000B0 System.Void SimpleCapsuleWithStickMovement::Start()
extern void SimpleCapsuleWithStickMovement_Start_m517C18566428FBB777185B97264539BE48D5A0F5 (void);
// 0x000000B1 System.Void SimpleCapsuleWithStickMovement::FixedUpdate()
extern void SimpleCapsuleWithStickMovement_FixedUpdate_mE88BF998394A1EB4FCEBC8A04875FA26D3D6C1BD (void);
// 0x000000B2 System.Void SimpleCapsuleWithStickMovement::RotatePlayerToHMD()
extern void SimpleCapsuleWithStickMovement_RotatePlayerToHMD_m762E683133240C6E945140A357920BC6ACB7D116 (void);
// 0x000000B3 System.Void SimpleCapsuleWithStickMovement::StickMovement()
extern void SimpleCapsuleWithStickMovement_StickMovement_m4550B6DF9A0272F282866822CC8F0E8AE32E1893 (void);
// 0x000000B4 System.Void SimpleCapsuleWithStickMovement::SnapTurn()
extern void SimpleCapsuleWithStickMovement_SnapTurn_m6BA0E49AA935C048AC9E8D59C90CBF50E087B396 (void);
// 0x000000B5 System.Void SimpleCapsuleWithStickMovement::.ctor()
extern void SimpleCapsuleWithStickMovement__ctor_m96AC7462B0905E8D35D74FCF0C54E3F96DDB68B9 (void);
// 0x000000B6 System.Void TeleportAimHandler::OnEnable()
extern void TeleportAimHandler_OnEnable_mACD1F9393ADE87B704B4A4EAA298FBA36EDAC843 (void);
// 0x000000B7 System.Void TeleportAimHandler::OnDisable()
extern void TeleportAimHandler_OnDisable_m61175BD9492F8C39A0BC8573FCAB06D008DF3961 (void);
// 0x000000B8 System.Void TeleportAimHandler::GetPoints(System.Collections.Generic.List`1<UnityEngine.Vector3>)
// 0x000000B9 System.Void TeleportAimHandler::.ctor()
extern void TeleportAimHandler__ctor_m1763BE837F8F8FA3B3C34EBA78BE0D593F50A5E7 (void);
// 0x000000BA System.Void TeleportAimHandlerLaser::GetPoints(System.Collections.Generic.List`1<UnityEngine.Vector3>)
extern void TeleportAimHandlerLaser_GetPoints_m5DE7FA7926409D22B8F5E329EC2A4935B95EC3A0 (void);
// 0x000000BB System.Void TeleportAimHandlerLaser::.ctor()
extern void TeleportAimHandlerLaser__ctor_m0D6E559AC138E39BBBF5E12AD223F13571A32D9C (void);
// 0x000000BC System.Void TeleportAimHandlerParabolic::GetPoints(System.Collections.Generic.List`1<UnityEngine.Vector3>)
extern void TeleportAimHandlerParabolic_GetPoints_m2AF261807D5A7F6FE7E14F20B473D985A93DD1C3 (void);
// 0x000000BD System.Void TeleportAimHandlerParabolic::.ctor()
extern void TeleportAimHandlerParabolic__ctor_mAB05081CBCE394C762E8B8F4F6CA12D3E9846A32 (void);
// 0x000000BE System.Void TeleportAimVisualLaser::.ctor()
extern void TeleportAimVisualLaser__ctor_mC232B12213106D0E44DD02D7EE16080182C5FCAC (void);
// 0x000000BF System.Void TeleportAimVisualLaser::EnterAimState()
extern void TeleportAimVisualLaser_EnterAimState_mFBE7E0BB8190B822638EB470B2559E9A52B29832 (void);
// 0x000000C0 System.Void TeleportAimVisualLaser::ExitAimState()
extern void TeleportAimVisualLaser_ExitAimState_m7076D541D8CFF0F24A2A19CBFE8C0EED448773D9 (void);
// 0x000000C1 System.Void TeleportAimVisualLaser::Awake()
extern void TeleportAimVisualLaser_Awake_mFC14E2C04A8ACFACA52131512BAE0FCDB425754A (void);
// 0x000000C2 System.Void TeleportAimVisualLaser::AddEventHandlers()
extern void TeleportAimVisualLaser_AddEventHandlers_m5E7A188CF65F15DEEE335F3B40C261EF1D1E0D10 (void);
// 0x000000C3 System.Void TeleportAimVisualLaser::RemoveEventHandlers()
extern void TeleportAimVisualLaser_RemoveEventHandlers_mAEC2273E026F953D7B71945590F0E7ED65539415 (void);
// 0x000000C4 System.Void TeleportAimVisualLaser::UpdateAimData(LocomotionTeleport/AimData)
extern void TeleportAimVisualLaser_UpdateAimData_m8060F21ACBE191832A7247F4D750DF9A5A7C9984 (void);
// 0x000000C5 System.Boolean TeleportDestination::get_IsValidDestination()
extern void TeleportDestination_get_IsValidDestination_m4A83CF1ABD625233373782FABAC9AC210A5FD151 (void);
// 0x000000C6 System.Void TeleportDestination::set_IsValidDestination(System.Boolean)
extern void TeleportDestination_set_IsValidDestination_m2A37F62F1E80ECB2FDE63D42471514A26B0ED0BA (void);
// 0x000000C7 System.Void TeleportDestination::.ctor()
extern void TeleportDestination__ctor_m01B9174443C128BB3639D39F0D2EBD7283DEB972 (void);
// 0x000000C8 System.Void TeleportDestination::OnEnable()
extern void TeleportDestination_OnEnable_mFB65AD58BD57263B4D14E91B6FA583DA9607EB91 (void);
// 0x000000C9 System.Void TeleportDestination::TryDisableEventHandlers()
extern void TeleportDestination_TryDisableEventHandlers_m450B9F64A3EAB6059B85B7573377A574606A9062 (void);
// 0x000000CA System.Void TeleportDestination::OnDisable()
extern void TeleportDestination_OnDisable_m0D75681D1CD83894438624CD3A7878C95904F63F (void);
// 0x000000CB System.Void TeleportDestination::add_Deactivated(System.Action`1<TeleportDestination>)
extern void TeleportDestination_add_Deactivated_m63745B4674198FDFA8D3CB45587CA14132840F9A (void);
// 0x000000CC System.Void TeleportDestination::remove_Deactivated(System.Action`1<TeleportDestination>)
extern void TeleportDestination_remove_Deactivated_mE2D6A7DCD9734262F34A8935272156A4B029A83A (void);
// 0x000000CD System.Void TeleportDestination::OnDeactivated()
extern void TeleportDestination_OnDeactivated_mA84FB07520E0C6EDA28F13A6349B994236BF8B46 (void);
// 0x000000CE System.Void TeleportDestination::Recycle()
extern void TeleportDestination_Recycle_m49B5F4B6776D55D76519AFA221B2881A51FED91E (void);
// 0x000000CF System.Void TeleportDestination::UpdateTeleportDestination(System.Boolean,System.Nullable`1<UnityEngine.Vector3>,System.Nullable`1<UnityEngine.Quaternion>,System.Nullable`1<UnityEngine.Quaternion>)
extern void TeleportDestination_UpdateTeleportDestination_m3FF1158B3A2BB73D914DC806E094D20F93A674B3 (void);
// 0x000000D0 System.Void TeleportInputHandler::.ctor()
extern void TeleportInputHandler__ctor_m54EE60B8D0F7935760E81AB820D4570094939F71 (void);
// 0x000000D1 System.Void TeleportInputHandler::AddEventHandlers()
extern void TeleportInputHandler_AddEventHandlers_m4609349778D031303E2767296E2B7E48E5E20FD2 (void);
// 0x000000D2 System.Void TeleportInputHandler::RemoveEventHandlers()
extern void TeleportInputHandler_RemoveEventHandlers_m739F90CC264BE61AC9316C2B1645BFE8BCBA0555 (void);
// 0x000000D3 System.Collections.IEnumerator TeleportInputHandler::TeleportReadyCoroutine()
extern void TeleportInputHandler_TeleportReadyCoroutine_m579E5F381DEA656B2A90444ADFA6DB5CB05BCD50 (void);
// 0x000000D4 System.Collections.IEnumerator TeleportInputHandler::TeleportAimCoroutine()
extern void TeleportInputHandler_TeleportAimCoroutine_m82502080BCE2BD26E5BEBD89D54C3E57A4780B07 (void);
// 0x000000D5 LocomotionTeleport/TeleportIntentions TeleportInputHandler::GetIntention()
// 0x000000D6 System.Void TeleportInputHandler::GetAimData(UnityEngine.Ray&)
// 0x000000D7 System.Void TeleportInputHandler::<.ctor>b__2_0()
extern void TeleportInputHandler_U3C_ctorU3Eb__2_0_m919CD5070F20E3A9E28CB8A95C9C9374663C52A1 (void);
// 0x000000D8 System.Void TeleportInputHandler::<.ctor>b__2_1()
extern void TeleportInputHandler_U3C_ctorU3Eb__2_1_m2CE609639EC53A6A19E043A2E053F30FAE77D486 (void);
// 0x000000D9 System.Void TeleportInputHandler/<TeleportReadyCoroutine>d__5::.ctor(System.Int32)
extern void U3CTeleportReadyCoroutineU3Ed__5__ctor_m7C396BD2404ABCBF1CF83320ACF13FA1B621B4FB (void);
// 0x000000DA System.Void TeleportInputHandler/<TeleportReadyCoroutine>d__5::System.IDisposable.Dispose()
extern void U3CTeleportReadyCoroutineU3Ed__5_System_IDisposable_Dispose_mA2DFF1E0C765EA7B2F4BA2E1EF7721049E8EA046 (void);
// 0x000000DB System.Boolean TeleportInputHandler/<TeleportReadyCoroutine>d__5::MoveNext()
extern void U3CTeleportReadyCoroutineU3Ed__5_MoveNext_m5A64679B873617863C4B38AA8008C8CC1E564029 (void);
// 0x000000DC System.Object TeleportInputHandler/<TeleportReadyCoroutine>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CTeleportReadyCoroutineU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m41A381D2B7B87AD5810E05A3C55A66FD7D8CADA2 (void);
// 0x000000DD System.Void TeleportInputHandler/<TeleportReadyCoroutine>d__5::System.Collections.IEnumerator.Reset()
extern void U3CTeleportReadyCoroutineU3Ed__5_System_Collections_IEnumerator_Reset_m12F830ADD072802D84BE4B3231F43A12FA4F8E73 (void);
// 0x000000DE System.Object TeleportInputHandler/<TeleportReadyCoroutine>d__5::System.Collections.IEnumerator.get_Current()
extern void U3CTeleportReadyCoroutineU3Ed__5_System_Collections_IEnumerator_get_Current_mF8243C3A677F944C06A59D21D17CC3AF8DD1B594 (void);
// 0x000000DF System.Void TeleportInputHandler/<TeleportAimCoroutine>d__6::.ctor(System.Int32)
extern void U3CTeleportAimCoroutineU3Ed__6__ctor_mE2F9BDC7C3225737B5ADD2AD7AAF0E449CFADE47 (void);
// 0x000000E0 System.Void TeleportInputHandler/<TeleportAimCoroutine>d__6::System.IDisposable.Dispose()
extern void U3CTeleportAimCoroutineU3Ed__6_System_IDisposable_Dispose_m443089F5C3CB24CF2E58C43B6D940BB6A97E7059 (void);
// 0x000000E1 System.Boolean TeleportInputHandler/<TeleportAimCoroutine>d__6::MoveNext()
extern void U3CTeleportAimCoroutineU3Ed__6_MoveNext_m3D532A7ED8718469618989D091672C1E1E833FA0 (void);
// 0x000000E2 System.Object TeleportInputHandler/<TeleportAimCoroutine>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CTeleportAimCoroutineU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m02583125A115ADE4CC8896C0108B7A2ADD9FCAFE (void);
// 0x000000E3 System.Void TeleportInputHandler/<TeleportAimCoroutine>d__6::System.Collections.IEnumerator.Reset()
extern void U3CTeleportAimCoroutineU3Ed__6_System_Collections_IEnumerator_Reset_mE894BDC51B100595A10489704983F8922185CCF8 (void);
// 0x000000E4 System.Object TeleportInputHandler/<TeleportAimCoroutine>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CTeleportAimCoroutineU3Ed__6_System_Collections_IEnumerator_get_Current_m28CB29942297ECF9C327DDDAB3D53AF8A77ABF05 (void);
// 0x000000E5 UnityEngine.Transform TeleportInputHandlerHMD::get_Pointer()
extern void TeleportInputHandlerHMD_get_Pointer_mCBC08A1A1B5D076AF31755E7771F3000034939FF (void);
// 0x000000E6 System.Void TeleportInputHandlerHMD::set_Pointer(UnityEngine.Transform)
extern void TeleportInputHandlerHMD_set_Pointer_m5F8552C6C93A90A8BA5E3473532D687559EB2E9D (void);
// 0x000000E7 LocomotionTeleport/TeleportIntentions TeleportInputHandlerHMD::GetIntention()
extern void TeleportInputHandlerHMD_GetIntention_m474B95CF0690765C9B5C7DCC8381385E9BF5FC6A (void);
// 0x000000E8 System.Void TeleportInputHandlerHMD::GetAimData(UnityEngine.Ray&)
extern void TeleportInputHandlerHMD_GetAimData_mBE4EAEA4E4AD483994731D9FE82D0491E131AA37 (void);
// 0x000000E9 System.Void TeleportInputHandlerHMD::.ctor()
extern void TeleportInputHandlerHMD__ctor_m3E2641D8040D2F44A53E2A184ECDDCA1ED6153C9 (void);
// 0x000000EA System.Void TeleportInputHandlerTouch::Start()
extern void TeleportInputHandlerTouch_Start_m14F2576DDA7F7AA52A05AD9456703D1FF68F2387 (void);
// 0x000000EB LocomotionTeleport/TeleportIntentions TeleportInputHandlerTouch::GetIntention()
extern void TeleportInputHandlerTouch_GetIntention_m23F1835AE8064C76A1C2F9631B343C721DB6A993 (void);
// 0x000000EC System.Void TeleportInputHandlerTouch::GetAimData(UnityEngine.Ray&)
extern void TeleportInputHandlerTouch_GetAimData_m4155B513561DCB66F1180407A5AB8B2B5936E91B (void);
// 0x000000ED System.Void TeleportInputHandlerTouch::.ctor()
extern void TeleportInputHandlerTouch__ctor_mE4E2BF438B9790F07ECA23A6D77748104C707E31 (void);
// 0x000000EE System.Void TeleportOrientationHandler::.ctor()
extern void TeleportOrientationHandler__ctor_m21A7BA2A330541DFEB4ADF80A53BE1046E65C10A (void);
// 0x000000EF System.Void TeleportOrientationHandler::UpdateAimData(LocomotionTeleport/AimData)
extern void TeleportOrientationHandler_UpdateAimData_m9B52AC667A8BF78D509B49403962756A1714DD9B (void);
// 0x000000F0 System.Void TeleportOrientationHandler::AddEventHandlers()
extern void TeleportOrientationHandler_AddEventHandlers_m2507C63FDF190C3BC81D2479EA8D488F32C6D4C6 (void);
// 0x000000F1 System.Void TeleportOrientationHandler::RemoveEventHandlers()
extern void TeleportOrientationHandler_RemoveEventHandlers_mB46ADA4F854BD6919ED74D88F027AB423FF522BF (void);
// 0x000000F2 System.Collections.IEnumerator TeleportOrientationHandler::UpdateOrientationCoroutine()
extern void TeleportOrientationHandler_UpdateOrientationCoroutine_m0CFA18DF96F986F14A52E6CC7EEFC90FA0C73B7B (void);
// 0x000000F3 System.Void TeleportOrientationHandler::InitializeTeleportDestination()
// 0x000000F4 System.Void TeleportOrientationHandler::UpdateTeleportDestination()
// 0x000000F5 UnityEngine.Quaternion TeleportOrientationHandler::GetLandingOrientation(TeleportOrientationHandler/OrientationModes,UnityEngine.Quaternion)
extern void TeleportOrientationHandler_GetLandingOrientation_m4CC4D8950AFCDAA13EB2244AD987B3225FEFAABA (void);
// 0x000000F6 System.Void TeleportOrientationHandler::<.ctor>b__3_0()
extern void TeleportOrientationHandler_U3C_ctorU3Eb__3_0_m3CA25FA37637FD4D799437E6037E2C278EC4B9A0 (void);
// 0x000000F7 System.Void TeleportOrientationHandler/<UpdateOrientationCoroutine>d__7::.ctor(System.Int32)
extern void U3CUpdateOrientationCoroutineU3Ed__7__ctor_m5DFE7ADCE7BF42B372F2097C1C019F301BBE3DFE (void);
// 0x000000F8 System.Void TeleportOrientationHandler/<UpdateOrientationCoroutine>d__7::System.IDisposable.Dispose()
extern void U3CUpdateOrientationCoroutineU3Ed__7_System_IDisposable_Dispose_m159520706F62855F2F55F4686A47A89D093AD2B0 (void);
// 0x000000F9 System.Boolean TeleportOrientationHandler/<UpdateOrientationCoroutine>d__7::MoveNext()
extern void U3CUpdateOrientationCoroutineU3Ed__7_MoveNext_m2D766D50632FD169A27EF1E4EE39B50AAEF4512C (void);
// 0x000000FA System.Object TeleportOrientationHandler/<UpdateOrientationCoroutine>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CUpdateOrientationCoroutineU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m87C4EB920BA4BF331493243117342D5E99905F1C (void);
// 0x000000FB System.Void TeleportOrientationHandler/<UpdateOrientationCoroutine>d__7::System.Collections.IEnumerator.Reset()
extern void U3CUpdateOrientationCoroutineU3Ed__7_System_Collections_IEnumerator_Reset_m8957A7F5F8D1D9C630D84F24A7C2B66642CA9B95 (void);
// 0x000000FC System.Object TeleportOrientationHandler/<UpdateOrientationCoroutine>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CUpdateOrientationCoroutineU3Ed__7_System_Collections_IEnumerator_get_Current_mDB4E059717A313A10584DB05F88E813A45D9A606 (void);
// 0x000000FD System.Void TeleportOrientationHandler360::InitializeTeleportDestination()
extern void TeleportOrientationHandler360_InitializeTeleportDestination_m6372AC3D398F0ABB35CDE87532ED8ACEE579351A (void);
// 0x000000FE System.Void TeleportOrientationHandler360::UpdateTeleportDestination()
extern void TeleportOrientationHandler360_UpdateTeleportDestination_mF98B570F0875BCE3DEF88E3EA2BDCF415C759E71 (void);
// 0x000000FF System.Void TeleportOrientationHandler360::.ctor()
extern void TeleportOrientationHandler360__ctor_m6FDD3AEEEB9FB23CEC3C6D7B6A453FF38428B49A (void);
// 0x00000100 System.Void TeleportOrientationHandlerHMD::InitializeTeleportDestination()
extern void TeleportOrientationHandlerHMD_InitializeTeleportDestination_m507B4108A8B145144CC59BFB969FB37F3EDB88E9 (void);
// 0x00000101 System.Void TeleportOrientationHandlerHMD::UpdateTeleportDestination()
extern void TeleportOrientationHandlerHMD_UpdateTeleportDestination_mA3F3F034F1C248EBE74640A15F13D287D1C2AE2E (void);
// 0x00000102 System.Void TeleportOrientationHandlerHMD::.ctor()
extern void TeleportOrientationHandlerHMD__ctor_mED7328A59A1C729AC845F7A8410DD4BD5C633DE2 (void);
// 0x00000103 System.Void TeleportOrientationHandlerThumbstick::InitializeTeleportDestination()
extern void TeleportOrientationHandlerThumbstick_InitializeTeleportDestination_m4ABFA62C2B8CB74DE7829D0005E6C98FE32EB8F2 (void);
// 0x00000104 System.Void TeleportOrientationHandlerThumbstick::UpdateTeleportDestination()
extern void TeleportOrientationHandlerThumbstick_UpdateTeleportDestination_mBE2E2B2A0BCED3855974AAC1AAFBEE6F6081587B (void);
// 0x00000105 System.Void TeleportOrientationHandlerThumbstick::.ctor()
extern void TeleportOrientationHandlerThumbstick__ctor_mEAA7AC3ABA1CE6E0253CF38FB2CA3E4C318F59BF (void);
// 0x00000106 System.Void TeleportPoint::Start()
extern void TeleportPoint_Start_m30ECA69CD4353B458F2295BAEBB450CC01F59805 (void);
// 0x00000107 UnityEngine.Transform TeleportPoint::GetDestTransform()
extern void TeleportPoint_GetDestTransform_m721C2E343BE9B54FABF84CA15E207FD2390F7DBD (void);
// 0x00000108 System.Void TeleportPoint::Update()
extern void TeleportPoint_Update_mAF9BC964BBC534053D53659EF53DA1A09B22F484 (void);
// 0x00000109 System.Void TeleportPoint::OnLookAt()
extern void TeleportPoint_OnLookAt_m2564753261F0D09670931BBF308955CB21B8302A (void);
// 0x0000010A System.Void TeleportPoint::.ctor()
extern void TeleportPoint__ctor_mA783018C82A82D1A80BA40DF577BC9D3BC245A6A (void);
// 0x0000010B LocomotionTeleport TeleportSupport::get_LocomotionTeleport()
extern void TeleportSupport_get_LocomotionTeleport_mC25D8C8701C7D25C8FBA30EB3C92527B8184A714 (void);
// 0x0000010C System.Void TeleportSupport::set_LocomotionTeleport(LocomotionTeleport)
extern void TeleportSupport_set_LocomotionTeleport_m718C84A5C4E1D039C61850BA424EFA71B5FF9327 (void);
// 0x0000010D System.Void TeleportSupport::OnEnable()
extern void TeleportSupport_OnEnable_mF890133242B29B518FDCBD5FEAFAF29298CBA609 (void);
// 0x0000010E System.Void TeleportSupport::OnDisable()
extern void TeleportSupport_OnDisable_m9C71979791E9FCD13837605745FA3C6CFF0B1409 (void);
// 0x0000010F System.Void TeleportSupport::LogEventHandler(System.String)
extern void TeleportSupport_LogEventHandler_m65943E6F024E9B73CE5D5ED5099B2A0164B3E2C5 (void);
// 0x00000110 System.Void TeleportSupport::AddEventHandlers()
extern void TeleportSupport_AddEventHandlers_mE4502DF40E56A3B5061FB931FC4F21678BAE3E84 (void);
// 0x00000111 System.Void TeleportSupport::RemoveEventHandlers()
extern void TeleportSupport_RemoveEventHandlers_mE7C77E2C141F41B7799C511FC1B8DEB699B6E8E1 (void);
// 0x00000112 System.Void TeleportSupport::.ctor()
extern void TeleportSupport__ctor_m05529A4609A1DFC91979CE4AE1790639B372E754 (void);
// 0x00000113 System.Void TeleportTargetHandler::.ctor()
extern void TeleportTargetHandler__ctor_m3AE469213D738BE0ABD908BB5AA27AB6FF60240E (void);
// 0x00000114 System.Void TeleportTargetHandler::AddEventHandlers()
extern void TeleportTargetHandler_AddEventHandlers_mFFEC0649545A9B1614D7114872C7AECEED6AE715 (void);
// 0x00000115 System.Void TeleportTargetHandler::RemoveEventHandlers()
extern void TeleportTargetHandler_RemoveEventHandlers_m3DC103164A1F0089BBC66E83C9E754B7F9C79849 (void);
// 0x00000116 System.Collections.IEnumerator TeleportTargetHandler::TargetAimCoroutine()
extern void TeleportTargetHandler_TargetAimCoroutine_m1CEA9B7FA57455F77D9639E1B62C205C515E959D (void);
// 0x00000117 System.Void TeleportTargetHandler::ResetAimData()
extern void TeleportTargetHandler_ResetAimData_mA32665779CE5C63B71357211C714615BF9CD4DA2 (void);
// 0x00000118 System.Boolean TeleportTargetHandler::ConsiderTeleport(UnityEngine.Vector3,UnityEngine.Vector3&)
// 0x00000119 System.Nullable`1<UnityEngine.Vector3> TeleportTargetHandler::ConsiderDestination(UnityEngine.Vector3)
extern void TeleportTargetHandler_ConsiderDestination_m1A96940FCF8FD62A59948C3080FF813ABA4F8331 (void);
// 0x0000011A System.Void TeleportTargetHandler::<.ctor>b__3_0()
extern void TeleportTargetHandler_U3C_ctorU3Eb__3_0_m6384FA377850643BB1088262AAFC6FFA8748F8F9 (void);
// 0x0000011B System.Void TeleportTargetHandler/<TargetAimCoroutine>d__7::.ctor(System.Int32)
extern void U3CTargetAimCoroutineU3Ed__7__ctor_m23D9D0D8A352A65E0D99985A676E454304BF8FE4 (void);
// 0x0000011C System.Void TeleportTargetHandler/<TargetAimCoroutine>d__7::System.IDisposable.Dispose()
extern void U3CTargetAimCoroutineU3Ed__7_System_IDisposable_Dispose_m21B111D2C0E7C1EB3CE98929690C0DD190716414 (void);
// 0x0000011D System.Boolean TeleportTargetHandler/<TargetAimCoroutine>d__7::MoveNext()
extern void U3CTargetAimCoroutineU3Ed__7_MoveNext_m67B9E5EA47ACD29BD93DB469A85D596C9D94F69A (void);
// 0x0000011E System.Object TeleportTargetHandler/<TargetAimCoroutine>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CTargetAimCoroutineU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m73510D945678774D27D2FEE03C45CF482A2EBBE6 (void);
// 0x0000011F System.Void TeleportTargetHandler/<TargetAimCoroutine>d__7::System.Collections.IEnumerator.Reset()
extern void U3CTargetAimCoroutineU3Ed__7_System_Collections_IEnumerator_Reset_m2BC8EDEB99F6EEEA6DB6EE8F703C99E5CC6A7301 (void);
// 0x00000120 System.Object TeleportTargetHandler/<TargetAimCoroutine>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CTargetAimCoroutineU3Ed__7_System_Collections_IEnumerator_get_Current_mF0915E668ADBDEC27BF20ED85385A44114046F62 (void);
// 0x00000121 System.Void TeleportTargetHandlerNavMesh::Awake()
extern void TeleportTargetHandlerNavMesh_Awake_m12528F076C3DFA64B7F8C2738B086CCBA390746A (void);
// 0x00000122 System.Boolean TeleportTargetHandlerNavMesh::ConsiderTeleport(UnityEngine.Vector3,UnityEngine.Vector3&)
extern void TeleportTargetHandlerNavMesh_ConsiderTeleport_m437418CEDAA72639344228B31EA8BDC8A52AD169 (void);
// 0x00000123 System.Nullable`1<UnityEngine.Vector3> TeleportTargetHandlerNavMesh::ConsiderDestination(UnityEngine.Vector3)
extern void TeleportTargetHandlerNavMesh_ConsiderDestination_m0095B7CAB6515175C4652DAFB166FB7851298DD8 (void);
// 0x00000124 System.Void TeleportTargetHandlerNavMesh::OnDrawGizmos()
extern void TeleportTargetHandlerNavMesh_OnDrawGizmos_mBBC3AFFADE1EA512DEF62DE6FB277B82EE99E1DC (void);
// 0x00000125 System.Void TeleportTargetHandlerNavMesh::.ctor()
extern void TeleportTargetHandlerNavMesh__ctor_m5BB4F183D1C263E465000903520478FBB0D2E990 (void);
// 0x00000126 System.Boolean TeleportTargetHandlerNode::ConsiderTeleport(UnityEngine.Vector3,UnityEngine.Vector3&)
extern void TeleportTargetHandlerNode_ConsiderTeleport_m144CD7243791916F4D785F4E01AAAD729C1F1150 (void);
// 0x00000127 System.Void TeleportTargetHandlerNode::.ctor()
extern void TeleportTargetHandlerNode__ctor_m66245B4B90DFAE3D51056B3DC1B825CF128E4145 (void);
// 0x00000128 System.Boolean TeleportTargetHandlerPhysical::ConsiderTeleport(UnityEngine.Vector3,UnityEngine.Vector3&)
extern void TeleportTargetHandlerPhysical_ConsiderTeleport_m3F6B5CBCEFE9B53FB42E5A853B728740B30A879F (void);
// 0x00000129 System.Void TeleportTargetHandlerPhysical::.ctor()
extern void TeleportTargetHandlerPhysical__ctor_mED10B7430B7A3A1104C1EDFF1A2F7B4B4B007B4D (void);
// 0x0000012A System.Void TeleportTransition::AddEventHandlers()
extern void TeleportTransition_AddEventHandlers_m7945828C3E62F3C4EF094AC2B12950618720397F (void);
// 0x0000012B System.Void TeleportTransition::RemoveEventHandlers()
extern void TeleportTransition_RemoveEventHandlers_mBD20E23B4E42E8DE6D30126DA98CF0B744864546 (void);
// 0x0000012C System.Void TeleportTransition::LocomotionTeleportOnEnterStateTeleporting()
// 0x0000012D System.Void TeleportTransition::.ctor()
extern void TeleportTransition__ctor_m1A4530AB5DEEA55E946DF9A491B5145EFE239ACF (void);
// 0x0000012E System.Void TeleportTransitionBlink::LocomotionTeleportOnEnterStateTeleporting()
extern void TeleportTransitionBlink_LocomotionTeleportOnEnterStateTeleporting_mCB87A486BE2733C4BCD2B57AEA4FAA69FED1D74D (void);
// 0x0000012F System.Collections.IEnumerator TeleportTransitionBlink::BlinkCoroutine()
extern void TeleportTransitionBlink_BlinkCoroutine_m502BAE8615CE4CC76BFD91E15746263D10CF2E77 (void);
// 0x00000130 System.Void TeleportTransitionBlink::.ctor()
extern void TeleportTransitionBlink__ctor_m709CADAB68439933BA41B5C318E4E60616F09932 (void);
// 0x00000131 System.Void TeleportTransitionBlink/<BlinkCoroutine>d__4::.ctor(System.Int32)
extern void U3CBlinkCoroutineU3Ed__4__ctor_m73115DAF8E2D89A49A9F6BD9AB03485AA7551BCD (void);
// 0x00000132 System.Void TeleportTransitionBlink/<BlinkCoroutine>d__4::System.IDisposable.Dispose()
extern void U3CBlinkCoroutineU3Ed__4_System_IDisposable_Dispose_m6A66CCE325E70F5794414025EA6A7B8EABFAE259 (void);
// 0x00000133 System.Boolean TeleportTransitionBlink/<BlinkCoroutine>d__4::MoveNext()
extern void U3CBlinkCoroutineU3Ed__4_MoveNext_mBEC5B507954BA0961920B4141D150885CE4D309B (void);
// 0x00000134 System.Object TeleportTransitionBlink/<BlinkCoroutine>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CBlinkCoroutineU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFB31D456C6490D00742481C0F18817914FC76018 (void);
// 0x00000135 System.Void TeleportTransitionBlink/<BlinkCoroutine>d__4::System.Collections.IEnumerator.Reset()
extern void U3CBlinkCoroutineU3Ed__4_System_Collections_IEnumerator_Reset_m6E21661229B022076FA60AF6C49A1980E56609E1 (void);
// 0x00000136 System.Object TeleportTransitionBlink/<BlinkCoroutine>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CBlinkCoroutineU3Ed__4_System_Collections_IEnumerator_get_Current_mAA053761AFFEF4D53F8AC25C2BDA6DC4729996DF (void);
// 0x00000137 System.Void TeleportTransitionInstant::LocomotionTeleportOnEnterStateTeleporting()
extern void TeleportTransitionInstant_LocomotionTeleportOnEnterStateTeleporting_m09B573BE4E14DD8DD571233C7F3CCA384A775A4B (void);
// 0x00000138 System.Void TeleportTransitionInstant::.ctor()
extern void TeleportTransitionInstant__ctor_m2596160D526DF43F58D24D83F33B43AEB3774DA4 (void);
// 0x00000139 System.Void TeleportTransitionWarp::LocomotionTeleportOnEnterStateTeleporting()
extern void TeleportTransitionWarp_LocomotionTeleportOnEnterStateTeleporting_m2D397B890161CCF004AF8C0AC064F28EAD6EFB8C (void);
// 0x0000013A System.Collections.IEnumerator TeleportTransitionWarp::DoWarp()
extern void TeleportTransitionWarp_DoWarp_m12AF9CC51FC2784F1FCBCD37A9D0CBBDE2EEF117 (void);
// 0x0000013B System.Void TeleportTransitionWarp::.ctor()
extern void TeleportTransitionWarp__ctor_mA2A7FA91C271FEC14A97EF24341CC25EF5C5639B (void);
// 0x0000013C System.Void TeleportTransitionWarp/<DoWarp>d__3::.ctor(System.Int32)
extern void U3CDoWarpU3Ed__3__ctor_mA023019BAD546DB8BFCFC8326BF22566DC855B45 (void);
// 0x0000013D System.Void TeleportTransitionWarp/<DoWarp>d__3::System.IDisposable.Dispose()
extern void U3CDoWarpU3Ed__3_System_IDisposable_Dispose_mE361978D56E79BC8335160F0DA180CB96DCE29F9 (void);
// 0x0000013E System.Boolean TeleportTransitionWarp/<DoWarp>d__3::MoveNext()
extern void U3CDoWarpU3Ed__3_MoveNext_m84CA338E50BBE257229411F56CD6B4D7F46015B1 (void);
// 0x0000013F System.Object TeleportTransitionWarp/<DoWarp>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDoWarpU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m94EA3502B1F9442D23A5EBF53483F6C85D484208 (void);
// 0x00000140 System.Void TeleportTransitionWarp/<DoWarp>d__3::System.Collections.IEnumerator.Reset()
extern void U3CDoWarpU3Ed__3_System_Collections_IEnumerator_Reset_mE692557C58D90117915B599FA91A88FC7D238200 (void);
// 0x00000141 System.Object TeleportTransitionWarp/<DoWarp>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CDoWarpU3Ed__3_System_Collections_IEnumerator_get_Current_mD1CE2712B5B0919B97EE26421E8DDC32F11E5314 (void);
// 0x00000142 System.IntPtr NativeVideoPlayer::get_VideoPlayerClass()
extern void NativeVideoPlayer_get_VideoPlayerClass_m1278CDB8894FE79931F13D6A356B3C7FCDCD8EF6 (void);
// 0x00000143 System.IntPtr NativeVideoPlayer::get_Activity()
extern void NativeVideoPlayer_get_Activity_m108EE6D906C924684CEC6DD69D2FBEC23A9DBB5D (void);
// 0x00000144 System.Boolean NativeVideoPlayer::get_IsAvailable()
extern void NativeVideoPlayer_get_IsAvailable_m823B06C61418FBDBC35DAA4A66B21527F52E603A (void);
// 0x00000145 System.Boolean NativeVideoPlayer::get_IsPlaying()
extern void NativeVideoPlayer_get_IsPlaying_m2E5DE7269712D276A12A1DC20E68A0C7C9D053CE (void);
// 0x00000146 NativeVideoPlayer/PlabackState NativeVideoPlayer::get_CurrentPlaybackState()
extern void NativeVideoPlayer_get_CurrentPlaybackState_mE90467F47CDA4DF5D910453D4C1297BA32DF0A60 (void);
// 0x00000147 System.Int64 NativeVideoPlayer::get_Duration()
extern void NativeVideoPlayer_get_Duration_m0886DCE7E032CBC9954231CD394520D45D8BDAE2 (void);
// 0x00000148 NativeVideoPlayer/StereoMode NativeVideoPlayer::get_VideoStereoMode()
extern void NativeVideoPlayer_get_VideoStereoMode_m01908C812015B7EFB33B31A9FE523F0ABD3337F2 (void);
// 0x00000149 System.Int32 NativeVideoPlayer::get_VideoWidth()
extern void NativeVideoPlayer_get_VideoWidth_m729F7483A5D0759B858D89464FF476B599C45348 (void);
// 0x0000014A System.Int32 NativeVideoPlayer::get_VideoHeight()
extern void NativeVideoPlayer_get_VideoHeight_m171A1E450D92AA7EB0782FB3833689FD10DCF565 (void);
// 0x0000014B System.Int64 NativeVideoPlayer::get_PlaybackPosition()
extern void NativeVideoPlayer_get_PlaybackPosition_mEE846FE9EB7A63A000406C4176832ACB36A7ADAE (void);
// 0x0000014C System.Void NativeVideoPlayer::set_PlaybackPosition(System.Int64)
extern void NativeVideoPlayer_set_PlaybackPosition_m1CC57A9D73A387CD240A581D039E96AC88FB6FFA (void);
// 0x0000014D System.Void NativeVideoPlayer::PlayVideo(System.String,System.String,System.IntPtr)
extern void NativeVideoPlayer_PlayVideo_mF6EDBA9A868E9B2A032656AB66EBB9BB1E0FB845 (void);
// 0x0000014E System.Void NativeVideoPlayer::Stop()
extern void NativeVideoPlayer_Stop_m14AD77315AC29B0C874D058B0C8FB81184E397B9 (void);
// 0x0000014F System.Void NativeVideoPlayer::Play()
extern void NativeVideoPlayer_Play_m5DFF9917DD1482B0182FCF149F15EB550BF68AE5 (void);
// 0x00000150 System.Void NativeVideoPlayer::Pause()
extern void NativeVideoPlayer_Pause_mBEE54BA105C3700260087E6FF3A44877B509DF1D (void);
// 0x00000151 System.Void NativeVideoPlayer::SetPlaybackSpeed(System.Single)
extern void NativeVideoPlayer_SetPlaybackSpeed_m20B272CBB173EB900F1D77EF61B3CFBD19EA48B1 (void);
// 0x00000152 System.Void NativeVideoPlayer::SetLooping(System.Boolean)
extern void NativeVideoPlayer_SetLooping_mD754507B7ECF37A2433731C9CB1F808ABDC37503 (void);
// 0x00000153 System.Void NativeVideoPlayer::SetListenerRotation(UnityEngine.Quaternion)
extern void NativeVideoPlayer_SetListenerRotation_m0027D91DD19BC0FBD84738F7E75C7867A9E55097 (void);
// 0x00000154 System.Void NativeVideoPlayer::.cctor()
extern void NativeVideoPlayer__cctor_m244E5B0BBB03FC464EC253A3194C25AE8E065D48 (void);
// 0x00000155 System.Void ButtonDownListener::add_onButtonDown(System.Action)
extern void ButtonDownListener_add_onButtonDown_m6A369389838232FEEA1344146C1FD176F2ECE3E0 (void);
// 0x00000156 System.Void ButtonDownListener::remove_onButtonDown(System.Action)
extern void ButtonDownListener_remove_onButtonDown_m88F58309C99390BCEB654878D6C45519ADFB5A02 (void);
// 0x00000157 System.Void ButtonDownListener::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void ButtonDownListener_OnPointerDown_mDC9FE990B96FBF2C6D980F8DBCB327E86BD04C9B (void);
// 0x00000158 System.Void ButtonDownListener::.ctor()
extern void ButtonDownListener__ctor_mD2522D29DCA57136E34554C01F2C1DB18B6B4DD7 (void);
// 0x00000159 MediaPlayerImage/ButtonType MediaPlayerImage::get_buttonType()
extern void MediaPlayerImage_get_buttonType_m738EF74C0EDD4616EFC1E68DC56AC701B53DD2A8 (void);
// 0x0000015A System.Void MediaPlayerImage::set_buttonType(MediaPlayerImage/ButtonType)
extern void MediaPlayerImage_set_buttonType_m86058D07781C3EE3FCEE2546DE6506CA31BB1874 (void);
// 0x0000015B System.Void MediaPlayerImage::OnPopulateMesh(UnityEngine.UI.VertexHelper)
extern void MediaPlayerImage_OnPopulateMesh_mBFADD4CECF3314066C0377254FCB875E8C8F078F (void);
// 0x0000015C System.Void MediaPlayerImage::.ctor()
extern void MediaPlayerImage__ctor_mB86C91BF2885DC329EDD99F24916F9AD717B42D3 (void);
// 0x0000015D System.Boolean MoviePlayerSample::get_IsPlaying()
extern void MoviePlayerSample_get_IsPlaying_mB7FE8B620130A38FB30BC09F7079E12084113CDF (void);
// 0x0000015E System.Void MoviePlayerSample::set_IsPlaying(System.Boolean)
extern void MoviePlayerSample_set_IsPlaying_mB98C234AC3AC146C0F74729906F71A7E9F849CE6 (void);
// 0x0000015F System.Int64 MoviePlayerSample::get_Duration()
extern void MoviePlayerSample_get_Duration_m4A055DBAEAC1819C7091ACEF4E2B8DBE5EF4BAFF (void);
// 0x00000160 System.Void MoviePlayerSample::set_Duration(System.Int64)
extern void MoviePlayerSample_set_Duration_m1B29A541BFA2385B82B3A793817338A24081ECBB (void);
// 0x00000161 System.Int64 MoviePlayerSample::get_PlaybackPosition()
extern void MoviePlayerSample_get_PlaybackPosition_m7B1A095968AFDF410EE031761D5BC5239328E00E (void);
// 0x00000162 System.Void MoviePlayerSample::set_PlaybackPosition(System.Int64)
extern void MoviePlayerSample_set_PlaybackPosition_mD8FC604C1C51A0577071A069BD3780DA303D4260 (void);
// 0x00000163 System.Void MoviePlayerSample::Awake()
extern void MoviePlayerSample_Awake_m096BC4D6D5E6B709F4BB2441B58E489540CAB06A (void);
// 0x00000164 System.Boolean MoviePlayerSample::IsLocalVideo(System.String)
extern void MoviePlayerSample_IsLocalVideo_m3B89DA94A65891DF9D5F3D6B8F7045A4B10897E9 (void);
// 0x00000165 System.Void MoviePlayerSample::UpdateShapeAndStereo()
extern void MoviePlayerSample_UpdateShapeAndStereo_mA373D088B7381DB5A34923E79695184EC0AA1F0C (void);
// 0x00000166 System.Collections.IEnumerator MoviePlayerSample::Start()
extern void MoviePlayerSample_Start_m41862FCCB8907A26C31EC7C5B374DEDD661F61A9 (void);
// 0x00000167 System.Void MoviePlayerSample::Play(System.String,System.String)
extern void MoviePlayerSample_Play_m64FB2763D4DCAC19B89BEE384C53488B702F84AB (void);
// 0x00000168 System.Void MoviePlayerSample::Play()
extern void MoviePlayerSample_Play_m43C298C6CA45290022B7F30B8CB648003469E44B (void);
// 0x00000169 System.Void MoviePlayerSample::Pause()
extern void MoviePlayerSample_Pause_m1A8CFD8E325D63FFB48510A74E79F4CD7A07892B (void);
// 0x0000016A System.Void MoviePlayerSample::SeekTo(System.Int64)
extern void MoviePlayerSample_SeekTo_mA2F91C759B810802D66038A151701C6AAC13B363 (void);
// 0x0000016B System.Void MoviePlayerSample::Update()
extern void MoviePlayerSample_Update_m844225048CC9A23F2DD1AA0544BB61DD7D3867E5 (void);
// 0x0000016C System.Void MoviePlayerSample::SetPlaybackSpeed(System.Single)
extern void MoviePlayerSample_SetPlaybackSpeed_mC9B71A0C6AD6851BE290D41ABB21689BE86BFE82 (void);
// 0x0000016D System.Void MoviePlayerSample::Stop()
extern void MoviePlayerSample_Stop_m869069EB49DB2E8DDC06D6833BA00905D0EC45CD (void);
// 0x0000016E System.Void MoviePlayerSample::OnApplicationPause(System.Boolean)
extern void MoviePlayerSample_OnApplicationPause_mD8379876774437E10F757DE1A6CEF26245746263 (void);
// 0x0000016F System.Void MoviePlayerSample::.ctor()
extern void MoviePlayerSample__ctor_mB1DFDA2F1E1E621CEDDE6898330836B240F1B7B7 (void);
// 0x00000170 System.Void MoviePlayerSample/<Start>d__33::.ctor(System.Int32)
extern void U3CStartU3Ed__33__ctor_mFBD4AF6C95F797D0753DD1E6789AB7EFB4F3A4B3 (void);
// 0x00000171 System.Void MoviePlayerSample/<Start>d__33::System.IDisposable.Dispose()
extern void U3CStartU3Ed__33_System_IDisposable_Dispose_mFD89D5A20E48873E01335A257B97824BE1B9EB56 (void);
// 0x00000172 System.Boolean MoviePlayerSample/<Start>d__33::MoveNext()
extern void U3CStartU3Ed__33_MoveNext_mD5A4E90D0615EF2D769C53930662CE60239B99F9 (void);
// 0x00000173 System.Object MoviePlayerSample/<Start>d__33::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__33_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5BCA0FBB841D0017B1050286ADA36684C73A329D (void);
// 0x00000174 System.Void MoviePlayerSample/<Start>d__33::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__33_System_Collections_IEnumerator_Reset_m8ECDE46C41B27547EFB6B7E24E378DD06DD9107A (void);
// 0x00000175 System.Object MoviePlayerSample/<Start>d__33::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__33_System_Collections_IEnumerator_get_Current_m8D673748B930D29175C9D928B493B015EB8158A9 (void);
// 0x00000176 System.Void MoviePlayerSample/<>c__DisplayClass34_0::.ctor()
extern void U3CU3Ec__DisplayClass34_0__ctor_m744287E1C5682D76E3E690286A071F39718E25A9 (void);
// 0x00000177 System.Void MoviePlayerSample/<>c__DisplayClass34_0::<Play>b__0()
extern void U3CU3Ec__DisplayClass34_0_U3CPlayU3Eb__0_mA90EE1D1E1F6E42A47F0488801D4A7734840FD4C (void);
// 0x00000178 System.Void MoviePlayerSampleControls::Start()
extern void MoviePlayerSampleControls_Start_m625008A6C19107D6BE93FF40434BAF580E2C1D80 (void);
// 0x00000179 System.Void MoviePlayerSampleControls::OnPlayPauseClicked()
extern void MoviePlayerSampleControls_OnPlayPauseClicked_mDFCC988FB43880C3A320FFC326517C5EEA554099 (void);
// 0x0000017A System.Void MoviePlayerSampleControls::OnFastForwardClicked()
extern void MoviePlayerSampleControls_OnFastForwardClicked_mDBE37296203D54341C0DFAA545CD2C37A3C6D36C (void);
// 0x0000017B System.Void MoviePlayerSampleControls::OnRewindClicked()
extern void MoviePlayerSampleControls_OnRewindClicked_m890FFF45A080BEC11F3B0B3A11D492A1F6692C6E (void);
// 0x0000017C System.Void MoviePlayerSampleControls::OnSeekBarMoved(System.Single)
extern void MoviePlayerSampleControls_OnSeekBarMoved_m53704CFB9FDB6324FEA81FC2E7EA57DA12AE04B6 (void);
// 0x0000017D System.Void MoviePlayerSampleControls::Seek(System.Int64)
extern void MoviePlayerSampleControls_Seek_mEC78032CC65FE67AA6A153DB42A4588955341EE5 (void);
// 0x0000017E System.Void MoviePlayerSampleControls::Update()
extern void MoviePlayerSampleControls_Update_m747FB37CA5F8CFBD485486E4517F4748489E38B3 (void);
// 0x0000017F System.Void MoviePlayerSampleControls::SetVisible(System.Boolean)
extern void MoviePlayerSampleControls_SetVisible_mB5C9350C361FC20DE05207E6CAB3D4805D28248F (void);
// 0x00000180 System.Void MoviePlayerSampleControls::.ctor()
extern void MoviePlayerSampleControls__ctor_mB9AEEE8EBB60CE81FD9C5BA1C8154171C669BB7A (void);
// 0x00000181 UnityEngine.Vector4 VectorUtil::ToVector(UnityEngine.Rect)
extern void VectorUtil_ToVector_m6B247D561540B92B96CD296261FF7DC2A02AB201 (void);
// 0x00000182 System.Void AppDeeplinkUI::Start()
extern void AppDeeplinkUI_Start_m775C152A115FF43C1AB34D97D9DC20A34FA705EA (void);
// 0x00000183 System.Void AppDeeplinkUI::Update()
extern void AppDeeplinkUI_Update_m8D48F1E3305A2469B0935D4E829931DCB0D76713 (void);
// 0x00000184 System.Void AppDeeplinkUI::LaunchUnrealDeeplinkSample()
extern void AppDeeplinkUI_LaunchUnrealDeeplinkSample_mA5F8A1774DB5724E43C917EBE88431C6FE0D9236 (void);
// 0x00000185 System.Void AppDeeplinkUI::LaunchSelf()
extern void AppDeeplinkUI_LaunchSelf_m2CA3C1FCE4414860CC1101B7D29B483B9AE45090 (void);
// 0x00000186 System.Void AppDeeplinkUI::LaunchOtherApp()
extern void AppDeeplinkUI_LaunchOtherApp_mD63AA72CBA23EA78C7B91B1E4B59D9B7860B0823 (void);
// 0x00000187 System.Void AppDeeplinkUI::.ctor()
extern void AppDeeplinkUI__ctor_mA31F77CBBA609E0128E496E7D23C0FE41A5F94A6 (void);
// 0x00000188 System.Void CustomDebugUI::Awake()
extern void CustomDebugUI_Awake_m312D4A67E8EA4824658432AE459FA21877975F71 (void);
// 0x00000189 System.Void CustomDebugUI::Start()
extern void CustomDebugUI_Start_m28DD234ADCAAC557207C669CE37554CD7C7CC833 (void);
// 0x0000018A System.Void CustomDebugUI::Update()
extern void CustomDebugUI_Update_mBF9B69FFE07A90FEA92DF170D99F185C952B043E (void);
// 0x0000018B UnityEngine.RectTransform CustomDebugUI::AddTextField(System.String,System.Int32)
extern void CustomDebugUI_AddTextField_m6CF8AA8D2CE855EF4B86EEF85E16E0F5AFC9F6FD (void);
// 0x0000018C System.Void CustomDebugUI::RemoveFromCanvas(UnityEngine.RectTransform,System.Int32)
extern void CustomDebugUI_RemoveFromCanvas_mC32F1E9CA35DFF39075ADC86BF7112A207C9F961 (void);
// 0x0000018D System.Void CustomDebugUI::.ctor()
extern void CustomDebugUI__ctor_m27BB96E142EA068E3029D709C3065D65DC9D2CFC (void);
// 0x0000018E System.Void DebugUISample::Start()
extern void DebugUISample_Start_mF7AB8D2997FF58D02F5EB0FFA83D5B59DB982C7B (void);
// 0x0000018F System.Void DebugUISample::TogglePressed(UnityEngine.UI.Toggle)
extern void DebugUISample_TogglePressed_m2A267F4169FCDC7C8E0DD77FD85253225B6F381C (void);
// 0x00000190 System.Void DebugUISample::RadioPressed(System.String,System.String,UnityEngine.UI.Toggle)
extern void DebugUISample_RadioPressed_m7E36B100115C60405CC14822838E3ECD66418624 (void);
// 0x00000191 System.Void DebugUISample::SliderPressed(System.Single)
extern void DebugUISample_SliderPressed_m6A03CFB38E2BAF3DC6E4C63A7DD2B895F2BD5F37 (void);
// 0x00000192 System.Void DebugUISample::Update()
extern void DebugUISample_Update_m7FA91668B8F852E7FDE02E7F8F82F3643D729C72 (void);
// 0x00000193 System.Void DebugUISample::LogButtonPressed()
extern void DebugUISample_LogButtonPressed_m8198BFFA8627BFEE6BAE268C801B5EE1C4B658F4 (void);
// 0x00000194 System.Void DebugUISample::.ctor()
extern void DebugUISample__ctor_mBD27AFA42BD0FA8EF120FCC4CFE624D2CD6B205B (void);
// 0x00000195 System.Void DebugUISample::<Start>b__2_0(UnityEngine.UI.Toggle)
extern void DebugUISample_U3CStartU3Eb__2_0_mC3BA080F2C315CCC00B545B43B4EC960D6CEA6D3 (void);
// 0x00000196 System.Void DebugUISample::<Start>b__2_1(UnityEngine.UI.Toggle)
extern void DebugUISample_U3CStartU3Eb__2_1_m499D6E39FF2F002CD620BD3A790C87CF58BFBAE3 (void);
// 0x00000197 System.Void DebugUISample::<Start>b__2_2(UnityEngine.UI.Toggle)
extern void DebugUISample_U3CStartU3Eb__2_2_m1D0C6B17BCC797AB931BDD9128F04612252A4517 (void);
// 0x00000198 System.Void DebugUISample::<Start>b__2_3(UnityEngine.UI.Toggle)
extern void DebugUISample_U3CStartU3Eb__2_3_m148206FDC1F12C8E839B21019CFDFBCB7C320CD4 (void);
// 0x00000199 System.Void AnalyticsUI::.ctor()
extern void AnalyticsUI__ctor_m21788905A6C8F76C59714536A99939E5B74426A3 (void);
// 0x0000019A System.Void SampleUI::Start()
extern void SampleUI_Start_m982A3C805909F26B7AA71851484369D105F0A2A5 (void);
// 0x0000019B System.Void SampleUI::Update()
extern void SampleUI_Update_m3509E9A6966DBFBC160CFF3D96B508D1F854113F (void);
// 0x0000019C System.String SampleUI::GetText()
extern void SampleUI_GetText_m26599FA6C9771765EC9AD94F73BAAD00AEE049BD (void);
// 0x0000019D System.Void SampleUI::.ctor()
extern void SampleUI__ctor_mD3D7D360CE2BFF47B7799D39FBC3A99E1D5F211D (void);
// 0x0000019E System.Void StartCrashlytics::.ctor()
extern void StartCrashlytics__ctor_m9C967A85082223F55E9138CB80F4107DE4A2C9BC (void);
// 0x0000019F System.Void HandsActiveChecker::Awake()
extern void HandsActiveChecker_Awake_m789AB07810059246D886069A33494EDE150458DD (void);
// 0x000001A0 System.Void HandsActiveChecker::Update()
extern void HandsActiveChecker_Update_m6C574E921E2E058430A4D6EFD50FB45F27F5526D (void);
// 0x000001A1 System.Collections.IEnumerator HandsActiveChecker::GetCenterEye()
extern void HandsActiveChecker_GetCenterEye_mEAD4DB3FC55FE5F6AA80E74EFFA4BFF053534D0E (void);
// 0x000001A2 System.Void HandsActiveChecker::.ctor()
extern void HandsActiveChecker__ctor_m8875DA4F87EC8FE57A7587203AA1F695AEA6CBA4 (void);
// 0x000001A3 System.Void HandsActiveChecker/<GetCenterEye>d__6::.ctor(System.Int32)
extern void U3CGetCenterEyeU3Ed__6__ctor_m99DAFAB56EEBC093A2EFF1857B7E41EDA623C071 (void);
// 0x000001A4 System.Void HandsActiveChecker/<GetCenterEye>d__6::System.IDisposable.Dispose()
extern void U3CGetCenterEyeU3Ed__6_System_IDisposable_Dispose_m87282416027E1D142DA109AC2CAE4B8A202B7F25 (void);
// 0x000001A5 System.Boolean HandsActiveChecker/<GetCenterEye>d__6::MoveNext()
extern void U3CGetCenterEyeU3Ed__6_MoveNext_m8F91FCC289F307F81C3B0A1A6D16809AEAFFFA8A (void);
// 0x000001A6 System.Object HandsActiveChecker/<GetCenterEye>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGetCenterEyeU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m52AA44D25E603CBD76F6492C277DFA1502C50301 (void);
// 0x000001A7 System.Void HandsActiveChecker/<GetCenterEye>d__6::System.Collections.IEnumerator.Reset()
extern void U3CGetCenterEyeU3Ed__6_System_Collections_IEnumerator_Reset_m60966632955FF20F0859533FE10315801CF3EFCC (void);
// 0x000001A8 System.Object HandsActiveChecker/<GetCenterEye>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CGetCenterEyeU3Ed__6_System_Collections_IEnumerator_get_Current_m45CF38A1CA12804B458CFC1C748D8A3490DF1854 (void);
// 0x000001A9 System.Void CharacterCapsule::Update()
extern void CharacterCapsule_Update_mD56302C0703C800E264601DFCECAD70323B6D8E4 (void);
// 0x000001AA System.Void CharacterCapsule::.ctor()
extern void CharacterCapsule__ctor_m3517E2CB4B11230AC5AF2F46D6FCBBBD3F2FBC0E (void);
// 0x000001AB LocomotionTeleport LocomotionSampleSupport::get_TeleportController()
extern void LocomotionSampleSupport_get_TeleportController_m8E3B912620DEA5687BAC288FE3CE5F8116706216 (void);
// 0x000001AC System.Void LocomotionSampleSupport::Start()
extern void LocomotionSampleSupport_Start_mCF86670570E36A40E41DC7613F8AFD7FAA740A89 (void);
// 0x000001AD System.Void LocomotionSampleSupport::Update()
extern void LocomotionSampleSupport_Update_m031DA7C38919902529A0267E72B70BB5B294C1BB (void);
// 0x000001AE System.Void LocomotionSampleSupport::Log(System.String)
extern void LocomotionSampleSupport_Log_m026777FB96DF6B35ADCDE19FFE75173F9A080C52 (void);
// 0x000001AF TActivate LocomotionSampleSupport::ActivateCategory(UnityEngine.GameObject)
// 0x000001B0 System.Void LocomotionSampleSupport::ActivateHandlers()
// 0x000001B1 System.Void LocomotionSampleSupport::ActivateInput()
// 0x000001B2 System.Void LocomotionSampleSupport::ActivateAim()
// 0x000001B3 System.Void LocomotionSampleSupport::ActivateTarget()
// 0x000001B4 System.Void LocomotionSampleSupport::ActivateOrientation()
// 0x000001B5 System.Void LocomotionSampleSupport::ActivateTransition()
// 0x000001B6 TActivate LocomotionSampleSupport::ActivateCategory()
// 0x000001B7 System.Void LocomotionSampleSupport::UpdateToggle(UnityEngine.UI.Toggle,System.Boolean)
extern void LocomotionSampleSupport_UpdateToggle_mD1A6755435B3A901645BDFA66781E84BB141B17A (void);
// 0x000001B8 System.Void LocomotionSampleSupport::SetupNonCap()
extern void LocomotionSampleSupport_SetupNonCap_mADDA8F8E1129D705CF311C52F138ACC60C3826B5 (void);
// 0x000001B9 System.Void LocomotionSampleSupport::SetupTeleportDefaults()
extern void LocomotionSampleSupport_SetupTeleportDefaults_m5ACC86875EBD2D512ED55DB8211FB894F462B737 (void);
// 0x000001BA UnityEngine.GameObject LocomotionSampleSupport::AddInstance(UnityEngine.GameObject,System.String)
extern void LocomotionSampleSupport_AddInstance_m585F0527D6C6C2351CC04DF1AD6AFDFE9252B09F (void);
// 0x000001BB System.Void LocomotionSampleSupport::SetupNodeTeleport()
extern void LocomotionSampleSupport_SetupNodeTeleport_m553B0B6C9BA5C94F976B0EA0828523B6C7FA8F79 (void);
// 0x000001BC System.Void LocomotionSampleSupport::SetupTwoStickTeleport()
extern void LocomotionSampleSupport_SetupTwoStickTeleport_m0BBFB6EA935A3715A1D1677BC957562431346217 (void);
// 0x000001BD System.Void LocomotionSampleSupport::SetupWalkOnly()
extern void LocomotionSampleSupport_SetupWalkOnly_m1EE59A1C4972A91669F90AD573D410C26DBB370E (void);
// 0x000001BE System.Void LocomotionSampleSupport::SetupLeftStrafeRightTeleport()
extern void LocomotionSampleSupport_SetupLeftStrafeRightTeleport_mAF0F734749B4AEEEF9E7BA5B6131BA447174A7C5 (void);
// 0x000001BF System.Void LocomotionSampleSupport::.ctor()
extern void LocomotionSampleSupport__ctor_m613DB25830E2B519F9C75665ABC4F75F3FBB3A44 (void);
// 0x000001C0 System.Void OVROverlayCanvas::Start()
extern void OVROverlayCanvas_Start_m02FACA531257AF78493D111425986BDE67AB2991 (void);
// 0x000001C1 System.Void OVROverlayCanvas::OnDestroy()
extern void OVROverlayCanvas_OnDestroy_mB9B73D9E5A1C61426EF8989A4BA55E64B920CB29 (void);
// 0x000001C2 System.Void OVROverlayCanvas::OnEnable()
extern void OVROverlayCanvas_OnEnable_m9FF98CCC4F3C096B72D990F0CF8FE6453D6FE7A1 (void);
// 0x000001C3 System.Void OVROverlayCanvas::OnDisable()
extern void OVROverlayCanvas_OnDisable_m9E0AE938543EDE56DB3FC961CB82FD14D6715961 (void);
// 0x000001C4 System.Boolean OVROverlayCanvas::ShouldRender()
extern void OVROverlayCanvas_ShouldRender_mAB1021F516AA15A84D0F9FA33C9DAE329A8C7157 (void);
// 0x000001C5 System.Void OVROverlayCanvas::Update()
extern void OVROverlayCanvas_Update_m6E50C6A065641B95F393BAB2C20ED325FCBBC750 (void);
// 0x000001C6 System.Boolean OVROverlayCanvas::get_overlayEnabled()
extern void OVROverlayCanvas_get_overlayEnabled_m9B539BAF0CDCCE9E1E5EBD918BE874E76F004BDF (void);
// 0x000001C7 System.Void OVROverlayCanvas::set_overlayEnabled(System.Boolean)
extern void OVROverlayCanvas_set_overlayEnabled_mDA991B436B2AB9BA5CBA3F2566352635795AD5F3 (void);
// 0x000001C8 System.Void OVROverlayCanvas::.ctor()
extern void OVROverlayCanvas__ctor_m8B8FA48BC2B16C384FEBB9DA6B887942548C6539 (void);
// 0x000001C9 System.Void OVROverlayCanvas::.cctor()
extern void OVROverlayCanvas__cctor_m172128EA1EF3A5E57D56D3657A8363EE747AC2D9 (void);
// 0x000001CA System.Void AugmentedObject::Start()
extern void AugmentedObject_Start_m8E0AD00236ACFDEAD69389D7467109BEE6B11B58 (void);
// 0x000001CB System.Void AugmentedObject::Update()
extern void AugmentedObject_Update_m3A9B0C9619B59930AC5508F8FE2758CB0BD02E55 (void);
// 0x000001CC System.Void AugmentedObject::Grab(OVRInput/Controller)
extern void AugmentedObject_Grab_m459B031A6D80E520AE75A6A43DBEE1A087DC1CB2 (void);
// 0x000001CD System.Void AugmentedObject::Release()
extern void AugmentedObject_Release_mAB1BE9AD51540219158AF0EFC23E4472C9B7BD3D (void);
// 0x000001CE System.Void AugmentedObject::ToggleShadowType()
extern void AugmentedObject_ToggleShadowType_mE9B7FDE7086D783F599CFE4164FFE16548F0E620 (void);
// 0x000001CF System.Void AugmentedObject::.ctor()
extern void AugmentedObject__ctor_m1BEA4EB1EEF2E15B9D43AF499898A2DC25F6056D (void);
// 0x000001D0 System.Void BrushController::Start()
extern void BrushController_Start_mC8607F5C5E0D3C39318951CE99D90FBE562B9BCB (void);
// 0x000001D1 System.Void BrushController::Update()
extern void BrushController_Update_mD50E4CA311FB894AAA68222ACA91EBE6C30593E1 (void);
// 0x000001D2 System.Void BrushController::Grab(OVRInput/Controller)
extern void BrushController_Grab_mD6E7E5B9DC7580A384370CAC4A26AFF0379EE6CB (void);
// 0x000001D3 System.Void BrushController::Release()
extern void BrushController_Release_m3D7E2EFE180929EF8235DFE5CCB7DEDBF7FD9507 (void);
// 0x000001D4 System.Collections.IEnumerator BrushController::FadeCameraClearColor(UnityEngine.Color,System.Single)
extern void BrushController_FadeCameraClearColor_mF279AC0C77EA6F6F1758D99361413188B619AB05 (void);
// 0x000001D5 System.Collections.IEnumerator BrushController::FadeSphere(UnityEngine.Color,System.Single,System.Boolean)
extern void BrushController_FadeSphere_mFE0E2D3B7CAB66CCBD2A263BF9E3C5234682E455 (void);
// 0x000001D6 System.Void BrushController::.ctor()
extern void BrushController__ctor_m9257CBA4722EF69E6444F82933C2A2123AAE0CC6 (void);
// 0x000001D7 System.Void BrushController/<FadeCameraClearColor>d__8::.ctor(System.Int32)
extern void U3CFadeCameraClearColorU3Ed__8__ctor_m71E38D8AED062F90AA945D9CC0540F9CDC001EE7 (void);
// 0x000001D8 System.Void BrushController/<FadeCameraClearColor>d__8::System.IDisposable.Dispose()
extern void U3CFadeCameraClearColorU3Ed__8_System_IDisposable_Dispose_mB45544A56417DF7024AF0BF2FAC2892BEAA4CB57 (void);
// 0x000001D9 System.Boolean BrushController/<FadeCameraClearColor>d__8::MoveNext()
extern void U3CFadeCameraClearColorU3Ed__8_MoveNext_mADA92C9B3E3117DC738F397AA647A824FCC4BCB1 (void);
// 0x000001DA System.Object BrushController/<FadeCameraClearColor>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CFadeCameraClearColorU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2CCA66E9BC227F6064FBD2B1EBDB29027B712EC6 (void);
// 0x000001DB System.Void BrushController/<FadeCameraClearColor>d__8::System.Collections.IEnumerator.Reset()
extern void U3CFadeCameraClearColorU3Ed__8_System_Collections_IEnumerator_Reset_m669F3DAFD50F24E439427435286DCBE5D5AAC116 (void);
// 0x000001DC System.Object BrushController/<FadeCameraClearColor>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CFadeCameraClearColorU3Ed__8_System_Collections_IEnumerator_get_Current_mD305219E1C23CE52D59AED4BFEF4C16461DAAAE1 (void);
// 0x000001DD System.Void BrushController/<FadeSphere>d__9::.ctor(System.Int32)
extern void U3CFadeSphereU3Ed__9__ctor_mFF2CC1B385DBDFF11A4C2EA5F29E290C231DFBE7 (void);
// 0x000001DE System.Void BrushController/<FadeSphere>d__9::System.IDisposable.Dispose()
extern void U3CFadeSphereU3Ed__9_System_IDisposable_Dispose_mBA8495222AE0CBF7E4A4569A41D5292D973A052B (void);
// 0x000001DF System.Boolean BrushController/<FadeSphere>d__9::MoveNext()
extern void U3CFadeSphereU3Ed__9_MoveNext_m3941BF83B9BCB6851DA0EAC9DBF79BC9052E9825 (void);
// 0x000001E0 System.Object BrushController/<FadeSphere>d__9::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CFadeSphereU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m09FCC5A8134E70A5773ABDBD3771EC06F0EFB03F (void);
// 0x000001E1 System.Void BrushController/<FadeSphere>d__9::System.Collections.IEnumerator.Reset()
extern void U3CFadeSphereU3Ed__9_System_Collections_IEnumerator_Reset_m38A51A10EA31A82FEC63C26F45E5AD2A52F86AA6 (void);
// 0x000001E2 System.Object BrushController/<FadeSphere>d__9::System.Collections.IEnumerator.get_Current()
extern void U3CFadeSphereU3Ed__9_System_Collections_IEnumerator_get_Current_m86BCBEDDEC870379C62644F1C8BF0CDB327A86C9 (void);
// 0x000001E3 System.Void EnableUnpremultipliedAlpha::Start()
extern void EnableUnpremultipliedAlpha_Start_m4926EC7506FF573EADA7ECC9CBB7D7E4DECAA67F (void);
// 0x000001E4 System.Void EnableUnpremultipliedAlpha::.ctor()
extern void EnableUnpremultipliedAlpha__ctor_m72150D5AFC1B04755F8960E4A7E2211BC3E6BF0A (void);
// 0x000001E5 System.Void Flashlight::LateUpdate()
extern void Flashlight_LateUpdate_mC96F82CE200FBCB574AE8EC09BC4A6794C3D8192 (void);
// 0x000001E6 System.Void Flashlight::ToggleFlashlight()
extern void Flashlight_ToggleFlashlight_m2C0E6D9CB2AFB7BC18058A37692B42554F53E0AD (void);
// 0x000001E7 System.Void Flashlight::EnableFlashlight(System.Boolean)
extern void Flashlight_EnableFlashlight_mB6ED8C5FDB101A26CD2329430939DC65D087907E (void);
// 0x000001E8 System.Void Flashlight::.ctor()
extern void Flashlight__ctor_m3126420E3768B6D3CFD0472251F924BE38E81671 (void);
// 0x000001E9 System.Void FlashlightController::Start()
extern void FlashlightController_Start_mA7C1D7727D1DE21DCFD4FD4A1FD3A432DFAD5A56 (void);
// 0x000001EA System.Void FlashlightController::LateUpdate()
extern void FlashlightController_LateUpdate_m6B632F81EDC2907F75EB22CBCE2BC4ECCAA121CD (void);
// 0x000001EB System.Void FlashlightController::FindHands()
extern void FlashlightController_FindHands_mBC00BF0B7AF22181B8586036875B7C9DB3C531C4 (void);
// 0x000001EC System.Void FlashlightController::AlignWithHand(OVRHand,OVRSkeleton)
extern void FlashlightController_AlignWithHand_mCE1A47A2AFD6232C0EA1D8D220E33454CD980E55 (void);
// 0x000001ED System.Void FlashlightController::AlignWithController(OVRInput/Controller)
extern void FlashlightController_AlignWithController_m124649B6FC7BDB21BCE91087F5AF313E83EAAB37 (void);
// 0x000001EE System.Void FlashlightController::Grab(OVRInput/Controller)
extern void FlashlightController_Grab_m1C763114DEBABE1752067BB044130A9BB5A75175 (void);
// 0x000001EF System.Void FlashlightController::Release()
extern void FlashlightController_Release_m0B6874BDB6CBABE601DB5C94FAD11F11C6F4FE67 (void);
// 0x000001F0 System.Collections.IEnumerator FlashlightController::FadeLighting(UnityEngine.Color,System.Single,System.Single)
extern void FlashlightController_FadeLighting_mE6DADFDB6FE28475B62BB3D04E99568CCC8052D8 (void);
// 0x000001F1 System.Void FlashlightController::.ctor()
extern void FlashlightController__ctor_mB8B30045D45FF7336C3D1ED3C2343C40EA1EEA10 (void);
// 0x000001F2 System.Void FlashlightController/<FadeLighting>d__17::.ctor(System.Int32)
extern void U3CFadeLightingU3Ed__17__ctor_m3178D02F76D3965BBFFA4F56F2F4243AF9B3B33A (void);
// 0x000001F3 System.Void FlashlightController/<FadeLighting>d__17::System.IDisposable.Dispose()
extern void U3CFadeLightingU3Ed__17_System_IDisposable_Dispose_m636516D26E0FC21793138E4235EAAFD9B87723BF (void);
// 0x000001F4 System.Boolean FlashlightController/<FadeLighting>d__17::MoveNext()
extern void U3CFadeLightingU3Ed__17_MoveNext_m114DFA09D1CD0CF21952702777E86C16871EADAA (void);
// 0x000001F5 System.Object FlashlightController/<FadeLighting>d__17::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CFadeLightingU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD9E7E8152BD8F758715E45BC1885F51A63E26315 (void);
// 0x000001F6 System.Void FlashlightController/<FadeLighting>d__17::System.Collections.IEnumerator.Reset()
extern void U3CFadeLightingU3Ed__17_System_Collections_IEnumerator_Reset_m532D658BF737F23E5DE75444F9DED652DF6FFC27 (void);
// 0x000001F7 System.Object FlashlightController/<FadeLighting>d__17::System.Collections.IEnumerator.get_Current()
extern void U3CFadeLightingU3Ed__17_System_Collections_IEnumerator_get_Current_mCCF09AD2D019507FE4A94EE11AFAE44708D1A70E (void);
// 0x000001F8 System.Void GrabObject::Grab(OVRInput/Controller)
extern void GrabObject_Grab_mB218C144950CC665DEF2C0D2621EC3BAB58F4DDB (void);
// 0x000001F9 System.Void GrabObject::Release()
extern void GrabObject_Release_m87C19D984E37BBC51F11AE79362BCC463D9DF541 (void);
// 0x000001FA System.Void GrabObject::CursorPos(UnityEngine.Vector3)
extern void GrabObject_CursorPos_m712B6B88A9A282219C22F5AC6D91563EF6CF129A (void);
// 0x000001FB System.Void GrabObject::.ctor()
extern void GrabObject__ctor_mE34AA1EC033A0A74D6E8F881A20312EAD2E37316 (void);
// 0x000001FC System.Void GrabObject/GrabbedObject::.ctor(System.Object,System.IntPtr)
extern void GrabbedObject__ctor_mD71655859A58277437D2D41BF2D2E32ED45A3142 (void);
// 0x000001FD System.Void GrabObject/GrabbedObject::Invoke(OVRInput/Controller)
extern void GrabbedObject_Invoke_mC5D11C66F1CA103D44AC0120309B8329C8C5AC12 (void);
// 0x000001FE System.IAsyncResult GrabObject/GrabbedObject::BeginInvoke(OVRInput/Controller,System.AsyncCallback,System.Object)
extern void GrabbedObject_BeginInvoke_m673B6B520980FE487E8E3698AC7C5E34E3999F7D (void);
// 0x000001FF System.Void GrabObject/GrabbedObject::EndInvoke(System.IAsyncResult)
extern void GrabbedObject_EndInvoke_mE288DA466CEF66AE67D22C3A85FC4FF0340AC220 (void);
// 0x00000200 System.Void GrabObject/ReleasedObject::.ctor(System.Object,System.IntPtr)
extern void ReleasedObject__ctor_mA1EB137D310B511053A0A1EB8F02C1A879C50E37 (void);
// 0x00000201 System.Void GrabObject/ReleasedObject::Invoke()
extern void ReleasedObject_Invoke_mAD25C4E4B23562D2DC5EA00C154F670B80B2EC1E (void);
// 0x00000202 System.IAsyncResult GrabObject/ReleasedObject::BeginInvoke(System.AsyncCallback,System.Object)
extern void ReleasedObject_BeginInvoke_mCF974E830722A67E3C0E157F95722386F5135C72 (void);
// 0x00000203 System.Void GrabObject/ReleasedObject::EndInvoke(System.IAsyncResult)
extern void ReleasedObject_EndInvoke_m022CDE0C465D8F488CB44DF0F4537DC8D8F97EBF (void);
// 0x00000204 System.Void GrabObject/SetCursorPosition::.ctor(System.Object,System.IntPtr)
extern void SetCursorPosition__ctor_mFF389CC65BAD558CBA69A47B907658D65C53A63E (void);
// 0x00000205 System.Void GrabObject/SetCursorPosition::Invoke(UnityEngine.Vector3)
extern void SetCursorPosition_Invoke_m15B4D29A4EFC2AA3C15ECF436299830A14AEA2F8 (void);
// 0x00000206 System.IAsyncResult GrabObject/SetCursorPosition::BeginInvoke(UnityEngine.Vector3,System.AsyncCallback,System.Object)
extern void SetCursorPosition_BeginInvoke_mEC0A62FFCF845C61EB8FF6587D574FFEE524AFE5 (void);
// 0x00000207 System.Void GrabObject/SetCursorPosition::EndInvoke(System.IAsyncResult)
extern void SetCursorPosition_EndInvoke_m621EE977B55491598884A9DFBFAC6D7B247250AE (void);
// 0x00000208 System.Void HandMeshMask::Awake()
extern void HandMeshMask_Awake_m507910DB71C1F770805606FBE79EC77CAE9E0896 (void);
// 0x00000209 System.Void HandMeshMask::Update()
extern void HandMeshMask_Update_m38AC51F8B4FAD5EB72AF1C53379AD9230B4D6185 (void);
// 0x0000020A System.Void HandMeshMask::CreateHandMesh()
extern void HandMeshMask_CreateHandMesh_m520E9CAE4BB1236F888D480F46994CDBC82BF3A7 (void);
// 0x0000020B System.Void HandMeshMask::AddKnuckleMesh(System.Int32,System.Single,System.Single,UnityEngine.Vector3,UnityEngine.Vector3)
extern void HandMeshMask_AddKnuckleMesh_m7FE07234E6CE21E5C1930864ADFE9F4E5A878EC9 (void);
// 0x0000020C System.Void HandMeshMask::AddPalmMesh(System.Int32)
extern void HandMeshMask_AddPalmMesh_m86D1CFFDA37877F6DDA84DEB1EB000ADE491CB5F (void);
// 0x0000020D System.Void HandMeshMask::AddVertex(UnityEngine.Vector3,UnityEngine.Vector2,UnityEngine.Color)
extern void HandMeshMask_AddVertex_mFC6D3205D1524A29258D1E7C25257D6A3EB0DE2A (void);
// 0x0000020E System.Void HandMeshMask::.ctor()
extern void HandMeshMask__ctor_m37E35E530849032B1EF4024717644B8FC3723361 (void);
// 0x0000020F System.Void HandMeshUI::Start()
extern void HandMeshUI_Start_m80C99FE9B881C46B86AE58C885846979489F8E2F (void);
// 0x00000210 System.Void HandMeshUI::Update()
extern void HandMeshUI_Update_mAC47D9C487ADFA21CB64CB5DA2735E72EB8A3345 (void);
// 0x00000211 System.Void HandMeshUI::SetSliderValue(System.Int32,System.Single,System.Boolean)
extern void HandMeshUI_SetSliderValue_m91E3D3B63E9A6677816037BB8DE109815A111FD5 (void);
// 0x00000212 System.Void HandMeshUI::CheckForHands()
extern void HandMeshUI_CheckForHands_m5C8F16DA9E3D2B1906E4681B453190E184D9048D (void);
// 0x00000213 System.Void HandMeshUI::.ctor()
extern void HandMeshUI__ctor_m85C3A4E86D772F5799B0271350C9564E31FDFC89 (void);
// 0x00000214 System.Void ObjectManipulator::Start()
extern void ObjectManipulator_Start_mEBCDF18C339DB050547E7FAC14A140A2DEFEB4C9 (void);
// 0x00000215 System.Void ObjectManipulator::Update()
extern void ObjectManipulator_Update_m72C78567C8C72FC86BB069AD411212072EA8F03D (void);
// 0x00000216 System.Void ObjectManipulator::GrabHoverObject(UnityEngine.GameObject,UnityEngine.Vector3,UnityEngine.Quaternion)
extern void ObjectManipulator_GrabHoverObject_mBC43E1E7E210F0D296012D424536B74C00805A5E (void);
// 0x00000217 System.Void ObjectManipulator::ReleaseObject()
extern void ObjectManipulator_ReleaseObject_m02231C55BA814EDC06B3F938B7C2E8DEAB914E28 (void);
// 0x00000218 System.Collections.IEnumerator ObjectManipulator::StartDemo()
extern void ObjectManipulator_StartDemo_m0754E0B57F00F054718FB66F9C646CB3882B26F9 (void);
// 0x00000219 System.Void ObjectManipulator::FindHoverObject(UnityEngine.Vector3,UnityEngine.Quaternion)
extern void ObjectManipulator_FindHoverObject_m3E6E90C275DF5252101B82C80D2CDBB5C22C4A37 (void);
// 0x0000021A System.Void ObjectManipulator::ManipulateObject(UnityEngine.GameObject,UnityEngine.Vector3,UnityEngine.Quaternion)
extern void ObjectManipulator_ManipulateObject_m229CCFCC09F5B4C39EFDE9E1D83993FB4675511C (void);
// 0x0000021B System.Void ObjectManipulator::ClampGrabOffset(UnityEngine.Vector3&,System.Single)
extern void ObjectManipulator_ClampGrabOffset_m53081A466E925B6FBBE4AFF4850DF53E4531B7A7 (void);
// 0x0000021C UnityEngine.Vector3 ObjectManipulator::ClampScale(UnityEngine.Vector3,UnityEngine.Vector2)
extern void ObjectManipulator_ClampScale_mAD641AE21DC8D808B1E66B18906A701471035267 (void);
// 0x0000021D System.Void ObjectManipulator::CheckForDominantHand()
extern void ObjectManipulator_CheckForDominantHand_m5492FB8645B7165F98D80E4F219962EA4DA4CBCD (void);
// 0x0000021E System.Void ObjectManipulator::AssignInstructions(GrabObject)
extern void ObjectManipulator_AssignInstructions_mA360EDC3C9EC6D0BAAF1C6D16618803917B3CC02 (void);
// 0x0000021F System.Void ObjectManipulator::.ctor()
extern void ObjectManipulator__ctor_mEEE59FC77CA6A41814536C9DFCB24192D5F21DE8 (void);
// 0x00000220 System.Void ObjectManipulator/<StartDemo>d__23::.ctor(System.Int32)
extern void U3CStartDemoU3Ed__23__ctor_m93B55F7FFD26DDF5A738871EBEB2107BF95DE4C7 (void);
// 0x00000221 System.Void ObjectManipulator/<StartDemo>d__23::System.IDisposable.Dispose()
extern void U3CStartDemoU3Ed__23_System_IDisposable_Dispose_m474A042BE09520BAFF40F9DAF52345B48C09EBD4 (void);
// 0x00000222 System.Boolean ObjectManipulator/<StartDemo>d__23::MoveNext()
extern void U3CStartDemoU3Ed__23_MoveNext_mD24727A203FF95967F58B93DAC73148D97E43ECF (void);
// 0x00000223 System.Object ObjectManipulator/<StartDemo>d__23::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartDemoU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m19A26D41F7FB9A11FB058C438E02F555FB51BA20 (void);
// 0x00000224 System.Void ObjectManipulator/<StartDemo>d__23::System.Collections.IEnumerator.Reset()
extern void U3CStartDemoU3Ed__23_System_Collections_IEnumerator_Reset_m4291011401E9FEB2BEB0922EAEC7AEF435F7D49B (void);
// 0x00000225 System.Object ObjectManipulator/<StartDemo>d__23::System.Collections.IEnumerator.get_Current()
extern void U3CStartDemoU3Ed__23_System_Collections_IEnumerator_get_Current_m804A4428AFF3C3D87F16FAD3D707FE4585EB405A (void);
// 0x00000226 System.Void OverlayPassthrough::Start()
extern void OverlayPassthrough_Start_m97C9F6B31E6315147C9E6EB5E796CA724229751A (void);
// 0x00000227 System.Void OverlayPassthrough::Update()
extern void OverlayPassthrough_Update_m5553EE278872B84F1DEB50AE402E7B512D997CD7 (void);
// 0x00000228 System.Void OverlayPassthrough::.ctor()
extern void OverlayPassthrough__ctor_mF913DC5B9EA0FCDC65A0E34CAF78F74F5057B499 (void);
// 0x00000229 System.Void PassthroughBrush::OnDisable()
extern void PassthroughBrush_OnDisable_m838B38AE602884CCB0BE04DE661EA60F012E47FB (void);
// 0x0000022A System.Void PassthroughBrush::LateUpdate()
extern void PassthroughBrush_LateUpdate_m03306C8C4FA821AA69875F16A21EF9F9908DE954 (void);
// 0x0000022B System.Void PassthroughBrush::StartLine(UnityEngine.Vector3)
extern void PassthroughBrush_StartLine_mEDDC47E33545C96489E037E8B136D4CA579624BE (void);
// 0x0000022C System.Void PassthroughBrush::UpdateLine(UnityEngine.Vector3)
extern void PassthroughBrush_UpdateLine_m508789A468A107D18B3C06A34E1A0A48FCA59FF0 (void);
// 0x0000022D System.Void PassthroughBrush::ClearLines()
extern void PassthroughBrush_ClearLines_m684A85937EAF8304462A02BB40F5167779554BF2 (void);
// 0x0000022E System.Void PassthroughBrush::UndoInkLine()
extern void PassthroughBrush_UndoInkLine_m23CD97DDBA483D18F39256132BDD62F5F0B2A63A (void);
// 0x0000022F System.Void PassthroughBrush::.ctor()
extern void PassthroughBrush__ctor_mDEF0A84B7E60A4F9FE0D7584CA5F2F0D92A89311 (void);
// 0x00000230 System.Void PassthroughController::Start()
extern void PassthroughController_Start_m086EE35AA2F6701D88686F0A874F2D3AB9684631 (void);
// 0x00000231 System.Void PassthroughController::Update()
extern void PassthroughController_Update_mCE0679C2D8F409F507F99818B9FFB4467554FFD1 (void);
// 0x00000232 System.Void PassthroughController::.ctor()
extern void PassthroughController__ctor_mB542E27BD18002805C29FA8B8AE4E70101CC2B12 (void);
// 0x00000233 System.Void PassthroughProjectionSurface::Start()
extern void PassthroughProjectionSurface_Start_m1C289BDEF11167F3B6B3CC13CA3D2B0FB243A94B (void);
// 0x00000234 System.Void PassthroughProjectionSurface::Update()
extern void PassthroughProjectionSurface_Update_m32347B174938842813CB23FF93C7DB52E17320AC (void);
// 0x00000235 System.Void PassthroughProjectionSurface::.ctor()
extern void PassthroughProjectionSurface__ctor_m62D8E3BAA312AF5CA874323872CD7E14EA0251C4 (void);
// 0x00000236 System.Void PassthroughStyler::Start()
extern void PassthroughStyler_Start_m4092C1316E5C3612B264AF1E185992BBBE17C3C6 (void);
// 0x00000237 System.Void PassthroughStyler::Update()
extern void PassthroughStyler_Update_mDDBAB8B2F74B759734BAB13249701F84AE886D8F (void);
// 0x00000238 System.Void PassthroughStyler::Grab(OVRInput/Controller)
extern void PassthroughStyler_Grab_mC1D22409F8C813A9516E1BC8C38762359F008BE4 (void);
// 0x00000239 System.Void PassthroughStyler::Release()
extern void PassthroughStyler_Release_m64EC76A02BD96B9818C1838EE93B509F0D59A28A (void);
// 0x0000023A System.Collections.IEnumerator PassthroughStyler::FadeToCurrentStyle(System.Single)
extern void PassthroughStyler_FadeToCurrentStyle_m89B40CBE7FC06A936547F98A3A11CE491E62F75C (void);
// 0x0000023B System.Collections.IEnumerator PassthroughStyler::FadeToDefaultPassthrough(System.Single)
extern void PassthroughStyler_FadeToDefaultPassthrough_m9CDB8FB4E0D030CB89347B19B531763D9E35EC6D (void);
// 0x0000023C System.Void PassthroughStyler::OnBrightnessChanged(System.Single)
extern void PassthroughStyler_OnBrightnessChanged_mAD761CCF67D62E8B6C31BA3E430E388310FD3C8A (void);
// 0x0000023D System.Void PassthroughStyler::OnContrastChanged(System.Single)
extern void PassthroughStyler_OnContrastChanged_mD62F12B93AD84016661953D7AC62A910E794D97C (void);
// 0x0000023E System.Void PassthroughStyler::OnAlphaChanged(System.Single)
extern void PassthroughStyler_OnAlphaChanged_m2E29232FB7882E0CDD74227B63A6B3409EA71568 (void);
// 0x0000023F System.Void PassthroughStyler::ShowFullMenu(System.Boolean)
extern void PassthroughStyler_ShowFullMenu_m63597CE4A8078251E6BA27225B375A4C493D310B (void);
// 0x00000240 System.Void PassthroughStyler::Cursor(UnityEngine.Vector3)
extern void PassthroughStyler_Cursor_m7BA7DDF0FAD50949DC8C251499BB8000BF2FD16D (void);
// 0x00000241 System.Void PassthroughStyler::DoColorDrag(System.Boolean)
extern void PassthroughStyler_DoColorDrag_m11B7DA9E8F21113859605E888C3BEB871684E2A5 (void);
// 0x00000242 System.Void PassthroughStyler::GetColorFromWheel()
extern void PassthroughStyler_GetColorFromWheel_mA52723A4C04478A9E36FD6BEADA82662744EBEBE (void);
// 0x00000243 System.Void PassthroughStyler::.ctor()
extern void PassthroughStyler__ctor_m5F97489280B446110F4C2BAAD9327E817821B130 (void);
// 0x00000244 System.Void PassthroughStyler/<FadeToCurrentStyle>d__18::.ctor(System.Int32)
extern void U3CFadeToCurrentStyleU3Ed__18__ctor_m6035597BE30443B81702055683400A4E114ACDD8 (void);
// 0x00000245 System.Void PassthroughStyler/<FadeToCurrentStyle>d__18::System.IDisposable.Dispose()
extern void U3CFadeToCurrentStyleU3Ed__18_System_IDisposable_Dispose_m1A9700B0CDAA3E66755A0252738541E51CF8FE4B (void);
// 0x00000246 System.Boolean PassthroughStyler/<FadeToCurrentStyle>d__18::MoveNext()
extern void U3CFadeToCurrentStyleU3Ed__18_MoveNext_m65A7265C8CFDE030F527396839DDFCF49FCEE6DA (void);
// 0x00000247 System.Object PassthroughStyler/<FadeToCurrentStyle>d__18::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CFadeToCurrentStyleU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1D0216642B51BD1DD34FF3F146ECBE7DEE474308 (void);
// 0x00000248 System.Void PassthroughStyler/<FadeToCurrentStyle>d__18::System.Collections.IEnumerator.Reset()
extern void U3CFadeToCurrentStyleU3Ed__18_System_Collections_IEnumerator_Reset_mFB080307112AC99A1B9B4B66D5B5F4F1AC119D01 (void);
// 0x00000249 System.Object PassthroughStyler/<FadeToCurrentStyle>d__18::System.Collections.IEnumerator.get_Current()
extern void U3CFadeToCurrentStyleU3Ed__18_System_Collections_IEnumerator_get_Current_m689B3AD6F09DEAAB97AF382C3E5069BA6A24BCD7 (void);
// 0x0000024A System.Void PassthroughStyler/<FadeToDefaultPassthrough>d__19::.ctor(System.Int32)
extern void U3CFadeToDefaultPassthroughU3Ed__19__ctor_mA5B4C841E6A03409D8B853A6DA5C88E57CD7AC6B (void);
// 0x0000024B System.Void PassthroughStyler/<FadeToDefaultPassthrough>d__19::System.IDisposable.Dispose()
extern void U3CFadeToDefaultPassthroughU3Ed__19_System_IDisposable_Dispose_mC41A619DD79902A65FD7FBB4F6BC88E81E56D050 (void);
// 0x0000024C System.Boolean PassthroughStyler/<FadeToDefaultPassthrough>d__19::MoveNext()
extern void U3CFadeToDefaultPassthroughU3Ed__19_MoveNext_m6D2608EC6E0869CF0D742E4716F98A308F74CBE1 (void);
// 0x0000024D System.Object PassthroughStyler/<FadeToDefaultPassthrough>d__19::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CFadeToDefaultPassthroughU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m09BAA401CD83AD635F5B79D9076EA6DE615AB35B (void);
// 0x0000024E System.Void PassthroughStyler/<FadeToDefaultPassthrough>d__19::System.Collections.IEnumerator.Reset()
extern void U3CFadeToDefaultPassthroughU3Ed__19_System_Collections_IEnumerator_Reset_m52FC0E312E9BBD076EB8A6837B646BBB3C4D8341 (void);
// 0x0000024F System.Object PassthroughStyler/<FadeToDefaultPassthrough>d__19::System.Collections.IEnumerator.get_Current()
extern void U3CFadeToDefaultPassthroughU3Ed__19_System_Collections_IEnumerator_get_Current_mCA24F0BC60A66684360A279879D8097EFDF055C8 (void);
// 0x00000250 System.Void PassthroughSurface::Start()
extern void PassthroughSurface_Start_m7E506149753FD8679C9CB718543978CA53CE1F74 (void);
// 0x00000251 System.Void PassthroughSurface::.ctor()
extern void PassthroughSurface__ctor_m6B56B0E135BDB59C84145F3D6DC79877B2C65090 (void);
// 0x00000252 System.Void SPPquad::Start()
extern void SPPquad_Start_m092C0AF73D6B9CD60ED91D3DAF859FDAA4EF5896 (void);
// 0x00000253 System.Void SPPquad::Grab(OVRInput/Controller)
extern void SPPquad_Grab_m8B4D385FEA16E6FFBD1A49860DB3B6FB13295A17 (void);
// 0x00000254 System.Void SPPquad::Release()
extern void SPPquad_Release_mE8CA2CE6FBABBFA6CFBA60A468981092C0914A63 (void);
// 0x00000255 System.Void SPPquad::.ctor()
extern void SPPquad__ctor_mE270AE3BB58A6EBD5FB9C47F99428A242A79CC41 (void);
// 0x00000256 System.Void SceneSampler::Awake()
extern void SceneSampler_Awake_m632DFE6F8657193318C2BD87E035227C40EB8507 (void);
// 0x00000257 System.Void SceneSampler::Update()
extern void SceneSampler_Update_m3D06DA05BAACAA52006AF75D138BCD5D4923207E (void);
// 0x00000258 System.Void SceneSampler::.ctor()
extern void SceneSampler__ctor_m613B46CE973907D623D017AD04DAF9B991B61C75 (void);
// 0x00000259 System.Void SelectivePassthroughExperience::Update()
extern void SelectivePassthroughExperience_Update_m0DA48E563BEB1B36C67E476AF5745FCB13B88903 (void);
// 0x0000025A System.Void SelectivePassthroughExperience::.ctor()
extern void SelectivePassthroughExperience__ctor_m312145A2A5A8560CE04653F4F4799F843525FD62 (void);
// 0x0000025B System.Void BouncingBallLogic::OnCollisionEnter()
extern void BouncingBallLogic_OnCollisionEnter_m97964DD8BDF64D2285B6D341EE320209DADF23D0 (void);
// 0x0000025C System.Void BouncingBallLogic::Start()
extern void BouncingBallLogic_Start_m6ED9444CFD23A527999969B09194731C07C274D3 (void);
// 0x0000025D System.Void BouncingBallLogic::Update()
extern void BouncingBallLogic_Update_m4B85E41A71F892783CE615AE23342FEE6BF7A4F7 (void);
// 0x0000025E System.Void BouncingBallLogic::UpdateVisibility()
extern void BouncingBallLogic_UpdateVisibility_m2CCAE3EC443F7F98D95DFE48A4539A2DEB69458A (void);
// 0x0000025F System.Void BouncingBallLogic::SetVisible(System.Boolean)
extern void BouncingBallLogic_SetVisible_m478FD34F99FB96C954133266A5865AF2D438D838 (void);
// 0x00000260 System.Void BouncingBallLogic::Release(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern void BouncingBallLogic_Release_m3429304DB0C5D4057AB984194F5F61C201AEC531 (void);
// 0x00000261 System.Collections.IEnumerator BouncingBallLogic::PlayPopCallback(System.Single)
extern void BouncingBallLogic_PlayPopCallback_m7ED3FF5AE8C9F6D428CCFD03CB152A0EF8E34712 (void);
// 0x00000262 System.Void BouncingBallLogic::.ctor()
extern void BouncingBallLogic__ctor_m41DF8D5FCD1944E15E75B3E218DD189B9427FF4A (void);
// 0x00000263 System.Void BouncingBallLogic/<PlayPopCallback>d__18::.ctor(System.Int32)
extern void U3CPlayPopCallbackU3Ed__18__ctor_m09CAC299D59FD092C88CC7D4F5E2C710C3B73571 (void);
// 0x00000264 System.Void BouncingBallLogic/<PlayPopCallback>d__18::System.IDisposable.Dispose()
extern void U3CPlayPopCallbackU3Ed__18_System_IDisposable_Dispose_mB7B49306A5581D3DEA1609F043E3F4332A7462C4 (void);
// 0x00000265 System.Boolean BouncingBallLogic/<PlayPopCallback>d__18::MoveNext()
extern void U3CPlayPopCallbackU3Ed__18_MoveNext_m5381F075A56A616247587E14F1609480C0B9426E (void);
// 0x00000266 System.Object BouncingBallLogic/<PlayPopCallback>d__18::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CPlayPopCallbackU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m01F862A1969B60101D9B821CB37B0048F1908909 (void);
// 0x00000267 System.Void BouncingBallLogic/<PlayPopCallback>d__18::System.Collections.IEnumerator.Reset()
extern void U3CPlayPopCallbackU3Ed__18_System_Collections_IEnumerator_Reset_mC415B1F10EBA24CBC428796DC3CE9D7B7890C1F4 (void);
// 0x00000268 System.Object BouncingBallLogic/<PlayPopCallback>d__18::System.Collections.IEnumerator.get_Current()
extern void U3CPlayPopCallbackU3Ed__18_System_Collections_IEnumerator_get_Current_mC01C5BAF3D09B65E6855C965728867AA6FC8B5F7 (void);
// 0x00000269 System.Void BouncingBallMgr::Update()
extern void BouncingBallMgr_Update_m9F87AFA8FE3EE6ADECBABF683C9E3FBDBE18DC5E (void);
// 0x0000026A System.Void BouncingBallMgr::.ctor()
extern void BouncingBallMgr__ctor_mBD94F44896B5ACD60FE139EE29D7B1AE5E4AB7FE (void);
// 0x0000026B System.Void FurnitureSpawner::Start()
extern void FurnitureSpawner_Start_m7B287469E81F13BCB22A8EF4CC3DE470A7E00BA3 (void);
// 0x0000026C System.Void FurnitureSpawner::SpawnSpawnable()
extern void FurnitureSpawner_SpawnSpawnable_m14F8BF89BFC117F1D2604062828C309B52C2E0AE (void);
// 0x0000026D System.Boolean FurnitureSpawner::FindValidSpawnable(Spawnable&)
extern void FurnitureSpawner_FindValidSpawnable_m11054694B333F94EA9BB3F7CA566EC8EF05E5086 (void);
// 0x0000026E System.Void FurnitureSpawner::AddRoomLight()
extern void FurnitureSpawner_AddRoomLight_mEA758994AD28A91AFEFD2D680C5C43D5DEC40445 (void);
// 0x0000026F System.Void FurnitureSpawner::GetVolumeFromTopPlane(UnityEngine.Transform,UnityEngine.Vector2,System.Single,UnityEngine.Vector3&,UnityEngine.Quaternion&,UnityEngine.Vector3&)
extern void FurnitureSpawner_GetVolumeFromTopPlane_m75C21F38C62F993856560A8E6E06331B9E070739 (void);
// 0x00000270 System.Void FurnitureSpawner::.ctor()
extern void FurnitureSpawner__ctor_m2704E3259085F2786CC23C4CDCC1A91A0F4965D4 (void);
// 0x00000271 System.Collections.IEnumerator MyCustomSceneModelLoader::DelayedLoad()
extern void MyCustomSceneModelLoader_DelayedLoad_mE1F053ABA645BB5B358558174A1C5A5C3763ABB4 (void);
// 0x00000272 System.Void MyCustomSceneModelLoader::OnStart()
extern void MyCustomSceneModelLoader_OnStart_mB8048368DCC911FAE90630B4F21B3D65513615AC (void);
// 0x00000273 System.Void MyCustomSceneModelLoader::OnNoSceneModelToLoad()
extern void MyCustomSceneModelLoader_OnNoSceneModelToLoad_m5F80931F5C7DC504F044B49A57FFE811A8E7EE8D (void);
// 0x00000274 System.Void MyCustomSceneModelLoader::.ctor()
extern void MyCustomSceneModelLoader__ctor_m5389D475E709A8C6FFA754A164DAFE7ADC025445 (void);
// 0x00000275 System.Void MyCustomSceneModelLoader/<DelayedLoad>d__0::.ctor(System.Int32)
extern void U3CDelayedLoadU3Ed__0__ctor_mE1E7EFE63326C48E90B5F6422206DA6D19010C4E (void);
// 0x00000276 System.Void MyCustomSceneModelLoader/<DelayedLoad>d__0::System.IDisposable.Dispose()
extern void U3CDelayedLoadU3Ed__0_System_IDisposable_Dispose_mBA61F920C8E466D873890ACF3FAB4BB60AE76186 (void);
// 0x00000277 System.Boolean MyCustomSceneModelLoader/<DelayedLoad>d__0::MoveNext()
extern void U3CDelayedLoadU3Ed__0_MoveNext_mB3DCA68048992655BFB20E473DE0FD377052ED89 (void);
// 0x00000278 System.Object MyCustomSceneModelLoader/<DelayedLoad>d__0::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDelayedLoadU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m87C2A2CC334B05234EA42BFCC643A870F694A4C7 (void);
// 0x00000279 System.Void MyCustomSceneModelLoader/<DelayedLoad>d__0::System.Collections.IEnumerator.Reset()
extern void U3CDelayedLoadU3Ed__0_System_Collections_IEnumerator_Reset_mD7D87180BB0F3517276BB6DB42E38C08059E5960 (void);
// 0x0000027A System.Object MyCustomSceneModelLoader/<DelayedLoad>d__0::System.Collections.IEnumerator.get_Current()
extern void U3CDelayedLoadU3Ed__0_System_Collections_IEnumerator_get_Current_mC4BFB03F1E206E7722C9A1A145A59E6E8A4D6F75 (void);
// 0x0000027B System.Void PassthroughPlayInEditor::Awake()
extern void PassthroughPlayInEditor_Awake_mC1097765DEF1E552A3E6FFC9B6F889D77BC37A81 (void);
// 0x0000027C System.Void PassthroughPlayInEditor::.ctor()
extern void PassthroughPlayInEditor__ctor_m7E47FF94310413244905059C3B3F2CB6A6BF010B (void);
// 0x0000027D System.Void RequestCaptureFlow::Start()
extern void RequestCaptureFlow_Start_m70FEF6A1472AD578CC6DDDE95F238FCDCD31ABBA (void);
// 0x0000027E System.Void RequestCaptureFlow::Update()
extern void RequestCaptureFlow_Update_m90F717772CADF56BC1E84D2C14B4A235F333CFD0 (void);
// 0x0000027F System.Void RequestCaptureFlow::.ctor()
extern void RequestCaptureFlow__ctor_mAF4C33175D2C5AD8995C83622D137F722197DC57 (void);
// 0x00000280 UnityEngine.Vector3 SimpleResizable::get_PivotPosition()
extern void SimpleResizable_get_PivotPosition_m1D01D61CDE96C726B6E081D6995B59280BEA6FBD (void);
// 0x00000281 UnityEngine.Vector3 SimpleResizable::get_NewSize()
extern void SimpleResizable_get_NewSize_m862F0F24439115ED23271B6A29EBC111F0F63131 (void);
// 0x00000282 System.Void SimpleResizable::set_NewSize(UnityEngine.Vector3)
extern void SimpleResizable_set_NewSize_m382322492871167D9E74ED5EC192BB82D02279AA (void);
// 0x00000283 UnityEngine.Vector3 SimpleResizable::get_DefaultSize()
extern void SimpleResizable_get_DefaultSize_m14C78FE6B0F7FA0F208F74AD2A2326AF9CF636BC (void);
// 0x00000284 System.Void SimpleResizable::set_DefaultSize(UnityEngine.Vector3)
extern void SimpleResizable_set_DefaultSize_m60F5422ED1DB9F50376C5EE3C9FF733AC808DA21 (void);
// 0x00000285 UnityEngine.Mesh SimpleResizable::get_Mesh()
extern void SimpleResizable_get_Mesh_mF390FA0186363590971BDE459CFE921FDDA9E02A (void);
// 0x00000286 System.Void SimpleResizable::set_Mesh(UnityEngine.Mesh)
extern void SimpleResizable_set_Mesh_m02E4372CAFDC10C62E5FE30CDB1948C45CF0237A (void);
// 0x00000287 System.Void SimpleResizable::Awake()
extern void SimpleResizable_Awake_m306B2EA96B96FF7B249EA4CAD09C1B4BBF1D9921 (void);
// 0x00000288 System.Void SimpleResizable::.ctor()
extern void SimpleResizable__ctor_m31B4992C1922806C21330594F300AEEB6CC4DFF9 (void);
// 0x00000289 System.Void SimpleResizer::CreateResizedObject(UnityEngine.Vector3,UnityEngine.GameObject,SimpleResizable)
extern void SimpleResizer_CreateResizedObject_mB110BD8FA383884EADF4F3B774CBAD1383748E90 (void);
// 0x0000028A UnityEngine.Mesh SimpleResizer::ProcessVertices(SimpleResizable,UnityEngine.Vector3)
extern void SimpleResizer_ProcessVertices_m24AE21ABC22850597CAF08DCCEB109FDEDB2D098 (void);
// 0x0000028B System.Single SimpleResizer::CalculateNewVertexPosition(SimpleResizable/Method,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
extern void SimpleResizer_CalculateNewVertexPosition_mA20E2B1065AF25EDFA560FDF38867A1400C36984 (void);
// 0x0000028C System.Void SimpleResizer::.ctor()
extern void SimpleResizer__ctor_m91352B862DDAE1F7C013209D2EBD167F380578BD (void);
// 0x0000028D System.Void Spawnable::OnBeforeSerialize()
extern void Spawnable_OnBeforeSerialize_mA9695790018650368C0B5D9F216F651600DE3A04 (void);
// 0x0000028E System.Void Spawnable::OnAfterDeserialize()
extern void Spawnable_OnAfterDeserialize_m876DF77B40363CFF3B260F14B58FDBE73B4D6CE3 (void);
// 0x0000028F System.Void Spawnable::.ctor()
extern void Spawnable__ctor_mFD124B2D210355D84C19347B3DC93650B9D4CC0E (void);
// 0x00000290 System.Int32 Spawnable::<OnAfterDeserialize>g__IndexOf|4_0(System.String,System.Collections.Generic.IEnumerable`1<System.String>)
extern void Spawnable_U3COnAfterDeserializeU3Eg__IndexOfU7C4_0_mEC6F35F276E3A9F92A32062D1BFFC0E9BF25FE06 (void);
// 0x00000291 System.Void VolumeAndPlaneSwitcher::ReplaceAnchor(OVRSceneAnchor,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Vector3)
extern void VolumeAndPlaneSwitcher_ReplaceAnchor_m0FFE091AD4D5660E2C93AAC782F9F779CC870CB1 (void);
// 0x00000292 System.Void VolumeAndPlaneSwitcher::Start()
extern void VolumeAndPlaneSwitcher_Start_m692E7D1223E10BCA05DB7598B12E733799D7A04F (void);
// 0x00000293 System.Void VolumeAndPlaneSwitcher::GetVolumeFromTopPlane(UnityEngine.Transform,UnityEngine.Vector2,System.Single,UnityEngine.Vector3&,UnityEngine.Quaternion&,UnityEngine.Vector3&)
extern void VolumeAndPlaneSwitcher_GetVolumeFromTopPlane_m7DDEDB605667BD14B19305BC4766874B96B91E17 (void);
// 0x00000294 System.Void VolumeAndPlaneSwitcher::GetTopPlaneFromVolume(UnityEngine.Transform,UnityEngine.Vector3,UnityEngine.Vector3&,UnityEngine.Quaternion&,UnityEngine.Vector3&)
extern void VolumeAndPlaneSwitcher_GetTopPlaneFromVolume_m25135ACC9448FCADA8B37EFE87E45C467C8FB945 (void);
// 0x00000295 System.Void VolumeAndPlaneSwitcher::.ctor()
extern void VolumeAndPlaneSwitcher__ctor_m664389B1CB38616F46BC0CC750A75866DAEB6B39 (void);
// 0x00000296 System.Void Anchor::Awake()
extern void Anchor_Awake_m34D125678EBEDBCD05EFBCE75AA6EB151F6BB915 (void);
// 0x00000297 System.Collections.IEnumerator Anchor::Start()
extern void Anchor_Start_mD36339D1948B883723F81FC3338331075307F6A4 (void);
// 0x00000298 System.Void Anchor::Update()
extern void Anchor_Update_mC072C2C4AB0F163B9AED43853F856C01626D0440 (void);
// 0x00000299 System.Void Anchor::OnSaveLocalButtonPressed()
extern void Anchor_OnSaveLocalButtonPressed_m2C9434A9CA569391D11E802B0682AD28B9AB5868 (void);
// 0x0000029A System.Void Anchor::OnHideButtonPressed()
extern void Anchor_OnHideButtonPressed_m4BB8148241319803AA2F43CECFBC73358D0E5DB5 (void);
// 0x0000029B System.Void Anchor::OnEraseButtonPressed()
extern void Anchor_OnEraseButtonPressed_mE13FF5BA0964D8A0538AD25E0A42920AD83F8B59 (void);
// 0x0000029C System.Void Anchor::set_ShowSaveIcon(System.Boolean)
extern void Anchor_set_ShowSaveIcon_m797042530A9E4EE738618A4C00D42F993EB1DD5E (void);
// 0x0000029D System.Void Anchor::OnHoverStart()
extern void Anchor_OnHoverStart_m437F0783984B9AFDE10118C88E5B87453B7F009F (void);
// 0x0000029E System.Void Anchor::OnHoverEnd()
extern void Anchor_OnHoverEnd_mE443FF191F33BA01B7E9FF7F683157F2AFC695BB (void);
// 0x0000029F System.Void Anchor::OnSelect()
extern void Anchor_OnSelect_m2F4DA100F34CC175BE9979E769F505B46F40F714 (void);
// 0x000002A0 System.Void Anchor::BillboardPanel(UnityEngine.Transform)
extern void Anchor_BillboardPanel_mA8E40FCBA276D7571C4A870CB2054D0CB7847645 (void);
// 0x000002A1 System.Void Anchor::HandleMenuNavigation()
extern void Anchor_HandleMenuNavigation_mF942E3629FB12B1C0E59BA71C4627B1600150CCF (void);
// 0x000002A2 System.Void Anchor::NavigateToIndexInMenu(System.Boolean)
extern void Anchor_NavigateToIndexInMenu_mB255CC0394AE2B29A4E6C4A009C15D533FD89E49 (void);
// 0x000002A3 System.Void Anchor::.ctor()
extern void Anchor__ctor_m75BC8BE83CF97E886358CF4095EF72718D3DBC71 (void);
// 0x000002A4 System.Void Anchor::<OnSaveLocalButtonPressed>b__22_0(OVRSpatialAnchor,System.Boolean)
extern void Anchor_U3COnSaveLocalButtonPressedU3Eb__22_0_mBDA39E4A533D16B7D65B1271306D7E04F121727C (void);
// 0x000002A5 System.Void Anchor::<OnEraseButtonPressed>b__24_0(OVRSpatialAnchor,System.Boolean)
extern void Anchor_U3COnEraseButtonPressedU3Eb__24_0_mC30B48B250A7854826276020EDD2FB9A0ECA949D (void);
// 0x000002A6 System.Void Anchor/<Start>d__20::.ctor(System.Int32)
extern void U3CStartU3Ed__20__ctor_mD01E4A5A8A351D8779E4DF6D534FB12D47419780 (void);
// 0x000002A7 System.Void Anchor/<Start>d__20::System.IDisposable.Dispose()
extern void U3CStartU3Ed__20_System_IDisposable_Dispose_m0DE26B36FC28513D5CB6B351B6FE0550AAF6D082 (void);
// 0x000002A8 System.Boolean Anchor/<Start>d__20::MoveNext()
extern void U3CStartU3Ed__20_MoveNext_m6C44A586391F1FFE93D205D9F7AABE8BDDFFDD86 (void);
// 0x000002A9 System.Object Anchor/<Start>d__20::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC14BAD61F2795601C2F3569E3D1FC2C9C820B307 (void);
// 0x000002AA System.Void Anchor/<Start>d__20::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__20_System_Collections_IEnumerator_Reset_mCAD6109634C4B7609B1033A6384B1158CF0EC46F (void);
// 0x000002AB System.Object Anchor/<Start>d__20::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__20_System_Collections_IEnumerator_get_Current_m5DBD7FA346AE9B7D63763850506C682A30BAA716 (void);
// 0x000002AC Anchor AnchorUIManager::get_AnchorPrefab()
extern void AnchorUIManager_get_AnchorPrefab_m2E86A98938176D1F9D12443C5BA5ED83B2B239D7 (void);
// 0x000002AD System.Void AnchorUIManager::Awake()
extern void AnchorUIManager_Awake_m6187A3F96F62BA9C4E9BF832FC6D57082BF6346F (void);
// 0x000002AE System.Void AnchorUIManager::Start()
extern void AnchorUIManager_Start_m3085EBC75FEEC38284A25172E8C3B72E6DFF9750 (void);
// 0x000002AF System.Void AnchorUIManager::Update()
extern void AnchorUIManager_Update_m5CB8F26FDCF1C51AE7142EE5FEC8013B5AA65274 (void);
// 0x000002B0 System.Void AnchorUIManager::OnCreateModeButtonPressed()
extern void AnchorUIManager_OnCreateModeButtonPressed_m79FE5CB06EC1A185CD9EF430D723706F3016EDF1 (void);
// 0x000002B1 System.Void AnchorUIManager::OnLoadAnchorsButtonPressed()
extern void AnchorUIManager_OnLoadAnchorsButtonPressed_mA02F03AA8DE96B5BDBFA790E2710C1DF0152DBA8 (void);
// 0x000002B2 System.Void AnchorUIManager::ToggleCreateMode()
extern void AnchorUIManager_ToggleCreateMode_m2050FECBD232A6297623301E672672CB674F5827 (void);
// 0x000002B3 System.Void AnchorUIManager::StartPlacementMode()
extern void AnchorUIManager_StartPlacementMode_m68824E6675A136D13ED67D1AE9F9F1697BE5E5FA (void);
// 0x000002B4 System.Void AnchorUIManager::EndPlacementMode()
extern void AnchorUIManager_EndPlacementMode_m9915D350F2E607E67AA0CDD7156C36098D857114 (void);
// 0x000002B5 System.Void AnchorUIManager::StartSelectMode()
extern void AnchorUIManager_StartSelectMode_m069C53AD8A15BB596D58397439DA48FF7440FC29 (void);
// 0x000002B6 System.Void AnchorUIManager::EndSelectMode()
extern void AnchorUIManager_EndSelectMode_m572E37B8952AFD69FC0EA0156A3512F260022043 (void);
// 0x000002B7 System.Void AnchorUIManager::HandleMenuNavigation()
extern void AnchorUIManager_HandleMenuNavigation_mF69382E1696E0E8F6AB1861BB58E30EB896976F7 (void);
// 0x000002B8 System.Void AnchorUIManager::NavigateToIndexInMenu(System.Boolean)
extern void AnchorUIManager_NavigateToIndexInMenu_m23142AEC4AE70869C98C7CD2BA2673DF51B18F74 (void);
// 0x000002B9 System.Void AnchorUIManager::ShowAnchorPreview()
extern void AnchorUIManager_ShowAnchorPreview_m492D99B5E1214BCE6B36A32A2DCD83E26FA89BB4 (void);
// 0x000002BA System.Void AnchorUIManager::HideAnchorPreview()
extern void AnchorUIManager_HideAnchorPreview_mCAC597CE861FEB2A753417E607999530DFC64DE1 (void);
// 0x000002BB System.Void AnchorUIManager::PlaceAnchor()
extern void AnchorUIManager_PlaceAnchor_mB9D837D24DC7AD460FE8137B7DF4C8AF648220ED (void);
// 0x000002BC System.Void AnchorUIManager::ShowRaycastLine()
extern void AnchorUIManager_ShowRaycastLine_mFC9DAA1E7A2A0A67CFA6B2C3223DB53968B8212E (void);
// 0x000002BD System.Void AnchorUIManager::HideRaycastLine()
extern void AnchorUIManager_HideRaycastLine_m51AEE1BCEE2627466893C02250B4430918C2F720 (void);
// 0x000002BE System.Void AnchorUIManager::ControllerRaycast()
extern void AnchorUIManager_ControllerRaycast_mB82AABC1DEA905717028D7796EF9AF81684B3C17 (void);
// 0x000002BF System.Void AnchorUIManager::HoverAnchor(Anchor)
extern void AnchorUIManager_HoverAnchor_m614467ECCFA8E9E9F7B54BE5ACE17CC84D24F1A2 (void);
// 0x000002C0 System.Void AnchorUIManager::UnhoverAnchor()
extern void AnchorUIManager_UnhoverAnchor_mC98A05A47B0DBA22AD97B1FA3679DDCF524B94D6 (void);
// 0x000002C1 System.Void AnchorUIManager::SelectAnchor()
extern void AnchorUIManager_SelectAnchor_mA6BF822A293DEF7743D8B5DD7C0E05843931E482 (void);
// 0x000002C2 System.Void AnchorUIManager::.ctor()
extern void AnchorUIManager__ctor_m88BDBD9BBB743E90038EFA5C3E5C9B351BBBF606 (void);
// 0x000002C3 System.Void AnchorUIManager/PrimaryPressDelegate::.ctor(System.Object,System.IntPtr)
extern void PrimaryPressDelegate__ctor_mDCEC312A1DF29CC56F2F61C7EF0D2FEBB49A43F5 (void);
// 0x000002C4 System.Void AnchorUIManager/PrimaryPressDelegate::Invoke()
extern void PrimaryPressDelegate_Invoke_m166299D1E905CD25DECDE5BA9CEA4CE9E9A0F55E (void);
// 0x000002C5 System.IAsyncResult AnchorUIManager/PrimaryPressDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern void PrimaryPressDelegate_BeginInvoke_m556D707A2F92E84E1CB72E5CF197780B4065B6D2 (void);
// 0x000002C6 System.Void AnchorUIManager/PrimaryPressDelegate::EndInvoke(System.IAsyncResult)
extern void PrimaryPressDelegate_EndInvoke_m8FE2F068960C1913EAE33AB0891E065DDF1F10FD (void);
// 0x000002C7 System.Void SpatialAnchorLoader::LoadAnchorsByUuid()
extern void SpatialAnchorLoader_LoadAnchorsByUuid_mAA5FF3CE2211ABEA8B8E26B0EC2EC56946EAAAEE (void);
// 0x000002C8 System.Void SpatialAnchorLoader::Awake()
extern void SpatialAnchorLoader_Awake_mD83820074A516C0DE2F825445E8F77025060064E (void);
// 0x000002C9 System.Void SpatialAnchorLoader::Load(OVRSpatialAnchor/LoadOptions)
extern void SpatialAnchorLoader_Load_mFF7DA24F1030BCE26CC86AD856AE60C9461B839B (void);
// 0x000002CA System.Void SpatialAnchorLoader::OnLocalized(OVRSpatialAnchor/UnboundAnchor,System.Boolean)
extern void SpatialAnchorLoader_OnLocalized_m2F849219D5DBB74687F926993F332CFD401D6996 (void);
// 0x000002CB System.Void SpatialAnchorLoader::Log(System.String)
extern void SpatialAnchorLoader_Log_mCC1BAB7E206826560B8659135EC2A3B45CF4BD5D (void);
// 0x000002CC System.Void SpatialAnchorLoader::.ctor()
extern void SpatialAnchorLoader__ctor_m2008E60AC4986C51481681062AD0740AE2420FCD (void);
// 0x000002CD System.Void SpatialAnchorLoader::<Load>b__4_0(OVRSpatialAnchor/UnboundAnchor[])
extern void SpatialAnchorLoader_U3CLoadU3Eb__4_0_m8A9BE73A7D0894DC4EAF29E14132863217C5220D (void);
// 0x000002CE System.Void StartMenu::Start()
extern void StartMenu_Start_m38A6B8AD8F7E61759540CF792C58672CFD13ED46 (void);
// 0x000002CF System.Void StartMenu::LoadScene(System.Int32)
extern void StartMenu_LoadScene_mF7663ABFF602CD53F3A26C076E6A8FA200347F97 (void);
// 0x000002D0 System.Void StartMenu::.ctor()
extern void StartMenu__ctor_mF07C92F5B046EB38E3933ABD69D96918F29EB952 (void);
// 0x000002D1 System.Void StartMenu/<>c__DisplayClass3_0::.ctor()
extern void U3CU3Ec__DisplayClass3_0__ctor_mB4BEA21B02CB620F151FB8C1D57C0384883E9130 (void);
// 0x000002D2 System.Void StartMenu/<>c__DisplayClass3_0::<Start>b__0()
extern void U3CU3Ec__DisplayClass3_0_U3CStartU3Eb__0_mFE41F96EFC3E7314714C2E5F6392280A788A23D2 (void);
// 0x000002D3 System.Void LocalizedHaptics::Start()
extern void LocalizedHaptics_Start_mC61E2CACF7E1098A8FD5C01522084850710B1DE8 (void);
// 0x000002D4 System.Void LocalizedHaptics::Update()
extern void LocalizedHaptics_Update_mD3E43622D8537AF0CEE5DA0B0218F10E01640095 (void);
// 0x000002D5 System.Void LocalizedHaptics::.ctor()
extern void LocalizedHaptics__ctor_m10C581A9E2F0C09D32D0DBA1EA4F072C041565AA (void);
// 0x000002D6 System.Void SceneSettings::Awake()
extern void SceneSettings_Awake_m6CE7EFC85AC6C161538C4DF007A69802C03BB2A9 (void);
// 0x000002D7 System.Void SceneSettings::Start()
extern void SceneSettings_Start_mB2B83815FED9C2D446C42B9253A147CFD0F6A4A9 (void);
// 0x000002D8 System.Void SceneSettings::CollidersSetContactOffset(System.Single)
extern void SceneSettings_CollidersSetContactOffset_mFB59467E260FC06F2775A8AF10C64DCB48FCDDB3 (void);
// 0x000002D9 System.Void SceneSettings::.ctor()
extern void SceneSettings__ctor_m2D8D57CDE722CC2D9BBAC817A40085B6964D6685 (void);
// 0x000002DA System.Void StylusTip::Awake()
extern void StylusTip_Awake_mD9B619E7308DB81EB72F434337A0551306343305 (void);
// 0x000002DB System.Void StylusTip::Update()
extern void StylusTip_Update_m78B5261BF7DDDF580FD53EB200B03B1EE3A41AE7 (void);
// 0x000002DC UnityEngine.Pose StylusTip::GetT_Device_StylusTip(OVRInput/Controller)
extern void StylusTip_GetT_Device_StylusTip_m17218314754A45C6237DDF58B8CA0F20D9E0AF59 (void);
// 0x000002DD System.Void StylusTip::.ctor()
extern void StylusTip__ctor_m3E82247B39CD1F7ABC9BBF7A0C217C6ECADCF5BE (void);
// 0x000002DE System.Void UiAxis1dInspector::SetExtents(System.Single,System.Single)
extern void UiAxis1dInspector_SetExtents_mACBB60DE84AAF9BC719AE83F4D2C6E16C76906EB (void);
// 0x000002DF System.Void UiAxis1dInspector::SetName(System.String)
extern void UiAxis1dInspector_SetName_m4A07447BFC08E009CD2F8B7AD86F167B0F5B8BE3 (void);
// 0x000002E0 System.Void UiAxis1dInspector::SetValue(System.Single)
extern void UiAxis1dInspector_SetValue_m420D37263382EBADC31665BD350763B35A57F960 (void);
// 0x000002E1 System.Void UiAxis1dInspector::.ctor()
extern void UiAxis1dInspector__ctor_mDE98644389C30FE4774268597C852F2F1E7CD6ED (void);
// 0x000002E2 System.Void UiAxis2dInspector::SetExtents(UnityEngine.Vector2,UnityEngine.Vector2)
extern void UiAxis2dInspector_SetExtents_mBACEEA893A74BEB73EA36DECEFFBA1A8E24E9E46 (void);
// 0x000002E3 System.Void UiAxis2dInspector::SetName(System.String)
extern void UiAxis2dInspector_SetName_m427BDDEA6032A8E729E8C51BF487A5E1BC20910D (void);
// 0x000002E4 System.Void UiAxis2dInspector::SetValue(System.Boolean,UnityEngine.Vector2)
extern void UiAxis2dInspector_SetValue_mC106368E30631722E4575DF3F7FF5C9424A90159 (void);
// 0x000002E5 System.Void UiAxis2dInspector::.ctor()
extern void UiAxis2dInspector__ctor_m4FEF37CE2DD5792BBBF44240230627E06AD08EA1 (void);
// 0x000002E6 System.Void UiBoolInspector::SetName(System.String)
extern void UiBoolInspector_SetName_m2A9FF57CC356247E0851F124BB8D6BFB4E3F8C81 (void);
// 0x000002E7 System.Void UiBoolInspector::SetValue(System.Boolean)
extern void UiBoolInspector_SetValue_m9C065608BC184F6E7E2F8F7668627A824EA32620 (void);
// 0x000002E8 System.Void UiBoolInspector::.ctor()
extern void UiBoolInspector__ctor_m40522F9AD5BF8A79301782BEBFA8C68DDBE3F974 (void);
// 0x000002E9 System.Void UiDeviceInspector::Start()
extern void UiDeviceInspector_Start_mE92B59858FFE0CDD557CBDC4EE75001BCCA79333 (void);
// 0x000002EA System.Void UiDeviceInspector::Update()
extern void UiDeviceInspector_Update_m2A95294AE50C81A88A6BE6235A2A768030269031 (void);
// 0x000002EB System.String UiDeviceInspector::ToDeviceModel()
extern void UiDeviceInspector_ToDeviceModel_mD4A63EADD4DBA54AFD4B8E63BC83C20CCB6CD8A0 (void);
// 0x000002EC System.String UiDeviceInspector::ToHandednessString(OVRInput/Handedness)
extern void UiDeviceInspector_ToHandednessString_mFDAECB5A6DB42D6D2577EA3CB62E66C287D4029C (void);
// 0x000002ED System.Void UiDeviceInspector::.ctor()
extern void UiDeviceInspector__ctor_mA68E9B930ECF2F480BCE82CE5B1E201413758E46 (void);
// 0x000002EE System.Void UiSceneMenu::Awake()
extern void UiSceneMenu_Awake_m24A0829C551B2616EDAC7CD9641D63308D5D47DC (void);
// 0x000002EF System.Void UiSceneMenu::Update()
extern void UiSceneMenu_Update_mE2CC16CFB12AB4B2A1A91BF8CF8BB471D46CF51A (void);
// 0x000002F0 System.Boolean UiSceneMenu::InputPrevScene()
extern void UiSceneMenu_InputPrevScene_m4478D06607F104031EF62CAC373C4060A904E899 (void);
// 0x000002F1 System.Boolean UiSceneMenu::InputNextScene()
extern void UiSceneMenu_InputNextScene_m3018EE6EFA089511D2315864B5829014632B5959 (void);
// 0x000002F2 System.Boolean UiSceneMenu::KeyboardPrevScene()
extern void UiSceneMenu_KeyboardPrevScene_mE606218757D66FFB06FBC283CF1BF3B541E62B45 (void);
// 0x000002F3 System.Boolean UiSceneMenu::KeyboardNextScene()
extern void UiSceneMenu_KeyboardNextScene_m3320B0F75EF37CA8FDCC0DB7DA9B9B94CC1A017F (void);
// 0x000002F4 System.Boolean UiSceneMenu::ThumbstickPrevScene(OVRInput/Controller)
extern void UiSceneMenu_ThumbstickPrevScene_m895D6B25D49D900C54665A74EC699283CD49F318 (void);
// 0x000002F5 System.Boolean UiSceneMenu::ThumbstickNextScene(OVRInput/Controller)
extern void UiSceneMenu_ThumbstickNextScene_m91AF192AA64631CE23F68107DBA40254CCF4B31E (void);
// 0x000002F6 UnityEngine.Vector2 UiSceneMenu::GetLastThumbstickValue(OVRInput/Controller)
extern void UiSceneMenu_GetLastThumbstickValue_mC4E4C2019D9244FFCD874B6B2E8482B38A62308F (void);
// 0x000002F7 System.Void UiSceneMenu::ChangeScene(System.Int32)
extern void UiSceneMenu_ChangeScene_mCDBE9A14D56EF5B56D360C800361F74DD0506DF6 (void);
// 0x000002F8 System.Void UiSceneMenu::CreateLabel(System.Int32,System.String)
extern void UiSceneMenu_CreateLabel_mA57587AB59F27847B7E510D4857B5DCEDCE0642C (void);
// 0x000002F9 System.Void UiSceneMenu::.ctor()
extern void UiSceneMenu__ctor_mDED3E1480A68682DDFCDA08B4C4F45E60EB060F7 (void);
// 0x000002FA System.Void UiVectorInspector::SetName(System.String)
extern void UiVectorInspector_SetName_m585A941198E2C066720393A50792773A1D19B459 (void);
// 0x000002FB System.Void UiVectorInspector::SetValue(System.Boolean)
extern void UiVectorInspector_SetValue_mBBB3DC1D5160692BD71B306C3C59F6A7DBC336B2 (void);
// 0x000002FC System.Void UiVectorInspector::.ctor()
extern void UiVectorInspector__ctor_mF45CE767D5F1589A324DAAA2716C528771BD6BAF (void);
// 0x000002FD System.Collections.IEnumerator BookTeleport::Teleport()
extern void BookTeleport_Teleport_m6E6D6A8D06455DBB24966AAD6DDAFA9FEA5838EB (void);
// 0x000002FE System.Void BookTeleport::OnTriggerEnter(UnityEngine.Collider)
extern void BookTeleport_OnTriggerEnter_m69842F096CF5D70A50162743F0E267409A516341 (void);
// 0x000002FF System.Void BookTeleport::.ctor()
extern void BookTeleport__ctor_m8EDA58152107BDFA7398CD02361FC76421759D82 (void);
// 0x00000300 System.Void BookTeleport/<Teleport>d__3::.ctor(System.Int32)
extern void U3CTeleportU3Ed__3__ctor_mC209EAD9B19B590B355A0614A8EE91BD95FE35AE (void);
// 0x00000301 System.Void BookTeleport/<Teleport>d__3::System.IDisposable.Dispose()
extern void U3CTeleportU3Ed__3_System_IDisposable_Dispose_mFBAE3F148D58D677ACBB37A53D3851F2153D93ED (void);
// 0x00000302 System.Boolean BookTeleport/<Teleport>d__3::MoveNext()
extern void U3CTeleportU3Ed__3_MoveNext_m3AD51C8419FF5D0B1EE5474754B359AADCD2511D (void);
// 0x00000303 System.Object BookTeleport/<Teleport>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CTeleportU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m263CD5B0151F6C38FCE54904EB90CEB2B4ECF32F (void);
// 0x00000304 System.Void BookTeleport/<Teleport>d__3::System.Collections.IEnumerator.Reset()
extern void U3CTeleportU3Ed__3_System_Collections_IEnumerator_Reset_m35A50F5E3AA4732116933911469DE7EA5F83AAB3 (void);
// 0x00000305 System.Object BookTeleport/<Teleport>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CTeleportU3Ed__3_System_Collections_IEnumerator_get_Current_mEAB9F5E864F436F8B6F22123A96AC31B6797D914 (void);
// 0x00000306 System.Void Test::Start()
extern void Test_Start_m4D6FA0B24EBFD6471F4596A93EC95EC1EB5355D8 (void);
// 0x00000307 System.Void Test::Update()
extern void Test_Update_m85FFE4CC559D42AAFA74D4DEAE19415052BB70D0 (void);
// 0x00000308 System.Void Test::.ctor()
extern void Test__ctor_m1C2B5E11A339FD79C0C458FB86723E5F2B74545C (void);
// 0x00000309 System.Void MGAssets.SpinWeapon::Update()
extern void SpinWeapon_Update_m6D73D6D900BEAE7002EBA691594EC13DEF9D7727 (void);
// 0x0000030A System.Void MGAssets.SpinWeapon::.ctor()
extern void SpinWeapon__ctor_mEE925257A3AFB354D5C69F7C218A7BB8462E4147 (void);
// 0x0000030B System.Boolean OculusSampleFramework.ColorGrabbable::get_Highlight()
extern void ColorGrabbable_get_Highlight_mEE9A5BC6077D6415C0F5AAEBF9268BCEDA9192CB (void);
// 0x0000030C System.Void OculusSampleFramework.ColorGrabbable::set_Highlight(System.Boolean)
extern void ColorGrabbable_set_Highlight_mF9C67E77A7CAB0FF638BDF7EE4C359530267191F (void);
// 0x0000030D System.Void OculusSampleFramework.ColorGrabbable::UpdateColor()
extern void ColorGrabbable_UpdateColor_m4D60081FC49D8BD81C4B63B35405B144232F8C60 (void);
// 0x0000030E System.Void OculusSampleFramework.ColorGrabbable::GrabBegin(OVRGrabber,UnityEngine.Collider)
extern void ColorGrabbable_GrabBegin_m756D3F58E950E5B54A1B7C4E79F2BD99AE630108 (void);
// 0x0000030F System.Void OculusSampleFramework.ColorGrabbable::GrabEnd(UnityEngine.Vector3,UnityEngine.Vector3)
extern void ColorGrabbable_GrabEnd_mCBB05FAD02AA291052245779AD795CEDA97F960F (void);
// 0x00000310 System.Void OculusSampleFramework.ColorGrabbable::Awake()
extern void ColorGrabbable_Awake_mC26EA500134341FD38388600111FF6E5668A8832 (void);
// 0x00000311 System.Void OculusSampleFramework.ColorGrabbable::SetColor(UnityEngine.Color)
extern void ColorGrabbable_SetColor_m1EA4F276B7EFDBD7EE35D598963E888307850540 (void);
// 0x00000312 System.Void OculusSampleFramework.ColorGrabbable::.ctor()
extern void ColorGrabbable__ctor_mCC1D8EDB3DB7A4DBEE8D15AC5A361A1B102EEA34 (void);
// 0x00000313 System.Void OculusSampleFramework.ColorGrabbable::.cctor()
extern void ColorGrabbable__cctor_m31CC0B60F1F9721609529F417739F468ADBB3792 (void);
// 0x00000314 System.Boolean OculusSampleFramework.DistanceGrabbable::get_InRange()
extern void DistanceGrabbable_get_InRange_m056971DE194AD0A8B85A8E8E3E82F76711FAF8EA (void);
// 0x00000315 System.Void OculusSampleFramework.DistanceGrabbable::set_InRange(System.Boolean)
extern void DistanceGrabbable_set_InRange_m0EFDE0426D5819B04621E607860A63531577586D (void);
// 0x00000316 System.Boolean OculusSampleFramework.DistanceGrabbable::get_Targeted()
extern void DistanceGrabbable_get_Targeted_m5D48533FA91BE16785B21D7881202E27DA1A8D0A (void);
// 0x00000317 System.Void OculusSampleFramework.DistanceGrabbable::set_Targeted(System.Boolean)
extern void DistanceGrabbable_set_Targeted_m9D01FAEC07F2BBD49B6DA12F1A014D2AFDBDD0E0 (void);
// 0x00000318 System.Void OculusSampleFramework.DistanceGrabbable::Start()
extern void DistanceGrabbable_Start_m0F13F152E7FCB05A41B8E0E8B6699462531AF358 (void);
// 0x00000319 System.Void OculusSampleFramework.DistanceGrabbable::RefreshCrosshair()
extern void DistanceGrabbable_RefreshCrosshair_m5461E3A1849F56BC79A9BEC50F63EE0F5C8C08A9 (void);
// 0x0000031A System.Void OculusSampleFramework.DistanceGrabbable::.ctor()
extern void DistanceGrabbable__ctor_mB69087D8AA20E7C90FAA564AC6D963C5606CDE22 (void);
// 0x0000031B System.Boolean OculusSampleFramework.DistanceGrabber::get_UseSpherecast()
extern void DistanceGrabber_get_UseSpherecast_mF25C99BCAF0C6E6CA18A4C42E4AB34BF8BFC6D5F (void);
// 0x0000031C System.Void OculusSampleFramework.DistanceGrabber::set_UseSpherecast(System.Boolean)
extern void DistanceGrabber_set_UseSpherecast_mE0BAA3127799C6328433698FA578D4E71D8AC86F (void);
// 0x0000031D System.Void OculusSampleFramework.DistanceGrabber::Start()
extern void DistanceGrabber_Start_m775855E7B9023538B7E56155A6597519A953ED70 (void);
// 0x0000031E System.Void OculusSampleFramework.DistanceGrabber::Update()
extern void DistanceGrabber_Update_m960BB0DB05A12AE7386926698E0153ED5BEAEAEE (void);
// 0x0000031F System.Void OculusSampleFramework.DistanceGrabber::GrabBegin()
extern void DistanceGrabber_GrabBegin_m77865480609755B1282A5A546DC6A53FA63B49CF (void);
// 0x00000320 System.Void OculusSampleFramework.DistanceGrabber::MoveGrabbedObject(UnityEngine.Vector3,UnityEngine.Quaternion,System.Boolean)
extern void DistanceGrabber_MoveGrabbedObject_m6B815535F8AAC860485491CCA49CD8046802F0A9 (void);
// 0x00000321 OculusSampleFramework.DistanceGrabbable OculusSampleFramework.DistanceGrabber::HitInfoToGrabbable(UnityEngine.RaycastHit)
extern void DistanceGrabber_HitInfoToGrabbable_m53F7D8C62778137165E37417B4AB3C087A485804 (void);
// 0x00000322 System.Boolean OculusSampleFramework.DistanceGrabber::FindTarget(OculusSampleFramework.DistanceGrabbable&,UnityEngine.Collider&)
extern void DistanceGrabber_FindTarget_m1F30C7639B39EEB998BC94B7AD6483A235F162EC (void);
// 0x00000323 System.Boolean OculusSampleFramework.DistanceGrabber::FindTargetWithSpherecast(OculusSampleFramework.DistanceGrabbable&,UnityEngine.Collider&)
extern void DistanceGrabber_FindTargetWithSpherecast_m561C09222DCCD4C52DF037E1AFD95D6EEFF3645D (void);
// 0x00000324 System.Void OculusSampleFramework.DistanceGrabber::GrabVolumeEnable(System.Boolean)
extern void DistanceGrabber_GrabVolumeEnable_m5AE9B41CBEB5A5EBC1D2F7945B875794FD93A405 (void);
// 0x00000325 System.Void OculusSampleFramework.DistanceGrabber::OffhandGrabbed(OVRGrabbable)
extern void DistanceGrabber_OffhandGrabbed_m31A56380ABE7DB5847799670467539468AF085DB (void);
// 0x00000326 System.Void OculusSampleFramework.DistanceGrabber::.ctor()
extern void DistanceGrabber__ctor_m80196B68D9FB9CDE30E2CEC5690F4BB97740A366 (void);
// 0x00000327 System.Void OculusSampleFramework.GrabManager::OnTriggerEnter(UnityEngine.Collider)
extern void GrabManager_OnTriggerEnter_m6EF4F094BEAE1A7A31854F98826B3FBB1B74E91F (void);
// 0x00000328 System.Void OculusSampleFramework.GrabManager::OnTriggerExit(UnityEngine.Collider)
extern void GrabManager_OnTriggerExit_mC52984A8FECA9E7098E2615B0DAFEFAEA92997F7 (void);
// 0x00000329 System.Void OculusSampleFramework.GrabManager::.ctor()
extern void GrabManager__ctor_m5A60AC8D6E026A5A4FDAB55DAC7CEDCDD2913376 (void);
// 0x0000032A System.Void OculusSampleFramework.GrabbableCrosshair::Start()
extern void GrabbableCrosshair_Start_mFE636535DBC51AC58F63DDB687E6D1875D63FD97 (void);
// 0x0000032B System.Void OculusSampleFramework.GrabbableCrosshair::SetState(OculusSampleFramework.GrabbableCrosshair/CrosshairState)
extern void GrabbableCrosshair_SetState_m5D2604BA1418ED7C18E0CFB823CAA172EA1B23C6 (void);
// 0x0000032C System.Void OculusSampleFramework.GrabbableCrosshair::Update()
extern void GrabbableCrosshair_Update_m9A0A89691DDE8B292D735158EB3C6EDC852DC979 (void);
// 0x0000032D System.Void OculusSampleFramework.GrabbableCrosshair::.ctor()
extern void GrabbableCrosshair__ctor_m59EFA7290C1E0CFC102C201C1303868B7AE1ED0D (void);
// 0x0000032E System.Void OculusSampleFramework.PauseOnInputLoss::Start()
extern void PauseOnInputLoss_Start_m309D5D446C65347D01220D6965E848AE62DB809D (void);
// 0x0000032F System.Void OculusSampleFramework.PauseOnInputLoss::OnInputFocusLost()
extern void PauseOnInputLoss_OnInputFocusLost_mAC19B6B6F2BD7435BD93C83448F67F22A86ECEB2 (void);
// 0x00000330 System.Void OculusSampleFramework.PauseOnInputLoss::OnInputFocusAcquired()
extern void PauseOnInputLoss_OnInputFocusAcquired_m21D3CFB7EF526E324CBF8F43F6B2102EACDEC057 (void);
// 0x00000331 System.Void OculusSampleFramework.PauseOnInputLoss::.ctor()
extern void PauseOnInputLoss__ctor_m0648631C18A65BFA8306CBD7C10288D6CB81EC25 (void);
// 0x00000332 System.Void OculusSampleFramework.BoneCapsuleTriggerLogic::OnDisable()
extern void BoneCapsuleTriggerLogic_OnDisable_m78D90F38B9238A03909E14C79AC447972BE23702 (void);
// 0x00000333 System.Void OculusSampleFramework.BoneCapsuleTriggerLogic::Update()
extern void BoneCapsuleTriggerLogic_Update_mA3BC03489B0871D073AD4CF06850AD26768D4A24 (void);
// 0x00000334 System.Void OculusSampleFramework.BoneCapsuleTriggerLogic::OnTriggerEnter(UnityEngine.Collider)
extern void BoneCapsuleTriggerLogic_OnTriggerEnter_m711C97FA1E316AB56D8084519BE087A638497C39 (void);
// 0x00000335 System.Void OculusSampleFramework.BoneCapsuleTriggerLogic::OnTriggerExit(UnityEngine.Collider)
extern void BoneCapsuleTriggerLogic_OnTriggerExit_m5E0F1A427B4A13D901592A0CA13FDC839C6C7C65 (void);
// 0x00000336 System.Void OculusSampleFramework.BoneCapsuleTriggerLogic::CleanUpDeadColliders()
extern void BoneCapsuleTriggerLogic_CleanUpDeadColliders_m93077C24AD19895958A85B39B597DDAE5EFC90B1 (void);
// 0x00000337 System.Void OculusSampleFramework.BoneCapsuleTriggerLogic::.ctor()
extern void BoneCapsuleTriggerLogic__ctor_m11A006B3E2E5D97ACC8F7908F57598EB8A7FADBD (void);
// 0x00000338 System.Int32 OculusSampleFramework.ButtonController::get_ValidToolTagsMask()
extern void ButtonController_get_ValidToolTagsMask_mC1A23D42383354957AC3ED2EE4E0E443E4BD7EE8 (void);
// 0x00000339 UnityEngine.Vector3 OculusSampleFramework.ButtonController::get_LocalButtonDirection()
extern void ButtonController_get_LocalButtonDirection_mCD9B2BA21A5208ACF45B9DE2348951CED9A2F597 (void);
// 0x0000033A OculusSampleFramework.InteractableState OculusSampleFramework.ButtonController::get_CurrentButtonState()
extern void ButtonController_get_CurrentButtonState_m97D3281E1AA62F0B28B9FC13D5C6D97E8898374A (void);
// 0x0000033B System.Void OculusSampleFramework.ButtonController::set_CurrentButtonState(OculusSampleFramework.InteractableState)
extern void ButtonController_set_CurrentButtonState_mDDD60059979F7FCE78D320A3D034ED7550F076C1 (void);
// 0x0000033C System.Void OculusSampleFramework.ButtonController::Awake()
extern void ButtonController_Awake_m9A642360ACD027C8D3C3B39598D5D7483AB9D199 (void);
// 0x0000033D System.Void OculusSampleFramework.ButtonController::FireInteractionEventsOnDepth(OculusSampleFramework.InteractableCollisionDepth,OculusSampleFramework.InteractableTool,OculusSampleFramework.InteractionType)
extern void ButtonController_FireInteractionEventsOnDepth_m8532030AB88C8754A41BA83EFE29ED352DE838C9 (void);
// 0x0000033E System.Void OculusSampleFramework.ButtonController::UpdateCollisionDepth(OculusSampleFramework.InteractableTool,OculusSampleFramework.InteractableCollisionDepth,OculusSampleFramework.InteractableCollisionDepth)
extern void ButtonController_UpdateCollisionDepth_mD5E62BC8315DE7342BBD20473AEE3FDD65788A3D (void);
// 0x0000033F OculusSampleFramework.InteractableState OculusSampleFramework.ButtonController::GetUpcomingStateNearField(OculusSampleFramework.InteractableState,OculusSampleFramework.InteractableCollisionDepth,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.Boolean)
extern void ButtonController_GetUpcomingStateNearField_m4377557E72D77C2AA2D3FE1DEAE6F5067F54D12E (void);
// 0x00000340 System.Void OculusSampleFramework.ButtonController::ForceResetButton()
extern void ButtonController_ForceResetButton_m82BCDD2D684499C11C329CB5AD25C9DAD23AEE9E (void);
// 0x00000341 System.Boolean OculusSampleFramework.ButtonController::IsValidContact(OculusSampleFramework.InteractableTool,UnityEngine.Vector3)
extern void ButtonController_IsValidContact_mF9BEA01F40D5CF760D0D26E15603D51B0BE2C59E (void);
// 0x00000342 System.Boolean OculusSampleFramework.ButtonController::PassEntryTest(OculusSampleFramework.InteractableTool,UnityEngine.Vector3)
extern void ButtonController_PassEntryTest_m1CC5A6460FD676ADC06C3D8C003D3E90784028B0 (void);
// 0x00000343 System.Boolean OculusSampleFramework.ButtonController::PassPerpTest(OculusSampleFramework.InteractableTool,UnityEngine.Vector3)
extern void ButtonController_PassPerpTest_mCCEFC8E20AAF9FBE58E5BEDCE554CEF048EC6196 (void);
// 0x00000344 System.Void OculusSampleFramework.ButtonController::.ctor()
extern void ButtonController__ctor_m64221E61350B4251820CAA250163801C4AAC1781 (void);
// 0x00000345 UnityEngine.Collider OculusSampleFramework.ButtonTriggerZone::get_Collider()
extern void ButtonTriggerZone_get_Collider_mA97511B633C8FA23B1E1B864FE94DF7771424CCA (void);
// 0x00000346 System.Void OculusSampleFramework.ButtonTriggerZone::set_Collider(UnityEngine.Collider)
extern void ButtonTriggerZone_set_Collider_mB7AE7E1CA02A2EB268DD49032D0F33472F443AB5 (void);
// 0x00000347 OculusSampleFramework.Interactable OculusSampleFramework.ButtonTriggerZone::get_ParentInteractable()
extern void ButtonTriggerZone_get_ParentInteractable_m14EDBF5B2E361C7B71513E0FD4D10EA87D388693 (void);
// 0x00000348 System.Void OculusSampleFramework.ButtonTriggerZone::set_ParentInteractable(OculusSampleFramework.Interactable)
extern void ButtonTriggerZone_set_ParentInteractable_m6BF1259D2ED0DF4B565C08D00C2A867074C03A53 (void);
// 0x00000349 OculusSampleFramework.InteractableCollisionDepth OculusSampleFramework.ButtonTriggerZone::get_CollisionDepth()
extern void ButtonTriggerZone_get_CollisionDepth_m066AACBCB480A4D05E1F9910AAB71F3EB4F90558 (void);
// 0x0000034A System.Void OculusSampleFramework.ButtonTriggerZone::Awake()
extern void ButtonTriggerZone_Awake_mAEDDFC443C2337ECC22046203921081412A8253C (void);
// 0x0000034B System.Void OculusSampleFramework.ButtonTriggerZone::.ctor()
extern void ButtonTriggerZone__ctor_mE84EA0EF8CC850618DF29DCF722071F662F46D35 (void);
// 0x0000034C UnityEngine.Collider OculusSampleFramework.ColliderZone::get_Collider()
// 0x0000034D OculusSampleFramework.Interactable OculusSampleFramework.ColliderZone::get_ParentInteractable()
// 0x0000034E OculusSampleFramework.InteractableCollisionDepth OculusSampleFramework.ColliderZone::get_CollisionDepth()
// 0x0000034F System.Void OculusSampleFramework.ColliderZoneArgs::.ctor(OculusSampleFramework.ColliderZone,System.Single,OculusSampleFramework.InteractableTool,OculusSampleFramework.InteractionType)
extern void ColliderZoneArgs__ctor_m2F17926EE45B385F3291ABB057D0DB1C46E7C721 (void);
// 0x00000350 OVRHand OculusSampleFramework.HandsManager::get_RightHand()
extern void HandsManager_get_RightHand_mE9F238C463B7E656776BAD73EFE3EF3EDF4479B3 (void);
// 0x00000351 System.Void OculusSampleFramework.HandsManager::set_RightHand(OVRHand)
extern void HandsManager_set_RightHand_m007F2D3739DF6176DA4F7E496C36EEB66A2437B6 (void);
// 0x00000352 OVRSkeleton OculusSampleFramework.HandsManager::get_RightHandSkeleton()
extern void HandsManager_get_RightHandSkeleton_mBCF403959F6ABB1D38A64EDFB2EF5410A71BA9D8 (void);
// 0x00000353 System.Void OculusSampleFramework.HandsManager::set_RightHandSkeleton(OVRSkeleton)
extern void HandsManager_set_RightHandSkeleton_m9F1BE77E728E2B0ADE142380CA4EA62589754159 (void);
// 0x00000354 OVRSkeletonRenderer OculusSampleFramework.HandsManager::get_RightHandSkeletonRenderer()
extern void HandsManager_get_RightHandSkeletonRenderer_mEBEE32D6AB8B63D8DA808FDBB8FC20F6E066D28C (void);
// 0x00000355 System.Void OculusSampleFramework.HandsManager::set_RightHandSkeletonRenderer(OVRSkeletonRenderer)
extern void HandsManager_set_RightHandSkeletonRenderer_mE330C23C803564C20D05F29CCA79AA575E32D3F1 (void);
// 0x00000356 OVRMesh OculusSampleFramework.HandsManager::get_RightHandMesh()
extern void HandsManager_get_RightHandMesh_mA1B64CFD74E89E1F8A3A59C040AC9AA732837C6D (void);
// 0x00000357 System.Void OculusSampleFramework.HandsManager::set_RightHandMesh(OVRMesh)
extern void HandsManager_set_RightHandMesh_m8B308743A2F158A112E5C0EBB6A54A1D19451DBD (void);
// 0x00000358 OVRMeshRenderer OculusSampleFramework.HandsManager::get_RightHandMeshRenderer()
extern void HandsManager_get_RightHandMeshRenderer_mE2D447583D12CB6E51360CD78BD25B85C33D613E (void);
// 0x00000359 System.Void OculusSampleFramework.HandsManager::set_RightHandMeshRenderer(OVRMeshRenderer)
extern void HandsManager_set_RightHandMeshRenderer_mD2468B3A5B1C6EA97F07D6314DA50969C600380E (void);
// 0x0000035A OVRHand OculusSampleFramework.HandsManager::get_LeftHand()
extern void HandsManager_get_LeftHand_m53807ABE4939BD2D9260E268C6674B7011713BA8 (void);
// 0x0000035B System.Void OculusSampleFramework.HandsManager::set_LeftHand(OVRHand)
extern void HandsManager_set_LeftHand_m32768139F21ADDBCD87E2833D2BB30DAE5B59F4F (void);
// 0x0000035C OVRSkeleton OculusSampleFramework.HandsManager::get_LeftHandSkeleton()
extern void HandsManager_get_LeftHandSkeleton_m44A310FFC70C3CB410D2DC2F235F57B1B58FF2D2 (void);
// 0x0000035D System.Void OculusSampleFramework.HandsManager::set_LeftHandSkeleton(OVRSkeleton)
extern void HandsManager_set_LeftHandSkeleton_mC76A55A9DCF670FC60B34CC92DD81027E7735B08 (void);
// 0x0000035E OVRSkeletonRenderer OculusSampleFramework.HandsManager::get_LeftHandSkeletonRenderer()
extern void HandsManager_get_LeftHandSkeletonRenderer_m8FD29F034B1EC81684F4C74A8301596395011B48 (void);
// 0x0000035F System.Void OculusSampleFramework.HandsManager::set_LeftHandSkeletonRenderer(OVRSkeletonRenderer)
extern void HandsManager_set_LeftHandSkeletonRenderer_m1C5A2A642C6614C040085355D8214E16D934D28D (void);
// 0x00000360 OVRMesh OculusSampleFramework.HandsManager::get_LeftHandMesh()
extern void HandsManager_get_LeftHandMesh_m16A9E535B1B520F309D0886704A3201648B16CDD (void);
// 0x00000361 System.Void OculusSampleFramework.HandsManager::set_LeftHandMesh(OVRMesh)
extern void HandsManager_set_LeftHandMesh_m56E8B4E036DA7AACB922228B4FE27FCC272278E3 (void);
// 0x00000362 OVRMeshRenderer OculusSampleFramework.HandsManager::get_LeftHandMeshRenderer()
extern void HandsManager_get_LeftHandMeshRenderer_mDE3580A395755759374279284C9650D59D09CFF2 (void);
// 0x00000363 System.Void OculusSampleFramework.HandsManager::set_LeftHandMeshRenderer(OVRMeshRenderer)
extern void HandsManager_set_LeftHandMeshRenderer_m102CF222EBA2577F468099C726325DF3B779FF64 (void);
// 0x00000364 OculusSampleFramework.HandsManager OculusSampleFramework.HandsManager::get_Instance()
extern void HandsManager_get_Instance_m9F9D019322154186D82B64FBDBE1A1EC2867778E (void);
// 0x00000365 System.Void OculusSampleFramework.HandsManager::set_Instance(OculusSampleFramework.HandsManager)
extern void HandsManager_set_Instance_mE462D6A4BED5922361C942E0630FDF3421AB3833 (void);
// 0x00000366 System.Void OculusSampleFramework.HandsManager::Awake()
extern void HandsManager_Awake_m7D236135CCB8A2F47F442B2EE89281B985ABA02A (void);
// 0x00000367 System.Void OculusSampleFramework.HandsManager::Update()
extern void HandsManager_Update_m3AAFA9F8B8B994326B7695807BA0D38FF592C2A3 (void);
// 0x00000368 System.Collections.IEnumerator OculusSampleFramework.HandsManager::FindSkeletonVisualGameObjects()
extern void HandsManager_FindSkeletonVisualGameObjects_m31E51747D167E74132150423190EC4AAC13B1E53 (void);
// 0x00000369 System.Void OculusSampleFramework.HandsManager::SwitchVisualization()
extern void HandsManager_SwitchVisualization_mFF5FEEE41ACCB620EC9E98BA8364A343CE7F5584 (void);
// 0x0000036A System.Void OculusSampleFramework.HandsManager::SetToCurrentVisualMode()
extern void HandsManager_SetToCurrentVisualMode_m183D5AEF5523A39C0054A34E304FE88E25E9E277 (void);
// 0x0000036B System.Collections.Generic.List`1<OVRBoneCapsule> OculusSampleFramework.HandsManager::GetCapsulesPerBone(OVRSkeleton,OVRSkeleton/BoneId)
extern void HandsManager_GetCapsulesPerBone_mC638435584A77EDC7E947B35A14242EA66BB9B61 (void);
// 0x0000036C System.Boolean OculusSampleFramework.HandsManager::IsInitialized()
extern void HandsManager_IsInitialized_m607714995C91CC61C6672A91A631EAC7FA934917 (void);
// 0x0000036D System.Void OculusSampleFramework.HandsManager::.ctor()
extern void HandsManager__ctor_mF764E9D2BE5BB64D933E7B6925E0F549EA1341C6 (void);
// 0x0000036E System.Void OculusSampleFramework.HandsManager/<FindSkeletonVisualGameObjects>d__52::.ctor(System.Int32)
extern void U3CFindSkeletonVisualGameObjectsU3Ed__52__ctor_mB55CA2BF6CE22AC93FB1D1E0324FA76F2895B6BB (void);
// 0x0000036F System.Void OculusSampleFramework.HandsManager/<FindSkeletonVisualGameObjects>d__52::System.IDisposable.Dispose()
extern void U3CFindSkeletonVisualGameObjectsU3Ed__52_System_IDisposable_Dispose_mBB9AC6B3987FBE05D17229FC65772D6AEF1AE5C5 (void);
// 0x00000370 System.Boolean OculusSampleFramework.HandsManager/<FindSkeletonVisualGameObjects>d__52::MoveNext()
extern void U3CFindSkeletonVisualGameObjectsU3Ed__52_MoveNext_mDB2E7404E01F1E9764DFD7C7E69BE594B4130CFA (void);
// 0x00000371 System.Object OculusSampleFramework.HandsManager/<FindSkeletonVisualGameObjects>d__52::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CFindSkeletonVisualGameObjectsU3Ed__52_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m53507F454464337DB141FA4B5B17D2ABBD2C0FE4 (void);
// 0x00000372 System.Void OculusSampleFramework.HandsManager/<FindSkeletonVisualGameObjects>d__52::System.Collections.IEnumerator.Reset()
extern void U3CFindSkeletonVisualGameObjectsU3Ed__52_System_Collections_IEnumerator_Reset_m38AAC961B881CFB50A5DD5D622E430A3F56E6416 (void);
// 0x00000373 System.Object OculusSampleFramework.HandsManager/<FindSkeletonVisualGameObjects>d__52::System.Collections.IEnumerator.get_Current()
extern void U3CFindSkeletonVisualGameObjectsU3Ed__52_System_Collections_IEnumerator_get_Current_m1534C6E698DEA0AE22B6DB0F7352E4815D614CD3 (void);
// 0x00000374 OculusSampleFramework.ColliderZone OculusSampleFramework.Interactable::get_ProximityCollider()
extern void Interactable_get_ProximityCollider_mF6F604B5AC84CDF8FDA76502CF97731CC056FF08 (void);
// 0x00000375 OculusSampleFramework.ColliderZone OculusSampleFramework.Interactable::get_ContactCollider()
extern void Interactable_get_ContactCollider_m4735130C7B2B2FE783C186EBC1A482C7E29D3873 (void);
// 0x00000376 OculusSampleFramework.ColliderZone OculusSampleFramework.Interactable::get_ActionCollider()
extern void Interactable_get_ActionCollider_mBAF9B046536B533C7BA2226926BBC50E17B6726D (void);
// 0x00000377 System.Int32 OculusSampleFramework.Interactable::get_ValidToolTagsMask()
extern void Interactable_get_ValidToolTagsMask_m85DABBD0A6116F184E84F198CAC92C26121F6DF6 (void);
// 0x00000378 System.Void OculusSampleFramework.Interactable::add_ProximityZoneEvent(System.Action`1<OculusSampleFramework.ColliderZoneArgs>)
extern void Interactable_add_ProximityZoneEvent_m2EFA9B8880CE03E14E54AAF2282D089F2E4B3FA0 (void);
// 0x00000379 System.Void OculusSampleFramework.Interactable::remove_ProximityZoneEvent(System.Action`1<OculusSampleFramework.ColliderZoneArgs>)
extern void Interactable_remove_ProximityZoneEvent_m314D6AC5B0CAC5A5FF915B383F9C4E24A44C0183 (void);
// 0x0000037A System.Void OculusSampleFramework.Interactable::OnProximityZoneEvent(OculusSampleFramework.ColliderZoneArgs)
extern void Interactable_OnProximityZoneEvent_mDED9C800E85CF54A06FB3CCA1DB2EF51ECF15A1F (void);
// 0x0000037B System.Void OculusSampleFramework.Interactable::add_ContactZoneEvent(System.Action`1<OculusSampleFramework.ColliderZoneArgs>)
extern void Interactable_add_ContactZoneEvent_m284AEAF4458BB62779A75C591F261091518B3481 (void);
// 0x0000037C System.Void OculusSampleFramework.Interactable::remove_ContactZoneEvent(System.Action`1<OculusSampleFramework.ColliderZoneArgs>)
extern void Interactable_remove_ContactZoneEvent_m96A59B1DA482F032CF5D490918466CF68A1A5D02 (void);
// 0x0000037D System.Void OculusSampleFramework.Interactable::OnContactZoneEvent(OculusSampleFramework.ColliderZoneArgs)
extern void Interactable_OnContactZoneEvent_mF346FBAB8E4F53A26B148E8126759D64A70B5E73 (void);
// 0x0000037E System.Void OculusSampleFramework.Interactable::add_ActionZoneEvent(System.Action`1<OculusSampleFramework.ColliderZoneArgs>)
extern void Interactable_add_ActionZoneEvent_m303D57C7749AD0BE641D5031D90F4A1053B9B438 (void);
// 0x0000037F System.Void OculusSampleFramework.Interactable::remove_ActionZoneEvent(System.Action`1<OculusSampleFramework.ColliderZoneArgs>)
extern void Interactable_remove_ActionZoneEvent_m4E8476203B9F028436EBD7F9E29DD629BE44FF2D (void);
// 0x00000380 System.Void OculusSampleFramework.Interactable::OnActionZoneEvent(OculusSampleFramework.ColliderZoneArgs)
extern void Interactable_OnActionZoneEvent_mA4413FFF1DD8593420D456C9A8EEA3487D1CB9DE (void);
// 0x00000381 System.Void OculusSampleFramework.Interactable::UpdateCollisionDepth(OculusSampleFramework.InteractableTool,OculusSampleFramework.InteractableCollisionDepth,OculusSampleFramework.InteractableCollisionDepth)
// 0x00000382 System.Void OculusSampleFramework.Interactable::Awake()
extern void Interactable_Awake_m673622B78CF87568C609EDD039F0D947CF4E9F32 (void);
// 0x00000383 System.Void OculusSampleFramework.Interactable::OnDestroy()
extern void Interactable_OnDestroy_m24FA62B42CADA4737F199EFB99C057CCD0ADFB94 (void);
// 0x00000384 System.Void OculusSampleFramework.Interactable::.ctor()
extern void Interactable__ctor_m38A75FA2BE69E84CE6E111E03319FA303714B4D6 (void);
// 0x00000385 System.Void OculusSampleFramework.Interactable/InteractableStateArgsEvent::.ctor()
extern void InteractableStateArgsEvent__ctor_mD35D9AB2D5844DC8C2246F8AD4EA36A0B09308DE (void);
// 0x00000386 System.Void OculusSampleFramework.InteractableStateArgs::.ctor(OculusSampleFramework.Interactable,OculusSampleFramework.InteractableTool,OculusSampleFramework.InteractableState,OculusSampleFramework.InteractableState,OculusSampleFramework.ColliderZoneArgs)
extern void InteractableStateArgs__ctor_m3C317F0382FE5F267D662DDCFABE336F06862624 (void);
// 0x00000387 System.Collections.Generic.HashSet`1<OculusSampleFramework.Interactable> OculusSampleFramework.InteractableRegistry::get_Interactables()
extern void InteractableRegistry_get_Interactables_mB661BE7ACD52E8D6C6F6F31CCF66761C0D6D152A (void);
// 0x00000388 System.Void OculusSampleFramework.InteractableRegistry::RegisterInteractable(OculusSampleFramework.Interactable)
extern void InteractableRegistry_RegisterInteractable_m6D6FC17C1D0D9E571387ABBB57B1884E30799B30 (void);
// 0x00000389 System.Void OculusSampleFramework.InteractableRegistry::UnregisterInteractable(OculusSampleFramework.Interactable)
extern void InteractableRegistry_UnregisterInteractable_mC1B0A16013D0A1FDA4F96E0832476986A257CA11 (void);
// 0x0000038A System.Void OculusSampleFramework.InteractableRegistry::.ctor()
extern void InteractableRegistry__ctor_mF51128E4BD939E58FF4E97F8F43DF49290548B80 (void);
// 0x0000038B System.Void OculusSampleFramework.InteractableRegistry::.cctor()
extern void InteractableRegistry__cctor_mB8D0E4DE4F59BCE89971E31C7C4B9FD1ABC1C30E (void);
// 0x0000038C System.Void OculusSampleFramework.InteractableToolsCreator::Awake()
extern void InteractableToolsCreator_Awake_m52A416198A9987B601BFB7B4DA2D4F81763787D5 (void);
// 0x0000038D System.Collections.IEnumerator OculusSampleFramework.InteractableToolsCreator::AttachToolsToHands(UnityEngine.Transform[],System.Boolean)
extern void InteractableToolsCreator_AttachToolsToHands_m494AA7A9F37A70A890E0E386D44474483AAB9169 (void);
// 0x0000038E System.Void OculusSampleFramework.InteractableToolsCreator::AttachToolToHandTransform(UnityEngine.Transform,System.Boolean)
extern void InteractableToolsCreator_AttachToolToHandTransform_m76F801FCEFADB36E154C683F39DAC5A38CD6F1D5 (void);
// 0x0000038F System.Void OculusSampleFramework.InteractableToolsCreator::.ctor()
extern void InteractableToolsCreator__ctor_m3B28F7ECE7DC21C19A7F090418D93B4A90D279A4 (void);
// 0x00000390 System.Void OculusSampleFramework.InteractableToolsCreator/<AttachToolsToHands>d__3::.ctor(System.Int32)
extern void U3CAttachToolsToHandsU3Ed__3__ctor_m734EDB11729722D3E2FF780175D03056A3FB6B59 (void);
// 0x00000391 System.Void OculusSampleFramework.InteractableToolsCreator/<AttachToolsToHands>d__3::System.IDisposable.Dispose()
extern void U3CAttachToolsToHandsU3Ed__3_System_IDisposable_Dispose_m465E039D390579A8A71EDEA3E92B1B21759BBFF9 (void);
// 0x00000392 System.Boolean OculusSampleFramework.InteractableToolsCreator/<AttachToolsToHands>d__3::MoveNext()
extern void U3CAttachToolsToHandsU3Ed__3_MoveNext_mED0C488063607CCD70041DB0782687049723B1E0 (void);
// 0x00000393 System.Void OculusSampleFramework.InteractableToolsCreator/<AttachToolsToHands>d__3::<>m__Finally1()
extern void U3CAttachToolsToHandsU3Ed__3_U3CU3Em__Finally1_m8A2BD809ED53D8BAB20DB018A676FEC81B1C6206 (void);
// 0x00000394 System.Object OculusSampleFramework.InteractableToolsCreator/<AttachToolsToHands>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAttachToolsToHandsU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9C9D3D39C28404672F886B0E4DF9571ADEFD737D (void);
// 0x00000395 System.Void OculusSampleFramework.InteractableToolsCreator/<AttachToolsToHands>d__3::System.Collections.IEnumerator.Reset()
extern void U3CAttachToolsToHandsU3Ed__3_System_Collections_IEnumerator_Reset_mF4B96290820469D2C97EEF77C56725A051EE2A5B (void);
// 0x00000396 System.Object OculusSampleFramework.InteractableToolsCreator/<AttachToolsToHands>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CAttachToolsToHandsU3Ed__3_System_Collections_IEnumerator_get_Current_m1BC783E9E2AEAC011D84DDF68154B2A50B83686F (void);
// 0x00000397 OculusSampleFramework.InteractableToolsInputRouter OculusSampleFramework.InteractableToolsInputRouter::get_Instance()
extern void InteractableToolsInputRouter_get_Instance_mF54ED5593C45AF98D05F43DCCF7F5B073115209B (void);
// 0x00000398 System.Void OculusSampleFramework.InteractableToolsInputRouter::RegisterInteractableTool(OculusSampleFramework.InteractableTool)
extern void InteractableToolsInputRouter_RegisterInteractableTool_m0427DDDC21A231B5E405F2D2B2C91470C99F0656 (void);
// 0x00000399 System.Void OculusSampleFramework.InteractableToolsInputRouter::UnregisterInteractableTool(OculusSampleFramework.InteractableTool)
extern void InteractableToolsInputRouter_UnregisterInteractableTool_m10F7F03FBF73F6D75A7D9FB96564C26190BCF96A (void);
// 0x0000039A System.Void OculusSampleFramework.InteractableToolsInputRouter::Update()
extern void InteractableToolsInputRouter_Update_mFFABA2B1A23E61F20573C7589FF38352FBF5B09B (void);
// 0x0000039B System.Boolean OculusSampleFramework.InteractableToolsInputRouter::UpdateToolsAndEnableState(System.Collections.Generic.HashSet`1<OculusSampleFramework.InteractableTool>,System.Boolean)
extern void InteractableToolsInputRouter_UpdateToolsAndEnableState_mDC4F03CF1C73759E460B7C55D69DF2953B928516 (void);
// 0x0000039C System.Boolean OculusSampleFramework.InteractableToolsInputRouter::UpdateTools(System.Collections.Generic.HashSet`1<OculusSampleFramework.InteractableTool>,System.Boolean)
extern void InteractableToolsInputRouter_UpdateTools_m497363491848B42184F5CD40662A510E5236FE5F (void);
// 0x0000039D System.Void OculusSampleFramework.InteractableToolsInputRouter::ToggleToolsEnableState(System.Collections.Generic.HashSet`1<OculusSampleFramework.InteractableTool>,System.Boolean)
extern void InteractableToolsInputRouter_ToggleToolsEnableState_mCB03C3A19E2CF7287E61B7BD684523AF54DECAAF (void);
// 0x0000039E System.Void OculusSampleFramework.InteractableToolsInputRouter::.ctor()
extern void InteractableToolsInputRouter__ctor_m1F12838905D93E459F495D3417F16A3347BC7359 (void);
// 0x0000039F OculusSampleFramework.InteractableToolTags OculusSampleFramework.FingerTipPokeTool::get_ToolTags()
extern void FingerTipPokeTool_get_ToolTags_mBD6F55F53B1190B9D1A07EE70C58B117C35DC144 (void);
// 0x000003A0 OculusSampleFramework.ToolInputState OculusSampleFramework.FingerTipPokeTool::get_ToolInputState()
extern void FingerTipPokeTool_get_ToolInputState_m5135949E46396699EFB438DCEF083077CA58A42D (void);
// 0x000003A1 System.Boolean OculusSampleFramework.FingerTipPokeTool::get_IsFarFieldTool()
extern void FingerTipPokeTool_get_IsFarFieldTool_m19F2F230707836E77DACA32EA3295BB3836E38FE (void);
// 0x000003A2 System.Boolean OculusSampleFramework.FingerTipPokeTool::get_EnableState()
extern void FingerTipPokeTool_get_EnableState_mAFAC89BBE30FA63FD0361CB2C73F546A6421A81A (void);
// 0x000003A3 System.Void OculusSampleFramework.FingerTipPokeTool::set_EnableState(System.Boolean)
extern void FingerTipPokeTool_set_EnableState_m53263FC9F17DBD554E665FE970FFFB8660840772 (void);
// 0x000003A4 System.Void OculusSampleFramework.FingerTipPokeTool::Initialize()
extern void FingerTipPokeTool_Initialize_mF6EAF229A254AAA628C55FD25DFC45F6DB52603F (void);
// 0x000003A5 System.Collections.IEnumerator OculusSampleFramework.FingerTipPokeTool::AttachTriggerLogic()
extern void FingerTipPokeTool_AttachTriggerLogic_m806CC48725DBC7E41BE57F20DA730BEB3C96EE1F (void);
// 0x000003A6 System.Void OculusSampleFramework.FingerTipPokeTool::Update()
extern void FingerTipPokeTool_Update_m2C3F38F5A27DC330842FE5B82FCA1C850EE3D85B (void);
// 0x000003A7 System.Void OculusSampleFramework.FingerTipPokeTool::UpdateAverageVelocity()
extern void FingerTipPokeTool_UpdateAverageVelocity_mC1BFD7068F92A60898578833DB93CF37BCA75E38 (void);
// 0x000003A8 System.Void OculusSampleFramework.FingerTipPokeTool::CheckAndUpdateScale()
extern void FingerTipPokeTool_CheckAndUpdateScale_mDE1DDF48D29BA291BCE6EDB0EDB6CDD04E803207 (void);
// 0x000003A9 System.Collections.Generic.List`1<OculusSampleFramework.InteractableCollisionInfo> OculusSampleFramework.FingerTipPokeTool::GetNextIntersectingObjects()
extern void FingerTipPokeTool_GetNextIntersectingObjects_mA962AC37DB3CBF382E81F163B450CC69A42E156B (void);
// 0x000003AA System.Void OculusSampleFramework.FingerTipPokeTool::FocusOnInteractable(OculusSampleFramework.Interactable,OculusSampleFramework.ColliderZone)
extern void FingerTipPokeTool_FocusOnInteractable_mBA11E0AA0F742ADA5FC0859146C644DEA5D51AE7 (void);
// 0x000003AB System.Void OculusSampleFramework.FingerTipPokeTool::DeFocus()
extern void FingerTipPokeTool_DeFocus_mEA2E9F73BCF74755786061B17A11B663BB01474C (void);
// 0x000003AC System.Void OculusSampleFramework.FingerTipPokeTool::.ctor()
extern void FingerTipPokeTool__ctor_m0793D77F8FC85C10E5943D692BEC9AC367F68173 (void);
// 0x000003AD System.Void OculusSampleFramework.FingerTipPokeTool/<AttachTriggerLogic>d__21::.ctor(System.Int32)
extern void U3CAttachTriggerLogicU3Ed__21__ctor_mCC871AFFBBB476D21423473C54CBDEBD2F14CE9D (void);
// 0x000003AE System.Void OculusSampleFramework.FingerTipPokeTool/<AttachTriggerLogic>d__21::System.IDisposable.Dispose()
extern void U3CAttachTriggerLogicU3Ed__21_System_IDisposable_Dispose_mBBABBA85272B6862FE1453CBD314E245B6EA24E7 (void);
// 0x000003AF System.Boolean OculusSampleFramework.FingerTipPokeTool/<AttachTriggerLogic>d__21::MoveNext()
extern void U3CAttachTriggerLogicU3Ed__21_MoveNext_m4D36E1F74B87AF1B2AE443A494AC3BD5616CD4FF (void);
// 0x000003B0 System.Object OculusSampleFramework.FingerTipPokeTool/<AttachTriggerLogic>d__21::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAttachTriggerLogicU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m91F1B4E86C3AF75E8070FF63A39EBFF0539499B5 (void);
// 0x000003B1 System.Void OculusSampleFramework.FingerTipPokeTool/<AttachTriggerLogic>d__21::System.Collections.IEnumerator.Reset()
extern void U3CAttachTriggerLogicU3Ed__21_System_Collections_IEnumerator_Reset_m550408ED48447F61859BFBD490115411547A6856 (void);
// 0x000003B2 System.Object OculusSampleFramework.FingerTipPokeTool/<AttachTriggerLogic>d__21::System.Collections.IEnumerator.get_Current()
extern void U3CAttachTriggerLogicU3Ed__21_System_Collections_IEnumerator_get_Current_mB807F3B3CCBDAF22F0233E0A7A6CDB2DE46DC36E (void);
// 0x000003B3 OculusSampleFramework.InteractableTool OculusSampleFramework.FingerTipPokeToolView::get_InteractableTool()
extern void FingerTipPokeToolView_get_InteractableTool_mBB4D33BE156B9EE4FBFEB6D3558E3E04B8ADB51F (void);
// 0x000003B4 System.Void OculusSampleFramework.FingerTipPokeToolView::set_InteractableTool(OculusSampleFramework.InteractableTool)
extern void FingerTipPokeToolView_set_InteractableTool_mDF529210D8F673D622B4427BBDC28106C80FF9AB (void);
// 0x000003B5 System.Boolean OculusSampleFramework.FingerTipPokeToolView::get_EnableState()
extern void FingerTipPokeToolView_get_EnableState_m4C69AA39F36971BCB98909949405B1B0766A7391 (void);
// 0x000003B6 System.Void OculusSampleFramework.FingerTipPokeToolView::set_EnableState(System.Boolean)
extern void FingerTipPokeToolView_set_EnableState_m5D51EB5315EB2FBF3FE5F9B471C9B1313905820F (void);
// 0x000003B7 System.Boolean OculusSampleFramework.FingerTipPokeToolView::get_ToolActivateState()
extern void FingerTipPokeToolView_get_ToolActivateState_mE74D50A247203ABF3902B02AC10F46172A64155B (void);
// 0x000003B8 System.Void OculusSampleFramework.FingerTipPokeToolView::set_ToolActivateState(System.Boolean)
extern void FingerTipPokeToolView_set_ToolActivateState_m5D27DF2EB537D9BA0E4DBA30FA1A339E7EC45EFF (void);
// 0x000003B9 System.Single OculusSampleFramework.FingerTipPokeToolView::get_SphereRadius()
extern void FingerTipPokeToolView_get_SphereRadius_mA3C7336BF367853C422F0AFEB012A77CFB627BD8 (void);
// 0x000003BA System.Void OculusSampleFramework.FingerTipPokeToolView::set_SphereRadius(System.Single)
extern void FingerTipPokeToolView_set_SphereRadius_mAD1E864E25672BE5A87327B53BEF716C3AF34275 (void);
// 0x000003BB System.Void OculusSampleFramework.FingerTipPokeToolView::Awake()
extern void FingerTipPokeToolView_Awake_m5F066BEF3A88699EBF8BD848574BD8CDCFDFDC24 (void);
// 0x000003BC System.Void OculusSampleFramework.FingerTipPokeToolView::SetFocusedInteractable(OculusSampleFramework.Interactable)
extern void FingerTipPokeToolView_SetFocusedInteractable_m5B02B5A8DECB19168DF581C171368E46F36D9409 (void);
// 0x000003BD System.Void OculusSampleFramework.FingerTipPokeToolView::.ctor()
extern void FingerTipPokeToolView__ctor_mAF5D655B9724611A68D1A582B2473D035D308784 (void);
// 0x000003BE System.Void OculusSampleFramework.InteractableCollisionInfo::.ctor(OculusSampleFramework.ColliderZone,OculusSampleFramework.InteractableCollisionDepth,OculusSampleFramework.InteractableTool)
extern void InteractableCollisionInfo__ctor_m7348491A5EAFD2DB5F4B9702C512F9244B6EAC91 (void);
// 0x000003BF UnityEngine.Transform OculusSampleFramework.InteractableTool::get_ToolTransform()
extern void InteractableTool_get_ToolTransform_m205F590619ED079C0A72EA22A89EE3927842DCA7 (void);
// 0x000003C0 System.Boolean OculusSampleFramework.InteractableTool::get_IsRightHandedTool()
extern void InteractableTool_get_IsRightHandedTool_m737F795BBD30E1EC82B68A7EF69CD222FB31541A (void);
// 0x000003C1 System.Void OculusSampleFramework.InteractableTool::set_IsRightHandedTool(System.Boolean)
extern void InteractableTool_set_IsRightHandedTool_m465A3CE11D7F4A97C4D2B0FC69F90922C886D53D (void);
// 0x000003C2 OculusSampleFramework.InteractableToolTags OculusSampleFramework.InteractableTool::get_ToolTags()
// 0x000003C3 OculusSampleFramework.ToolInputState OculusSampleFramework.InteractableTool::get_ToolInputState()
// 0x000003C4 System.Boolean OculusSampleFramework.InteractableTool::get_IsFarFieldTool()
// 0x000003C5 UnityEngine.Vector3 OculusSampleFramework.InteractableTool::get_Velocity()
extern void InteractableTool_get_Velocity_mAC1D35D388D176A5F972A883516180AC66B8090F (void);
// 0x000003C6 System.Void OculusSampleFramework.InteractableTool::set_Velocity(UnityEngine.Vector3)
extern void InteractableTool_set_Velocity_m42BE2E5966AF9A78A1C157A35B123926EFB1814B (void);
// 0x000003C7 UnityEngine.Vector3 OculusSampleFramework.InteractableTool::get_InteractionPosition()
extern void InteractableTool_get_InteractionPosition_mE0FB722D4A4A52B1ACFB6D35CAFB876AA01C2111 (void);
// 0x000003C8 System.Void OculusSampleFramework.InteractableTool::set_InteractionPosition(UnityEngine.Vector3)
extern void InteractableTool_set_InteractionPosition_m7D1E65ACF3D7C8D81E733557B4B97C858B11FAE2 (void);
// 0x000003C9 System.Collections.Generic.List`1<OculusSampleFramework.InteractableCollisionInfo> OculusSampleFramework.InteractableTool::GetCurrentIntersectingObjects()
extern void InteractableTool_GetCurrentIntersectingObjects_m6F4274996E629AF558DAE685B712356444DE3147 (void);
// 0x000003CA System.Collections.Generic.List`1<OculusSampleFramework.InteractableCollisionInfo> OculusSampleFramework.InteractableTool::GetNextIntersectingObjects()
// 0x000003CB System.Void OculusSampleFramework.InteractableTool::FocusOnInteractable(OculusSampleFramework.Interactable,OculusSampleFramework.ColliderZone)
// 0x000003CC System.Void OculusSampleFramework.InteractableTool::DeFocus()
// 0x000003CD System.Boolean OculusSampleFramework.InteractableTool::get_EnableState()
// 0x000003CE System.Void OculusSampleFramework.InteractableTool::set_EnableState(System.Boolean)
// 0x000003CF System.Void OculusSampleFramework.InteractableTool::Initialize()
// 0x000003D0 System.Collections.Generic.KeyValuePair`2<OculusSampleFramework.Interactable,OculusSampleFramework.InteractableCollisionInfo> OculusSampleFramework.InteractableTool::GetFirstCurrentCollisionInfo()
extern void InteractableTool_GetFirstCurrentCollisionInfo_m128C0E18F5FF4B752ECAA380CEAD01A0C35D4C40 (void);
// 0x000003D1 System.Void OculusSampleFramework.InteractableTool::ClearAllCurrentCollisionInfos()
extern void InteractableTool_ClearAllCurrentCollisionInfos_mB2B670C225F4D7E9ECA1FE8F32A4591C794D3F95 (void);
// 0x000003D2 System.Void OculusSampleFramework.InteractableTool::UpdateCurrentCollisionsBasedOnDepth()
extern void InteractableTool_UpdateCurrentCollisionsBasedOnDepth_m0C7062FF22E200B4D7E8E315BC862B9D47698F9A (void);
// 0x000003D3 System.Void OculusSampleFramework.InteractableTool::UpdateLatestCollisionData()
extern void InteractableTool_UpdateLatestCollisionData_m464888128C71919DDBB2C6C7688EAF0740634250 (void);
// 0x000003D4 System.Void OculusSampleFramework.InteractableTool::.ctor()
extern void InteractableTool__ctor_m53710FF617E85E4274C1AAA0653FF167C28A3722 (void);
// 0x000003D5 OculusSampleFramework.InteractableTool OculusSampleFramework.InteractableToolView::get_InteractableTool()
// 0x000003D6 System.Void OculusSampleFramework.InteractableToolView::SetFocusedInteractable(OculusSampleFramework.Interactable)
// 0x000003D7 System.Boolean OculusSampleFramework.InteractableToolView::get_EnableState()
// 0x000003D8 System.Void OculusSampleFramework.InteractableToolView::set_EnableState(System.Boolean)
// 0x000003D9 System.Boolean OculusSampleFramework.InteractableToolView::get_ToolActivateState()
// 0x000003DA System.Void OculusSampleFramework.InteractableToolView::set_ToolActivateState(System.Boolean)
// 0x000003DB System.Boolean OculusSampleFramework.PinchStateModule::get_PinchUpAndDownOnFocusedObject()
extern void PinchStateModule_get_PinchUpAndDownOnFocusedObject_m6A5427560A3F4A2372BC64C89FD513CB60D6E345 (void);
// 0x000003DC System.Boolean OculusSampleFramework.PinchStateModule::get_PinchSteadyOnFocusedObject()
extern void PinchStateModule_get_PinchSteadyOnFocusedObject_mE211E69F60B0867D44633FA5B25FFDFEDE2E5D13 (void);
// 0x000003DD System.Boolean OculusSampleFramework.PinchStateModule::get_PinchDownOnFocusedObject()
extern void PinchStateModule_get_PinchDownOnFocusedObject_m1D2C7ED77EFB4833B1921792A3735636BA207109 (void);
// 0x000003DE System.Void OculusSampleFramework.PinchStateModule::.ctor()
extern void PinchStateModule__ctor_m06DE10F3EADE5A00F264E444C105C23A0F4D6891 (void);
// 0x000003DF System.Void OculusSampleFramework.PinchStateModule::UpdateState(OVRHand,OculusSampleFramework.Interactable)
extern void PinchStateModule_UpdateState_mE6EC067C8424F21E0C1553703E69E5B25E56DCBE (void);
// 0x000003E0 OculusSampleFramework.InteractableToolTags OculusSampleFramework.RayTool::get_ToolTags()
extern void RayTool_get_ToolTags_mF79256339F1A375AF576B2E2029537F43EE2C0B7 (void);
// 0x000003E1 OculusSampleFramework.ToolInputState OculusSampleFramework.RayTool::get_ToolInputState()
extern void RayTool_get_ToolInputState_m54478A71F7ED3FE7E08BE631D08037E4A6F76D89 (void);
// 0x000003E2 System.Boolean OculusSampleFramework.RayTool::get_IsFarFieldTool()
extern void RayTool_get_IsFarFieldTool_mF690CF4ED00E55B933E58EE8B46737D215F4C6A7 (void);
// 0x000003E3 System.Boolean OculusSampleFramework.RayTool::get_EnableState()
extern void RayTool_get_EnableState_mF4A05EDD10F5FE793D780CF3745D6881ED3B3D3D (void);
// 0x000003E4 System.Void OculusSampleFramework.RayTool::set_EnableState(System.Boolean)
extern void RayTool_set_EnableState_mE27327551A200506EC3FA1185A10F9392AAAC4C9 (void);
// 0x000003E5 System.Void OculusSampleFramework.RayTool::Initialize()
extern void RayTool_Initialize_m6C629CDBF7127645DCD9CABC894F1D1327FC036C (void);
// 0x000003E6 System.Void OculusSampleFramework.RayTool::OnDestroy()
extern void RayTool_OnDestroy_m8332B404479AA0DA187DF0C424576FB9262442C1 (void);
// 0x000003E7 System.Void OculusSampleFramework.RayTool::Update()
extern void RayTool_Update_mFF995DE32795EB15C757CFD5CB8201786B0F62DC (void);
// 0x000003E8 UnityEngine.Vector3 OculusSampleFramework.RayTool::GetRayCastOrigin()
extern void RayTool_GetRayCastOrigin_m4AE3575A3D65B27E3809814C3415E39FC5489B47 (void);
// 0x000003E9 System.Collections.Generic.List`1<OculusSampleFramework.InteractableCollisionInfo> OculusSampleFramework.RayTool::GetNextIntersectingObjects()
extern void RayTool_GetNextIntersectingObjects_m8BA14292395263E433DD82B9F64080C42A5A4821 (void);
// 0x000003EA System.Boolean OculusSampleFramework.RayTool::HasRayReleasedInteractable(OculusSampleFramework.Interactable)
extern void RayTool_HasRayReleasedInteractable_m9B7A76FC8F15B453D91980F1D118EFEF432BA5CA (void);
// 0x000003EB OculusSampleFramework.Interactable OculusSampleFramework.RayTool::FindTargetInteractable()
extern void RayTool_FindTargetInteractable_m4D0BD21D1425CD76DD0FA675ECBF7E3AA4AAB685 (void);
// 0x000003EC OculusSampleFramework.Interactable OculusSampleFramework.RayTool::FindPrimaryRaycastHit(UnityEngine.Vector3,UnityEngine.Vector3)
extern void RayTool_FindPrimaryRaycastHit_mB6C7E211ADEC012A379E02BBC2D60B5B567CB8F8 (void);
// 0x000003ED OculusSampleFramework.Interactable OculusSampleFramework.RayTool::FindInteractableViaConeTest(UnityEngine.Vector3,UnityEngine.Vector3)
extern void RayTool_FindInteractableViaConeTest_mEDB0AE5A0EF2B9FF895B7D08EE6B50A5DD1AD5CF (void);
// 0x000003EE System.Void OculusSampleFramework.RayTool::FocusOnInteractable(OculusSampleFramework.Interactable,OculusSampleFramework.ColliderZone)
extern void RayTool_FocusOnInteractable_m535AB705865AB75D40842C9B5587E17732DDD2F9 (void);
// 0x000003EF System.Void OculusSampleFramework.RayTool::DeFocus()
extern void RayTool_DeFocus_m085E103A8A047F7067906C31DACE15ACE126C6CE (void);
// 0x000003F0 System.Void OculusSampleFramework.RayTool::.ctor()
extern void RayTool__ctor_mC1D1E10E1C6B15267986E22E04C6A23966CA681B (void);
// 0x000003F1 System.Boolean OculusSampleFramework.RayToolView::get_EnableState()
extern void RayToolView_get_EnableState_m464E298E5F0DDCF469868DA3F7A07C1BC74FCDE7 (void);
// 0x000003F2 System.Void OculusSampleFramework.RayToolView::set_EnableState(System.Boolean)
extern void RayToolView_set_EnableState_m0C757E1F42C28C763F57C93F4670ED3DA3B511A5 (void);
// 0x000003F3 System.Boolean OculusSampleFramework.RayToolView::get_ToolActivateState()
extern void RayToolView_get_ToolActivateState_m5DE8AD80488ACB610C8A00467984095998DB4723 (void);
// 0x000003F4 System.Void OculusSampleFramework.RayToolView::set_ToolActivateState(System.Boolean)
extern void RayToolView_set_ToolActivateState_m382F731A347E5EF495984B48AB4EE7941FD0E99B (void);
// 0x000003F5 System.Void OculusSampleFramework.RayToolView::Awake()
extern void RayToolView_Awake_m8D4B4C38C29579601A9E21E8BE3A2E4E95246985 (void);
// 0x000003F6 OculusSampleFramework.InteractableTool OculusSampleFramework.RayToolView::get_InteractableTool()
extern void RayToolView_get_InteractableTool_m6678005A9883BB94200F353DF981AE211CFFE44A (void);
// 0x000003F7 System.Void OculusSampleFramework.RayToolView::set_InteractableTool(OculusSampleFramework.InteractableTool)
extern void RayToolView_set_InteractableTool_m0E3083D4DD470BB252858CF5D401E248705FD46B (void);
// 0x000003F8 System.Void OculusSampleFramework.RayToolView::SetFocusedInteractable(OculusSampleFramework.Interactable)
extern void RayToolView_SetFocusedInteractable_mE7F566BFFD744607512ACBBDC070D22D737A1E64 (void);
// 0x000003F9 System.Void OculusSampleFramework.RayToolView::Update()
extern void RayToolView_Update_mC0702F11E7C9A01B8E14CDD5C607E15B2B1882D7 (void);
// 0x000003FA UnityEngine.Vector3 OculusSampleFramework.RayToolView::GetPointOnBezierCurve(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void RayToolView_GetPointOnBezierCurve_m3D4198786E95ADE93CB7673AC90DB274A655D5D7 (void);
// 0x000003FB System.Void OculusSampleFramework.RayToolView::.ctor()
extern void RayToolView__ctor_m729843F49E716782D52EEB4D3A725EAE9E7F59D9 (void);
// 0x000003FC System.Boolean OculusSampleFramework.DistanceGrabberSample::get_UseSpherecast()
extern void DistanceGrabberSample_get_UseSpherecast_m1940F73780371F34046759ED81DDBEDBFCA99FAC (void);
// 0x000003FD System.Void OculusSampleFramework.DistanceGrabberSample::set_UseSpherecast(System.Boolean)
extern void DistanceGrabberSample_set_UseSpherecast_mE18E95F373903F263003BD244ED69D60E4F17C74 (void);
// 0x000003FE System.Boolean OculusSampleFramework.DistanceGrabberSample::get_AllowGrabThroughWalls()
extern void DistanceGrabberSample_get_AllowGrabThroughWalls_m0E2C0AE5522B8066A49EE17D4ED8901796CEB3F2 (void);
// 0x000003FF System.Void OculusSampleFramework.DistanceGrabberSample::set_AllowGrabThroughWalls(System.Boolean)
extern void DistanceGrabberSample_set_AllowGrabThroughWalls_m47024AC8A8622EB0582E9FBBD92AFAE9FDC70186 (void);
// 0x00000400 System.Void OculusSampleFramework.DistanceGrabberSample::Start()
extern void DistanceGrabberSample_Start_m17A8BC2B0CD5CA1D6A19E5AFAA95B07239A6EFDC (void);
// 0x00000401 System.Void OculusSampleFramework.DistanceGrabberSample::ToggleSphereCasting(UnityEngine.UI.Toggle)
extern void DistanceGrabberSample_ToggleSphereCasting_mA150837732BE9CA29DB37AAA3D21D0956F198488 (void);
// 0x00000402 System.Void OculusSampleFramework.DistanceGrabberSample::ToggleGrabThroughWalls(UnityEngine.UI.Toggle)
extern void DistanceGrabberSample_ToggleGrabThroughWalls_mCEBEA385FE68A84650D3849E5B20D729FE663A74 (void);
// 0x00000403 System.Void OculusSampleFramework.DistanceGrabberSample::.ctor()
extern void DistanceGrabberSample__ctor_m91EB15189BB6C76FE3D70960D66F80C3F474CE39 (void);
// 0x00000404 System.Void OculusSampleFramework.ControllerBoxController::Awake()
extern void ControllerBoxController_Awake_m10CE6D278450B3090C9F6FEBE087CA23CC9FFD8F (void);
// 0x00000405 System.Void OculusSampleFramework.ControllerBoxController::StartStopStateChanged(OculusSampleFramework.InteractableStateArgs)
extern void ControllerBoxController_StartStopStateChanged_mD500AA96C8E03F97F611D5EA54F954C933753451 (void);
// 0x00000406 System.Void OculusSampleFramework.ControllerBoxController::DecreaseSpeedStateChanged(OculusSampleFramework.InteractableStateArgs)
extern void ControllerBoxController_DecreaseSpeedStateChanged_m3229EACD0C33045DC2CB25A0BF6120F0838F0EE0 (void);
// 0x00000407 System.Void OculusSampleFramework.ControllerBoxController::IncreaseSpeedStateChanged(OculusSampleFramework.InteractableStateArgs)
extern void ControllerBoxController_IncreaseSpeedStateChanged_mEFE12568E8F140F770F50AF38395E0DB22C9788B (void);
// 0x00000408 System.Void OculusSampleFramework.ControllerBoxController::SmokeButtonStateChanged(OculusSampleFramework.InteractableStateArgs)
extern void ControllerBoxController_SmokeButtonStateChanged_m886E38B2844CF61820F76E9724FDCCA2C61CA372 (void);
// 0x00000409 System.Void OculusSampleFramework.ControllerBoxController::WhistleButtonStateChanged(OculusSampleFramework.InteractableStateArgs)
extern void ControllerBoxController_WhistleButtonStateChanged_m28F2550607F6321A43FFB7891295EBBCCC90179A (void);
// 0x0000040A System.Void OculusSampleFramework.ControllerBoxController::ReverseButtonStateChanged(OculusSampleFramework.InteractableStateArgs)
extern void ControllerBoxController_ReverseButtonStateChanged_m8183F155BD2B5E5F76680E51DA08B88B315AE0FA (void);
// 0x0000040B System.Void OculusSampleFramework.ControllerBoxController::SwitchVisualization(OculusSampleFramework.InteractableStateArgs)
extern void ControllerBoxController_SwitchVisualization_m5D7DB78DF80500F6C712B626B834CEC6BD2D59D0 (void);
// 0x0000040C System.Void OculusSampleFramework.ControllerBoxController::GoMoo(OculusSampleFramework.InteractableStateArgs)
extern void ControllerBoxController_GoMoo_mFBEDA2C31525584CD95F0C59E73B5A10E9FCD509 (void);
// 0x0000040D System.Void OculusSampleFramework.ControllerBoxController::.ctor()
extern void ControllerBoxController__ctor_mE2B5C31F1DA7DC4A076BB4715E72787A1FB3705C (void);
// 0x0000040E System.Void OculusSampleFramework.CowController::Start()
extern void CowController_Start_mC4B6534879CC6708AFC5D442E5282DFE915173D7 (void);
// 0x0000040F System.Void OculusSampleFramework.CowController::PlayMooSound()
extern void CowController_PlayMooSound_m6EC41C83F14928C33AAB8C96D1DB6F10E70B5104 (void);
// 0x00000410 System.Void OculusSampleFramework.CowController::GoMooCowGo()
extern void CowController_GoMooCowGo_m0F2CD8AAFA84A5E77222026981CE7BE734090D96 (void);
// 0x00000411 System.Void OculusSampleFramework.CowController::.ctor()
extern void CowController__ctor_m4EC07EAB37300BBD79EDBAA4073CB736455CBF28 (void);
// 0x00000412 System.Void OculusSampleFramework.PanelHMDFollower::Awake()
extern void PanelHMDFollower_Awake_m2CF549A1B7AD1994E135D0C3A64B3AAE4BE1C91C (void);
// 0x00000413 System.Void OculusSampleFramework.PanelHMDFollower::Update()
extern void PanelHMDFollower_Update_m78849650838D5B71CC1A8AF283E9A26DF5E78E17 (void);
// 0x00000414 UnityEngine.Vector3 OculusSampleFramework.PanelHMDFollower::CalculateIdealAnchorPosition()
extern void PanelHMDFollower_CalculateIdealAnchorPosition_m74F280D8BC9291C194EB4FC9817A4150ABC944E2 (void);
// 0x00000415 System.Collections.IEnumerator OculusSampleFramework.PanelHMDFollower::LerpToHMD()
extern void PanelHMDFollower_LerpToHMD_mD8B9F448F88D9F598368D4B930DFA7BC19A843BB (void);
// 0x00000416 System.Void OculusSampleFramework.PanelHMDFollower::.ctor()
extern void PanelHMDFollower__ctor_mDFD1C555C729E0009E9FC989048FA5806267EC72 (void);
// 0x00000417 System.Void OculusSampleFramework.PanelHMDFollower/<LerpToHMD>d__13::.ctor(System.Int32)
extern void U3CLerpToHMDU3Ed__13__ctor_m2718D92561980DA3B2F04C2D102AB928155D0AAA (void);
// 0x00000418 System.Void OculusSampleFramework.PanelHMDFollower/<LerpToHMD>d__13::System.IDisposable.Dispose()
extern void U3CLerpToHMDU3Ed__13_System_IDisposable_Dispose_m6F79FD9B76A0633FBC96538B385DB07AF57C90DC (void);
// 0x00000419 System.Boolean OculusSampleFramework.PanelHMDFollower/<LerpToHMD>d__13::MoveNext()
extern void U3CLerpToHMDU3Ed__13_MoveNext_m9C0F7AA018E489420080E85A9C327248E2301DC4 (void);
// 0x0000041A System.Object OculusSampleFramework.PanelHMDFollower/<LerpToHMD>d__13::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLerpToHMDU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD58D03CC073A72DDD7B0EC4188CB4614E92A68AF (void);
// 0x0000041B System.Void OculusSampleFramework.PanelHMDFollower/<LerpToHMD>d__13::System.Collections.IEnumerator.Reset()
extern void U3CLerpToHMDU3Ed__13_System_Collections_IEnumerator_Reset_mA09722794732F819AA5D31CB2AF5BCB0C2FD817A (void);
// 0x0000041C System.Object OculusSampleFramework.PanelHMDFollower/<LerpToHMD>d__13::System.Collections.IEnumerator.get_Current()
extern void U3CLerpToHMDU3Ed__13_System_Collections_IEnumerator_get_Current_mF39A3910A6D633D8600FF5B154BC7B6764772E9C (void);
// 0x0000041D OculusSampleFramework.SelectionCylinder/SelectionState OculusSampleFramework.SelectionCylinder::get_CurrSelectionState()
extern void SelectionCylinder_get_CurrSelectionState_mEEA0EACE12CE6A5F1BDAA7E59A7EC5D77D56FAEA (void);
// 0x0000041E System.Void OculusSampleFramework.SelectionCylinder::set_CurrSelectionState(OculusSampleFramework.SelectionCylinder/SelectionState)
extern void SelectionCylinder_set_CurrSelectionState_m960B75182FF50885FD248F74A05F7D33BA3D67D3 (void);
// 0x0000041F System.Void OculusSampleFramework.SelectionCylinder::Awake()
extern void SelectionCylinder_Awake_m3C9863696F1825751F817545A99016F0840CC0EF (void);
// 0x00000420 System.Void OculusSampleFramework.SelectionCylinder::OnDestroy()
extern void SelectionCylinder_OnDestroy_mB8F6CF93EB637C30627D4851B5F0290D0D730A49 (void);
// 0x00000421 System.Void OculusSampleFramework.SelectionCylinder::AffectSelectionColor(UnityEngine.Color[])
extern void SelectionCylinder_AffectSelectionColor_m98DE925B474211ACCCCB6085DDB3A90399E4DCA3 (void);
// 0x00000422 System.Void OculusSampleFramework.SelectionCylinder::.ctor()
extern void SelectionCylinder__ctor_m19D204E84FD5989ECFE8FBF1480C7FCF758CFE92 (void);
// 0x00000423 System.Void OculusSampleFramework.SelectionCylinder::.cctor()
extern void SelectionCylinder__cctor_m27E9FE6C9906FDF3BE47F840DE95C67921BA60D2 (void);
// 0x00000424 System.Single OculusSampleFramework.TrackSegment::get_StartDistance()
extern void TrackSegment_get_StartDistance_m7754F5C2A4BE8A74DB09CE3CED8D2598630B0D7B (void);
// 0x00000425 System.Void OculusSampleFramework.TrackSegment::set_StartDistance(System.Single)
extern void TrackSegment_set_StartDistance_mCAF42BD5254AFCFFE3001A6E20070939A6F20195 (void);
// 0x00000426 System.Single OculusSampleFramework.TrackSegment::get_GridSize()
extern void TrackSegment_get_GridSize_m3D99EF429B98F214239CA9CA54A076BBB7FE92E3 (void);
// 0x00000427 System.Void OculusSampleFramework.TrackSegment::set_GridSize(System.Single)
extern void TrackSegment_set_GridSize_m9823CF02AE0029655CBEAFF47200B88A76BD2E85 (void);
// 0x00000428 System.Int32 OculusSampleFramework.TrackSegment::get_SubDivCount()
extern void TrackSegment_get_SubDivCount_mE962363663526F01DC7D77F360A5E97A5E8D333E (void);
// 0x00000429 System.Void OculusSampleFramework.TrackSegment::set_SubDivCount(System.Int32)
extern void TrackSegment_set_SubDivCount_m208543971918F3C42B26627EB3BFC845A034B19A (void);
// 0x0000042A OculusSampleFramework.TrackSegment/SegmentType OculusSampleFramework.TrackSegment::get_Type()
extern void TrackSegment_get_Type_mB16F5DB9C4FD3AF8FA91CF5880D5DD48AA8681FF (void);
// 0x0000042B OculusSampleFramework.Pose OculusSampleFramework.TrackSegment::get_EndPose()
extern void TrackSegment_get_EndPose_m0F2F98AAF1A5C6C067720CA8067C3FD47D43876A (void);
// 0x0000042C System.Single OculusSampleFramework.TrackSegment::get_Radius()
extern void TrackSegment_get_Radius_mB458D02292A84615CA9BFEA5F03C1059268D6BE6 (void);
// 0x0000042D System.Single OculusSampleFramework.TrackSegment::setGridSize(System.Single)
extern void TrackSegment_setGridSize_mC74C3F3285B5ABD033CF68BDB0770DE56F16E990 (void);
// 0x0000042E System.Single OculusSampleFramework.TrackSegment::get_SegmentLength()
extern void TrackSegment_get_SegmentLength_m63FFFA4AD5055C552C805872637BC6839AC55F39 (void);
// 0x0000042F System.Void OculusSampleFramework.TrackSegment::Awake()
extern void TrackSegment_Awake_m9650E6E117E2253990A03F82515ED14E990DDA97 (void);
// 0x00000430 System.Void OculusSampleFramework.TrackSegment::UpdatePose(System.Single,OculusSampleFramework.Pose)
extern void TrackSegment_UpdatePose_m97FBB83914AB8D3CB49D9D85467DEB7431248B53 (void);
// 0x00000431 System.Void OculusSampleFramework.TrackSegment::Update()
extern void TrackSegment_Update_m108D239B881A8A7A1729E74C2EF814E2376D5DB5 (void);
// 0x00000432 System.Void OculusSampleFramework.TrackSegment::OnDisable()
extern void TrackSegment_OnDisable_mB9B6EB483265C1B1BCF980EA6F58ED15D06ABB0A (void);
// 0x00000433 System.Void OculusSampleFramework.TrackSegment::DrawDebugLines()
extern void TrackSegment_DrawDebugLines_m37FEF6E5D1842508645C8F8FEB28BABDA03D7253 (void);
// 0x00000434 System.Void OculusSampleFramework.TrackSegment::RegenerateTrackAndMesh()
extern void TrackSegment_RegenerateTrackAndMesh_m4841B12BDC519C7862B4901565B44A9B6787AD5C (void);
// 0x00000435 System.Void OculusSampleFramework.TrackSegment::.ctor()
extern void TrackSegment__ctor_mC6FA53B0963D2B395BCE338C1EC39985B28C4F96 (void);
// 0x00000436 System.Void OculusSampleFramework.TrainButtonVisualController::Awake()
extern void TrainButtonVisualController_Awake_m84F6745B2D689E89F2AAF1ECA67B2FC59FEC4F14 (void);
// 0x00000437 System.Void OculusSampleFramework.TrainButtonVisualController::OnDestroy()
extern void TrainButtonVisualController_OnDestroy_m297A762E98F73EF8414F095856CE387C438D1798 (void);
// 0x00000438 System.Void OculusSampleFramework.TrainButtonVisualController::OnEnable()
extern void TrainButtonVisualController_OnEnable_m0564F0AD8EDBCF9FAFE21A3D15EB7CCC7C65DE97 (void);
// 0x00000439 System.Void OculusSampleFramework.TrainButtonVisualController::OnDisable()
extern void TrainButtonVisualController_OnDisable_m24A294D4F2E5C301D05DE52F29EDA4400EC831C5 (void);
// 0x0000043A System.Void OculusSampleFramework.TrainButtonVisualController::ActionOrInContactZoneStayEvent(OculusSampleFramework.ColliderZoneArgs)
extern void TrainButtonVisualController_ActionOrInContactZoneStayEvent_m7AF8B0FD00D7F603EC903094DC343CC42BF7ABD0 (void);
// 0x0000043B System.Void OculusSampleFramework.TrainButtonVisualController::InteractableStateChanged(OculusSampleFramework.InteractableStateArgs)
extern void TrainButtonVisualController_InteractableStateChanged_mCF09476B0142274EAFE2C5ADE762C28633BD23EF (void);
// 0x0000043C System.Void OculusSampleFramework.TrainButtonVisualController::PlaySound(UnityEngine.AudioClip)
extern void TrainButtonVisualController_PlaySound_m65FC0A9207B130533020BEA24C3F291415B02249 (void);
// 0x0000043D System.Void OculusSampleFramework.TrainButtonVisualController::StopResetLerping()
extern void TrainButtonVisualController_StopResetLerping_m9ADCE7C308758261CC14032402BEED4F21EE4B4B (void);
// 0x0000043E System.Void OculusSampleFramework.TrainButtonVisualController::LerpToOldPosition()
extern void TrainButtonVisualController_LerpToOldPosition_m95E0582BA249685039EEA46A3AD1BDD57CCE3B68 (void);
// 0x0000043F System.Collections.IEnumerator OculusSampleFramework.TrainButtonVisualController::ResetPosition()
extern void TrainButtonVisualController_ResetPosition_m9232F6767751D6404FFFCB0944D9FE7926F02B0B (void);
// 0x00000440 System.Void OculusSampleFramework.TrainButtonVisualController::.ctor()
extern void TrainButtonVisualController__ctor_m67AA3D848961906B2E5F20150B0C82462B7E190F (void);
// 0x00000441 System.Void OculusSampleFramework.TrainButtonVisualController/<ResetPosition>d__26::.ctor(System.Int32)
extern void U3CResetPositionU3Ed__26__ctor_m5635000743474AB03A282A54DAA034BE871944D8 (void);
// 0x00000442 System.Void OculusSampleFramework.TrainButtonVisualController/<ResetPosition>d__26::System.IDisposable.Dispose()
extern void U3CResetPositionU3Ed__26_System_IDisposable_Dispose_mFD4D0FB62BBFDC93E0DEAD7EA68430B7A4ACAF69 (void);
// 0x00000443 System.Boolean OculusSampleFramework.TrainButtonVisualController/<ResetPosition>d__26::MoveNext()
extern void U3CResetPositionU3Ed__26_MoveNext_m5E72132EBEF7DB0236E6E8EBD6632A16E0F748E0 (void);
// 0x00000444 System.Object OculusSampleFramework.TrainButtonVisualController/<ResetPosition>d__26::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CResetPositionU3Ed__26_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2BE3FF62F4373A00DD3E8BBCBB2036F6761E547B (void);
// 0x00000445 System.Void OculusSampleFramework.TrainButtonVisualController/<ResetPosition>d__26::System.Collections.IEnumerator.Reset()
extern void U3CResetPositionU3Ed__26_System_Collections_IEnumerator_Reset_mFAB28539B7B0AF0B783CD738C41D9DC51A1E8853 (void);
// 0x00000446 System.Object OculusSampleFramework.TrainButtonVisualController/<ResetPosition>d__26::System.Collections.IEnumerator.get_Current()
extern void U3CResetPositionU3Ed__26_System_Collections_IEnumerator_get_Current_m84A8CF3B9E09A2B224C5C4969FBB701BB9E8FF7B (void);
// 0x00000447 System.Single OculusSampleFramework.TrainCar::get_DistanceBehindParentScaled()
extern void TrainCar_get_DistanceBehindParentScaled_m373B41D22495598A3431F0C5FC5017D442208A2F (void);
// 0x00000448 System.Void OculusSampleFramework.TrainCar::Awake()
extern void TrainCar_Awake_m05D4446FBB4ADAF94AECC0466BBF94055909C8F0 (void);
// 0x00000449 System.Void OculusSampleFramework.TrainCar::UpdatePosition()
extern void TrainCar_UpdatePosition_m338A73EAC14EC679C9F5E00DEED605CEA2BDE432 (void);
// 0x0000044A System.Void OculusSampleFramework.TrainCar::.ctor()
extern void TrainCar__ctor_m086A9601B0ADD3B4FD4118437F6DED5539A25486 (void);
// 0x0000044B System.Single OculusSampleFramework.TrainCarBase::get_Distance()
extern void TrainCarBase_get_Distance_mA33D6D4848DFD0820D2C75F9E1F2A76DC755E8A6 (void);
// 0x0000044C System.Void OculusSampleFramework.TrainCarBase::set_Distance(System.Single)
extern void TrainCarBase_set_Distance_m4C3DF32DF27C6B14AB536FA6A983A8AD056B55CB (void);
// 0x0000044D System.Single OculusSampleFramework.TrainCarBase::get_Scale()
extern void TrainCarBase_get_Scale_m5DBF114FDC5DBE97AD3C5BC2712624BBADF3F5D5 (void);
// 0x0000044E System.Void OculusSampleFramework.TrainCarBase::set_Scale(System.Single)
extern void TrainCarBase_set_Scale_mAF09262F53F500D9ED7DA7829847172C5410ED61 (void);
// 0x0000044F System.Void OculusSampleFramework.TrainCarBase::Awake()
extern void TrainCarBase_Awake_m37A702A4A3B24463E19C126F64F2D89F11A9A502 (void);
// 0x00000450 System.Void OculusSampleFramework.TrainCarBase::UpdatePose(System.Single,OculusSampleFramework.TrainCarBase,OculusSampleFramework.Pose)
extern void TrainCarBase_UpdatePose_m0A67C10B69FB1D9A986E883B1CDC382B571085BE (void);
// 0x00000451 System.Void OculusSampleFramework.TrainCarBase::UpdateCarPosition()
extern void TrainCarBase_UpdateCarPosition_m2B2E8799969C239937C47D4615D2101E6F9133A1 (void);
// 0x00000452 System.Void OculusSampleFramework.TrainCarBase::RotateCarWheels()
extern void TrainCarBase_RotateCarWheels_m965650C8E46E761835CC060092CB6F4B69252858 (void);
// 0x00000453 System.Void OculusSampleFramework.TrainCarBase::UpdatePosition()
// 0x00000454 System.Void OculusSampleFramework.TrainCarBase::.ctor()
extern void TrainCarBase__ctor_mF505ADDC2083B90F7A3F45C89C32025DD0002ED8 (void);
// 0x00000455 System.Void OculusSampleFramework.TrainCarBase::.cctor()
extern void TrainCarBase__cctor_m9E6D5CCA7F5D3E7F7A06FFF301303C946A0EFCB4 (void);
// 0x00000456 System.Void OculusSampleFramework.TrainCrossingController::Awake()
extern void TrainCrossingController_Awake_mC8448F974906D7A72F4FB52C18CC6B00887E120A (void);
// 0x00000457 System.Void OculusSampleFramework.TrainCrossingController::OnDestroy()
extern void TrainCrossingController_OnDestroy_m5913750A3AF404FB4E8FC54EA209A020A231A5D4 (void);
// 0x00000458 System.Void OculusSampleFramework.TrainCrossingController::CrossingButtonStateChanged(OculusSampleFramework.InteractableStateArgs)
extern void TrainCrossingController_CrossingButtonStateChanged_mE31580E5F7428418ECBE6FF15AB6BE20D73D998D (void);
// 0x00000459 System.Void OculusSampleFramework.TrainCrossingController::Update()
extern void TrainCrossingController_Update_m580817DC6CF5C8E2EB48A214F1F84EE51EFBB736 (void);
// 0x0000045A System.Void OculusSampleFramework.TrainCrossingController::ActivateTrainCrossing()
extern void TrainCrossingController_ActivateTrainCrossing_m85B47BC1A24078D2E973B527337EEB61794E7849 (void);
// 0x0000045B System.Collections.IEnumerator OculusSampleFramework.TrainCrossingController::AnimateCrossing(System.Single)
extern void TrainCrossingController_AnimateCrossing_m8AE7CFD19C0FF9120FA3040DFD02538E8A297C65 (void);
// 0x0000045C System.Void OculusSampleFramework.TrainCrossingController::AffectMaterials(UnityEngine.Material[],UnityEngine.Color)
extern void TrainCrossingController_AffectMaterials_m6401A1A3E5B6E85E11681CF60FA492BB4BA510FA (void);
// 0x0000045D System.Void OculusSampleFramework.TrainCrossingController::ToggleLightObjects(System.Boolean)
extern void TrainCrossingController_ToggleLightObjects_m4B93FC82AC5CA62BC1F0755839F5AE2D3983C38C (void);
// 0x0000045E System.Void OculusSampleFramework.TrainCrossingController::.ctor()
extern void TrainCrossingController__ctor_m0EA7B850A7E56F174B1E49FF0513392222383826 (void);
// 0x0000045F System.Void OculusSampleFramework.TrainCrossingController/<AnimateCrossing>d__15::.ctor(System.Int32)
extern void U3CAnimateCrossingU3Ed__15__ctor_mFAA4EB62DD8CF2E800A17FB70610D210D4572B2C (void);
// 0x00000460 System.Void OculusSampleFramework.TrainCrossingController/<AnimateCrossing>d__15::System.IDisposable.Dispose()
extern void U3CAnimateCrossingU3Ed__15_System_IDisposable_Dispose_m15B4C628F2391E4765AAD7E5FBEB1ED37163CF6C (void);
// 0x00000461 System.Boolean OculusSampleFramework.TrainCrossingController/<AnimateCrossing>d__15::MoveNext()
extern void U3CAnimateCrossingU3Ed__15_MoveNext_m9E22324ADCB019B88C3415E7B1087C2E5E277572 (void);
// 0x00000462 System.Object OculusSampleFramework.TrainCrossingController/<AnimateCrossing>d__15::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateCrossingU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2E5821A766805B7DA85AF5CDF5B3E79C9007A97D (void);
// 0x00000463 System.Void OculusSampleFramework.TrainCrossingController/<AnimateCrossing>d__15::System.Collections.IEnumerator.Reset()
extern void U3CAnimateCrossingU3Ed__15_System_Collections_IEnumerator_Reset_m1221B7705FF6866AB85D415E8D00A92F1B40B0C3 (void);
// 0x00000464 System.Object OculusSampleFramework.TrainCrossingController/<AnimateCrossing>d__15::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateCrossingU3Ed__15_System_Collections_IEnumerator_get_Current_mDF8E56DBC547B33C024A5EB10750F3D8B075A412 (void);
// 0x00000465 System.Void OculusSampleFramework.TrainLocomotive::Start()
extern void TrainLocomotive_Start_m596B010A45C501F05A1BE9CE412138AB82498900 (void);
// 0x00000466 System.Void OculusSampleFramework.TrainLocomotive::Update()
extern void TrainLocomotive_Update_mC64296C895306B462D399E315CCFE6106979396A (void);
// 0x00000467 System.Void OculusSampleFramework.TrainLocomotive::UpdatePosition()
extern void TrainLocomotive_UpdatePosition_mF409B3053EC449F8F57AB7B971A9053C642C0D1A (void);
// 0x00000468 System.Void OculusSampleFramework.TrainLocomotive::StartStopStateChanged()
extern void TrainLocomotive_StartStopStateChanged_m37D2604A94869E9ABAE686C05C06DA8039D5EAFC (void);
// 0x00000469 System.Collections.IEnumerator OculusSampleFramework.TrainLocomotive::StartStopTrain(System.Boolean)
extern void TrainLocomotive_StartStopTrain_m2983A1995E2F95A8642F03A8105C0B489F9C8037 (void);
// 0x0000046A System.Single OculusSampleFramework.TrainLocomotive::PlayEngineSound(OculusSampleFramework.TrainLocomotive/EngineSoundState)
extern void TrainLocomotive_PlayEngineSound_mBBE7F6F9988F707FFC60EE195E18C7C7C37C05CE (void);
// 0x0000046B System.Void OculusSampleFramework.TrainLocomotive::UpdateDistance()
extern void TrainLocomotive_UpdateDistance_m18B8F2E7593CBA725B2A7B5034AF46279D35A321 (void);
// 0x0000046C System.Void OculusSampleFramework.TrainLocomotive::DecreaseSpeedStateChanged()
extern void TrainLocomotive_DecreaseSpeedStateChanged_m58EE2750F1CC037AB7C44A209C5D7A3D6FDCB9A1 (void);
// 0x0000046D System.Void OculusSampleFramework.TrainLocomotive::IncreaseSpeedStateChanged()
extern void TrainLocomotive_IncreaseSpeedStateChanged_m4DEB392A7FA443870D2A849A663EE6E3E740A22B (void);
// 0x0000046E System.Void OculusSampleFramework.TrainLocomotive::UpdateSmokeEmissionBasedOnSpeed()
extern void TrainLocomotive_UpdateSmokeEmissionBasedOnSpeed_mABC70306F1FBF3D00E0BA0901760D44793F9D3EF (void);
// 0x0000046F System.Single OculusSampleFramework.TrainLocomotive::GetCurrentSmokeEmission()
extern void TrainLocomotive_GetCurrentSmokeEmission_mCBF588B02E4B7CB77E4A4EEE98FA557C6C09A267 (void);
// 0x00000470 System.Void OculusSampleFramework.TrainLocomotive::SmokeButtonStateChanged()
extern void TrainLocomotive_SmokeButtonStateChanged_m2E6E404156BF780272ED69C4A2D7D07FF94E01EC (void);
// 0x00000471 System.Void OculusSampleFramework.TrainLocomotive::WhistleButtonStateChanged()
extern void TrainLocomotive_WhistleButtonStateChanged_m2B8FF22070F53567B9DB0EF8F021411531CCF240 (void);
// 0x00000472 System.Void OculusSampleFramework.TrainLocomotive::ReverseButtonStateChanged()
extern void TrainLocomotive_ReverseButtonStateChanged_mDC7C10B6A595AABCF8351143917C1CDCFEA74A98 (void);
// 0x00000473 System.Void OculusSampleFramework.TrainLocomotive::.ctor()
extern void TrainLocomotive__ctor_mF249EAA95B7C51F5D9493892D960C0E8460AF909 (void);
// 0x00000474 System.Void OculusSampleFramework.TrainLocomotive/<StartStopTrain>d__34::.ctor(System.Int32)
extern void U3CStartStopTrainU3Ed__34__ctor_m39BEC907A4658AB1DC65D214A86D35F6E246795A (void);
// 0x00000475 System.Void OculusSampleFramework.TrainLocomotive/<StartStopTrain>d__34::System.IDisposable.Dispose()
extern void U3CStartStopTrainU3Ed__34_System_IDisposable_Dispose_m6A0A898733F46F849ACEF0984B4C8938E5A88B20 (void);
// 0x00000476 System.Boolean OculusSampleFramework.TrainLocomotive/<StartStopTrain>d__34::MoveNext()
extern void U3CStartStopTrainU3Ed__34_MoveNext_m02D96704D3CA82F8BECFC12A487D9C4AC7903FA1 (void);
// 0x00000477 System.Object OculusSampleFramework.TrainLocomotive/<StartStopTrain>d__34::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartStopTrainU3Ed__34_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m26D7E65A695D870236E43D9C2E2D46D88EA33065 (void);
// 0x00000478 System.Void OculusSampleFramework.TrainLocomotive/<StartStopTrain>d__34::System.Collections.IEnumerator.Reset()
extern void U3CStartStopTrainU3Ed__34_System_Collections_IEnumerator_Reset_mE5B9500E4605106C99B8718D3105A15B0CB73A87 (void);
// 0x00000479 System.Object OculusSampleFramework.TrainLocomotive/<StartStopTrain>d__34::System.Collections.IEnumerator.get_Current()
extern void U3CStartStopTrainU3Ed__34_System_Collections_IEnumerator_get_Current_mEF6808D31C6E8D74E845C551B8618F3902FC469E (void);
// 0x0000047A System.Single OculusSampleFramework.TrainTrack::get_TrackLength()
extern void TrainTrack_get_TrackLength_m2527E507114E21D65C843667ECF4F42CBA3F7FCA (void);
// 0x0000047B System.Void OculusSampleFramework.TrainTrack::set_TrackLength(System.Single)
extern void TrainTrack_set_TrackLength_m32ED334511B32330D895514C07B3BED3381F02E5 (void);
// 0x0000047C System.Void OculusSampleFramework.TrainTrack::Awake()
extern void TrainTrack_Awake_mF48A851C3560D8D9ADA00466789E9571E49047B4 (void);
// 0x0000047D OculusSampleFramework.TrackSegment OculusSampleFramework.TrainTrack::GetSegment(System.Single)
extern void TrainTrack_GetSegment_m357BC8C004D29CAD4A3CA54E7896EB1A9C7F6B4D (void);
// 0x0000047E System.Void OculusSampleFramework.TrainTrack::Regenerate()
extern void TrainTrack_Regenerate_m69F9C3D51A88D5FC844056750E4B233C06B427D6 (void);
// 0x0000047F System.Void OculusSampleFramework.TrainTrack::SetScale(System.Single)
extern void TrainTrack_SetScale_mA57F06F4C3421BB4EB73D3311A661210F8FE7416 (void);
// 0x00000480 System.Void OculusSampleFramework.TrainTrack::.ctor()
extern void TrainTrack__ctor_mB9A2233319F4A20CD3719E44B1A65AF41FA2CB38 (void);
// 0x00000481 System.Void OculusSampleFramework.Pose::.ctor()
extern void Pose__ctor_mB4BBD3D8EA8C633539F804F43FB086FB99C168E2 (void);
// 0x00000482 System.Void OculusSampleFramework.Pose::.ctor(UnityEngine.Vector3,UnityEngine.Quaternion)
extern void Pose__ctor_m8C52CA7D6BF59F3F82C09B76750DE7AF776F06C5 (void);
// 0x00000483 System.Boolean OculusSampleFramework.WindmillBladesController::get_IsMoving()
extern void WindmillBladesController_get_IsMoving_m5B2902A40908BBD84B5478EDA121ED00DBDECF56 (void);
// 0x00000484 System.Void OculusSampleFramework.WindmillBladesController::set_IsMoving(System.Boolean)
extern void WindmillBladesController_set_IsMoving_mAFFB89C8CAA4C34CA91560EE2188E80B3B66897A (void);
// 0x00000485 System.Void OculusSampleFramework.WindmillBladesController::Start()
extern void WindmillBladesController_Start_m68D68EA86272066DDC385AF8B8FDC2307AE6D4CA (void);
// 0x00000486 System.Void OculusSampleFramework.WindmillBladesController::Update()
extern void WindmillBladesController_Update_m6D72C558EA8D91B66F86A4B977F32B78C6A19C9A (void);
// 0x00000487 System.Void OculusSampleFramework.WindmillBladesController::SetMoveState(System.Boolean,System.Single)
extern void WindmillBladesController_SetMoveState_m7E9FF42447DFDD4DC710B07309C4ECB6A9027B3F (void);
// 0x00000488 System.Collections.IEnumerator OculusSampleFramework.WindmillBladesController::LerpToSpeed(System.Single)
extern void WindmillBladesController_LerpToSpeed_mEC59BAB1ADEBDA2D4051ADE4DF879F255B80C50A (void);
// 0x00000489 System.Collections.IEnumerator OculusSampleFramework.WindmillBladesController::PlaySoundDelayed(UnityEngine.AudioClip,UnityEngine.AudioClip,System.Single)
extern void WindmillBladesController_PlaySoundDelayed_m5C9465C96F6157B1A20D4F7F5A7D4F307D10D0FA (void);
// 0x0000048A System.Void OculusSampleFramework.WindmillBladesController::PlaySound(UnityEngine.AudioClip,System.Boolean)
extern void WindmillBladesController_PlaySound_mBFB8F38297A3551225D2E57F4285B58E9EF1EA63 (void);
// 0x0000048B System.Void OculusSampleFramework.WindmillBladesController::.ctor()
extern void WindmillBladesController__ctor_m9ACCD7787AFD75BF05A2D1282EAC87C3E8EFAF50 (void);
// 0x0000048C System.Void OculusSampleFramework.WindmillBladesController/<LerpToSpeed>d__17::.ctor(System.Int32)
extern void U3CLerpToSpeedU3Ed__17__ctor_mC566935DE291006EA7DBEFE0365E202472FD9E7B (void);
// 0x0000048D System.Void OculusSampleFramework.WindmillBladesController/<LerpToSpeed>d__17::System.IDisposable.Dispose()
extern void U3CLerpToSpeedU3Ed__17_System_IDisposable_Dispose_m47D668CCF11A5E20E41EE17981808A194A891934 (void);
// 0x0000048E System.Boolean OculusSampleFramework.WindmillBladesController/<LerpToSpeed>d__17::MoveNext()
extern void U3CLerpToSpeedU3Ed__17_MoveNext_mEA106649270155724FF7B4F3557200EB0C46C649 (void);
// 0x0000048F System.Object OculusSampleFramework.WindmillBladesController/<LerpToSpeed>d__17::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLerpToSpeedU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0BCE854267BF1FBEBD932AD293ECE426245E39C3 (void);
// 0x00000490 System.Void OculusSampleFramework.WindmillBladesController/<LerpToSpeed>d__17::System.Collections.IEnumerator.Reset()
extern void U3CLerpToSpeedU3Ed__17_System_Collections_IEnumerator_Reset_mDB45CD8EC190F294BD562D4090ADB533E5EBF9F7 (void);
// 0x00000491 System.Object OculusSampleFramework.WindmillBladesController/<LerpToSpeed>d__17::System.Collections.IEnumerator.get_Current()
extern void U3CLerpToSpeedU3Ed__17_System_Collections_IEnumerator_get_Current_m81542027D8F2618D5B750EF7344E51128D3A6EA3 (void);
// 0x00000492 System.Void OculusSampleFramework.WindmillBladesController/<PlaySoundDelayed>d__18::.ctor(System.Int32)
extern void U3CPlaySoundDelayedU3Ed__18__ctor_m43BFCD0BBEAFA46AF3EC55B65A2F55BF43541166 (void);
// 0x00000493 System.Void OculusSampleFramework.WindmillBladesController/<PlaySoundDelayed>d__18::System.IDisposable.Dispose()
extern void U3CPlaySoundDelayedU3Ed__18_System_IDisposable_Dispose_m3978D8C006709205406E26F305AAD051EDA0CB61 (void);
// 0x00000494 System.Boolean OculusSampleFramework.WindmillBladesController/<PlaySoundDelayed>d__18::MoveNext()
extern void U3CPlaySoundDelayedU3Ed__18_MoveNext_m5CE5E650B1870D226841E3D4EA3A269EC73C7493 (void);
// 0x00000495 System.Object OculusSampleFramework.WindmillBladesController/<PlaySoundDelayed>d__18::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CPlaySoundDelayedU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6DE8A8951E14A7AB0C941F512740278C4DF7841A (void);
// 0x00000496 System.Void OculusSampleFramework.WindmillBladesController/<PlaySoundDelayed>d__18::System.Collections.IEnumerator.Reset()
extern void U3CPlaySoundDelayedU3Ed__18_System_Collections_IEnumerator_Reset_mD0C04D5F54AAD444CE77C15B853BE79F5504160F (void);
// 0x00000497 System.Object OculusSampleFramework.WindmillBladesController/<PlaySoundDelayed>d__18::System.Collections.IEnumerator.get_Current()
extern void U3CPlaySoundDelayedU3Ed__18_System_Collections_IEnumerator_get_Current_mC1F5FCA59AD560DFD85357BAC5AD0581AE393182 (void);
// 0x00000498 System.Void OculusSampleFramework.WindmillController::Awake()
extern void WindmillController_Awake_m819B04397F7D5A549DB64C972D9B35A0B54DCA77 (void);
// 0x00000499 System.Void OculusSampleFramework.WindmillController::OnEnable()
extern void WindmillController_OnEnable_mD0EBB3C707F79AB7CFB88EE7CE33355A24311BD0 (void);
// 0x0000049A System.Void OculusSampleFramework.WindmillController::OnDisable()
extern void WindmillController_OnDisable_mDAF50F89AB116A19F75AD91C088CF0B58F441365 (void);
// 0x0000049B System.Void OculusSampleFramework.WindmillController::StartStopStateChanged(OculusSampleFramework.InteractableStateArgs)
extern void WindmillController_StartStopStateChanged_m662296A93D8F31FC2608D8C3E89D1E3714B9561F (void);
// 0x0000049C System.Void OculusSampleFramework.WindmillController::Update()
extern void WindmillController_Update_mA8A7025A970E66000E92C858A97B07D1A73650C2 (void);
// 0x0000049D System.Void OculusSampleFramework.WindmillController::.ctor()
extern void WindmillController__ctor_m2AE15DFF314EC3FE467821FB5F2EB1EE0F19EABC (void);
// 0x0000049E System.Void OculusSampleFramework.OVROverlaySample::Start()
extern void OVROverlaySample_Start_m4266C95062A3A7F9DB99739F13F1767E8E135CDC (void);
// 0x0000049F System.Void OculusSampleFramework.OVROverlaySample::Update()
extern void OVROverlaySample_Update_mE6B42EE1FD90945F73BB8A1E71C98D6FE45F8BAB (void);
// 0x000004A0 System.Void OculusSampleFramework.OVROverlaySample::ActivateWorldGeo()
extern void OVROverlaySample_ActivateWorldGeo_mE88CC5B6F95FF940DF64CC3E30A163299A0028AB (void);
// 0x000004A1 System.Void OculusSampleFramework.OVROverlaySample::ActivateOVROverlay()
extern void OVROverlaySample_ActivateOVROverlay_m0D8B1F2CE350FC1D6530827588176FED1E8B0E52 (void);
// 0x000004A2 System.Void OculusSampleFramework.OVROverlaySample::ActivateNone()
extern void OVROverlaySample_ActivateNone_mA8A0AC683DA78AACC6DBB04E1BFCCAA41CC83D66 (void);
// 0x000004A3 System.Void OculusSampleFramework.OVROverlaySample::TriggerLoad()
extern void OVROverlaySample_TriggerLoad_mA4868A61FDAFE908F1178A852FAAC4DA4A614B42 (void);
// 0x000004A4 System.Collections.IEnumerator OculusSampleFramework.OVROverlaySample::WaitforOVROverlay()
extern void OVROverlaySample_WaitforOVROverlay_mE2A11AA7618CAF92461A97CFFF9BF0163B14D463 (void);
// 0x000004A5 System.Void OculusSampleFramework.OVROverlaySample::TriggerUnload()
extern void OVROverlaySample_TriggerUnload_m6B1496A0AF1064FF3E131ED27D4BE41916FEAACA (void);
// 0x000004A6 System.Void OculusSampleFramework.OVROverlaySample::CameraAndRenderTargetSetup()
extern void OVROverlaySample_CameraAndRenderTargetSetup_mF5D8804C1E12AE7E92207ACE4AE38C9C4B12005B (void);
// 0x000004A7 System.Void OculusSampleFramework.OVROverlaySample::SimulateLevelLoad()
extern void OVROverlaySample_SimulateLevelLoad_m5792B7A4F133CB3538E9A9CF021C1EED354E47C4 (void);
// 0x000004A8 System.Void OculusSampleFramework.OVROverlaySample::ClearObjects()
extern void OVROverlaySample_ClearObjects_m0B71842496007E962955E65310B2A4B15653E446 (void);
// 0x000004A9 System.Void OculusSampleFramework.OVROverlaySample::RadioPressed(System.String,System.String,UnityEngine.UI.Toggle)
extern void OVROverlaySample_RadioPressed_m67B72653B224FFE4E86C34D625F9B442D32BD19F (void);
// 0x000004AA System.Void OculusSampleFramework.OVROverlaySample::.ctor()
extern void OVROverlaySample__ctor_mB44CEBA69BEAF1A6A58A099C565D834246303FEC (void);
// 0x000004AB System.Void OculusSampleFramework.OVROverlaySample::<Start>b__24_0(UnityEngine.UI.Toggle)
extern void OVROverlaySample_U3CStartU3Eb__24_0_mBD67EE6633B4F08B23C48B5757BF037F011FA444 (void);
// 0x000004AC System.Void OculusSampleFramework.OVROverlaySample::<Start>b__24_1(UnityEngine.UI.Toggle)
extern void OVROverlaySample_U3CStartU3Eb__24_1_m9C707473B8AD3A3E9A2A5DE4CBB97FE7067691B9 (void);
// 0x000004AD System.Void OculusSampleFramework.OVROverlaySample::<Start>b__24_2(UnityEngine.UI.Toggle)
extern void OVROverlaySample_U3CStartU3Eb__24_2_m4676B4B0549094AF080DCCCC880399B388366734 (void);
// 0x000004AE System.Void OculusSampleFramework.OVROverlaySample/<WaitforOVROverlay>d__30::.ctor(System.Int32)
extern void U3CWaitforOVROverlayU3Ed__30__ctor_m2AB6E29E1D1F5472C16F001263DCA536B3140AB4 (void);
// 0x000004AF System.Void OculusSampleFramework.OVROverlaySample/<WaitforOVROverlay>d__30::System.IDisposable.Dispose()
extern void U3CWaitforOVROverlayU3Ed__30_System_IDisposable_Dispose_m1650460A7C7917454B04CB2BE659585291CBEA40 (void);
// 0x000004B0 System.Boolean OculusSampleFramework.OVROverlaySample/<WaitforOVROverlay>d__30::MoveNext()
extern void U3CWaitforOVROverlayU3Ed__30_MoveNext_mEC25CDDFCBF7B22EEB0CD01AE64AF805A2A11534 (void);
// 0x000004B1 System.Object OculusSampleFramework.OVROverlaySample/<WaitforOVROverlay>d__30::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitforOVROverlayU3Ed__30_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m615D0B2569205ABB59D00AC9BA50160C1554A6F2 (void);
// 0x000004B2 System.Void OculusSampleFramework.OVROverlaySample/<WaitforOVROverlay>d__30::System.Collections.IEnumerator.Reset()
extern void U3CWaitforOVROverlayU3Ed__30_System_Collections_IEnumerator_Reset_mEEB99599B3CF4FC0F90F891156B2FDE7157003F3 (void);
// 0x000004B3 System.Object OculusSampleFramework.OVROverlaySample/<WaitforOVROverlay>d__30::System.Collections.IEnumerator.get_Current()
extern void U3CWaitforOVROverlayU3Ed__30_System_Collections_IEnumerator_get_Current_mD516520BDEAB4043256A81E49B7366A842120F09 (void);
// 0x000004B4 System.Void OVRTouchSample.Hand::Awake()
extern void Hand_Awake_m8481AE5F78D80F6D46DEB65B2B9D0182A56DF09F (void);
// 0x000004B5 System.Void OVRTouchSample.Hand::Start()
extern void Hand_Start_m9705EEDEA171DF9AF0D8E107F3E1D42EF11BC79B (void);
// 0x000004B6 System.Void OVRTouchSample.Hand::OnDestroy()
extern void Hand_OnDestroy_mD3C5A20383810964D0D12F41E40C185DE7AAA046 (void);
// 0x000004B7 System.Void OVRTouchSample.Hand::Update()
extern void Hand_Update_mB561E1D8A8122B9C20C5C6A00B9A43F952A09F2A (void);
// 0x000004B8 System.Void OVRTouchSample.Hand::UpdateCapTouchStates()
extern void Hand_UpdateCapTouchStates_mD04F599C0EB3A207AE420014E51B65DA66F5A49E (void);
// 0x000004B9 System.Void OVRTouchSample.Hand::LateUpdate()
extern void Hand_LateUpdate_mA7932BE84EA2D5A09A4637502E4E52CA4321B8E3 (void);
// 0x000004BA System.Void OVRTouchSample.Hand::OnInputFocusLost()
extern void Hand_OnInputFocusLost_m8C6FE8A97BA23153CEFD5D8AB561781E43AB6E02 (void);
// 0x000004BB System.Void OVRTouchSample.Hand::OnInputFocusAcquired()
extern void Hand_OnInputFocusAcquired_m57F1BE51C79CB9B267C1866F851C75E6B848FA1C (void);
// 0x000004BC System.Single OVRTouchSample.Hand::InputValueRateChange(System.Boolean,System.Single)
extern void Hand_InputValueRateChange_m76AA4719D87E5D5C3BC14DBE98B145551F685784 (void);
// 0x000004BD System.Void OVRTouchSample.Hand::UpdateAnimStates()
extern void Hand_UpdateAnimStates_m513AAE4141F73A2CF86B65C1E5C88804AC288D6E (void);
// 0x000004BE System.Void OVRTouchSample.Hand::CollisionEnable(System.Boolean)
extern void Hand_CollisionEnable_m23E8B46BA537F57146805D86CE15089DB121554E (void);
// 0x000004BF System.Void OVRTouchSample.Hand::.ctor()
extern void Hand__ctor_m7990A68D0B224E873A13E2ABFF28659D667951AE (void);
// 0x000004C0 System.Void OVRTouchSample.Hand/<>c::.cctor()
extern void U3CU3Ec__cctor_m3FFD2D2D03A3D619CC157C8E95E60BDA7529EF17 (void);
// 0x000004C1 System.Void OVRTouchSample.Hand/<>c::.ctor()
extern void U3CU3Ec__ctor_m29502B502F048DD5DE6F011936D3ACF3E92D116E (void);
// 0x000004C2 System.Boolean OVRTouchSample.Hand/<>c::<Start>b__28_0(UnityEngine.Collider)
extern void U3CU3Ec_U3CStartU3Eb__28_0_m982333D324DA5B0E763CCC459B17E62E62201591 (void);
// 0x000004C3 System.Boolean OVRTouchSample.HandPose::get_AllowPointing()
extern void HandPose_get_AllowPointing_mC5B4C9CDBFCF4883AC67C1637C2D1FA5C5553D3D (void);
// 0x000004C4 System.Boolean OVRTouchSample.HandPose::get_AllowThumbsUp()
extern void HandPose_get_AllowThumbsUp_mAE1D1913C2FEE204F4A74A8E40135873C9DB6615 (void);
// 0x000004C5 OVRTouchSample.HandPoseId OVRTouchSample.HandPose::get_PoseId()
extern void HandPose_get_PoseId_m676CB369677442E624811201F99B12E4F5BB8F8E (void);
// 0x000004C6 System.Void OVRTouchSample.HandPose::.ctor()
extern void HandPose__ctor_mB1A903B2059D5FB6676ACBD06E475480BBC767C9 (void);
// 0x000004C7 System.Void OVRTouchSample.TouchController::Update()
extern void TouchController_Update_m19376C0269F927CC0CFD041646CEA78E2748D6C1 (void);
// 0x000004C8 System.Void OVRTouchSample.TouchController::OnInputFocusLost()
extern void TouchController_OnInputFocusLost_m09A7F448B045C56328B5E92B8DE149BA7ADA065A (void);
// 0x000004C9 System.Void OVRTouchSample.TouchController::OnInputFocusAcquired()
extern void TouchController_OnInputFocusAcquired_m7B23CB3B2CAD8C70AE1D8711D16F06F3E9ADA4A9 (void);
// 0x000004CA System.Void OVRTouchSample.TouchController::.ctor()
extern void TouchController__ctor_mBE4D0026D782C0768D1B286B617197245B28D9C6 (void);
// 0x000004CB BNG.HandPoseDefinition BNG.AutoPoser::get_CollisionPose()
extern void AutoPoser_get_CollisionPose_m53A195A5D0B58CDB348FE0200B5416094DE61D5B (void);
// 0x000004CC System.Boolean BNG.AutoPoser::get_CollisionDetected()
extern void AutoPoser_get_CollisionDetected_m424853DE983C16E5CD7C404BDEC681D6BD381081 (void);
// 0x000004CD System.Void BNG.AutoPoser::Start()
extern void AutoPoser_Start_mD37529D47F7AA218149B6608A0D449D909C86917 (void);
// 0x000004CE System.Void BNG.AutoPoser::OnEnable()
extern void AutoPoser_OnEnable_mE2DF53F8FAC08F26595885BABB62F43B9462F29B (void);
// 0x000004CF System.Void BNG.AutoPoser::Update()
extern void AutoPoser_Update_mB85BAD41EB82143187912F99B454AD4AEE76BBA3 (void);
// 0x000004D0 System.Void BNG.AutoPoser::UpdateAutoPose(System.Boolean)
extern void AutoPoser_UpdateAutoPose_mF7EC9331D7010E9283FB6C029819A034EFBBEC43 (void);
// 0x000004D1 System.Void BNG.AutoPoser::UpdateAutoPoseOnce()
extern void AutoPoser_UpdateAutoPoseOnce_m8638729C9E7321C8730951BC9BAA59AD3803BB53 (void);
// 0x000004D2 System.Collections.IEnumerator BNG.AutoPoser::updateAutoPoseRoutine()
extern void AutoPoser_updateAutoPoseRoutine_m90FBA7BF00E27A385862143CF3E5E0195F3667C0 (void);
// 0x000004D3 BNG.HandPoseDefinition BNG.AutoPoser::GetAutoPose()
extern void AutoPoser_GetAutoPose_m01AF7D57616463410EC9CC162F16312224087C83 (void);
// 0x000004D4 BNG.HandPoseDefinition BNG.AutoPoser::CopyHandDefinition(BNG.HandPoseDefinition)
extern void AutoPoser_CopyHandDefinition_mD0FFB3774A76C0DD2AB4D59FB4AA5664194B6386 (void);
// 0x000004D5 BNG.FingerJoint BNG.AutoPoser::GetJointCopy(BNG.FingerJoint)
extern void AutoPoser_GetJointCopy_m9B32137BD666D08178734AE797125BD8F042287B (void);
// 0x000004D6 System.Collections.Generic.List`1<BNG.FingerJoint> BNG.AutoPoser::GetJointsCopy(System.Collections.Generic.List`1<BNG.FingerJoint>)
extern void AutoPoser_GetJointsCopy_m97D08F63064537F4C6E746380A79809710EB2A54 (void);
// 0x000004D7 System.Boolean BNG.AutoPoser::GetThumbHit(BNG.HandPoser)
extern void AutoPoser_GetThumbHit_mB2CD477DB4C47A67428D0A9D704A528567E25E10 (void);
// 0x000004D8 System.Boolean BNG.AutoPoser::GetIndexHit(BNG.HandPoser)
extern void AutoPoser_GetIndexHit_m345CDA7E370AD0DB7F70D525ED49C2913BE544EE (void);
// 0x000004D9 System.Boolean BNG.AutoPoser::GetMiddleHit(BNG.HandPoser)
extern void AutoPoser_GetMiddleHit_mC5793AAF881FC477BAE9EC36E39D1E064540B4B8 (void);
// 0x000004DA System.Boolean BNG.AutoPoser::GetRingHit(BNG.HandPoser)
extern void AutoPoser_GetRingHit_mED67C354CEAF74C80303807A833D0D7214138490 (void);
// 0x000004DB System.Boolean BNG.AutoPoser::GetPinkyHit(BNG.HandPoser)
extern void AutoPoser_GetPinkyHit_mDC40E961F3AB4C575BA2EA34E7F6E019CE5159B6 (void);
// 0x000004DC System.Boolean BNG.AutoPoser::LoopThroughJoints(System.Collections.Generic.List`1<UnityEngine.Transform>,System.Collections.Generic.List`1<BNG.FingerJoint>,UnityEngine.Vector3,System.Single)
extern void AutoPoser_LoopThroughJoints_mC5731F18648BD721E868E1082E3B5161CEECFFC1 (void);
// 0x000004DD System.Boolean BNG.AutoPoser::IsValidCollision(UnityEngine.Collider)
extern void AutoPoser_IsValidCollision_mE2562E3EFF79CE3DB93FFF23837B9C8EE156FD7A (void);
// 0x000004DE System.Void BNG.AutoPoser::OnDrawGizmos()
extern void AutoPoser_OnDrawGizmos_m91B3A9116E5346D42960108AEC8D05F354DC545D (void);
// 0x000004DF System.Void BNG.AutoPoser::DrawJointGizmo(BNG.FingerTipCollider,UnityEngine.Vector3,System.Single,BNG.GizmoDisplayType)
extern void AutoPoser_DrawJointGizmo_m0D40E5CAB74C54C8394AF1D1C6D06BB916F2873E (void);
// 0x000004E0 System.Void BNG.AutoPoser::.ctor()
extern void AutoPoser__ctor_m869B97BFF9C19D9EAA1F2032E9A56EC587C2B7A0 (void);
// 0x000004E1 System.Void BNG.AutoPoser/<updateAutoPoseRoutine>d__33::.ctor(System.Int32)
extern void U3CupdateAutoPoseRoutineU3Ed__33__ctor_m6A2C843FB040DE88C7DC34B74B092A2A4D31DA49 (void);
// 0x000004E2 System.Void BNG.AutoPoser/<updateAutoPoseRoutine>d__33::System.IDisposable.Dispose()
extern void U3CupdateAutoPoseRoutineU3Ed__33_System_IDisposable_Dispose_m2E0280435EEF17ADACF1570ABB3638E1B46FB85B (void);
// 0x000004E3 System.Boolean BNG.AutoPoser/<updateAutoPoseRoutine>d__33::MoveNext()
extern void U3CupdateAutoPoseRoutineU3Ed__33_MoveNext_m53CCCC2CC2437DBCAB34374637472C5A40D79B67 (void);
// 0x000004E4 System.Object BNG.AutoPoser/<updateAutoPoseRoutine>d__33::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CupdateAutoPoseRoutineU3Ed__33_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0CEAF8D9EBCE439B0014B0F69C4435CA7288AA62 (void);
// 0x000004E5 System.Void BNG.AutoPoser/<updateAutoPoseRoutine>d__33::System.Collections.IEnumerator.Reset()
extern void U3CupdateAutoPoseRoutineU3Ed__33_System_Collections_IEnumerator_Reset_m6B038D82610F656F6095BD1DF732303C08126225 (void);
// 0x000004E6 System.Object BNG.AutoPoser/<updateAutoPoseRoutine>d__33::System.Collections.IEnumerator.get_Current()
extern void U3CupdateAutoPoseRoutineU3Ed__33_System_Collections_IEnumerator_get_Current_m83D4722A893B3775119B737FE11E6AFBAC6669B7 (void);
// 0x000004E7 System.Void BNG.BoneMapping::Update()
extern void BoneMapping_Update_m62C65EDB90283A1A7946B299905631A28E29FE9E (void);
// 0x000004E8 System.Void BNG.BoneMapping::OnDrawGizmos()
extern void BoneMapping_OnDrawGizmos_mD6CB298C3E743BF011CE2D1E15E7B64645FF607F (void);
// 0x000004E9 System.Void BNG.BoneMapping::.ctor()
extern void BoneMapping__ctor_m6B126A1C1CEBF09599FC858540461818BA5E8717 (void);
// 0x000004EA System.Void BNG.BoneObject::.ctor()
extern void BoneObject__ctor_mDB4F39B60A78703F790F9F68E3DDC5EB925D7EA6 (void);
// 0x000004EB System.Void BNG.EditorHandle::.ctor()
extern void EditorHandle__ctor_mBC7223D07F9876479E16B5296CC15431884B1D4A (void);
// 0x000004EC System.Void BNG.FingerJoint::.ctor()
extern void FingerJoint__ctor_m68BA194D4A79AEC18E06441DF6A013FC99D5812F (void);
// 0x000004ED System.Void BNG.FingerTipCollider::.ctor()
extern void FingerTipCollider__ctor_mAD998E6EB83269EC40237F377F2857A4A76A8FE3 (void);
// 0x000004EE System.Void BNG.HandPose::.ctor()
extern void HandPose__ctor_mFEBC46B97C4BE59528B0929173223F6ED3BDB38A (void);
// 0x000004EF System.Void BNG.HandPoseBlender::Start()
extern void HandPoseBlender_Start_mFD87E62D6E49CB82489517925312A8C7FB3AC479 (void);
// 0x000004F0 System.Void BNG.HandPoseBlender::Update()
extern void HandPoseBlender_Update_m8A82E3CBC6D23931CAF57F9360C744BC18BB4B66 (void);
// 0x000004F1 System.Void BNG.HandPoseBlender::UpdatePoseFromInputs()
extern void HandPoseBlender_UpdatePoseFromInputs_m59DE35BF96FBC0DBC3CDF90B9F48AEEDB6BAA8ED (void);
// 0x000004F2 System.Void BNG.HandPoseBlender::UpdateThumb(System.Single)
extern void HandPoseBlender_UpdateThumb_mCD192CF78255958BDCFABD83FAC43EF27345F4A5 (void);
// 0x000004F3 System.Void BNG.HandPoseBlender::UpdateIndex(System.Single)
extern void HandPoseBlender_UpdateIndex_m0280E5CA85ADEFB422F58CDFD105098C523CFA3F (void);
// 0x000004F4 System.Void BNG.HandPoseBlender::UpdateMiddle(System.Single)
extern void HandPoseBlender_UpdateMiddle_m0B4E71E3B771B6D5BFA3FE3DF01215DF8F6BA056 (void);
// 0x000004F5 System.Void BNG.HandPoseBlender::UpdateRing(System.Single)
extern void HandPoseBlender_UpdateRing_m85BD2A963EE9DFB486B94C1862F85F26DB7F5D01 (void);
// 0x000004F6 System.Void BNG.HandPoseBlender::UpdatePinky(System.Single)
extern void HandPoseBlender_UpdatePinky_mD3D53BEB5388F660954882DC8FE5F307129A48D1 (void);
// 0x000004F7 System.Void BNG.HandPoseBlender::UpdateGrip(System.Single)
extern void HandPoseBlender_UpdateGrip_m9DC2238312613CF966915B5325734E9966186977 (void);
// 0x000004F8 System.Void BNG.HandPoseBlender::DoIdleBlendPose()
extern void HandPoseBlender_DoIdleBlendPose_m12E61753A8C6F1F3D3AA1E81501B86D206F7A70E (void);
// 0x000004F9 System.Void BNG.HandPoseBlender::.ctor()
extern void HandPoseBlender__ctor_mA4A84706694A17B391A75ED34090F5F97857F55B (void);
// 0x000004FA System.Void BNG.HandPoseDefinition::.ctor()
extern void HandPoseDefinition__ctor_m91C4BD177705AB3B7342B2BC748CD25ED9BF6335 (void);
// 0x000004FB BNG.HandPoseDefinition BNG.HandPoser::get_HandPoseJoints()
extern void HandPoser_get_HandPoseJoints_m2AEC7F46DF233F5DB97D87A577DC45B9F3A560C1 (void);
// 0x000004FC System.Void BNG.HandPoser::Start()
extern void HandPoser_Start_m1083EE1A44356497AF777C1BA1EC020C9F462F5D (void);
// 0x000004FD System.Void BNG.HandPoser::Update()
extern void HandPoser_Update_mE51880905F74E541D4F96057488E2C424E74BDF1 (void);
// 0x000004FE System.Void BNG.HandPoser::CheckForPoseChange()
extern void HandPoser_CheckForPoseChange_mC3F6615E1D5C6808E57C194B32E20FBB97D68491 (void);
// 0x000004FF System.Void BNG.HandPoser::OnPoseChanged()
extern void HandPoser_OnPoseChanged_mA37AA139B5B13CBA4566A91D81379F98527228AA (void);
// 0x00000500 BNG.FingerJoint BNG.HandPoser::GetWristJoint()
extern void HandPoser_GetWristJoint_m3BB33DBEA69E743E2D91EA1A1D168EEB4D40A8D1 (void);
// 0x00000501 System.Collections.Generic.List`1<BNG.FingerJoint> BNG.HandPoser::GetThumbJoints()
extern void HandPoser_GetThumbJoints_mD3425A5A39218F3B48C7570827E6318AEE9F341C (void);
// 0x00000502 System.Collections.Generic.List`1<BNG.FingerJoint> BNG.HandPoser::GetIndexJoints()
extern void HandPoser_GetIndexJoints_m081CD820EEB398FBE020B20480CE9BD290F7C685 (void);
// 0x00000503 System.Collections.Generic.List`1<BNG.FingerJoint> BNG.HandPoser::GetMiddleJoints()
extern void HandPoser_GetMiddleJoints_m22619BAC6787D560B491648811A1D96252C9FFED (void);
// 0x00000504 System.Collections.Generic.List`1<BNG.FingerJoint> BNG.HandPoser::GetRingJoints()
extern void HandPoser_GetRingJoints_m16FAEFD13CC19EB7081B2333E7D8A9BD89580D37 (void);
// 0x00000505 System.Collections.Generic.List`1<BNG.FingerJoint> BNG.HandPoser::GetPinkyJoints()
extern void HandPoser_GetPinkyJoints_mA86965AC39E8C5D454E0CC16CA25AE635A32B4A5 (void);
// 0x00000506 System.Collections.Generic.List`1<BNG.FingerJoint> BNG.HandPoser::GetOtherJoints()
extern void HandPoser_GetOtherJoints_mB512FF86BE2D716A0C4ABCE4D96AEA91B881E5D8 (void);
// 0x00000507 System.Int32 BNG.HandPoser::GetTotalJointsCount()
extern void HandPoser_GetTotalJointsCount_m5F05599245BEAB393CC583AD84D3DC25BBA6BDF6 (void);
// 0x00000508 UnityEngine.Transform BNG.HandPoser::GetTip(System.Collections.Generic.List`1<UnityEngine.Transform>)
extern void HandPoser_GetTip_mD2AD11303DFA081C63E2645E360D5B89A1A6DB95 (void);
// 0x00000509 UnityEngine.Transform BNG.HandPoser::GetThumbTip()
extern void HandPoser_GetThumbTip_mCE3E46AAA429DAD4C18F4D2AFC097F961011E8E1 (void);
// 0x0000050A UnityEngine.Transform BNG.HandPoser::GetIndexTip()
extern void HandPoser_GetIndexTip_m8DC3E563AA6BE09B3AA97626763200B4163D7772 (void);
// 0x0000050B UnityEngine.Transform BNG.HandPoser::GetMiddleTip()
extern void HandPoser_GetMiddleTip_m8E9252264A2E11DFF24122B0C051B3E45CCD32B9 (void);
// 0x0000050C UnityEngine.Transform BNG.HandPoser::GetRingTip()
extern void HandPoser_GetRingTip_mC76ACCEDE23BC94AB7A90F8D5F37AF1F039102B6 (void);
// 0x0000050D UnityEngine.Transform BNG.HandPoser::GetPinkyTip()
extern void HandPoser_GetPinkyTip_m465FB6E50470882B3CFEC563B949E500A8C99CB8 (void);
// 0x0000050E UnityEngine.Vector3 BNG.HandPoser::GetFingerTipPositionWithOffset(System.Collections.Generic.List`1<UnityEngine.Transform>,System.Single)
extern void HandPoser_GetFingerTipPositionWithOffset_m3080C5F3083D5BF593FA9CEF4ED4392D72BE9BBD (void);
// 0x0000050F System.Collections.Generic.List`1<BNG.FingerJoint> BNG.HandPoser::GetJointsFromTransforms(System.Collections.Generic.List`1<UnityEngine.Transform>)
extern void HandPoser_GetJointsFromTransforms_m4B9816B8ED419215CB5B5A6407452C1F886DE510 (void);
// 0x00000510 BNG.FingerJoint BNG.HandPoser::GetJointFromTransform(UnityEngine.Transform)
extern void HandPoser_GetJointFromTransform_m83459A9B11F903C94FB38FE578ACEF667317525B (void);
// 0x00000511 System.Void BNG.HandPoser::UpdateHandPose(BNG.HandPose,System.Boolean)
extern void HandPoser_UpdateHandPose_m5E9BDBA4F0600FF48D962B0B4B5C493EDDEED4FB (void);
// 0x00000512 System.Void BNG.HandPoser::UpdateHandPose(BNG.HandPoseDefinition,System.Boolean)
extern void HandPoser_UpdateHandPose_m64038681DB9E103EC8D6538EB1AD06A891C0344F (void);
// 0x00000513 System.Void BNG.HandPoser::UpdateJoint(BNG.FingerJoint,UnityEngine.Transform,System.Boolean,System.Boolean,System.Boolean)
extern void HandPoser_UpdateJoint_m4E436DBA91076C507B1263CA01EEF8D0FA72605A (void);
// 0x00000514 System.Void BNG.HandPoser::UpdateJoint(BNG.FingerJoint,UnityEngine.Transform,System.Single,System.Boolean,System.Boolean)
extern void HandPoser_UpdateJoint_mD59EFF395E575925A85D779F282D02BAA13B15BB (void);
// 0x00000515 System.Void BNG.HandPoser::UpdateJoints(System.Collections.Generic.List`1<BNG.FingerJoint>,System.Collections.Generic.List`1<UnityEngine.Transform>,System.Boolean)
extern void HandPoser_UpdateJoints_mA2D418B4617B2414FA30604A8BEDA1A82F02F188 (void);
// 0x00000516 System.Void BNG.HandPoser::UpdateJoints(System.Collections.Generic.List`1<BNG.FingerJoint>,System.Collections.Generic.List`1<UnityEngine.Transform>,System.Single)
extern void HandPoser_UpdateJoints_mD32B654D3CCBF48FCE706D4318BD46E1A02D81A2 (void);
// 0x00000517 BNG.HandPoseDefinition BNG.HandPoser::GetHandPoseDefinition()
extern void HandPoser_GetHandPoseDefinition_m998DB64ED81202AF9B60722E2A5C27395BE22D07 (void);
// 0x00000518 System.Void BNG.HandPoser::SavePoseAsScriptablObject(System.String)
extern void HandPoser_SavePoseAsScriptablObject_mF758F6A24C4F214774C1BC6CC1DB054A6D22B5D7 (void);
// 0x00000519 System.Void BNG.HandPoser::CreateUniquePose(System.String)
extern void HandPoser_CreateUniquePose_mCDDA53EB42E6F945764B472EFE4FF69B5C6A8B07 (void);
// 0x0000051A BNG.HandPose BNG.HandPoser::GetHandPoseScriptableObject()
extern void HandPoser_GetHandPoseScriptableObject_mD9B6EB6AC2AE98C46EEC699481010702EA01AAAE (void);
// 0x0000051B System.Void BNG.HandPoser::DoPoseUpdate()
extern void HandPoser_DoPoseUpdate_mD07B38F910DBA27226017B6410D5697D31C028D2 (void);
// 0x0000051C System.Void BNG.HandPoser::ResetEditorHandles()
extern void HandPoser_ResetEditorHandles_m9CD0A2A5AAE9BE066B26E65ED38621A7C658B2F5 (void);
// 0x0000051D System.Void BNG.HandPoser::OnDrawGizmos()
extern void HandPoser_OnDrawGizmos_mFAECD5070B7D5C96302B2A505384B85B236B7B3C (void);
// 0x0000051E System.Void BNG.HandPoser::.ctor()
extern void HandPoser__ctor_m7AC615054284FACA9252F9D75E3252375E6C6254 (void);
// 0x0000051F System.Void BNG.PoseableObject::.ctor()
extern void PoseableObject__ctor_mA9A89DA02C7AA1D38EC7D301C7F483F74FF20338 (void);
// 0x00000520 System.Void BNG.SampleHandController::Start()
extern void SampleHandController_Start_m2B5985694869C5081D8581218E8B35F434062903 (void);
// 0x00000521 System.Void BNG.SampleHandController::Update()
extern void SampleHandController_Update_m33CA5B88637EAF7ED8B6EAD4C96B268ACBC3A4D8 (void);
// 0x00000522 System.Void BNG.SampleHandController::DoHandControllerUpdate()
extern void SampleHandController_DoHandControllerUpdate_m244DD8CA7C49574918D2A374322432719188CEC5 (void);
// 0x00000523 System.Void BNG.SampleHandController::SetCurrentlyHeldObject(UnityEngine.GameObject)
extern void SampleHandController_SetCurrentlyHeldObject_m96212353322C65C6B8E3C0D8B6DED53F61B89AE6 (void);
// 0x00000524 System.Void BNG.SampleHandController::ClearCurrentlyHeldObject()
extern void SampleHandController_ClearCurrentlyHeldObject_m3C85CDA0C04F3A2C79CA4FC35127E652183CB65F (void);
// 0x00000525 System.Void BNG.SampleHandController::ResetToIdleComponents()
extern void SampleHandController_ResetToIdleComponents_m1634EAC6DC9B72E3C58972A09BDBDC36980F94EA (void);
// 0x00000526 System.Void BNG.SampleHandController::UpdateFingerInputs()
extern void SampleHandController_UpdateFingerInputs_m9B554A20FE3A026CDF865D2EEF41E1D7BE7E86B0 (void);
// 0x00000527 System.Void BNG.SampleHandController::DoHeldItemPose()
extern void SampleHandController_DoHeldItemPose_m00DF00DD6B8E8BE72FDEF10B84AF26D4504EDB2C (void);
// 0x00000528 System.Void BNG.SampleHandController::DisableContinousAutoPose()
extern void SampleHandController_DisableContinousAutoPose_m284A4361441A1B6DB379BB4FC13A2426DCC5F3EC (void);
// 0x00000529 System.Void BNG.SampleHandController::DoIdlePose()
extern void SampleHandController_DoIdlePose_mEC7ECA71D1A28CFAD86CCC19F8E6DF0AB9BADB9F (void);
// 0x0000052A System.Boolean BNG.SampleHandController::HoldingObject()
extern void SampleHandController_HoldingObject_mDF42B5805BAF61B220CEE3343184CC26A3C3A4E5 (void);
// 0x0000052B System.Single BNG.SampleHandController::correctValue(System.Single)
extern void SampleHandController_correctValue_mABC1B5B250BA7ADABCBA5996C6CBEBCF3D7B5C06 (void);
// 0x0000052C System.Void BNG.SampleHandController::UpdateXRDevices()
extern void SampleHandController_UpdateXRDevices_mDE680814E4721D1BA36BFB81C0140477F164DD4A (void);
// 0x0000052D System.Single BNG.SampleHandController::getFeatureUsage(UnityEngine.XR.InputDevice,UnityEngine.XR.InputFeatureUsage`1<System.Single>)
extern void SampleHandController_getFeatureUsage_m523F7E748928E93D9E7421241B10BBEDAD8D34B4 (void);
// 0x0000052E System.Boolean BNG.SampleHandController::getFeatureUsage(UnityEngine.XR.InputDevice,UnityEngine.XR.InputFeatureUsage`1<System.Boolean>)
extern void SampleHandController_getFeatureUsage_mC75FE44A601F3469D82D5FBF9647EEC44DBC20FA (void);
// 0x0000052F UnityEngine.XR.InputDevice BNG.SampleHandController::GetLeftController()
extern void SampleHandController_GetLeftController_m51E0B51C2D8DB22740585B5B8A2CF57B17BDDE58 (void);
// 0x00000530 UnityEngine.XR.InputDevice BNG.SampleHandController::GetRightController()
extern void SampleHandController_GetRightController_mB1265E5B8005D8E30B7169E0E34B9762626C2806 (void);
// 0x00000531 System.Boolean BNG.SampleHandController::GetThumbIsNear()
extern void SampleHandController_GetThumbIsNear_m312D69816A3A974FE9026A97997FDE9BC7AED500 (void);
// 0x00000532 System.Boolean BNG.SampleHandController::GetIndexIsNear()
extern void SampleHandController_GetIndexIsNear_m3D1031451BAD7186ABE82E3028B53616934D7AB5 (void);
// 0x00000533 System.Void BNG.SampleHandController::.ctor()
extern void SampleHandController__ctor_m3844E5E50DC2C3F20527154F7C761EAFD0C515FC (void);
// 0x00000534 System.Void BNG.SampleHandController::.cctor()
extern void SampleHandController__cctor_m8811E6DA2E421377D63D1E3100C361351839F413 (void);
// 0x00000535 System.Void BNG.SavePoseBinding::Start()
extern void SavePoseBinding_Start_m60D6F61483BCDCCE44F918E5361D9D06E268E9DA (void);
// 0x00000536 System.Void BNG.SavePoseBinding::Update()
extern void SavePoseBinding_Update_m3634087E4371BCEDF46A606ED16A5152ABF26E5B (void);
// 0x00000537 System.Void BNG.SavePoseBinding::OnGUI()
extern void SavePoseBinding_OnGUI_m159E4108E69DE35C9F5431C6B0A6B93D7003E39F (void);
// 0x00000538 System.Void BNG.SavePoseBinding::.ctor()
extern void SavePoseBinding__ctor_mC93F70B13598A47A8322A5C29DF54602A3B8CE6D (void);
// 0x00000539 System.Void BNG.SkeletonVisualizer::OnApplicationQuit()
extern void SkeletonVisualizer_OnApplicationQuit_mC23E40BDCB8522A0D3BCFF178324C99E55FD9ABB (void);
// 0x0000053A System.Void BNG.SkeletonVisualizer::OnDestroy()
extern void SkeletonVisualizer_OnDestroy_mB1D7B5369871A5FE34984D164633168017D02845 (void);
// 0x0000053B System.Boolean BNG.SkeletonVisualizer::IsTipOfBone(UnityEngine.Transform)
extern void SkeletonVisualizer_IsTipOfBone_m5BBBD86531D02365A975283B7FBC0AFFCD2C5597 (void);
// 0x0000053C System.Void BNG.SkeletonVisualizer::ResetEditorHandles()
extern void SkeletonVisualizer_ResetEditorHandles_m3C997F258F70985BE0C5F9EA6C9DC1FA8596A11C (void);
// 0x0000053D System.Void BNG.SkeletonVisualizer::.ctor()
extern void SkeletonVisualizer__ctor_mFB3C1F4FA841DE31D9740E6E663689E4FECAAD21 (void);
// 0x0000053E System.Void BNG.XRTrackedPoseDriver::Awake()
extern void XRTrackedPoseDriver_Awake_mB79236506A2F6FD4038CF3F598A4C93C59BBFD27 (void);
// 0x0000053F System.Void BNG.XRTrackedPoseDriver::OnEnable()
extern void XRTrackedPoseDriver_OnEnable_m2E9B51B20C74B145AA3E021FF81A6D37A27B8877 (void);
// 0x00000540 System.Void BNG.XRTrackedPoseDriver::OnDisable()
extern void XRTrackedPoseDriver_OnDisable_m38BD85CA98BF0E6CEC0DAD2508DA8FD85A69BF29 (void);
// 0x00000541 System.Void BNG.XRTrackedPoseDriver::Update()
extern void XRTrackedPoseDriver_Update_mB724466A22AC89CDBF441E41340D1DFC37A90571 (void);
// 0x00000542 System.Void BNG.XRTrackedPoseDriver::FixedUpdate()
extern void XRTrackedPoseDriver_FixedUpdate_mF122848E5ACD326B80147F3E22B89AE3E0B6A279 (void);
// 0x00000543 System.Void BNG.XRTrackedPoseDriver::RefreshDeviceStatus()
extern void XRTrackedPoseDriver_RefreshDeviceStatus_mCCC33BF7DF6AA8977EBEDFEB0943E11B0136D446 (void);
// 0x00000544 System.Void BNG.XRTrackedPoseDriver::UpdateDevice()
extern void XRTrackedPoseDriver_UpdateDevice_m9F23BA7BB23C4DF479BB93FD4D002AF5B3A92D6F (void);
// 0x00000545 System.Void BNG.XRTrackedPoseDriver::OnBeforeRender()
extern void XRTrackedPoseDriver_OnBeforeRender_mE50AF2B475304742DFDF1F8EC592385A30C78659 (void);
// 0x00000546 UnityEngine.Vector3 BNG.XRTrackedPoseDriver::GetHMDLocalPosition()
extern void XRTrackedPoseDriver_GetHMDLocalPosition_mE27D6B72554702A2127CC7024C5B7D18FEB77348 (void);
// 0x00000547 UnityEngine.XR.InputDevice BNG.XRTrackedPoseDriver::GetHMD()
extern void XRTrackedPoseDriver_GetHMD_m757BCEC47CFADCB44D8A474AD272B8BDAABB2F5C (void);
// 0x00000548 UnityEngine.Quaternion BNG.XRTrackedPoseDriver::GetHMDLocalRotation()
extern void XRTrackedPoseDriver_GetHMDLocalRotation_m10CBED20A4260441009477F4D85B793C739F94E9 (void);
// 0x00000549 UnityEngine.Vector3 BNG.XRTrackedPoseDriver::GetControllerLocalPosition(BNG.ControllerHandedness)
extern void XRTrackedPoseDriver_GetControllerLocalPosition_m7D0BF72D4CB8AA1EDE60BCE21F9829CE755249D3 (void);
// 0x0000054A UnityEngine.Quaternion BNG.XRTrackedPoseDriver::GetControllerLocalRotation(BNG.ControllerHandedness)
extern void XRTrackedPoseDriver_GetControllerLocalRotation_mB9749BB06C958A09B8E431598A43F6BDF72E8E04 (void);
// 0x0000054B UnityEngine.XR.InputDevice BNG.XRTrackedPoseDriver::GetLeftController()
extern void XRTrackedPoseDriver_GetLeftController_mD7299D053D3928D0E46B71C98BC74DA556B2FC0B (void);
// 0x0000054C UnityEngine.XR.InputDevice BNG.XRTrackedPoseDriver::GetRightController()
extern void XRTrackedPoseDriver_GetRightController_m844AD552503ECA2A110C745A7A6EE629726279DE (void);
// 0x0000054D System.Void BNG.XRTrackedPoseDriver::.ctor()
extern void XRTrackedPoseDriver__ctor_m2E90E323D9C680556139032452BC3A130942656A (void);
// 0x0000054E System.Void BNG.XRTrackedPoseDriver::.cctor()
extern void XRTrackedPoseDriver__cctor_m2F3F3F39D80091F4AF3EE180E057DC3C54C002F4 (void);
// 0x0000054F System.Void BNG.DemoCube::Start()
extern void DemoCube_Start_m3EC0EB024C86C36241890E74D3DF80047466B60E (void);
// 0x00000550 System.Void BNG.DemoCube::SetActive(UnityEngine.EventSystems.PointerEventData)
extern void DemoCube_SetActive_mC5BE3B6872AF9C5808725F1B9A255B77A9811000 (void);
// 0x00000551 System.Void BNG.DemoCube::SetInactive(UnityEngine.EventSystems.PointerEventData)
extern void DemoCube_SetInactive_m329831D29C91F5570B6B19A1B9962C25E8925624 (void);
// 0x00000552 System.Void BNG.DemoCube::SetHovering(UnityEngine.EventSystems.PointerEventData)
extern void DemoCube_SetHovering_mBF79A1662019DAD48F8D319E9CA633AFCBB81BA0 (void);
// 0x00000553 System.Void BNG.DemoCube::ResetHovering(UnityEngine.EventSystems.PointerEventData)
extern void DemoCube_ResetHovering_m0E31632A067838EBEA2377F92E08F426E9040B75 (void);
// 0x00000554 System.Void BNG.DemoCube::UpdateMaterial()
extern void DemoCube_UpdateMaterial_mD60E030787D48A5C0E828D38B7F1AF614049B784 (void);
// 0x00000555 System.Void BNG.DemoCube::.ctor()
extern void DemoCube__ctor_mBD6836260A62681F86D09B27D91EEC93B8EA4453 (void);
// 0x00000556 System.Void BNG.DemoScript::Start()
extern void DemoScript_Start_mF8A0FE9B6D0FE12D489E178E0A6BBB7E78EC9BBD (void);
// 0x00000557 System.Void BNG.DemoScript::Update()
extern void DemoScript_Update_m68E4F2B56DA947A9ED1256E60941E08F21BB41A4 (void);
// 0x00000558 System.Void BNG.DemoScript::UpdateSliderText(System.Single)
extern void DemoScript_UpdateSliderText_m7B4B817E95F2162ED2104107CDB45BDE41A1C520 (void);
// 0x00000559 System.Void BNG.DemoScript::UpdateJoystickText(System.Single,System.Single)
extern void DemoScript_UpdateJoystickText_m657EEFE3D00E47F471463E27D2CEF810C4ED2F63 (void);
// 0x0000055A System.Void BNG.DemoScript::ResetGrabbables()
extern void DemoScript_ResetGrabbables_m106266FA28E995ADFC12D504E508780AE1A0C3A4 (void);
// 0x0000055B System.Void BNG.DemoScript::GrabAmmo(BNG.Grabber)
extern void DemoScript_GrabAmmo_mFCBF9A175A6E123FE4623BA704130B5A03B79771 (void);
// 0x0000055C System.Void BNG.DemoScript::ShootLauncher()
extern void DemoScript_ShootLauncher_mC15C73FD554C8179674C0B30C96F62EEEF0227ED (void);
// 0x0000055D System.Void BNG.DemoScript::initGravityCubes()
extern void DemoScript_initGravityCubes_mB8070B527D3D593438D6A7C4C553A4A36B6F423A (void);
// 0x0000055E System.Void BNG.DemoScript::rotateGravityCubes()
extern void DemoScript_rotateGravityCubes_m8EF9633B519781B6E4D942FECCF86C5DD3F1D8CE (void);
// 0x0000055F System.Void BNG.DemoScript::.ctor()
extern void DemoScript__ctor_m21F662BCB229EE6E67FEE09498C8EF333F9B9E2C (void);
// 0x00000560 System.Void BNG.PosRot::.ctor()
extern void PosRot__ctor_mC103513C67D370DB2B0B5A1202AC94E57FDA8BEE (void);
// 0x00000561 System.Void BNG.CharacterConstraint::Awake()
extern void CharacterConstraint_Awake_mA1569C6CBFBAA9D5207D51A5EF719A67EF9A9A49 (void);
// 0x00000562 System.Void BNG.CharacterConstraint::Update()
extern void CharacterConstraint_Update_m8CC91407767950F4DC267028A9FE95BAA751BDC2 (void);
// 0x00000563 System.Void BNG.CharacterConstraint::CheckCharacterCollisionMove()
extern void CharacterConstraint_CheckCharacterCollisionMove_mA80535E48514184E9528B063171EDCC68E6D96A0 (void);
// 0x00000564 System.Void BNG.CharacterConstraint::.ctor()
extern void CharacterConstraint__ctor_m97A50EBE5A3C6D59C8A919A6D51770E8B1B0796C (void);
// 0x00000565 System.Void BNG.CharacterIK::Start()
extern void CharacterIK_Start_mB6F97291EBFF9F4733570D863444018D6589AF79 (void);
// 0x00000566 System.Void BNG.CharacterIK::Update()
extern void CharacterIK_Update_mB89C04DFB842CAF69CB387B57CEC6AD5B82A7C63 (void);
// 0x00000567 System.Void BNG.CharacterIK::OnAnimatorIK()
extern void CharacterIK_OnAnimatorIK_mDEB6E20E5AFE09D800CC885DEB281991FD52789D (void);
// 0x00000568 System.Void BNG.CharacterIK::.ctor()
extern void CharacterIK__ctor_m9924ECD9D858F11214678E4DFA176BC5A8F70ADC (void);
// 0x00000569 System.Void BNG.CharacterYOffset::LateUpdate()
extern void CharacterYOffset_LateUpdate_mAD6C1F1F57659D702C731779E02CE9A9C6D3F1F9 (void);
// 0x0000056A System.Void BNG.CharacterYOffset::.ctor()
extern void CharacterYOffset__ctor_m4E137DD98CB306E3F3E95CDE7B554E9F44D04830 (void);
// 0x0000056B System.Void BNG.Climbable::Start()
extern void Climbable_Start_mED801A7C6ABC922FA53B18E81354A22E59FBEB6A (void);
// 0x0000056C System.Void BNG.Climbable::GrabItem(BNG.Grabber)
extern void Climbable_GrabItem_mE7947126527CA589FB2204B651493EB298BF2139 (void);
// 0x0000056D System.Void BNG.Climbable::DropItem(BNG.Grabber)
extern void Climbable_DropItem_m984FCD4FE2B24743A98B7D2F74ED3E84E56E195D (void);
// 0x0000056E System.Void BNG.Climbable::.ctor()
extern void Climbable__ctor_m3B0477518B343FD8FB7784422E72722FD7C7A9C6 (void);
// 0x0000056F System.Void BNG.CollisionSound::Start()
extern void CollisionSound_Start_m71E4A80B529DA11855634235D15348E95DECF7A4 (void);
// 0x00000570 System.Void BNG.CollisionSound::OnCollisionEnter(UnityEngine.Collision)
extern void CollisionSound_OnCollisionEnter_mE68DC6020040D49D78CBAEBF90FF0060E7AC9BD3 (void);
// 0x00000571 System.Void BNG.CollisionSound::resetLastPlayedSound()
extern void CollisionSound_resetLastPlayedSound_m01EAB435EB3CB6429152143288BEBDC75A496144 (void);
// 0x00000572 System.Void BNG.CollisionSound::.ctor()
extern void CollisionSound__ctor_mFE73000BCAD2274F3A14635B07CAFB995EE804F9 (void);
// 0x00000573 System.Void BNG.ConstrainLocalPosition::Update()
extern void ConstrainLocalPosition_Update_m1A34BDD9CA246E3145812045D75175C52DE999A3 (void);
// 0x00000574 System.Void BNG.ConstrainLocalPosition::doConstrain()
extern void ConstrainLocalPosition_doConstrain_mD01D66C5B2B24B029A6C5A7333349F2309D31638 (void);
// 0x00000575 System.Void BNG.ConstrainLocalPosition::.ctor()
extern void ConstrainLocalPosition__ctor_mFDBC14EFD8ACD77117584FEB4146A3F935666848 (void);
// 0x00000576 System.Void BNG.DamageCollider::Start()
extern void DamageCollider_Start_mF6C8CCA5727CC018D54C6C8803AA60BC14542ACE (void);
// 0x00000577 System.Void BNG.DamageCollider::OnCollisionEnter(UnityEngine.Collision)
extern void DamageCollider_OnCollisionEnter_mBA2C03ED77D1AD9076A5DF91E6E9629D98005D18 (void);
// 0x00000578 System.Void BNG.DamageCollider::OnCollisionEvent(UnityEngine.Collision)
extern void DamageCollider_OnCollisionEvent_m4BD7D7EB831A9D89A15B78780A32C7A1D9C2FF3D (void);
// 0x00000579 System.Void BNG.DamageCollider::.ctor()
extern void DamageCollider__ctor_m24CC62C0185DED3F623D24E33DC1DD18C2982ACE (void);
// 0x0000057A System.Void BNG.Damageable::Start()
extern void Damageable_Start_m04F8440585F350F7FBD0924D41581812ED1CA917 (void);
// 0x0000057B System.Void BNG.Damageable::DealDamage(System.Single)
extern void Damageable_DealDamage_m593470335966F15B7859AF34C75309FCFB7B6CB7 (void);
// 0x0000057C System.Void BNG.Damageable::DealDamage(System.Single,System.Nullable`1<UnityEngine.Vector3>,System.Nullable`1<UnityEngine.Vector3>,System.Boolean,UnityEngine.GameObject,UnityEngine.GameObject)
extern void Damageable_DealDamage_m146215804E39668D8A4F7E3DE888987585024CC5 (void);
// 0x0000057D System.Void BNG.Damageable::DestroyThis()
extern void Damageable_DestroyThis_m1D13204E94B1AD6C73330B0A5B263F52CD6EA3D5 (void);
// 0x0000057E System.Collections.IEnumerator BNG.Damageable::RespawnRoutine(System.Single)
extern void Damageable_RespawnRoutine_m98438AD3620986BB4C12670BEEC45A74EC698460 (void);
// 0x0000057F System.Void BNG.Damageable::.ctor()
extern void Damageable__ctor_m92C2FD92ECAD10D6D1F0E8CC24288D42DC9419E0 (void);
// 0x00000580 System.Void BNG.Damageable/<RespawnRoutine>d__22::.ctor(System.Int32)
extern void U3CRespawnRoutineU3Ed__22__ctor_m69D6950294EA9F9B77E24A393EA2074A3CF2628B (void);
// 0x00000581 System.Void BNG.Damageable/<RespawnRoutine>d__22::System.IDisposable.Dispose()
extern void U3CRespawnRoutineU3Ed__22_System_IDisposable_Dispose_m9D7014422AC615050DD5938A4AEA282697036F6C (void);
// 0x00000582 System.Boolean BNG.Damageable/<RespawnRoutine>d__22::MoveNext()
extern void U3CRespawnRoutineU3Ed__22_MoveNext_mCCA13A1167CF403D99830E0A4981B201FF825389 (void);
// 0x00000583 System.Object BNG.Damageable/<RespawnRoutine>d__22::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRespawnRoutineU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBF3FF1461C49FFDA560C68881CC0EEAF96C56AC5 (void);
// 0x00000584 System.Void BNG.Damageable/<RespawnRoutine>d__22::System.Collections.IEnumerator.Reset()
extern void U3CRespawnRoutineU3Ed__22_System_Collections_IEnumerator_Reset_mA73787DCBE03B96D4D07F7308F58141D08F94584 (void);
// 0x00000585 System.Object BNG.Damageable/<RespawnRoutine>d__22::System.Collections.IEnumerator.get_Current()
extern void U3CRespawnRoutineU3Ed__22_System_Collections_IEnumerator_get_Current_m68AA77D0D5D6FC65A5B0E20A2014AEBC21DE8BF9 (void);
// 0x00000586 System.Void BNG.DestroyObjectWithDelay::Start()
extern void DestroyObjectWithDelay_Start_m4A7346D68C69344DBCE38CC27D18A44AA7BCBC0A (void);
// 0x00000587 System.Void BNG.DestroyObjectWithDelay::.ctor()
extern void DestroyObjectWithDelay__ctor_m6B398C72F359B69BD9D0B1D136E5BE0CB6141715 (void);
// 0x00000588 System.Void BNG.FollowRigidbody::Start()
extern void FollowRigidbody_Start_m081B54F1C99F50853E84456E26D75F5C45392406 (void);
// 0x00000589 System.Void BNG.FollowRigidbody::FixedUpdate()
extern void FollowRigidbody_FixedUpdate_m883D623857B87205C897DE0C3BFDBE528698BD23 (void);
// 0x0000058A System.Void BNG.FollowRigidbody::.ctor()
extern void FollowRigidbody__ctor_mDA874184052CE9E0FBB45D88373A9215DB6219B9 (void);
// 0x0000058B System.Void BNG.FollowTransform::Update()
extern void FollowTransform_Update_mEC8166ADAA77B379C0A961A4C5E9ADDE24D2FBBE (void);
// 0x0000058C System.Void BNG.FollowTransform::.ctor()
extern void FollowTransform__ctor_m7A5BF699BF9182038B2A19E33F3052A364F686E9 (void);
// 0x0000058D System.Void BNG.GrabAction::OnGrab(BNG.Grabber)
extern void GrabAction_OnGrab_m4736BBCA8BA58260EAB54A747A6DD7A2CBC25354 (void);
// 0x0000058E System.Void BNG.GrabAction::.ctor()
extern void GrabAction__ctor_mC752DE22A025AA8F8B47B9F607D2382FEDF35E7F (void);
// 0x0000058F System.Void BNG.GrabPointTrigger::Start()
extern void GrabPointTrigger_Start_m37085176E4C9E8EBF4240CAA70E549D47BA73990 (void);
// 0x00000590 System.Void BNG.GrabPointTrigger::Update()
extern void GrabPointTrigger_Update_mD1EC23573E0105BF9651C4A5BCA8F64895BB3A98 (void);
// 0x00000591 System.Void BNG.GrabPointTrigger::UpdateGrabPoint(BNG.GrabPoint)
extern void GrabPointTrigger_UpdateGrabPoint_mDB3C96865A32D562B4AFE160D5112D0210CBD305 (void);
// 0x00000592 System.Void BNG.GrabPointTrigger::OnTriggerEnter(UnityEngine.Collider)
extern void GrabPointTrigger_OnTriggerEnter_m57E8927B35E49EEBE44736C85F4CBB83C99D74E2 (void);
// 0x00000593 System.Void BNG.GrabPointTrigger::OnTriggerExit(UnityEngine.Collider)
extern void GrabPointTrigger_OnTriggerExit_m718E4B5D37B5B306A10CB75FCC13DE86D3FBD494 (void);
// 0x00000594 System.Void BNG.GrabPointTrigger::setGrabber(BNG.Grabber)
extern void GrabPointTrigger_setGrabber_mB6638561BA91E6A6B2FD4BDB8E9777968728E29A (void);
// 0x00000595 System.Void BNG.GrabPointTrigger::ReleaseGrabber()
extern void GrabPointTrigger_ReleaseGrabber_m49FE5DCCD05C8A5876A403080051EE54E5639CCB (void);
// 0x00000596 System.Void BNG.GrabPointTrigger::.ctor()
extern void GrabPointTrigger__ctor_m96EB1E8B764FE13D879AA6DBAAB36BA561DBAF3E (void);
// 0x00000597 System.Void BNG.GrabbableBezierLine::Start()
extern void GrabbableBezierLine_Start_m7EC00D491AE67F0920D5316885544CDBF4312D90 (void);
// 0x00000598 System.Void BNG.GrabbableBezierLine::LateUpdate()
extern void GrabbableBezierLine_LateUpdate_m8CB9FFD91C5A77E22A70714B49867BD5A2B249E7 (void);
// 0x00000599 System.Void BNG.GrabbableBezierLine::OnGrab(BNG.Grabber)
extern void GrabbableBezierLine_OnGrab_m79E6D087F682410489B7870D1420883A2D4F057E (void);
// 0x0000059A System.Void BNG.GrabbableBezierLine::OnBecomesClosestGrabbable(BNG.Grabber)
extern void GrabbableBezierLine_OnBecomesClosestGrabbable_m7C2195D08CE1A694FD937E56391939A87114366D (void);
// 0x0000059B System.Void BNG.GrabbableBezierLine::OnNoLongerClosestGrabbable(BNG.Grabber)
extern void GrabbableBezierLine_OnNoLongerClosestGrabbable_m7557F4CBD127E1157F2B3B1066BD6B31E9CEDBF6 (void);
// 0x0000059C System.Void BNG.GrabbableBezierLine::OnBecomesClosestRemoteGrabbable(BNG.Grabber)
extern void GrabbableBezierLine_OnBecomesClosestRemoteGrabbable_mA2E214808E60C880AB6C54E5F10FC0B6B1B599DC (void);
// 0x0000059D System.Void BNG.GrabbableBezierLine::OnNoLongerClosestRemoteGrabbable(BNG.Grabber)
extern void GrabbableBezierLine_OnNoLongerClosestRemoteGrabbable_m7512D9177AC9151074B2E822974277702D0E1B3D (void);
// 0x0000059E System.Void BNG.GrabbableBezierLine::HighlightItem(BNG.Grabber)
extern void GrabbableBezierLine_HighlightItem_m1514F2D4658654AB5779CA2FB8AC8B1F823C2047 (void);
// 0x0000059F System.Void BNG.GrabbableBezierLine::UnhighlightItem()
extern void GrabbableBezierLine_UnhighlightItem_mD70696C561995F53F7A8302D7FFD70207B399FB7 (void);
// 0x000005A0 System.Void BNG.GrabbableBezierLine::DrawBezierCurve(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.LineRenderer)
extern void GrabbableBezierLine_DrawBezierCurve_mA29E3067EB5FA28E82D53C4EE6D0EEB49D7764E3 (void);
// 0x000005A1 System.Void BNG.GrabbableBezierLine::.ctor()
extern void GrabbableBezierLine__ctor_m7507DB9985C1EBD8F0CB4188EA79A54C2B3AB808 (void);
// 0x000005A2 System.Void BNG.GrabbableHaptics::OnGrab(BNG.Grabber)
extern void GrabbableHaptics_OnGrab_m92492E705873ADC68B655725E91476DD99F147D1 (void);
// 0x000005A3 System.Void BNG.GrabbableHaptics::OnRelease()
extern void GrabbableHaptics_OnRelease_m2FC8080FBA3EBB59F56AEB13C84F7514A0634A69 (void);
// 0x000005A4 System.Void BNG.GrabbableHaptics::OnBecomesClosestGrabbable(BNG.ControllerHand)
extern void GrabbableHaptics_OnBecomesClosestGrabbable_m20CE75F567E84F0B0DCC5DF2B6B50F2D4CC69F20 (void);
// 0x000005A5 System.Void BNG.GrabbableHaptics::OnBecomesClosestRemoteGrabbable(BNG.ControllerHand)
extern void GrabbableHaptics_OnBecomesClosestRemoteGrabbable_mD66B08DC083C2875BA7A02674CC5134E48391C3A (void);
// 0x000005A6 System.Void BNG.GrabbableHaptics::doHaptics(BNG.ControllerHand)
extern void GrabbableHaptics_doHaptics_mB18D35108539CE8F4A49737AFAB4E03397A1300D (void);
// 0x000005A7 System.Void BNG.GrabbableHaptics::OnCollisionEnter(UnityEngine.Collision)
extern void GrabbableHaptics_OnCollisionEnter_mA3D1120EAD34EF8F651A7F4A0993287ED685A4AB (void);
// 0x000005A8 System.Void BNG.GrabbableHaptics::.ctor()
extern void GrabbableHaptics__ctor_m3D075B87B86958ACF2BB3B8AE7931105D33857D8 (void);
// 0x000005A9 System.Void BNG.GrabbableHighlight::OnGrab(BNG.Grabber)
extern void GrabbableHighlight_OnGrab_m3D17F5360A135194ABE9690CA87F7BC7FE7DC59C (void);
// 0x000005AA System.Void BNG.GrabbableHighlight::OnBecomesClosestGrabbable(BNG.ControllerHand)
extern void GrabbableHighlight_OnBecomesClosestGrabbable_mB8AC55A5D790B1D60406EA80ABC31193D6C0DB97 (void);
// 0x000005AB System.Void BNG.GrabbableHighlight::OnNoLongerClosestGrabbable(BNG.ControllerHand)
extern void GrabbableHighlight_OnNoLongerClosestGrabbable_mA4147768E1583D699E3BFFE521BA90A385E5C491 (void);
// 0x000005AC System.Void BNG.GrabbableHighlight::OnBecomesClosestRemoteGrabbable(BNG.ControllerHand)
extern void GrabbableHighlight_OnBecomesClosestRemoteGrabbable_mAD422A92E1CE0CF3E025F070E8C9997F3D776ECC (void);
// 0x000005AD System.Void BNG.GrabbableHighlight::OnNoLongerClosestRemoteGrabbable(BNG.ControllerHand)
extern void GrabbableHighlight_OnNoLongerClosestRemoteGrabbable_mC3A601636A7233FA0AA808981D7544541FD6A157 (void);
// 0x000005AE System.Void BNG.GrabbableHighlight::HighlightItem()
extern void GrabbableHighlight_HighlightItem_mF4B06B58B25E513913451A034A1CF731DBE5F944 (void);
// 0x000005AF System.Void BNG.GrabbableHighlight::UnhighlightItem()
extern void GrabbableHighlight_UnhighlightItem_m174D6685B68657F06EFBD73ADD03D05CA8CE1A4D (void);
// 0x000005B0 System.Void BNG.GrabbableHighlight::.ctor()
extern void GrabbableHighlight__ctor_mEB46B0AB3C512F9BB4362431632A2B7325088479 (void);
// 0x000005B1 System.Void BNG.GrabbableHighlightMaterial::Start()
extern void GrabbableHighlightMaterial_Start_mDC15A925FF8FDBCFD3C14A6A29E9ACE1100D3E49 (void);
// 0x000005B2 System.Void BNG.GrabbableHighlightMaterial::OnGrab(BNG.Grabber)
extern void GrabbableHighlightMaterial_OnGrab_mF579CB2DFBADCAA6EF3CEB7995FB4EF6EA027347 (void);
// 0x000005B3 System.Void BNG.GrabbableHighlightMaterial::OnBecomesClosestGrabbable(BNG.ControllerHand)
extern void GrabbableHighlightMaterial_OnBecomesClosestGrabbable_mE6077DD4E01F9D52DA628FA461CCBAFA7A8D141F (void);
// 0x000005B4 System.Void BNG.GrabbableHighlightMaterial::OnNoLongerClosestGrabbable(BNG.ControllerHand)
extern void GrabbableHighlightMaterial_OnNoLongerClosestGrabbable_mFF1FF0294C9787422634F0B44DB72AD68193E21D (void);
// 0x000005B5 System.Void BNG.GrabbableHighlightMaterial::OnBecomesClosestRemoteGrabbable(BNG.ControllerHand)
extern void GrabbableHighlightMaterial_OnBecomesClosestRemoteGrabbable_m8CE0FEF90F81E0EF07A4845B9C741BC58BD848C6 (void);
// 0x000005B6 System.Void BNG.GrabbableHighlightMaterial::OnNoLongerClosestRemoteGrabbable(BNG.ControllerHand)
extern void GrabbableHighlightMaterial_OnNoLongerClosestRemoteGrabbable_m68165125C1A1CBFE1C3311C97AD71B354C7298A3 (void);
// 0x000005B7 System.Void BNG.GrabbableHighlightMaterial::HighlightItem()
extern void GrabbableHighlightMaterial_HighlightItem_mAA5E04BD58CCDB777E898A3AC4AC6C519C210349 (void);
// 0x000005B8 System.Void BNG.GrabbableHighlightMaterial::UnhighlightItem()
extern void GrabbableHighlightMaterial_UnhighlightItem_m323C4E10B73594A184A33F67458A3E83CE2793F3 (void);
// 0x000005B9 System.Void BNG.GrabbableHighlightMaterial::.ctor()
extern void GrabbableHighlightMaterial__ctor_m4F7F3C30F2E141F3B49718EC7D27FF2CC31CB8FD (void);
// 0x000005BA System.Void BNG.GrabbableRingHelper::Start()
extern void GrabbableRingHelper_Start_mDA24D827B24AD9D6CBF0BCABE8F75E1FDBCD74BB (void);
// 0x000005BB System.Void BNG.GrabbableRingHelper::.ctor()
extern void GrabbableRingHelper__ctor_m1BE13978BA6C8511B044592C03E4EFED58076E3C (void);
// 0x000005BC System.Void BNG.LineToTransform::Start()
extern void LineToTransform_Start_m763B49E6F81141D1C9097DEB9981638805421F2E (void);
// 0x000005BD System.Void BNG.LineToTransform::LateUpdate()
extern void LineToTransform_LateUpdate_mFC88A01A1625435C484DA068F0DF2384DB7FC44C (void);
// 0x000005BE System.Void BNG.LineToTransform::UpdateLine()
extern void LineToTransform_UpdateLine_m15A38E03378250BE16A0B51F1A59CD2B36FD0075 (void);
// 0x000005BF System.Void BNG.LineToTransform::OnDrawGizmos()
extern void LineToTransform_OnDrawGizmos_m0BA6CDBE9A5AB2F0C0B429B6D35534F0B9ACF81C (void);
// 0x000005C0 System.Void BNG.LineToTransform::.ctor()
extern void LineToTransform__ctor_m84950AA66CE67E99203CE0B489CC503C6A945FDE (void);
// 0x000005C1 System.Void BNG.LookAtTransform::Update()
extern void LookAtTransform_Update_mA6C604FE7F4EC8966F813AA16755925AD7440AAE (void);
// 0x000005C2 System.Void BNG.LookAtTransform::LateUpdate()
extern void LookAtTransform_LateUpdate_mD810095C360DD2CBD9D1C842C20C64A998724E39 (void);
// 0x000005C3 System.Void BNG.LookAtTransform::lookAt()
extern void LookAtTransform_lookAt_m902A726AD4D12BA6E0F853B5950E90347DCB457B (void);
// 0x000005C4 System.Void BNG.LookAtTransform::.ctor()
extern void LookAtTransform__ctor_mFADC1FE351A684889BD707EA8DD62C8BF0944427 (void);
// 0x000005C5 System.Void BNG.PlaySoundOnGrab::OnGrab(BNG.Grabber)
extern void PlaySoundOnGrab_OnGrab_m939FA1E32C71E78B3878C32DA71BBC22A3EC5D91 (void);
// 0x000005C6 System.Void BNG.PlaySoundOnGrab::.ctor()
extern void PlaySoundOnGrab__ctor_m099DE8154FD7B6A3948278F257C3078D33B08FAD (void);
// 0x000005C7 System.Void BNG.PunctureCollider::Start()
extern void PunctureCollider_Start_m23D077FB975FEC89E31A2BA82E97666FF1404F78 (void);
// 0x000005C8 System.Void BNG.PunctureCollider::FixedUpdate()
extern void PunctureCollider_FixedUpdate_m84CCB8A79496DBA69578096EF4798AE13A605A53 (void);
// 0x000005C9 System.Void BNG.PunctureCollider::UpdatePunctureValue()
extern void PunctureCollider_UpdatePunctureValue_m68939D5B98DA8E4D51AA8C41305DB9F467350FB9 (void);
// 0x000005CA System.Void BNG.PunctureCollider::MovePunctureUp()
extern void PunctureCollider_MovePunctureUp_mEFF26B2796A1C023E543ACF28BEE48CDC60344C2 (void);
// 0x000005CB System.Void BNG.PunctureCollider::MovePunctureDown()
extern void PunctureCollider_MovePunctureDown_m3EE635716B5D2FA4F0B892EF266A4DB4C50D40E4 (void);
// 0x000005CC System.Void BNG.PunctureCollider::CheckBreakDistance()
extern void PunctureCollider_CheckBreakDistance_m55752030C85948D77868FA5FAA5734717A43295A (void);
// 0x000005CD System.Void BNG.PunctureCollider::CheckPunctureRelease()
extern void PunctureCollider_CheckPunctureRelease_mF30B9EB4813DB023AE21CC233BD9677551C8DE31 (void);
// 0x000005CE System.Void BNG.PunctureCollider::AdjustJointMass()
extern void PunctureCollider_AdjustJointMass_mA89A602589FB41C3BA9B4FDE3B8DBA3283231849 (void);
// 0x000005CF System.Void BNG.PunctureCollider::ApplyResistanceForce()
extern void PunctureCollider_ApplyResistanceForce_m90924CD3DD46B9B320171C9864DD0C30B5DADD33 (void);
// 0x000005D0 System.Void BNG.PunctureCollider::DoPuncture(UnityEngine.Collider,UnityEngine.Vector3)
extern void PunctureCollider_DoPuncture_m4D3F79F8C0E33FAFA23166E5C1227458C90A2E09 (void);
// 0x000005D1 System.Void BNG.PunctureCollider::SetPenetration(System.Single)
extern void PunctureCollider_SetPenetration_m36A5312D41D0A65B398D1B52219F4EB336DE3508 (void);
// 0x000005D2 System.Void BNG.PunctureCollider::ReleasePuncture()
extern void PunctureCollider_ReleasePuncture_m06FE1333EC9FB1084295C70859A7F85A8B5B942E (void);
// 0x000005D3 System.Boolean BNG.PunctureCollider::CanPunctureObject(UnityEngine.GameObject)
extern void PunctureCollider_CanPunctureObject_mA09A2ABBEFCBF6EC9C46685C3C37D761874A0DF9 (void);
// 0x000005D4 System.Void BNG.PunctureCollider::OnCollisionEnter(UnityEngine.Collision)
extern void PunctureCollider_OnCollisionEnter_m744DFC0B620F09ECD948DAADFA2317B1080E747D (void);
// 0x000005D5 System.Void BNG.PunctureCollider::.ctor()
extern void PunctureCollider__ctor_m0FD116151DDF1EE2B0221FB749116D90714DC4AA (void);
// 0x000005D6 System.Void BNG.ReturnToSnapZone::Start()
extern void ReturnToSnapZone_Start_mD8949AAA83F5ED8435C85E2D9B41E457A1766521 (void);
// 0x000005D7 System.Void BNG.ReturnToSnapZone::Update()
extern void ReturnToSnapZone_Update_m2C6BBA85099B2C47E3C563C6523A2421BF54282D (void);
// 0x000005D8 System.Void BNG.ReturnToSnapZone::moveToSnapZone()
extern void ReturnToSnapZone_moveToSnapZone_m0ECC6F390849EBA5CDD0C3E008E814A1EBC8BB43 (void);
// 0x000005D9 System.Void BNG.ReturnToSnapZone::.ctor()
extern void ReturnToSnapZone__ctor_m4FA3F79F4DB7752E46B0B242D7C90F0097AEEDF6 (void);
// 0x000005DA System.Void BNG.RotateTowards::Update()
extern void RotateTowards_Update_m7A2896ACD2F19045D6BE5D29E926F8AC617EECFB (void);
// 0x000005DB System.Void BNG.RotateTowards::.ctor()
extern void RotateTowards__ctor_m561E188D3A6F8405270984037261EEC0DA243134 (void);
// 0x000005DC System.Void BNG.RotateWithHMD::Start()
extern void RotateWithHMD_Start_m4985031D350A93616441FDDC15AF9572B3DCAED6 (void);
// 0x000005DD System.Void BNG.RotateWithHMD::LateUpdate()
extern void RotateWithHMD_LateUpdate_m4FF90B28FE400655898AAE17368918AAA4513E27 (void);
// 0x000005DE System.Void BNG.RotateWithHMD::UpdatePosition()
extern void RotateWithHMD_UpdatePosition_m3BCA3C61B0E7D8518C16C7CB434152C6BCF052B7 (void);
// 0x000005DF System.Void BNG.RotateWithHMD::.ctor()
extern void RotateWithHMD__ctor_mC3890C2887ADCC6DD80CB6878F6D4F08EB252B33 (void);
// 0x000005E0 System.Void BNG.ScaleBetweenPoints::Update()
extern void ScaleBetweenPoints_Update_m76ED29EA52A36DB6E6C0A7035005C204EF10F890 (void);
// 0x000005E1 System.Void BNG.ScaleBetweenPoints::LateUpdate()
extern void ScaleBetweenPoints_LateUpdate_mEDFDF0A33785834A9776D073F2DC5CA0693497FF (void);
// 0x000005E2 System.Void BNG.ScaleBetweenPoints::doScale()
extern void ScaleBetweenPoints_doScale_m662588F63E1B3CDC3ADABE15D2AFA30ABD6994A3 (void);
// 0x000005E3 System.Void BNG.ScaleBetweenPoints::.ctor()
extern void ScaleBetweenPoints__ctor_m0D45EF03AB1AC4FFE1E32BA6D322ED7A4B18CF6B (void);
// 0x000005E4 System.Void BNG.ScreenFader::Awake()
extern void ScreenFader_Awake_mAAC372FCE277400FD3D6E6B320FF9166FDB87007 (void);
// 0x000005E5 System.Void BNG.ScreenFader::initialize()
extern void ScreenFader_initialize_mC923BFB9D29EB8762357E7700F6037685AD5E0C3 (void);
// 0x000005E6 System.Void BNG.ScreenFader::OnEnable()
extern void ScreenFader_OnEnable_m8F43113B1747665901B6996BDE794A10074A4A10 (void);
// 0x000005E7 System.Void BNG.ScreenFader::OnDisable()
extern void ScreenFader_OnDisable_mC617118A44EA298356E5D82ABC0C1A32FA513CCC (void);
// 0x000005E8 System.Void BNG.ScreenFader::OnSceneLoaded(UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode)
extern void ScreenFader_OnSceneLoaded_m37BD890B46551171AAD388CB7BB97B3A83FA9C39 (void);
// 0x000005E9 System.Collections.IEnumerator BNG.ScreenFader::fadeOutWithDelay(System.Single)
extern void ScreenFader_fadeOutWithDelay_mD86B773AE4D434860DADB74AF0DC6FE38B5A342D (void);
// 0x000005EA System.Void BNG.ScreenFader::DoFadeIn()
extern void ScreenFader_DoFadeIn_m062123ADE0427D57705C5BB77D5915962BFD12F0 (void);
// 0x000005EB System.Void BNG.ScreenFader::DoFadeOut()
extern void ScreenFader_DoFadeOut_m3AFBFE78C945B7F199557001458A608D723EE2B4 (void);
// 0x000005EC System.Void BNG.ScreenFader::SetFadeLevel(System.Single)
extern void ScreenFader_SetFadeLevel_mDE4FD12958A22767F7A5D58B937811311EF39EEC (void);
// 0x000005ED System.Collections.IEnumerator BNG.ScreenFader::doFade(System.Single,System.Single)
extern void ScreenFader_doFade_m862A3EF8A45B3051AFC5242A61D1C66AB78FE5BD (void);
// 0x000005EE System.Void BNG.ScreenFader::updateImageAlpha(System.Single)
extern void ScreenFader_updateImageAlpha_m6F946DFCB09EA85F8B613C8906530C9B9CF3363F (void);
// 0x000005EF System.Void BNG.ScreenFader::.ctor()
extern void ScreenFader__ctor_m4A79D6C895C42101F43AF0007E4E2666F0ACBD03 (void);
// 0x000005F0 System.Void BNG.ScreenFader/<fadeOutWithDelay>d__17::.ctor(System.Int32)
extern void U3CfadeOutWithDelayU3Ed__17__ctor_mE6A4698C3034BF181360C61E3F772E760AD0D8D9 (void);
// 0x000005F1 System.Void BNG.ScreenFader/<fadeOutWithDelay>d__17::System.IDisposable.Dispose()
extern void U3CfadeOutWithDelayU3Ed__17_System_IDisposable_Dispose_mCCB0765AE3D78937291A5AE49BD9B1BB9632CAFF (void);
// 0x000005F2 System.Boolean BNG.ScreenFader/<fadeOutWithDelay>d__17::MoveNext()
extern void U3CfadeOutWithDelayU3Ed__17_MoveNext_mE88C053E1E93978CDEFE793880939F8396CDB436 (void);
// 0x000005F3 System.Object BNG.ScreenFader/<fadeOutWithDelay>d__17::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CfadeOutWithDelayU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBDEBF359EFD0B9866A9AD796FC7F818F175F44B1 (void);
// 0x000005F4 System.Void BNG.ScreenFader/<fadeOutWithDelay>d__17::System.Collections.IEnumerator.Reset()
extern void U3CfadeOutWithDelayU3Ed__17_System_Collections_IEnumerator_Reset_m1EDA5F1320C288F543D7E60AD25ED9F7FCE69721 (void);
// 0x000005F5 System.Object BNG.ScreenFader/<fadeOutWithDelay>d__17::System.Collections.IEnumerator.get_Current()
extern void U3CfadeOutWithDelayU3Ed__17_System_Collections_IEnumerator_get_Current_mC8790369D8770293F8CCA82A75F9B622C6D1092D (void);
// 0x000005F6 System.Void BNG.ScreenFader/<doFade>d__21::.ctor(System.Int32)
extern void U3CdoFadeU3Ed__21__ctor_mFF2CCA1CB7F3B22254145D85230E3D0C117A7B35 (void);
// 0x000005F7 System.Void BNG.ScreenFader/<doFade>d__21::System.IDisposable.Dispose()
extern void U3CdoFadeU3Ed__21_System_IDisposable_Dispose_m006CBCC7A347057E864B04CE50FDDE18EABE0B15 (void);
// 0x000005F8 System.Boolean BNG.ScreenFader/<doFade>d__21::MoveNext()
extern void U3CdoFadeU3Ed__21_MoveNext_m69889B85F7D874ECCB3558DF5BC15B0EA64E9B2E (void);
// 0x000005F9 System.Object BNG.ScreenFader/<doFade>d__21::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CdoFadeU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8C7BED05CF7923BCEFE48AF9BB50F6D275FE83F2 (void);
// 0x000005FA System.Void BNG.ScreenFader/<doFade>d__21::System.Collections.IEnumerator.Reset()
extern void U3CdoFadeU3Ed__21_System_Collections_IEnumerator_Reset_mA4254373313176EA305CE175793B735FE7A4A4AF (void);
// 0x000005FB System.Object BNG.ScreenFader/<doFade>d__21::System.Collections.IEnumerator.get_Current()
extern void U3CdoFadeU3Ed__21_System_Collections_IEnumerator_get_Current_m411E69641964C55BC4010C8C44D26FB95F257182 (void);
// 0x000005FC System.Void BNG.SnapZoneRingHelper::Start()
extern void SnapZoneRingHelper_Start_m23D834E468F5D56B921ECF2376D47BAFCC6D0BB8 (void);
// 0x000005FD System.Void BNG.SnapZoneRingHelper::Update()
extern void SnapZoneRingHelper_Update_m1AEF094E4051CDA8CDD8AEE3E7BCFE666059249C (void);
// 0x000005FE System.Boolean BNG.SnapZoneRingHelper::checkIsValidSnap()
extern void SnapZoneRingHelper_checkIsValidSnap_m7EE8EA1EB6DE482C629A0964CA9B1F7D514FD093 (void);
// 0x000005FF System.Void BNG.SnapZoneRingHelper::.ctor()
extern void SnapZoneRingHelper__ctor_m75521EA3485CFBEFF72748D162964C0233A5AD87 (void);
// 0x00000600 System.Void BNG.Tooltip::Start()
extern void Tooltip_Start_mE38AA786D848E1561A5D11541E6356DC8287717B (void);
// 0x00000601 System.Void BNG.Tooltip::Update()
extern void Tooltip_Update_mFC24E5E457AD10B38952720326F9B641A8195E9A (void);
// 0x00000602 System.Void BNG.Tooltip::UpdateTooltipPosition()
extern void Tooltip_UpdateTooltipPosition_m36A83BDB6D46B2C7429C60A1053450754CAB5C31 (void);
// 0x00000603 System.Void BNG.Tooltip::.ctor()
extern void Tooltip__ctor_m3CB7B06DAB3CC4D19A406670FBC4DD5441F7CB3E (void);
// 0x00000604 System.Void BNG.VelocityTracker::Start()
extern void VelocityTracker_Start_mFCBC48FFADFD340097A92AEB8F0AC125EA57E656 (void);
// 0x00000605 System.Void BNG.VelocityTracker::FixedUpdate()
extern void VelocityTracker_FixedUpdate_mDBD3FE6AA7E3D15A17FF7E6C948723B1299ADDE1 (void);
// 0x00000606 System.Void BNG.VelocityTracker::UpdateVelocities()
extern void VelocityTracker_UpdateVelocities_m2484908532B94E504FA54ACA216251E0A8BDC27A (void);
// 0x00000607 System.Void BNG.VelocityTracker::UpdateVelocity()
extern void VelocityTracker_UpdateVelocity_m0BCBA61741ABAA74DA27DE70AE8F50AF865E94B9 (void);
// 0x00000608 System.Void BNG.VelocityTracker::UpdateAngularVelocity()
extern void VelocityTracker_UpdateAngularVelocity_mA407FB6B3B8FE06D58AD5CD07E89A6707E48968A (void);
// 0x00000609 UnityEngine.Vector3 BNG.VelocityTracker::GetVelocity()
extern void VelocityTracker_GetVelocity_mD7827FCEB570FAAEA40983047B125B2E48CC7EAC (void);
// 0x0000060A UnityEngine.Vector3 BNG.VelocityTracker::GetAveragedVelocity()
extern void VelocityTracker_GetAveragedVelocity_m6036C953779EE2E07DFAFE5ACDAB8BA5CE7EB87A (void);
// 0x0000060B UnityEngine.Vector3 BNG.VelocityTracker::GetAngularVelocity()
extern void VelocityTracker_GetAngularVelocity_mB3DE2D0DD8424EBE512EE82DCA57E85C32CB33DB (void);
// 0x0000060C UnityEngine.Vector3 BNG.VelocityTracker::GetAveragedAngularVelocity()
extern void VelocityTracker_GetAveragedAngularVelocity_mF0F495C1648C6EB164CA8EA3D51B85587047385F (void);
// 0x0000060D UnityEngine.Vector3 BNG.VelocityTracker::GetAveragedVector(System.Collections.Generic.List`1<UnityEngine.Vector3>)
extern void VelocityTracker_GetAveragedVector_mC5EE78C23B6C1798CF310EE662B7BDBC7E2C95F1 (void);
// 0x0000060E System.Void BNG.VelocityTracker::.ctor()
extern void VelocityTracker__ctor_m8E5973F5BE65EEA2F042B02C398E1B09C9C80C12 (void);
// 0x0000060F System.Single BNG.ArmSwingLocomotion::get_VelocitySum()
extern void ArmSwingLocomotion_get_VelocitySum_m9A00DCF3E39A58DA36726960379E60DA66AC5B34 (void);
// 0x00000610 System.Void BNG.ArmSwingLocomotion::Start()
extern void ArmSwingLocomotion_Start_m71F71E7C2E7B1569ED875947D22FBC6A77CF8C0C (void);
// 0x00000611 System.Void BNG.ArmSwingLocomotion::Update()
extern void ArmSwingLocomotion_Update_m65696F14ECAA1C51CE865965D595ACD865CA5FB9 (void);
// 0x00000612 System.Void BNG.ArmSwingLocomotion::UpdateMovement()
extern void ArmSwingLocomotion_UpdateMovement_m726E83EA8D6217DDFEDA966DE483F46E4CA4E799 (void);
// 0x00000613 System.Void BNG.ArmSwingLocomotion::UpdateVelocities()
extern void ArmSwingLocomotion_UpdateVelocities_m234795C427FB1AF60336CB9632B529D3DA764417 (void);
// 0x00000614 System.Boolean BNG.ArmSwingLocomotion::LeftInputReady()
extern void ArmSwingLocomotion_LeftInputReady_m304BED2FAC6BC9317D56D93AF35075844AE01E94 (void);
// 0x00000615 System.Boolean BNG.ArmSwingLocomotion::RightInputReady()
extern void ArmSwingLocomotion_RightInputReady_m1C7742AF45FA73AF4BFEC15420B33C22D7025E34 (void);
// 0x00000616 System.Void BNG.ArmSwingLocomotion::.ctor()
extern void ArmSwingLocomotion__ctor_mBB44CE0685C9FF82EC747224F7E60DC7B0BBB63D (void);
// 0x00000617 System.Void BNG.BNGPlayerController::Start()
extern void BNGPlayerController_Start_mC4A4E9DF9F9AEB0417B18D9E475D9E8B5FFDFDBA (void);
// 0x00000618 System.Void BNG.BNGPlayerController::Update()
extern void BNGPlayerController_Update_mE563FC1580FB30C97B0D16535D2092A3A4696443 (void);
// 0x00000619 System.Void BNG.BNGPlayerController::FixedUpdate()
extern void BNGPlayerController_FixedUpdate_m118FF6D53309B3D3DF7DACB76735272D6F1F9EAA (void);
// 0x0000061A System.Void BNG.BNGPlayerController::CheckPlayerElevationRespawn()
extern void BNGPlayerController_CheckPlayerElevationRespawn_mCA8A6EBE4AFB0E96639181CEBCDD374FF87FB9C4 (void);
// 0x0000061B System.Void BNG.BNGPlayerController::UpdateDistanceFromGround()
extern void BNGPlayerController_UpdateDistanceFromGround_m10BF5D47123AF4EC454A153845F68726ABE19EE6 (void);
// 0x0000061C System.Void BNG.BNGPlayerController::RotateTrackingSpaceToCamera()
extern void BNGPlayerController_RotateTrackingSpaceToCamera_mC3B9DE8809C08DA7FC68876C70BAC1E4372947B2 (void);
// 0x0000061D System.Void BNG.BNGPlayerController::UpdateCameraRigPosition()
extern void BNGPlayerController_UpdateCameraRigPosition_mE4A1A4DA0C7284E54829EC29FDDA687A643A7EA7 (void);
// 0x0000061E System.Void BNG.BNGPlayerController::UpdateCharacterHeight()
extern void BNGPlayerController_UpdateCharacterHeight_mC0697D60105CAACC24DC932DF8F47CA03776903A (void);
// 0x0000061F System.Void BNG.BNGPlayerController::UpdateCameraHeight()
extern void BNGPlayerController_UpdateCameraHeight_mC753215A11095AFE519923F0C2C0FEAD6CC31103 (void);
// 0x00000620 System.Void BNG.BNGPlayerController::CheckCharacterCollisionMove()
extern void BNGPlayerController_CheckCharacterCollisionMove_m4C0E587A65B36F69B92B3BF39A9F737DEE4AD193 (void);
// 0x00000621 System.Void BNG.BNGPlayerController::CheckRigidbodyCapsuleMove(UnityEngine.Vector3)
extern void BNGPlayerController_CheckRigidbodyCapsuleMove_m8740D2E41E2426E3EB4A19808340B2039A120DE7 (void);
// 0x00000622 System.Boolean BNG.BNGPlayerController::IsGrounded()
extern void BNGPlayerController_IsGrounded_m6971C747A2C7370505A68D626E31131E34D9437A (void);
// 0x00000623 System.Void BNG.BNGPlayerController::OnClimbingChange()
extern void BNGPlayerController_OnClimbingChange_m8A5A7545D1DC74638D615A7E9E4A16E5CCE954C7 (void);
// 0x00000624 System.Void BNG.BNGPlayerController::.ctor()
extern void BNGPlayerController__ctor_mFDE3785D715242E8E7D653F1C50CF13A833605AB (void);
// 0x00000625 System.Void BNG.Button::Start()
extern void Button_Start_m2EDEBB078BFBD9A56BEC947CF9E75295A5C9A666 (void);
// 0x00000626 System.Void BNG.Button::Update()
extern void Button_Update_m98F5C08C1541B59D8AECBD95586257C41CF6BF40 (void);
// 0x00000627 UnityEngine.Vector3 BNG.Button::GetButtonUpPosition()
extern void Button_GetButtonUpPosition_mAD229F48708202619364760DA3B4557E5C97AAD7 (void);
// 0x00000628 UnityEngine.Vector3 BNG.Button::GetButtonDownPosition()
extern void Button_GetButtonDownPosition_m6B9A5A51AA7EB8F89ECAC2E11B790B02BC2DB967 (void);
// 0x00000629 System.Void BNG.Button::OnButtonDown()
extern void Button_OnButtonDown_m1BD11A554FC890DB805E12E595C31C9C835FE24B (void);
// 0x0000062A System.Void BNG.Button::OnButtonUp()
extern void Button_OnButtonUp_mEB88729BF0D91B57222321C3B2A95F69AEB1CC5C (void);
// 0x0000062B System.Void BNG.Button::OnTriggerEnter(UnityEngine.Collider)
extern void Button_OnTriggerEnter_m7AEC71268FA897615C86AE49B56AB26B26E9B338 (void);
// 0x0000062C System.Void BNG.Button::OnTriggerExit(UnityEngine.Collider)
extern void Button_OnTriggerExit_m9D91CB892299BD528B4598F222988CC203C6B62D (void);
// 0x0000062D System.Void BNG.Button::OnDrawGizmosSelected()
extern void Button_OnDrawGizmosSelected_m2356F2EE120CA63412CE08A3C964698187F580A4 (void);
// 0x0000062E System.Void BNG.Button::.ctor()
extern void Button__ctor_m5F20BF9DD534AED16292D059DA5C84BF059E1B43 (void);
// 0x0000062F System.Void BNG.ControllerModelSelector::OnEnable()
extern void ControllerModelSelector_OnEnable_m660FA9702C50ED4644F274754E9D8F2386D3EDEF (void);
// 0x00000630 System.Void BNG.ControllerModelSelector::UpdateControllerModel()
extern void ControllerModelSelector_UpdateControllerModel_m0E72CA18A0AE0BDE798CE6B77D9C1516B4F140FE (void);
// 0x00000631 System.Void BNG.ControllerModelSelector::EnableChildController(System.Int32)
extern void ControllerModelSelector_EnableChildController_m7EBC09DE873AC387A8B0381C4FCC39BCF07D3144 (void);
// 0x00000632 System.Void BNG.ControllerModelSelector::OnDisable()
extern void ControllerModelSelector_OnDisable_m7D7318A7D6994C94A454105EA269E87EAB86A5AE (void);
// 0x00000633 System.Void BNG.ControllerModelSelector::OnApplicationQuit()
extern void ControllerModelSelector_OnApplicationQuit_m4A390A71CB85B11C85BD58662F91DE6D64AF84DD (void);
// 0x00000634 System.Void BNG.ControllerModelSelector::.ctor()
extern void ControllerModelSelector__ctor_m2FB425A768C65819142E96A0CBB1BCFC996A14C0 (void);
// 0x00000635 System.Void BNG.GrabPoint::UpdatePreviewTransforms()
extern void GrabPoint_UpdatePreviewTransforms_m0B2165BB6DC6D1C63D1B049954B25628052CBFAD (void);
// 0x00000636 System.Void BNG.GrabPoint::UpdateHandPosePreview()
extern void GrabPoint_UpdateHandPosePreview_m8B5C6302FDF1032ACC534241ECF1A09368EDED5D (void);
// 0x00000637 System.Void BNG.GrabPoint::UpdateAutoPoserPreview()
extern void GrabPoint_UpdateAutoPoserPreview_m6BBEC848EC274DCE354FE722883FB1C95F102372 (void);
// 0x00000638 System.Void BNG.GrabPoint::UpdateChildAnimators()
extern void GrabPoint_UpdateChildAnimators_mE232FB3EE90FD25FD27270C84F5D4E78A0BCD568 (void);
// 0x00000639 System.Void BNG.GrabPoint::.ctor()
extern void GrabPoint__ctor_mBE4D16187F0EF7BFB7D5D6EDD06AC0CF2E56DBE3 (void);
// 0x0000063A System.Boolean BNG.Grabbable::get_BeingHeldWithTwoHands()
extern void Grabbable_get_BeingHeldWithTwoHands_m3DAC984D14FBB33E0BF2036B531B8E5D21BCF3F7 (void);
// 0x0000063B System.Collections.Generic.List`1<BNG.Grabber> BNG.Grabbable::get_HeldByGrabbers()
extern void Grabbable_get_HeldByGrabbers_mB3E16882EFD51B9F40D5BD24F5F01207124CA9AA (void);
// 0x0000063C System.Boolean BNG.Grabbable::get_RemoteGrabbing()
extern void Grabbable_get_RemoteGrabbing_m16A2DA665EA7BB3F416EC5C8BF1644F2C2F5495B (void);
// 0x0000063D UnityEngine.Vector3 BNG.Grabbable::get_OriginalScale()
extern void Grabbable_get_OriginalScale_m9B19AD1DEEC7426B690B8411F045B55E4DF3A532 (void);
// 0x0000063E System.Void BNG.Grabbable::set_OriginalScale(UnityEngine.Vector3)
extern void Grabbable_set_OriginalScale_m7FCE747EB1C49788546E552ABCBA224C221A751A (void);
// 0x0000063F System.Single BNG.Grabbable::get_lastCollisionSeconds()
extern void Grabbable_get_lastCollisionSeconds_mBF7F1D4BCBE4C738A07576FC4C8C6ADF26E85CDA (void);
// 0x00000640 System.Void BNG.Grabbable::set_lastCollisionSeconds(System.Single)
extern void Grabbable_set_lastCollisionSeconds_m97DA7151E8F6E9790FCB7D2082F407359A97B908 (void);
// 0x00000641 System.Single BNG.Grabbable::get_lastNoCollisionSeconds()
extern void Grabbable_get_lastNoCollisionSeconds_mF7FA6B3E0A03B7F11295FA04BF0FD3DD48098554 (void);
// 0x00000642 System.Void BNG.Grabbable::set_lastNoCollisionSeconds(System.Single)
extern void Grabbable_set_lastNoCollisionSeconds_m30D0E1CECB0890BC6819E5FEE31E2284BEEE24CC (void);
// 0x00000643 System.Boolean BNG.Grabbable::get_RecentlyCollided()
extern void Grabbable_get_RecentlyCollided_m2733EDCB2A47FF99B02F1147E5FCB3D743877B29 (void);
// 0x00000644 System.Single BNG.Grabbable::get_requestSpringTime()
extern void Grabbable_get_requestSpringTime_m47611550BCD10522CF270C53D3B014AED22387E9 (void);
// 0x00000645 System.Void BNG.Grabbable::set_requestSpringTime(System.Single)
extern void Grabbable_set_requestSpringTime_m08C34AF8FD76EE0C8982E10A237F21AC99142162 (void);
// 0x00000646 UnityEngine.Vector3 BNG.Grabbable::get_grabPosition()
extern void Grabbable_get_grabPosition_mBB2405AFFCFE0C5322AEA75B4011DCC8C23DFCE7 (void);
// 0x00000647 UnityEngine.Vector3 BNG.Grabbable::get_GrabPositionOffset()
extern void Grabbable_get_GrabPositionOffset_m6ED3CE31AC735A6A238B94AB8CC08C95B6A3C9E8 (void);
// 0x00000648 UnityEngine.Vector3 BNG.Grabbable::get_GrabRotationOffset()
extern void Grabbable_get_GrabRotationOffset_m3051403B2AD53C48743FF306B069524235B3D883 (void);
// 0x00000649 UnityEngine.Transform BNG.Grabbable::get_grabTransform()
extern void Grabbable_get_grabTransform_mDC2EFFB018A7B1213C8C99EDD2F988FF66E47319 (void);
// 0x0000064A UnityEngine.Transform BNG.Grabbable::get_grabTransformSecondary()
extern void Grabbable_get_grabTransformSecondary_mE96FE5DD2219FA578ABEA7F6B1FA525445BA302C (void);
// 0x0000064B System.Boolean BNG.Grabbable::get_CanBeMoved()
extern void Grabbable_get_CanBeMoved_mEBF6AD807ABBF3BAFDE9BB7E6B58C2EE3AF0C611 (void);
// 0x0000064C BNG.BNGPlayerController BNG.Grabbable::get_player()
extern void Grabbable_get_player_mD3C5F9F61DC448F11C366DE7AC381C231930C28B (void);
// 0x0000064D BNG.Grabber BNG.Grabbable::get_FlyingToGrabber()
extern void Grabbable_get_FlyingToGrabber_mFF8A10085F3D418A2A8775EE7699F58C5AEA2543 (void);
// 0x0000064E System.Boolean BNG.Grabbable::get_DidParentHands()
extern void Grabbable_get_DidParentHands_mF0E351F8FE4BFAC9E3AFEF876D950635757D34F8 (void);
// 0x0000064F System.Void BNG.Grabbable::Awake()
extern void Grabbable_Awake_m0A771E6A81B3DDFB0FB3F1C74C97778EEF83A4DD (void);
// 0x00000650 System.Void BNG.Grabbable::Update()
extern void Grabbable_Update_m4C33A838362EBC26CC3D53D4C0C2EF150A3819F4 (void);
// 0x00000651 System.Void BNG.Grabbable::FixedUpdate()
extern void Grabbable_FixedUpdate_m289F026EDC0196D4D854BAC4FCE16420AAFCE387 (void);
// 0x00000652 UnityEngine.Vector3 BNG.Grabbable::GetGrabberWithGrabPointOffset(BNG.Grabber,UnityEngine.Transform)
extern void Grabbable_GetGrabberWithGrabPointOffset_m71AC439623F39CFA9A448859979B299B672BCA1B (void);
// 0x00000653 UnityEngine.Quaternion BNG.Grabbable::GetGrabberWithOffsetWorldRotation(BNG.Grabber)
extern void Grabbable_GetGrabberWithOffsetWorldRotation_m71C96A5CDABC7C4506F2F9614835D1B6EEC5DE29 (void);
// 0x00000654 System.Void BNG.Grabbable::positionHandGraphics(BNG.Grabber)
extern void Grabbable_positionHandGraphics_m3850913BA843F928E9C61DFF39A898CD4CB3AFA9 (void);
// 0x00000655 System.Boolean BNG.Grabbable::IsGrabbable()
extern void Grabbable_IsGrabbable_m997F6E3753A6DCE1D6A26A36E625D82028C49ACC (void);
// 0x00000656 System.Void BNG.Grabbable::UpdateRemoteGrab()
extern void Grabbable_UpdateRemoteGrab_m7E78ECA7E83F2B39FB622EF9F416FB4102081516 (void);
// 0x00000657 System.Void BNG.Grabbable::CheckRemoteGrabLinear()
extern void Grabbable_CheckRemoteGrabLinear_m57AD00989F1282952B3E6A4D6DFAD2C62447BCFC (void);
// 0x00000658 System.Void BNG.Grabbable::CheckRemoteGrabVelocity()
extern void Grabbable_CheckRemoteGrabVelocity_m549D9E22D66102F13D24A4E62ABF2BB75C056B43 (void);
// 0x00000659 System.Void BNG.Grabbable::InitiateFlick()
extern void Grabbable_InitiateFlick_m696A2064D593AC8DD62D50EA06CAE4F0656CF770 (void);
// 0x0000065A UnityEngine.Vector3 BNG.Grabbable::GetVelocityToHitTargetByTime(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void Grabbable_GetVelocityToHitTargetByTime_m2DFD923E24C36AFD07B136EBE94E298D3C9763D9 (void);
// 0x0000065B System.Void BNG.Grabbable::CheckRemoteGrabFlick()
extern void Grabbable_CheckRemoteGrabFlick_m4454EE3D7F8BE7CDEF23DB7792AE8E13122FFCEA (void);
// 0x0000065C System.Void BNG.Grabbable::UpdateFixedJoints()
extern void Grabbable_UpdateFixedJoints_m6C71682DC5A3A0D4501D818F06885F845E01272D (void);
// 0x0000065D System.Void BNG.Grabbable::UpdatePhysicsJoints()
extern void Grabbable_UpdatePhysicsJoints_mC6B06606B262B2A76833A5F7497F54A46F0CC40A (void);
// 0x0000065E System.Void BNG.Grabbable::setPositionSpring(System.Single,System.Single)
extern void Grabbable_setPositionSpring_m0E060522B62823ABF7352CF13E02E7439DE877FD (void);
// 0x0000065F System.Void BNG.Grabbable::setSlerpDrive(System.Single,System.Single)
extern void Grabbable_setSlerpDrive_mB2C6541C8A0B8E39588EBAEE855A1CF6AE606527 (void);
// 0x00000660 UnityEngine.Vector3 BNG.Grabbable::GetGrabberVector3(BNG.Grabber,System.Boolean)
extern void Grabbable_GetGrabberVector3_mF1065B3DF1C14AA45485D27D6689434DEF895410 (void);
// 0x00000661 UnityEngine.Quaternion BNG.Grabbable::GetGrabberQuaternion(BNG.Grabber,System.Boolean)
extern void Grabbable_GetGrabberQuaternion_mC2569CFBC405F0F867DEC9B5EDBB419B94C160FF (void);
// 0x00000662 System.Void BNG.Grabbable::moveWithVelocity()
extern void Grabbable_moveWithVelocity_mDFF369846D10AC7EF418202200D85563D3550C48 (void);
// 0x00000663 System.Void BNG.Grabbable::rotateWithVelocity()
extern void Grabbable_rotateWithVelocity_mCE81DDC3153E4C12B851EDA1267516AB2EE50A99 (void);
// 0x00000664 UnityEngine.Vector3 BNG.Grabbable::GetGrabbersAveragedPosition()
extern void Grabbable_GetGrabbersAveragedPosition_mFEFDA9A3A343F01310D2D17C0F9D8E260C77A760 (void);
// 0x00000665 UnityEngine.Quaternion BNG.Grabbable::GetGrabbersAveragedRotation()
extern void Grabbable_GetGrabbersAveragedRotation_m0F3CAE0A24FAA9B0C70DECD5515D2B6CA9082769 (void);
// 0x00000666 System.Void BNG.Grabbable::UpdateKinematicPhysics()
extern void Grabbable_UpdateKinematicPhysics_mD2DF5B23882AC29D8121E30F45B664704DF6C9A8 (void);
// 0x00000667 System.Void BNG.Grabbable::UpdateVelocityPhysics()
extern void Grabbable_UpdateVelocityPhysics_m6A289A4BF217C990FAF3C0AD7511B77012D4A124 (void);
// 0x00000668 System.Void BNG.Grabbable::checkParentHands(BNG.Grabber)
extern void Grabbable_checkParentHands_m149A7CD11FE226346BA21E6E931A82BE67717405 (void);
// 0x00000669 System.Boolean BNG.Grabbable::canBeMoved()
extern void Grabbable_canBeMoved_m80E78D8FC28841415A901C2A441EE7F995BE0B03 (void);
// 0x0000066A System.Void BNG.Grabbable::checkSecondaryLook()
extern void Grabbable_checkSecondaryLook_mF127E579F07C3ED5882A9E9A376F87C04CE62B9B (void);
// 0x0000066B System.Void BNG.Grabbable::rotateGrabber(System.Boolean)
extern void Grabbable_rotateGrabber_mA57A66550109DDF99A5164B0EEE21D25906DA028 (void);
// 0x0000066C UnityEngine.Transform BNG.Grabbable::GetGrabPoint()
extern void Grabbable_GetGrabPoint_m83A0824B21D097CDBD62BF0237923253DC1D3B88 (void);
// 0x0000066D System.Void BNG.Grabbable::GrabItem(BNG.Grabber)
extern void Grabbable_GrabItem_m9CB66185368B7434BF1464BDC987C77388F8B3E4 (void);
// 0x0000066E System.Void BNG.Grabbable::setupConfigJointGrab(BNG.Grabber,BNG.GrabType)
extern void Grabbable_setupConfigJointGrab_m0291F5BEB8E722EB909C7FBEE419046825919778 (void);
// 0x0000066F System.Void BNG.Grabbable::setupFixedJointGrab(BNG.Grabber,BNG.GrabType)
extern void Grabbable_setupFixedJointGrab_mA46C958D87378DD1D4D30F0AA753580E0ED00529 (void);
// 0x00000670 System.Void BNG.Grabbable::setupKinematicGrab(BNG.Grabber,BNG.GrabType)
extern void Grabbable_setupKinematicGrab_m69AA9B7F8DC0CD2825732C24939F9F7CF7C7955F (void);
// 0x00000671 System.Void BNG.Grabbable::setupVelocityGrab(BNG.Grabber,BNG.GrabType)
extern void Grabbable_setupVelocityGrab_m1DD8C5E5E4881B0DEDFCD89F10FC3957356D8979 (void);
// 0x00000672 System.Void BNG.Grabbable::GrabRemoteItem(BNG.Grabber)
extern void Grabbable_GrabRemoteItem_m50B7AE2497F7898717FD3FAEA0C545A21E409965 (void);
// 0x00000673 System.Void BNG.Grabbable::ResetGrabbing()
extern void Grabbable_ResetGrabbing_m89E2677F90FD73DBC89CA11457FC6320149D2735 (void);
// 0x00000674 System.Void BNG.Grabbable::DropItem(BNG.Grabber,System.Boolean,System.Boolean)
extern void Grabbable_DropItem_mEFAFF6A597B3040204BE5ABCF90A74C80AB11DB0 (void);
// 0x00000675 System.Void BNG.Grabbable::clearLookAtTransform()
extern void Grabbable_clearLookAtTransform_m422AC60814B87213BED1E9BDE61FB680721B1B62 (void);
// 0x00000676 System.Void BNG.Grabbable::callEvents(BNG.Grabber)
extern void Grabbable_callEvents_m524680A6268E78D0FE4050E6197AE5542436DCFD (void);
// 0x00000677 System.Void BNG.Grabbable::DropItem(BNG.Grabber)
extern void Grabbable_DropItem_m35EAFD02E34C19BB7E02077964C4856E88E65BFF (void);
// 0x00000678 System.Void BNG.Grabbable::DropItem(System.Boolean,System.Boolean)
extern void Grabbable_DropItem_m13C62518E7B3957E8938CD5CF84944586CC2C02E (void);
// 0x00000679 System.Void BNG.Grabbable::ResetScale()
extern void Grabbable_ResetScale_mC49D5BCB43BFC6FDA0126F73BF1F8EBA7B92F981 (void);
// 0x0000067A System.Void BNG.Grabbable::ResetParent()
extern void Grabbable_ResetParent_m7F9AA9E1D12E9FFF3C2652287CAE7B1A2BC4C265 (void);
// 0x0000067B System.Void BNG.Grabbable::UpdateOriginalParent(UnityEngine.Transform)
extern void Grabbable_UpdateOriginalParent_m6EDD79D965D9AB995B66A591A64D3A3D354BC69C (void);
// 0x0000067C System.Void BNG.Grabbable::UpdateOriginalParent()
extern void Grabbable_UpdateOriginalParent_m051285106A39098C3FAC0ED4371431436BBDCAC5 (void);
// 0x0000067D BNG.ControllerHand BNG.Grabbable::GetControllerHand(BNG.Grabber)
extern void Grabbable_GetControllerHand_m8CD072DBA6604E18EF3D7665065BC535A263386A (void);
// 0x0000067E BNG.Grabber BNG.Grabbable::GetPrimaryGrabber()
extern void Grabbable_GetPrimaryGrabber_m63BB2F47CAD26232495D6E8A39633E27E1612026 (void);
// 0x0000067F BNG.Grabber BNG.Grabbable::GetClosestGrabber()
extern void Grabbable_GetClosestGrabber_mCAC30D4248928BFCF7385F87CE2840429C1AA889 (void);
// 0x00000680 UnityEngine.Transform BNG.Grabbable::GetClosestGrabPoint(BNG.Grabber)
extern void Grabbable_GetClosestGrabPoint_m8DE1C2F88614F14D43531110C584DF7A4739FFA8 (void);
// 0x00000681 System.Void BNG.Grabbable::Release(UnityEngine.Vector3,UnityEngine.Vector3)
extern void Grabbable_Release_mD669FA02038028187B8A91E7910EF367E84C221C (void);
// 0x00000682 System.Boolean BNG.Grabbable::IsValidCollision(UnityEngine.Collision)
extern void Grabbable_IsValidCollision_m1944EB6A501EB92FE8C52BEE451A3683F27F2A66 (void);
// 0x00000683 System.Boolean BNG.Grabbable::IsValidCollision(UnityEngine.Collider)
extern void Grabbable_IsValidCollision_mF5D08297EA71AC961426FD3F5CFED0209D7BEF8A (void);
// 0x00000684 System.Void BNG.Grabbable::parentHandGraphics(BNG.Grabber)
extern void Grabbable_parentHandGraphics_mAB917C0704D7790C1C0FBF098CB6FBDF89055602 (void);
// 0x00000685 System.Void BNG.Grabbable::setupConfigJoint(BNG.Grabber)
extern void Grabbable_setupConfigJoint_m0F25C0778DCB756888C72702052D46EA08B32733 (void);
// 0x00000686 System.Void BNG.Grabbable::removeConfigJoint()
extern void Grabbable_removeConfigJoint_mD9A50031744C2E77EDEFFA7955357B1F7C4D925B (void);
// 0x00000687 System.Void BNG.Grabbable::addGrabber(BNG.Grabber)
extern void Grabbable_addGrabber_mA4EFEB17A105F32B85A15170DD62D425153E4D25 (void);
// 0x00000688 System.Void BNG.Grabbable::removeGrabber(BNG.Grabber)
extern void Grabbable_removeGrabber_m4DE10E2F63484923BDD4F1393E57F4DCA519B54E (void);
// 0x00000689 System.Void BNG.Grabbable::movePosition(UnityEngine.Vector3)
extern void Grabbable_movePosition_m64C7E68BEC4072762D15904A16B9F77DD514573A (void);
// 0x0000068A System.Void BNG.Grabbable::moveRotation(UnityEngine.Quaternion)
extern void Grabbable_moveRotation_m99114744F077B7176944786BD563E262C4853F53 (void);
// 0x0000068B UnityEngine.Vector3 BNG.Grabbable::getRemotePosition(BNG.Grabber)
extern void Grabbable_getRemotePosition_m4A18A72F65F402CA915FCBE558D1A67E3B360F32 (void);
// 0x0000068C UnityEngine.Quaternion BNG.Grabbable::getRemoteRotation(BNG.Grabber)
extern void Grabbable_getRemoteRotation_mCCE58EB19018D6108E50BEB89B5D07AFE038CA16 (void);
// 0x0000068D System.Void BNG.Grabbable::filterCollisions()
extern void Grabbable_filterCollisions_mCFE6BA43F956680A3438AE5BF718D25F3E3FCCBA (void);
// 0x0000068E BNG.BNGPlayerController BNG.Grabbable::GetBNGPlayerController()
extern void Grabbable_GetBNGPlayerController_m841FF78F9139D8F732499320B49A294C3436D6F4 (void);
// 0x0000068F System.Void BNG.Grabbable::RequestSpringTime(System.Single)
extern void Grabbable_RequestSpringTime_mA089D6C5C6DB2998EF9F124C1B3C1776A89B0C10 (void);
// 0x00000690 System.Void BNG.Grabbable::AddValidGrabber(BNG.Grabber)
extern void Grabbable_AddValidGrabber_mA164DDD0E08A76F6F7AB7AEC89F3A81DA1588395 (void);
// 0x00000691 System.Void BNG.Grabbable::RemoveValidGrabber(BNG.Grabber)
extern void Grabbable_RemoveValidGrabber_mA673F23FB019D142E55DC84A7E67EB776FFBB05A (void);
// 0x00000692 System.Void BNG.Grabbable::SubscribeToMoveEvents()
extern void Grabbable_SubscribeToMoveEvents_m74469140C5A493A49F9B3D41E64A0D16A9E6C07C (void);
// 0x00000693 System.Void BNG.Grabbable::UnsubscribeFromMoveEvents()
extern void Grabbable_UnsubscribeFromMoveEvents_m7F997A86883B3EA36F2EFAE0647C2C710FF7E147 (void);
// 0x00000694 System.Void BNG.Grabbable::LockGrabbable()
extern void Grabbable_LockGrabbable_m458940515475256513E228CB7A98F3D6D99EC9A8 (void);
// 0x00000695 System.Void BNG.Grabbable::LockGrabbableWithRotation()
extern void Grabbable_LockGrabbableWithRotation_m7F3E23B93A050813F1C941FB03A2381039A8B700 (void);
// 0x00000696 System.Void BNG.Grabbable::RequestLockGrabbable()
extern void Grabbable_RequestLockGrabbable_m22C18DDC409A38113766BEAA0419C2D2F36D61B0 (void);
// 0x00000697 System.Void BNG.Grabbable::RequestUnlockGrabbable()
extern void Grabbable_RequestUnlockGrabbable_m13479A09FB3084A71857893BEBD3146988476DA1 (void);
// 0x00000698 System.Void BNG.Grabbable::ResetLockResets()
extern void Grabbable_ResetLockResets_mA630724C6CC169D12AC103E379E1B6CA5ADF70C2 (void);
// 0x00000699 System.Void BNG.Grabbable::LockGrabbable(System.Boolean,System.Boolean,System.Boolean)
extern void Grabbable_LockGrabbable_m0B7196BB74F0FC5F7D449DD743A836F6595ABB4C (void);
// 0x0000069A System.Void BNG.Grabbable::UnlockGrabbable()
extern void Grabbable_UnlockGrabbable_m98A0C3AD2FFC4FE06C16C2FBBD7BAACB8345E69B (void);
// 0x0000069B System.Void BNG.Grabbable::OnCollisionStay(UnityEngine.Collision)
extern void Grabbable_OnCollisionStay_m1F7020127DEFF0B39FC3EF43D750A7D8B544EED4 (void);
// 0x0000069C System.Void BNG.Grabbable::OnCollisionEnter(UnityEngine.Collision)
extern void Grabbable_OnCollisionEnter_m30B58EFDA5E90709DCC394C6F80369239D2FEF1C (void);
// 0x0000069D System.Void BNG.Grabbable::OnCollisionExit(UnityEngine.Collision)
extern void Grabbable_OnCollisionExit_mE55B730A11F0D6C6CB7F473743F642CE585A6EF8 (void);
// 0x0000069E System.Void BNG.Grabbable::OnApplicationQuit()
extern void Grabbable_OnApplicationQuit_m7075D5CE29205FBF60773CB8166DAAC02739A7E9 (void);
// 0x0000069F System.Void BNG.Grabbable::OnDestroy()
extern void Grabbable_OnDestroy_mF1796ED5C1AFBDEA6E8FE9FFA216A654BD626E93 (void);
// 0x000006A0 System.Void BNG.Grabbable::OnDrawGizmosSelected()
extern void Grabbable_OnDrawGizmosSelected_m147D090E0B47ECBAF9325064C59E56D005BF1B86 (void);
// 0x000006A1 System.Void BNG.Grabbable::.ctor()
extern void Grabbable__ctor_mC6E92A845737C5C5A07E8009E1B6B85E7ADF634D (void);
// 0x000006A2 System.Void BNG.GrabbableChild::.ctor()
extern void GrabbableChild__ctor_m230EE8AC38B17A31E2D8B7719F4D27A502B77232 (void);
// 0x000006A3 System.Void BNG.GrabbableEvents::Awake()
extern void GrabbableEvents_Awake_m16300D28B226E3B589C8D6975A19EFAE29374A1C (void);
// 0x000006A4 System.Void BNG.GrabbableEvents::OnGrab(BNG.Grabber)
extern void GrabbableEvents_OnGrab_m3A05CCC25783C467CCDB81C322DE87DF275D35A3 (void);
// 0x000006A5 System.Void BNG.GrabbableEvents::OnRelease()
extern void GrabbableEvents_OnRelease_m6BDD7C143E37849459BB1859FBFD5114A7BB0ABC (void);
// 0x000006A6 System.Void BNG.GrabbableEvents::OnBecomesClosestGrabbable(BNG.ControllerHand)
extern void GrabbableEvents_OnBecomesClosestGrabbable_m6646B10925B1291A49C93FEDFC843C9BFB2C2CA7 (void);
// 0x000006A7 System.Void BNG.GrabbableEvents::OnBecomesClosestGrabbable(BNG.Grabber)
extern void GrabbableEvents_OnBecomesClosestGrabbable_m1F1244A0A1D864031B46C757A9227A5C9C0D4A1C (void);
// 0x000006A8 System.Void BNG.GrabbableEvents::OnNoLongerClosestGrabbable(BNG.ControllerHand)
extern void GrabbableEvents_OnNoLongerClosestGrabbable_m84FDE1FF9362141BF49712B24B9893CDF5707C9A (void);
// 0x000006A9 System.Void BNG.GrabbableEvents::OnNoLongerClosestGrabbable(BNG.Grabber)
extern void GrabbableEvents_OnNoLongerClosestGrabbable_mED434FAF6530B79F80BE4927248F8C55CAE40B3F (void);
// 0x000006AA System.Void BNG.GrabbableEvents::OnBecomesClosestRemoteGrabbable(BNG.ControllerHand)
extern void GrabbableEvents_OnBecomesClosestRemoteGrabbable_mBF87EA092E5885C6E13B479A7074DA00B00885D4 (void);
// 0x000006AB System.Void BNG.GrabbableEvents::OnBecomesClosestRemoteGrabbable(BNG.Grabber)
extern void GrabbableEvents_OnBecomesClosestRemoteGrabbable_mA9894E92464EAB14B2532DAF25203D4DE7800F4A (void);
// 0x000006AC System.Void BNG.GrabbableEvents::OnNoLongerClosestRemoteGrabbable(BNG.ControllerHand)
extern void GrabbableEvents_OnNoLongerClosestRemoteGrabbable_mA9439058D561DADE05C8747D41E3416C88052123 (void);
// 0x000006AD System.Void BNG.GrabbableEvents::OnNoLongerClosestRemoteGrabbable(BNG.Grabber)
extern void GrabbableEvents_OnNoLongerClosestRemoteGrabbable_m030E2169C9A89829D2AE1ABD30146ED817F7B44E (void);
// 0x000006AE System.Void BNG.GrabbableEvents::OnGrip(System.Single)
extern void GrabbableEvents_OnGrip_mA4A69BBB36F64A289D267C1BD4F1C5FB50BAA745 (void);
// 0x000006AF System.Void BNG.GrabbableEvents::OnTrigger(System.Single)
extern void GrabbableEvents_OnTrigger_mB19BD489E62C51248E465C43952B601FAC77E028 (void);
// 0x000006B0 System.Void BNG.GrabbableEvents::OnTriggerDown()
extern void GrabbableEvents_OnTriggerDown_m80B1286DD9DFAD1CDFB944643C8A5866AF63BA94 (void);
// 0x000006B1 System.Void BNG.GrabbableEvents::OnTriggerUp()
extern void GrabbableEvents_OnTriggerUp_mBE343B9EDA4BA874D59393935ECBEDD42A2CA2A9 (void);
// 0x000006B2 System.Void BNG.GrabbableEvents::OnButton1()
extern void GrabbableEvents_OnButton1_m876D37284D6FAB1B4863D2F85718CE96CA7A5818 (void);
// 0x000006B3 System.Void BNG.GrabbableEvents::OnButton1Down()
extern void GrabbableEvents_OnButton1Down_m85C0FABF7E09A3B0D534895621D66BDC0F47BD76 (void);
// 0x000006B4 System.Void BNG.GrabbableEvents::OnButton1Up()
extern void GrabbableEvents_OnButton1Up_mABB5D6348953192E56287D658F99ED19033D0625 (void);
// 0x000006B5 System.Void BNG.GrabbableEvents::OnButton2()
extern void GrabbableEvents_OnButton2_mE4ED9758FF318561AB14DE372C98F693E41FF128 (void);
// 0x000006B6 System.Void BNG.GrabbableEvents::OnButton2Down()
extern void GrabbableEvents_OnButton2Down_m9DD4C2009AFD65085C9D92A83EEF47DA98CED002 (void);
// 0x000006B7 System.Void BNG.GrabbableEvents::OnButton2Up()
extern void GrabbableEvents_OnButton2Up_m15E851F22E8B4FC73719C16C3C1A237E4CC2DE4E (void);
// 0x000006B8 System.Void BNG.GrabbableEvents::OnSnapZoneEnter()
extern void GrabbableEvents_OnSnapZoneEnter_m566BB9919D73CAA6B0380D3A6E2BDE839410BBCA (void);
// 0x000006B9 System.Void BNG.GrabbableEvents::OnSnapZoneExit()
extern void GrabbableEvents_OnSnapZoneExit_mEA0085319033EB6C2B3F3ECF2B249CD85156EBBF (void);
// 0x000006BA System.Void BNG.GrabbableEvents::.ctor()
extern void GrabbableEvents__ctor_m80801610DF42B99D03171148052B719B301D1BD1 (void);
// 0x000006BB System.Void BNG.FloatEvent::.ctor()
extern void FloatEvent__ctor_m8A9FA5A390670F2AD577D293CBDDAADE22853D51 (void);
// 0x000006BC System.Void BNG.FloatFloatEvent::.ctor()
extern void FloatFloatEvent__ctor_m5BCF4F985CADC8E14994E5A01185AB6A5B570FF0 (void);
// 0x000006BD System.Void BNG.GrabberEvent::.ctor()
extern void GrabberEvent__ctor_m789C6B162EB1B4D67000B926BF30C53EC7137204 (void);
// 0x000006BE System.Void BNG.GrabbableEvent::.ctor()
extern void GrabbableEvent__ctor_m02D8FF725C353C1AD06E5B1AD6A1BB678626A36F (void);
// 0x000006BF System.Void BNG.RaycastHitEvent::.ctor()
extern void RaycastHitEvent__ctor_m0BB0E55E0714170447616937DFAF6CEB1F117D1B (void);
// 0x000006C0 System.Void BNG.Vector2Event::.ctor()
extern void Vector2Event__ctor_m86573ED291DBEB689CBCB88C51852FC1ACEC0CBD (void);
// 0x000006C1 System.Void BNG.Vector3Event::.ctor()
extern void Vector3Event__ctor_m6CCC6493E1EF9F8DB74A2348251D601860EE1FB9 (void);
// 0x000006C2 System.Void BNG.PointerEventDataEvent::.ctor()
extern void PointerEventDataEvent__ctor_m2C3BA6FF9A476FB2CA5EBA05ADA7FF383231A293 (void);
// 0x000006C3 System.Void BNG.GrabbableUnityEvents::OnGrab(BNG.Grabber)
extern void GrabbableUnityEvents_OnGrab_m992FD6332DAE9075F08A167A4D6B32719D49A697 (void);
// 0x000006C4 System.Void BNG.GrabbableUnityEvents::OnRelease()
extern void GrabbableUnityEvents_OnRelease_m5AE17A470A11CFF11109D4D0BF7247A20B47E58F (void);
// 0x000006C5 System.Void BNG.GrabbableUnityEvents::OnBecomesClosestGrabbable(BNG.ControllerHand)
extern void GrabbableUnityEvents_OnBecomesClosestGrabbable_mEA4F7EF7F895436E7D8077447A605B5B3CD103A7 (void);
// 0x000006C6 System.Void BNG.GrabbableUnityEvents::OnNoLongerClosestGrabbable(BNG.ControllerHand)
extern void GrabbableUnityEvents_OnNoLongerClosestGrabbable_m7B1E01CCB83C397A978BEE1412DAD48E5F4BD152 (void);
// 0x000006C7 System.Void BNG.GrabbableUnityEvents::OnBecomesClosestRemoteGrabbable(BNG.ControllerHand)
extern void GrabbableUnityEvents_OnBecomesClosestRemoteGrabbable_m20FFB1C1E55CE812A70F6F8B7940CA83EA8B0F20 (void);
// 0x000006C8 System.Void BNG.GrabbableUnityEvents::OnNoLongerClosestRemoteGrabbable(BNG.ControllerHand)
extern void GrabbableUnityEvents_OnNoLongerClosestRemoteGrabbable_mA2F662498E607E048B02B4FCE8CB34513FB787C2 (void);
// 0x000006C9 System.Void BNG.GrabbableUnityEvents::OnGrip(System.Single)
extern void GrabbableUnityEvents_OnGrip_m386F5C308E986AA1485381673A2B74298F663104 (void);
// 0x000006CA System.Void BNG.GrabbableUnityEvents::OnTrigger(System.Single)
extern void GrabbableUnityEvents_OnTrigger_m15882625E2F3A0E175E6AD7D4BC9C140C21D6385 (void);
// 0x000006CB System.Void BNG.GrabbableUnityEvents::OnTriggerDown()
extern void GrabbableUnityEvents_OnTriggerDown_m1F1ABEC37952B4FD3D9C8165F3466C19627AA1B9 (void);
// 0x000006CC System.Void BNG.GrabbableUnityEvents::OnTriggerUp()
extern void GrabbableUnityEvents_OnTriggerUp_mF4B8729B5E2B4BC495BB99262C3D2E4B701298CA (void);
// 0x000006CD System.Void BNG.GrabbableUnityEvents::OnButton1()
extern void GrabbableUnityEvents_OnButton1_m727E035043C0A2F63B2F9A7E4B37931EF07E95F1 (void);
// 0x000006CE System.Void BNG.GrabbableUnityEvents::OnButton1Down()
extern void GrabbableUnityEvents_OnButton1Down_m1F0FA0B17A23A2D1900BABD179E308A6602266A5 (void);
// 0x000006CF System.Void BNG.GrabbableUnityEvents::OnButton1Up()
extern void GrabbableUnityEvents_OnButton1Up_m4736B453BD9852472F3F9F046F6F6172073D2810 (void);
// 0x000006D0 System.Void BNG.GrabbableUnityEvents::OnButton2()
extern void GrabbableUnityEvents_OnButton2_mC6E2BF5812076792C4F6BE60617A16CF86649E9D (void);
// 0x000006D1 System.Void BNG.GrabbableUnityEvents::OnButton2Down()
extern void GrabbableUnityEvents_OnButton2Down_m939839E3A95E47A52EE5AE2CFEA96B4EB3CC729D (void);
// 0x000006D2 System.Void BNG.GrabbableUnityEvents::OnButton2Up()
extern void GrabbableUnityEvents_OnButton2Up_m61C4D718A250FB503C69417AD596DB8D92D687C4 (void);
// 0x000006D3 System.Void BNG.GrabbableUnityEvents::OnSnapZoneEnter()
extern void GrabbableUnityEvents_OnSnapZoneEnter_m2601BAA82BAC722515106196D689E3B2623A72C0 (void);
// 0x000006D4 System.Void BNG.GrabbableUnityEvents::OnSnapZoneExit()
extern void GrabbableUnityEvents_OnSnapZoneExit_mD904BEC2C0DAF76DB800A0E177D637E8998D8794 (void);
// 0x000006D5 System.Void BNG.GrabbableUnityEvents::.ctor()
extern void GrabbableUnityEvents__ctor_m86AF32CE7330806B0EA387E6643357E82214C16E (void);
// 0x000006D6 System.Void BNG.GrabbablesInTrigger::Start()
extern void GrabbablesInTrigger_Start_mFE2A6E4FFD35EA627D96B83CD4DD17630446A3B8 (void);
// 0x000006D7 System.Void BNG.GrabbablesInTrigger::Update()
extern void GrabbablesInTrigger_Update_mD8419DC552935002CD3C613E24472C5427457D68 (void);
// 0x000006D8 System.Void BNG.GrabbablesInTrigger::updateClosestGrabbable()
extern void GrabbablesInTrigger_updateClosestGrabbable_m97A03BD03B28251EFCF5BCC9516A1FF75BAC9DA2 (void);
// 0x000006D9 System.Void BNG.GrabbablesInTrigger::updateClosestRemoteGrabbables()
extern void GrabbablesInTrigger_updateClosestRemoteGrabbables_mEA75423C51F7730A5DB2CB74B9192F536B196F25 (void);
// 0x000006DA BNG.Grabbable BNG.GrabbablesInTrigger::GetClosestGrabbable(System.Collections.Generic.Dictionary`2<UnityEngine.Collider,BNG.Grabbable>,System.Boolean,System.Boolean)
extern void GrabbablesInTrigger_GetClosestGrabbable_m24BBA6A2A8EE2BBAD28B77C14E070312DAAB4A4D (void);
// 0x000006DB System.Boolean BNG.GrabbablesInTrigger::CheckObjectBetweenGrabbable(UnityEngine.Vector3,BNG.Grabbable)
extern void GrabbablesInTrigger_CheckObjectBetweenGrabbable_m991D345C787EC4D6BE8906FE637B89589B13C3AF (void);
// 0x000006DC System.Collections.Generic.Dictionary`2<UnityEngine.Collider,BNG.Grabbable> BNG.GrabbablesInTrigger::GetValidGrabbables(System.Collections.Generic.Dictionary`2<UnityEngine.Collider,BNG.Grabbable>)
extern void GrabbablesInTrigger_GetValidGrabbables_m297F193E8A4C04979F9F610796FC5921F76BDCA3 (void);
// 0x000006DD System.Boolean BNG.GrabbablesInTrigger::isValidGrabbable(UnityEngine.Collider,BNG.Grabbable)
extern void GrabbablesInTrigger_isValidGrabbable_m0E6C113E2D8A0697AD8A9952E05F73F92167D93D (void);
// 0x000006DE System.Collections.Generic.Dictionary`2<UnityEngine.Collider,BNG.Grabbable> BNG.GrabbablesInTrigger::SanitizeGrabbables(System.Collections.Generic.Dictionary`2<UnityEngine.Collider,BNG.Grabbable>)
extern void GrabbablesInTrigger_SanitizeGrabbables_m81D62F641097063F710E16B889603E4BD9045CC9 (void);
// 0x000006DF System.Void BNG.GrabbablesInTrigger::AddNearbyGrabbable(UnityEngine.Collider,BNG.Grabbable)
extern void GrabbablesInTrigger_AddNearbyGrabbable_mB385FDD255A726B793CAA8ACF79FEA678BFB3727 (void);
// 0x000006E0 System.Void BNG.GrabbablesInTrigger::RemoveNearbyGrabbable(UnityEngine.Collider,BNG.Grabbable)
extern void GrabbablesInTrigger_RemoveNearbyGrabbable_m4497A4349870227E35762BEF55C96AEE34ECAB3B (void);
// 0x000006E1 System.Void BNG.GrabbablesInTrigger::RemoveNearbyGrabbable(BNG.Grabbable)
extern void GrabbablesInTrigger_RemoveNearbyGrabbable_m7D410E42E68CF798B6C98F8E1F82C88EFDF83B01 (void);
// 0x000006E2 System.Void BNG.GrabbablesInTrigger::AddValidRemoteGrabbable(UnityEngine.Collider,BNG.Grabbable)
extern void GrabbablesInTrigger_AddValidRemoteGrabbable_m7FE179B1CC64A27D08CA540ED3A2CE882DAEE5D5 (void);
// 0x000006E3 System.Void BNG.GrabbablesInTrigger::RemoveValidRemoteGrabbable(UnityEngine.Collider,BNG.Grabbable)
extern void GrabbablesInTrigger_RemoveValidRemoteGrabbable_m004DDC4387028CDBFA128A063019BF1B8B02FBA0 (void);
// 0x000006E4 System.Void BNG.GrabbablesInTrigger::OnTriggerEnter(UnityEngine.Collider)
extern void GrabbablesInTrigger_OnTriggerEnter_m95FF2C2D7AAD72222C1889D81315098D2BE48E8A (void);
// 0x000006E5 System.Void BNG.GrabbablesInTrigger::OnTriggerExit(UnityEngine.Collider)
extern void GrabbablesInTrigger_OnTriggerExit_m311247B15CC935FA17FA7372463C26D530A27E6B (void);
// 0x000006E6 System.Void BNG.GrabbablesInTrigger::.ctor()
extern void GrabbablesInTrigger__ctor_mA9BC1CBFCDFEA0CC038B7F11C0800E23E30A8D91 (void);
// 0x000006E7 System.Boolean BNG.Grabber::get_HoldingItem()
extern void Grabber_get_HoldingItem_mA918F4C86BA6A82D6616A34B4988B056E84735EC (void);
// 0x000006E8 System.Boolean BNG.Grabber::get_RemoteGrabbingItem()
extern void Grabber_get_RemoteGrabbingItem_mD7DBFB29A517F55C3C62EB66CA791CD33CD4FADC (void);
// 0x000006E9 BNG.GrabbablesInTrigger BNG.Grabber::get_GrabsInTrigger()
extern void Grabber_get_GrabsInTrigger_mA382FE101CBE5E96DF5D671327B43540FCCA346E (void);
// 0x000006EA BNG.Grabbable BNG.Grabber::get_RemoteGrabbingGrabbable()
extern void Grabber_get_RemoteGrabbingGrabbable_m210788C57E8FBF9F6402AC83AAF5E6A2EBB4371F (void);
// 0x000006EB UnityEngine.Vector3 BNG.Grabber::get_handsGraphicsGrabberOffset()
extern void Grabber_get_handsGraphicsGrabberOffset_mFAB29C48E66673C04802FFD831720CBCB5C9D0C3 (void);
// 0x000006EC System.Void BNG.Grabber::set_handsGraphicsGrabberOffset(UnityEngine.Vector3)
extern void Grabber_set_handsGraphicsGrabberOffset_m93905AB51A7DCF04E742557C86555E68418B5D3F (void);
// 0x000006ED UnityEngine.Vector3 BNG.Grabber::get_handsGraphicsGrabberOffsetRotation()
extern void Grabber_get_handsGraphicsGrabberOffsetRotation_mEBB2C0304794B7D4D7183C6FD0E4983B2DDECDBF (void);
// 0x000006EE System.Void BNG.Grabber::set_handsGraphicsGrabberOffsetRotation(UnityEngine.Vector3)
extern void Grabber_set_handsGraphicsGrabberOffsetRotation_mCF974D42B6106C73EC52147DC3FDA255D45FBD9A (void);
// 0x000006EF System.Void BNG.Grabber::Start()
extern void Grabber_Start_m41C8A5AAC797D8B5397A97A0A7DA8FA99C0F0898 (void);
// 0x000006F0 System.Void BNG.Grabber::Update()
extern void Grabber_Update_m50D9831F9AE1BB3C67DE6DFF4936E0CE877AAE90 (void);
// 0x000006F1 System.Void BNG.Grabber::updateFreshGrabStatus()
extern void Grabber_updateFreshGrabStatus_m387939703FDB04B51D944C8E6929ACC7F78AEBD6 (void);
// 0x000006F2 System.Void BNG.Grabber::checkGrabbableEvents()
extern void Grabber_checkGrabbableEvents_m2C6C703950C97302558CCB65CB41A91E9F3D23A8 (void);
// 0x000006F3 System.Boolean BNG.Grabber::InputCheckGrab()
extern void Grabber_InputCheckGrab_mB628A83862758A0C13333858D3E07788670BEF80 (void);
// 0x000006F4 System.Boolean BNG.Grabber::GetInputDownForGrabbable(BNG.Grabbable)
extern void Grabber_GetInputDownForGrabbable_m2F5633EA96AB9619ACFBDFA210529C798128BD51 (void);
// 0x000006F5 BNG.HoldType BNG.Grabber::getHoldType(BNG.Grabbable)
extern void Grabber_getHoldType_m9B9B1F14E75FE624ABBA6FF668E5D1B02FC97A39 (void);
// 0x000006F6 BNG.GrabButton BNG.Grabber::GetGrabButton(BNG.Grabbable)
extern void Grabber_GetGrabButton_m995CA336DC3BE522FD6FD95DD7D7EBBF2100B5F5 (void);
// 0x000006F7 BNG.Grabbable BNG.Grabber::getClosestOrRemote()
extern void Grabber_getClosestOrRemote_mB5AB5E7602DE93918C6605DAE2F1130CB33884AE (void);
// 0x000006F8 System.Boolean BNG.Grabber::inputCheckRelease()
extern void Grabber_inputCheckRelease_m86F9773052F844DA41CA925DE98426DC505715C1 (void);
// 0x000006F9 System.Single BNG.Grabber::getGrabInput(BNG.GrabButton)
extern void Grabber_getGrabInput_mA78C92796FB1DED24E53F375B230C5ED04E50ABF (void);
// 0x000006FA System.Boolean BNG.Grabber::getToggleInput(BNG.GrabButton)
extern void Grabber_getToggleInput_mC876FB41DFC3B1A88EDB352F75F42052FF1145E3 (void);
// 0x000006FB System.Boolean BNG.Grabber::TryGrab()
extern void Grabber_TryGrab_mB552AA0E4E68C5B66B2BE91A6A71DFABD725063F (void);
// 0x000006FC System.Void BNG.Grabber::GrabGrabbable(BNG.Grabbable)
extern void Grabber_GrabGrabbable_mD44ED05776B563B6617C84B9F8F123950A70D186 (void);
// 0x000006FD System.Void BNG.Grabber::DidDrop()
extern void Grabber_DidDrop_m510F5BE88AE92870D47F0E0D5BA01B59B4D9682C (void);
// 0x000006FE System.Void BNG.Grabber::HideHandGraphics()
extern void Grabber_HideHandGraphics_m8150A6D7F45936CEFEB28C9F76324809695F6805 (void);
// 0x000006FF System.Void BNG.Grabber::ResetHandGraphics()
extern void Grabber_ResetHandGraphics_m6063F04887618EEAC54E1718B2E4382A47272B3D (void);
// 0x00000700 System.Void BNG.Grabber::TryRelease()
extern void Grabber_TryRelease_m779EC399D0902860B3877358CC85F98A0A99DB1C (void);
// 0x00000701 System.Void BNG.Grabber::resetFlyingGrabbable()
extern void Grabber_resetFlyingGrabbable_m98D7A80A95A8363BFB12564DEA59A7B1A224E22A (void);
// 0x00000702 UnityEngine.Vector3 BNG.Grabber::GetGrabberAveragedVelocity()
extern void Grabber_GetGrabberAveragedVelocity_m5D3B141B0AA79E40833871D7B5AB3232F0049B43 (void);
// 0x00000703 UnityEngine.Vector3 BNG.Grabber::GetGrabberAveragedAngularVelocity()
extern void Grabber_GetGrabberAveragedAngularVelocity_mCA2AF0D698355D82C626B20DAE38E32FD4779D59 (void);
// 0x00000704 System.Void BNG.Grabber::.ctor()
extern void Grabber__ctor_m26535D953B7600FCCF91114A433319E5E6F46329 (void);
// 0x00000705 System.Void BNG.HandModelSelector::Start()
extern void HandModelSelector_Start_m5435C32074C11B0B1F7F6A654C2662DEA26B1D1C (void);
// 0x00000706 System.Void BNG.HandModelSelector::Update()
extern void HandModelSelector_Update_m539C53836509024959A96BDD90C3C870687F3989 (void);
// 0x00000707 System.Void BNG.HandModelSelector::CacheHandModels()
extern void HandModelSelector_CacheHandModels_m1DC93D421AF8A3FEFDAC22FDB9D92757ABDACC25 (void);
// 0x00000708 System.Void BNG.HandModelSelector::ChangeHandsModel(System.Int32,System.Boolean)
extern void HandModelSelector_ChangeHandsModel_m0EB1F7000CF7D5588670E8C9E84BC77CA62FC8F8 (void);
// 0x00000709 System.Void BNG.HandModelSelector::.ctor()
extern void HandModelSelector__ctor_m8583284E734224FD6DAF623E0CA1FF012F6E1D33 (void);
// 0x0000070A System.Void BNG.HeadCollisionFade::Start()
extern void HeadCollisionFade_Start_mF8F19CBF36E0B59ECDD88A5E3792DCE51B02156C (void);
// 0x0000070B System.Void BNG.HeadCollisionFade::LateUpdate()
extern void HeadCollisionFade_LateUpdate_m6F410A70B7DF7C276DCDC8CACAFB1CDD6C561E3D (void);
// 0x0000070C System.Void BNG.HeadCollisionFade::OnCollisionEnter(UnityEngine.Collision)
extern void HeadCollisionFade_OnCollisionEnter_mBC9DE278FEADB562DC8F426CC1566E3B3C6090B5 (void);
// 0x0000070D System.Void BNG.HeadCollisionFade::OnCollisionExit(UnityEngine.Collision)
extern void HeadCollisionFade_OnCollisionExit_m3340BB9C5E408E1B509FC7FC42C081672D036799 (void);
// 0x0000070E System.Void BNG.HeadCollisionFade::.ctor()
extern void HeadCollisionFade__ctor_mCAD93DB2A9725DAC0AAFEA56CCFF0F6090CE237A (void);
// 0x0000070F System.Void BNG.HeadCollisionMove::Start()
extern void HeadCollisionMove_Start_mD2809DD9305868C7F4C5B2CA25193A3A4396836E (void);
// 0x00000710 System.Void BNG.HeadCollisionMove::OnCollisionStay(UnityEngine.Collision)
extern void HeadCollisionMove_OnCollisionStay_m8C2D4AE4790BDA791575D2D8F02221AD854135EE (void);
// 0x00000711 System.Void BNG.HeadCollisionMove::OnCollisionExit(UnityEngine.Collision)
extern void HeadCollisionMove_OnCollisionExit_mCF8BF2BAD2FFB72DB1B64EE7830CA8E6785FEFB1 (void);
// 0x00000712 System.Collections.IEnumerator BNG.HeadCollisionMove::PushBackPlayer()
extern void HeadCollisionMove_PushBackPlayer_mEBEBB2B5F3AE37B78B064499C38ADC1BF6BD4066 (void);
// 0x00000713 System.Void BNG.HeadCollisionMove::.ctor()
extern void HeadCollisionMove__ctor_mC4C557E3A3962987555875DA5F3AD0C071CFAA29 (void);
// 0x00000714 System.Void BNG.HeadCollisionMove/<PushBackPlayer>d__8::.ctor(System.Int32)
extern void U3CPushBackPlayerU3Ed__8__ctor_m08C1160C1AD97D2B3E78428A0E2115815F1814D3 (void);
// 0x00000715 System.Void BNG.HeadCollisionMove/<PushBackPlayer>d__8::System.IDisposable.Dispose()
extern void U3CPushBackPlayerU3Ed__8_System_IDisposable_Dispose_m0E90D8943D62FD3470C5A27805895B517AD8EDCB (void);
// 0x00000716 System.Boolean BNG.HeadCollisionMove/<PushBackPlayer>d__8::MoveNext()
extern void U3CPushBackPlayerU3Ed__8_MoveNext_mA6F9566890C79654F3F7E7E392C961499C75C08E (void);
// 0x00000717 System.Object BNG.HeadCollisionMove/<PushBackPlayer>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CPushBackPlayerU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m61E82635C8C75A9B3882E0D696BB94EFC89C891D (void);
// 0x00000718 System.Void BNG.HeadCollisionMove/<PushBackPlayer>d__8::System.Collections.IEnumerator.Reset()
extern void U3CPushBackPlayerU3Ed__8_System_Collections_IEnumerator_Reset_m61BFC1A2BEB2E30EC0B565B496701E56408EE839 (void);
// 0x00000719 System.Object BNG.HeadCollisionMove/<PushBackPlayer>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CPushBackPlayerU3Ed__8_System_Collections_IEnumerator_get_Current_m8EFAE279F2B03DC2418C0EEDB5ED8FB1B4D84904 (void);
// 0x0000071A System.Void BNG.HingeHelper::Start()
extern void HingeHelper_Start_m4F24E52D7522552A44353F7718A18909BCF1AE81 (void);
// 0x0000071B System.Void BNG.HingeHelper::Update()
extern void HingeHelper_Update_mE4E02727B805F19526BA43B661EBE98DBEE69B66 (void);
// 0x0000071C System.Void BNG.HingeHelper::OnSnapChange(System.Single)
extern void HingeHelper_OnSnapChange_m463459440948D61DAD49693CFEF844C5DA7FEC60 (void);
// 0x0000071D System.Void BNG.HingeHelper::OnRelease()
extern void HingeHelper_OnRelease_mAFC29CFF6D0D271329CB8C6DCAF7DFEB8303D438 (void);
// 0x0000071E System.Void BNG.HingeHelper::OnHingeChange(System.Single)
extern void HingeHelper_OnHingeChange_mB60E4583045C4E593185C8439E03FC776F5B4914 (void);
// 0x0000071F System.Single BNG.HingeHelper::getSmoothedValue(System.Single)
extern void HingeHelper_getSmoothedValue_m689E9C2C092CC1FEBE5A1482CA210AA594A67BE4 (void);
// 0x00000720 System.Void BNG.HingeHelper::.ctor()
extern void HingeHelper__ctor_m14F9CFB5F7A7625DBC0CDC54401249E7094DBE92 (void);
// 0x00000721 BNG.InputBridge BNG.InputBridge::get_Instance()
extern void InputBridge_get_Instance_mDB8A03F2E5F5E928F3A274DE0BA381828B2D2A31 (void);
// 0x00000722 System.Single BNG.InputBridge::get_DownThreshold()
extern void InputBridge_get_DownThreshold_m2081BEBD133900473D927E47BA7761EF2A5BA4FE (void);
// 0x00000723 BNG.SDKProvider BNG.InputBridge::get_LoadedSDK()
extern void InputBridge_get_LoadedSDK_mCA5321762C1A21E05D77CCDA9A15EED63C205A26 (void);
// 0x00000724 System.Void BNG.InputBridge::set_LoadedSDK(BNG.SDKProvider)
extern void InputBridge_set_LoadedSDK_m6547C440E945B93F54973ECFF38F55809A78E705 (void);
// 0x00000725 System.Boolean BNG.InputBridge::get_IsOculusDevice()
extern void InputBridge_get_IsOculusDevice_mDCB5B70A79A11C56B73F121DA77AD2D91BB250DA (void);
// 0x00000726 System.Void BNG.InputBridge::set_IsOculusDevice(System.Boolean)
extern void InputBridge_set_IsOculusDevice_mBB0509C77C3AD75330903A1B0960D16365CBEF56 (void);
// 0x00000727 System.Boolean BNG.InputBridge::get_IsOculusQuest()
extern void InputBridge_get_IsOculusQuest_mD9CDF3DB23F8B270DB2C109525FE522A79D1C51A (void);
// 0x00000728 System.Void BNG.InputBridge::set_IsOculusQuest(System.Boolean)
extern void InputBridge_set_IsOculusQuest_mD0AFB82BCD37E923041574FB3356433AF0A11756 (void);
// 0x00000729 System.Boolean BNG.InputBridge::get_IsHTCDevice()
extern void InputBridge_get_IsHTCDevice_mCC833DD8884CB4B140782D3E42ED18F3BC9394DD (void);
// 0x0000072A System.Void BNG.InputBridge::set_IsHTCDevice(System.Boolean)
extern void InputBridge_set_IsHTCDevice_m414FE787559A603A01D52A9283668D29BF28CE27 (void);
// 0x0000072B System.Boolean BNG.InputBridge::get_IsPicoDevice()
extern void InputBridge_get_IsPicoDevice_mA7EAF3D69B5563F38FC2A8F8F697389B487CF328 (void);
// 0x0000072C System.Void BNG.InputBridge::set_IsPicoDevice(System.Boolean)
extern void InputBridge_set_IsPicoDevice_mC36E16FFEAEB0C3802DB4DC3C87AADFA78CA23E5 (void);
// 0x0000072D System.Boolean BNG.InputBridge::get_IsValveIndexController()
extern void InputBridge_get_IsValveIndexController_m4586B3DD662D12BEEC0036B6EBC72853C1986C1E (void);
// 0x0000072E System.Void BNG.InputBridge::set_IsValveIndexController(System.Boolean)
extern void InputBridge_set_IsValveIndexController_m9B7E917D503FBBB00FB5D5FF2D3595A46B1EA5CA (void);
// 0x0000072F System.Void BNG.InputBridge::add_OnInputsUpdated(BNG.InputBridge/InputsUpdatedAction)
extern void InputBridge_add_OnInputsUpdated_m8F00808E038DC995AB0FA7FBB8EC6499F720F64A (void);
// 0x00000730 System.Void BNG.InputBridge::remove_OnInputsUpdated(BNG.InputBridge/InputsUpdatedAction)
extern void InputBridge_remove_OnInputsUpdated_mC3A15696ABD1F8BC6307E08780E1AB49EC598100 (void);
// 0x00000731 System.Void BNG.InputBridge::add_OnControllerFound(BNG.InputBridge/ControllerFoundAction)
extern void InputBridge_add_OnControllerFound_m1F7C0C9508154BC1A1CD3A173343FF509EF4BCAF (void);
// 0x00000732 System.Void BNG.InputBridge::remove_OnControllerFound(BNG.InputBridge/ControllerFoundAction)
extern void InputBridge_remove_OnControllerFound_mB435B677AC080F64A2594FB243DD80C171BAA2F1 (void);
// 0x00000733 System.Void BNG.InputBridge::Awake()
extern void InputBridge_Awake_m5980A05FB2A1414A50519DB50CF79A2EAC9FBAE6 (void);
// 0x00000734 System.Void BNG.InputBridge::Start()
extern void InputBridge_Start_mA4C528622FFE046EC4E3011D43693CE229528791 (void);
// 0x00000735 System.Void BNG.InputBridge::OnEnable()
extern void InputBridge_OnEnable_mDB743EF29DBB4F02101BF98B8564745500252DF7 (void);
// 0x00000736 System.Void BNG.InputBridge::OnDisable()
extern void InputBridge_OnDisable_mED1F43BF82CBEA6D76754C4CAED6DF3F00F279D3 (void);
// 0x00000737 System.Void BNG.InputBridge::Update()
extern void InputBridge_Update_m68B16C061206959DC7D0D69A6E6014E8269D6172 (void);
// 0x00000738 System.Void BNG.InputBridge::UpdateInputs()
extern void InputBridge_UpdateInputs_m2936DD153830EFBBC25E8786CA7A6AF98F36FE08 (void);
// 0x00000739 System.Void BNG.InputBridge::UpdateSteamInput()
extern void InputBridge_UpdateSteamInput_m87EDE0234E000212A60522A4291717D4FFEC84D6 (void);
// 0x0000073A System.Void BNG.InputBridge::UpdateXRInput()
extern void InputBridge_UpdateXRInput_m450BC69DFED522F18481120DBC01BB7333F3E88D (void);
// 0x0000073B System.Void BNG.InputBridge::UpdateUnityInput()
extern void InputBridge_UpdateUnityInput_mC98EB65A4B678AD6F35617F0CB341D17F7EB7BD7 (void);
// 0x0000073C System.Void BNG.InputBridge::CreateUnityInputActions()
extern void InputBridge_CreateUnityInputActions_m211F9950ED065D049A0FCE39EAE472A068406BCA (void);
// 0x0000073D System.Void BNG.InputBridge::EnableActions()
extern void InputBridge_EnableActions_m695A68C04D10029B209AD6B4363C0FC380CF9376 (void);
// 0x0000073E System.Void BNG.InputBridge::DisableActions()
extern void InputBridge_DisableActions_mF2399458D38AC102473F83D6DCB121DC429FDA27 (void);
// 0x0000073F UnityEngine.InputSystem.InputAction BNG.InputBridge::CreateInputAction(System.String,System.String,System.Boolean)
extern void InputBridge_CreateInputAction_mD42D618DF2610E26A6654538D3E5D6C3FEDD52AF (void);
// 0x00000740 System.Void BNG.InputBridge::UpdateOVRInput()
extern void InputBridge_UpdateOVRInput_mB83BD672F9BA7AEC060656F21B46C227E69C92EE (void);
// 0x00000741 System.Void BNG.InputBridge::UpdatePicoInput()
extern void InputBridge_UpdatePicoInput_mF5BFE136091A62D906F2D108ABA320C161DAE14C (void);
// 0x00000742 System.Void BNG.InputBridge::UpdateDeviceActive()
extern void InputBridge_UpdateDeviceActive_mF5116FE517FABFB13F3820C4F98040092A2F0F9D (void);
// 0x00000743 System.Single BNG.InputBridge::correctValue(System.Single)
extern void InputBridge_correctValue_m71A24B32AA42AAE99AC6AC534359325E79E7EE9D (void);
// 0x00000744 System.Boolean BNG.InputBridge::GetControllerBindingValue(BNG.ControllerBinding)
extern void InputBridge_GetControllerBindingValue_mDDE0E9F0AD34D8CC626E3D9C6F3506772888BCBD (void);
// 0x00000745 System.Boolean BNG.InputBridge::GetGrabbedControllerBinding(BNG.GrabbedControllerBinding,BNG.ControllerHand)
extern void InputBridge_GetGrabbedControllerBinding_mDFB5639E819F2A9DBE9D6D967331CE44A5F0E7DF (void);
// 0x00000746 UnityEngine.Vector2 BNG.InputBridge::GetInputAxisValue(BNG.InputAxis)
extern void InputBridge_GetInputAxisValue_mF6A968A61135C8FEBF2E9FE7339293CA331A2CF7 (void);
// 0x00000747 UnityEngine.Vector2 BNG.InputBridge::ApplyDeadZones(UnityEngine.Vector2,System.Single,System.Single)
extern void InputBridge_ApplyDeadZones_mD0918C38EEBC9124E4919094215BAC98C9A291DB (void);
// 0x00000748 System.Void BNG.InputBridge::onDeviceChanged(UnityEngine.XR.InputDevice)
extern void InputBridge_onDeviceChanged_m2F69D8E65B501CDA5DB96A7D0EA968044D6C5201 (void);
// 0x00000749 System.Void BNG.InputBridge::setDeviceProperties()
extern void InputBridge_setDeviceProperties_mD577F7D76A64774DD96A34A230480CD0C5AA7808 (void);
// 0x0000074A System.Boolean BNG.InputBridge::GetSupportsXRInput()
extern void InputBridge_GetSupportsXRInput_m1015BCECA4DF0B9C4F8568740710FD6156394211 (void);
// 0x0000074B System.Boolean BNG.InputBridge::GetSupportsIndexTouch()
extern void InputBridge_GetSupportsIndexTouch_m519170D5C143D8595967E8F16002A7600BE568D9 (void);
// 0x0000074C BNG.SDKProvider BNG.InputBridge::GetLoadedSDK()
extern void InputBridge_GetLoadedSDK_m05B420FF161A3DBE133180D917AFAC0EA15DDCF3 (void);
// 0x0000074D System.Boolean BNG.InputBridge::GetSupportsThumbTouch()
extern void InputBridge_GetSupportsThumbTouch_mB5BAB7B0590A9AB38C64D058594EADA898D3D5B0 (void);
// 0x0000074E System.Boolean BNG.InputBridge::GetIsOculusDevice()
extern void InputBridge_GetIsOculusDevice_m5645AED5E336DDC6D5EE7F24EFE3621A021EFDC7 (void);
// 0x0000074F System.Boolean BNG.InputBridge::GetIsOculusQuest()
extern void InputBridge_GetIsOculusQuest_m173A1586EE231E7A767AA8270FB08DBBE65C0375 (void);
// 0x00000750 System.Boolean BNG.InputBridge::GetIsHTCDevice()
extern void InputBridge_GetIsHTCDevice_mA0EBE6FE00069E731D12D66C3B180754F793C9D7 (void);
// 0x00000751 System.Boolean BNG.InputBridge::GetIsPicoDevice()
extern void InputBridge_GetIsPicoDevice_mB9979C6BDB6A9782B5FC5922A6886DFD1B0FF0C0 (void);
// 0x00000752 UnityEngine.XR.InputDevice BNG.InputBridge::GetHMD()
extern void InputBridge_GetHMD_m12F10F1BA64538C8CA9331BB8398C07C179586E1 (void);
// 0x00000753 System.String BNG.InputBridge::GetHMDName()
extern void InputBridge_GetHMDName_m0E5284E5E6E4F9BC534D9CB917CD0E65519FE42F (void);
// 0x00000754 UnityEngine.Vector3 BNG.InputBridge::GetHMDLocalPosition()
extern void InputBridge_GetHMDLocalPosition_m49F666478367CE8AFB43E428ACCC19EADA2D6559 (void);
// 0x00000755 UnityEngine.Quaternion BNG.InputBridge::GetHMDLocalRotation()
extern void InputBridge_GetHMDLocalRotation_mCCA2E96090AC7492F724A02E613A692DCF9FB148 (void);
// 0x00000756 UnityEngine.XR.InputDevice BNG.InputBridge::GetLeftController()
extern void InputBridge_GetLeftController_m4F440B48E2CE2B815C5F9239370FB993926AAB8F (void);
// 0x00000757 UnityEngine.XR.InputDevice BNG.InputBridge::GetRightController()
extern void InputBridge_GetRightController_mAF423700F997707B8608D24953E79A85974F7A99 (void);
// 0x00000758 UnityEngine.Vector3 BNG.InputBridge::GetControllerLocalPosition(BNG.ControllerHand)
extern void InputBridge_GetControllerLocalPosition_m8BED058A73B3C685F0D6AB58CB96DD299782F7B1 (void);
// 0x00000759 UnityEngine.Quaternion BNG.InputBridge::GetControllerLocalRotation(BNG.ControllerHand)
extern void InputBridge_GetControllerLocalRotation_m5F41D9D401C9F923373C56959172520622A71CC2 (void);
// 0x0000075A BNG.ControllerType BNG.InputBridge::GetControllerType()
extern void InputBridge_GetControllerType_m70BE983CADCB5B7FD4E80E50F899683E6EE025B1 (void);
// 0x0000075B UnityEngine.Vector3 BNG.InputBridge::GetControllerVelocity(BNG.ControllerHand)
extern void InputBridge_GetControllerVelocity_mBF5EB84F3E2D9E2B31700393BCB44E2DA32102CF (void);
// 0x0000075C UnityEngine.Vector3 BNG.InputBridge::GetControllerAngularVelocity(BNG.ControllerHand)
extern void InputBridge_GetControllerAngularVelocity_m36E61CAFA4BF927264D030A0E0CA7C7B1B244FC9 (void);
// 0x0000075D System.String BNG.InputBridge::GetControllerName()
extern void InputBridge_GetControllerName_m839686D9E3462DA104B0EC43AA9AED9AB2E78877 (void);
// 0x0000075E System.Boolean BNG.InputBridge::GetIsValveIndexController()
extern void InputBridge_GetIsValveIndexController_m0C8CE2F94C6DEE092295553458FA6F00B13AAA21 (void);
// 0x0000075F System.Single BNG.InputBridge::getFeatureUsage(UnityEngine.XR.InputDevice,UnityEngine.XR.InputFeatureUsage`1<System.Single>,System.Boolean)
extern void InputBridge_getFeatureUsage_m32A73379A81829E1A48EB5116D6530AB864B146F (void);
// 0x00000760 System.Boolean BNG.InputBridge::getFeatureUsage(UnityEngine.XR.InputDevice,UnityEngine.XR.InputFeatureUsage`1<System.Boolean>)
extern void InputBridge_getFeatureUsage_m9DD72820A9B6414D8967DD8C0F4333EE46A903EF (void);
// 0x00000761 UnityEngine.Vector2 BNG.InputBridge::getFeatureUsage(UnityEngine.XR.InputDevice,UnityEngine.XR.InputFeatureUsage`1<UnityEngine.Vector2>)
extern void InputBridge_getFeatureUsage_mA10C64E3CF1855636758162B808A7C0208E6EA58 (void);
// 0x00000762 UnityEngine.Vector3 BNG.InputBridge::getFeatureUsage(UnityEngine.XR.InputDevice,UnityEngine.XR.InputFeatureUsage`1<UnityEngine.Vector3>)
extern void InputBridge_getFeatureUsage_m42872AD344088A3D67E248713FA1422B35AF56CF (void);
// 0x00000763 System.Void BNG.InputBridge::SetTrackingOriginMode(UnityEngine.XR.TrackingOriginModeFlags)
extern void InputBridge_SetTrackingOriginMode_mBC52018EFDFA97C54E12B913EDF97E82521BDCE7 (void);
// 0x00000764 System.Collections.IEnumerator BNG.InputBridge::changeOriginModeRoutine(UnityEngine.XR.TrackingOriginModeFlags)
extern void InputBridge_changeOriginModeRoutine_m0B400D890F0C20C44F747CAC345381F6950F0810 (void);
// 0x00000765 System.Void BNG.InputBridge::VibrateController(System.Single,System.Single,System.Single,BNG.ControllerHand)
extern void InputBridge_VibrateController_m91C479D60EEC4DD53CF7104757B209FE866020E4 (void);
// 0x00000766 System.Collections.IEnumerator BNG.InputBridge::Vibrate(System.Single,System.Single,System.Single,BNG.ControllerHand)
extern void InputBridge_Vibrate_m6A2F789AEF61524FB32629E5EFA09C498B61D219 (void);
// 0x00000767 System.Void BNG.InputBridge::.ctor()
extern void InputBridge__ctor_m2B7A25A6EC21AEBFD3BCFB0845699648BCDCFFD6 (void);
// 0x00000768 System.Void BNG.InputBridge::.cctor()
extern void InputBridge__cctor_m168EDC4B851DBFB39F9BE36EC0CA2363934484BA (void);
// 0x00000769 System.Void BNG.InputBridge/InputsUpdatedAction::.ctor(System.Object,System.IntPtr)
extern void InputsUpdatedAction__ctor_mC03BEFABA11960D878D93F24B66A3C4D8FE9BC1C (void);
// 0x0000076A System.Void BNG.InputBridge/InputsUpdatedAction::Invoke()
extern void InputsUpdatedAction_Invoke_mD52710CEC5DDCAB294DE2B0862B622A3D6F410DA (void);
// 0x0000076B System.IAsyncResult BNG.InputBridge/InputsUpdatedAction::BeginInvoke(System.AsyncCallback,System.Object)
extern void InputsUpdatedAction_BeginInvoke_mFCAA1268FA5D19025F97DC91DF7DB36B95EFE96D (void);
// 0x0000076C System.Void BNG.InputBridge/InputsUpdatedAction::EndInvoke(System.IAsyncResult)
extern void InputsUpdatedAction_EndInvoke_m10A789C1C18D695BBF6AEF959CBC352148E8FD43 (void);
// 0x0000076D System.Void BNG.InputBridge/ControllerFoundAction::.ctor(System.Object,System.IntPtr)
extern void ControllerFoundAction__ctor_m597CA54F761EF1EBED8CCDAAC320551DE53AD48D (void);
// 0x0000076E System.Void BNG.InputBridge/ControllerFoundAction::Invoke()
extern void ControllerFoundAction_Invoke_m1E0C4BD3BA15EBBED1C73D99B1A562F07116331F (void);
// 0x0000076F System.IAsyncResult BNG.InputBridge/ControllerFoundAction::BeginInvoke(System.AsyncCallback,System.Object)
extern void ControllerFoundAction_BeginInvoke_mAF4304154B7C7C5389798CD3A83D88194E9A80D1 (void);
// 0x00000770 System.Void BNG.InputBridge/ControllerFoundAction::EndInvoke(System.IAsyncResult)
extern void ControllerFoundAction_EndInvoke_mDE0C70E719B5543A4351D7789562E6C7DD485151 (void);
// 0x00000771 System.Void BNG.InputBridge/<changeOriginModeRoutine>d__178::.ctor(System.Int32)
extern void U3CchangeOriginModeRoutineU3Ed__178__ctor_m0154E8F7E7C48C34BEE3C52F5C942A4326CEBD06 (void);
// 0x00000772 System.Void BNG.InputBridge/<changeOriginModeRoutine>d__178::System.IDisposable.Dispose()
extern void U3CchangeOriginModeRoutineU3Ed__178_System_IDisposable_Dispose_mC8372952199F3333B013CE1C0549F5496ED8F688 (void);
// 0x00000773 System.Boolean BNG.InputBridge/<changeOriginModeRoutine>d__178::MoveNext()
extern void U3CchangeOriginModeRoutineU3Ed__178_MoveNext_mF924084F9F296566536DB7ED300D762825DC52F5 (void);
// 0x00000774 System.Object BNG.InputBridge/<changeOriginModeRoutine>d__178::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CchangeOriginModeRoutineU3Ed__178_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2863071A07D2B34FA46102AFC732308D5A23F1C9 (void);
// 0x00000775 System.Void BNG.InputBridge/<changeOriginModeRoutine>d__178::System.Collections.IEnumerator.Reset()
extern void U3CchangeOriginModeRoutineU3Ed__178_System_Collections_IEnumerator_Reset_m87FF77EBC47F08283E39EE2812F487DE05F7AC28 (void);
// 0x00000776 System.Object BNG.InputBridge/<changeOriginModeRoutine>d__178::System.Collections.IEnumerator.get_Current()
extern void U3CchangeOriginModeRoutineU3Ed__178_System_Collections_IEnumerator_get_Current_m5BF5C46F43F0E09BD6EB09EE9DE186BCDA2588D8 (void);
// 0x00000777 System.Void BNG.InputBridge/<Vibrate>d__180::.ctor(System.Int32)
extern void U3CVibrateU3Ed__180__ctor_m72198B9A716A339729494625E91499FB486A5A2A (void);
// 0x00000778 System.Void BNG.InputBridge/<Vibrate>d__180::System.IDisposable.Dispose()
extern void U3CVibrateU3Ed__180_System_IDisposable_Dispose_m1A3D42AD89DD04172E168797023E68CFCD301B29 (void);
// 0x00000779 System.Boolean BNG.InputBridge/<Vibrate>d__180::MoveNext()
extern void U3CVibrateU3Ed__180_MoveNext_mF3D514AFEBBB86BFDAAC85016882B554B5458AFE (void);
// 0x0000077A System.Object BNG.InputBridge/<Vibrate>d__180::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CVibrateU3Ed__180_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m52022956AD93A835A26B10247FCB43954547928D (void);
// 0x0000077B System.Void BNG.InputBridge/<Vibrate>d__180::System.Collections.IEnumerator.Reset()
extern void U3CVibrateU3Ed__180_System_Collections_IEnumerator_Reset_m8006401B424DFCB494ED2037BC961C65FEBB89A8 (void);
// 0x0000077C System.Object BNG.InputBridge/<Vibrate>d__180::System.Collections.IEnumerator.get_Current()
extern void U3CVibrateU3Ed__180_System_Collections_IEnumerator_get_Current_m3D0B18794FDFCAF1600FF960E27DFF60CFD44101 (void);
// 0x0000077D System.Void BNG.JoystickControl::Start()
extern void JoystickControl_Start_m1445DCAF3ED72538CD63CB7ACFEFF07C0CB89A08 (void);
// 0x0000077E System.Void BNG.JoystickControl::Update()
extern void JoystickControl_Update_mF9E9AD26E93A11E95216A622399C461188354E0A (void);
// 0x0000077F System.Void BNG.JoystickControl::FixedUpdate()
extern void JoystickControl_FixedUpdate_mBDC6F9432905A27BCD47512876F84BB0B7B522DF (void);
// 0x00000780 System.Void BNG.JoystickControl::doJoystickLook()
extern void JoystickControl_doJoystickLook_m2A3A22F4BA878C4BFAA84E61A9DBD1D8AAB3CD59 (void);
// 0x00000781 System.Void BNG.JoystickControl::OnJoystickChange(System.Single,System.Single)
extern void JoystickControl_OnJoystickChange_m1BF16DDE49BB06ED9579E665B505D4852F1C99C8 (void);
// 0x00000782 System.Void BNG.JoystickControl::OnJoystickChange(UnityEngine.Vector2)
extern void JoystickControl_OnJoystickChange_m57914546F0EAA6E117337F12B5CA51718FD610E4 (void);
// 0x00000783 System.Void BNG.JoystickControl::.ctor()
extern void JoystickControl__ctor_mED8CDC4B85D5AD9685C2DE07B1F1E8F53C65522A (void);
// 0x00000784 System.Void BNG.JoystickVehicleControl::Update()
extern void JoystickVehicleControl_Update_mA4A542866AEBCDD6CD7E07F5F6D717C2FC56D47E (void);
// 0x00000785 System.Void BNG.JoystickVehicleControl::CallJoystickEvents()
extern void JoystickVehicleControl_CallJoystickEvents_mE91FADA140ACDD9ED01006ECEC843DF2B678435D (void);
// 0x00000786 System.Void BNG.JoystickVehicleControl::OnJoystickChange(System.Single,System.Single)
extern void JoystickVehicleControl_OnJoystickChange_m79E94087854207DA2AC1AA9E92ED78CA7ADABCDB (void);
// 0x00000787 System.Void BNG.JoystickVehicleControl::OnJoystickChange(UnityEngine.Vector2)
extern void JoystickVehicleControl_OnJoystickChange_m5B4562BD133B96F957A4D85E227EB3CDE21848D0 (void);
// 0x00000788 System.Void BNG.JoystickVehicleControl::.ctor()
extern void JoystickVehicleControl__ctor_mAD07E831FFF7E44D7A35C5239C1A413987C8CCA4 (void);
// 0x00000789 System.Void BNG.Lever::Start()
extern void Lever_Start_mF6BD3FF0A1E598EAE55516AC87F8A5F28D08DE80 (void);
// 0x0000078A System.Void BNG.Lever::Awake()
extern void Lever_Awake_m9D4893FF25C1AD946567FF9E01C5E19096F86515 (void);
// 0x0000078B System.Void BNG.Lever::Update()
extern void Lever_Update_mC0D422FD574A0B68961A53721C5944CFF61D9A55 (void);
// 0x0000078C System.Single BNG.Lever::GetAnglePercentage(System.Single)
extern void Lever_GetAnglePercentage_m05401E2186E31F8B6C504E9DB52CA139A687ABD1 (void);
// 0x0000078D System.Void BNG.Lever::FixedUpdate()
extern void Lever_FixedUpdate_m38A8C59C66AFA0A56F7BF9F48BCAE6BC84883085 (void);
// 0x0000078E System.Void BNG.Lever::doLeverLook()
extern void Lever_doLeverLook_mCBB37EDBD406EC547C9B5F5330B72CBE05BCB9A3 (void);
// 0x0000078F System.Void BNG.Lever::SetLeverAngle(System.Single)
extern void Lever_SetLeverAngle_m0A4314CFAA7A6846D218D02D6D990600D5C546DD (void);
// 0x00000790 System.Void BNG.Lever::OnLeverChange(System.Single)
extern void Lever_OnLeverChange_m9DE75905AE1E5870C636CB4A034E628A26B6A024 (void);
// 0x00000791 System.Void BNG.Lever::OnLeverDown()
extern void Lever_OnLeverDown_mF1BC34C08B96042B2063FD0D1225AAB1BE11DA6F (void);
// 0x00000792 System.Void BNG.Lever::OnLeverUp()
extern void Lever_OnLeverUp_m4BFE33D0F16B4363FF0F082EB18C2DE829ED0E1B (void);
// 0x00000793 System.Void BNG.Lever::.ctor()
extern void Lever__ctor_mF8BD3C1EBA5AB75FC339D942AD210654EEF66760 (void);
// 0x00000794 BNG.LocomotionType BNG.LocomotionManager::get_SelectedLocomotion()
extern void LocomotionManager_get_SelectedLocomotion_m26B47CB9E812333C47C1520E9E85BE2D0885BA78 (void);
// 0x00000795 System.Void BNG.LocomotionManager::Start()
extern void LocomotionManager_Start_mBC6667D05E4A4E8CB01B090BF7AC1553AB406828 (void);
// 0x00000796 System.Void BNG.LocomotionManager::Update()
extern void LocomotionManager_Update_mFC83AA50CDE19A539FAF68A94E8C43B107BA251B (void);
// 0x00000797 System.Void BNG.LocomotionManager::CheckControllerToggleInput()
extern void LocomotionManager_CheckControllerToggleInput_mA6F79880F7BD82CB6F9A8F359F6BDB2A2A99C847 (void);
// 0x00000798 System.Void BNG.LocomotionManager::OnEnable()
extern void LocomotionManager_OnEnable_m24C944EBBCF2890B8741C3BEF66C6396287B38DE (void);
// 0x00000799 System.Void BNG.LocomotionManager::OnDisable()
extern void LocomotionManager_OnDisable_mFB9662766C1F2E4ABB76F279F3CC6E2B5511F9BC (void);
// 0x0000079A System.Void BNG.LocomotionManager::OnLocomotionToggle(UnityEngine.InputSystem.InputAction/CallbackContext)
extern void LocomotionManager_OnLocomotionToggle_mE059366592694E10105E89C02CD2EFF3AC7494CB (void);
// 0x0000079B System.Void BNG.LocomotionManager::LocomotionToggle()
extern void LocomotionManager_LocomotionToggle_mF621EFCA898B252B0833C646D96DD0A8BBAA4366 (void);
// 0x0000079C System.Void BNG.LocomotionManager::UpdateTeleportStatus()
extern void LocomotionManager_UpdateTeleportStatus_m48C5E981174D32EAAED5244BE1B829BF6E1A3EB4 (void);
// 0x0000079D System.Void BNG.LocomotionManager::ChangeLocomotion(BNG.LocomotionType,System.Boolean)
extern void LocomotionManager_ChangeLocomotion_mCBB8F576E09992D74600C2796D7E2043D3515CC6 (void);
// 0x0000079E System.Void BNG.LocomotionManager::ChangeLocomotionType(BNG.LocomotionType)
extern void LocomotionManager_ChangeLocomotionType_m0503D36C3B8139538E7D9E720D326C5494C7BB21 (void);
// 0x0000079F System.Void BNG.LocomotionManager::toggleTeleport(System.Boolean)
extern void LocomotionManager_toggleTeleport_m50256E600D35B417498941D5F11A6DABCD4E9A65 (void);
// 0x000007A0 System.Void BNG.LocomotionManager::toggleSmoothLocomotion(System.Boolean)
extern void LocomotionManager_toggleSmoothLocomotion_m380AD027FB28C5B5F961D46572698629B1E5824F (void);
// 0x000007A1 System.Void BNG.LocomotionManager::ToggleLocomotionType()
extern void LocomotionManager_ToggleLocomotionType_mC374E1A9D57E5767BCEED219A5E1D2615B1E4807 (void);
// 0x000007A2 System.Void BNG.LocomotionManager::.ctor()
extern void LocomotionManager__ctor_mF3D07E7728C3408BD4722554E233FC4716FD51FF (void);
// 0x000007A3 System.Boolean BNG.PlayerClimbing::get_IsRigidbodyPlayer()
extern void PlayerClimbing_get_IsRigidbodyPlayer_m36525AA9D6978819DDBBEF8AA7982D593668E831 (void);
// 0x000007A4 System.Void BNG.PlayerClimbing::Start()
extern void PlayerClimbing_Start_m5B72AFF8C0B7963D30417304F9C7769977260D4F (void);
// 0x000007A5 System.Void BNG.PlayerClimbing::LateUpdate()
extern void PlayerClimbing_LateUpdate_m255945F6B5575F59206E642BAF724A0ACC21A60D (void);
// 0x000007A6 System.Void BNG.PlayerClimbing::AddClimber(BNG.Climbable,BNG.Grabber)
extern void PlayerClimbing_AddClimber_mEB03045DE55A2CB924B25B6579C9F112B9945C74 (void);
// 0x000007A7 System.Void BNG.PlayerClimbing::RemoveClimber(BNG.Grabber)
extern void PlayerClimbing_RemoveClimber_mC9BD75CB41216D2FEEC65DF04607B53BE8A665DB (void);
// 0x000007A8 System.Boolean BNG.PlayerClimbing::GrippingAtLeastOneClimbable()
extern void PlayerClimbing_GrippingAtLeastOneClimbable_m3ED015E6A4C212F8924C5458151DCF3B8E5CC32A (void);
// 0x000007A9 System.Void BNG.PlayerClimbing::checkClimbing()
extern void PlayerClimbing_checkClimbing_m100386B3F13EC9A3D10E34342673173764EC37D0 (void);
// 0x000007AA System.Void BNG.PlayerClimbing::DoPhysicalClimbing()
extern void PlayerClimbing_DoPhysicalClimbing_mC18F3ECB6F02A7B0B0891E1DAD9FE23BC4BED1CA (void);
// 0x000007AB System.Void BNG.PlayerClimbing::onGrabbedClimbable()
extern void PlayerClimbing_onGrabbedClimbable_m53BB40E07C60A3ABA899A4ADB64F2113BEA54286 (void);
// 0x000007AC System.Void BNG.PlayerClimbing::onReleasedClimbable()
extern void PlayerClimbing_onReleasedClimbable_m5A065889B781F6E6D97CA2595FABC0FA5EA1A7CA (void);
// 0x000007AD System.Void BNG.PlayerClimbing::.ctor()
extern void PlayerClimbing__ctor_m753AA9613150B8919770397E353E94E53BD54C78 (void);
// 0x000007AE System.Void BNG.PlayerGravity::Start()
extern void PlayerGravity_Start_mB4879FC67F338329DC92BAC8CB41DBFF20E1F3BF (void);
// 0x000007AF System.Void BNG.PlayerGravity::LateUpdate()
extern void PlayerGravity_LateUpdate_m40AEF37F58E1FFB633C2FCCDD042C950FE0A7611 (void);
// 0x000007B0 System.Void BNG.PlayerGravity::FixedUpdate()
extern void PlayerGravity_FixedUpdate_m93E133642A4D9D04AA42C6DEFD799CF780122404 (void);
// 0x000007B1 System.Void BNG.PlayerGravity::ToggleGravity(System.Boolean)
extern void PlayerGravity_ToggleGravity_m0642CE5BADFABC298650446A8C3FDCCD357639A3 (void);
// 0x000007B2 System.Void BNG.PlayerGravity::.ctor()
extern void PlayerGravity__ctor_m29F80F82ABD1A4E673A3B75F519D080BD274A1B0 (void);
// 0x000007B3 System.Void BNG.PlayerMovingPlatformSupport::Start()
extern void PlayerMovingPlatformSupport_Start_m267915A4D7274B8EFC6C6875BCED0E2E185AEF4B (void);
// 0x000007B4 System.Void BNG.PlayerMovingPlatformSupport::Update()
extern void PlayerMovingPlatformSupport_Update_mCB5820A5887AAADAD18A9A94978D25E639B498C3 (void);
// 0x000007B5 System.Void BNG.PlayerMovingPlatformSupport::FixedUpdate()
extern void PlayerMovingPlatformSupport_FixedUpdate_mF3C9EC3E7AD27ADE6A89C5C0736CDAA708D357F2 (void);
// 0x000007B6 System.Void BNG.PlayerMovingPlatformSupport::CheckMovingPlatform()
extern void PlayerMovingPlatformSupport_CheckMovingPlatform_m972A47D29BEF03159118DA063EFB6D6EC789CE11 (void);
// 0x000007B7 System.Void BNG.PlayerMovingPlatformSupport::UpdateCurrentPlatform()
extern void PlayerMovingPlatformSupport_UpdateCurrentPlatform_m042D9C90F5F597FBC98CF8F28542446C0D00D45C (void);
// 0x000007B8 System.Void BNG.PlayerMovingPlatformSupport::UpdateDistanceFromGround()
extern void PlayerMovingPlatformSupport_UpdateDistanceFromGround_m9CB47BACB358D98FFEF0D9425C52BB7E686B86A3 (void);
// 0x000007B9 System.Void BNG.PlayerMovingPlatformSupport::.ctor()
extern void PlayerMovingPlatformSupport__ctor_m88AA9551118D68B718DBA5349B8C74E35EA942D2 (void);
// 0x000007BA System.Void BNG.PlayerRotation::add_OnBeforeRotate(BNG.PlayerRotation/OnBeforeRotateAction)
extern void PlayerRotation_add_OnBeforeRotate_mEBD292D98627CE23F9F67B7025F1B8239CD6FB79 (void);
// 0x000007BB System.Void BNG.PlayerRotation::remove_OnBeforeRotate(BNG.PlayerRotation/OnBeforeRotateAction)
extern void PlayerRotation_remove_OnBeforeRotate_mB3A8283F33E2EF4C12433F1FDF8212DA4C437922 (void);
// 0x000007BC System.Void BNG.PlayerRotation::add_OnAfterRotate(BNG.PlayerRotation/OnAfterRotateAction)
extern void PlayerRotation_add_OnAfterRotate_mEC3DA6A47940598800713029A4104155EFBBFCF5 (void);
// 0x000007BD System.Void BNG.PlayerRotation::remove_OnAfterRotate(BNG.PlayerRotation/OnAfterRotateAction)
extern void PlayerRotation_remove_OnAfterRotate_m962D0EB3785FE39DD113185D335853374F1AA117 (void);
// 0x000007BE System.Void BNG.PlayerRotation::Update()
extern void PlayerRotation_Update_mC37460909BF57BC8808C87477EB6775A6CA1474B (void);
// 0x000007BF System.Single BNG.PlayerRotation::GetAxisInput()
extern void PlayerRotation_GetAxisInput_mEA1E8D826BE9BA5C48F6AD8F33808A395901383C (void);
// 0x000007C0 System.Void BNG.PlayerRotation::DoSnapRotation(System.Single)
extern void PlayerRotation_DoSnapRotation_m05F34440F2920C585DBF38AB537AD529BDA300C6 (void);
// 0x000007C1 System.Boolean BNG.PlayerRotation::RecentlySnapTurned()
extern void PlayerRotation_RecentlySnapTurned_mA56719EB793972496259A4EC9619477E3DB2468F (void);
// 0x000007C2 System.Void BNG.PlayerRotation::DoSmoothRotation(System.Single)
extern void PlayerRotation_DoSmoothRotation_m132E2B2506AE89F1DFBF1BEF9F981F0EC6DD9C1C (void);
// 0x000007C3 System.Void BNG.PlayerRotation::.ctor()
extern void PlayerRotation__ctor_m3C3ED11159D38C5EB6FAAD3F37E89515290277AD (void);
// 0x000007C4 System.Void BNG.PlayerRotation/OnBeforeRotateAction::.ctor(System.Object,System.IntPtr)
extern void OnBeforeRotateAction__ctor_m733AFE124B79AFE2A5F18219AE40CB41AF22AE15 (void);
// 0x000007C5 System.Void BNG.PlayerRotation/OnBeforeRotateAction::Invoke()
extern void OnBeforeRotateAction_Invoke_mE17E479B50EDB533F7C46FB54A09A3E9C0F92E8C (void);
// 0x000007C6 System.IAsyncResult BNG.PlayerRotation/OnBeforeRotateAction::BeginInvoke(System.AsyncCallback,System.Object)
extern void OnBeforeRotateAction_BeginInvoke_m38EE8CEFA50ABAD2864A5FB8EABC13CD6329AC96 (void);
// 0x000007C7 System.Void BNG.PlayerRotation/OnBeforeRotateAction::EndInvoke(System.IAsyncResult)
extern void OnBeforeRotateAction_EndInvoke_m60AC27054049FB3256ADDC10325532441FA5A109 (void);
// 0x000007C8 System.Void BNG.PlayerRotation/OnAfterRotateAction::.ctor(System.Object,System.IntPtr)
extern void OnAfterRotateAction__ctor_mF81E6E61007FA10DA844E619EE262BF5C8E09DF6 (void);
// 0x000007C9 System.Void BNG.PlayerRotation/OnAfterRotateAction::Invoke()
extern void OnAfterRotateAction_Invoke_m2892DBBEC5077E3D6AA38367477B23B67DC2E6E8 (void);
// 0x000007CA System.IAsyncResult BNG.PlayerRotation/OnAfterRotateAction::BeginInvoke(System.AsyncCallback,System.Object)
extern void OnAfterRotateAction_BeginInvoke_m01B0EDF386CD90B800B1E0607E463C9A2856C38D (void);
// 0x000007CB System.Void BNG.PlayerRotation/OnAfterRotateAction::EndInvoke(System.IAsyncResult)
extern void OnAfterRotateAction_EndInvoke_m5DA489D12DB803F09337CC925E89C1A08B7A3836 (void);
// 0x000007CC UnityEngine.Transform BNG.PlayerTeleport::get_teleportTransform()
extern void PlayerTeleport_get_teleportTransform_mF214F00D3A564CB60EFB3907B48B390F99EEB7FD (void);
// 0x000007CD UnityEngine.Vector2 BNG.PlayerTeleport::get_handedThumbstickAxis()
extern void PlayerTeleport_get_handedThumbstickAxis_m8DDD505688A7823B032F27A6E8AB2A67BDE486BB (void);
// 0x000007CE System.Boolean BNG.PlayerTeleport::get_AimingTeleport()
extern void PlayerTeleport_get_AimingTeleport_m85DAE3B89D4351A52C8F485562FB040FB7DBC757 (void);
// 0x000007CF System.Void BNG.PlayerTeleport::add_OnBeforeTeleportFade(BNG.PlayerTeleport/OnBeforeTeleportFadeAction)
extern void PlayerTeleport_add_OnBeforeTeleportFade_mD9ED8AAAEBF1D615A1C4F116DFFE8AE7187DF32E (void);
// 0x000007D0 System.Void BNG.PlayerTeleport::remove_OnBeforeTeleportFade(BNG.PlayerTeleport/OnBeforeTeleportFadeAction)
extern void PlayerTeleport_remove_OnBeforeTeleportFade_m22C92A686C7A96F6FA38FEB4E2CA4BB89EA65103 (void);
// 0x000007D1 System.Void BNG.PlayerTeleport::add_OnBeforeTeleport(BNG.PlayerTeleport/OnBeforeTeleportAction)
extern void PlayerTeleport_add_OnBeforeTeleport_m70BFF304DD515BE9C43646663E60843CBE6BF3C9 (void);
// 0x000007D2 System.Void BNG.PlayerTeleport::remove_OnBeforeTeleport(BNG.PlayerTeleport/OnBeforeTeleportAction)
extern void PlayerTeleport_remove_OnBeforeTeleport_mED9B21414F6EE5B7359FF95C5596DF4FD8A8F90F (void);
// 0x000007D3 System.Void BNG.PlayerTeleport::add_OnAfterTeleport(BNG.PlayerTeleport/OnAfterTeleportAction)
extern void PlayerTeleport_add_OnAfterTeleport_m5C206A4B41BF8B7FE56BDD50F80A5059ABB68611 (void);
// 0x000007D4 System.Void BNG.PlayerTeleport::remove_OnAfterTeleport(BNG.PlayerTeleport/OnAfterTeleportAction)
extern void PlayerTeleport_remove_OnAfterTeleport_mC1004A815D0FB89D47E1FAABDE7FCCDFEFB69601 (void);
// 0x000007D5 System.Void BNG.PlayerTeleport::Start()
extern void PlayerTeleport_Start_m2AEA5DA25BCBE32486884EB3E5F701DA938335A8 (void);
// 0x000007D6 System.Void BNG.PlayerTeleport::OnEnable()
extern void PlayerTeleport_OnEnable_m7CAC35C718CF398F44F9C103FFDD25A39D2D4E71 (void);
// 0x000007D7 System.Void BNG.PlayerTeleport::setupVariables()
extern void PlayerTeleport_setupVariables_m44E8A8237F6B82E8B691853E0FE36E419AD138ED (void);
// 0x000007D8 System.Void BNG.PlayerTeleport::LateUpdate()
extern void PlayerTeleport_LateUpdate_mFB2E314FADF810CF1F2FC5027C811BFA39C9C74A (void);
// 0x000007D9 System.Void BNG.PlayerTeleport::DoCheckTeleport()
extern void PlayerTeleport_DoCheckTeleport_mF73FA56DFA031E6A7CE1D430D4E2DC9B519C46E4 (void);
// 0x000007DA System.Void BNG.PlayerTeleport::TryOrHideTeleport()
extern void PlayerTeleport_TryOrHideTeleport_m4F12F580C61DE99A284A44673C2E0FA6079CD327 (void);
// 0x000007DB System.Void BNG.PlayerTeleport::EnableTeleportation()
extern void PlayerTeleport_EnableTeleportation_mAC3EB704B839833292ECB4C1F6BAAED9596D3BE6 (void);
// 0x000007DC System.Void BNG.PlayerTeleport::DisableTeleportation()
extern void PlayerTeleport_DisableTeleportation_m2BB19E8462CCA888E464812CD9CE5E6F5AD847D7 (void);
// 0x000007DD System.Void BNG.PlayerTeleport::calculateParabola()
extern void PlayerTeleport_calculateParabola_m7830B1D508B592FFFA661FDAEBF2C7BACCD94F9F (void);
// 0x000007DE System.Boolean BNG.PlayerTeleport::teleportClear()
extern void PlayerTeleport_teleportClear_mFB066D26C47C8308F9FED6A833CBB7E249260227 (void);
// 0x000007DF System.Void BNG.PlayerTeleport::hideTeleport()
extern void PlayerTeleport_hideTeleport_m7D12D06234E6956ADB659CE9B3871C332FCD5AEA (void);
// 0x000007E0 System.Void BNG.PlayerTeleport::updateTeleport()
extern void PlayerTeleport_updateTeleport_m5882305B823F43A024B9E40375C179020DF20953 (void);
// 0x000007E1 System.Void BNG.PlayerTeleport::rotateMarker()
extern void PlayerTeleport_rotateMarker_mF6A76873B151A4D86A45215239F7720959A46CEB (void);
// 0x000007E2 System.Void BNG.PlayerTeleport::tryTeleport()
extern void PlayerTeleport_tryTeleport_mC7BC5335B3DD3203E5ED0217B9910D71CD733BA1 (void);
// 0x000007E3 System.Void BNG.PlayerTeleport::BeforeTeleportFade()
extern void PlayerTeleport_BeforeTeleportFade_m22F5853197AE551C15771D828B43395806303100 (void);
// 0x000007E4 System.Void BNG.PlayerTeleport::BeforeTeleport()
extern void PlayerTeleport_BeforeTeleport_mAE6BEC5C19D2A9D82F74DF25DF1FC07BF6D1AC8C (void);
// 0x000007E5 System.Void BNG.PlayerTeleport::AfterTeleport()
extern void PlayerTeleport_AfterTeleport_m3F78343E6D6BC3263150A3DC17FAD2CAA2470889 (void);
// 0x000007E6 System.Collections.IEnumerator BNG.PlayerTeleport::doTeleport(UnityEngine.Vector3,UnityEngine.Quaternion,System.Boolean)
extern void PlayerTeleport_doTeleport_m32D4C4948C071595A1BF2F3B9E4263CEB3FF366B (void);
// 0x000007E7 System.Void BNG.PlayerTeleport::TeleportPlayer(UnityEngine.Vector3,UnityEngine.Quaternion)
extern void PlayerTeleport_TeleportPlayer_mE95F215801FD5C67CBB55FCC44605E201A94CAD1 (void);
// 0x000007E8 System.Void BNG.PlayerTeleport::TeleportPlayerToTransform(UnityEngine.Transform)
extern void PlayerTeleport_TeleportPlayerToTransform_m1EF265F993EB67586D737E652C6474BFCE10127B (void);
// 0x000007E9 System.Boolean BNG.PlayerTeleport::KeyDownForTeleport()
extern void PlayerTeleport_KeyDownForTeleport_mEE22B5590752D600C91167C9171D36B42FE084BF (void);
// 0x000007EA System.Boolean BNG.PlayerTeleport::KeyUpFromTeleport()
extern void PlayerTeleport_KeyUpFromTeleport_mDC31A6CABC63A5C761FD059D00B920FB692F17F7 (void);
// 0x000007EB System.Void BNG.PlayerTeleport::OnDrawGizmosSelected()
extern void PlayerTeleport_OnDrawGizmosSelected_m3F138F284660F7DBA9244484DE1AF891F5817F34 (void);
// 0x000007EC System.Void BNG.PlayerTeleport::.ctor()
extern void PlayerTeleport__ctor_m9CD166A3231AA4C35293C9BE8056C9EBB8CF17E6 (void);
// 0x000007ED System.Void BNG.PlayerTeleport/OnBeforeTeleportFadeAction::.ctor(System.Object,System.IntPtr)
extern void OnBeforeTeleportFadeAction__ctor_m43DF800553234853DC5F69AB626A64102D995A70 (void);
// 0x000007EE System.Void BNG.PlayerTeleport/OnBeforeTeleportFadeAction::Invoke()
extern void OnBeforeTeleportFadeAction_Invoke_m014033EBB10CFC8EDAD1C2CD5DAAAC1B2E2DEF7F (void);
// 0x000007EF System.IAsyncResult BNG.PlayerTeleport/OnBeforeTeleportFadeAction::BeginInvoke(System.AsyncCallback,System.Object)
extern void OnBeforeTeleportFadeAction_BeginInvoke_mC50EE42F6F07916E953AFBDDC64E22283B27DFE8 (void);
// 0x000007F0 System.Void BNG.PlayerTeleport/OnBeforeTeleportFadeAction::EndInvoke(System.IAsyncResult)
extern void OnBeforeTeleportFadeAction_EndInvoke_mDA25DB39CE6782D067ABB57FF66648318447F90E (void);
// 0x000007F1 System.Void BNG.PlayerTeleport/OnBeforeTeleportAction::.ctor(System.Object,System.IntPtr)
extern void OnBeforeTeleportAction__ctor_m869BB218A0C8C0F7A7D02EB4B5748152FA1E3339 (void);
// 0x000007F2 System.Void BNG.PlayerTeleport/OnBeforeTeleportAction::Invoke()
extern void OnBeforeTeleportAction_Invoke_m3229BD04682D45A05DB23CCC353B37341530823F (void);
// 0x000007F3 System.IAsyncResult BNG.PlayerTeleport/OnBeforeTeleportAction::BeginInvoke(System.AsyncCallback,System.Object)
extern void OnBeforeTeleportAction_BeginInvoke_mAAA468D615EE3DF33B5F05EBCB94FB189F93C95A (void);
// 0x000007F4 System.Void BNG.PlayerTeleport/OnBeforeTeleportAction::EndInvoke(System.IAsyncResult)
extern void OnBeforeTeleportAction_EndInvoke_m974572E1B0C427CD88CC416BF3F2826A8D36E05D (void);
// 0x000007F5 System.Void BNG.PlayerTeleport/OnAfterTeleportAction::.ctor(System.Object,System.IntPtr)
extern void OnAfterTeleportAction__ctor_m144D46179D12B9E7B77909A360A614F1BC94C83E (void);
// 0x000007F6 System.Void BNG.PlayerTeleport/OnAfterTeleportAction::Invoke()
extern void OnAfterTeleportAction_Invoke_m0DE0EABCAAA58D51C3E2C6971658DAE0215438D6 (void);
// 0x000007F7 System.IAsyncResult BNG.PlayerTeleport/OnAfterTeleportAction::BeginInvoke(System.AsyncCallback,System.Object)
extern void OnAfterTeleportAction_BeginInvoke_mB81CBB51D9CC63BD1C580175B17B40A763086CEA (void);
// 0x000007F8 System.Void BNG.PlayerTeleport/OnAfterTeleportAction::EndInvoke(System.IAsyncResult)
extern void OnAfterTeleportAction_EndInvoke_mABEA5AE907CECDC60241DF05D874F33E15142FDF (void);
// 0x000007F9 System.Void BNG.PlayerTeleport/<doTeleport>d__83::.ctor(System.Int32)
extern void U3CdoTeleportU3Ed__83__ctor_mC7CB68F3EB217879E4D20BCCF0BF912D803A165C (void);
// 0x000007FA System.Void BNG.PlayerTeleport/<doTeleport>d__83::System.IDisposable.Dispose()
extern void U3CdoTeleportU3Ed__83_System_IDisposable_Dispose_mDDAD4DD996A850B509B088B6605CC5A483051E72 (void);
// 0x000007FB System.Boolean BNG.PlayerTeleport/<doTeleport>d__83::MoveNext()
extern void U3CdoTeleportU3Ed__83_MoveNext_m95097F669BA8A851FBE5FCA3E8134D2F9EEC7601 (void);
// 0x000007FC System.Object BNG.PlayerTeleport/<doTeleport>d__83::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CdoTeleportU3Ed__83_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB84B30B02B24E4C9FA947EBD401F66EAB4EEADB1 (void);
// 0x000007FD System.Void BNG.PlayerTeleport/<doTeleport>d__83::System.Collections.IEnumerator.Reset()
extern void U3CdoTeleportU3Ed__83_System_Collections_IEnumerator_Reset_mEC6E8F25628B1167BA69BF5492410C162D82E3D6 (void);
// 0x000007FE System.Object BNG.PlayerTeleport/<doTeleport>d__83::System.Collections.IEnumerator.get_Current()
extern void U3CdoTeleportU3Ed__83_System_Collections_IEnumerator_get_Current_mA6360FC5779A20C9E159B61FC2767947ED31A751 (void);
// 0x000007FF System.Void BNG.PointerEvents::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern void PointerEvents_OnPointerClick_mA0597E704B3636E00D96D2659E305E29FEB18799 (void);
// 0x00000800 System.Void BNG.PointerEvents::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void PointerEvents_OnPointerEnter_mC73A248D7EE074DC9837AD1A5C9D40E0D672C124 (void);
// 0x00000801 System.Void BNG.PointerEvents::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void PointerEvents_OnPointerExit_mC1B23407FA76BFB4CABF4E8EA78A6D7743F4C629 (void);
// 0x00000802 System.Void BNG.PointerEvents::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void PointerEvents_OnPointerDown_m7FF27624FA5D42DAB92DFB5F35ECFF040D05601B (void);
// 0x00000803 System.Void BNG.PointerEvents::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void PointerEvents_OnPointerUp_m61E064680AD74E710C3188C53A5C955968F224C2 (void);
// 0x00000804 System.Boolean BNG.PointerEvents::DistanceExceeded(UnityEngine.EventSystems.PointerEventData)
extern void PointerEvents_DistanceExceeded_mAC16DF23DE2415EF79DF917CB28267A2649A3236 (void);
// 0x00000805 System.Void BNG.PointerEvents::.ctor()
extern void PointerEvents__ctor_m5AD08409C0295E23405D5D68B3F5349411737D75 (void);
// 0x00000806 System.Void BNG.RemoteGrabber::Start()
extern void RemoteGrabber_Start_m9BACF630D3D2E00B5C0B7F67BC688335410CABF3 (void);
// 0x00000807 System.Void BNG.RemoteGrabber::Update()
extern void RemoteGrabber_Update_m3E43533A6AC51EF00CA3ABDF9E6C2B0E112DE75A (void);
// 0x00000808 System.Void BNG.RemoteGrabber::ObjectHit(UnityEngine.Collider)
extern void RemoteGrabber_ObjectHit_m34EDCA7C5C82A0D615DCB085661C65BBA0D239A0 (void);
// 0x00000809 System.Void BNG.RemoteGrabber::RemovePreviousHitObject()
extern void RemoteGrabber_RemovePreviousHitObject_m144838D42809F2153CEDADE1723ABC2F4A5005B3 (void);
// 0x0000080A System.Void BNG.RemoteGrabber::OnTriggerEnter(UnityEngine.Collider)
extern void RemoteGrabber_OnTriggerEnter_m82AAE00CED45B2AD2C9208C4E473ED31F796A054 (void);
// 0x0000080B System.Void BNG.RemoteGrabber::OnTriggerExit(UnityEngine.Collider)
extern void RemoteGrabber_OnTriggerExit_m2F68FCC7CBFFBCB309147965DC07A14BA8EC0079 (void);
// 0x0000080C System.Void BNG.RemoteGrabber::.ctor()
extern void RemoteGrabber__ctor_m8FA0B5E749F4F50F5FA6A6C0FF02241AF9A73BCC (void);
// 0x0000080D System.Single BNG.Slider::get_SlidePercentage()
extern void Slider_get_SlidePercentage_mDD5557043C513F08DD1400CC1F4C6910DFC94BBC (void);
// 0x0000080E System.Void BNG.Slider::Start()
extern void Slider_Start_m992F0EDA809BBEFDD6B47AA7664C3EAC52A29DD1 (void);
// 0x0000080F System.Void BNG.Slider::Update()
extern void Slider_Update_m29CA9A4B42B98E1784B763B715F8C526987765EA (void);
// 0x00000810 System.Void BNG.Slider::OnSliderChange(System.Single)
extern void Slider_OnSliderChange_mCD5E819F417B3B71A33C92F288DA678A65A95843 (void);
// 0x00000811 System.Void BNG.Slider::.ctor()
extern void Slider__ctor_m4DBA547A5B37B65FD5F31678E4750AA050B57F5F (void);
// 0x00000812 System.Void BNG.SmoothLocomotion::add_OnBeforeMove(BNG.SmoothLocomotion/OnBeforeMoveAction)
extern void SmoothLocomotion_add_OnBeforeMove_mAD779D5B2C7DE5561B09E2155664BC9EB66CF780 (void);
// 0x00000813 System.Void BNG.SmoothLocomotion::remove_OnBeforeMove(BNG.SmoothLocomotion/OnBeforeMoveAction)
extern void SmoothLocomotion_remove_OnBeforeMove_m6E98D62F6DF0372C8B1B2FC6278D3FB020E13B47 (void);
// 0x00000814 System.Void BNG.SmoothLocomotion::add_OnAfterMove(BNG.SmoothLocomotion/OnAfterMoveAction)
extern void SmoothLocomotion_add_OnAfterMove_m1615DD7DEF761D1DF017DE9C45AAD8F0BC12AC4A (void);
// 0x00000815 System.Void BNG.SmoothLocomotion::remove_OnAfterMove(BNG.SmoothLocomotion/OnAfterMoveAction)
extern void SmoothLocomotion_remove_OnAfterMove_m644B9E0CF62BEF150AC825DC7A76EBF01BF777E3 (void);
// 0x00000816 System.Void BNG.SmoothLocomotion::Update()
extern void SmoothLocomotion_Update_mBBE01A976A9D3E1AF5FB20961DD06CAFA0996CF6 (void);
// 0x00000817 System.Void BNG.SmoothLocomotion::FixedUpdate()
extern void SmoothLocomotion_FixedUpdate_mB7B1E63602EDE31D59C65F39C7E844A1784C87B7 (void);
// 0x00000818 System.Void BNG.SmoothLocomotion::CheckControllerReferences()
extern void SmoothLocomotion_CheckControllerReferences_mB4CA93B93A1A68942838F6807D873FFF82E10A87 (void);
// 0x00000819 System.Void BNG.SmoothLocomotion::UpdateInputs()
extern void SmoothLocomotion_UpdateInputs_m154451C47A85B4038D98B7596D5CDE626120ABD1 (void);
// 0x0000081A System.Void BNG.SmoothLocomotion::DoRigidBodyJump()
extern void SmoothLocomotion_DoRigidBodyJump_m4BFB14CEBC44F45C4C3E4C9072E70513ED7DCF04 (void);
// 0x0000081B UnityEngine.Vector2 BNG.SmoothLocomotion::GetMovementAxis()
extern void SmoothLocomotion_GetMovementAxis_m0A4B174246AC36B8F7DB845F5E5DA0F0873F5337 (void);
// 0x0000081C System.Void BNG.SmoothLocomotion::MoveCharacter()
extern void SmoothLocomotion_MoveCharacter_mC2EE8A27F7CD299D94DCAA73181F9540F15A9D64 (void);
// 0x0000081D System.Void BNG.SmoothLocomotion::MoveRigidCharacter(UnityEngine.Vector3)
extern void SmoothLocomotion_MoveRigidCharacter_m14016A2626D5F344E57456E8194C9B30E49A8BE1 (void);
// 0x0000081E System.Void BNG.SmoothLocomotion::MoveRigidCharacter()
extern void SmoothLocomotion_MoveRigidCharacter_m41D7C1EECFB4FC0017426B1EDF0B78B8023ED45A (void);
// 0x0000081F System.Void BNG.SmoothLocomotion::MoveCharacter(UnityEngine.Vector3)
extern void SmoothLocomotion_MoveCharacter_mD848740DDDF05DBC5A7BD947A51AA78886F5345B (void);
// 0x00000820 System.Boolean BNG.SmoothLocomotion::CheckJump()
extern void SmoothLocomotion_CheckJump_m55C3B3E4CB67B2C55AA04026BCE17DC69291A4C1 (void);
// 0x00000821 System.Boolean BNG.SmoothLocomotion::CheckSprint()
extern void SmoothLocomotion_CheckSprint_m310A3538226035911A29786C2F390084B7FBA4CB (void);
// 0x00000822 System.Boolean BNG.SmoothLocomotion::IsGrounded()
extern void SmoothLocomotion_IsGrounded_m3BD6160FDAEB468C426EE749CCC3D93E42AD889C (void);
// 0x00000823 System.Void BNG.SmoothLocomotion::SetupCharacterController()
extern void SmoothLocomotion_SetupCharacterController_mA70A22FFA62DBF8E6D8D5495D7295D635DA4BFB7 (void);
// 0x00000824 System.Void BNG.SmoothLocomotion::SetupRigidbodyPlayer()
extern void SmoothLocomotion_SetupRigidbodyPlayer_m3D2FB68A2B2F3073387145BC04454076B8E2B073 (void);
// 0x00000825 System.Void BNG.SmoothLocomotion::EnableMovement()
extern void SmoothLocomotion_EnableMovement_m270F5F4BE1EF0D8D2F4A84FA745F27F2518B0257 (void);
// 0x00000826 System.Void BNG.SmoothLocomotion::DisableMovement()
extern void SmoothLocomotion_DisableMovement_m28B0CBB7A70152CA216F2789BD97B29239EA5EED (void);
// 0x00000827 System.Void BNG.SmoothLocomotion::OnCollisionStay(UnityEngine.Collision)
extern void SmoothLocomotion_OnCollisionStay_m3851EC1329C51267B652898C5C6ACD5574DCF7F3 (void);
// 0x00000828 System.Void BNG.SmoothLocomotion::.ctor()
extern void SmoothLocomotion__ctor_m69F34A9C73372C67D41955079BF686A83FFB663E (void);
// 0x00000829 System.Void BNG.SmoothLocomotion/OnBeforeMoveAction::.ctor(System.Object,System.IntPtr)
extern void OnBeforeMoveAction__ctor_m4CB0B5517A245C08D1D742818D1054EB74A30A86 (void);
// 0x0000082A System.Void BNG.SmoothLocomotion/OnBeforeMoveAction::Invoke()
extern void OnBeforeMoveAction_Invoke_mEB85846742941F2CF0C05939DDD9FC39B2CC0311 (void);
// 0x0000082B System.IAsyncResult BNG.SmoothLocomotion/OnBeforeMoveAction::BeginInvoke(System.AsyncCallback,System.Object)
extern void OnBeforeMoveAction_BeginInvoke_mFA47CBDD8F0408F59EF0EAF68018913E4EA0427A (void);
// 0x0000082C System.Void BNG.SmoothLocomotion/OnBeforeMoveAction::EndInvoke(System.IAsyncResult)
extern void OnBeforeMoveAction_EndInvoke_mF139491E3F291413611517A33C0825CE32B2301E (void);
// 0x0000082D System.Void BNG.SmoothLocomotion/OnAfterMoveAction::.ctor(System.Object,System.IntPtr)
extern void OnAfterMoveAction__ctor_m5D5909C587EEC3AE83879A54A8B494FA32D9AC0F (void);
// 0x0000082E System.Void BNG.SmoothLocomotion/OnAfterMoveAction::Invoke()
extern void OnAfterMoveAction_Invoke_mC5D6A5932EF93FECE667AD90A05BD9E858577468 (void);
// 0x0000082F System.IAsyncResult BNG.SmoothLocomotion/OnAfterMoveAction::BeginInvoke(System.AsyncCallback,System.Object)
extern void OnAfterMoveAction_BeginInvoke_mAF8D34335BA2FED7FE9E19110B55B50F94277DCD (void);
// 0x00000830 System.Void BNG.SmoothLocomotion/OnAfterMoveAction::EndInvoke(System.IAsyncResult)
extern void OnAfterMoveAction_EndInvoke_m0D9D13E9A77D7D875DEE5CD95BBBEEA82A1EA494 (void);
// 0x00000831 System.Void BNG.SnapZone::Start()
extern void SnapZone_Start_mC415999D6A25E21F1C4335249EA6B0B97942C76D (void);
// 0x00000832 System.Void BNG.SnapZone::Update()
extern void SnapZone_Update_m6CB16F4F10AB31F35E3B2CE41DF3CE1D12894B03 (void);
// 0x00000833 BNG.Grabbable BNG.SnapZone::getClosestGrabbable()
extern void SnapZone_getClosestGrabbable_mAE5222BC1F161BA94DF3403716E931436EBE6AF4 (void);
// 0x00000834 System.Void BNG.SnapZone::GrabGrabbable(BNG.Grabbable)
extern void SnapZone_GrabGrabbable_m8CC39A7003ACB96F4375D38C71A17878B0646A20 (void);
// 0x00000835 System.Void BNG.SnapZone::disableGrabbable(BNG.Grabbable)
extern void SnapZone_disableGrabbable_m01395286E18A87EC2F528D18E0130239CA2D6891 (void);
// 0x00000836 System.Void BNG.SnapZone::GrabEquipped(BNG.Grabber)
extern void SnapZone_GrabEquipped_m19762FCD313B38D14A109AB937AAB7FB60214229 (void);
// 0x00000837 System.Boolean BNG.SnapZone::CanBeRemoved()
extern void SnapZone_CanBeRemoved_mC46BBB09A9EB63EF2190CA048E4AB85E42E51956 (void);
// 0x00000838 System.Void BNG.SnapZone::ReleaseAll()
extern void SnapZone_ReleaseAll_m44658CA73C0B328B5EDB279805402699D1BE662F (void);
// 0x00000839 System.Void BNG.SnapZone::.ctor()
extern void SnapZone__ctor_m27C8333659D2722A92CA9C4C349D153A549E1CF9 (void);
// 0x0000083A System.Void BNG.SnapZoneOffset::.ctor()
extern void SnapZoneOffset__ctor_m4C5C5A6864B47BDFC8150F521A2B15E1153F6A94 (void);
// 0x0000083B System.Void BNG.SnapZoneScale::.ctor()
extern void SnapZoneScale__ctor_m9512AB05C931E2E458A0840FE1EC2EEAC412E81E (void);
// 0x0000083C System.Single BNG.SteeringWheel::get_Angle()
extern void SteeringWheel_get_Angle_m7D5BD425D256B54294131C5F1B3059FE8DD20C18 (void);
// 0x0000083D System.Single BNG.SteeringWheel::get_RawAngle()
extern void SteeringWheel_get_RawAngle_m91A3726644A4CF5F7467800BFC6FA084C362D43B (void);
// 0x0000083E System.Single BNG.SteeringWheel::get_ScaleValue()
extern void SteeringWheel_get_ScaleValue_m4232332170D69C42B135BF0B56449F28C5A33B76 (void);
// 0x0000083F System.Single BNG.SteeringWheel::get_ScaleValueInverted()
extern void SteeringWheel_get_ScaleValueInverted_mCDE9E0A126617919A9536CC10DD258F7FD38ED74 (void);
// 0x00000840 System.Single BNG.SteeringWheel::get_AngleInverted()
extern void SteeringWheel_get_AngleInverted_mE81DFB9BA027FF6A5168DA2323B1DBFFAA5081FE (void);
// 0x00000841 BNG.Grabber BNG.SteeringWheel::get_PrimaryGrabber()
extern void SteeringWheel_get_PrimaryGrabber_mF3D5F3A9AB11A26C81CA07F617A7F22F0835AA7E (void);
// 0x00000842 BNG.Grabber BNG.SteeringWheel::get_SecondaryGrabber()
extern void SteeringWheel_get_SecondaryGrabber_m9A1638D742BA5372EDD5BCAC26FFCBD1D383712B (void);
// 0x00000843 System.Void BNG.SteeringWheel::Update()
extern void SteeringWheel_Update_mE38521DA886E5163496914F207DCF909D83D7E36 (void);
// 0x00000844 System.Void BNG.SteeringWheel::UpdateAngleCalculations()
extern void SteeringWheel_UpdateAngleCalculations_mCC23DA8519DD1400E518F91DF4E218CA1B5DAD0E (void);
// 0x00000845 System.Single BNG.SteeringWheel::GetRelativeAngle(UnityEngine.Vector3,UnityEngine.Vector3)
extern void SteeringWheel_GetRelativeAngle_mE28475C7A21349F427E55044713568F42A231321 (void);
// 0x00000846 System.Void BNG.SteeringWheel::ApplyAngleToSteeringWheel(System.Single)
extern void SteeringWheel_ApplyAngleToSteeringWheel_m4F31382FDBD3F7C7D8229BF2A4BE2FFDB2F7456D (void);
// 0x00000847 System.Void BNG.SteeringWheel::UpdatePreviewText()
extern void SteeringWheel_UpdatePreviewText_mC55BED46B93B01C0945E685B4A059C7AB64EACBF (void);
// 0x00000848 System.Void BNG.SteeringWheel::CallEvents()
extern void SteeringWheel_CallEvents_m41A5165FE40E5F7DE53E536F7E89D7DE1CB64E48 (void);
// 0x00000849 System.Void BNG.SteeringWheel::OnGrab(BNG.Grabber)
extern void SteeringWheel_OnGrab_m3537B7970BC77B6BDE7AB94B6E23E9259AC542C1 (void);
// 0x0000084A System.Void BNG.SteeringWheel::ReturnToCenterAngle()
extern void SteeringWheel_ReturnToCenterAngle_mBD2628731E08658D13096B234C7AE5E2D945383C (void);
// 0x0000084B BNG.Grabber BNG.SteeringWheel::GetPrimaryGrabber()
extern void SteeringWheel_GetPrimaryGrabber_m6161DAC4F9D0C81366A82DFFE80E0621CEA8A22F (void);
// 0x0000084C BNG.Grabber BNG.SteeringWheel::GetSecondaryGrabber()
extern void SteeringWheel_GetSecondaryGrabber_m007B5CA44DDC1E897AFA4D62EE2BE8FF85B56881 (void);
// 0x0000084D System.Void BNG.SteeringWheel::UpdatePreviousAngle(System.Single)
extern void SteeringWheel_UpdatePreviousAngle_m8C8822E655133FD8EF773B340104E24927D6F4AC (void);
// 0x0000084E System.Single BNG.SteeringWheel::GetScaledValue(System.Single,System.Single,System.Single)
extern void SteeringWheel_GetScaledValue_m23BE2DEBB3321D27E0F8355DA027A35E6F8B5381 (void);
// 0x0000084F System.Void BNG.SteeringWheel::.ctor()
extern void SteeringWheel__ctor_mA7E943C394B9CCC7B5819BDEFBEA803550DFC3B3 (void);
// 0x00000850 System.Void BNG.TrackedDevice::Awake()
extern void TrackedDevice_Awake_m94E6306A4B7D265F127E909C5159E6EE9A591C70 (void);
// 0x00000851 System.Void BNG.TrackedDevice::OnEnable()
extern void TrackedDevice_OnEnable_mB762C7B02E310D85F047075B609668B2DBD43498 (void);
// 0x00000852 System.Void BNG.TrackedDevice::OnDisable()
extern void TrackedDevice_OnDisable_mBA7AF6EA9CAF616A3227312979390BBCD2EABA4D (void);
// 0x00000853 System.Void BNG.TrackedDevice::Update()
extern void TrackedDevice_Update_mF1295DFC84BDD17EEA3D6D1E01B9E81D359F3585 (void);
// 0x00000854 System.Void BNG.TrackedDevice::FixedUpdate()
extern void TrackedDevice_FixedUpdate_m1F072E07B54554E27C5A366049052DA7E830DC01 (void);
// 0x00000855 System.Void BNG.TrackedDevice::RefreshDeviceStatus()
extern void TrackedDevice_RefreshDeviceStatus_m19E111F81F1A8E599B84BEF97EDB03C948570016 (void);
// 0x00000856 System.Void BNG.TrackedDevice::UpdateDevice()
extern void TrackedDevice_UpdateDevice_m6B0BB134FF4A6F9CA306E56A728F55C385A374A3 (void);
// 0x00000857 System.Void BNG.TrackedDevice::OnBeforeRender()
extern void TrackedDevice_OnBeforeRender_m207F3444CC3B818688FCAB95E2EDA84336862D1C (void);
// 0x00000858 System.Void BNG.TrackedDevice::.ctor()
extern void TrackedDevice__ctor_m653C53AE71D4992C56358CBB79184639D9C52B2C (void);
// 0x00000859 System.Void BNG.Arrow::Start()
extern void Arrow_Start_mACE000AD183FE265E758D93549FCAEDE3AF85C24 (void);
// 0x0000085A System.Void BNG.Arrow::FixedUpdate()
extern void Arrow_FixedUpdate_m96DFEA74E387C0D4F7970417F02996473C003196 (void);
// 0x0000085B System.Void BNG.Arrow::ShootArrow(UnityEngine.Vector3)
extern void Arrow_ShootArrow_mF6F56567CF1DB0D6EE58CAA0B53EF14230C07134 (void);
// 0x0000085C System.Collections.IEnumerator BNG.Arrow::QueueDestroy()
extern void Arrow_QueueDestroy_m4FD78102E06AC8862CA1C90A897FEDEFE0EAC12D (void);
// 0x0000085D System.Collections.IEnumerator BNG.Arrow::ReEnableCollider()
extern void Arrow_ReEnableCollider_m0F70A05EFEDFFE32C191651FED9B4B456E75732E (void);
// 0x0000085E System.Void BNG.Arrow::OnCollisionEnter(UnityEngine.Collision)
extern void Arrow_OnCollisionEnter_m835D0E1D37222AE76FAE92C41444C10449B897C1 (void);
// 0x0000085F System.Void BNG.Arrow::tryStickArrow(UnityEngine.Collision)
extern void Arrow_tryStickArrow_m033EB379024793DAEC0D0D1A9C8329D964D44CDC (void);
// 0x00000860 System.Void BNG.Arrow::playSoundInterval(System.Single,System.Single)
extern void Arrow_playSoundInterval_m1144E89FCC32A6CC508721E57C8B3050F4D4B3A4 (void);
// 0x00000861 System.Void BNG.Arrow::.ctor()
extern void Arrow__ctor_mB43957529F437443848F41C7E9AE2B6666535E81 (void);
// 0x00000862 System.Void BNG.Arrow/<QueueDestroy>d__14::.ctor(System.Int32)
extern void U3CQueueDestroyU3Ed__14__ctor_m922A344B5DCA80DED43AE5CC16AECA5446543B78 (void);
// 0x00000863 System.Void BNG.Arrow/<QueueDestroy>d__14::System.IDisposable.Dispose()
extern void U3CQueueDestroyU3Ed__14_System_IDisposable_Dispose_m9025E988B1CF725AC796F368CFE79A5E368C40D5 (void);
// 0x00000864 System.Boolean BNG.Arrow/<QueueDestroy>d__14::MoveNext()
extern void U3CQueueDestroyU3Ed__14_MoveNext_mE210CCA7AF752E12371C1414422CD6E143054FAF (void);
// 0x00000865 System.Object BNG.Arrow/<QueueDestroy>d__14::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CQueueDestroyU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m21AB1125753459BA0322D4BA486A032E61FC33D2 (void);
// 0x00000866 System.Void BNG.Arrow/<QueueDestroy>d__14::System.Collections.IEnumerator.Reset()
extern void U3CQueueDestroyU3Ed__14_System_Collections_IEnumerator_Reset_mA7BA7204A429BE8B1C8D8B2B9C0F45F36C912459 (void);
// 0x00000867 System.Object BNG.Arrow/<QueueDestroy>d__14::System.Collections.IEnumerator.get_Current()
extern void U3CQueueDestroyU3Ed__14_System_Collections_IEnumerator_get_Current_mB583407068D8C0B99FD8D209755D6729019AEA3B (void);
// 0x00000868 System.Void BNG.Arrow/<ReEnableCollider>d__15::.ctor(System.Int32)
extern void U3CReEnableColliderU3Ed__15__ctor_mE3FC175189E7FC481725563B65732AD1A89F50B1 (void);
// 0x00000869 System.Void BNG.Arrow/<ReEnableCollider>d__15::System.IDisposable.Dispose()
extern void U3CReEnableColliderU3Ed__15_System_IDisposable_Dispose_m28C7779546BF806504E78B1F9A9413D72C041099 (void);
// 0x0000086A System.Boolean BNG.Arrow/<ReEnableCollider>d__15::MoveNext()
extern void U3CReEnableColliderU3Ed__15_MoveNext_mF79E629D706251472C6175491227892588A37636 (void);
// 0x0000086B System.Object BNG.Arrow/<ReEnableCollider>d__15::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CReEnableColliderU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFDC4EA4610B1AD32F42F582397407D360C069A1C (void);
// 0x0000086C System.Void BNG.Arrow/<ReEnableCollider>d__15::System.Collections.IEnumerator.Reset()
extern void U3CReEnableColliderU3Ed__15_System_Collections_IEnumerator_Reset_m20D294F8767255E36FD654721876BA0BE40699B7 (void);
// 0x0000086D System.Object BNG.Arrow/<ReEnableCollider>d__15::System.Collections.IEnumerator.get_Current()
extern void U3CReEnableColliderU3Ed__15_System_Collections_IEnumerator_get_Current_mED43E716116B9AFCF45581A30EA8AE86AA635ACC (void);
// 0x0000086E System.Void BNG.ArrowGrabArea::Start()
extern void ArrowGrabArea_Start_mE827A10C7B60A39F880F16CF9AB52A0FC2A3983B (void);
// 0x0000086F System.Void BNG.ArrowGrabArea::OnTriggerEnter(UnityEngine.Collider)
extern void ArrowGrabArea_OnTriggerEnter_m7CD004B0C43D14D0E35DBD24F5C17A7C8B74EBED (void);
// 0x00000870 System.Void BNG.ArrowGrabArea::OnTriggerExit(UnityEngine.Collider)
extern void ArrowGrabArea_OnTriggerExit_mEC366B949998F0E41D3A1643F60FEECF29FE1523 (void);
// 0x00000871 System.Void BNG.ArrowGrabArea::.ctor()
extern void ArrowGrabArea__ctor_m05124CA3E40339C63292F72362E071FCD1705B05 (void);
// 0x00000872 System.Void BNG.AutoGrabGrabbable::OnBecomesClosestGrabbable(BNG.Grabber)
extern void AutoGrabGrabbable_OnBecomesClosestGrabbable_m5E2CD463F0B9BFEF686D17876B82D69702872287 (void);
// 0x00000873 System.Void BNG.AutoGrabGrabbable::.ctor()
extern void AutoGrabGrabbable__ctor_mE3EC80BF7CE854F653497000FE7EE4D5B0DE67ED (void);
// 0x00000874 System.Single BNG.Bow::get_DrawPercent()
extern void Bow_get_DrawPercent_mBDD2C6608A01C96B5E12C05244878CE03DA02865 (void);
// 0x00000875 System.Void BNG.Bow::set_DrawPercent(System.Single)
extern void Bow_set_DrawPercent_m17BCBAA4346B06BBC1D0216E8200F3E90F1A8A84 (void);
// 0x00000876 System.Void BNG.Bow::Start()
extern void Bow_Start_m9D0D06BFCC61BAA15AEE3C95DAB83B5C3F0C5D81 (void);
// 0x00000877 System.Void BNG.Bow::Update()
extern void Bow_Update_m5E5F1668FE1128C0AC561D7C47F9873F02CA6700 (void);
// 0x00000878 UnityEngine.Transform BNG.Bow::getArrowRest()
extern void Bow_getArrowRest_m624FA23820E53CD03FFE2AA07EDF0F72DB20E71D (void);
// 0x00000879 System.Boolean BNG.Bow::canGrabArrowFromKnock()
extern void Bow_canGrabArrowFromKnock_m84A1321BF13B12E5C14CEFD0556E8E37BC66F9DD (void);
// 0x0000087A System.Single BNG.Bow::getGrabArrowInput()
extern void Bow_getGrabArrowInput_m82015C94D7AE41F9328E950952F10144F2F70EC3 (void);
// 0x0000087B System.Single BNG.Bow::getGripInput(BNG.ControllerHand)
extern void Bow_getGripInput_m67270EC70AA2D1F591CC7AB1A4B5CDC32668A15D (void);
// 0x0000087C System.Single BNG.Bow::getTriggerInput(BNG.ControllerHand)
extern void Bow_getTriggerInput_m47BC7A14649E753414D49F56E6C8285D7F0B9CD4 (void);
// 0x0000087D System.Void BNG.Bow::setKnockPosition()
extern void Bow_setKnockPosition_m76559A5BC41A252A42104E539E2D9976F69B9F75 (void);
// 0x0000087E System.Void BNG.Bow::checkDrawSound()
extern void Bow_checkDrawSound_mA44801D915D394630923B46F4776BFCABA877D22 (void);
// 0x0000087F System.Void BNG.Bow::updateDrawDistance()
extern void Bow_updateDrawDistance_m5EA7C796FDE83F1AA532D96D9AA94F240E4ECF2F (void);
// 0x00000880 System.Void BNG.Bow::checkBowHaptics()
extern void Bow_checkBowHaptics_mBF3894B2410F12AD09250948CDD20E80297DC4ED (void);
// 0x00000881 System.Void BNG.Bow::resetStringPosition()
extern void Bow_resetStringPosition_mCD1878FB440B8FB26FBFB7CE0DDE0F29BA658737 (void);
// 0x00000882 System.Void BNG.Bow::alignArrow()
extern void Bow_alignArrow_m1C1B466B7862443373F6B104BC13BF124B5090CE (void);
// 0x00000883 System.Void BNG.Bow::alignBow()
extern void Bow_alignBow_mF845696336235C0598842E0DBD6A120208279F34 (void);
// 0x00000884 System.Void BNG.Bow::ResetBowAlignment()
extern void Bow_ResetBowAlignment_m14D5E4731B733CFDD76A5704D6257ACA88D71EB2 (void);
// 0x00000885 System.Void BNG.Bow::GrabArrow(BNG.Arrow)
extern void Bow_GrabArrow_m5EDAF162BAC2372358E551D4229DC71313B504FA (void);
// 0x00000886 System.Void BNG.Bow::ReleaseArrow()
extern void Bow_ReleaseArrow_mAC9F6C0B6A4195F35AC902DE370B24E056308D3D (void);
// 0x00000887 System.Void BNG.Bow::OnRelease()
extern void Bow_OnRelease_mA42A363984A7D17E02F8DAA5123A4BE1C08CE253 (void);
// 0x00000888 System.Void BNG.Bow::resetArrowValues()
extern void Bow_resetArrowValues_mAD7BFF3496FF983E98D93F9993234A25C58C4027 (void);
// 0x00000889 System.Void BNG.Bow::playSoundInterval(System.Single,System.Single,System.Single)
extern void Bow_playSoundInterval_mFF24C8B922FE92EF41DEE6D2F38332F31976DA79 (void);
// 0x0000088A System.Void BNG.Bow::playBowDraw()
extern void Bow_playBowDraw_mAB05F95A93820CDE0BA8FE1BF311649AC20ECD65 (void);
// 0x0000088B System.Void BNG.Bow::playBowRelease()
extern void Bow_playBowRelease_mC3A710EFF59CEAA91FA8B51D6750ABECC305F02D (void);
// 0x0000088C System.Void BNG.Bow::.ctor()
extern void Bow__ctor_m951BBDA0CAADAB88F232E69A416015153ABA6C19 (void);
// 0x0000088D System.Boolean BNG.Bow::<checkBowHaptics>b__43_0(BNG.DrawDefinition)
extern void Bow_U3CcheckBowHapticsU3Eb__43_0_mD0A0833A82F341279AA29E82AB578B22114EFEC1 (void);
// 0x0000088E System.Single BNG.DrawDefinition::get_DrawPercentage()
extern void DrawDefinition_get_DrawPercentage_m0AED0E7C8352ECA58AF2AB0AE30E09D119C59582 (void);
// 0x0000088F System.Void BNG.DrawDefinition::set_DrawPercentage(System.Single)
extern void DrawDefinition_set_DrawPercentage_mA2C4167F9F224F9D8A751D921D7F9CA7FAA1E33C (void);
// 0x00000890 System.Single BNG.DrawDefinition::get_HapticAmplitude()
extern void DrawDefinition_get_HapticAmplitude_mE2209B8ACF279D6EDAA148EA49FB273AA0DE7A5B (void);
// 0x00000891 System.Void BNG.DrawDefinition::set_HapticAmplitude(System.Single)
extern void DrawDefinition_set_HapticAmplitude_m06887EBAF0FA54B8BD9D291A2F3F34137F821BB7 (void);
// 0x00000892 System.Single BNG.DrawDefinition::get_HapticFrequency()
extern void DrawDefinition_get_HapticFrequency_m91963595ECE0CA71C8D428D311A7D86926FF8B18 (void);
// 0x00000893 System.Void BNG.DrawDefinition::set_HapticFrequency(System.Single)
extern void DrawDefinition_set_HapticFrequency_m9A734EC004E29CBFD8DD10085F279022979CD3E0 (void);
// 0x00000894 System.Void BNG.DrawDefinition::.ctor()
extern void DrawDefinition__ctor_mA2A66B7D89E40377508CF90B606D9EE4099783C4 (void);
// 0x00000895 System.Void BNG.BowArm::Start()
extern void BowArm_Start_m7E5917F8A4EDCB129047ED01728416FCB9B19176 (void);
// 0x00000896 System.Void BNG.BowArm::Update()
extern void BowArm_Update_mE5D893716591C6DDC36A6E5B505AE0995C562C93 (void);
// 0x00000897 System.Void BNG.BowArm::.ctor()
extern void BowArm__ctor_m4EF8D498B9AFB4FFDB5B518D6D00B77E6A9A5D2E (void);
// 0x00000898 System.Void BNG.BulletHole::Start()
extern void BulletHole_Start_mC54745D50973B60543474E27AD576C7F23A996F1 (void);
// 0x00000899 System.Void BNG.BulletHole::TryAttachTo(UnityEngine.Collider)
extern void BulletHole_TryAttachTo_m7EE9F913CB1FFDC013216BDD13448E36E6F34C45 (void);
// 0x0000089A System.Boolean BNG.BulletHole::transformIsEqualScale(UnityEngine.Transform)
extern void BulletHole_transformIsEqualScale_mF65371E8F6489A2E4F0B214D23BA50E94878B839 (void);
// 0x0000089B System.Void BNG.BulletHole::DestroySelf()
extern void BulletHole_DestroySelf_mD05C735B904461DEE908F0FDF37D19612F27AD52 (void);
// 0x0000089C System.Void BNG.BulletHole::.ctor()
extern void BulletHole__ctor_m5FFCA6934A65784C9E8E464D81D5C7580CD2F490 (void);
// 0x0000089D System.Void BNG.CalibratePlayerHeight::Start()
extern void CalibratePlayerHeight_Start_mAB3A842A49CFDFF603D79E0DBF8EE091F56F4AA4 (void);
// 0x0000089E System.Void BNG.CalibratePlayerHeight::CalibrateHeight()
extern void CalibratePlayerHeight_CalibrateHeight_mA5DD0F54DF03F859CBF273930206259269C8A186 (void);
// 0x0000089F System.Void BNG.CalibratePlayerHeight::CalibrateHeight(System.Single)
extern void CalibratePlayerHeight_CalibrateHeight_m8C57C0D63C220F1DDF55E35ECC5B7EA643617118 (void);
// 0x000008A0 System.Void BNG.CalibratePlayerHeight::ResetPlayerHeight()
extern void CalibratePlayerHeight_ResetPlayerHeight_m32DC98C98D7E6A1E94F594FFE7A57DDD48FD52F0 (void);
// 0x000008A1 System.Single BNG.CalibratePlayerHeight::GetCurrentPlayerHeight()
extern void CalibratePlayerHeight_GetCurrentPlayerHeight_mF145DBAE01C900DEF963170118CB33F9E7BF1AD9 (void);
// 0x000008A2 System.Void BNG.CalibratePlayerHeight::SetInitialOffset()
extern void CalibratePlayerHeight_SetInitialOffset_m0C95591906687F3EF68307AAB58877D60B8D3723 (void);
// 0x000008A3 System.Collections.IEnumerator BNG.CalibratePlayerHeight::setupInitialOffset()
extern void CalibratePlayerHeight_setupInitialOffset_m9394FF545D22A46926BBFA5C68F2394C232D41B8 (void);
// 0x000008A4 System.Void BNG.CalibratePlayerHeight::.ctor()
extern void CalibratePlayerHeight__ctor_m2500F880D743AC5ED5A9316D385BA188B20231D5 (void);
// 0x000008A5 System.Void BNG.CalibratePlayerHeight::<Start>b__5_0(UnityEngine.InputSystem.InputAction/CallbackContext)
extern void CalibratePlayerHeight_U3CStartU3Eb__5_0_mB706AC833CD8D8F38F4F975A9C8C754E59CBAE67 (void);
// 0x000008A6 System.Void BNG.CalibratePlayerHeight/<setupInitialOffset>d__11::.ctor(System.Int32)
extern void U3CsetupInitialOffsetU3Ed__11__ctor_mD7F1EB16000D56A59AF5AD7EA59A659751CB2E84 (void);
// 0x000008A7 System.Void BNG.CalibratePlayerHeight/<setupInitialOffset>d__11::System.IDisposable.Dispose()
extern void U3CsetupInitialOffsetU3Ed__11_System_IDisposable_Dispose_m88A359114955415273E8ABE95838D980E17A1C22 (void);
// 0x000008A8 System.Boolean BNG.CalibratePlayerHeight/<setupInitialOffset>d__11::MoveNext()
extern void U3CsetupInitialOffsetU3Ed__11_MoveNext_m3C4ACEEE590FA0E7B8B49F943182AB69A1459E32 (void);
// 0x000008A9 System.Object BNG.CalibratePlayerHeight/<setupInitialOffset>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CsetupInitialOffsetU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m13C3A6E2AEF617A3901F844FA128480E0E6EC564 (void);
// 0x000008AA System.Void BNG.CalibratePlayerHeight/<setupInitialOffset>d__11::System.Collections.IEnumerator.Reset()
extern void U3CsetupInitialOffsetU3Ed__11_System_Collections_IEnumerator_Reset_mFC402F746E7B773FD0900952870B6DE59927B4A3 (void);
// 0x000008AB System.Object BNG.CalibratePlayerHeight/<setupInitialOffset>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CsetupInitialOffsetU3Ed__11_System_Collections_IEnumerator_get_Current_mDD4F10151E03A4EBCD5A51B19E51512F0278E2EB (void);
// 0x000008AC System.Void BNG.CustomCenterOfMass::Start()
extern void CustomCenterOfMass_Start_mB9FC7963FDAF4219AC72C9B77F65F8243F7101DF (void);
// 0x000008AD System.Void BNG.CustomCenterOfMass::SetCenterOfMass(UnityEngine.Vector3)
extern void CustomCenterOfMass_SetCenterOfMass_m82E4F98426956AB9DF8EC19EFFE4ACFC28F4ECAD (void);
// 0x000008AE UnityEngine.Vector3 BNG.CustomCenterOfMass::getThisCenterOfMass()
extern void CustomCenterOfMass_getThisCenterOfMass_m62E50D006F2B7226FAC6D8248025945B28294DA8 (void);
// 0x000008AF System.Void BNG.CustomCenterOfMass::OnDrawGizmos()
extern void CustomCenterOfMass_OnDrawGizmos_mB796C717A58F974F3C4F63A3CA6B3B5C130C3AAA (void);
// 0x000008B0 System.Void BNG.CustomCenterOfMass::.ctor()
extern void CustomCenterOfMass__ctor_m15D2031B9C5EA367819170BDD52A4C9CBA361908 (void);
// 0x000008B1 System.Void BNG.DrawerSound::OnDrawerUpdate(System.Single)
extern void DrawerSound_OnDrawerUpdate_m73C9C4D9D8EA79FA50447E02DADD9302C9020FDD (void);
// 0x000008B2 System.Void BNG.DrawerSound::.ctor()
extern void DrawerSound__ctor_m19D538C794741DCE169D19512051733F9A11122E (void);
// 0x000008B3 System.Void BNG.Explosive::DoExplosion()
extern void Explosive_DoExplosion_mFF220F821E0445F3352D398ED269FE3722DD7B7C (void);
// 0x000008B4 System.Collections.IEnumerator BNG.Explosive::explosionRoutine()
extern void Explosive_explosionRoutine_m567F948B0AA00884D57487E5BA31BB6CF1846A22 (void);
// 0x000008B5 System.Collections.IEnumerator BNG.Explosive::dealDelayedDamaged(BNG.Damageable,System.Single)
extern void Explosive_dealDelayedDamaged_m4AFCB6647FAAB4AADAF76A05B0484A707DAFC9BA (void);
// 0x000008B6 System.Void BNG.Explosive::OnDrawGizmosSelected()
extern void Explosive_OnDrawGizmosSelected_m164D43FD92EE29B88BA4F06CD182CDBD5B574541 (void);
// 0x000008B7 System.Void BNG.Explosive::.ctor()
extern void Explosive__ctor_m7552584D30FC2DCBBF1CED3ED9CFCFD11BFBC45E (void);
// 0x000008B8 System.Void BNG.Explosive/<explosionRoutine>d__6::.ctor(System.Int32)
extern void U3CexplosionRoutineU3Ed__6__ctor_m4D2C0AE2979426A68085DD2D67C2523DC9D22F0E (void);
// 0x000008B9 System.Void BNG.Explosive/<explosionRoutine>d__6::System.IDisposable.Dispose()
extern void U3CexplosionRoutineU3Ed__6_System_IDisposable_Dispose_m61BF921FBD39692E4F3B47362ED457A002E06B87 (void);
// 0x000008BA System.Boolean BNG.Explosive/<explosionRoutine>d__6::MoveNext()
extern void U3CexplosionRoutineU3Ed__6_MoveNext_m8E1CE453031BB0723722A11727CFCBEA0699027E (void);
// 0x000008BB System.Object BNG.Explosive/<explosionRoutine>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CexplosionRoutineU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0E290262394572A563762816CF297A8C1D705CD0 (void);
// 0x000008BC System.Void BNG.Explosive/<explosionRoutine>d__6::System.Collections.IEnumerator.Reset()
extern void U3CexplosionRoutineU3Ed__6_System_Collections_IEnumerator_Reset_m066FD066123C85628E1A57D1C874A54757983522 (void);
// 0x000008BD System.Object BNG.Explosive/<explosionRoutine>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CexplosionRoutineU3Ed__6_System_Collections_IEnumerator_get_Current_m0E6107B06543A67A36F43A5AA2D5772D706CAB0B (void);
// 0x000008BE System.Void BNG.Explosive/<dealDelayedDamaged>d__7::.ctor(System.Int32)
extern void U3CdealDelayedDamagedU3Ed__7__ctor_m7B98EC93310F920F79F326050F9719FBABE62BF2 (void);
// 0x000008BF System.Void BNG.Explosive/<dealDelayedDamaged>d__7::System.IDisposable.Dispose()
extern void U3CdealDelayedDamagedU3Ed__7_System_IDisposable_Dispose_m58B49841EE49D730E6581279F45813A7565E3F71 (void);
// 0x000008C0 System.Boolean BNG.Explosive/<dealDelayedDamaged>d__7::MoveNext()
extern void U3CdealDelayedDamagedU3Ed__7_MoveNext_mE9F4E293497DB94BDC8326C997A846846A279B4D (void);
// 0x000008C1 System.Object BNG.Explosive/<dealDelayedDamaged>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CdealDelayedDamagedU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB5012D37CE1E7B6FBC3AA13D9EADC6F27D948976 (void);
// 0x000008C2 System.Void BNG.Explosive/<dealDelayedDamaged>d__7::System.Collections.IEnumerator.Reset()
extern void U3CdealDelayedDamagedU3Ed__7_System_Collections_IEnumerator_Reset_mE80C6FE5262AEB6EBDDE0B19D7425018F6B3152B (void);
// 0x000008C3 System.Object BNG.Explosive/<dealDelayedDamaged>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CdealDelayedDamagedU3Ed__7_System_Collections_IEnumerator_get_Current_m8638F2DCFA1C9C87DC77B7B27AD1BED7B2076D53 (void);
// 0x000008C4 System.Boolean BNG.ExtensionMethods::GetDown(BNG.ControllerBinding)
extern void ExtensionMethods_GetDown_m5A0B33F9B42B036DD7D6BC7B5E977DBD0090B82E (void);
// 0x000008C5 System.Void BNG.FPSText::Start()
extern void FPSText_Start_m5A9E4FBF80912EF14B782495DF169D754F348554 (void);
// 0x000008C6 System.Void BNG.FPSText::Update()
extern void FPSText_Update_mC90A6D7A9AF5874C36C1021B4B35D68464C7E2C4 (void);
// 0x000008C7 System.Void BNG.FPSText::OnGUI()
extern void FPSText_OnGUI_m1B678EF8177CC715464D1EC56EF3B1B1F16863C2 (void);
// 0x000008C8 System.Void BNG.FPSText::.ctor()
extern void FPSText__ctor_mBF8B285D21AAE9E714C43B46787723ABD45102C1 (void);
// 0x000008C9 System.Void BNG.Flashlight::Start()
extern void Flashlight_Start_m265B0F0FDE1DAF53DF6D4E29239218828176267A (void);
// 0x000008CA System.Void BNG.Flashlight::OnTrigger(System.Single)
extern void Flashlight_OnTrigger_mAF8A2B53A2CB89754AB1548AC77FFB13C384B432 (void);
// 0x000008CB System.Void BNG.Flashlight::OnTriggerUp()
extern void Flashlight_OnTriggerUp_mE4226B596A18244455F65658D848C30B068E501D (void);
// 0x000008CC System.Void BNG.Flashlight::.ctor()
extern void Flashlight__ctor_m13C9931920C72C38F06C9064D2C0C03E2B489B61 (void);
// 0x000008CD System.Void BNG.GrappleShot::Start()
extern void GrappleShot_Start_mBAA3EEB4B9FD27E566A8A3503968D68BFBAF380F (void);
// 0x000008CE System.Void BNG.GrappleShot::LateUpdate()
extern void GrappleShot_LateUpdate_m34C3F1CCCAA0929DB587F95224039BF1F504EB73 (void);
// 0x000008CF System.Void BNG.GrappleShot::OnTrigger(System.Single)
extern void GrappleShot_OnTrigger_m53462BFD6F0CD5630BBC8FCFF5014CBFE9562291 (void);
// 0x000008D0 System.Void BNG.GrappleShot::updateGrappleDistance()
extern void GrappleShot_updateGrappleDistance_m0235D09E0E10C35443C2BBCD23EC27259D6AF789 (void);
// 0x000008D1 System.Void BNG.GrappleShot::OnGrab(BNG.Grabber)
extern void GrappleShot_OnGrab_m9E7617878B6DBE2266F1233B14DE8C161D1EA7B0 (void);
// 0x000008D2 System.Void BNG.GrappleShot::OnRelease()
extern void GrappleShot_OnRelease_mD926B210B95C7BC4E349DF5B3CC85F350D4AAB4F (void);
// 0x000008D3 System.Void BNG.GrappleShot::onReleaseGrapple()
extern void GrappleShot_onReleaseGrapple_mB6EBBB0232735EF07157B95A60143F641F331F27 (void);
// 0x000008D4 System.Void BNG.GrappleShot::drawGrappleHelper()
extern void GrappleShot_drawGrappleHelper_mCABB9DB5F6610D22CB45BA44170BCB00DD2B8FEB (void);
// 0x000008D5 System.Void BNG.GrappleShot::drawGrappleLine()
extern void GrappleShot_drawGrappleLine_mCDFFB7A5D1A70B6B395805C46C3F2E8B481B4F5D (void);
// 0x000008D6 System.Void BNG.GrappleShot::hideGrappleLine()
extern void GrappleShot_hideGrappleLine_mA753CECEEB84617D1935DF3888EF735CEEA43D4E (void);
// 0x000008D7 System.Void BNG.GrappleShot::showGrappleHelper(UnityEngine.Vector3,UnityEngine.Quaternion)
extern void GrappleShot_showGrappleHelper_m6A7E2FAEB57BE6EB5128C99F5409EF31D6C4D8CC (void);
// 0x000008D8 System.Void BNG.GrappleShot::hideGrappleHelper()
extern void GrappleShot_hideGrappleHelper_mFD118BE1C008E9D1A0968C2456E60840188B4C80 (void);
// 0x000008D9 System.Void BNG.GrappleShot::reelInGrapple(System.Single)
extern void GrappleShot_reelInGrapple_mE8229D69408856FEE14CF8D60670C1A0E8422467 (void);
// 0x000008DA System.Void BNG.GrappleShot::shootGrapple()
extern void GrappleShot_shootGrapple_m3D72B56DEAE9BA0EC4D900251FB640D16FD46414 (void);
// 0x000008DB System.Void BNG.GrappleShot::dropGrapple()
extern void GrappleShot_dropGrapple_m704F5C25242EF60BC41FC699BD71987B65199E00 (void);
// 0x000008DC System.Void BNG.GrappleShot::changeGravity(System.Boolean)
extern void GrappleShot_changeGravity_m8A73ABA947D4FB33A74BC65118C682BB2CB1F083 (void);
// 0x000008DD System.Void BNG.GrappleShot::.ctor()
extern void GrappleShot__ctor_m8392AB16FFE0677FE377EBF2AA09EE79070EF0ED (void);
// 0x000008DE System.Void BNG.HandJet::Start()
extern void HandJet_Start_mACD37866AFF3FD4FAE52DFAE2EB9E0226259A9E4 (void);
// 0x000008DF System.Void BNG.HandJet::OnTrigger(System.Single)
extern void HandJet_OnTrigger_mE30D9C2A6EA64D7C79BA611FEC4E3993E588FEAC (void);
// 0x000008E0 System.Void BNG.HandJet::FixedUpdate()
extern void HandJet_FixedUpdate_m5B6BF672A632D813288BF15AB7D991519BF6C790 (void);
// 0x000008E1 System.Void BNG.HandJet::OnGrab(BNG.Grabber)
extern void HandJet_OnGrab_mE78D6F6380C4B0B14C7881A679153EA500950D86 (void);
// 0x000008E2 System.Void BNG.HandJet::ChangeGravity(System.Boolean)
extern void HandJet_ChangeGravity_mD641F4E05D94A30B9F5B85F7E0266D17D1C2BBA5 (void);
// 0x000008E3 System.Void BNG.HandJet::OnRelease()
extern void HandJet_OnRelease_mA3A5D574B6BB92102E8CE71164F0DF61938D46E8 (void);
// 0x000008E4 System.Void BNG.HandJet::doJet(System.Single)
extern void HandJet_doJet_m4359124626F45527F9B344D7148EC14077DB50E9 (void);
// 0x000008E5 System.Void BNG.HandJet::stopJet()
extern void HandJet_stopJet_m3C19D05A06FDDB87F0A3A2DC5A4D0D454E83DD9F (void);
// 0x000008E6 System.Void BNG.HandJet::OnTriggerUp()
extern void HandJet_OnTriggerUp_m673EABAE1AAD9ED4849AB766074874353D0E64A3 (void);
// 0x000008E7 System.Void BNG.HandJet::.ctor()
extern void HandJet__ctor_m440AC77EBD0A842724DFA4409D6816F1EA826582 (void);
// 0x000008E8 System.Void BNG.HandModelSwitcher::Start()
extern void HandModelSwitcher_Start_mE8EBE22FE29F15D135CB0CB079A0D638B22137A8 (void);
// 0x000008E9 System.Void BNG.HandModelSwitcher::OnTriggerEnter(UnityEngine.Collider)
extern void HandModelSwitcher_OnTriggerEnter_mB3A551B0A490A89EFD57D48BD9AE0D40EE2457BB (void);
// 0x000008EA System.Void BNG.HandModelSwitcher::.ctor()
extern void HandModelSwitcher__ctor_mB7A09AFD2290D0214273156E8CBC8A124B760653 (void);
// 0x000008EB System.Void BNG.IKDummy::Start()
extern void IKDummy_Start_m073A6328A79C7E3F773255DD248E8D1470CA1592 (void);
// 0x000008EC UnityEngine.Transform BNG.IKDummy::SetParentAndLocalPosRot(System.String,UnityEngine.Transform)
extern void IKDummy_SetParentAndLocalPosRot_mF40BB8432A8DB412DEC1881237B55AE9C9C329C0 (void);
// 0x000008ED System.Void BNG.IKDummy::LateUpdate()
extern void IKDummy_LateUpdate_m4D4072365D6FD9D549C9DBA57C983FFE02B8797E (void);
// 0x000008EE System.Void BNG.IKDummy::OnAnimatorIK()
extern void IKDummy_OnAnimatorIK_mDB045590843411F78C6A7EA244B293C6B8B3C6BC (void);
// 0x000008EF System.Void BNG.IKDummy::.ctor()
extern void IKDummy__ctor_m89334DCF79821A9F3C2364733766E86FE994D377 (void);
// 0x000008F0 System.Void BNG.LaserPointer::Start()
extern void LaserPointer_Start_mCE5E0C21CD0961CB5B11BEC3B63253C38ACCF558 (void);
// 0x000008F1 System.Void BNG.LaserPointer::LateUpdate()
extern void LaserPointer_LateUpdate_m76170C22F264D9A97FF5DEF27046BCFEFA3BD6DA (void);
// 0x000008F2 System.Void BNG.LaserPointer::.ctor()
extern void LaserPointer__ctor_mC3E7F5691E9DA94AF1E05A35185D23776CC08A98 (void);
// 0x000008F3 System.Void BNG.LaserSword::Start()
extern void LaserSword_Start_m818B5E765788C598CAAE1233D163F7237002BED6 (void);
// 0x000008F4 System.Void BNG.LaserSword::Update()
extern void LaserSword_Update_m3915F29453AF8D03011B487B9DD42751E592E487 (void);
// 0x000008F5 System.Void BNG.LaserSword::OnTrigger(System.Single)
extern void LaserSword_OnTrigger_m18DA6DFE14F95DC886003C47AEC7B9E8EAE85A16 (void);
// 0x000008F6 System.Void BNG.LaserSword::checkCollision()
extern void LaserSword_checkCollision_mCF875F460D470383E013FFA41BF16F740EF6EF55 (void);
// 0x000008F7 System.Void BNG.LaserSword::OnDrawGizmosSelected()
extern void LaserSword_OnDrawGizmosSelected_m9AFCB27121A775D23D3D2EFAD9203EF1343EEFEB (void);
// 0x000008F8 System.Void BNG.LaserSword::.ctor()
extern void LaserSword__ctor_m88D6E10518842ADF9E23F5234911857ADB9629E8 (void);
// 0x000008F9 System.Void BNG.LiquidWobble::Start()
extern void LiquidWobble_Start_mB7657D43F8C86FFD27097F1C452A9CC5CEABBF72 (void);
// 0x000008FA System.Void BNG.LiquidWobble::Update()
extern void LiquidWobble_Update_m256077E9EEC35186E659B8A5BDFA295AA2182FB7 (void);
// 0x000008FB System.Void BNG.LiquidWobble::.ctor()
extern void LiquidWobble__ctor_m0D7DD52927DCB0FB0C78063752C1155C0F81EAAD (void);
// 0x000008FC System.Void BNG.Marker::OnGrab(BNG.Grabber)
extern void Marker_OnGrab_m955881271C7EF7E885232632B2AEF6C6F2F3291D (void);
// 0x000008FD System.Void BNG.Marker::OnRelease()
extern void Marker_OnRelease_m221D02CA988DAA3D29A9CE466E7D448BC0AA838D (void);
// 0x000008FE System.Collections.IEnumerator BNG.Marker::WriteRoutine()
extern void Marker_WriteRoutine_mB1692C2867273C5C381E3C043A0FA6EA1A305471 (void);
// 0x000008FF System.Void BNG.Marker::InitDraw(UnityEngine.Vector3,UnityEngine.Quaternion,System.Single,UnityEngine.Color)
extern void Marker_InitDraw_mA37F4EE7D70851BB2D2CE08F488298BAF720CD01 (void);
// 0x00000900 UnityEngine.Vector3 BNG.Marker::DrawPoint(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,UnityEngine.Color,UnityEngine.Quaternion)
extern void Marker_DrawPoint_mEE35C86054025A7B9E50AFF4249AE561481EAC56 (void);
// 0x00000901 System.Void BNG.Marker::OnDrawGizmosSelected()
extern void Marker_OnDrawGizmosSelected_m5EBFBCB72BB0FABF62B956A9C155687221C952AC (void);
// 0x00000902 System.Void BNG.Marker::.ctor()
extern void Marker__ctor_mC14FEDBA3F69B7949BDBA203F7C49469CC187742 (void);
// 0x00000903 System.Void BNG.Marker/<WriteRoutine>d__18::.ctor(System.Int32)
extern void U3CWriteRoutineU3Ed__18__ctor_m9904A28786A35DF7C404C1C7AD3CD193AC99CBD0 (void);
// 0x00000904 System.Void BNG.Marker/<WriteRoutine>d__18::System.IDisposable.Dispose()
extern void U3CWriteRoutineU3Ed__18_System_IDisposable_Dispose_m44F04732D353CE0DA94B609AB77B944B0ECB4C26 (void);
// 0x00000905 System.Boolean BNG.Marker/<WriteRoutine>d__18::MoveNext()
extern void U3CWriteRoutineU3Ed__18_MoveNext_m72DEF6A5A33AC0038E2EF4981D45770930AD6136 (void);
// 0x00000906 System.Object BNG.Marker/<WriteRoutine>d__18::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWriteRoutineU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m425C562E9D830B31722521E69EF4FFFBD79F8DF5 (void);
// 0x00000907 System.Void BNG.Marker/<WriteRoutine>d__18::System.Collections.IEnumerator.Reset()
extern void U3CWriteRoutineU3Ed__18_System_Collections_IEnumerator_Reset_mD0493F5470C504CF1063DDD4150D993895EEB667 (void);
// 0x00000908 System.Object BNG.Marker/<WriteRoutine>d__18::System.Collections.IEnumerator.get_Current()
extern void U3CWriteRoutineU3Ed__18_System_Collections_IEnumerator_get_Current_m1E2AF7A6404A9780D08C3883F31DCB29947B5BA9 (void);
// 0x00000909 System.Void BNG.MoveToWaypoint::Start()
extern void MoveToWaypoint_Start_m7D61C3D54EA8F356BC20CCD0402BE467A02CB53B (void);
// 0x0000090A System.Void BNG.MoveToWaypoint::Update()
extern void MoveToWaypoint_Update_m906260C7936DCDD48119DA15CF9DC78D0DE40556 (void);
// 0x0000090B System.Void BNG.MoveToWaypoint::FixedUpdate()
extern void MoveToWaypoint_FixedUpdate_mB220EF2F1A671349EE31A9C9DABFF0113291810F (void);
// 0x0000090C System.Void BNG.MoveToWaypoint::movePlatform(System.Single)
extern void MoveToWaypoint_movePlatform_m978FF3DDAFC42486F7B1B3A544FE88A64D0E65FB (void);
// 0x0000090D System.Void BNG.MoveToWaypoint::resetDelayStatus()
extern void MoveToWaypoint_resetDelayStatus_m6BD947719C7913894783DD152F99D4948EE5F967 (void);
// 0x0000090E System.Void BNG.MoveToWaypoint::.ctor()
extern void MoveToWaypoint__ctor_m5946D0C4B3C7986439A61F971E717790125AA686 (void);
// 0x0000090F System.Void BNG.MovingPlatform::Update()
extern void MovingPlatform_Update_mDD6E84121C69F0691BEE435EBA8EEBEE328AC5E6 (void);
// 0x00000910 System.Void BNG.MovingPlatform::.ctor()
extern void MovingPlatform__ctor_mAD14B3DED4729F1BAB73910AB5DE02EC4558D15C (void);
// 0x00000911 System.Void BNG.PlayerScaler::Update()
extern void PlayerScaler_Update_m0FE18A240032BA39CCFA3B66241F222D50B5149C (void);
// 0x00000912 System.Void BNG.PlayerScaler::.ctor()
extern void PlayerScaler__ctor_mD939A5A79AF6799D25FBA13DE84469184434E53E (void);
// 0x00000913 System.Void BNG.ProjectileLauncher::Start()
extern void ProjectileLauncher_Start_m474367587C569265BB9D0E862B338206F471AC08 (void);
// 0x00000914 UnityEngine.GameObject BNG.ProjectileLauncher::ShootProjectile(System.Single)
extern void ProjectileLauncher_ShootProjectile_m90FC3B650EAA0ABD84A5C043FAC375F57B939219 (void);
// 0x00000915 System.Void BNG.ProjectileLauncher::ShootProjectile()
extern void ProjectileLauncher_ShootProjectile_mEB905A0BE3EC08A15EE5E66DF701792729A8C33A (void);
// 0x00000916 System.Void BNG.ProjectileLauncher::SetForce(System.Single)
extern void ProjectileLauncher_SetForce_mE558C5D25BD4D8FC2022F9D6178005A65A1F363A (void);
// 0x00000917 System.Single BNG.ProjectileLauncher::GetInitialProjectileForce()
extern void ProjectileLauncher_GetInitialProjectileForce_m4B7E79533C87E984AACAEFD0DA02B8470B6627A0 (void);
// 0x00000918 System.Void BNG.ProjectileLauncher::.ctor()
extern void ProjectileLauncher__ctor_m60461DFF1548AA5291460DF742E48D865AE08C3D (void);
// 0x00000919 System.Void BNG.SceneLoader::LoadScene(System.String)
extern void SceneLoader_LoadScene_mEC58A5D5CFC9B2E72C783E0E11875C97F6D708A4 (void);
// 0x0000091A System.Collections.IEnumerator BNG.SceneLoader::FadeThenLoadScene()
extern void SceneLoader_FadeThenLoadScene_m820E1C36C708034F9FD87A355A5D4BD0CEFE9033 (void);
// 0x0000091B System.Void BNG.SceneLoader::.ctor()
extern void SceneLoader__ctor_m761B6D58B22486514F85F6DB1ED7DCEF624A8DC4 (void);
// 0x0000091C System.Void BNG.SceneLoader/<FadeThenLoadScene>d__6::.ctor(System.Int32)
extern void U3CFadeThenLoadSceneU3Ed__6__ctor_m2DA8CFEABA4B0554851069E82E403638B4A01D32 (void);
// 0x0000091D System.Void BNG.SceneLoader/<FadeThenLoadScene>d__6::System.IDisposable.Dispose()
extern void U3CFadeThenLoadSceneU3Ed__6_System_IDisposable_Dispose_mF8486D193B55BD97D418F3E4D0721A60092DEE80 (void);
// 0x0000091E System.Boolean BNG.SceneLoader/<FadeThenLoadScene>d__6::MoveNext()
extern void U3CFadeThenLoadSceneU3Ed__6_MoveNext_m49B07F099BB7822FE4B35C2DAF55E4D70DC6CB11 (void);
// 0x0000091F System.Object BNG.SceneLoader/<FadeThenLoadScene>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CFadeThenLoadSceneU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7F1E153895F5133C628633D98A08D8E49D71584C (void);
// 0x00000920 System.Void BNG.SceneLoader/<FadeThenLoadScene>d__6::System.Collections.IEnumerator.Reset()
extern void U3CFadeThenLoadSceneU3Ed__6_System_Collections_IEnumerator_Reset_m6B5C1BFC6A7D1CDD2CC37D849D385E724CDED218 (void);
// 0x00000921 System.Object BNG.SceneLoader/<FadeThenLoadScene>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CFadeThenLoadSceneU3Ed__6_System_Collections_IEnumerator_get_Current_mEFBEBF6A54C6FDA2ED7A4654A603A98221F49038 (void);
// 0x00000922 System.Void BNG.SlidingDoorMover::Update()
extern void SlidingDoorMover_Update_mA7E680F08859A08A54C17FC8155EFDAFDAC0C09F (void);
// 0x00000923 System.Void BNG.SlidingDoorMover::SetTargetPosition(System.Single)
extern void SlidingDoorMover_SetTargetPosition_mEF646D285C1A7AC7B9D3105FDAA18A6EA86ADD44 (void);
// 0x00000924 System.Void BNG.SlidingDoorMover::.ctor()
extern void SlidingDoorMover__ctor_mCB5FB879BED01ED95C701ADE6FDC55D6A193C5AB (void);
// 0x00000925 System.Boolean BNG.TimeController::get_TimeSlowing()
extern void TimeController_get_TimeSlowing_mE05BB36E99ECF89E9642B4F9B0ACEC7EE6756FE9 (void);
// 0x00000926 System.Void BNG.TimeController::Start()
extern void TimeController_Start_m3E4DC4D16CF851EC9D89B0A175D303DE93E31E37 (void);
// 0x00000927 System.Void BNG.TimeController::Update()
extern void TimeController_Update_m66920FC371F3A6010E24D693FD32AFCADC1A77B7 (void);
// 0x00000928 System.Boolean BNG.TimeController::SlowTimeInputDown()
extern void TimeController_SlowTimeInputDown_mF6D2E0675347A32195A9EC2B2697A69FFD4B8177 (void);
// 0x00000929 System.Void BNG.TimeController::SlowTime()
extern void TimeController_SlowTime_m192B560DEE339BB0CD0500BF44347A98FB1EB3EB (void);
// 0x0000092A System.Void BNG.TimeController::ResumeTime()
extern void TimeController_ResumeTime_m8F3D10DA86373E9FF44BFC7D16B5A270D7DB84D1 (void);
// 0x0000092B System.Collections.IEnumerator BNG.TimeController::resumeTimeRoutine()
extern void TimeController_resumeTimeRoutine_m5C89FA34B6C20682DE459FB6CB9FEB3EB53B6E8C (void);
// 0x0000092C System.Void BNG.TimeController::.ctor()
extern void TimeController__ctor_m1028A4580949600A4ED8E49BC0B217B8ADC54142 (void);
// 0x0000092D System.Void BNG.TimeController/<resumeTimeRoutine>d__20::.ctor(System.Int32)
extern void U3CresumeTimeRoutineU3Ed__20__ctor_m65C0FF97BEC41FE13889278D1F6996452AD6CD91 (void);
// 0x0000092E System.Void BNG.TimeController/<resumeTimeRoutine>d__20::System.IDisposable.Dispose()
extern void U3CresumeTimeRoutineU3Ed__20_System_IDisposable_Dispose_m26A8386A8E0614746DABD0FCE0A86172FB88CF33 (void);
// 0x0000092F System.Boolean BNG.TimeController/<resumeTimeRoutine>d__20::MoveNext()
extern void U3CresumeTimeRoutineU3Ed__20_MoveNext_m1639C3A5EC163F99BF08C45B1DCA04F71EE48009 (void);
// 0x00000930 System.Object BNG.TimeController/<resumeTimeRoutine>d__20::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CresumeTimeRoutineU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAF56F1DB7AA08D37725CA739AF28CED5D6062E1F (void);
// 0x00000931 System.Void BNG.TimeController/<resumeTimeRoutine>d__20::System.Collections.IEnumerator.Reset()
extern void U3CresumeTimeRoutineU3Ed__20_System_Collections_IEnumerator_Reset_m48B923886A3DDDDE0932EA96C177A6BD17432EFE (void);
// 0x00000932 System.Object BNG.TimeController/<resumeTimeRoutine>d__20::System.Collections.IEnumerator.get_Current()
extern void U3CresumeTimeRoutineU3Ed__20_System_Collections_IEnumerator_get_Current_mA9E4F21D6FB9DD3A982938F6782208FA8326DDE4 (void);
// 0x00000933 System.Void BNG.ToggleActiveOnInputAction::OnEnable()
extern void ToggleActiveOnInputAction_OnEnable_mD6CD99FFC292031EE47138F0E93C1194733C4F55 (void);
// 0x00000934 System.Void BNG.ToggleActiveOnInputAction::OnDisable()
extern void ToggleActiveOnInputAction_OnDisable_mA35261BEC25B24998F4E8F681A3D3536C7218A69 (void);
// 0x00000935 System.Void BNG.ToggleActiveOnInputAction::ToggleActive(UnityEngine.InputSystem.InputAction/CallbackContext)
extern void ToggleActiveOnInputAction_ToggleActive_m2FAD5969C167B527450761CD9F5D7857678BC87B (void);
// 0x00000936 System.Void BNG.ToggleActiveOnInputAction::.ctor()
extern void ToggleActiveOnInputAction__ctor_mE5749C9B881DCD36B4D875DD0ACE54B9DE3B9E90 (void);
// 0x00000937 System.Void BNG.VREmulator::Start()
extern void VREmulator_Start_mAE4D19B47622F4F9ACE3D2F7ACB9B91D9DC10BC9 (void);
// 0x00000938 System.Void BNG.VREmulator::OnBeforeRender()
extern void VREmulator_OnBeforeRender_m7DF6991EA153AD6BA32114EF33249984892D0B21 (void);
// 0x00000939 System.Void BNG.VREmulator::onFirstActivate()
extern void VREmulator_onFirstActivate_m264D820F440E488B1AA689D6DB19F3F2D3EC4425 (void);
// 0x0000093A System.Void BNG.VREmulator::Update()
extern void VREmulator_Update_mF1744B74AD47239F6915D748EA96B87335C1A7B3 (void);
// 0x0000093B System.Boolean BNG.VREmulator::HasRequiredFocus()
extern void VREmulator_HasRequiredFocus_mB5A7E31DE1AF3B9868B54AA39812AD9B871A525D (void);
// 0x0000093C System.Void BNG.VREmulator::CheckHeadControls()
extern void VREmulator_CheckHeadControls_m534CB28C5E377172C2C331ABC0D32B9AA23BBD10 (void);
// 0x0000093D System.Void BNG.VREmulator::UpdateInputs()
extern void VREmulator_UpdateInputs_m09401723DCE17CB83AEBF7784BE859ADA3F0B48D (void);
// 0x0000093E System.Void BNG.VREmulator::CheckPlayerControls()
extern void VREmulator_CheckPlayerControls_mA5F8119F4D589EB29024B9A1FBC1F75EF8B3BE70 (void);
// 0x0000093F System.Void BNG.VREmulator::FixedUpdate()
extern void VREmulator_FixedUpdate_m341E851A83D37D6F1228FCC44DBDFA9944B42854 (void);
// 0x00000940 System.Void BNG.VREmulator::UpdateControllerPositions()
extern void VREmulator_UpdateControllerPositions_mE5B410AE71577AD02246C38334AE4EF248EAC02B (void);
// 0x00000941 System.Void BNG.VREmulator::checkGrabbers()
extern void VREmulator_checkGrabbers_m00AE43E56EB5C9BF13D1CC64D4F53F68BAA99E39 (void);
// 0x00000942 System.Void BNG.VREmulator::ResetHands()
extern void VREmulator_ResetHands_m8176D448295C0B1987968E9C36778C69A45AF369 (void);
// 0x00000943 System.Void BNG.VREmulator::ResetAll()
extern void VREmulator_ResetAll_m4B0EDF806AC98BB637E40EE421F5AD385B7A1812 (void);
// 0x00000944 System.Void BNG.VREmulator::OnEnable()
extern void VREmulator_OnEnable_m13D2F0F79A89A6B3EE7893682F4B8BD1269077F4 (void);
// 0x00000945 System.Void BNG.VREmulator::OnDisable()
extern void VREmulator_OnDisable_m6CD6C6E9778EDDE3867158D8E0EC5CB72B00E6AF (void);
// 0x00000946 System.Void BNG.VREmulator::OnApplicationQuit()
extern void VREmulator_OnApplicationQuit_m3E37F3A70F683FCD1DD1CEE396323D53B9EAA7B1 (void);
// 0x00000947 System.Void BNG.VREmulator::.ctor()
extern void VREmulator__ctor_m0C518E5807793308EBCD167DC042E36875E9ED0A (void);
// 0x00000948 System.Void BNG.VehicleController::Start()
extern void VehicleController_Start_m029C203F2FB131A1CCBA142E1600046660B843CB (void);
// 0x00000949 System.Void BNG.VehicleController::Update()
extern void VehicleController_Update_m9413D15BC68E308AE8E966D677C8EF529A888AE9 (void);
// 0x0000094A System.Void BNG.VehicleController::CrankEngine()
extern void VehicleController_CrankEngine_mB5692F3718414BA2C77661F95762F32463C2AB35 (void);
// 0x0000094B System.Collections.IEnumerator BNG.VehicleController::crankEngine()
extern void VehicleController_crankEngine_m863D282917C9F6421BC13EEBCDA9779B1BBF77ED (void);
// 0x0000094C System.Void BNG.VehicleController::CheckOutOfBounds()
extern void VehicleController_CheckOutOfBounds_mFCEB783E485BDAC806C7FF551920CB6106EB80C3 (void);
// 0x0000094D System.Void BNG.VehicleController::GetTorqueInputFromTriggers()
extern void VehicleController_GetTorqueInputFromTriggers_mFC4925DE3A27E5349C2EB9BD9F5A54F733BC380A (void);
// 0x0000094E System.Void BNG.VehicleController::FixedUpdate()
extern void VehicleController_FixedUpdate_m7BA18855C26FC426977BCBED3FF37780FB77C08F (void);
// 0x0000094F System.Void BNG.VehicleController::UpdateWheelTorque()
extern void VehicleController_UpdateWheelTorque_mDAD282B68D2CEE69A655DF7824A8ED4EB01A5F65 (void);
// 0x00000950 System.Void BNG.VehicleController::SetSteeringAngle(System.Single)
extern void VehicleController_SetSteeringAngle_m9F8F82CF46DD2A32134FFCD19C561476B4E6FD56 (void);
// 0x00000951 System.Void BNG.VehicleController::SetSteeringAngleInverted(System.Single)
extern void VehicleController_SetSteeringAngleInverted_m4220208DA9800CFEFF9FDCD8FFBAB7FAAA01AF27 (void);
// 0x00000952 System.Void BNG.VehicleController::SetSteeringAngle(UnityEngine.Vector2)
extern void VehicleController_SetSteeringAngle_m1DC81C75995163ED9136A6E69B02E3F2E9F1E807 (void);
// 0x00000953 System.Void BNG.VehicleController::SetSteeringAngleInverted(UnityEngine.Vector2)
extern void VehicleController_SetSteeringAngleInverted_mEFC7938199B5A78196FEB11C96D8A8F2433060BA (void);
// 0x00000954 System.Void BNG.VehicleController::SetMotorTorqueInput(System.Single)
extern void VehicleController_SetMotorTorqueInput_mBE41CB8019D8BF73643503B300B953EB33626485 (void);
// 0x00000955 System.Void BNG.VehicleController::SetMotorTorqueInputInverted(System.Single)
extern void VehicleController_SetMotorTorqueInputInverted_mA992C59AF072EC6E408AEE593BBCD624CB50BD19 (void);
// 0x00000956 System.Void BNG.VehicleController::SetMotorTorqueInput(UnityEngine.Vector2)
extern void VehicleController_SetMotorTorqueInput_m4251871AF3877523025EC97977BF9CF1BB1E3BB8 (void);
// 0x00000957 System.Void BNG.VehicleController::SetMotorTorqueInputInverted(UnityEngine.Vector2)
extern void VehicleController_SetMotorTorqueInputInverted_mEA3FC18ADE50ECEDCC9250E4431B4A0A54453BDB (void);
// 0x00000958 System.Void BNG.VehicleController::UpdateWheelVisuals(BNG.WheelObject)
extern void VehicleController_UpdateWheelVisuals_mB062B5578A39B69AFB03F2FDDB0A6B3CEAE765CE (void);
// 0x00000959 System.Void BNG.VehicleController::UpdateEngineAudio()
extern void VehicleController_UpdateEngineAudio_m6881E649462EC9D4F4878813DC512F656E6231D3 (void);
// 0x0000095A System.Void BNG.VehicleController::OnCollisionEnter(UnityEngine.Collision)
extern void VehicleController_OnCollisionEnter_m094C2A3C3166BE95F0BE43666DB98E8619ED470C (void);
// 0x0000095B System.Single BNG.VehicleController::correctValue(System.Single)
extern void VehicleController_correctValue_mE72A3C04FB169E3EC84C5E87A8D44D311465BB2A (void);
// 0x0000095C System.Void BNG.VehicleController::.ctor()
extern void VehicleController__ctor_m9C5EE513E0072158FB05E42549BEB3DF07F90655 (void);
// 0x0000095D System.Void BNG.VehicleController/<crankEngine>d__24::.ctor(System.Int32)
extern void U3CcrankEngineU3Ed__24__ctor_mA54C3A1469980669C05FEFC77BAB39C3D7ED01F0 (void);
// 0x0000095E System.Void BNG.VehicleController/<crankEngine>d__24::System.IDisposable.Dispose()
extern void U3CcrankEngineU3Ed__24_System_IDisposable_Dispose_mFBE64A7A2A024A4F9FA419FA23419D36D61C771E (void);
// 0x0000095F System.Boolean BNG.VehicleController/<crankEngine>d__24::MoveNext()
extern void U3CcrankEngineU3Ed__24_MoveNext_m4F370B6045BA52D9E435731BC222C0FC8E18B2F9 (void);
// 0x00000960 System.Object BNG.VehicleController/<crankEngine>d__24::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CcrankEngineU3Ed__24_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6A80CC9E37CCCADA4FACA1273DA7B40FC55F9BAC (void);
// 0x00000961 System.Void BNG.VehicleController/<crankEngine>d__24::System.Collections.IEnumerator.Reset()
extern void U3CcrankEngineU3Ed__24_System_Collections_IEnumerator_Reset_mAF5E0AA65246A693A6071E2E88DE56059742D3B9 (void);
// 0x00000962 System.Object BNG.VehicleController/<crankEngine>d__24::System.Collections.IEnumerator.get_Current()
extern void U3CcrankEngineU3Ed__24_System_Collections_IEnumerator_get_Current_m73B404E26B55FCF851B9174BCCBFB8AA22E14F46 (void);
// 0x00000963 System.Void BNG.WheelObject::.ctor()
extern void WheelObject__ctor_m219A7030ADC46E86FE8A75195B9AE2D3951D24B8 (void);
// 0x00000964 System.Void BNG.Waypoint::OnDrawGizmosSelected()
extern void Waypoint_OnDrawGizmosSelected_mCF8F7727D5D7019517BC9637BF72CAB00EB96C19 (void);
// 0x00000965 System.Void BNG.Waypoint::.ctor()
extern void Waypoint__ctor_mC4BDB821098252CC99743B52CC626CF775FBACB2 (void);
// 0x00000966 System.Void BNG.Zipline::Start()
extern void Zipline_Start_mD0D47AE92DDABBF8A5477E63A30B231E61705589 (void);
// 0x00000967 System.Void BNG.Zipline::Update()
extern void Zipline_Update_mAB404E24C3C5E6F58E0D08609A81A7A0BF158639 (void);
// 0x00000968 System.Void BNG.Zipline::OnDrawGizmosSelected()
extern void Zipline_OnDrawGizmosSelected_mD0A97503A8D42FE0EC36A2E6879AB5EDBA1F26D4 (void);
// 0x00000969 System.Void BNG.Zipline::OnTrigger(System.Single)
extern void Zipline_OnTrigger_m91C3AADC46115752C760EBB944379675AE20F31B (void);
// 0x0000096A System.Void BNG.Zipline::OnButton1()
extern void Zipline_OnButton1_m72FAB550E0356A65531533D6EC8EE583A1674EC8 (void);
// 0x0000096B System.Void BNG.Zipline::OnButton2()
extern void Zipline_OnButton2_m1EBE05A782D01AF95B44A0CDB84F58FFA58A1092 (void);
// 0x0000096C System.Void BNG.Zipline::moveTowards(UnityEngine.Vector3,System.Boolean)
extern void Zipline_moveTowards_m0961825A8807B9801F2A67CE7A148638E43479DB (void);
// 0x0000096D System.Void BNG.Zipline::.ctor()
extern void Zipline__ctor_mC58D65142B817A4C963CAEB0181EF78F7D078FA2 (void);
// 0x0000096E System.Void BNG.ControllerOffsetHelper::Start()
extern void ControllerOffsetHelper_Start_m784EC7C763C4780B6271F35C85C3B6E4E2326D25 (void);
// 0x0000096F System.Collections.IEnumerator BNG.ControllerOffsetHelper::checkForController()
extern void ControllerOffsetHelper_checkForController_m97D83C5BFE0F23CA6E10AF5842E0E7370973947A (void);
// 0x00000970 System.Void BNG.ControllerOffsetHelper::OnControllerFound()
extern void ControllerOffsetHelper_OnControllerFound_m5C3BC19C5B624F9643C2C409318FDB75093C728A (void);
// 0x00000971 BNG.ControllerOffset BNG.ControllerOffsetHelper::GetControllerOffset(System.String)
extern void ControllerOffsetHelper_GetControllerOffset_m49AE7F40B38476D0DB8EC443217E7A83E3DD40C3 (void);
// 0x00000972 System.Void BNG.ControllerOffsetHelper::DefineControllerOffsets()
extern void ControllerOffsetHelper_DefineControllerOffsets_mF631CD0DB3619FA93439D65D65EAF12E9585D675 (void);
// 0x00000973 BNG.ControllerOffset BNG.ControllerOffsetHelper::GetOpenXROffset()
extern void ControllerOffsetHelper_GetOpenXROffset_mD7125EF48DF532E88112081242D7E34549BDB16F (void);
// 0x00000974 System.Void BNG.ControllerOffsetHelper::.ctor()
extern void ControllerOffsetHelper__ctor_mBAB737F606A71C8A711DDE6247DC90B51FF62818 (void);
// 0x00000975 System.Boolean BNG.ControllerOffsetHelper::<GetControllerOffset>b__9_0(BNG.ControllerOffset)
extern void ControllerOffsetHelper_U3CGetControllerOffsetU3Eb__9_0_m56C8631CC1373F9DC0F7901546F3CA31095F1BF6 (void);
// 0x00000976 System.Void BNG.ControllerOffsetHelper/<checkForController>d__7::.ctor(System.Int32)
extern void U3CcheckForControllerU3Ed__7__ctor_m3CCE9882260856E301FA46909325EEF1D7F5BF41 (void);
// 0x00000977 System.Void BNG.ControllerOffsetHelper/<checkForController>d__7::System.IDisposable.Dispose()
extern void U3CcheckForControllerU3Ed__7_System_IDisposable_Dispose_m41BE73D23FBF7747B408E822BACA44B732A9E88D (void);
// 0x00000978 System.Boolean BNG.ControllerOffsetHelper/<checkForController>d__7::MoveNext()
extern void U3CcheckForControllerU3Ed__7_MoveNext_m0C53DAB3426DA90917A51A289C214FE9B02C2EC1 (void);
// 0x00000979 System.Object BNG.ControllerOffsetHelper/<checkForController>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CcheckForControllerU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2D9C45AA1B8D94A2875E694FC44B0A11E4A57A77 (void);
// 0x0000097A System.Void BNG.ControllerOffsetHelper/<checkForController>d__7::System.Collections.IEnumerator.Reset()
extern void U3CcheckForControllerU3Ed__7_System_Collections_IEnumerator_Reset_m3E48252A4C90D45F08C6068DA6CC8CFF1A7CD5BC (void);
// 0x0000097B System.Object BNG.ControllerOffsetHelper/<checkForController>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CcheckForControllerU3Ed__7_System_Collections_IEnumerator_get_Current_m456877EEDD6515ECE058343280D3BC37754E5270 (void);
// 0x0000097C System.String BNG.ControllerOffset::get_ControllerName()
extern void ControllerOffset_get_ControllerName_mAFB63DED6372D0101D243F4C40321838650822FF (void);
// 0x0000097D System.Void BNG.ControllerOffset::set_ControllerName(System.String)
extern void ControllerOffset_set_ControllerName_m91AA379DD0B198061E0C4C05C732A0A33FA74684 (void);
// 0x0000097E UnityEngine.Vector3 BNG.ControllerOffset::get_LeftControllerPositionOffset()
extern void ControllerOffset_get_LeftControllerPositionOffset_mF93F4E6F3B22A1FD1DC0225A524A17491FBCB8D9 (void);
// 0x0000097F System.Void BNG.ControllerOffset::set_LeftControllerPositionOffset(UnityEngine.Vector3)
extern void ControllerOffset_set_LeftControllerPositionOffset_mB22EB34739B09182D54268F58D96F753F5F069DD (void);
// 0x00000980 UnityEngine.Vector3 BNG.ControllerOffset::get_RightControllerPositionOffset()
extern void ControllerOffset_get_RightControllerPositionOffset_mFE97FF8F13EAFC47F4C5D78B0B12E0554F3CFC4C (void);
// 0x00000981 System.Void BNG.ControllerOffset::set_RightControllerPositionOffset(UnityEngine.Vector3)
extern void ControllerOffset_set_RightControllerPositionOffset_m2BCB7872DFC9BA42E74E9630AED8DBF73181F060 (void);
// 0x00000982 UnityEngine.Vector3 BNG.ControllerOffset::get_LeftControllerRotationOffset()
extern void ControllerOffset_get_LeftControllerRotationOffset_mA060B1BBCEFDF0C31F9B2D56797E5AEDD7856DAF (void);
// 0x00000983 System.Void BNG.ControllerOffset::set_LeftControllerRotationOffset(UnityEngine.Vector3)
extern void ControllerOffset_set_LeftControllerRotationOffset_m7741A534932CF187E41B6A10E261874AEB03B73C (void);
// 0x00000984 UnityEngine.Vector3 BNG.ControllerOffset::get_RightControlleRotationOffset()
extern void ControllerOffset_get_RightControlleRotationOffset_mAC98B24D3AE8398971446111F2BFBC9A684242E0 (void);
// 0x00000985 System.Void BNG.ControllerOffset::set_RightControlleRotationOffset(UnityEngine.Vector3)
extern void ControllerOffset_set_RightControlleRotationOffset_m9C3C1F4E5FC4A3E1F1D0BD09512FB258C2AE92EF (void);
// 0x00000986 System.Void BNG.ControllerOffset::.ctor()
extern void ControllerOffset__ctor_m0B84AB4AD82A894F9939AB773E640A2D8891F756 (void);
// 0x00000987 System.Void BNG.DetachableLimb::DoDismemberment(BNG.Grabber)
extern void DetachableLimb_DoDismemberment_m16A05DA438472684EDEBF2D46C6702F3A9F5D9E9 (void);
// 0x00000988 System.Void BNG.DetachableLimb::ReverseDismemberment()
extern void DetachableLimb_ReverseDismemberment_m3000EF1934BFF88DA405E54CA2D48B03B900A09B (void);
// 0x00000989 System.Void BNG.DetachableLimb::.ctor()
extern void DetachableLimb__ctor_mBEF4A41606A88305FDC3BF174347B75119636937 (void);
// 0x0000098A System.Void BNG.DoorHelper::Start()
extern void DoorHelper_Start_m95108467000AA8B4E3D494F796A9F41984EBD5FB (void);
// 0x0000098B System.Void BNG.DoorHelper::Update()
extern void DoorHelper_Update_m95D3F7BB838DAC85C6F2AC709FDF897F9B236605 (void);
// 0x0000098C System.Void BNG.DoorHelper::.ctor()
extern void DoorHelper__ctor_m2CCC5F53D176ABBA52222F902672BA402BCE7171 (void);
// 0x0000098D System.Void BNG.GrabberArea::Update()
extern void GrabberArea_Update_mA4E8125C2BF8BECF9B3ECC932DEF6BD36D085518 (void);
// 0x0000098E BNG.Grabber BNG.GrabberArea::GetOpenGrabber()
extern void GrabberArea_GetOpenGrabber_m0AC6B333A39F133EF7D7428F5CCEDF076E6D3CB6 (void);
// 0x0000098F System.Void BNG.GrabberArea::OnTriggerEnter(UnityEngine.Collider)
extern void GrabberArea_OnTriggerEnter_m7113E53D902BA769736AD0A7F4CC2F7D8E45B7D4 (void);
// 0x00000990 System.Void BNG.GrabberArea::OnTriggerExit(UnityEngine.Collider)
extern void GrabberArea_OnTriggerExit_mF2E21A8B3822E7186B79CE71D3BBB5C26296E553 (void);
// 0x00000991 System.Void BNG.GrabberArea::.ctor()
extern void GrabberArea__ctor_m88E05D41289F2A97E081FEBAA8846AFFDB665B45 (void);
// 0x00000992 System.Void BNG.HandCollision::Start()
extern void HandCollision_Start_mD2124BF6CC2B87ADF3D52D2275B4AD23C2BA8DD7 (void);
// 0x00000993 System.Void BNG.HandCollision::Update()
extern void HandCollision_Update_m345506931BA6238B8CA089EB2637088EEB8FBE59 (void);
// 0x00000994 System.Void BNG.HandCollision::.ctor()
extern void HandCollision__ctor_m7AFED6F155ED06DDE28224209F3262FD3BE14C50 (void);
// 0x00000995 UnityEngine.Vector3 BNG.HandController::get_offsetPosition()
extern void HandController_get_offsetPosition_m2AC840F59F3F39BDB88C9C0A7AA73223669F72F9 (void);
// 0x00000996 UnityEngine.Vector3 BNG.HandController::get_offsetRotation()
extern void HandController_get_offsetRotation_m3C118555EEDD4EA94B3D3D96738ED1C00E3C01D3 (void);
// 0x00000997 System.Void BNG.HandController::Start()
extern void HandController_Start_m59B94EEF13EDE9FFDAACF0E2D0A5492B62EE1033 (void);
// 0x00000998 System.Void BNG.HandController::Update()
extern void HandController_Update_mAB83F37CF8C4C707E2FAB83EB67A34054975434D (void);
// 0x00000999 System.Void BNG.HandController::UpdateHeldObjectState()
extern void HandController_UpdateHeldObjectState_mBA81BD09837E2577516457D96967E679C387C7DC (void);
// 0x0000099A System.Void BNG.HandController::UpdateIdleState()
extern void HandController_UpdateIdleState_mE22C4C38973928ED21369DA01D5726740C8EE2A1 (void);
// 0x0000099B System.Boolean BNG.HandController::HoldingObject()
extern void HandController_HoldingObject_m2C13BEE93C0D49E70E55A6D184E1959D24F5019A (void);
// 0x0000099C System.Void BNG.HandController::CheckForGrabChange()
extern void HandController_CheckForGrabChange_mABB278D91A5652825378CAA7EB8DA4FD30E1C2F9 (void);
// 0x0000099D System.Void BNG.HandController::OnGrabChange(UnityEngine.GameObject)
extern void HandController_OnGrabChange_m2D26DBA9D7DC93B8498E59515BDA8E0E7DCC8C25 (void);
// 0x0000099E System.Void BNG.HandController::OnGrabDrop()
extern void HandController_OnGrabDrop_mD2FF1E10A0D861C0F6C87FB517FC5162D4900938 (void);
// 0x0000099F System.Void BNG.HandController::SetHandAnimator()
extern void HandController_SetHandAnimator_m34E82355A4E0C927DA07E87AC692C30D480E6332 (void);
// 0x000009A0 System.Void BNG.HandController::UpdateFromInputs()
extern void HandController_UpdateFromInputs_m41827FBEAE91E8EF680BAFD282DD6EC2E8376002 (void);
// 0x000009A1 System.Void BNG.HandController::UpdateAnimimationStates()
extern void HandController_UpdateAnimimationStates_mFD746541C50DF9EB5645A3A160C8778402E052BB (void);
// 0x000009A2 System.Void BNG.HandController::setAnimatorBlend(System.Single,System.Single,System.Single,System.Int32)
extern void HandController_setAnimatorBlend_m7A057B0515E7F0E94FB6AF84AB6A599E3D13E657 (void);
// 0x000009A3 System.Boolean BNG.HandController::IsAnimatorGrabbable()
extern void HandController_IsAnimatorGrabbable_m2E24880A7C081A720F87ADEA1914E1F0C637C62C (void);
// 0x000009A4 System.Void BNG.HandController::UpdateHandPoser()
extern void HandController_UpdateHandPoser_mC5AFE8D7F41A0CF426B0000A4925B46DB1763E90 (void);
// 0x000009A5 System.Boolean BNG.HandController::IsHandPoserGrabbable()
extern void HandController_IsHandPoserGrabbable_m7DA472A5B1B37E9E9E68BCC1C46FDE9AB1F41BFC (void);
// 0x000009A6 System.Void BNG.HandController::UpdateHandPoserIdleState()
extern void HandController_UpdateHandPoserIdleState_m67F9A758AB0CD38FA2AD1D0672832B6DCFF30F34 (void);
// 0x000009A7 System.Void BNG.HandController::UpdateIndexFingerBlending()
extern void HandController_UpdateIndexFingerBlending_m43BAEFE240B3CCF5EBF0520F918428FDE19CDD02 (void);
// 0x000009A8 System.Boolean BNG.HandController::SetupPoseBlender()
extern void HandController_SetupPoseBlender_m2EC1B0B72E4CBEA7227199414A0EDC8E1F1E4D2A (void);
// 0x000009A9 BNG.HandPose BNG.HandController::GetDefaultOpenPose()
extern void HandController_GetDefaultOpenPose_mF65B0B371C9D163BABBC854D2E0FE03FDAD3F230 (void);
// 0x000009AA BNG.HandPose BNG.HandController::GetDefaultClosedPose()
extern void HandController_GetDefaultClosedPose_m21252D099BE81FAF79AAAC974DBC0229953052BB (void);
// 0x000009AB System.Void BNG.HandController::EnableHandPoser()
extern void HandController_EnableHandPoser_m00A624686DDA95BACB7892316E26B39C3E6B06FF (void);
// 0x000009AC System.Void BNG.HandController::EnableAutoPoser(System.Boolean)
extern void HandController_EnableAutoPoser_m4794689C8721FB16AFA1FF72099BA6B1308A3749 (void);
// 0x000009AD System.Void BNG.HandController::DisablePoseBlender()
extern void HandController_DisablePoseBlender_m54F3490B925AC12E0F7C39A1233F538F051D1B4F (void);
// 0x000009AE System.Void BNG.HandController::DisableAutoPoser()
extern void HandController_DisableAutoPoser_mAAF974038E55AB937A35065C331F4EB22A046DD0 (void);
// 0x000009AF System.Boolean BNG.HandController::IsAutoPoserGrabbable()
extern void HandController_IsAutoPoserGrabbable_mF3B2554F3323996F0B9EA16DB085629B224383FD (void);
// 0x000009B0 System.Void BNG.HandController::EnableHandAnimator()
extern void HandController_EnableHandAnimator_m1D80D5D6990E7DB7890D28CEBF2AE0F709B79B80 (void);
// 0x000009B1 System.Void BNG.HandController::DisableHandAnimator()
extern void HandController_DisableHandAnimator_m696926FD81D4364F6D1C56DB163E5C920463F3B2 (void);
// 0x000009B2 System.Void BNG.HandController::OnGrabberGrabbed(BNG.Grabbable)
extern void HandController_OnGrabberGrabbed_m61E6E493A22E6E6485FCE1F95C89549828DE530A (void);
// 0x000009B3 System.Void BNG.HandController::UpdateCurrentHandPose()
extern void HandController_UpdateCurrentHandPose_m2DA775E36FEEFC30007391A76211E1B04C3C0EEE (void);
// 0x000009B4 System.Void BNG.HandController::OnGrabberReleased(BNG.Grabbable)
extern void HandController_OnGrabberReleased_m3C8D3E9C5608A9D3F671D4620F58D4CE90014F38 (void);
// 0x000009B5 System.Void BNG.HandController::.ctor()
extern void HandController__ctor_mC4F1014DE6B1AF277F54BAA96453DA676E2BAEB6 (void);
// 0x000009B6 System.Boolean BNG.HandPhysics::get_HoldingObject()
extern void HandPhysics_get_HoldingObject_mDB1B275317DC6DFDFFDCAEB0609CD0C42DDB7C39 (void);
// 0x000009B7 System.Void BNG.HandPhysics::Start()
extern void HandPhysics_Start_m92F320AB071BD9A20A2ABCAA673DCA2EAFB3115D (void);
// 0x000009B8 System.Void BNG.HandPhysics::Update()
extern void HandPhysics_Update_mBAE9BC4AE96C24465996842C2F20C8E4F0EE158C (void);
// 0x000009B9 System.Void BNG.HandPhysics::FixedUpdate()
extern void HandPhysics_FixedUpdate_m70ADCCC0C977FFB31FA9F66158B83CD9FE4CDE37 (void);
// 0x000009BA System.Void BNG.HandPhysics::initHandColliders()
extern void HandPhysics_initHandColliders_mB4D7BBEC86087DA6511A2369DB21E897970D73BC (void);
// 0x000009BB System.Void BNG.HandPhysics::checkRemoteCollision()
extern void HandPhysics_checkRemoteCollision_mD7361C59ECD04B0C36A233C0256C830BADED6141 (void);
// 0x000009BC System.Void BNG.HandPhysics::drawDistanceLine()
extern void HandPhysics_drawDistanceLine_m23F044DC914857FD1099D846BF78B4A9D38C02C7 (void);
// 0x000009BD System.Void BNG.HandPhysics::checkBreakDistance()
extern void HandPhysics_checkBreakDistance_m6CC454BEF50BB6DA52266A8BFB71AB9BD2227EA8 (void);
// 0x000009BE System.Void BNG.HandPhysics::updateHandGraphics()
extern void HandPhysics_updateHandGraphics_mEE1CD2728F86343294CF649736C0245CD4EFA346 (void);
// 0x000009BF System.Collections.IEnumerator BNG.HandPhysics::UnignoreAllCollisions()
extern void HandPhysics_UnignoreAllCollisions_m0EFF25FF369079E5EA0A002E7F387A1BB3F34997 (void);
// 0x000009C0 System.Void BNG.HandPhysics::IgnoreGrabbableCollisions(BNG.Grabbable,System.Boolean)
extern void HandPhysics_IgnoreGrabbableCollisions_mD923E9AC08CC51B6A983086772963ABDB5BE0341 (void);
// 0x000009C1 System.Void BNG.HandPhysics::DisableHandColliders()
extern void HandPhysics_DisableHandColliders_m3A5873E2329042EA6BAFD17F2E0C6A60795BC442 (void);
// 0x000009C2 System.Void BNG.HandPhysics::EnableHandColliders()
extern void HandPhysics_EnableHandColliders_mC33FB7EBD8A19046AC5A18394189EBC64B644F33 (void);
// 0x000009C3 System.Void BNG.HandPhysics::OnGrabbedObject(BNG.Grabbable)
extern void HandPhysics_OnGrabbedObject_mC67D85FAAA671CDFD32E9A1C547F47F9AA0445C3 (void);
// 0x000009C4 System.Void BNG.HandPhysics::LockLocalPosition()
extern void HandPhysics_LockLocalPosition_mF23373F08E07FAE9082C9C16C333CD4E6D6E175D (void);
// 0x000009C5 System.Void BNG.HandPhysics::UnlockLocalPosition()
extern void HandPhysics_UnlockLocalPosition_mF9980C48388DBDCE870CF77B7B985F694A12D172 (void);
// 0x000009C6 System.Void BNG.HandPhysics::OnReleasedObject(BNG.Grabbable)
extern void HandPhysics_OnReleasedObject_mE1F002815A2C76C70BF4569B53BEB31F71B32446 (void);
// 0x000009C7 System.Void BNG.HandPhysics::OnEnable()
extern void HandPhysics_OnEnable_mA2EC662A570C6DB2A0A5EC3C3E88664133FD47F1 (void);
// 0x000009C8 System.Void BNG.HandPhysics::LockOffset()
extern void HandPhysics_LockOffset_m7A971F1C12598FCFCA8B7D7E01D1558C3A0BC502 (void);
// 0x000009C9 System.Void BNG.HandPhysics::UnlockOffset()
extern void HandPhysics_UnlockOffset_mC342DEC1514B64C0027F41FA47E8986CEFF055F7 (void);
// 0x000009CA System.Void BNG.HandPhysics::OnDisable()
extern void HandPhysics_OnDisable_m01568D27A14993B085F4A3F64D42FC86F39127C7 (void);
// 0x000009CB System.Void BNG.HandPhysics::OnCollisionStay(UnityEngine.Collision)
extern void HandPhysics_OnCollisionStay_m7BFE91F8E3158380AA97C639D8A4A58801100295 (void);
// 0x000009CC System.Boolean BNG.HandPhysics::IsValidCollision(UnityEngine.Collider)
extern void HandPhysics_IsValidCollision_mAB6AFE78071E144F45A08C050500D27D9BAF0DEB (void);
// 0x000009CD System.Void BNG.HandPhysics::.ctor()
extern void HandPhysics__ctor_mC31905D5A3AC1866B5936B1D1FF3D67945EB5874 (void);
// 0x000009CE System.Void BNG.HandPhysics/<UnignoreAllCollisions>d__31::.ctor(System.Int32)
extern void U3CUnignoreAllCollisionsU3Ed__31__ctor_m2A10E1CA75FF9308987B6C5C61689174039128DB (void);
// 0x000009CF System.Void BNG.HandPhysics/<UnignoreAllCollisions>d__31::System.IDisposable.Dispose()
extern void U3CUnignoreAllCollisionsU3Ed__31_System_IDisposable_Dispose_m3C24DF416C6BFD2FA4A404C0096653630AE7F95A (void);
// 0x000009D0 System.Boolean BNG.HandPhysics/<UnignoreAllCollisions>d__31::MoveNext()
extern void U3CUnignoreAllCollisionsU3Ed__31_MoveNext_m69BEB12669E385880E0B9F1157D9020C06A1EFA2 (void);
// 0x000009D1 System.Object BNG.HandPhysics/<UnignoreAllCollisions>d__31::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CUnignoreAllCollisionsU3Ed__31_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m666CF651E265F21CE8038BE1F77DFEBD07361BB3 (void);
// 0x000009D2 System.Void BNG.HandPhysics/<UnignoreAllCollisions>d__31::System.Collections.IEnumerator.Reset()
extern void U3CUnignoreAllCollisionsU3Ed__31_System_Collections_IEnumerator_Reset_m102F18A1E01590EAF327BF4956777283BD1E21AD (void);
// 0x000009D3 System.Object BNG.HandPhysics/<UnignoreAllCollisions>d__31::System.Collections.IEnumerator.get_Current()
extern void U3CUnignoreAllCollisionsU3Ed__31_System_Collections_IEnumerator_get_Current_mFA9906E663EA03CBFF3613BB631011D12BE85CD6 (void);
// 0x000009D4 System.Void BNG.HandRepresentationHelper::Update()
extern void HandRepresentationHelper_Update_m4B7478FBE0A162FDE71BA8D88D4CA9C04274D069 (void);
// 0x000009D5 System.Void BNG.HandRepresentationHelper::.ctor()
extern void HandRepresentationHelper__ctor_m7387CDD14C92B023FA614ECDB7BC5C2035C98758 (void);
// 0x000009D6 System.Void BNG.HandleGFXHelper::Start()
extern void HandleGFXHelper_Start_mEBA5A2DFF5400199879C68877DF840577E2C4900 (void);
// 0x000009D7 System.Void BNG.HandleGFXHelper::Update()
extern void HandleGFXHelper_Update_m91083572E255174490B02AA3E90C385A57D05284 (void);
// 0x000009D8 System.Void BNG.HandleGFXHelper::.ctor()
extern void HandleGFXHelper__ctor_m94D39FFD7FE1E26C18AAD1D3B01335725A4728D2 (void);
// 0x000009D9 System.Void BNG.HandleHelper::Start()
extern void HandleHelper_Start_mDC219E507E5DD1A0568C163071750084E55E2D08 (void);
// 0x000009DA System.Void BNG.HandleHelper::FixedUpdate()
extern void HandleHelper_FixedUpdate_mFDA4E9B5B344187517303A1FB0F0C78A3D705529 (void);
// 0x000009DB System.Void BNG.HandleHelper::OnCollisionEnter(UnityEngine.Collision)
extern void HandleHelper_OnCollisionEnter_m21006EA5A0C4EAC12BD537599554B81C21C2FE09 (void);
// 0x000009DC System.Collections.IEnumerator BNG.HandleHelper::doRelease()
extern void HandleHelper_doRelease_m2E98AF5F84F9C9B5A91398B5B6273E456518AAD3 (void);
// 0x000009DD System.Void BNG.HandleHelper::.ctor()
extern void HandleHelper__ctor_mBFF1BFA91CF42662D8C271D16211F88D9898A4BC (void);
// 0x000009DE System.Void BNG.HandleHelper/<doRelease>d__10::.ctor(System.Int32)
extern void U3CdoReleaseU3Ed__10__ctor_mA65B8960D309F96EBCA523AFE925048DDF62AD60 (void);
// 0x000009DF System.Void BNG.HandleHelper/<doRelease>d__10::System.IDisposable.Dispose()
extern void U3CdoReleaseU3Ed__10_System_IDisposable_Dispose_m4FFC1159EAEC4CEC81C0EE34AEF01E9F4D22669E (void);
// 0x000009E0 System.Boolean BNG.HandleHelper/<doRelease>d__10::MoveNext()
extern void U3CdoReleaseU3Ed__10_MoveNext_mEFF6E684DA16C18BBB49A95F0A4AE2FCE2B613C6 (void);
// 0x000009E1 System.Object BNG.HandleHelper/<doRelease>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CdoReleaseU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m39A9D07ECF872B1E1DEB7AD669C20C92778F77B8 (void);
// 0x000009E2 System.Void BNG.HandleHelper/<doRelease>d__10::System.Collections.IEnumerator.Reset()
extern void U3CdoReleaseU3Ed__10_System_Collections_IEnumerator_Reset_m7FFDDB0EBBC4E4513E8C96FDD50899200C712F8D (void);
// 0x000009E3 System.Object BNG.HandleHelper/<doRelease>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CdoReleaseU3Ed__10_System_Collections_IEnumerator_get_Current_mFB033D473EF9FA59C6B52A12ED4C2137690F20AE (void);
// 0x000009E4 System.Void BNG.IgnoreColliders::Start()
extern void IgnoreColliders_Start_m6ADA9ABD9ADE4DF555319410C6600D494C37A870 (void);
// 0x000009E5 System.Void BNG.IgnoreColliders::.ctor()
extern void IgnoreColliders__ctor_m62214857F9AC233A2DB82AB777B23255239C96ED (void);
// 0x000009E6 System.Void BNG.InvalidTeleportArea::.ctor()
extern void InvalidTeleportArea__ctor_mA658A29683901676B958A92A7640B55143760152 (void);
// 0x000009E7 System.Void BNG.JointBreaker::Start()
extern void JointBreaker_Start_mE8C810443921B30FAE16133042393B81A13441BB (void);
// 0x000009E8 System.Void BNG.JointBreaker::Update()
extern void JointBreaker_Update_m3ABC0474C8A3234946256DE5B0C7CA0D158F2CC7 (void);
// 0x000009E9 System.Void BNG.JointBreaker::BreakJoint()
extern void JointBreaker_BreakJoint_m1E4D4CB659FF9E044D0AA89A0B40999741B3CBB5 (void);
// 0x000009EA System.Void BNG.JointBreaker::.ctor()
extern void JointBreaker__ctor_m7021C024CC4A3B73301713115C73A01DA54B2899 (void);
// 0x000009EB System.Void BNG.JointHelper::Start()
extern void JointHelper_Start_m65766EA2418DB0790739F6688CFCBFDBCC73268B (void);
// 0x000009EC System.Void BNG.JointHelper::lockPosition()
extern void JointHelper_lockPosition_mD6996F016043796F6EDFEFA3B7CB57C35E068CAA (void);
// 0x000009ED System.Void BNG.JointHelper::LateUpdate()
extern void JointHelper_LateUpdate_m2569F8F2CE0BEDADA1422497B9702B983291F6EF (void);
// 0x000009EE System.Void BNG.JointHelper::FixedUpdate()
extern void JointHelper_FixedUpdate_mDD02387183B9E311D0093599E6A4F1A41FCB40FD (void);
// 0x000009EF System.Void BNG.JointHelper::.ctor()
extern void JointHelper__ctor_mF0B4B998F37B412A5D6A8473A64C37EEB91FF95F (void);
// 0x000009F0 System.Void BNG.RagdollHelper::Start()
extern void RagdollHelper_Start_m45ACD86AEE0BA7931419739DB83926FA524127CF (void);
// 0x000009F1 System.Void BNG.RagdollHelper::.ctor()
extern void RagdollHelper__ctor_m8AF5C2E5C68181E85E843E56A9195A5763F649A6 (void);
// 0x000009F2 System.Void BNG.RingHelper::Start()
extern void RingHelper_Start_m9B4FA89DC6578D49C4BA2DA3834C088AE20AC464 (void);
// 0x000009F3 System.Void BNG.RingHelper::Update()
extern void RingHelper_Update_mB1D686781689430C7D3788C197273CD569058EDF (void);
// 0x000009F4 System.Void BNG.RingHelper::AssignCamera()
extern void RingHelper_AssignCamera_m3530C1A5AFD87038EA5FB30486F1AA472DED98F5 (void);
// 0x000009F5 System.Void BNG.RingHelper::AssignGrabbers()
extern void RingHelper_AssignGrabbers_mD76B64441D0F1034C5D6CD88B7F7EEAD7423F829 (void);
// 0x000009F6 UnityEngine.Color BNG.RingHelper::getSelectedColor()
extern void RingHelper_getSelectedColor_m5D98ECE615250DAF5CE1CFE0BF9D6EA86E47D91A (void);
// 0x000009F7 System.Void BNG.RingHelper::.ctor()
extern void RingHelper__ctor_m348A3C055F421D59B36BB75C370892075FDD39F2 (void);
// 0x000009F8 System.Void BNG.ScaleMaterialHelper::Start()
extern void ScaleMaterialHelper_Start_mB583982C1F887B02E183E295A55D7DCFC2ACCDB2 (void);
// 0x000009F9 System.Void BNG.ScaleMaterialHelper::updateTexture()
extern void ScaleMaterialHelper_updateTexture_m4367C00350E17E41CD23EF3A08B416F12F13B506 (void);
// 0x000009FA System.Void BNG.ScaleMaterialHelper::OnDrawGizmosSelected()
extern void ScaleMaterialHelper_OnDrawGizmosSelected_mFED377B26C6C3C3EE83D6F8B15CED65CCACD162C (void);
// 0x000009FB System.Void BNG.ScaleMaterialHelper::.ctor()
extern void ScaleMaterialHelper__ctor_m55557AF8A6E1ED1EA1A9212C5FB48A91D596BC9C (void);
// 0x000009FC System.Void BNG.StaticBatch::Start()
extern void StaticBatch_Start_m0EF6D24E64DCFF0EEA6B6FD2490AED1410A17F4C (void);
// 0x000009FD System.Void BNG.StaticBatch::.ctor()
extern void StaticBatch__ctor_m0D16A6689B36BB1520BC2E6A08A849B9BF1ECDC5 (void);
// 0x000009FE System.Void BNG.TeleportDestination::.ctor()
extern void TeleportDestination__ctor_mEA19F056F0275B56CC66D42FEA6DEB6FA6759677 (void);
// 0x000009FF System.Void BNG.TeleportPlayerOnEnter::OnTriggerEnter(UnityEngine.Collider)
extern void TeleportPlayerOnEnter_OnTriggerEnter_m17DB864460699473831F881C36B8FEDB9B12CAAF (void);
// 0x00000A00 System.Void BNG.TeleportPlayerOnEnter::.ctor()
extern void TeleportPlayerOnEnter__ctor_m93B56FCA3C3BAD816CBA25531B68C8B936F48287 (void);
// 0x00000A01 System.Void BNG.UITrigger::.ctor()
extern void UITrigger__ctor_mC0B136AFAB16B0E094832633F668BB2144DC803C (void);
// 0x00000A02 System.Void BNG.VRIFGrabpointUpdater::Start()
extern void VRIFGrabpointUpdater_Start_m91AA5C7EE6961B26ABA9ED5E5BFEFDBB41AC2D3A (void);
// 0x00000A03 System.Void BNG.VRIFGrabpointUpdater::ApplyGrabPointUpdate()
extern void VRIFGrabpointUpdater_ApplyGrabPointUpdate_m5CC314D536D8E52247B99E7E6AF8DF117010E5C2 (void);
// 0x00000A04 System.Void BNG.VRIFGrabpointUpdater::.ctor()
extern void VRIFGrabpointUpdater__ctor_m8B01A9B3AA73462F27C7F6D082970154E80209AD (void);
// 0x00000A05 System.Void BNG.UIButtonCollider::Awake()
extern void UIButtonCollider_Awake_m81FD2F2F31A0D4E65D13B1707879B78A742AB2D3 (void);
// 0x00000A06 System.Void BNG.UIButtonCollider::Update()
extern void UIButtonCollider_Update_mD930D32BDDBB3F5E23031B49A9A6ED7DD1A076B4 (void);
// 0x00000A07 System.Void BNG.UIButtonCollider::OnTriggerEnter(UnityEngine.Collider)
extern void UIButtonCollider_OnTriggerEnter_m4F0FAED675F2617E87C511EDC4D52FB7A9188954 (void);
// 0x00000A08 System.Void BNG.UIButtonCollider::OnTriggerExit(UnityEngine.Collider)
extern void UIButtonCollider_OnTriggerExit_m80818CE2B98BBEA522ACD2CF2302BF2B1AECB803 (void);
// 0x00000A09 System.Void BNG.UIButtonCollider::.ctor()
extern void UIButtonCollider__ctor_m9C5F99FE2840119790170343B1A9E41850B3C575 (void);
// 0x00000A0A System.Void BNG.UICanvasGroup::ActivateCanvas(System.Int32)
extern void UICanvasGroup_ActivateCanvas_mE4A23C4A8D611A5B3374A69A1C29C449970AFBBF (void);
// 0x00000A0B System.Void BNG.UICanvasGroup::.ctor()
extern void UICanvasGroup__ctor_m6E692768DDA62FBC6C5D8FD625D9DB121C531CFA (void);
// 0x00000A0C System.Void BNG.UIPointer::Awake()
extern void UIPointer_Awake_m69730CED7472ABF03AE719475280B59E3F2A545D (void);
// 0x00000A0D System.Void BNG.UIPointer::OnEnable()
extern void UIPointer_OnEnable_m75186E8A057F0FAD0A7BBFE07302BE3224D7F75A (void);
// 0x00000A0E System.Void BNG.UIPointer::updateUITransforms()
extern void UIPointer_updateUITransforms_mA366E947FAFCAF93E70F3C77A978853560970C5C (void);
// 0x00000A0F System.Void BNG.UIPointer::Update()
extern void UIPointer_Update_m0E1E89304AF18D85D1450B6CE0D650C5996F3FA8 (void);
// 0x00000A10 System.Void BNG.UIPointer::UpdatePointer()
extern void UIPointer_UpdatePointer_m686699772361C814A637E197979F573477D66B07 (void);
// 0x00000A11 System.Void BNG.UIPointer::HidePointer()
extern void UIPointer_HidePointer_m4C8909B1181A0F43156BDA44826DFE643D162A88 (void);
// 0x00000A12 System.Void BNG.UIPointer::.ctor()
extern void UIPointer__ctor_m022152C2BFEED4E05660C0D6CC693D3160EA6B34 (void);
// 0x00000A13 System.Void BNG.VRCanvas::Start()
extern void VRCanvas_Start_m425CA3D5E2C82A9F9BD1E3E0C2E32CF03E7F145F (void);
// 0x00000A14 System.Void BNG.VRCanvas::.ctor()
extern void VRCanvas__ctor_mCC3E4C15A9167965CD1656E978C346FFFF039C06 (void);
// 0x00000A15 System.Void BNG.VRKeyboard::Awake()
extern void VRKeyboard_Awake_m4FA667BE4963019A398DE69D5D4B51557BE398E2 (void);
// 0x00000A16 System.Void BNG.VRKeyboard::PressKey(System.String)
extern void VRKeyboard_PressKey_m7AC3EFB8BE01EA20161BA43D0E3B0235FCE334B8 (void);
// 0x00000A17 System.Void BNG.VRKeyboard::UpdateInputField(System.String)
extern void VRKeyboard_UpdateInputField_m88E94DED3C3F62D515280D26F466AA8E909F0004 (void);
// 0x00000A18 System.Void BNG.VRKeyboard::PlayClickSound()
extern void VRKeyboard_PlayClickSound_m7AAB1ADDE7C4BA56B56DE7777226965B164D4AC1 (void);
// 0x00000A19 System.Void BNG.VRKeyboard::MoveCaretUp()
extern void VRKeyboard_MoveCaretUp_mC7DC6ADFDCED8B66798430C32692F4D1731C396C (void);
// 0x00000A1A System.Void BNG.VRKeyboard::MoveCaretBack()
extern void VRKeyboard_MoveCaretBack_m007150DB3DA196E5F0A8E86C50A0FE2F4EA1C92F (void);
// 0x00000A1B System.Void BNG.VRKeyboard::ToggleShift()
extern void VRKeyboard_ToggleShift_mFF607A7489041670F017A9D71A3C5767E0AFBED7 (void);
// 0x00000A1C System.Collections.IEnumerator BNG.VRKeyboard::IncreaseInputFieldCareteRoutine()
extern void VRKeyboard_IncreaseInputFieldCareteRoutine_m6502D7D720C5F164BD769F8852EB2B8E74C6D75D (void);
// 0x00000A1D System.Collections.IEnumerator BNG.VRKeyboard::DecreaseInputFieldCareteRoutine()
extern void VRKeyboard_DecreaseInputFieldCareteRoutine_m1AE305D903A46471AE40965482FA7DAA92B6ED44 (void);
// 0x00000A1E System.Void BNG.VRKeyboard::AttachToInputField(UnityEngine.UI.InputField)
extern void VRKeyboard_AttachToInputField_m807384747F2012A7097CC6B2B2D495EB1252D22D (void);
// 0x00000A1F System.Void BNG.VRKeyboard::.ctor()
extern void VRKeyboard__ctor_m80D7318B57CFC0DFD1AC8C4A9903DCD0667748CF (void);
// 0x00000A20 System.Void BNG.VRKeyboard/<IncreaseInputFieldCareteRoutine>d__11::.ctor(System.Int32)
extern void U3CIncreaseInputFieldCareteRoutineU3Ed__11__ctor_m45F042DB132B6DF8CC147FDDF8B02E03B2E69AD3 (void);
// 0x00000A21 System.Void BNG.VRKeyboard/<IncreaseInputFieldCareteRoutine>d__11::System.IDisposable.Dispose()
extern void U3CIncreaseInputFieldCareteRoutineU3Ed__11_System_IDisposable_Dispose_m753F2576F5FFFE17A49DB23E241BC3715C76BAFD (void);
// 0x00000A22 System.Boolean BNG.VRKeyboard/<IncreaseInputFieldCareteRoutine>d__11::MoveNext()
extern void U3CIncreaseInputFieldCareteRoutineU3Ed__11_MoveNext_mE93B47EA93BF8239D17F0D1B88A76D13644E2B15 (void);
// 0x00000A23 System.Object BNG.VRKeyboard/<IncreaseInputFieldCareteRoutine>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CIncreaseInputFieldCareteRoutineU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m027B2AE78004B9227EF7B596B088452BA0506F45 (void);
// 0x00000A24 System.Void BNG.VRKeyboard/<IncreaseInputFieldCareteRoutine>d__11::System.Collections.IEnumerator.Reset()
extern void U3CIncreaseInputFieldCareteRoutineU3Ed__11_System_Collections_IEnumerator_Reset_mCDC66CF75060732FFF41A1C54806F156E45174F0 (void);
// 0x00000A25 System.Object BNG.VRKeyboard/<IncreaseInputFieldCareteRoutine>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CIncreaseInputFieldCareteRoutineU3Ed__11_System_Collections_IEnumerator_get_Current_mF6B3BC8C0401976684AAACA4580A4F40282A1F6C (void);
// 0x00000A26 System.Void BNG.VRKeyboard/<DecreaseInputFieldCareteRoutine>d__12::.ctor(System.Int32)
extern void U3CDecreaseInputFieldCareteRoutineU3Ed__12__ctor_m4EC051CEA1251B15D7F5FD2E8CD1E297649734CB (void);
// 0x00000A27 System.Void BNG.VRKeyboard/<DecreaseInputFieldCareteRoutine>d__12::System.IDisposable.Dispose()
extern void U3CDecreaseInputFieldCareteRoutineU3Ed__12_System_IDisposable_Dispose_m61DC59CB10351BAFA8DA3B280DBD64E8344F5A01 (void);
// 0x00000A28 System.Boolean BNG.VRKeyboard/<DecreaseInputFieldCareteRoutine>d__12::MoveNext()
extern void U3CDecreaseInputFieldCareteRoutineU3Ed__12_MoveNext_mB022D5DC7BBA39076CFC1167C3CE93A841674C5C (void);
// 0x00000A29 System.Object BNG.VRKeyboard/<DecreaseInputFieldCareteRoutine>d__12::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDecreaseInputFieldCareteRoutineU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0D074637B927F2BCBD5AAA7F13E26EFED969C94E (void);
// 0x00000A2A System.Void BNG.VRKeyboard/<DecreaseInputFieldCareteRoutine>d__12::System.Collections.IEnumerator.Reset()
extern void U3CDecreaseInputFieldCareteRoutineU3Ed__12_System_Collections_IEnumerator_Reset_m5FFC85AC31E2F5C5291BEB8C716ECE9FD9323A6D (void);
// 0x00000A2B System.Object BNG.VRKeyboard/<DecreaseInputFieldCareteRoutine>d__12::System.Collections.IEnumerator.get_Current()
extern void U3CDecreaseInputFieldCareteRoutineU3Ed__12_System_Collections_IEnumerator_get_Current_mE1D1C7AF887E5381AC6476E00F326D9F1A156838 (void);
// 0x00000A2C System.Void BNG.VRKeyboardKey::Awake()
extern void VRKeyboardKey_Awake_m28CD209323E3C9D7C068A3A5F5C294676074CFBA (void);
// 0x00000A2D System.Void BNG.VRKeyboardKey::ToggleShift()
extern void VRKeyboardKey_ToggleShift_m41293C588928EAB1F1B97114F8FDE2B537965BEE (void);
// 0x00000A2E System.Void BNG.VRKeyboardKey::OnKeyHit()
extern void VRKeyboardKey_OnKeyHit_m29546821E7F2F8D4EC850F46D6F852AF940FFC36 (void);
// 0x00000A2F System.Void BNG.VRKeyboardKey::OnKeyHit(System.String)
extern void VRKeyboardKey_OnKeyHit_m7AE7990A83375AA05360C1CA72595792107E255A (void);
// 0x00000A30 System.Void BNG.VRKeyboardKey::.ctor()
extern void VRKeyboardKey__ctor_m5ED75C83D5FDD45C8DA029A303D95EE5430B6D73 (void);
// 0x00000A31 System.Void BNG.VRTextInput::Awake()
extern void VRTextInput_Awake_mCE93F9DA1311C52BAF5998F9AD8F1519717F6E95 (void);
// 0x00000A32 System.Void BNG.VRTextInput::Update()
extern void VRTextInput_Update_m9DB15E7E8646FD90C120B06AB063F76CCFDD6033 (void);
// 0x00000A33 System.Void BNG.VRTextInput::OnInputSelect()
extern void VRTextInput_OnInputSelect_mF6BA8DF36B007EE85DACD8F1C8E63F061C296FF0 (void);
// 0x00000A34 System.Void BNG.VRTextInput::OnInputDeselect()
extern void VRTextInput_OnInputDeselect_mC32A631D9CA35A0526F627AE761B9D4E790D62A6 (void);
// 0x00000A35 System.Void BNG.VRTextInput::Reset()
extern void VRTextInput_Reset_mC6E8627D3E684407796A0587425DEE7AC8CFCAD7 (void);
// 0x00000A36 System.Void BNG.VRTextInput::.ctor()
extern void VRTextInput__ctor_mCA2ACB71D192B82BAA43B1A183EA9DE93F969F82 (void);
// 0x00000A37 UnityEngine.EventSystems.PointerEventData BNG.VRUISystem::get_EventData()
extern void VRUISystem_get_EventData_m5A84EEA6F5D734D4C9539572D3BDE525ED349A0F (void);
// 0x00000A38 System.Void BNG.VRUISystem::set_EventData(UnityEngine.EventSystems.PointerEventData)
extern void VRUISystem_set_EventData_m286CCFF96EDF05E2E710837DA5366C52F0DE985E (void);
// 0x00000A39 BNG.VRUISystem BNG.VRUISystem::get_Instance()
extern void VRUISystem_get_Instance_mD97758F84664BD80358C3F209DAF624AC03AB271 (void);
// 0x00000A3A System.Void BNG.VRUISystem::Awake()
extern void VRUISystem_Awake_mE570A47F4660F1A4FAC583D9E15781433EE88213 (void);
// 0x00000A3B System.Void BNG.VRUISystem::initEventSystem()
extern void VRUISystem_initEventSystem_mB5328099BC9B8BF1C5318F0A481F7CA3622F53A4 (void);
// 0x00000A3C System.Void BNG.VRUISystem::Start()
extern void VRUISystem_Start_m89AF6C31A81A5B760FAA97E81B7A8F3A5DF02FB0 (void);
// 0x00000A3D System.Void BNG.VRUISystem::init()
extern void VRUISystem_init_mD83A8F25C7803E1EB3C37F576F8B940FA5C2701F (void);
// 0x00000A3E System.Void BNG.VRUISystem::Process()
extern void VRUISystem_Process_m263CD7E5F478263B41838A36CF8BC36AF6F49A23 (void);
// 0x00000A3F System.Void BNG.VRUISystem::DoProcess()
extern void VRUISystem_DoProcess_mED1D34586060101246C22A21203C9C18EE2A75D5 (void);
// 0x00000A40 System.Boolean BNG.VRUISystem::InputReady()
extern void VRUISystem_InputReady_m602B31B91B2F56E1EE834E177002B96B926F50C9 (void);
// 0x00000A41 System.Boolean BNG.VRUISystem::CameraCasterReady()
extern void VRUISystem_CameraCasterReady_m78B3FBCEF38FEFAF1057B24AFFE45C62EAFBE197 (void);
// 0x00000A42 System.Void BNG.VRUISystem::PressDown()
extern void VRUISystem_PressDown_m9B83D446799E2E88EF8E8ACAE13506129A084F0F (void);
// 0x00000A43 System.Void BNG.VRUISystem::Press()
extern void VRUISystem_Press_m53029C32D12D8B5335D7BC7F4A6C94F2C2E98F01 (void);
// 0x00000A44 System.Void BNG.VRUISystem::Release()
extern void VRUISystem_Release_m06F5FE7BDD007127775C0ABA4F2A8A00452A0B11 (void);
// 0x00000A45 System.Void BNG.VRUISystem::ClearAll()
extern void VRUISystem_ClearAll_mDEFA603EC2E05C64DCE9F19A4C8E75F7B14B525B (void);
// 0x00000A46 System.Void BNG.VRUISystem::SetPressingObject(UnityEngine.GameObject)
extern void VRUISystem_SetPressingObject_m2CE936DE4472A598DD2B4F894284852B8F99BDEC (void);
// 0x00000A47 System.Void BNG.VRUISystem::SetDraggingObject(UnityEngine.GameObject)
extern void VRUISystem_SetDraggingObject_m90926D2DD41FEA267E5BF99238073F67B76AE249 (void);
// 0x00000A48 System.Void BNG.VRUISystem::SetReleasingObject(UnityEngine.GameObject)
extern void VRUISystem_SetReleasingObject_mED8745FF8E87A6BB87BA1170C707221A48A48B0A (void);
// 0x00000A49 System.Void BNG.VRUISystem::AssignCameraToAllCanvases(UnityEngine.Camera)
extern void VRUISystem_AssignCameraToAllCanvases_mA3192DDE80F4D69CCDCFACA440D15C47018800AD (void);
// 0x00000A4A System.Void BNG.VRUISystem::AddCanvas(UnityEngine.Canvas)
extern void VRUISystem_AddCanvas_mFC80E6C080E6D984A86E25C22A93DC3B4B3812A1 (void);
// 0x00000A4B System.Void BNG.VRUISystem::AddCanvasToCamera(UnityEngine.Canvas,UnityEngine.Camera)
extern void VRUISystem_AddCanvasToCamera_mCEF6752EA7D6607A53ECCF3BBDB669FDF5ACE426 (void);
// 0x00000A4C System.Void BNG.VRUISystem::UpdateControllerHand(BNG.ControllerHand)
extern void VRUISystem_UpdateControllerHand_m2968AA6B926CBFA66837C0A7D213D380FF716D02 (void);
// 0x00000A4D System.Void BNG.VRUISystem::.ctor()
extern void VRUISystem__ctor_m960EB3E8F8DE3F80469BA34474231112326A1FE7 (void);
// 0x00000A4E System.Void BNG.DestroyIfPlayMode::Start()
extern void DestroyIfPlayMode_Start_m4155B2B69AF5CD3FB19905A1780F826CEEB3ABA2 (void);
// 0x00000A4F System.Void BNG.DestroyIfPlayMode::.ctor()
extern void DestroyIfPlayMode__ctor_m5672C42AA14759A21472D99990F9CA533542C69E (void);
// 0x00000A50 BNG.VRUtils BNG.VRUtils::get_Instance()
extern void VRUtils_get_Instance_m7AA00DAB70FF60A93B3C8222C3B6DD442133E99D (void);
// 0x00000A51 System.Void BNG.VRUtils::Awake()
extern void VRUtils_Awake_mA2FA53CBD383EC57BE89A35F3DD3F82846B43695 (void);
// 0x00000A52 System.Void BNG.VRUtils::Log(System.String)
extern void VRUtils_Log_mF963ECBBBB5A6CABAC21BD5E5FC583095288C2D3 (void);
// 0x00000A53 System.Void BNG.VRUtils::Warn(System.String)
extern void VRUtils_Warn_mDE28781732E0FACE75E11D01174093A2F6D05A4D (void);
// 0x00000A54 System.Void BNG.VRUtils::Error(System.String)
extern void VRUtils_Error_m5B26706E7145B386AE953F4A36E585A471CFE2EA (void);
// 0x00000A55 System.Void BNG.VRUtils::VRDebugLog(System.String,UnityEngine.Color)
extern void VRUtils_VRDebugLog_m23CAC007AC1BBB4BE53104AFDE089863160E4A99 (void);
// 0x00000A56 System.Void BNG.VRUtils::CullDebugPanel()
extern void VRUtils_CullDebugPanel_m131011AE3BC4F1D3F91EF6A742511F5A6A79092E (void);
// 0x00000A57 UnityEngine.AudioSource BNG.VRUtils::PlaySpatialClipAt(UnityEngine.AudioClip,UnityEngine.Vector3,System.Single,System.Single,System.Single)
extern void VRUtils_PlaySpatialClipAt_m140591815557106467AA58422E69E29BF485C110 (void);
// 0x00000A58 System.Single BNG.VRUtils::getRandomizedPitch(System.Single)
extern void VRUtils_getRandomizedPitch_mD74E3A5095FE6A7B2317D66BB96CE4733631E086 (void);
// 0x00000A59 System.Void BNG.VRUtils::.ctor()
extern void VRUtils__ctor_m93F61EBDE0496A70F55A582C16D949DE58691AC7 (void);
// 0x00000A5A System.Void BNG.AmmoDispenser::Update()
extern void AmmoDispenser_Update_m901FCC52EBA687048145AECA907BEEA442417B1E (void);
// 0x00000A5B System.Boolean BNG.AmmoDispenser::grabberHasWeapon(BNG.Grabber)
extern void AmmoDispenser_grabberHasWeapon_m8B0F3515833FEA634371CAE2FBBAAC474957BE3D (void);
// 0x00000A5C UnityEngine.GameObject BNG.AmmoDispenser::GetAmmo()
extern void AmmoDispenser_GetAmmo_m41B36145FBD124FB4393226DFFE3BA0BB16BE0F3 (void);
// 0x00000A5D System.Void BNG.AmmoDispenser::GrabAmmo(BNG.Grabber)
extern void AmmoDispenser_GrabAmmo_m1AB82B8376B93734A4F6220FDC3E49A70B41EC4A (void);
// 0x00000A5E System.Void BNG.AmmoDispenser::AddAmmo(System.String)
extern void AmmoDispenser_AddAmmo_mD67F600B1865CF8805112DFA09C9E9E41920F0BF (void);
// 0x00000A5F System.Void BNG.AmmoDispenser::.ctor()
extern void AmmoDispenser__ctor_mBB04FDC8FB56E9F4ABF604164682083C608715EB (void);
// 0x00000A60 System.Void BNG.AmmoDisplay::OnGUI()
extern void AmmoDisplay_OnGUI_m71405B97DF64F2743577FE20E53E5E1891C0CA6D (void);
// 0x00000A61 System.Void BNG.AmmoDisplay::.ctor()
extern void AmmoDisplay__ctor_mCAE6A3AE79AE3E6D4E8C7D71819D155E7179E9C6 (void);
// 0x00000A62 System.Void BNG.Bullet::.ctor()
extern void Bullet__ctor_m3EBF0BF131781B03E5D1C1C4FF2638C53AB7C6AB (void);
// 0x00000A63 System.Void BNG.BulletInsert::OnTriggerEnter(UnityEngine.Collider)
extern void BulletInsert_OnTriggerEnter_m7F38113116276027D0D1CA756CAD858263AEE7F1 (void);
// 0x00000A64 System.Void BNG.BulletInsert::.ctor()
extern void BulletInsert__ctor_m85443F15FA81372CA6A80EA16F300CDC320F1227 (void);
// 0x00000A65 System.Void BNG.MagazineSlide::Awake()
extern void MagazineSlide_Awake_m014D8D73686AC5EF57081BDC4677F0D564096E9B (void);
// 0x00000A66 System.Void BNG.MagazineSlide::LateUpdate()
extern void MagazineSlide_LateUpdate_m2C13213B534D229D0717DAB4A9352D9C66D7A206 (void);
// 0x00000A67 System.Boolean BNG.MagazineSlide::recentlyEjected()
extern void MagazineSlide_recentlyEjected_mA5D634FD19F6F55D09BD713A71DBA195E17FE8D7 (void);
// 0x00000A68 System.Void BNG.MagazineSlide::moveMagazine(UnityEngine.Vector3)
extern void MagazineSlide_moveMagazine_m9A2E09EE24BDAD50829C7D65AC0AF4C5074A9F5E (void);
// 0x00000A69 System.Void BNG.MagazineSlide::CheckGrabClipInput()
extern void MagazineSlide_CheckGrabClipInput_m4C4DAA1362FC59524C01305DBA825D65D4717DCE (void);
// 0x00000A6A System.Void BNG.MagazineSlide::attachMagazine()
extern void MagazineSlide_attachMagazine_m8A8AB649456AA4D12E902E4AF0416215873E6C0D (void);
// 0x00000A6B BNG.Grabbable BNG.MagazineSlide::detachMagazine()
extern void MagazineSlide_detachMagazine_m7D8887A4D85BB507DD32005D64DDB2E96B5FA37E (void);
// 0x00000A6C System.Void BNG.MagazineSlide::EjectMagazine()
extern void MagazineSlide_EjectMagazine_m2709F5A1A5FC2C22089EF8996ACDD8711332487F (void);
// 0x00000A6D System.Collections.IEnumerator BNG.MagazineSlide::EjectMagRoutine(BNG.Grabbable)
extern void MagazineSlide_EjectMagRoutine_m6022B8E32541B3664144306DC60E8996BBD5BB45 (void);
// 0x00000A6E System.Void BNG.MagazineSlide::OnGrabClipArea(BNG.Grabber)
extern void MagazineSlide_OnGrabClipArea_m42230BC8B39B05F71F9CC41E6887A6A63EA7D25C (void);
// 0x00000A6F System.Void BNG.MagazineSlide::AttachGrabbableMagazine(BNG.Grabbable,UnityEngine.Collider)
extern void MagazineSlide_AttachGrabbableMagazine_m7ABCCB4A8A9D0C9967D03998FB1ED0C1703461E2 (void);
// 0x00000A70 System.Void BNG.MagazineSlide::OnTriggerEnter(UnityEngine.Collider)
extern void MagazineSlide_OnTriggerEnter_m65D03749757B7A76FD565AD9E1DCF0F64EDA9AB5 (void);
// 0x00000A71 System.Void BNG.MagazineSlide::.ctor()
extern void MagazineSlide__ctor_m10C52E9B2ACE443C457EB00B6620FAE5BD017164 (void);
// 0x00000A72 System.Void BNG.MagazineSlide/<EjectMagRoutine>d__23::.ctor(System.Int32)
extern void U3CEjectMagRoutineU3Ed__23__ctor_mCE7F658BA8248261C8000AF40944BF1971C009B9 (void);
// 0x00000A73 System.Void BNG.MagazineSlide/<EjectMagRoutine>d__23::System.IDisposable.Dispose()
extern void U3CEjectMagRoutineU3Ed__23_System_IDisposable_Dispose_mD68AF0496811F6B740C49533CAE290664A795E02 (void);
// 0x00000A74 System.Boolean BNG.MagazineSlide/<EjectMagRoutine>d__23::MoveNext()
extern void U3CEjectMagRoutineU3Ed__23_MoveNext_m1343CA23BAA6A6AD1540DF0E902AE4550DA1B122 (void);
// 0x00000A75 System.Object BNG.MagazineSlide/<EjectMagRoutine>d__23::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CEjectMagRoutineU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3729222694C223FB4C4FC3C4BBE6B4E5A7ADA406 (void);
// 0x00000A76 System.Void BNG.MagazineSlide/<EjectMagRoutine>d__23::System.Collections.IEnumerator.Reset()
extern void U3CEjectMagRoutineU3Ed__23_System_Collections_IEnumerator_Reset_mC9DF130CF70466992D1DB8A6853DC629E70FE8F5 (void);
// 0x00000A77 System.Object BNG.MagazineSlide/<EjectMagRoutine>d__23::System.Collections.IEnumerator.get_Current()
extern void U3CEjectMagRoutineU3Ed__23_System_Collections_IEnumerator_get_Current_m30511E60DF6A16C65C27AE3B16CCDDD281552375 (void);
// 0x00000A78 System.Void BNG.Projectile::OnCollisionEnter(UnityEngine.Collision)
extern void Projectile_OnCollisionEnter_m4D3CA4D3251E8266A0ABC47FE6313E82A9D9D6C7 (void);
// 0x00000A79 System.Void BNG.Projectile::OnCollisionEvent(UnityEngine.Collision)
extern void Projectile_OnCollisionEvent_mC77ED3BD49049F6E1A0222BA85168F9D2581FDD9 (void);
// 0x00000A7A System.Void BNG.Projectile::DoHitFX(UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Collider)
extern void Projectile_DoHitFX_mA0256AB6391A12E1CDC0E35106BC2B9845BEA3F0 (void);
// 0x00000A7B System.Void BNG.Projectile::MarkAsRaycastBullet()
extern void Projectile_MarkAsRaycastBullet_m98F6BB130FCA221113C6BF714774C84CEB8FA20D (void);
// 0x00000A7C System.Void BNG.Projectile::DoRayCastProjectile()
extern void Projectile_DoRayCastProjectile_m069BAAA00C4E49745AC2823A934449999C830214 (void);
// 0x00000A7D System.Collections.IEnumerator BNG.Projectile::CheckForRaycast()
extern void Projectile_CheckForRaycast_m4D77A42F4A68CAB3B3D044A53062D37BE870E83E (void);
// 0x00000A7E System.Void BNG.Projectile::.ctor()
extern void Projectile__ctor_m4A0B1D74F24D70ACDDA7E7CD7EED4D47237E24B9 (void);
// 0x00000A7F System.Void BNG.Projectile/<CheckForRaycast>d__13::.ctor(System.Int32)
extern void U3CCheckForRaycastU3Ed__13__ctor_mC8B8B6CBB6E308BF6629949DD45747112B5A720B (void);
// 0x00000A80 System.Void BNG.Projectile/<CheckForRaycast>d__13::System.IDisposable.Dispose()
extern void U3CCheckForRaycastU3Ed__13_System_IDisposable_Dispose_m4B0839B513CBAF8FB2E6E6A10ECF14AB5B9FDDA4 (void);
// 0x00000A81 System.Boolean BNG.Projectile/<CheckForRaycast>d__13::MoveNext()
extern void U3CCheckForRaycastU3Ed__13_MoveNext_mF927828E0D81015DD8A735BDD7069B30E218CC8D (void);
// 0x00000A82 System.Object BNG.Projectile/<CheckForRaycast>d__13::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCheckForRaycastU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9757C356C93330958C419B749E72E85965B3CC8C (void);
// 0x00000A83 System.Void BNG.Projectile/<CheckForRaycast>d__13::System.Collections.IEnumerator.Reset()
extern void U3CCheckForRaycastU3Ed__13_System_Collections_IEnumerator_Reset_m9F9B273D801025BC8C295F4BDBEDF36D3F86BBEC (void);
// 0x00000A84 System.Object BNG.Projectile/<CheckForRaycast>d__13::System.Collections.IEnumerator.get_Current()
extern void U3CCheckForRaycastU3Ed__13_System_Collections_IEnumerator_get_Current_m6400D4F08273B0D122BAB28599A7DB4EF949E719 (void);
// 0x00000A85 System.Void BNG.RaycastWeapon::Start()
extern void RaycastWeapon_Start_m088DFE9F2B526CEB43CF2F83F9194D1A669B8D3D (void);
// 0x00000A86 System.Void BNG.RaycastWeapon::OnTrigger(System.Single)
extern void RaycastWeapon_OnTrigger_mF74F921FFAAB156C4B31D00B79FB5EE13CA45468 (void);
// 0x00000A87 System.Void BNG.RaycastWeapon::checkSlideInput()
extern void RaycastWeapon_checkSlideInput_m20F1A6BA9768F5E5EE8D0C3F41C9554326B1A3E8 (void);
// 0x00000A88 System.Void BNG.RaycastWeapon::checkEjectInput()
extern void RaycastWeapon_checkEjectInput_m26FBAC88544A099E974D4D493FCEA186745D129A (void);
// 0x00000A89 System.Void BNG.RaycastWeapon::CheckReloadInput()
extern void RaycastWeapon_CheckReloadInput_m3C3E54083FA0E8318E6ADB93CCBB05582A7F360A (void);
// 0x00000A8A System.Void BNG.RaycastWeapon::UnlockSlide()
extern void RaycastWeapon_UnlockSlide_mD4D9770411F18BB33DEE7E4E31BA4551A192B6D7 (void);
// 0x00000A8B System.Void BNG.RaycastWeapon::EjectMagazine()
extern void RaycastWeapon_EjectMagazine_m53393216FF73326DF70C16C9411D95DA011451A2 (void);
// 0x00000A8C System.Void BNG.RaycastWeapon::Shoot()
extern void RaycastWeapon_Shoot_mD5A0B076589011DACC03DFE8352D0079A0F14F1C (void);
// 0x00000A8D System.Void BNG.RaycastWeapon::ApplyRecoil()
extern void RaycastWeapon_ApplyRecoil_m4DC24E444FE98E06C5DC8A6CF786E70820D4D4DE (void);
// 0x00000A8E System.Void BNG.RaycastWeapon::OnRaycastHit(UnityEngine.RaycastHit)
extern void RaycastWeapon_OnRaycastHit_m7CA11082405451F12F4A37BF650E4121C614346F (void);
// 0x00000A8F System.Void BNG.RaycastWeapon::ApplyParticleFX(UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Collider)
extern void RaycastWeapon_ApplyParticleFX_m5EF741E5AEB6CCB9C8554283DE6338B55EEB0AE3 (void);
// 0x00000A90 System.Void BNG.RaycastWeapon::OnAttachedAmmo()
extern void RaycastWeapon_OnAttachedAmmo_m49BF500072A8DFF66D29AA0A28939D7F88DE9D2B (void);
// 0x00000A91 System.Void BNG.RaycastWeapon::OnDetachedAmmo()
extern void RaycastWeapon_OnDetachedAmmo_m761A55E303947FBB2E6BF46FC485F52DAFDE7746 (void);
// 0x00000A92 System.Int32 BNG.RaycastWeapon::GetBulletCount()
extern void RaycastWeapon_GetBulletCount_m2D711368E575D9A566318FE5E4121BA6B144A379 (void);
// 0x00000A93 System.Void BNG.RaycastWeapon::RemoveBullet()
extern void RaycastWeapon_RemoveBullet_m5B553B4C590FAD68289EDE3AF879727AC1E01934 (void);
// 0x00000A94 System.Void BNG.RaycastWeapon::Reload()
extern void RaycastWeapon_Reload_m859D580D1A61C6FE845E378F92FACB567E443CDC (void);
// 0x00000A95 System.Void BNG.RaycastWeapon::updateChamberedBullet()
extern void RaycastWeapon_updateChamberedBullet_m7B841D186844650E44EE06ADA6619D0B35204128 (void);
// 0x00000A96 System.Void BNG.RaycastWeapon::chamberRound()
extern void RaycastWeapon_chamberRound_m246285E40A67D03A38A05B8D5E3752CABA8D8798 (void);
// 0x00000A97 System.Void BNG.RaycastWeapon::randomizeMuzzleFlashScaleRotation()
extern void RaycastWeapon_randomizeMuzzleFlashScaleRotation_m983625E82643A8C2A1B66274F8235717E25983F0 (void);
// 0x00000A98 System.Void BNG.RaycastWeapon::OnWeaponCharged(System.Boolean)
extern void RaycastWeapon_OnWeaponCharged_m29A7994808890365D515CC660836B6A8671A26CA (void);
// 0x00000A99 System.Void BNG.RaycastWeapon::ejectCasing()
extern void RaycastWeapon_ejectCasing_m7E64983B2DD5DF3E3BF354CF75078ACEF1659DB3 (void);
// 0x00000A9A System.Collections.IEnumerator BNG.RaycastWeapon::doMuzzleFlash()
extern void RaycastWeapon_doMuzzleFlash_mA343CADBC9EE1F5827E8CA9BBBCA5CCC5E85729C (void);
// 0x00000A9B System.Collections.IEnumerator BNG.RaycastWeapon::animateSlideAndEject()
extern void RaycastWeapon_animateSlideAndEject_m97EF32D4C5C0ED7030A7000D55816D52812C210A (void);
// 0x00000A9C System.Void BNG.RaycastWeapon::.ctor()
extern void RaycastWeapon__ctor_mB3EF17D286241C8EBA55B14374A37D723A67B8F5 (void);
// 0x00000A9D System.Void BNG.RaycastWeapon/<doMuzzleFlash>d__74::.ctor(System.Int32)
extern void U3CdoMuzzleFlashU3Ed__74__ctor_m67E6347BEC0A0F4002C32A2E749DCAF584309801 (void);
// 0x00000A9E System.Void BNG.RaycastWeapon/<doMuzzleFlash>d__74::System.IDisposable.Dispose()
extern void U3CdoMuzzleFlashU3Ed__74_System_IDisposable_Dispose_mB16A9F9BD2F6FFEE36C8F4FB93BE273BFCEBF4CD (void);
// 0x00000A9F System.Boolean BNG.RaycastWeapon/<doMuzzleFlash>d__74::MoveNext()
extern void U3CdoMuzzleFlashU3Ed__74_MoveNext_mCEDC6E3116DC6B428BAA75AA73149B8FC6772139 (void);
// 0x00000AA0 System.Object BNG.RaycastWeapon/<doMuzzleFlash>d__74::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CdoMuzzleFlashU3Ed__74_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA0762CD160AC327A9DC2A88ABA8B88E53DF24762 (void);
// 0x00000AA1 System.Void BNG.RaycastWeapon/<doMuzzleFlash>d__74::System.Collections.IEnumerator.Reset()
extern void U3CdoMuzzleFlashU3Ed__74_System_Collections_IEnumerator_Reset_m11DD22F570A901853A7993A4BC7927727A901323 (void);
// 0x00000AA2 System.Object BNG.RaycastWeapon/<doMuzzleFlash>d__74::System.Collections.IEnumerator.get_Current()
extern void U3CdoMuzzleFlashU3Ed__74_System_Collections_IEnumerator_get_Current_mE2188F32B79B4D054792B57F09C0D67195294CEE (void);
// 0x00000AA3 System.Void BNG.RaycastWeapon/<animateSlideAndEject>d__75::.ctor(System.Int32)
extern void U3CanimateSlideAndEjectU3Ed__75__ctor_mA8093F159CB825247734E8AC6AD554830B08D42C (void);
// 0x00000AA4 System.Void BNG.RaycastWeapon/<animateSlideAndEject>d__75::System.IDisposable.Dispose()
extern void U3CanimateSlideAndEjectU3Ed__75_System_IDisposable_Dispose_mCEE099200CB96B8FD835DE34A1B4886D972D210B (void);
// 0x00000AA5 System.Boolean BNG.RaycastWeapon/<animateSlideAndEject>d__75::MoveNext()
extern void U3CanimateSlideAndEjectU3Ed__75_MoveNext_m526CDF4B343FBCE904EB46D56C8B7B9063EB042C (void);
// 0x00000AA6 System.Object BNG.RaycastWeapon/<animateSlideAndEject>d__75::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CanimateSlideAndEjectU3Ed__75_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0F88EBA6310003E92C1937AC9B6BD47112D0CAAC (void);
// 0x00000AA7 System.Void BNG.RaycastWeapon/<animateSlideAndEject>d__75::System.Collections.IEnumerator.Reset()
extern void U3CanimateSlideAndEjectU3Ed__75_System_Collections_IEnumerator_Reset_m368E7B2A30F22F1A327B5670D1AAE3B5BC05044F (void);
// 0x00000AA8 System.Object BNG.RaycastWeapon/<animateSlideAndEject>d__75::System.Collections.IEnumerator.get_Current()
extern void U3CanimateSlideAndEjectU3Ed__75_System_Collections_IEnumerator_get_Current_mFA299CD3E0642A512C7E9061E3498F1A5F763E45 (void);
// 0x00000AA9 System.Void BNG.WeaponSlide::Start()
extern void WeaponSlide_Start_mF6BF252F43AC40B39B722BF13380919D566E9F35 (void);
// 0x00000AAA System.Void BNG.WeaponSlide::OnEnable()
extern void WeaponSlide_OnEnable_m9ED0E47730F234BA8A4013325A5D7002437F56F1 (void);
// 0x00000AAB System.Void BNG.WeaponSlide::OnDisable()
extern void WeaponSlide_OnDisable_m96CED0CF3128AF706B3138709ECDFFC0B98A40E8 (void);
// 0x00000AAC System.Void BNG.WeaponSlide::Update()
extern void WeaponSlide_Update_m6BE600BBF2C50177CD5A839880BB453AD1205DD0 (void);
// 0x00000AAD System.Void BNG.WeaponSlide::FixedUpdate()
extern void WeaponSlide_FixedUpdate_m65652DFB47852BED64C8581DDDD0C5406033919E (void);
// 0x00000AAE System.Void BNG.WeaponSlide::LockBack()
extern void WeaponSlide_LockBack_mAEAB34CD45422460C2536852514D2E7CD0187C88 (void);
// 0x00000AAF System.Void BNG.WeaponSlide::UnlockBack()
extern void WeaponSlide_UnlockBack_mA64D9EB4A8838F60290465A8CC8B15E4EF75B5DB (void);
// 0x00000AB0 System.Void BNG.WeaponSlide::onSlideBack()
extern void WeaponSlide_onSlideBack_m0899932CDEFD7373FEE5D30EAD7BF05ED06DEAA9 (void);
// 0x00000AB1 System.Void BNG.WeaponSlide::onSlideForward()
extern void WeaponSlide_onSlideForward_mCD127C11DB2CA2FB3E6F43F2BAA28ED7500C15DD (void);
// 0x00000AB2 System.Void BNG.WeaponSlide::LockSlidePosition()
extern void WeaponSlide_LockSlidePosition_mE49A25A669BBBB8B7877EBD9E73E419C8E9A670A (void);
// 0x00000AB3 System.Void BNG.WeaponSlide::UnlockSlidePosition()
extern void WeaponSlide_UnlockSlidePosition_mC1EFAB88298DA02606F04C29D039B3042C4F2E04 (void);
// 0x00000AB4 System.Collections.IEnumerator BNG.WeaponSlide::UnlockSlideRoutine()
extern void WeaponSlide_UnlockSlideRoutine_m4BD07AAEB5D32AEB6CC63ED3B95A95DCAD8CF66A (void);
// 0x00000AB5 System.Void BNG.WeaponSlide::playSoundInterval(System.Single,System.Single,System.Single)
extern void WeaponSlide_playSoundInterval_m44C26D3857EB4BB80E55545999A623E24DE832BC (void);
// 0x00000AB6 System.Void BNG.WeaponSlide::.ctor()
extern void WeaponSlide__ctor_m588FE1906E723B9B301A5BAA2DC6B7BE98D74D4D (void);
// 0x00000AB7 System.Void BNG.WeaponSlide/<UnlockSlideRoutine>d__27::.ctor(System.Int32)
extern void U3CUnlockSlideRoutineU3Ed__27__ctor_mFF7CE62005AD460AABE81DC1535FEA27ECA66239 (void);
// 0x00000AB8 System.Void BNG.WeaponSlide/<UnlockSlideRoutine>d__27::System.IDisposable.Dispose()
extern void U3CUnlockSlideRoutineU3Ed__27_System_IDisposable_Dispose_m9815711712F3427AF3D1D96F4AB356FCEAAFC99F (void);
// 0x00000AB9 System.Boolean BNG.WeaponSlide/<UnlockSlideRoutine>d__27::MoveNext()
extern void U3CUnlockSlideRoutineU3Ed__27_MoveNext_mA77DD403B3F851F683818AE6526F1817B60AFBA3 (void);
// 0x00000ABA System.Object BNG.WeaponSlide/<UnlockSlideRoutine>d__27::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CUnlockSlideRoutineU3Ed__27_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0FB3490EBBD9949B6FE38D486580A3D01DF13303 (void);
// 0x00000ABB System.Void BNG.WeaponSlide/<UnlockSlideRoutine>d__27::System.Collections.IEnumerator.Reset()
extern void U3CUnlockSlideRoutineU3Ed__27_System_Collections_IEnumerator_Reset_mC5540BFFE9AE9B7B6E750DB99D5EA556BA01BF51 (void);
// 0x00000ABC System.Object BNG.WeaponSlide/<UnlockSlideRoutine>d__27::System.Collections.IEnumerator.get_Current()
extern void U3CUnlockSlideRoutineU3Ed__27_System_Collections_IEnumerator_get_Current_m7D17E8CE3BFD434D3902C46A78D4683EB93AF186 (void);
static Il2CppMethodPointer s_methodPointers[2748] = 
{
	FixNonUniformScale_OnDrawGizmosSelected_mD129C37D8D8F97A675783E56CB5CE7961A8C5504,
	FixNonUniformScale_MakeUniform_mE53E4A87F82BCFE5BD27F76F5F5E95EB4192A217,
	FixNonUniformScale__ctor_m8EDCB00E217D23CE028EE27B8D08A7353A44575D,
	DebugUIBuilder_Awake_m403F18586F94598A6A54C5B1899CF76032848602,
	DebugUIBuilder_Show_mF284EB0ED0943893BD5799EBFCCB1DB6B192DC2E,
	DebugUIBuilder_Hide_m23BD4DC6DB4E714215B97C9A37417D70356FD0C2,
	DebugUIBuilder_StackedRelayout_mDE795B974E8386D3A693380CCFE57E98E67AAC60,
	DebugUIBuilder_PanelCentricRelayout_mCE6C50E7BA6C02D8CDD3F32E46A045B4CF2104B0,
	DebugUIBuilder_Relayout_m3937EE57F0E4C6F007BE6CB4CF0940FBEC300C28,
	DebugUIBuilder_AddRect_m877BEB5A300FE474E3A148ABB8BA0C0FBB224EA9,
	DebugUIBuilder_AddButton_m76A914A3B5BA3E19134F8E109BA557AA842E2014,
	DebugUIBuilder_AddLabel_m6A98E248939BD65B054A4B4C4420D950F4811CAD,
	DebugUIBuilder_AddSlider_mC4E1DB56081E390B06F2B7D02C67BA683D84686F,
	DebugUIBuilder_AddDivider_m858451240D2EAE0D367F8145E2AC917929B15DB2,
	DebugUIBuilder_AddToggle_m3A79A54CC94114F24B771CD465F40B001CBFCAFD,
	DebugUIBuilder_AddToggle_mAA47BCD38AB222645D00709AFB422F4D5CF02DB8,
	DebugUIBuilder_AddRadio_m783EACB19FD91F15BD9BE7EDABCDD6B05D639639,
	DebugUIBuilder_AddTextField_m726C38770CC36156FC5B71739F63536EA55540A1,
	DebugUIBuilder_ToggleLaserPointer_m19D2F9E314DF85DEE576A53C5E719685B14A2B55,
	DebugUIBuilder__ctor_m95BD850859B88DCA361D481FC4A0C998DB46496E,
	OnClick__ctor_mC5739B4F2FE456031D0F032D2B1335A8C7E07F76,
	OnClick_Invoke_mDC64AF5D49F6C27A1C15E0695D0B9B8A51958AC9,
	OnClick_BeginInvoke_m26A6065C75CFBA6B575C90229E2689F93697C002,
	OnClick_EndInvoke_m9DC1A8BFEE9B5B1036401A570221ECB4C32AD697,
	OnToggleValueChange__ctor_mDED41EA7D290F566BBDE14F48F7DAB95A65513A3,
	OnToggleValueChange_Invoke_m7C6B23451E232EEEB6C2F95BAD41C5E5716303CE,
	OnToggleValueChange_BeginInvoke_m33F94F819A1EB1D23BC216963765EDB1740702C8,
	OnToggleValueChange_EndInvoke_mA11DE32406ADE7239AD2833535BF1C49DB4E292B,
	OnSlider__ctor_m748CDFE6C061BCC4EAB8C70EDEF6413DBC5748C8,
	OnSlider_Invoke_mC63EB553A881C3BD597451B10831BAD4EC03F5C9,
	OnSlider_BeginInvoke_mFA997752B26047A4D133433B32C14383D0E1F7DB,
	OnSlider_EndInvoke_mCC34E0F20749EC0287A3FDC4FDC354A88202FEB2,
	ActiveUpdate__ctor_mC45D2B4C2665FD2DC12090EAB9C0B5044514DF19,
	ActiveUpdate_Invoke_m769E06EC3AB44D98C5C452D5B2336809F87C1E18,
	ActiveUpdate_BeginInvoke_m720D31DDBFECDC31563B554DE0450793E969F877,
	ActiveUpdate_EndInvoke_mFA8C1E60854662BE8B396F7BC93367E235ACB707,
	U3CU3Ec__DisplayClass41_0__ctor_m6527E632BD200CEC2C12AA6E5775DD1A9B9385A2,
	U3CU3Ec__DisplayClass41_0_U3CAddButtonU3Eb__0_m75C21986A5DBE0F1E571B7F9AFF69149AB1AF492,
	U3CU3Ec__DisplayClass43_0__ctor_m84C5DC7D04C357D12494C7BF4F2A5871F18BB3A5,
	U3CU3Ec__DisplayClass43_0_U3CAddSliderU3Eb__0_m8D2A0F94778703422A6F82B8A92A7AE0E9E56D28,
	U3CU3Ec__DisplayClass45_0__ctor_m57D3B15225A9BC9E3DEE6B197DFC64DF0A3634E4,
	U3CU3Ec__DisplayClass45_0_U3CAddToggleU3Eb__0_m0B89042D3D5BFDF300DB436C5C1B9F2C8B3D4C2F,
	U3CU3Ec__DisplayClass46_0__ctor_m5B53CA1415680044F78C8FC6CA4D4B242D012563,
	U3CU3Ec__DisplayClass46_0_U3CAddToggleU3Eb__0_mC2735E33A83EA7341D913BD4001D5D977C935174,
	U3CU3Ec__DisplayClass47_0__ctor_m2DBE10617ACA2335CE8026196DE0F91419618249,
	U3CU3Ec__DisplayClass47_0_U3CAddRadioU3Eb__0_mDD9D163D69AF5B03BEF971FE625C79630C300C89,
	HandedInputSelector_Start_m9F0E0845F78B86C943B82C6C00D00C2755379664,
	HandedInputSelector_Update_m4AD93E3F8125E142011798D2CABF248A3DFB73C6,
	HandedInputSelector_SetActiveController_m675CE04EEBD60BA97F10205CFCC8E0360A57640D,
	HandedInputSelector__ctor_mFB9B45C64E23FAA0F3AC5FC734673E1A4EC7B6FD,
	LaserPointer_set_laserBeamBehavior_mAEDF3DABF1D4C13A02570DFB7539EA32D96C3EB7,
	LaserPointer_get_laserBeamBehavior_mC452C41CF65C2D1E3D13990D519BCE2B5459F4CE,
	LaserPointer_Awake_mB3F93300A58061D247AAD53BA961CA67148670CA,
	LaserPointer_Start_mC859C4A4E40BCB5E1A39D8B6135D256178A1B0C1,
	LaserPointer_SetCursorStartDest_m58BE76CF5DC6A138D6F39812B510E58A8195CA18,
	LaserPointer_SetCursorRay_m73C5954CB43A8D590BF1FF9674846886C869E3CA,
	LaserPointer_LateUpdate_m922E6CC571CF3BF292D1E4A89ABA3DE721940E40,
	LaserPointer_UpdateLaserBeam_m2EC759248E0200A9EDC06D995886AED2C3BBE84D,
	LaserPointer_OnDisable_m386D5CB4ED7CC015BDD625F9BB173D48A44963BC,
	LaserPointer_OnInputFocusLost_m03F3165CDC0823985D85DBDECE16679C4A574EC1,
	LaserPointer_OnInputFocusAcquired_m253C322D76D9D1A00FD1041F30DE52EC189857D8,
	LaserPointer_OnDestroy_mA7F7B1BDDB6897671BC633CF81F15F3C2AC8A27E,
	LaserPointer__ctor_m9834E61B31A7B83B51FC2BEB7B9F59FCE781B66A,
	CharacterCameraConstraint__ctor_mB4775E6A3FD15170E7EDA1FF7083E59DC9D9356B,
	CharacterCameraConstraint_Awake_m11B961F3351B1DCF59D2F9F25F6129261B996D8C,
	CharacterCameraConstraint_OnEnable_mA51E71B397BB043BDCDBD57881301ACEDBB7A937,
	CharacterCameraConstraint_OnDisable_m0C85B3913ABB8DD8EF6AB5A38003925365C97700,
	CharacterCameraConstraint_CameraUpdate_m7D846796F0337138A375E39F69E31C68B0EBA41B,
	CharacterCameraConstraint_CheckCameraOverlapped_m7EF5BBE41ADD0F7610CE1CC6EE39CA631513C783,
	CharacterCameraConstraint_CheckCameraNearClipping_m3BD4B953D24FBF24CAB9E86824E600EAFE549483,
	LocomotionController_Start_mFF771607088B6AC1B9647FFD5262BD156C32990B,
	LocomotionController__ctor_m5AA73F4FF00769A834D5732FC4BD08BDD4634BDF,
	LocomotionTeleport_EnableMovement_m8ADC24447284A9EE7AEECCED581CC2B51326FDC9,
	LocomotionTeleport_EnableRotation_m1B04E1D2444E02AC4407BEE5362EF08441C2AFA5,
	LocomotionTeleport_get_CurrentState_mCEDC22238BFC0EB60D9DE2616698C71D0CEF0BA8,
	LocomotionTeleport_set_CurrentState_m6D752383FDB712A2347A0CAC2F7734E26274DA77,
	LocomotionTeleport_add_UpdateTeleportDestination_m5CF34E190EF7950C68B07AA59440C8904B000EDA,
	LocomotionTeleport_remove_UpdateTeleportDestination_m6D4B9E576EAFC2F0F8812B24237AE65840368C8D,
	LocomotionTeleport_OnUpdateTeleportDestination_m14192CB82A0C6B54908D63B87428912B984459D1,
	LocomotionTeleport_get_DestinationRotation_m713A8A847E529FFF3636C695730856FF0F591CC6,
	LocomotionTeleport_get_LocomotionController_mED79293C6EA765335D25527FC7111EF971257498,
	LocomotionTeleport_set_LocomotionController_m460991F216978FB103CB533BF9DEF4BC93DA814B,
	LocomotionTeleport_AimCollisionTest_m230A548C1CE75228EFA5CA239FA7171FB0FB4D3E,
	LocomotionTeleport_LogState_m2272C15F12A05D8C031A50FB08AE122C1FCA2EA7,
	LocomotionTeleport_CreateNewTeleportDestination_m985E3B33C931F465655EF6156CC75DC3B7A1D99D,
	LocomotionTeleport_DeactivateDestination_m4F8704E73CA946449DF98D49763F10EF733987A0,
	LocomotionTeleport_RecycleTeleportDestination_m6DA8327336475283A325074C859CD4D281BA9879,
	LocomotionTeleport_EnableMotion_m9ACD9EF8F110F90CD996939A058242FF89BCEA22,
	LocomotionTeleport_Awake_m9C80807870EFB91801F476BAE526C31D9E29E66E,
	LocomotionTeleport_OnEnable_mFCFB80918BF2D1AF964CB043299BBE69CC83FDB5,
	LocomotionTeleport_OnDisable_mA887B447817410EA3AC80DF9C6B2390D0B26788D,
	LocomotionTeleport_add_EnterStateReady_m694F17237FD2AEEDCADA5B5BF9D32F91E249BD39,
	LocomotionTeleport_remove_EnterStateReady_m02A5BF8752C71DC2A54CFA7325F737BC3B6099C1,
	LocomotionTeleport_ReadyStateCoroutine_mEF056F1254CED1ABD142E517BDEB2815AAE24A0C,
	LocomotionTeleport_add_EnterStateAim_m9EEEAB1100A4CC7635EF023A0078BCE95434C5A2,
	LocomotionTeleport_remove_EnterStateAim_mBBF9AA5970D87E22D59FC766F8278643C61B6250,
	LocomotionTeleport_add_UpdateAimData_mF17892665B70341AB40666AA39187787B9E060E5,
	LocomotionTeleport_remove_UpdateAimData_mBFAF399B9B0200FDC43ADEF36B4123F60941D24C,
	LocomotionTeleport_OnUpdateAimData_m76ED9D517FF45B8A7CE7757BD0E8D147892F90A8,
	LocomotionTeleport_add_ExitStateAim_m93E127BBB502440C22348EDF011EF6ED12E1E607,
	LocomotionTeleport_remove_ExitStateAim_m86CEE4BAF96D0CDAB9CEF69231DDB1A3FCE7964F,
	LocomotionTeleport_AimStateCoroutine_mC98C40FA612A3DC181B2A622B2AFC2B78C72D182,
	LocomotionTeleport_add_EnterStateCancelAim_m98AE021C2A1274CE289ADBDC891DCC027AB06668,
	LocomotionTeleport_remove_EnterStateCancelAim_mF547CBA5F10D4A7374318065008202266E14E729,
	LocomotionTeleport_CancelAimStateCoroutine_m89E30D85AE2200D46C6B6CEC487CA86309336DFE,
	LocomotionTeleport_add_EnterStatePreTeleport_m5436C07729E835E670F3D438A7EAC8743646071F,
	LocomotionTeleport_remove_EnterStatePreTeleport_m1BB7C07D45DA26BA3FC294749B845A6F04E9035D,
	LocomotionTeleport_PreTeleportStateCoroutine_m4D4467A0B6570EEE09C5366CF0B443665718D27C,
	LocomotionTeleport_add_EnterStateCancelTeleport_mE447D28CF848635B7BF086DA4CC7592E174225F5,
	LocomotionTeleport_remove_EnterStateCancelTeleport_mD9C6A70C33006402CC14DF75C06AA68EE0CB38B7,
	LocomotionTeleport_CancelTeleportStateCoroutine_m376EE91D4337D6E0DFE214FAF8277E4AD3A80F09,
	LocomotionTeleport_add_EnterStateTeleporting_m2939235BCF9A6D936A57686136AAF38FA38F7AE9,
	LocomotionTeleport_remove_EnterStateTeleporting_mE99129EE268F102CFBF05222E234285FD41EA602,
	LocomotionTeleport_TeleportingStateCoroutine_m29325603C91C678B7035FDA9AA324D657149A1C7,
	LocomotionTeleport_add_EnterStatePostTeleport_m6352237DD5B10EB9C90693107DD8C33A6CE538FA,
	LocomotionTeleport_remove_EnterStatePostTeleport_m9E53B5CBF3804AC61F9E38AEA246E2599341373D,
	LocomotionTeleport_PostTeleportStateCoroutine_m55EFBB7452FA0FEF1F0C025CE559516ED977AA96,
	LocomotionTeleport_add_Teleported_m8CAC3A947E40F5C79B38D5F059E94B75A6CE623A,
	LocomotionTeleport_remove_Teleported_m27E84C43ACE915F7D5ADA07A974F7A76530BBA3E,
	LocomotionTeleport_DoTeleport_m1477EB38D2FD6E2AD4A7553EB3237B18D40C0B0C,
	LocomotionTeleport_GetCharacterPosition_m30F7EB551F5787D2489CE2716284B848D2C11D26,
	LocomotionTeleport_GetHeadRotationY_m0F2078ED6649DCED3A8022BA2CD27BF02378639B,
	LocomotionTeleport_DoWarp_m2195B0DF360E9B04AD486855F66560DFC22EEB76,
	LocomotionTeleport__ctor_m9AFDEE21E452CC311C904A90D753CB815495A59C,
	AimData__ctor_mDEB139E72E07987C9EC05AFFACE3A5C393EE56B8,
	AimData_get_Points_m3169977C66C0D412F32CECB2B7FD471F65A191B2,
	AimData_set_Points_mDECAE723C539EE8F8513307662F0CCF743ED7BF4,
	AimData_Reset_mA22ED9AA08B2642374D760A48D198B98DA3D49E7,
	U3CReadyStateCoroutineU3Ed__52__ctor_m8DE1982B50214C0C02CF0920B2BD0C8503DD7BC6,
	U3CReadyStateCoroutineU3Ed__52_System_IDisposable_Dispose_m281149F5ECF711FBFD9262A09DA4339B2DB2F7FC,
	U3CReadyStateCoroutineU3Ed__52_MoveNext_mA3BE219EA412B9FF5022A2D51A8C98962618203D,
	U3CReadyStateCoroutineU3Ed__52_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF049A6B9DF16EBDE92993801D9D70B3DF6724BB3,
	U3CReadyStateCoroutineU3Ed__52_System_Collections_IEnumerator_Reset_m57879FEF2CA0007DFA14EE7C948F128236C49F20,
	U3CReadyStateCoroutineU3Ed__52_System_Collections_IEnumerator_get_Current_m8CFB46F37C1A1C464F6C813BD5B61E5E4D60D65B,
	U3CAimStateCoroutineU3Ed__64__ctor_m58306B48D136424B1A697461F28CCD0112C0C386,
	U3CAimStateCoroutineU3Ed__64_System_IDisposable_Dispose_m1F7871364B779E25C381A3A485C5E8360E58BA33,
	U3CAimStateCoroutineU3Ed__64_MoveNext_mF0CCCD83D0E4DDD4864A079C21C7423C75D51811,
	U3CAimStateCoroutineU3Ed__64_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCEA3E5325E391A08ECF74573A3F1DA3F202A609F,
	U3CAimStateCoroutineU3Ed__64_System_Collections_IEnumerator_Reset_m430B643E53905F6ECA3AFC5D72DF1F1C7431E345,
	U3CAimStateCoroutineU3Ed__64_System_Collections_IEnumerator_get_Current_m800D35A57B1A8C7F13AB42BA27EC5C8C763D7EA8,
	U3CCancelAimStateCoroutineU3Ed__68__ctor_m1DDF58501076B8077E3441AF06D8A6A7F13B6747,
	U3CCancelAimStateCoroutineU3Ed__68_System_IDisposable_Dispose_mD7599C67A3F973C9B291989150D3639890B5BD84,
	U3CCancelAimStateCoroutineU3Ed__68_MoveNext_m0387258B4B35F7D9F037F52A915D247B2EBDF011,
	U3CCancelAimStateCoroutineU3Ed__68_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m422572D5779C1216188EDB8521144B4BD48335B0,
	U3CCancelAimStateCoroutineU3Ed__68_System_Collections_IEnumerator_Reset_m537442ED372D14EA04A5967D6B7078DC83C32107,
	U3CCancelAimStateCoroutineU3Ed__68_System_Collections_IEnumerator_get_Current_mCA030531FABF8A358DCC082E6F56B591B13DC194,
	U3CPreTeleportStateCoroutineU3Ed__72__ctor_m9FCD57D81CBFA88D2D1B4A777B29A236034CAF8E,
	U3CPreTeleportStateCoroutineU3Ed__72_System_IDisposable_Dispose_mBD12FF7C2306AAEF739BCCFF39B2C1F2C42D6ECD,
	U3CPreTeleportStateCoroutineU3Ed__72_MoveNext_mAB1EF7B08ADA03C55A05F1ADC5EF0628EAB6858B,
	U3CPreTeleportStateCoroutineU3Ed__72_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m842BD3A40FAB08CE9ED7384F00A4035A6A8A6006,
	U3CPreTeleportStateCoroutineU3Ed__72_System_Collections_IEnumerator_Reset_m8AA3FEA6ABDAC9A68A96DD7A90B1190B504ED09A,
	U3CPreTeleportStateCoroutineU3Ed__72_System_Collections_IEnumerator_get_Current_m1FC972F30DE3D7BAB7166BB72A107D0FD9E1E49E,
	U3CCancelTeleportStateCoroutineU3Ed__76__ctor_m6BBBA231C275F8B93FEFD97A2279B18FED30AFFF,
	U3CCancelTeleportStateCoroutineU3Ed__76_System_IDisposable_Dispose_mBB05318F123DC133C386E503BD026985038DF644,
	U3CCancelTeleportStateCoroutineU3Ed__76_MoveNext_m02B6031D1BB5B1B0662A91F6E9B78231CDC0CBAE,
	U3CCancelTeleportStateCoroutineU3Ed__76_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9B45046196D18E8F11B7FFCA3836CDD0E069BFB6,
	U3CCancelTeleportStateCoroutineU3Ed__76_System_Collections_IEnumerator_Reset_m2FE09112ABFFCA00C122EFD44AE58DA551BD9817,
	U3CCancelTeleportStateCoroutineU3Ed__76_System_Collections_IEnumerator_get_Current_m888B89F4C478800F9FACD7F1EC137820F5094C57,
	U3CTeleportingStateCoroutineU3Ed__80__ctor_m1A3D09A30548B419B3E2083990FF4DA65B66A1C2,
	U3CTeleportingStateCoroutineU3Ed__80_System_IDisposable_Dispose_m52588F27903FD706F72A84E217008CDCEA1CE19C,
	U3CTeleportingStateCoroutineU3Ed__80_MoveNext_m01613F55E14EE215BD297AA0E61DFD1B400B786E,
	U3CTeleportingStateCoroutineU3Ed__80_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m59D9DB94A89EE8EC99B646C00995380363E17BA6,
	U3CTeleportingStateCoroutineU3Ed__80_System_Collections_IEnumerator_Reset_m87DE12198C0424F46FA9337E312A56AC85B48F34,
	U3CTeleportingStateCoroutineU3Ed__80_System_Collections_IEnumerator_get_Current_mD05407DB2BA285B804EC4E4DF432340D1424DF0D,
	U3CPostTeleportStateCoroutineU3Ed__84__ctor_m73CB3DEE73F665F765BA80FDF21B564AE8F22CAB,
	U3CPostTeleportStateCoroutineU3Ed__84_System_IDisposable_Dispose_mDDC6BB7ECF31923D9B8AA237DFDC162642BE3F5E,
	U3CPostTeleportStateCoroutineU3Ed__84_MoveNext_m5428FACDB4F6A7C6B9C2966106C9CA97A70CAFEC,
	U3CPostTeleportStateCoroutineU3Ed__84_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m50185F8995916556B5531091EAC1D796207EE4D6,
	U3CPostTeleportStateCoroutineU3Ed__84_System_Collections_IEnumerator_Reset_m8E16BCCA8A3B018A2CAC418CDAA14DB7C9B3D897,
	U3CPostTeleportStateCoroutineU3Ed__84_System_Collections_IEnumerator_get_Current_mA97CEA8F012879C52B5756B734B3AA464439F83F,
	SimpleCapsuleWithStickMovement_add_CameraUpdated_m62D68F4E8BD481D3679FDB0979AE6CAC8DAFAC46,
	SimpleCapsuleWithStickMovement_remove_CameraUpdated_mA39A1F5D5F0CA44D0B6EB9E9208FD39B7D0D3E06,
	SimpleCapsuleWithStickMovement_add_PreCharacterMove_m682E5AAFBCDF0BF832F5447D69CC73DB39C096D2,
	SimpleCapsuleWithStickMovement_remove_PreCharacterMove_m57CBA41BD4CD9C1EA3864D4AD464C1255EA80B22,
	SimpleCapsuleWithStickMovement_Awake_m0C068EE45D2BEDB94F8D1D289C04BF7480443D2E,
	SimpleCapsuleWithStickMovement_Start_m517C18566428FBB777185B97264539BE48D5A0F5,
	SimpleCapsuleWithStickMovement_FixedUpdate_mE88BF998394A1EB4FCEBC8A04875FA26D3D6C1BD,
	SimpleCapsuleWithStickMovement_RotatePlayerToHMD_m762E683133240C6E945140A357920BC6ACB7D116,
	SimpleCapsuleWithStickMovement_StickMovement_m4550B6DF9A0272F282866822CC8F0E8AE32E1893,
	SimpleCapsuleWithStickMovement_SnapTurn_m6BA0E49AA935C048AC9E8D59C90CBF50E087B396,
	SimpleCapsuleWithStickMovement__ctor_m96AC7462B0905E8D35D74FCF0C54E3F96DDB68B9,
	TeleportAimHandler_OnEnable_mACD1F9393ADE87B704B4A4EAA298FBA36EDAC843,
	TeleportAimHandler_OnDisable_m61175BD9492F8C39A0BC8573FCAB06D008DF3961,
	NULL,
	TeleportAimHandler__ctor_m1763BE837F8F8FA3B3C34EBA78BE0D593F50A5E7,
	TeleportAimHandlerLaser_GetPoints_m5DE7FA7926409D22B8F5E329EC2A4935B95EC3A0,
	TeleportAimHandlerLaser__ctor_m0D6E559AC138E39BBBF5E12AD223F13571A32D9C,
	TeleportAimHandlerParabolic_GetPoints_m2AF261807D5A7F6FE7E14F20B473D985A93DD1C3,
	TeleportAimHandlerParabolic__ctor_mAB05081CBCE394C762E8B8F4F6CA12D3E9846A32,
	TeleportAimVisualLaser__ctor_mC232B12213106D0E44DD02D7EE16080182C5FCAC,
	TeleportAimVisualLaser_EnterAimState_mFBE7E0BB8190B822638EB470B2559E9A52B29832,
	TeleportAimVisualLaser_ExitAimState_m7076D541D8CFF0F24A2A19CBFE8C0EED448773D9,
	TeleportAimVisualLaser_Awake_mFC14E2C04A8ACFACA52131512BAE0FCDB425754A,
	TeleportAimVisualLaser_AddEventHandlers_m5E7A188CF65F15DEEE335F3B40C261EF1D1E0D10,
	TeleportAimVisualLaser_RemoveEventHandlers_mAEC2273E026F953D7B71945590F0E7ED65539415,
	TeleportAimVisualLaser_UpdateAimData_m8060F21ACBE191832A7247F4D750DF9A5A7C9984,
	TeleportDestination_get_IsValidDestination_m4A83CF1ABD625233373782FABAC9AC210A5FD151,
	TeleportDestination_set_IsValidDestination_m2A37F62F1E80ECB2FDE63D42471514A26B0ED0BA,
	TeleportDestination__ctor_m01B9174443C128BB3639D39F0D2EBD7283DEB972,
	TeleportDestination_OnEnable_mFB65AD58BD57263B4D14E91B6FA583DA9607EB91,
	TeleportDestination_TryDisableEventHandlers_m450B9F64A3EAB6059B85B7573377A574606A9062,
	TeleportDestination_OnDisable_m0D75681D1CD83894438624CD3A7878C95904F63F,
	TeleportDestination_add_Deactivated_m63745B4674198FDFA8D3CB45587CA14132840F9A,
	TeleportDestination_remove_Deactivated_mE2D6A7DCD9734262F34A8935272156A4B029A83A,
	TeleportDestination_OnDeactivated_mA84FB07520E0C6EDA28F13A6349B994236BF8B46,
	TeleportDestination_Recycle_m49B5F4B6776D55D76519AFA221B2881A51FED91E,
	TeleportDestination_UpdateTeleportDestination_m3FF1158B3A2BB73D914DC806E094D20F93A674B3,
	TeleportInputHandler__ctor_m54EE60B8D0F7935760E81AB820D4570094939F71,
	TeleportInputHandler_AddEventHandlers_m4609349778D031303E2767296E2B7E48E5E20FD2,
	TeleportInputHandler_RemoveEventHandlers_m739F90CC264BE61AC9316C2B1645BFE8BCBA0555,
	TeleportInputHandler_TeleportReadyCoroutine_m579E5F381DEA656B2A90444ADFA6DB5CB05BCD50,
	TeleportInputHandler_TeleportAimCoroutine_m82502080BCE2BD26E5BEBD89D54C3E57A4780B07,
	NULL,
	NULL,
	TeleportInputHandler_U3C_ctorU3Eb__2_0_m919CD5070F20E3A9E28CB8A95C9C9374663C52A1,
	TeleportInputHandler_U3C_ctorU3Eb__2_1_m2CE609639EC53A6A19E043A2E053F30FAE77D486,
	U3CTeleportReadyCoroutineU3Ed__5__ctor_m7C396BD2404ABCBF1CF83320ACF13FA1B621B4FB,
	U3CTeleportReadyCoroutineU3Ed__5_System_IDisposable_Dispose_mA2DFF1E0C765EA7B2F4BA2E1EF7721049E8EA046,
	U3CTeleportReadyCoroutineU3Ed__5_MoveNext_m5A64679B873617863C4B38AA8008C8CC1E564029,
	U3CTeleportReadyCoroutineU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m41A381D2B7B87AD5810E05A3C55A66FD7D8CADA2,
	U3CTeleportReadyCoroutineU3Ed__5_System_Collections_IEnumerator_Reset_m12F830ADD072802D84BE4B3231F43A12FA4F8E73,
	U3CTeleportReadyCoroutineU3Ed__5_System_Collections_IEnumerator_get_Current_mF8243C3A677F944C06A59D21D17CC3AF8DD1B594,
	U3CTeleportAimCoroutineU3Ed__6__ctor_mE2F9BDC7C3225737B5ADD2AD7AAF0E449CFADE47,
	U3CTeleportAimCoroutineU3Ed__6_System_IDisposable_Dispose_m443089F5C3CB24CF2E58C43B6D940BB6A97E7059,
	U3CTeleportAimCoroutineU3Ed__6_MoveNext_m3D532A7ED8718469618989D091672C1E1E833FA0,
	U3CTeleportAimCoroutineU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m02583125A115ADE4CC8896C0108B7A2ADD9FCAFE,
	U3CTeleportAimCoroutineU3Ed__6_System_Collections_IEnumerator_Reset_mE894BDC51B100595A10489704983F8922185CCF8,
	U3CTeleportAimCoroutineU3Ed__6_System_Collections_IEnumerator_get_Current_m28CB29942297ECF9C327DDDAB3D53AF8A77ABF05,
	TeleportInputHandlerHMD_get_Pointer_mCBC08A1A1B5D076AF31755E7771F3000034939FF,
	TeleportInputHandlerHMD_set_Pointer_m5F8552C6C93A90A8BA5E3473532D687559EB2E9D,
	TeleportInputHandlerHMD_GetIntention_m474B95CF0690765C9B5C7DCC8381385E9BF5FC6A,
	TeleportInputHandlerHMD_GetAimData_mBE4EAEA4E4AD483994731D9FE82D0491E131AA37,
	TeleportInputHandlerHMD__ctor_m3E2641D8040D2F44A53E2A184ECDDCA1ED6153C9,
	TeleportInputHandlerTouch_Start_m14F2576DDA7F7AA52A05AD9456703D1FF68F2387,
	TeleportInputHandlerTouch_GetIntention_m23F1835AE8064C76A1C2F9631B343C721DB6A993,
	TeleportInputHandlerTouch_GetAimData_m4155B513561DCB66F1180407A5AB8B2B5936E91B,
	TeleportInputHandlerTouch__ctor_mE4E2BF438B9790F07ECA23A6D77748104C707E31,
	TeleportOrientationHandler__ctor_m21A7BA2A330541DFEB4ADF80A53BE1046E65C10A,
	TeleportOrientationHandler_UpdateAimData_m9B52AC667A8BF78D509B49403962756A1714DD9B,
	TeleportOrientationHandler_AddEventHandlers_m2507C63FDF190C3BC81D2479EA8D488F32C6D4C6,
	TeleportOrientationHandler_RemoveEventHandlers_mB46ADA4F854BD6919ED74D88F027AB423FF522BF,
	TeleportOrientationHandler_UpdateOrientationCoroutine_m0CFA18DF96F986F14A52E6CC7EEFC90FA0C73B7B,
	NULL,
	NULL,
	TeleportOrientationHandler_GetLandingOrientation_m4CC4D8950AFCDAA13EB2244AD987B3225FEFAABA,
	TeleportOrientationHandler_U3C_ctorU3Eb__3_0_m3CA25FA37637FD4D799437E6037E2C278EC4B9A0,
	U3CUpdateOrientationCoroutineU3Ed__7__ctor_m5DFE7ADCE7BF42B372F2097C1C019F301BBE3DFE,
	U3CUpdateOrientationCoroutineU3Ed__7_System_IDisposable_Dispose_m159520706F62855F2F55F4686A47A89D093AD2B0,
	U3CUpdateOrientationCoroutineU3Ed__7_MoveNext_m2D766D50632FD169A27EF1E4EE39B50AAEF4512C,
	U3CUpdateOrientationCoroutineU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m87C4EB920BA4BF331493243117342D5E99905F1C,
	U3CUpdateOrientationCoroutineU3Ed__7_System_Collections_IEnumerator_Reset_m8957A7F5F8D1D9C630D84F24A7C2B66642CA9B95,
	U3CUpdateOrientationCoroutineU3Ed__7_System_Collections_IEnumerator_get_Current_mDB4E059717A313A10584DB05F88E813A45D9A606,
	TeleportOrientationHandler360_InitializeTeleportDestination_m6372AC3D398F0ABB35CDE87532ED8ACEE579351A,
	TeleportOrientationHandler360_UpdateTeleportDestination_mF98B570F0875BCE3DEF88E3EA2BDCF415C759E71,
	TeleportOrientationHandler360__ctor_m6FDD3AEEEB9FB23CEC3C6D7B6A453FF38428B49A,
	TeleportOrientationHandlerHMD_InitializeTeleportDestination_m507B4108A8B145144CC59BFB969FB37F3EDB88E9,
	TeleportOrientationHandlerHMD_UpdateTeleportDestination_mA3F3F034F1C248EBE74640A15F13D287D1C2AE2E,
	TeleportOrientationHandlerHMD__ctor_mED7328A59A1C729AC845F7A8410DD4BD5C633DE2,
	TeleportOrientationHandlerThumbstick_InitializeTeleportDestination_m4ABFA62C2B8CB74DE7829D0005E6C98FE32EB8F2,
	TeleportOrientationHandlerThumbstick_UpdateTeleportDestination_mBE2E2B2A0BCED3855974AAC1AAFBEE6F6081587B,
	TeleportOrientationHandlerThumbstick__ctor_mEAA7AC3ABA1CE6E0253CF38FB2CA3E4C318F59BF,
	TeleportPoint_Start_m30ECA69CD4353B458F2295BAEBB450CC01F59805,
	TeleportPoint_GetDestTransform_m721C2E343BE9B54FABF84CA15E207FD2390F7DBD,
	TeleportPoint_Update_mAF9BC964BBC534053D53659EF53DA1A09B22F484,
	TeleportPoint_OnLookAt_m2564753261F0D09670931BBF308955CB21B8302A,
	TeleportPoint__ctor_mA783018C82A82D1A80BA40DF577BC9D3BC245A6A,
	TeleportSupport_get_LocomotionTeleport_mC25D8C8701C7D25C8FBA30EB3C92527B8184A714,
	TeleportSupport_set_LocomotionTeleport_m718C84A5C4E1D039C61850BA424EFA71B5FF9327,
	TeleportSupport_OnEnable_mF890133242B29B518FDCBD5FEAFAF29298CBA609,
	TeleportSupport_OnDisable_m9C71979791E9FCD13837605745FA3C6CFF0B1409,
	TeleportSupport_LogEventHandler_m65943E6F024E9B73CE5D5ED5099B2A0164B3E2C5,
	TeleportSupport_AddEventHandlers_mE4502DF40E56A3B5061FB931FC4F21678BAE3E84,
	TeleportSupport_RemoveEventHandlers_mE7C77E2C141F41B7799C511FC1B8DEB699B6E8E1,
	TeleportSupport__ctor_m05529A4609A1DFC91979CE4AE1790639B372E754,
	TeleportTargetHandler__ctor_m3AE469213D738BE0ABD908BB5AA27AB6FF60240E,
	TeleportTargetHandler_AddEventHandlers_mFFEC0649545A9B1614D7114872C7AECEED6AE715,
	TeleportTargetHandler_RemoveEventHandlers_m3DC103164A1F0089BBC66E83C9E754B7F9C79849,
	TeleportTargetHandler_TargetAimCoroutine_m1CEA9B7FA57455F77D9639E1B62C205C515E959D,
	TeleportTargetHandler_ResetAimData_mA32665779CE5C63B71357211C714615BF9CD4DA2,
	NULL,
	TeleportTargetHandler_ConsiderDestination_m1A96940FCF8FD62A59948C3080FF813ABA4F8331,
	TeleportTargetHandler_U3C_ctorU3Eb__3_0_m6384FA377850643BB1088262AAFC6FFA8748F8F9,
	U3CTargetAimCoroutineU3Ed__7__ctor_m23D9D0D8A352A65E0D99985A676E454304BF8FE4,
	U3CTargetAimCoroutineU3Ed__7_System_IDisposable_Dispose_m21B111D2C0E7C1EB3CE98929690C0DD190716414,
	U3CTargetAimCoroutineU3Ed__7_MoveNext_m67B9E5EA47ACD29BD93DB469A85D596C9D94F69A,
	U3CTargetAimCoroutineU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m73510D945678774D27D2FEE03C45CF482A2EBBE6,
	U3CTargetAimCoroutineU3Ed__7_System_Collections_IEnumerator_Reset_m2BC8EDEB99F6EEEA6DB6EE8F703C99E5CC6A7301,
	U3CTargetAimCoroutineU3Ed__7_System_Collections_IEnumerator_get_Current_mF0915E668ADBDEC27BF20ED85385A44114046F62,
	TeleportTargetHandlerNavMesh_Awake_m12528F076C3DFA64B7F8C2738B086CCBA390746A,
	TeleportTargetHandlerNavMesh_ConsiderTeleport_m437418CEDAA72639344228B31EA8BDC8A52AD169,
	TeleportTargetHandlerNavMesh_ConsiderDestination_m0095B7CAB6515175C4652DAFB166FB7851298DD8,
	TeleportTargetHandlerNavMesh_OnDrawGizmos_mBBC3AFFADE1EA512DEF62DE6FB277B82EE99E1DC,
	TeleportTargetHandlerNavMesh__ctor_m5BB4F183D1C263E465000903520478FBB0D2E990,
	TeleportTargetHandlerNode_ConsiderTeleport_m144CD7243791916F4D785F4E01AAAD729C1F1150,
	TeleportTargetHandlerNode__ctor_m66245B4B90DFAE3D51056B3DC1B825CF128E4145,
	TeleportTargetHandlerPhysical_ConsiderTeleport_m3F6B5CBCEFE9B53FB42E5A853B728740B30A879F,
	TeleportTargetHandlerPhysical__ctor_mED10B7430B7A3A1104C1EDFF1A2F7B4B4B007B4D,
	TeleportTransition_AddEventHandlers_m7945828C3E62F3C4EF094AC2B12950618720397F,
	TeleportTransition_RemoveEventHandlers_mBD20E23B4E42E8DE6D30126DA98CF0B744864546,
	NULL,
	TeleportTransition__ctor_m1A4530AB5DEEA55E946DF9A491B5145EFE239ACF,
	TeleportTransitionBlink_LocomotionTeleportOnEnterStateTeleporting_mCB87A486BE2733C4BCD2B57AEA4FAA69FED1D74D,
	TeleportTransitionBlink_BlinkCoroutine_m502BAE8615CE4CC76BFD91E15746263D10CF2E77,
	TeleportTransitionBlink__ctor_m709CADAB68439933BA41B5C318E4E60616F09932,
	U3CBlinkCoroutineU3Ed__4__ctor_m73115DAF8E2D89A49A9F6BD9AB03485AA7551BCD,
	U3CBlinkCoroutineU3Ed__4_System_IDisposable_Dispose_m6A66CCE325E70F5794414025EA6A7B8EABFAE259,
	U3CBlinkCoroutineU3Ed__4_MoveNext_mBEC5B507954BA0961920B4141D150885CE4D309B,
	U3CBlinkCoroutineU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFB31D456C6490D00742481C0F18817914FC76018,
	U3CBlinkCoroutineU3Ed__4_System_Collections_IEnumerator_Reset_m6E21661229B022076FA60AF6C49A1980E56609E1,
	U3CBlinkCoroutineU3Ed__4_System_Collections_IEnumerator_get_Current_mAA053761AFFEF4D53F8AC25C2BDA6DC4729996DF,
	TeleportTransitionInstant_LocomotionTeleportOnEnterStateTeleporting_m09B573BE4E14DD8DD571233C7F3CCA384A775A4B,
	TeleportTransitionInstant__ctor_m2596160D526DF43F58D24D83F33B43AEB3774DA4,
	TeleportTransitionWarp_LocomotionTeleportOnEnterStateTeleporting_m2D397B890161CCF004AF8C0AC064F28EAD6EFB8C,
	TeleportTransitionWarp_DoWarp_m12AF9CC51FC2784F1FCBCD37A9D0CBBDE2EEF117,
	TeleportTransitionWarp__ctor_mA2A7FA91C271FEC14A97EF24341CC25EF5C5639B,
	U3CDoWarpU3Ed__3__ctor_mA023019BAD546DB8BFCFC8326BF22566DC855B45,
	U3CDoWarpU3Ed__3_System_IDisposable_Dispose_mE361978D56E79BC8335160F0DA180CB96DCE29F9,
	U3CDoWarpU3Ed__3_MoveNext_m84CA338E50BBE257229411F56CD6B4D7F46015B1,
	U3CDoWarpU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m94EA3502B1F9442D23A5EBF53483F6C85D484208,
	U3CDoWarpU3Ed__3_System_Collections_IEnumerator_Reset_mE692557C58D90117915B599FA91A88FC7D238200,
	U3CDoWarpU3Ed__3_System_Collections_IEnumerator_get_Current_mD1CE2712B5B0919B97EE26421E8DDC32F11E5314,
	NativeVideoPlayer_get_VideoPlayerClass_m1278CDB8894FE79931F13D6A356B3C7FCDCD8EF6,
	NativeVideoPlayer_get_Activity_m108EE6D906C924684CEC6DD69D2FBEC23A9DBB5D,
	NativeVideoPlayer_get_IsAvailable_m823B06C61418FBDBC35DAA4A66B21527F52E603A,
	NativeVideoPlayer_get_IsPlaying_m2E5DE7269712D276A12A1DC20E68A0C7C9D053CE,
	NativeVideoPlayer_get_CurrentPlaybackState_mE90467F47CDA4DF5D910453D4C1297BA32DF0A60,
	NativeVideoPlayer_get_Duration_m0886DCE7E032CBC9954231CD394520D45D8BDAE2,
	NativeVideoPlayer_get_VideoStereoMode_m01908C812015B7EFB33B31A9FE523F0ABD3337F2,
	NativeVideoPlayer_get_VideoWidth_m729F7483A5D0759B858D89464FF476B599C45348,
	NativeVideoPlayer_get_VideoHeight_m171A1E450D92AA7EB0782FB3833689FD10DCF565,
	NativeVideoPlayer_get_PlaybackPosition_mEE846FE9EB7A63A000406C4176832ACB36A7ADAE,
	NativeVideoPlayer_set_PlaybackPosition_m1CC57A9D73A387CD240A581D039E96AC88FB6FFA,
	NativeVideoPlayer_PlayVideo_mF6EDBA9A868E9B2A032656AB66EBB9BB1E0FB845,
	NativeVideoPlayer_Stop_m14AD77315AC29B0C874D058B0C8FB81184E397B9,
	NativeVideoPlayer_Play_m5DFF9917DD1482B0182FCF149F15EB550BF68AE5,
	NativeVideoPlayer_Pause_mBEE54BA105C3700260087E6FF3A44877B509DF1D,
	NativeVideoPlayer_SetPlaybackSpeed_m20B272CBB173EB900F1D77EF61B3CFBD19EA48B1,
	NativeVideoPlayer_SetLooping_mD754507B7ECF37A2433731C9CB1F808ABDC37503,
	NativeVideoPlayer_SetListenerRotation_m0027D91DD19BC0FBD84738F7E75C7867A9E55097,
	NativeVideoPlayer__cctor_m244E5B0BBB03FC464EC253A3194C25AE8E065D48,
	ButtonDownListener_add_onButtonDown_m6A369389838232FEEA1344146C1FD176F2ECE3E0,
	ButtonDownListener_remove_onButtonDown_m88F58309C99390BCEB654878D6C45519ADFB5A02,
	ButtonDownListener_OnPointerDown_mDC9FE990B96FBF2C6D980F8DBCB327E86BD04C9B,
	ButtonDownListener__ctor_mD2522D29DCA57136E34554C01F2C1DB18B6B4DD7,
	MediaPlayerImage_get_buttonType_m738EF74C0EDD4616EFC1E68DC56AC701B53DD2A8,
	MediaPlayerImage_set_buttonType_m86058D07781C3EE3FCEE2546DE6506CA31BB1874,
	MediaPlayerImage_OnPopulateMesh_mBFADD4CECF3314066C0377254FCB875E8C8F078F,
	MediaPlayerImage__ctor_mB86C91BF2885DC329EDD99F24916F9AD717B42D3,
	MoviePlayerSample_get_IsPlaying_mB7FE8B620130A38FB30BC09F7079E12084113CDF,
	MoviePlayerSample_set_IsPlaying_mB98C234AC3AC146C0F74729906F71A7E9F849CE6,
	MoviePlayerSample_get_Duration_m4A055DBAEAC1819C7091ACEF4E2B8DBE5EF4BAFF,
	MoviePlayerSample_set_Duration_m1B29A541BFA2385B82B3A793817338A24081ECBB,
	MoviePlayerSample_get_PlaybackPosition_m7B1A095968AFDF410EE031761D5BC5239328E00E,
	MoviePlayerSample_set_PlaybackPosition_mD8FC604C1C51A0577071A069BD3780DA303D4260,
	MoviePlayerSample_Awake_m096BC4D6D5E6B709F4BB2441B58E489540CAB06A,
	MoviePlayerSample_IsLocalVideo_m3B89DA94A65891DF9D5F3D6B8F7045A4B10897E9,
	MoviePlayerSample_UpdateShapeAndStereo_mA373D088B7381DB5A34923E79695184EC0AA1F0C,
	MoviePlayerSample_Start_m41862FCCB8907A26C31EC7C5B374DEDD661F61A9,
	MoviePlayerSample_Play_m64FB2763D4DCAC19B89BEE384C53488B702F84AB,
	MoviePlayerSample_Play_m43C298C6CA45290022B7F30B8CB648003469E44B,
	MoviePlayerSample_Pause_m1A8CFD8E325D63FFB48510A74E79F4CD7A07892B,
	MoviePlayerSample_SeekTo_mA2F91C759B810802D66038A151701C6AAC13B363,
	MoviePlayerSample_Update_m844225048CC9A23F2DD1AA0544BB61DD7D3867E5,
	MoviePlayerSample_SetPlaybackSpeed_mC9B71A0C6AD6851BE290D41ABB21689BE86BFE82,
	MoviePlayerSample_Stop_m869069EB49DB2E8DDC06D6833BA00905D0EC45CD,
	MoviePlayerSample_OnApplicationPause_mD8379876774437E10F757DE1A6CEF26245746263,
	MoviePlayerSample__ctor_mB1DFDA2F1E1E621CEDDE6898330836B240F1B7B7,
	U3CStartU3Ed__33__ctor_mFBD4AF6C95F797D0753DD1E6789AB7EFB4F3A4B3,
	U3CStartU3Ed__33_System_IDisposable_Dispose_mFD89D5A20E48873E01335A257B97824BE1B9EB56,
	U3CStartU3Ed__33_MoveNext_mD5A4E90D0615EF2D769C53930662CE60239B99F9,
	U3CStartU3Ed__33_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5BCA0FBB841D0017B1050286ADA36684C73A329D,
	U3CStartU3Ed__33_System_Collections_IEnumerator_Reset_m8ECDE46C41B27547EFB6B7E24E378DD06DD9107A,
	U3CStartU3Ed__33_System_Collections_IEnumerator_get_Current_m8D673748B930D29175C9D928B493B015EB8158A9,
	U3CU3Ec__DisplayClass34_0__ctor_m744287E1C5682D76E3E690286A071F39718E25A9,
	U3CU3Ec__DisplayClass34_0_U3CPlayU3Eb__0_mA90EE1D1E1F6E42A47F0488801D4A7734840FD4C,
	MoviePlayerSampleControls_Start_m625008A6C19107D6BE93FF40434BAF580E2C1D80,
	MoviePlayerSampleControls_OnPlayPauseClicked_mDFCC988FB43880C3A320FFC326517C5EEA554099,
	MoviePlayerSampleControls_OnFastForwardClicked_mDBE37296203D54341C0DFAA545CD2C37A3C6D36C,
	MoviePlayerSampleControls_OnRewindClicked_m890FFF45A080BEC11F3B0B3A11D492A1F6692C6E,
	MoviePlayerSampleControls_OnSeekBarMoved_m53704CFB9FDB6324FEA81FC2E7EA57DA12AE04B6,
	MoviePlayerSampleControls_Seek_mEC78032CC65FE67AA6A153DB42A4588955341EE5,
	MoviePlayerSampleControls_Update_m747FB37CA5F8CFBD485486E4517F4748489E38B3,
	MoviePlayerSampleControls_SetVisible_mB5C9350C361FC20DE05207E6CAB3D4805D28248F,
	MoviePlayerSampleControls__ctor_mB9AEEE8EBB60CE81FD9C5BA1C8154171C669BB7A,
	VectorUtil_ToVector_m6B247D561540B92B96CD296261FF7DC2A02AB201,
	AppDeeplinkUI_Start_m775C152A115FF43C1AB34D97D9DC20A34FA705EA,
	AppDeeplinkUI_Update_m8D48F1E3305A2469B0935D4E829931DCB0D76713,
	AppDeeplinkUI_LaunchUnrealDeeplinkSample_mA5F8A1774DB5724E43C917EBE88431C6FE0D9236,
	AppDeeplinkUI_LaunchSelf_m2CA3C1FCE4414860CC1101B7D29B483B9AE45090,
	AppDeeplinkUI_LaunchOtherApp_mD63AA72CBA23EA78C7B91B1E4B59D9B7860B0823,
	AppDeeplinkUI__ctor_mA31F77CBBA609E0128E496E7D23C0FE41A5F94A6,
	CustomDebugUI_Awake_m312D4A67E8EA4824658432AE459FA21877975F71,
	CustomDebugUI_Start_m28DD234ADCAAC557207C669CE37554CD7C7CC833,
	CustomDebugUI_Update_mBF9B69FFE07A90FEA92DF170D99F185C952B043E,
	CustomDebugUI_AddTextField_m6CF8AA8D2CE855EF4B86EEF85E16E0F5AFC9F6FD,
	CustomDebugUI_RemoveFromCanvas_mC32F1E9CA35DFF39075ADC86BF7112A207C9F961,
	CustomDebugUI__ctor_m27BB96E142EA068E3029D709C3065D65DC9D2CFC,
	DebugUISample_Start_mF7AB8D2997FF58D02F5EB0FFA83D5B59DB982C7B,
	DebugUISample_TogglePressed_m2A267F4169FCDC7C8E0DD77FD85253225B6F381C,
	DebugUISample_RadioPressed_m7E36B100115C60405CC14822838E3ECD66418624,
	DebugUISample_SliderPressed_m6A03CFB38E2BAF3DC6E4C63A7DD2B895F2BD5F37,
	DebugUISample_Update_m7FA91668B8F852E7FDE02E7F8F82F3643D729C72,
	DebugUISample_LogButtonPressed_m8198BFFA8627BFEE6BAE268C801B5EE1C4B658F4,
	DebugUISample__ctor_mBD27AFA42BD0FA8EF120FCC4CFE624D2CD6B205B,
	DebugUISample_U3CStartU3Eb__2_0_mC3BA080F2C315CCC00B545B43B4EC960D6CEA6D3,
	DebugUISample_U3CStartU3Eb__2_1_m499D6E39FF2F002CD620BD3A790C87CF58BFBAE3,
	DebugUISample_U3CStartU3Eb__2_2_m1D0C6B17BCC797AB931BDD9128F04612252A4517,
	DebugUISample_U3CStartU3Eb__2_3_m148206FDC1F12C8E839B21019CFDFBCB7C320CD4,
	AnalyticsUI__ctor_m21788905A6C8F76C59714536A99939E5B74426A3,
	SampleUI_Start_m982A3C805909F26B7AA71851484369D105F0A2A5,
	SampleUI_Update_m3509E9A6966DBFBC160CFF3D96B508D1F854113F,
	SampleUI_GetText_m26599FA6C9771765EC9AD94F73BAAD00AEE049BD,
	SampleUI__ctor_mD3D7D360CE2BFF47B7799D39FBC3A99E1D5F211D,
	StartCrashlytics__ctor_m9C967A85082223F55E9138CB80F4107DE4A2C9BC,
	HandsActiveChecker_Awake_m789AB07810059246D886069A33494EDE150458DD,
	HandsActiveChecker_Update_m6C574E921E2E058430A4D6EFD50FB45F27F5526D,
	HandsActiveChecker_GetCenterEye_mEAD4DB3FC55FE5F6AA80E74EFFA4BFF053534D0E,
	HandsActiveChecker__ctor_m8875DA4F87EC8FE57A7587203AA1F695AEA6CBA4,
	U3CGetCenterEyeU3Ed__6__ctor_m99DAFAB56EEBC093A2EFF1857B7E41EDA623C071,
	U3CGetCenterEyeU3Ed__6_System_IDisposable_Dispose_m87282416027E1D142DA109AC2CAE4B8A202B7F25,
	U3CGetCenterEyeU3Ed__6_MoveNext_m8F91FCC289F307F81C3B0A1A6D16809AEAFFFA8A,
	U3CGetCenterEyeU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m52AA44D25E603CBD76F6492C277DFA1502C50301,
	U3CGetCenterEyeU3Ed__6_System_Collections_IEnumerator_Reset_m60966632955FF20F0859533FE10315801CF3EFCC,
	U3CGetCenterEyeU3Ed__6_System_Collections_IEnumerator_get_Current_m45CF38A1CA12804B458CFC1C748D8A3490DF1854,
	CharacterCapsule_Update_mD56302C0703C800E264601DFCECAD70323B6D8E4,
	CharacterCapsule__ctor_m3517E2CB4B11230AC5AF2F46D6FCBBBD3F2FBC0E,
	LocomotionSampleSupport_get_TeleportController_m8E3B912620DEA5687BAC288FE3CE5F8116706216,
	LocomotionSampleSupport_Start_mCF86670570E36A40E41DC7613F8AFD7FAA740A89,
	LocomotionSampleSupport_Update_m031DA7C38919902529A0267E72B70BB5B294C1BB,
	LocomotionSampleSupport_Log_m026777FB96DF6B35ADCDE19FFE75173F9A080C52,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	LocomotionSampleSupport_UpdateToggle_mD1A6755435B3A901645BDFA66781E84BB141B17A,
	LocomotionSampleSupport_SetupNonCap_mADDA8F8E1129D705CF311C52F138ACC60C3826B5,
	LocomotionSampleSupport_SetupTeleportDefaults_m5ACC86875EBD2D512ED55DB8211FB894F462B737,
	LocomotionSampleSupport_AddInstance_m585F0527D6C6C2351CC04DF1AD6AFDFE9252B09F,
	LocomotionSampleSupport_SetupNodeTeleport_m553B0B6C9BA5C94F976B0EA0828523B6C7FA8F79,
	LocomotionSampleSupport_SetupTwoStickTeleport_m0BBFB6EA935A3715A1D1677BC957562431346217,
	LocomotionSampleSupport_SetupWalkOnly_m1EE59A1C4972A91669F90AD573D410C26DBB370E,
	LocomotionSampleSupport_SetupLeftStrafeRightTeleport_mAF0F734749B4AEEEF9E7BA5B6131BA447174A7C5,
	LocomotionSampleSupport__ctor_m613DB25830E2B519F9C75665ABC4F75F3FBB3A44,
	OVROverlayCanvas_Start_m02FACA531257AF78493D111425986BDE67AB2991,
	OVROverlayCanvas_OnDestroy_mB9B73D9E5A1C61426EF8989A4BA55E64B920CB29,
	OVROverlayCanvas_OnEnable_m9FF98CCC4F3C096B72D990F0CF8FE6453D6FE7A1,
	OVROverlayCanvas_OnDisable_m9E0AE938543EDE56DB3FC961CB82FD14D6715961,
	OVROverlayCanvas_ShouldRender_mAB1021F516AA15A84D0F9FA33C9DAE329A8C7157,
	OVROverlayCanvas_Update_m6E50C6A065641B95F393BAB2C20ED325FCBBC750,
	OVROverlayCanvas_get_overlayEnabled_m9B539BAF0CDCCE9E1E5EBD918BE874E76F004BDF,
	OVROverlayCanvas_set_overlayEnabled_mDA991B436B2AB9BA5CBA3F2566352635795AD5F3,
	OVROverlayCanvas__ctor_m8B8FA48BC2B16C384FEBB9DA6B887942548C6539,
	OVROverlayCanvas__cctor_m172128EA1EF3A5E57D56D3657A8363EE747AC2D9,
	AugmentedObject_Start_m8E0AD00236ACFDEAD69389D7467109BEE6B11B58,
	AugmentedObject_Update_m3A9B0C9619B59930AC5508F8FE2758CB0BD02E55,
	AugmentedObject_Grab_m459B031A6D80E520AE75A6A43DBEE1A087DC1CB2,
	AugmentedObject_Release_mAB1BE9AD51540219158AF0EFC23E4472C9B7BD3D,
	AugmentedObject_ToggleShadowType_mE9B7FDE7086D783F599CFE4164FFE16548F0E620,
	AugmentedObject__ctor_m1BEA4EB1EEF2E15B9D43AF499898A2DC25F6056D,
	BrushController_Start_mC8607F5C5E0D3C39318951CE99D90FBE562B9BCB,
	BrushController_Update_mD50E4CA311FB894AAA68222ACA91EBE6C30593E1,
	BrushController_Grab_mD6E7E5B9DC7580A384370CAC4A26AFF0379EE6CB,
	BrushController_Release_m3D7E2EFE180929EF8235DFE5CCB7DEDBF7FD9507,
	BrushController_FadeCameraClearColor_mF279AC0C77EA6F6F1758D99361413188B619AB05,
	BrushController_FadeSphere_mFE0E2D3B7CAB66CCBD2A263BF9E3C5234682E455,
	BrushController__ctor_m9257CBA4722EF69E6444F82933C2A2123AAE0CC6,
	U3CFadeCameraClearColorU3Ed__8__ctor_m71E38D8AED062F90AA945D9CC0540F9CDC001EE7,
	U3CFadeCameraClearColorU3Ed__8_System_IDisposable_Dispose_mB45544A56417DF7024AF0BF2FAC2892BEAA4CB57,
	U3CFadeCameraClearColorU3Ed__8_MoveNext_mADA92C9B3E3117DC738F397AA647A824FCC4BCB1,
	U3CFadeCameraClearColorU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2CCA66E9BC227F6064FBD2B1EBDB29027B712EC6,
	U3CFadeCameraClearColorU3Ed__8_System_Collections_IEnumerator_Reset_m669F3DAFD50F24E439427435286DCBE5D5AAC116,
	U3CFadeCameraClearColorU3Ed__8_System_Collections_IEnumerator_get_Current_mD305219E1C23CE52D59AED4BFEF4C16461DAAAE1,
	U3CFadeSphereU3Ed__9__ctor_mFF2CC1B385DBDFF11A4C2EA5F29E290C231DFBE7,
	U3CFadeSphereU3Ed__9_System_IDisposable_Dispose_mBA8495222AE0CBF7E4A4569A41D5292D973A052B,
	U3CFadeSphereU3Ed__9_MoveNext_m3941BF83B9BCB6851DA0EAC9DBF79BC9052E9825,
	U3CFadeSphereU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m09FCC5A8134E70A5773ABDBD3771EC06F0EFB03F,
	U3CFadeSphereU3Ed__9_System_Collections_IEnumerator_Reset_m38A51A10EA31A82FEC63C26F45E5AD2A52F86AA6,
	U3CFadeSphereU3Ed__9_System_Collections_IEnumerator_get_Current_m86BCBEDDEC870379C62644F1C8BF0CDB327A86C9,
	EnableUnpremultipliedAlpha_Start_m4926EC7506FF573EADA7ECC9CBB7D7E4DECAA67F,
	EnableUnpremultipliedAlpha__ctor_m72150D5AFC1B04755F8960E4A7E2211BC3E6BF0A,
	Flashlight_LateUpdate_mC96F82CE200FBCB574AE8EC09BC4A6794C3D8192,
	Flashlight_ToggleFlashlight_m2C0E6D9CB2AFB7BC18058A37692B42554F53E0AD,
	Flashlight_EnableFlashlight_mB6ED8C5FDB101A26CD2329430939DC65D087907E,
	Flashlight__ctor_m3126420E3768B6D3CFD0472251F924BE38E81671,
	FlashlightController_Start_mA7C1D7727D1DE21DCFD4FD4A1FD3A432DFAD5A56,
	FlashlightController_LateUpdate_m6B632F81EDC2907F75EB22CBCE2BC4ECCAA121CD,
	FlashlightController_FindHands_mBC00BF0B7AF22181B8586036875B7C9DB3C531C4,
	FlashlightController_AlignWithHand_mCE1A47A2AFD6232C0EA1D8D220E33454CD980E55,
	FlashlightController_AlignWithController_m124649B6FC7BDB21BCE91087F5AF313E83EAAB37,
	FlashlightController_Grab_m1C763114DEBABE1752067BB044130A9BB5A75175,
	FlashlightController_Release_m0B6874BDB6CBABE601DB5C94FAD11F11C6F4FE67,
	FlashlightController_FadeLighting_mE6DADFDB6FE28475B62BB3D04E99568CCC8052D8,
	FlashlightController__ctor_mB8B30045D45FF7336C3D1ED3C2343C40EA1EEA10,
	U3CFadeLightingU3Ed__17__ctor_m3178D02F76D3965BBFFA4F56F2F4243AF9B3B33A,
	U3CFadeLightingU3Ed__17_System_IDisposable_Dispose_m636516D26E0FC21793138E4235EAAFD9B87723BF,
	U3CFadeLightingU3Ed__17_MoveNext_m114DFA09D1CD0CF21952702777E86C16871EADAA,
	U3CFadeLightingU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD9E7E8152BD8F758715E45BC1885F51A63E26315,
	U3CFadeLightingU3Ed__17_System_Collections_IEnumerator_Reset_m532D658BF737F23E5DE75444F9DED652DF6FFC27,
	U3CFadeLightingU3Ed__17_System_Collections_IEnumerator_get_Current_mCCF09AD2D019507FE4A94EE11AFAE44708D1A70E,
	GrabObject_Grab_mB218C144950CC665DEF2C0D2621EC3BAB58F4DDB,
	GrabObject_Release_m87C19D984E37BBC51F11AE79362BCC463D9DF541,
	GrabObject_CursorPos_m712B6B88A9A282219C22F5AC6D91563EF6CF129A,
	GrabObject__ctor_mE34AA1EC033A0A74D6E8F881A20312EAD2E37316,
	GrabbedObject__ctor_mD71655859A58277437D2D41BF2D2E32ED45A3142,
	GrabbedObject_Invoke_mC5D11C66F1CA103D44AC0120309B8329C8C5AC12,
	GrabbedObject_BeginInvoke_m673B6B520980FE487E8E3698AC7C5E34E3999F7D,
	GrabbedObject_EndInvoke_mE288DA466CEF66AE67D22C3A85FC4FF0340AC220,
	ReleasedObject__ctor_mA1EB137D310B511053A0A1EB8F02C1A879C50E37,
	ReleasedObject_Invoke_mAD25C4E4B23562D2DC5EA00C154F670B80B2EC1E,
	ReleasedObject_BeginInvoke_mCF974E830722A67E3C0E157F95722386F5135C72,
	ReleasedObject_EndInvoke_m022CDE0C465D8F488CB44DF0F4537DC8D8F97EBF,
	SetCursorPosition__ctor_mFF389CC65BAD558CBA69A47B907658D65C53A63E,
	SetCursorPosition_Invoke_m15B4D29A4EFC2AA3C15ECF436299830A14AEA2F8,
	SetCursorPosition_BeginInvoke_mEC0A62FFCF845C61EB8FF6587D574FFEE524AFE5,
	SetCursorPosition_EndInvoke_m621EE977B55491598884A9DFBFAC6D7B247250AE,
	HandMeshMask_Awake_m507910DB71C1F770805606FBE79EC77CAE9E0896,
	HandMeshMask_Update_m38AC51F8B4FAD5EB72AF1C53379AD9230B4D6185,
	HandMeshMask_CreateHandMesh_m520E9CAE4BB1236F888D480F46994CDBC82BF3A7,
	HandMeshMask_AddKnuckleMesh_m7FE07234E6CE21E5C1930864ADFE9F4E5A878EC9,
	HandMeshMask_AddPalmMesh_m86D1CFFDA37877F6DDA84DEB1EB000ADE491CB5F,
	HandMeshMask_AddVertex_mFC6D3205D1524A29258D1E7C25257D6A3EB0DE2A,
	HandMeshMask__ctor_m37E35E530849032B1EF4024717644B8FC3723361,
	HandMeshUI_Start_m80C99FE9B881C46B86AE58C885846979489F8E2F,
	HandMeshUI_Update_mAC47D9C487ADFA21CB64CB5DA2735E72EB8A3345,
	HandMeshUI_SetSliderValue_m91E3D3B63E9A6677816037BB8DE109815A111FD5,
	HandMeshUI_CheckForHands_m5C8F16DA9E3D2B1906E4681B453190E184D9048D,
	HandMeshUI__ctor_m85C3A4E86D772F5799B0271350C9564E31FDFC89,
	ObjectManipulator_Start_mEBCDF18C339DB050547E7FAC14A140A2DEFEB4C9,
	ObjectManipulator_Update_m72C78567C8C72FC86BB069AD411212072EA8F03D,
	ObjectManipulator_GrabHoverObject_mBC43E1E7E210F0D296012D424536B74C00805A5E,
	ObjectManipulator_ReleaseObject_m02231C55BA814EDC06B3F938B7C2E8DEAB914E28,
	ObjectManipulator_StartDemo_m0754E0B57F00F054718FB66F9C646CB3882B26F9,
	ObjectManipulator_FindHoverObject_m3E6E90C275DF5252101B82C80D2CDBB5C22C4A37,
	ObjectManipulator_ManipulateObject_m229CCFCC09F5B4C39EFDE9E1D83993FB4675511C,
	ObjectManipulator_ClampGrabOffset_m53081A466E925B6FBBE4AFF4850DF53E4531B7A7,
	ObjectManipulator_ClampScale_mAD641AE21DC8D808B1E66B18906A701471035267,
	ObjectManipulator_CheckForDominantHand_m5492FB8645B7165F98D80E4F219962EA4DA4CBCD,
	ObjectManipulator_AssignInstructions_mA360EDC3C9EC6D0BAAF1C6D16618803917B3CC02,
	ObjectManipulator__ctor_mEEE59FC77CA6A41814536C9DFCB24192D5F21DE8,
	U3CStartDemoU3Ed__23__ctor_m93B55F7FFD26DDF5A738871EBEB2107BF95DE4C7,
	U3CStartDemoU3Ed__23_System_IDisposable_Dispose_m474A042BE09520BAFF40F9DAF52345B48C09EBD4,
	U3CStartDemoU3Ed__23_MoveNext_mD24727A203FF95967F58B93DAC73148D97E43ECF,
	U3CStartDemoU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m19A26D41F7FB9A11FB058C438E02F555FB51BA20,
	U3CStartDemoU3Ed__23_System_Collections_IEnumerator_Reset_m4291011401E9FEB2BEB0922EAEC7AEF435F7D49B,
	U3CStartDemoU3Ed__23_System_Collections_IEnumerator_get_Current_m804A4428AFF3C3D87F16FAD3D707FE4585EB405A,
	OverlayPassthrough_Start_m97C9F6B31E6315147C9E6EB5E796CA724229751A,
	OverlayPassthrough_Update_m5553EE278872B84F1DEB50AE402E7B512D997CD7,
	OverlayPassthrough__ctor_mF913DC5B9EA0FCDC65A0E34CAF78F74F5057B499,
	PassthroughBrush_OnDisable_m838B38AE602884CCB0BE04DE661EA60F012E47FB,
	PassthroughBrush_LateUpdate_m03306C8C4FA821AA69875F16A21EF9F9908DE954,
	PassthroughBrush_StartLine_mEDDC47E33545C96489E037E8B136D4CA579624BE,
	PassthroughBrush_UpdateLine_m508789A468A107D18B3C06A34E1A0A48FCA59FF0,
	PassthroughBrush_ClearLines_m684A85937EAF8304462A02BB40F5167779554BF2,
	PassthroughBrush_UndoInkLine_m23CD97DDBA483D18F39256132BDD62F5F0B2A63A,
	PassthroughBrush__ctor_mDEF0A84B7E60A4F9FE0D7584CA5F2F0D92A89311,
	PassthroughController_Start_m086EE35AA2F6701D88686F0A874F2D3AB9684631,
	PassthroughController_Update_mCE0679C2D8F409F507F99818B9FFB4467554FFD1,
	PassthroughController__ctor_mB542E27BD18002805C29FA8B8AE4E70101CC2B12,
	PassthroughProjectionSurface_Start_m1C289BDEF11167F3B6B3CC13CA3D2B0FB243A94B,
	PassthroughProjectionSurface_Update_m32347B174938842813CB23FF93C7DB52E17320AC,
	PassthroughProjectionSurface__ctor_m62D8E3BAA312AF5CA874323872CD7E14EA0251C4,
	PassthroughStyler_Start_m4092C1316E5C3612B264AF1E185992BBBE17C3C6,
	PassthroughStyler_Update_mDDBAB8B2F74B759734BAB13249701F84AE886D8F,
	PassthroughStyler_Grab_mC1D22409F8C813A9516E1BC8C38762359F008BE4,
	PassthroughStyler_Release_m64EC76A02BD96B9818C1838EE93B509F0D59A28A,
	PassthroughStyler_FadeToCurrentStyle_m89B40CBE7FC06A936547F98A3A11CE491E62F75C,
	PassthroughStyler_FadeToDefaultPassthrough_m9CDB8FB4E0D030CB89347B19B531763D9E35EC6D,
	PassthroughStyler_OnBrightnessChanged_mAD761CCF67D62E8B6C31BA3E430E388310FD3C8A,
	PassthroughStyler_OnContrastChanged_mD62F12B93AD84016661953D7AC62A910E794D97C,
	PassthroughStyler_OnAlphaChanged_m2E29232FB7882E0CDD74227B63A6B3409EA71568,
	PassthroughStyler_ShowFullMenu_m63597CE4A8078251E6BA27225B375A4C493D310B,
	PassthroughStyler_Cursor_m7BA7DDF0FAD50949DC8C251499BB8000BF2FD16D,
	PassthroughStyler_DoColorDrag_m11B7DA9E8F21113859605E888C3BEB871684E2A5,
	PassthroughStyler_GetColorFromWheel_mA52723A4C04478A9E36FD6BEADA82662744EBEBE,
	PassthroughStyler__ctor_m5F97489280B446110F4C2BAAD9327E817821B130,
	U3CFadeToCurrentStyleU3Ed__18__ctor_m6035597BE30443B81702055683400A4E114ACDD8,
	U3CFadeToCurrentStyleU3Ed__18_System_IDisposable_Dispose_m1A9700B0CDAA3E66755A0252738541E51CF8FE4B,
	U3CFadeToCurrentStyleU3Ed__18_MoveNext_m65A7265C8CFDE030F527396839DDFCF49FCEE6DA,
	U3CFadeToCurrentStyleU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1D0216642B51BD1DD34FF3F146ECBE7DEE474308,
	U3CFadeToCurrentStyleU3Ed__18_System_Collections_IEnumerator_Reset_mFB080307112AC99A1B9B4B66D5B5F4F1AC119D01,
	U3CFadeToCurrentStyleU3Ed__18_System_Collections_IEnumerator_get_Current_m689B3AD6F09DEAAB97AF382C3E5069BA6A24BCD7,
	U3CFadeToDefaultPassthroughU3Ed__19__ctor_mA5B4C841E6A03409D8B853A6DA5C88E57CD7AC6B,
	U3CFadeToDefaultPassthroughU3Ed__19_System_IDisposable_Dispose_mC41A619DD79902A65FD7FBB4F6BC88E81E56D050,
	U3CFadeToDefaultPassthroughU3Ed__19_MoveNext_m6D2608EC6E0869CF0D742E4716F98A308F74CBE1,
	U3CFadeToDefaultPassthroughU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m09BAA401CD83AD635F5B79D9076EA6DE615AB35B,
	U3CFadeToDefaultPassthroughU3Ed__19_System_Collections_IEnumerator_Reset_m52FC0E312E9BBD076EB8A6837B646BBB3C4D8341,
	U3CFadeToDefaultPassthroughU3Ed__19_System_Collections_IEnumerator_get_Current_mCA24F0BC60A66684360A279879D8097EFDF055C8,
	PassthroughSurface_Start_m7E506149753FD8679C9CB718543978CA53CE1F74,
	PassthroughSurface__ctor_m6B56B0E135BDB59C84145F3D6DC79877B2C65090,
	SPPquad_Start_m092C0AF73D6B9CD60ED91D3DAF859FDAA4EF5896,
	SPPquad_Grab_m8B4D385FEA16E6FFBD1A49860DB3B6FB13295A17,
	SPPquad_Release_mE8CA2CE6FBABBFA6CFBA60A468981092C0914A63,
	SPPquad__ctor_mE270AE3BB58A6EBD5FB9C47F99428A242A79CC41,
	SceneSampler_Awake_m632DFE6F8657193318C2BD87E035227C40EB8507,
	SceneSampler_Update_m3D06DA05BAACAA52006AF75D138BCD5D4923207E,
	SceneSampler__ctor_m613B46CE973907D623D017AD04DAF9B991B61C75,
	SelectivePassthroughExperience_Update_m0DA48E563BEB1B36C67E476AF5745FCB13B88903,
	SelectivePassthroughExperience__ctor_m312145A2A5A8560CE04653F4F4799F843525FD62,
	BouncingBallLogic_OnCollisionEnter_m97964DD8BDF64D2285B6D341EE320209DADF23D0,
	BouncingBallLogic_Start_m6ED9444CFD23A527999969B09194731C07C274D3,
	BouncingBallLogic_Update_m4B85E41A71F892783CE615AE23342FEE6BF7A4F7,
	BouncingBallLogic_UpdateVisibility_m2CCAE3EC443F7F98D95DFE48A4539A2DEB69458A,
	BouncingBallLogic_SetVisible_m478FD34F99FB96C954133266A5865AF2D438D838,
	BouncingBallLogic_Release_m3429304DB0C5D4057AB984194F5F61C201AEC531,
	BouncingBallLogic_PlayPopCallback_m7ED3FF5AE8C9F6D428CCFD03CB152A0EF8E34712,
	BouncingBallLogic__ctor_m41DF8D5FCD1944E15E75B3E218DD189B9427FF4A,
	U3CPlayPopCallbackU3Ed__18__ctor_m09CAC299D59FD092C88CC7D4F5E2C710C3B73571,
	U3CPlayPopCallbackU3Ed__18_System_IDisposable_Dispose_mB7B49306A5581D3DEA1609F043E3F4332A7462C4,
	U3CPlayPopCallbackU3Ed__18_MoveNext_m5381F075A56A616247587E14F1609480C0B9426E,
	U3CPlayPopCallbackU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m01F862A1969B60101D9B821CB37B0048F1908909,
	U3CPlayPopCallbackU3Ed__18_System_Collections_IEnumerator_Reset_mC415B1F10EBA24CBC428796DC3CE9D7B7890C1F4,
	U3CPlayPopCallbackU3Ed__18_System_Collections_IEnumerator_get_Current_mC01C5BAF3D09B65E6855C965728867AA6FC8B5F7,
	BouncingBallMgr_Update_m9F87AFA8FE3EE6ADECBABF683C9E3FBDBE18DC5E,
	BouncingBallMgr__ctor_mBD94F44896B5ACD60FE139EE29D7B1AE5E4AB7FE,
	FurnitureSpawner_Start_m7B287469E81F13BCB22A8EF4CC3DE470A7E00BA3,
	FurnitureSpawner_SpawnSpawnable_m14F8BF89BFC117F1D2604062828C309B52C2E0AE,
	FurnitureSpawner_FindValidSpawnable_m11054694B333F94EA9BB3F7CA566EC8EF05E5086,
	FurnitureSpawner_AddRoomLight_mEA758994AD28A91AFEFD2D680C5C43D5DEC40445,
	FurnitureSpawner_GetVolumeFromTopPlane_m75C21F38C62F993856560A8E6E06331B9E070739,
	FurnitureSpawner__ctor_m2704E3259085F2786CC23C4CDCC1A91A0F4965D4,
	MyCustomSceneModelLoader_DelayedLoad_mE1F053ABA645BB5B358558174A1C5A5C3763ABB4,
	MyCustomSceneModelLoader_OnStart_mB8048368DCC911FAE90630B4F21B3D65513615AC,
	MyCustomSceneModelLoader_OnNoSceneModelToLoad_m5F80931F5C7DC504F044B49A57FFE811A8E7EE8D,
	MyCustomSceneModelLoader__ctor_m5389D475E709A8C6FFA754A164DAFE7ADC025445,
	U3CDelayedLoadU3Ed__0__ctor_mE1E7EFE63326C48E90B5F6422206DA6D19010C4E,
	U3CDelayedLoadU3Ed__0_System_IDisposable_Dispose_mBA61F920C8E466D873890ACF3FAB4BB60AE76186,
	U3CDelayedLoadU3Ed__0_MoveNext_mB3DCA68048992655BFB20E473DE0FD377052ED89,
	U3CDelayedLoadU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m87C2A2CC334B05234EA42BFCC643A870F694A4C7,
	U3CDelayedLoadU3Ed__0_System_Collections_IEnumerator_Reset_mD7D87180BB0F3517276BB6DB42E38C08059E5960,
	U3CDelayedLoadU3Ed__0_System_Collections_IEnumerator_get_Current_mC4BFB03F1E206E7722C9A1A145A59E6E8A4D6F75,
	PassthroughPlayInEditor_Awake_mC1097765DEF1E552A3E6FFC9B6F889D77BC37A81,
	PassthroughPlayInEditor__ctor_m7E47FF94310413244905059C3B3F2CB6A6BF010B,
	RequestCaptureFlow_Start_m70FEF6A1472AD578CC6DDDE95F238FCDCD31ABBA,
	RequestCaptureFlow_Update_m90F717772CADF56BC1E84D2C14B4A235F333CFD0,
	RequestCaptureFlow__ctor_mAF4C33175D2C5AD8995C83622D137F722197DC57,
	SimpleResizable_get_PivotPosition_m1D01D61CDE96C726B6E081D6995B59280BEA6FBD,
	SimpleResizable_get_NewSize_m862F0F24439115ED23271B6A29EBC111F0F63131,
	SimpleResizable_set_NewSize_m382322492871167D9E74ED5EC192BB82D02279AA,
	SimpleResizable_get_DefaultSize_m14C78FE6B0F7FA0F208F74AD2A2326AF9CF636BC,
	SimpleResizable_set_DefaultSize_m60F5422ED1DB9F50376C5EE3C9FF733AC808DA21,
	SimpleResizable_get_Mesh_mF390FA0186363590971BDE459CFE921FDDA9E02A,
	SimpleResizable_set_Mesh_m02E4372CAFDC10C62E5FE30CDB1948C45CF0237A,
	SimpleResizable_Awake_m306B2EA96B96FF7B249EA4CAD09C1B4BBF1D9921,
	SimpleResizable__ctor_m31B4992C1922806C21330594F300AEEB6CC4DFF9,
	SimpleResizer_CreateResizedObject_mB110BD8FA383884EADF4F3B774CBAD1383748E90,
	SimpleResizer_ProcessVertices_m24AE21ABC22850597CAF08DCCEB109FDEDB2D098,
	SimpleResizer_CalculateNewVertexPosition_mA20E2B1065AF25EDFA560FDF38867A1400C36984,
	SimpleResizer__ctor_m91352B862DDAE1F7C013209D2EBD167F380578BD,
	Spawnable_OnBeforeSerialize_mA9695790018650368C0B5D9F216F651600DE3A04,
	Spawnable_OnAfterDeserialize_m876DF77B40363CFF3B260F14B58FDBE73B4D6CE3,
	Spawnable__ctor_mFD124B2D210355D84C19347B3DC93650B9D4CC0E,
	Spawnable_U3COnAfterDeserializeU3Eg__IndexOfU7C4_0_mEC6F35F276E3A9F92A32062D1BFFC0E9BF25FE06,
	VolumeAndPlaneSwitcher_ReplaceAnchor_m0FFE091AD4D5660E2C93AAC782F9F779CC870CB1,
	VolumeAndPlaneSwitcher_Start_m692E7D1223E10BCA05DB7598B12E733799D7A04F,
	VolumeAndPlaneSwitcher_GetVolumeFromTopPlane_m7DDEDB605667BD14B19305BC4766874B96B91E17,
	VolumeAndPlaneSwitcher_GetTopPlaneFromVolume_m25135ACC9448FCADA8B37EFE87E45C467C8FB945,
	VolumeAndPlaneSwitcher__ctor_m664389B1CB38616F46BC0CC750A75866DAEB6B39,
	Anchor_Awake_m34D125678EBEDBCD05EFBCE75AA6EB151F6BB915,
	Anchor_Start_mD36339D1948B883723F81FC3338331075307F6A4,
	Anchor_Update_mC072C2C4AB0F163B9AED43853F856C01626D0440,
	Anchor_OnSaveLocalButtonPressed_m2C9434A9CA569391D11E802B0682AD28B9AB5868,
	Anchor_OnHideButtonPressed_m4BB8148241319803AA2F43CECFBC73358D0E5DB5,
	Anchor_OnEraseButtonPressed_mE13FF5BA0964D8A0538AD25E0A42920AD83F8B59,
	Anchor_set_ShowSaveIcon_m797042530A9E4EE738618A4C00D42F993EB1DD5E,
	Anchor_OnHoverStart_m437F0783984B9AFDE10118C88E5B87453B7F009F,
	Anchor_OnHoverEnd_mE443FF191F33BA01B7E9FF7F683157F2AFC695BB,
	Anchor_OnSelect_m2F4DA100F34CC175BE9979E769F505B46F40F714,
	Anchor_BillboardPanel_mA8E40FCBA276D7571C4A870CB2054D0CB7847645,
	Anchor_HandleMenuNavigation_mF942E3629FB12B1C0E59BA71C4627B1600150CCF,
	Anchor_NavigateToIndexInMenu_mB255CC0394AE2B29A4E6C4A009C15D533FD89E49,
	Anchor__ctor_m75BC8BE83CF97E886358CF4095EF72718D3DBC71,
	Anchor_U3COnSaveLocalButtonPressedU3Eb__22_0_mBDA39E4A533D16B7D65B1271306D7E04F121727C,
	Anchor_U3COnEraseButtonPressedU3Eb__24_0_mC30B48B250A7854826276020EDD2FB9A0ECA949D,
	U3CStartU3Ed__20__ctor_mD01E4A5A8A351D8779E4DF6D534FB12D47419780,
	U3CStartU3Ed__20_System_IDisposable_Dispose_m0DE26B36FC28513D5CB6B351B6FE0550AAF6D082,
	U3CStartU3Ed__20_MoveNext_m6C44A586391F1FFE93D205D9F7AABE8BDDFFDD86,
	U3CStartU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC14BAD61F2795601C2F3569E3D1FC2C9C820B307,
	U3CStartU3Ed__20_System_Collections_IEnumerator_Reset_mCAD6109634C4B7609B1033A6384B1158CF0EC46F,
	U3CStartU3Ed__20_System_Collections_IEnumerator_get_Current_m5DBD7FA346AE9B7D63763850506C682A30BAA716,
	AnchorUIManager_get_AnchorPrefab_m2E86A98938176D1F9D12443C5BA5ED83B2B239D7,
	AnchorUIManager_Awake_m6187A3F96F62BA9C4E9BF832FC6D57082BF6346F,
	AnchorUIManager_Start_m3085EBC75FEEC38284A25172E8C3B72E6DFF9750,
	AnchorUIManager_Update_m5CB8F26FDCF1C51AE7142EE5FEC8013B5AA65274,
	AnchorUIManager_OnCreateModeButtonPressed_m79FE5CB06EC1A185CD9EF430D723706F3016EDF1,
	AnchorUIManager_OnLoadAnchorsButtonPressed_mA02F03AA8DE96B5BDBFA790E2710C1DF0152DBA8,
	AnchorUIManager_ToggleCreateMode_m2050FECBD232A6297623301E672672CB674F5827,
	AnchorUIManager_StartPlacementMode_m68824E6675A136D13ED67D1AE9F9F1697BE5E5FA,
	AnchorUIManager_EndPlacementMode_m9915D350F2E607E67AA0CDD7156C36098D857114,
	AnchorUIManager_StartSelectMode_m069C53AD8A15BB596D58397439DA48FF7440FC29,
	AnchorUIManager_EndSelectMode_m572E37B8952AFD69FC0EA0156A3512F260022043,
	AnchorUIManager_HandleMenuNavigation_mF69382E1696E0E8F6AB1861BB58E30EB896976F7,
	AnchorUIManager_NavigateToIndexInMenu_m23142AEC4AE70869C98C7CD2BA2673DF51B18F74,
	AnchorUIManager_ShowAnchorPreview_m492D99B5E1214BCE6B36A32A2DCD83E26FA89BB4,
	AnchorUIManager_HideAnchorPreview_mCAC597CE861FEB2A753417E607999530DFC64DE1,
	AnchorUIManager_PlaceAnchor_mB9D837D24DC7AD460FE8137B7DF4C8AF648220ED,
	AnchorUIManager_ShowRaycastLine_mFC9DAA1E7A2A0A67CFA6B2C3223DB53968B8212E,
	AnchorUIManager_HideRaycastLine_m51AEE1BCEE2627466893C02250B4430918C2F720,
	AnchorUIManager_ControllerRaycast_mB82AABC1DEA905717028D7796EF9AF81684B3C17,
	AnchorUIManager_HoverAnchor_m614467ECCFA8E9E9F7B54BE5ACE17CC84D24F1A2,
	AnchorUIManager_UnhoverAnchor_mC98A05A47B0DBA22AD97B1FA3679DDCF524B94D6,
	AnchorUIManager_SelectAnchor_mA6BF822A293DEF7743D8B5DD7C0E05843931E482,
	AnchorUIManager__ctor_m88BDBD9BBB743E90038EFA5C3E5C9B351BBBF606,
	PrimaryPressDelegate__ctor_mDCEC312A1DF29CC56F2F61C7EF0D2FEBB49A43F5,
	PrimaryPressDelegate_Invoke_m166299D1E905CD25DECDE5BA9CEA4CE9E9A0F55E,
	PrimaryPressDelegate_BeginInvoke_m556D707A2F92E84E1CB72E5CF197780B4065B6D2,
	PrimaryPressDelegate_EndInvoke_m8FE2F068960C1913EAE33AB0891E065DDF1F10FD,
	SpatialAnchorLoader_LoadAnchorsByUuid_mAA5FF3CE2211ABEA8B8E26B0EC2EC56946EAAAEE,
	SpatialAnchorLoader_Awake_mD83820074A516C0DE2F825445E8F77025060064E,
	SpatialAnchorLoader_Load_mFF7DA24F1030BCE26CC86AD856AE60C9461B839B,
	SpatialAnchorLoader_OnLocalized_m2F849219D5DBB74687F926993F332CFD401D6996,
	SpatialAnchorLoader_Log_mCC1BAB7E206826560B8659135EC2A3B45CF4BD5D,
	SpatialAnchorLoader__ctor_m2008E60AC4986C51481681062AD0740AE2420FCD,
	SpatialAnchorLoader_U3CLoadU3Eb__4_0_m8A9BE73A7D0894DC4EAF29E14132863217C5220D,
	StartMenu_Start_m38A6B8AD8F7E61759540CF792C58672CFD13ED46,
	StartMenu_LoadScene_mF7663ABFF602CD53F3A26C076E6A8FA200347F97,
	StartMenu__ctor_mF07C92F5B046EB38E3933ABD69D96918F29EB952,
	U3CU3Ec__DisplayClass3_0__ctor_mB4BEA21B02CB620F151FB8C1D57C0384883E9130,
	U3CU3Ec__DisplayClass3_0_U3CStartU3Eb__0_mFE41F96EFC3E7314714C2E5F6392280A788A23D2,
	LocalizedHaptics_Start_mC61E2CACF7E1098A8FD5C01522084850710B1DE8,
	LocalizedHaptics_Update_mD3E43622D8537AF0CEE5DA0B0218F10E01640095,
	LocalizedHaptics__ctor_m10C581A9E2F0C09D32D0DBA1EA4F072C041565AA,
	SceneSettings_Awake_m6CE7EFC85AC6C161538C4DF007A69802C03BB2A9,
	SceneSettings_Start_mB2B83815FED9C2D446C42B9253A147CFD0F6A4A9,
	SceneSettings_CollidersSetContactOffset_mFB59467E260FC06F2775A8AF10C64DCB48FCDDB3,
	SceneSettings__ctor_m2D8D57CDE722CC2D9BBAC817A40085B6964D6685,
	StylusTip_Awake_mD9B619E7308DB81EB72F434337A0551306343305,
	StylusTip_Update_m78B5261BF7DDDF580FD53EB200B03B1EE3A41AE7,
	StylusTip_GetT_Device_StylusTip_m17218314754A45C6237DDF58B8CA0F20D9E0AF59,
	StylusTip__ctor_m3E82247B39CD1F7ABC9BBF7A0C217C6ECADCF5BE,
	UiAxis1dInspector_SetExtents_mACBB60DE84AAF9BC719AE83F4D2C6E16C76906EB,
	UiAxis1dInspector_SetName_m4A07447BFC08E009CD2F8B7AD86F167B0F5B8BE3,
	UiAxis1dInspector_SetValue_m420D37263382EBADC31665BD350763B35A57F960,
	UiAxis1dInspector__ctor_mDE98644389C30FE4774268597C852F2F1E7CD6ED,
	UiAxis2dInspector_SetExtents_mBACEEA893A74BEB73EA36DECEFFBA1A8E24E9E46,
	UiAxis2dInspector_SetName_m427BDDEA6032A8E729E8C51BF487A5E1BC20910D,
	UiAxis2dInspector_SetValue_mC106368E30631722E4575DF3F7FF5C9424A90159,
	UiAxis2dInspector__ctor_m4FEF37CE2DD5792BBBF44240230627E06AD08EA1,
	UiBoolInspector_SetName_m2A9FF57CC356247E0851F124BB8D6BFB4E3F8C81,
	UiBoolInspector_SetValue_m9C065608BC184F6E7E2F8F7668627A824EA32620,
	UiBoolInspector__ctor_m40522F9AD5BF8A79301782BEBFA8C68DDBE3F974,
	UiDeviceInspector_Start_mE92B59858FFE0CDD557CBDC4EE75001BCCA79333,
	UiDeviceInspector_Update_m2A95294AE50C81A88A6BE6235A2A768030269031,
	UiDeviceInspector_ToDeviceModel_mD4A63EADD4DBA54AFD4B8E63BC83C20CCB6CD8A0,
	UiDeviceInspector_ToHandednessString_mFDAECB5A6DB42D6D2577EA3CB62E66C287D4029C,
	UiDeviceInspector__ctor_mA68E9B930ECF2F480BCE82CE5B1E201413758E46,
	UiSceneMenu_Awake_m24A0829C551B2616EDAC7CD9641D63308D5D47DC,
	UiSceneMenu_Update_mE2CC16CFB12AB4B2A1A91BF8CF8BB471D46CF51A,
	UiSceneMenu_InputPrevScene_m4478D06607F104031EF62CAC373C4060A904E899,
	UiSceneMenu_InputNextScene_m3018EE6EFA089511D2315864B5829014632B5959,
	UiSceneMenu_KeyboardPrevScene_mE606218757D66FFB06FBC283CF1BF3B541E62B45,
	UiSceneMenu_KeyboardNextScene_m3320B0F75EF37CA8FDCC0DB7DA9B9B94CC1A017F,
	UiSceneMenu_ThumbstickPrevScene_m895D6B25D49D900C54665A74EC699283CD49F318,
	UiSceneMenu_ThumbstickNextScene_m91AF192AA64631CE23F68107DBA40254CCF4B31E,
	UiSceneMenu_GetLastThumbstickValue_mC4E4C2019D9244FFCD874B6B2E8482B38A62308F,
	UiSceneMenu_ChangeScene_mCDBE9A14D56EF5B56D360C800361F74DD0506DF6,
	UiSceneMenu_CreateLabel_mA57587AB59F27847B7E510D4857B5DCEDCE0642C,
	UiSceneMenu__ctor_mDED3E1480A68682DDFCDA08B4C4F45E60EB060F7,
	UiVectorInspector_SetName_m585A941198E2C066720393A50792773A1D19B459,
	UiVectorInspector_SetValue_mBBB3DC1D5160692BD71B306C3C59F6A7DBC336B2,
	UiVectorInspector__ctor_mF45CE767D5F1589A324DAAA2716C528771BD6BAF,
	BookTeleport_Teleport_m6E6D6A8D06455DBB24966AAD6DDAFA9FEA5838EB,
	BookTeleport_OnTriggerEnter_m69842F096CF5D70A50162743F0E267409A516341,
	BookTeleport__ctor_m8EDA58152107BDFA7398CD02361FC76421759D82,
	U3CTeleportU3Ed__3__ctor_mC209EAD9B19B590B355A0614A8EE91BD95FE35AE,
	U3CTeleportU3Ed__3_System_IDisposable_Dispose_mFBAE3F148D58D677ACBB37A53D3851F2153D93ED,
	U3CTeleportU3Ed__3_MoveNext_m3AD51C8419FF5D0B1EE5474754B359AADCD2511D,
	U3CTeleportU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m263CD5B0151F6C38FCE54904EB90CEB2B4ECF32F,
	U3CTeleportU3Ed__3_System_Collections_IEnumerator_Reset_m35A50F5E3AA4732116933911469DE7EA5F83AAB3,
	U3CTeleportU3Ed__3_System_Collections_IEnumerator_get_Current_mEAB9F5E864F436F8B6F22123A96AC31B6797D914,
	Test_Start_m4D6FA0B24EBFD6471F4596A93EC95EC1EB5355D8,
	Test_Update_m85FFE4CC559D42AAFA74D4DEAE19415052BB70D0,
	Test__ctor_m1C2B5E11A339FD79C0C458FB86723E5F2B74545C,
	SpinWeapon_Update_m6D73D6D900BEAE7002EBA691594EC13DEF9D7727,
	SpinWeapon__ctor_mEE925257A3AFB354D5C69F7C218A7BB8462E4147,
	ColorGrabbable_get_Highlight_mEE9A5BC6077D6415C0F5AAEBF9268BCEDA9192CB,
	ColorGrabbable_set_Highlight_mF9C67E77A7CAB0FF638BDF7EE4C359530267191F,
	ColorGrabbable_UpdateColor_m4D60081FC49D8BD81C4B63B35405B144232F8C60,
	ColorGrabbable_GrabBegin_m756D3F58E950E5B54A1B7C4E79F2BD99AE630108,
	ColorGrabbable_GrabEnd_mCBB05FAD02AA291052245779AD795CEDA97F960F,
	ColorGrabbable_Awake_mC26EA500134341FD38388600111FF6E5668A8832,
	ColorGrabbable_SetColor_m1EA4F276B7EFDBD7EE35D598963E888307850540,
	ColorGrabbable__ctor_mCC1D8EDB3DB7A4DBEE8D15AC5A361A1B102EEA34,
	ColorGrabbable__cctor_m31CC0B60F1F9721609529F417739F468ADBB3792,
	DistanceGrabbable_get_InRange_m056971DE194AD0A8B85A8E8E3E82F76711FAF8EA,
	DistanceGrabbable_set_InRange_m0EFDE0426D5819B04621E607860A63531577586D,
	DistanceGrabbable_get_Targeted_m5D48533FA91BE16785B21D7881202E27DA1A8D0A,
	DistanceGrabbable_set_Targeted_m9D01FAEC07F2BBD49B6DA12F1A014D2AFDBDD0E0,
	DistanceGrabbable_Start_m0F13F152E7FCB05A41B8E0E8B6699462531AF358,
	DistanceGrabbable_RefreshCrosshair_m5461E3A1849F56BC79A9BEC50F63EE0F5C8C08A9,
	DistanceGrabbable__ctor_mB69087D8AA20E7C90FAA564AC6D963C5606CDE22,
	DistanceGrabber_get_UseSpherecast_mF25C99BCAF0C6E6CA18A4C42E4AB34BF8BFC6D5F,
	DistanceGrabber_set_UseSpherecast_mE0BAA3127799C6328433698FA578D4E71D8AC86F,
	DistanceGrabber_Start_m775855E7B9023538B7E56155A6597519A953ED70,
	DistanceGrabber_Update_m960BB0DB05A12AE7386926698E0153ED5BEAEAEE,
	DistanceGrabber_GrabBegin_m77865480609755B1282A5A546DC6A53FA63B49CF,
	DistanceGrabber_MoveGrabbedObject_m6B815535F8AAC860485491CCA49CD8046802F0A9,
	DistanceGrabber_HitInfoToGrabbable_m53F7D8C62778137165E37417B4AB3C087A485804,
	DistanceGrabber_FindTarget_m1F30C7639B39EEB998BC94B7AD6483A235F162EC,
	DistanceGrabber_FindTargetWithSpherecast_m561C09222DCCD4C52DF037E1AFD95D6EEFF3645D,
	DistanceGrabber_GrabVolumeEnable_m5AE9B41CBEB5A5EBC1D2F7945B875794FD93A405,
	DistanceGrabber_OffhandGrabbed_m31A56380ABE7DB5847799670467539468AF085DB,
	DistanceGrabber__ctor_m80196B68D9FB9CDE30E2CEC5690F4BB97740A366,
	GrabManager_OnTriggerEnter_m6EF4F094BEAE1A7A31854F98826B3FBB1B74E91F,
	GrabManager_OnTriggerExit_mC52984A8FECA9E7098E2615B0DAFEFAEA92997F7,
	GrabManager__ctor_m5A60AC8D6E026A5A4FDAB55DAC7CEDCDD2913376,
	GrabbableCrosshair_Start_mFE636535DBC51AC58F63DDB687E6D1875D63FD97,
	GrabbableCrosshair_SetState_m5D2604BA1418ED7C18E0CFB823CAA172EA1B23C6,
	GrabbableCrosshair_Update_m9A0A89691DDE8B292D735158EB3C6EDC852DC979,
	GrabbableCrosshair__ctor_m59EFA7290C1E0CFC102C201C1303868B7AE1ED0D,
	PauseOnInputLoss_Start_m309D5D446C65347D01220D6965E848AE62DB809D,
	PauseOnInputLoss_OnInputFocusLost_mAC19B6B6F2BD7435BD93C83448F67F22A86ECEB2,
	PauseOnInputLoss_OnInputFocusAcquired_m21D3CFB7EF526E324CBF8F43F6B2102EACDEC057,
	PauseOnInputLoss__ctor_m0648631C18A65BFA8306CBD7C10288D6CB81EC25,
	BoneCapsuleTriggerLogic_OnDisable_m78D90F38B9238A03909E14C79AC447972BE23702,
	BoneCapsuleTriggerLogic_Update_mA3BC03489B0871D073AD4CF06850AD26768D4A24,
	BoneCapsuleTriggerLogic_OnTriggerEnter_m711C97FA1E316AB56D8084519BE087A638497C39,
	BoneCapsuleTriggerLogic_OnTriggerExit_m5E0F1A427B4A13D901592A0CA13FDC839C6C7C65,
	BoneCapsuleTriggerLogic_CleanUpDeadColliders_m93077C24AD19895958A85B39B597DDAE5EFC90B1,
	BoneCapsuleTriggerLogic__ctor_m11A006B3E2E5D97ACC8F7908F57598EB8A7FADBD,
	ButtonController_get_ValidToolTagsMask_mC1A23D42383354957AC3ED2EE4E0E443E4BD7EE8,
	ButtonController_get_LocalButtonDirection_mCD9B2BA21A5208ACF45B9DE2348951CED9A2F597,
	ButtonController_get_CurrentButtonState_m97D3281E1AA62F0B28B9FC13D5C6D97E8898374A,
	ButtonController_set_CurrentButtonState_mDDD60059979F7FCE78D320A3D034ED7550F076C1,
	ButtonController_Awake_m9A642360ACD027C8D3C3B39598D5D7483AB9D199,
	ButtonController_FireInteractionEventsOnDepth_m8532030AB88C8754A41BA83EFE29ED352DE838C9,
	ButtonController_UpdateCollisionDepth_mD5E62BC8315DE7342BBD20473AEE3FDD65788A3D,
	ButtonController_GetUpcomingStateNearField_m4377557E72D77C2AA2D3FE1DEAE6F5067F54D12E,
	ButtonController_ForceResetButton_m82BCDD2D684499C11C329CB5AD25C9DAD23AEE9E,
	ButtonController_IsValidContact_mF9BEA01F40D5CF760D0D26E15603D51B0BE2C59E,
	ButtonController_PassEntryTest_m1CC5A6460FD676ADC06C3D8C003D3E90784028B0,
	ButtonController_PassPerpTest_mCCEFC8E20AAF9FBE58E5BEDCE554CEF048EC6196,
	ButtonController__ctor_m64221E61350B4251820CAA250163801C4AAC1781,
	ButtonTriggerZone_get_Collider_mA97511B633C8FA23B1E1B864FE94DF7771424CCA,
	ButtonTriggerZone_set_Collider_mB7AE7E1CA02A2EB268DD49032D0F33472F443AB5,
	ButtonTriggerZone_get_ParentInteractable_m14EDBF5B2E361C7B71513E0FD4D10EA87D388693,
	ButtonTriggerZone_set_ParentInteractable_m6BF1259D2ED0DF4B565C08D00C2A867074C03A53,
	ButtonTriggerZone_get_CollisionDepth_m066AACBCB480A4D05E1F9910AAB71F3EB4F90558,
	ButtonTriggerZone_Awake_mAEDDFC443C2337ECC22046203921081412A8253C,
	ButtonTriggerZone__ctor_mE84EA0EF8CC850618DF29DCF722071F662F46D35,
	NULL,
	NULL,
	NULL,
	ColliderZoneArgs__ctor_m2F17926EE45B385F3291ABB057D0DB1C46E7C721,
	HandsManager_get_RightHand_mE9F238C463B7E656776BAD73EFE3EF3EDF4479B3,
	HandsManager_set_RightHand_m007F2D3739DF6176DA4F7E496C36EEB66A2437B6,
	HandsManager_get_RightHandSkeleton_mBCF403959F6ABB1D38A64EDFB2EF5410A71BA9D8,
	HandsManager_set_RightHandSkeleton_m9F1BE77E728E2B0ADE142380CA4EA62589754159,
	HandsManager_get_RightHandSkeletonRenderer_mEBEE32D6AB8B63D8DA808FDBB8FC20F6E066D28C,
	HandsManager_set_RightHandSkeletonRenderer_mE330C23C803564C20D05F29CCA79AA575E32D3F1,
	HandsManager_get_RightHandMesh_mA1B64CFD74E89E1F8A3A59C040AC9AA732837C6D,
	HandsManager_set_RightHandMesh_m8B308743A2F158A112E5C0EBB6A54A1D19451DBD,
	HandsManager_get_RightHandMeshRenderer_mE2D447583D12CB6E51360CD78BD25B85C33D613E,
	HandsManager_set_RightHandMeshRenderer_mD2468B3A5B1C6EA97F07D6314DA50969C600380E,
	HandsManager_get_LeftHand_m53807ABE4939BD2D9260E268C6674B7011713BA8,
	HandsManager_set_LeftHand_m32768139F21ADDBCD87E2833D2BB30DAE5B59F4F,
	HandsManager_get_LeftHandSkeleton_m44A310FFC70C3CB410D2DC2F235F57B1B58FF2D2,
	HandsManager_set_LeftHandSkeleton_mC76A55A9DCF670FC60B34CC92DD81027E7735B08,
	HandsManager_get_LeftHandSkeletonRenderer_m8FD29F034B1EC81684F4C74A8301596395011B48,
	HandsManager_set_LeftHandSkeletonRenderer_m1C5A2A642C6614C040085355D8214E16D934D28D,
	HandsManager_get_LeftHandMesh_m16A9E535B1B520F309D0886704A3201648B16CDD,
	HandsManager_set_LeftHandMesh_m56E8B4E036DA7AACB922228B4FE27FCC272278E3,
	HandsManager_get_LeftHandMeshRenderer_mDE3580A395755759374279284C9650D59D09CFF2,
	HandsManager_set_LeftHandMeshRenderer_m102CF222EBA2577F468099C726325DF3B779FF64,
	HandsManager_get_Instance_m9F9D019322154186D82B64FBDBE1A1EC2867778E,
	HandsManager_set_Instance_mE462D6A4BED5922361C942E0630FDF3421AB3833,
	HandsManager_Awake_m7D236135CCB8A2F47F442B2EE89281B985ABA02A,
	HandsManager_Update_m3AAFA9F8B8B994326B7695807BA0D38FF592C2A3,
	HandsManager_FindSkeletonVisualGameObjects_m31E51747D167E74132150423190EC4AAC13B1E53,
	HandsManager_SwitchVisualization_mFF5FEEE41ACCB620EC9E98BA8364A343CE7F5584,
	HandsManager_SetToCurrentVisualMode_m183D5AEF5523A39C0054A34E304FE88E25E9E277,
	HandsManager_GetCapsulesPerBone_mC638435584A77EDC7E947B35A14242EA66BB9B61,
	HandsManager_IsInitialized_m607714995C91CC61C6672A91A631EAC7FA934917,
	HandsManager__ctor_mF764E9D2BE5BB64D933E7B6925E0F549EA1341C6,
	U3CFindSkeletonVisualGameObjectsU3Ed__52__ctor_mB55CA2BF6CE22AC93FB1D1E0324FA76F2895B6BB,
	U3CFindSkeletonVisualGameObjectsU3Ed__52_System_IDisposable_Dispose_mBB9AC6B3987FBE05D17229FC65772D6AEF1AE5C5,
	U3CFindSkeletonVisualGameObjectsU3Ed__52_MoveNext_mDB2E7404E01F1E9764DFD7C7E69BE594B4130CFA,
	U3CFindSkeletonVisualGameObjectsU3Ed__52_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m53507F454464337DB141FA4B5B17D2ABBD2C0FE4,
	U3CFindSkeletonVisualGameObjectsU3Ed__52_System_Collections_IEnumerator_Reset_m38AAC961B881CFB50A5DD5D622E430A3F56E6416,
	U3CFindSkeletonVisualGameObjectsU3Ed__52_System_Collections_IEnumerator_get_Current_m1534C6E698DEA0AE22B6DB0F7352E4815D614CD3,
	Interactable_get_ProximityCollider_mF6F604B5AC84CDF8FDA76502CF97731CC056FF08,
	Interactable_get_ContactCollider_m4735130C7B2B2FE783C186EBC1A482C7E29D3873,
	Interactable_get_ActionCollider_mBAF9B046536B533C7BA2226926BBC50E17B6726D,
	Interactable_get_ValidToolTagsMask_m85DABBD0A6116F184E84F198CAC92C26121F6DF6,
	Interactable_add_ProximityZoneEvent_m2EFA9B8880CE03E14E54AAF2282D089F2E4B3FA0,
	Interactable_remove_ProximityZoneEvent_m314D6AC5B0CAC5A5FF915B383F9C4E24A44C0183,
	Interactable_OnProximityZoneEvent_mDED9C800E85CF54A06FB3CCA1DB2EF51ECF15A1F,
	Interactable_add_ContactZoneEvent_m284AEAF4458BB62779A75C591F261091518B3481,
	Interactable_remove_ContactZoneEvent_m96A59B1DA482F032CF5D490918466CF68A1A5D02,
	Interactable_OnContactZoneEvent_mF346FBAB8E4F53A26B148E8126759D64A70B5E73,
	Interactable_add_ActionZoneEvent_m303D57C7749AD0BE641D5031D90F4A1053B9B438,
	Interactable_remove_ActionZoneEvent_m4E8476203B9F028436EBD7F9E29DD629BE44FF2D,
	Interactable_OnActionZoneEvent_mA4413FFF1DD8593420D456C9A8EEA3487D1CB9DE,
	NULL,
	Interactable_Awake_m673622B78CF87568C609EDD039F0D947CF4E9F32,
	Interactable_OnDestroy_m24FA62B42CADA4737F199EFB99C057CCD0ADFB94,
	Interactable__ctor_m38A75FA2BE69E84CE6E111E03319FA303714B4D6,
	InteractableStateArgsEvent__ctor_mD35D9AB2D5844DC8C2246F8AD4EA36A0B09308DE,
	InteractableStateArgs__ctor_m3C317F0382FE5F267D662DDCFABE336F06862624,
	InteractableRegistry_get_Interactables_mB661BE7ACD52E8D6C6F6F31CCF66761C0D6D152A,
	InteractableRegistry_RegisterInteractable_m6D6FC17C1D0D9E571387ABBB57B1884E30799B30,
	InteractableRegistry_UnregisterInteractable_mC1B0A16013D0A1FDA4F96E0832476986A257CA11,
	InteractableRegistry__ctor_mF51128E4BD939E58FF4E97F8F43DF49290548B80,
	InteractableRegistry__cctor_mB8D0E4DE4F59BCE89971E31C7C4B9FD1ABC1C30E,
	InteractableToolsCreator_Awake_m52A416198A9987B601BFB7B4DA2D4F81763787D5,
	InteractableToolsCreator_AttachToolsToHands_m494AA7A9F37A70A890E0E386D44474483AAB9169,
	InteractableToolsCreator_AttachToolToHandTransform_m76F801FCEFADB36E154C683F39DAC5A38CD6F1D5,
	InteractableToolsCreator__ctor_m3B28F7ECE7DC21C19A7F090418D93B4A90D279A4,
	U3CAttachToolsToHandsU3Ed__3__ctor_m734EDB11729722D3E2FF780175D03056A3FB6B59,
	U3CAttachToolsToHandsU3Ed__3_System_IDisposable_Dispose_m465E039D390579A8A71EDEA3E92B1B21759BBFF9,
	U3CAttachToolsToHandsU3Ed__3_MoveNext_mED0C488063607CCD70041DB0782687049723B1E0,
	U3CAttachToolsToHandsU3Ed__3_U3CU3Em__Finally1_m8A2BD809ED53D8BAB20DB018A676FEC81B1C6206,
	U3CAttachToolsToHandsU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9C9D3D39C28404672F886B0E4DF9571ADEFD737D,
	U3CAttachToolsToHandsU3Ed__3_System_Collections_IEnumerator_Reset_mF4B96290820469D2C97EEF77C56725A051EE2A5B,
	U3CAttachToolsToHandsU3Ed__3_System_Collections_IEnumerator_get_Current_m1BC783E9E2AEAC011D84DDF68154B2A50B83686F,
	InteractableToolsInputRouter_get_Instance_mF54ED5593C45AF98D05F43DCCF7F5B073115209B,
	InteractableToolsInputRouter_RegisterInteractableTool_m0427DDDC21A231B5E405F2D2B2C91470C99F0656,
	InteractableToolsInputRouter_UnregisterInteractableTool_m10F7F03FBF73F6D75A7D9FB96564C26190BCF96A,
	InteractableToolsInputRouter_Update_mFFABA2B1A23E61F20573C7589FF38352FBF5B09B,
	InteractableToolsInputRouter_UpdateToolsAndEnableState_mDC4F03CF1C73759E460B7C55D69DF2953B928516,
	InteractableToolsInputRouter_UpdateTools_m497363491848B42184F5CD40662A510E5236FE5F,
	InteractableToolsInputRouter_ToggleToolsEnableState_mCB03C3A19E2CF7287E61B7BD684523AF54DECAAF,
	InteractableToolsInputRouter__ctor_m1F12838905D93E459F495D3417F16A3347BC7359,
	FingerTipPokeTool_get_ToolTags_mBD6F55F53B1190B9D1A07EE70C58B117C35DC144,
	FingerTipPokeTool_get_ToolInputState_m5135949E46396699EFB438DCEF083077CA58A42D,
	FingerTipPokeTool_get_IsFarFieldTool_m19F2F230707836E77DACA32EA3295BB3836E38FE,
	FingerTipPokeTool_get_EnableState_mAFAC89BBE30FA63FD0361CB2C73F546A6421A81A,
	FingerTipPokeTool_set_EnableState_m53263FC9F17DBD554E665FE970FFFB8660840772,
	FingerTipPokeTool_Initialize_mF6EAF229A254AAA628C55FD25DFC45F6DB52603F,
	FingerTipPokeTool_AttachTriggerLogic_m806CC48725DBC7E41BE57F20DA730BEB3C96EE1F,
	FingerTipPokeTool_Update_m2C3F38F5A27DC330842FE5B82FCA1C850EE3D85B,
	FingerTipPokeTool_UpdateAverageVelocity_mC1BFD7068F92A60898578833DB93CF37BCA75E38,
	FingerTipPokeTool_CheckAndUpdateScale_mDE1DDF48D29BA291BCE6EDB0EDB6CDD04E803207,
	FingerTipPokeTool_GetNextIntersectingObjects_mA962AC37DB3CBF382E81F163B450CC69A42E156B,
	FingerTipPokeTool_FocusOnInteractable_mBA11E0AA0F742ADA5FC0859146C644DEA5D51AE7,
	FingerTipPokeTool_DeFocus_mEA2E9F73BCF74755786061B17A11B663BB01474C,
	FingerTipPokeTool__ctor_m0793D77F8FC85C10E5943D692BEC9AC367F68173,
	U3CAttachTriggerLogicU3Ed__21__ctor_mCC871AFFBBB476D21423473C54CBDEBD2F14CE9D,
	U3CAttachTriggerLogicU3Ed__21_System_IDisposable_Dispose_mBBABBA85272B6862FE1453CBD314E245B6EA24E7,
	U3CAttachTriggerLogicU3Ed__21_MoveNext_m4D36E1F74B87AF1B2AE443A494AC3BD5616CD4FF,
	U3CAttachTriggerLogicU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m91F1B4E86C3AF75E8070FF63A39EBFF0539499B5,
	U3CAttachTriggerLogicU3Ed__21_System_Collections_IEnumerator_Reset_m550408ED48447F61859BFBD490115411547A6856,
	U3CAttachTriggerLogicU3Ed__21_System_Collections_IEnumerator_get_Current_mB807F3B3CCBDAF22F0233E0A7A6CDB2DE46DC36E,
	FingerTipPokeToolView_get_InteractableTool_mBB4D33BE156B9EE4FBFEB6D3558E3E04B8ADB51F,
	FingerTipPokeToolView_set_InteractableTool_mDF529210D8F673D622B4427BBDC28106C80FF9AB,
	FingerTipPokeToolView_get_EnableState_m4C69AA39F36971BCB98909949405B1B0766A7391,
	FingerTipPokeToolView_set_EnableState_m5D51EB5315EB2FBF3FE5F9B471C9B1313905820F,
	FingerTipPokeToolView_get_ToolActivateState_mE74D50A247203ABF3902B02AC10F46172A64155B,
	FingerTipPokeToolView_set_ToolActivateState_m5D27DF2EB537D9BA0E4DBA30FA1A339E7EC45EFF,
	FingerTipPokeToolView_get_SphereRadius_mA3C7336BF367853C422F0AFEB012A77CFB627BD8,
	FingerTipPokeToolView_set_SphereRadius_mAD1E864E25672BE5A87327B53BEF716C3AF34275,
	FingerTipPokeToolView_Awake_m5F066BEF3A88699EBF8BD848574BD8CDCFDFDC24,
	FingerTipPokeToolView_SetFocusedInteractable_m5B02B5A8DECB19168DF581C171368E46F36D9409,
	FingerTipPokeToolView__ctor_mAF5D655B9724611A68D1A582B2473D035D308784,
	InteractableCollisionInfo__ctor_m7348491A5EAFD2DB5F4B9702C512F9244B6EAC91,
	InteractableTool_get_ToolTransform_m205F590619ED079C0A72EA22A89EE3927842DCA7,
	InteractableTool_get_IsRightHandedTool_m737F795BBD30E1EC82B68A7EF69CD222FB31541A,
	InteractableTool_set_IsRightHandedTool_m465A3CE11D7F4A97C4D2B0FC69F90922C886D53D,
	NULL,
	NULL,
	NULL,
	InteractableTool_get_Velocity_mAC1D35D388D176A5F972A883516180AC66B8090F,
	InteractableTool_set_Velocity_m42BE2E5966AF9A78A1C157A35B123926EFB1814B,
	InteractableTool_get_InteractionPosition_mE0FB722D4A4A52B1ACFB6D35CAFB876AA01C2111,
	InteractableTool_set_InteractionPosition_m7D1E65ACF3D7C8D81E733557B4B97C858B11FAE2,
	InteractableTool_GetCurrentIntersectingObjects_m6F4274996E629AF558DAE685B712356444DE3147,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	InteractableTool_GetFirstCurrentCollisionInfo_m128C0E18F5FF4B752ECAA380CEAD01A0C35D4C40,
	InteractableTool_ClearAllCurrentCollisionInfos_mB2B670C225F4D7E9ECA1FE8F32A4591C794D3F95,
	InteractableTool_UpdateCurrentCollisionsBasedOnDepth_m0C7062FF22E200B4D7E8E315BC862B9D47698F9A,
	InteractableTool_UpdateLatestCollisionData_m464888128C71919DDBB2C6C7688EAF0740634250,
	InteractableTool__ctor_m53710FF617E85E4274C1AAA0653FF167C28A3722,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	PinchStateModule_get_PinchUpAndDownOnFocusedObject_m6A5427560A3F4A2372BC64C89FD513CB60D6E345,
	PinchStateModule_get_PinchSteadyOnFocusedObject_mE211E69F60B0867D44633FA5B25FFDFEDE2E5D13,
	PinchStateModule_get_PinchDownOnFocusedObject_m1D2C7ED77EFB4833B1921792A3735636BA207109,
	PinchStateModule__ctor_m06DE10F3EADE5A00F264E444C105C23A0F4D6891,
	PinchStateModule_UpdateState_mE6EC067C8424F21E0C1553703E69E5B25E56DCBE,
	RayTool_get_ToolTags_mF79256339F1A375AF576B2E2029537F43EE2C0B7,
	RayTool_get_ToolInputState_m54478A71F7ED3FE7E08BE631D08037E4A6F76D89,
	RayTool_get_IsFarFieldTool_mF690CF4ED00E55B933E58EE8B46737D215F4C6A7,
	RayTool_get_EnableState_mF4A05EDD10F5FE793D780CF3745D6881ED3B3D3D,
	RayTool_set_EnableState_mE27327551A200506EC3FA1185A10F9392AAAC4C9,
	RayTool_Initialize_m6C629CDBF7127645DCD9CABC894F1D1327FC036C,
	RayTool_OnDestroy_m8332B404479AA0DA187DF0C424576FB9262442C1,
	RayTool_Update_mFF995DE32795EB15C757CFD5CB8201786B0F62DC,
	RayTool_GetRayCastOrigin_m4AE3575A3D65B27E3809814C3415E39FC5489B47,
	RayTool_GetNextIntersectingObjects_m8BA14292395263E433DD82B9F64080C42A5A4821,
	RayTool_HasRayReleasedInteractable_m9B7A76FC8F15B453D91980F1D118EFEF432BA5CA,
	RayTool_FindTargetInteractable_m4D0BD21D1425CD76DD0FA675ECBF7E3AA4AAB685,
	RayTool_FindPrimaryRaycastHit_mB6C7E211ADEC012A379E02BBC2D60B5B567CB8F8,
	RayTool_FindInteractableViaConeTest_mEDB0AE5A0EF2B9FF895B7D08EE6B50A5DD1AD5CF,
	RayTool_FocusOnInteractable_m535AB705865AB75D40842C9B5587E17732DDD2F9,
	RayTool_DeFocus_m085E103A8A047F7067906C31DACE15ACE126C6CE,
	RayTool__ctor_mC1D1E10E1C6B15267986E22E04C6A23966CA681B,
	RayToolView_get_EnableState_m464E298E5F0DDCF469868DA3F7A07C1BC74FCDE7,
	RayToolView_set_EnableState_m0C757E1F42C28C763F57C93F4670ED3DA3B511A5,
	RayToolView_get_ToolActivateState_m5DE8AD80488ACB610C8A00467984095998DB4723,
	RayToolView_set_ToolActivateState_m382F731A347E5EF495984B48AB4EE7941FD0E99B,
	RayToolView_Awake_m8D4B4C38C29579601A9E21E8BE3A2E4E95246985,
	RayToolView_get_InteractableTool_m6678005A9883BB94200F353DF981AE211CFFE44A,
	RayToolView_set_InteractableTool_m0E3083D4DD470BB252858CF5D401E248705FD46B,
	RayToolView_SetFocusedInteractable_mE7F566BFFD744607512ACBBDC070D22D737A1E64,
	RayToolView_Update_mC0702F11E7C9A01B8E14CDD5C607E15B2B1882D7,
	RayToolView_GetPointOnBezierCurve_m3D4198786E95ADE93CB7673AC90DB274A655D5D7,
	RayToolView__ctor_m729843F49E716782D52EEB4D3A725EAE9E7F59D9,
	DistanceGrabberSample_get_UseSpherecast_m1940F73780371F34046759ED81DDBEDBFCA99FAC,
	DistanceGrabberSample_set_UseSpherecast_mE18E95F373903F263003BD244ED69D60E4F17C74,
	DistanceGrabberSample_get_AllowGrabThroughWalls_m0E2C0AE5522B8066A49EE17D4ED8901796CEB3F2,
	DistanceGrabberSample_set_AllowGrabThroughWalls_m47024AC8A8622EB0582E9FBBD92AFAE9FDC70186,
	DistanceGrabberSample_Start_m17A8BC2B0CD5CA1D6A19E5AFAA95B07239A6EFDC,
	DistanceGrabberSample_ToggleSphereCasting_mA150837732BE9CA29DB37AAA3D21D0956F198488,
	DistanceGrabberSample_ToggleGrabThroughWalls_mCEBEA385FE68A84650D3849E5B20D729FE663A74,
	DistanceGrabberSample__ctor_m91EB15189BB6C76FE3D70960D66F80C3F474CE39,
	ControllerBoxController_Awake_m10CE6D278450B3090C9F6FEBE087CA23CC9FFD8F,
	ControllerBoxController_StartStopStateChanged_mD500AA96C8E03F97F611D5EA54F954C933753451,
	ControllerBoxController_DecreaseSpeedStateChanged_m3229EACD0C33045DC2CB25A0BF6120F0838F0EE0,
	ControllerBoxController_IncreaseSpeedStateChanged_mEFE12568E8F140F770F50AF38395E0DB22C9788B,
	ControllerBoxController_SmokeButtonStateChanged_m886E38B2844CF61820F76E9724FDCCA2C61CA372,
	ControllerBoxController_WhistleButtonStateChanged_m28F2550607F6321A43FFB7891295EBBCCC90179A,
	ControllerBoxController_ReverseButtonStateChanged_m8183F155BD2B5E5F76680E51DA08B88B315AE0FA,
	ControllerBoxController_SwitchVisualization_m5D7DB78DF80500F6C712B626B834CEC6BD2D59D0,
	ControllerBoxController_GoMoo_mFBEDA2C31525584CD95F0C59E73B5A10E9FCD509,
	ControllerBoxController__ctor_mE2B5C31F1DA7DC4A076BB4715E72787A1FB3705C,
	CowController_Start_mC4B6534879CC6708AFC5D442E5282DFE915173D7,
	CowController_PlayMooSound_m6EC41C83F14928C33AAB8C96D1DB6F10E70B5104,
	CowController_GoMooCowGo_m0F2CD8AAFA84A5E77222026981CE7BE734090D96,
	CowController__ctor_m4EC07EAB37300BBD79EDBAA4073CB736455CBF28,
	PanelHMDFollower_Awake_m2CF549A1B7AD1994E135D0C3A64B3AAE4BE1C91C,
	PanelHMDFollower_Update_m78849650838D5B71CC1A8AF283E9A26DF5E78E17,
	PanelHMDFollower_CalculateIdealAnchorPosition_m74F280D8BC9291C194EB4FC9817A4150ABC944E2,
	PanelHMDFollower_LerpToHMD_mD8B9F448F88D9F598368D4B930DFA7BC19A843BB,
	PanelHMDFollower__ctor_mDFD1C555C729E0009E9FC989048FA5806267EC72,
	U3CLerpToHMDU3Ed__13__ctor_m2718D92561980DA3B2F04C2D102AB928155D0AAA,
	U3CLerpToHMDU3Ed__13_System_IDisposable_Dispose_m6F79FD9B76A0633FBC96538B385DB07AF57C90DC,
	U3CLerpToHMDU3Ed__13_MoveNext_m9C0F7AA018E489420080E85A9C327248E2301DC4,
	U3CLerpToHMDU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD58D03CC073A72DDD7B0EC4188CB4614E92A68AF,
	U3CLerpToHMDU3Ed__13_System_Collections_IEnumerator_Reset_mA09722794732F819AA5D31CB2AF5BCB0C2FD817A,
	U3CLerpToHMDU3Ed__13_System_Collections_IEnumerator_get_Current_mF39A3910A6D633D8600FF5B154BC7B6764772E9C,
	SelectionCylinder_get_CurrSelectionState_mEEA0EACE12CE6A5F1BDAA7E59A7EC5D77D56FAEA,
	SelectionCylinder_set_CurrSelectionState_m960B75182FF50885FD248F74A05F7D33BA3D67D3,
	SelectionCylinder_Awake_m3C9863696F1825751F817545A99016F0840CC0EF,
	SelectionCylinder_OnDestroy_mB8F6CF93EB637C30627D4851B5F0290D0D730A49,
	SelectionCylinder_AffectSelectionColor_m98DE925B474211ACCCCB6085DDB3A90399E4DCA3,
	SelectionCylinder__ctor_m19D204E84FD5989ECFE8FBF1480C7FCF758CFE92,
	SelectionCylinder__cctor_m27E9FE6C9906FDF3BE47F840DE95C67921BA60D2,
	TrackSegment_get_StartDistance_m7754F5C2A4BE8A74DB09CE3CED8D2598630B0D7B,
	TrackSegment_set_StartDistance_mCAF42BD5254AFCFFE3001A6E20070939A6F20195,
	TrackSegment_get_GridSize_m3D99EF429B98F214239CA9CA54A076BBB7FE92E3,
	TrackSegment_set_GridSize_m9823CF02AE0029655CBEAFF47200B88A76BD2E85,
	TrackSegment_get_SubDivCount_mE962363663526F01DC7D77F360A5E97A5E8D333E,
	TrackSegment_set_SubDivCount_m208543971918F3C42B26627EB3BFC845A034B19A,
	TrackSegment_get_Type_mB16F5DB9C4FD3AF8FA91CF5880D5DD48AA8681FF,
	TrackSegment_get_EndPose_m0F2F98AAF1A5C6C067720CA8067C3FD47D43876A,
	TrackSegment_get_Radius_mB458D02292A84615CA9BFEA5F03C1059268D6BE6,
	TrackSegment_setGridSize_mC74C3F3285B5ABD033CF68BDB0770DE56F16E990,
	TrackSegment_get_SegmentLength_m63FFFA4AD5055C552C805872637BC6839AC55F39,
	TrackSegment_Awake_m9650E6E117E2253990A03F82515ED14E990DDA97,
	TrackSegment_UpdatePose_m97FBB83914AB8D3CB49D9D85467DEB7431248B53,
	TrackSegment_Update_m108D239B881A8A7A1729E74C2EF814E2376D5DB5,
	TrackSegment_OnDisable_mB9B6EB483265C1B1BCF980EA6F58ED15D06ABB0A,
	TrackSegment_DrawDebugLines_m37FEF6E5D1842508645C8F8FEB28BABDA03D7253,
	TrackSegment_RegenerateTrackAndMesh_m4841B12BDC519C7862B4901565B44A9B6787AD5C,
	TrackSegment__ctor_mC6FA53B0963D2B395BCE338C1EC39985B28C4F96,
	TrainButtonVisualController_Awake_m84F6745B2D689E89F2AAF1ECA67B2FC59FEC4F14,
	TrainButtonVisualController_OnDestroy_m297A762E98F73EF8414F095856CE387C438D1798,
	TrainButtonVisualController_OnEnable_m0564F0AD8EDBCF9FAFE21A3D15EB7CCC7C65DE97,
	TrainButtonVisualController_OnDisable_m24A294D4F2E5C301D05DE52F29EDA4400EC831C5,
	TrainButtonVisualController_ActionOrInContactZoneStayEvent_m7AF8B0FD00D7F603EC903094DC343CC42BF7ABD0,
	TrainButtonVisualController_InteractableStateChanged_mCF09476B0142274EAFE2C5ADE762C28633BD23EF,
	TrainButtonVisualController_PlaySound_m65FC0A9207B130533020BEA24C3F291415B02249,
	TrainButtonVisualController_StopResetLerping_m9ADCE7C308758261CC14032402BEED4F21EE4B4B,
	TrainButtonVisualController_LerpToOldPosition_m95E0582BA249685039EEA46A3AD1BDD57CCE3B68,
	TrainButtonVisualController_ResetPosition_m9232F6767751D6404FFFCB0944D9FE7926F02B0B,
	TrainButtonVisualController__ctor_m67AA3D848961906B2E5F20150B0C82462B7E190F,
	U3CResetPositionU3Ed__26__ctor_m5635000743474AB03A282A54DAA034BE871944D8,
	U3CResetPositionU3Ed__26_System_IDisposable_Dispose_mFD4D0FB62BBFDC93E0DEAD7EA68430B7A4ACAF69,
	U3CResetPositionU3Ed__26_MoveNext_m5E72132EBEF7DB0236E6E8EBD6632A16E0F748E0,
	U3CResetPositionU3Ed__26_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2BE3FF62F4373A00DD3E8BBCBB2036F6761E547B,
	U3CResetPositionU3Ed__26_System_Collections_IEnumerator_Reset_mFAB28539B7B0AF0B783CD738C41D9DC51A1E8853,
	U3CResetPositionU3Ed__26_System_Collections_IEnumerator_get_Current_m84A8CF3B9E09A2B224C5C4969FBB701BB9E8FF7B,
	TrainCar_get_DistanceBehindParentScaled_m373B41D22495598A3431F0C5FC5017D442208A2F,
	TrainCar_Awake_m05D4446FBB4ADAF94AECC0466BBF94055909C8F0,
	TrainCar_UpdatePosition_m338A73EAC14EC679C9F5E00DEED605CEA2BDE432,
	TrainCar__ctor_m086A9601B0ADD3B4FD4118437F6DED5539A25486,
	TrainCarBase_get_Distance_mA33D6D4848DFD0820D2C75F9E1F2A76DC755E8A6,
	TrainCarBase_set_Distance_m4C3DF32DF27C6B14AB536FA6A983A8AD056B55CB,
	TrainCarBase_get_Scale_m5DBF114FDC5DBE97AD3C5BC2712624BBADF3F5D5,
	TrainCarBase_set_Scale_mAF09262F53F500D9ED7DA7829847172C5410ED61,
	TrainCarBase_Awake_m37A702A4A3B24463E19C126F64F2D89F11A9A502,
	TrainCarBase_UpdatePose_m0A67C10B69FB1D9A986E883B1CDC382B571085BE,
	TrainCarBase_UpdateCarPosition_m2B2E8799969C239937C47D4615D2101E6F9133A1,
	TrainCarBase_RotateCarWheels_m965650C8E46E761835CC060092CB6F4B69252858,
	NULL,
	TrainCarBase__ctor_mF505ADDC2083B90F7A3F45C89C32025DD0002ED8,
	TrainCarBase__cctor_m9E6D5CCA7F5D3E7F7A06FFF301303C946A0EFCB4,
	TrainCrossingController_Awake_mC8448F974906D7A72F4FB52C18CC6B00887E120A,
	TrainCrossingController_OnDestroy_m5913750A3AF404FB4E8FC54EA209A020A231A5D4,
	TrainCrossingController_CrossingButtonStateChanged_mE31580E5F7428418ECBE6FF15AB6BE20D73D998D,
	TrainCrossingController_Update_m580817DC6CF5C8E2EB48A214F1F84EE51EFBB736,
	TrainCrossingController_ActivateTrainCrossing_m85B47BC1A24078D2E973B527337EEB61794E7849,
	TrainCrossingController_AnimateCrossing_m8AE7CFD19C0FF9120FA3040DFD02538E8A297C65,
	TrainCrossingController_AffectMaterials_m6401A1A3E5B6E85E11681CF60FA492BB4BA510FA,
	TrainCrossingController_ToggleLightObjects_m4B93FC82AC5CA62BC1F0755839F5AE2D3983C38C,
	TrainCrossingController__ctor_m0EA7B850A7E56F174B1E49FF0513392222383826,
	U3CAnimateCrossingU3Ed__15__ctor_mFAA4EB62DD8CF2E800A17FB70610D210D4572B2C,
	U3CAnimateCrossingU3Ed__15_System_IDisposable_Dispose_m15B4C628F2391E4765AAD7E5FBEB1ED37163CF6C,
	U3CAnimateCrossingU3Ed__15_MoveNext_m9E22324ADCB019B88C3415E7B1087C2E5E277572,
	U3CAnimateCrossingU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2E5821A766805B7DA85AF5CDF5B3E79C9007A97D,
	U3CAnimateCrossingU3Ed__15_System_Collections_IEnumerator_Reset_m1221B7705FF6866AB85D415E8D00A92F1B40B0C3,
	U3CAnimateCrossingU3Ed__15_System_Collections_IEnumerator_get_Current_mDF8E56DBC547B33C024A5EB10750F3D8B075A412,
	TrainLocomotive_Start_m596B010A45C501F05A1BE9CE412138AB82498900,
	TrainLocomotive_Update_mC64296C895306B462D399E315CCFE6106979396A,
	TrainLocomotive_UpdatePosition_mF409B3053EC449F8F57AB7B971A9053C642C0D1A,
	TrainLocomotive_StartStopStateChanged_m37D2604A94869E9ABAE686C05C06DA8039D5EAFC,
	TrainLocomotive_StartStopTrain_m2983A1995E2F95A8642F03A8105C0B489F9C8037,
	TrainLocomotive_PlayEngineSound_mBBE7F6F9988F707FFC60EE195E18C7C7C37C05CE,
	TrainLocomotive_UpdateDistance_m18B8F2E7593CBA725B2A7B5034AF46279D35A321,
	TrainLocomotive_DecreaseSpeedStateChanged_m58EE2750F1CC037AB7C44A209C5D7A3D6FDCB9A1,
	TrainLocomotive_IncreaseSpeedStateChanged_m4DEB392A7FA443870D2A849A663EE6E3E740A22B,
	TrainLocomotive_UpdateSmokeEmissionBasedOnSpeed_mABC70306F1FBF3D00E0BA0901760D44793F9D3EF,
	TrainLocomotive_GetCurrentSmokeEmission_mCBF588B02E4B7CB77E4A4EEE98FA557C6C09A267,
	TrainLocomotive_SmokeButtonStateChanged_m2E6E404156BF780272ED69C4A2D7D07FF94E01EC,
	TrainLocomotive_WhistleButtonStateChanged_m2B8FF22070F53567B9DB0EF8F021411531CCF240,
	TrainLocomotive_ReverseButtonStateChanged_mDC7C10B6A595AABCF8351143917C1CDCFEA74A98,
	TrainLocomotive__ctor_mF249EAA95B7C51F5D9493892D960C0E8460AF909,
	U3CStartStopTrainU3Ed__34__ctor_m39BEC907A4658AB1DC65D214A86D35F6E246795A,
	U3CStartStopTrainU3Ed__34_System_IDisposable_Dispose_m6A0A898733F46F849ACEF0984B4C8938E5A88B20,
	U3CStartStopTrainU3Ed__34_MoveNext_m02D96704D3CA82F8BECFC12A487D9C4AC7903FA1,
	U3CStartStopTrainU3Ed__34_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m26D7E65A695D870236E43D9C2E2D46D88EA33065,
	U3CStartStopTrainU3Ed__34_System_Collections_IEnumerator_Reset_mE5B9500E4605106C99B8718D3105A15B0CB73A87,
	U3CStartStopTrainU3Ed__34_System_Collections_IEnumerator_get_Current_mEF6808D31C6E8D74E845C551B8618F3902FC469E,
	TrainTrack_get_TrackLength_m2527E507114E21D65C843667ECF4F42CBA3F7FCA,
	TrainTrack_set_TrackLength_m32ED334511B32330D895514C07B3BED3381F02E5,
	TrainTrack_Awake_mF48A851C3560D8D9ADA00466789E9571E49047B4,
	TrainTrack_GetSegment_m357BC8C004D29CAD4A3CA54E7896EB1A9C7F6B4D,
	TrainTrack_Regenerate_m69F9C3D51A88D5FC844056750E4B233C06B427D6,
	TrainTrack_SetScale_mA57F06F4C3421BB4EB73D3311A661210F8FE7416,
	TrainTrack__ctor_mB9A2233319F4A20CD3719E44B1A65AF41FA2CB38,
	Pose__ctor_mB4BBD3D8EA8C633539F804F43FB086FB99C168E2,
	Pose__ctor_m8C52CA7D6BF59F3F82C09B76750DE7AF776F06C5,
	WindmillBladesController_get_IsMoving_m5B2902A40908BBD84B5478EDA121ED00DBDECF56,
	WindmillBladesController_set_IsMoving_mAFFB89C8CAA4C34CA91560EE2188E80B3B66897A,
	WindmillBladesController_Start_m68D68EA86272066DDC385AF8B8FDC2307AE6D4CA,
	WindmillBladesController_Update_m6D72C558EA8D91B66F86A4B977F32B78C6A19C9A,
	WindmillBladesController_SetMoveState_m7E9FF42447DFDD4DC710B07309C4ECB6A9027B3F,
	WindmillBladesController_LerpToSpeed_mEC59BAB1ADEBDA2D4051ADE4DF879F255B80C50A,
	WindmillBladesController_PlaySoundDelayed_m5C9465C96F6157B1A20D4F7F5A7D4F307D10D0FA,
	WindmillBladesController_PlaySound_mBFB8F38297A3551225D2E57F4285B58E9EF1EA63,
	WindmillBladesController__ctor_m9ACCD7787AFD75BF05A2D1282EAC87C3E8EFAF50,
	U3CLerpToSpeedU3Ed__17__ctor_mC566935DE291006EA7DBEFE0365E202472FD9E7B,
	U3CLerpToSpeedU3Ed__17_System_IDisposable_Dispose_m47D668CCF11A5E20E41EE17981808A194A891934,
	U3CLerpToSpeedU3Ed__17_MoveNext_mEA106649270155724FF7B4F3557200EB0C46C649,
	U3CLerpToSpeedU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0BCE854267BF1FBEBD932AD293ECE426245E39C3,
	U3CLerpToSpeedU3Ed__17_System_Collections_IEnumerator_Reset_mDB45CD8EC190F294BD562D4090ADB533E5EBF9F7,
	U3CLerpToSpeedU3Ed__17_System_Collections_IEnumerator_get_Current_m81542027D8F2618D5B750EF7344E51128D3A6EA3,
	U3CPlaySoundDelayedU3Ed__18__ctor_m43BFCD0BBEAFA46AF3EC55B65A2F55BF43541166,
	U3CPlaySoundDelayedU3Ed__18_System_IDisposable_Dispose_m3978D8C006709205406E26F305AAD051EDA0CB61,
	U3CPlaySoundDelayedU3Ed__18_MoveNext_m5CE5E650B1870D226841E3D4EA3A269EC73C7493,
	U3CPlaySoundDelayedU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6DE8A8951E14A7AB0C941F512740278C4DF7841A,
	U3CPlaySoundDelayedU3Ed__18_System_Collections_IEnumerator_Reset_mD0C04D5F54AAD444CE77C15B853BE79F5504160F,
	U3CPlaySoundDelayedU3Ed__18_System_Collections_IEnumerator_get_Current_mC1F5FCA59AD560DFD85357BAC5AD0581AE393182,
	WindmillController_Awake_m819B04397F7D5A549DB64C972D9B35A0B54DCA77,
	WindmillController_OnEnable_mD0EBB3C707F79AB7CFB88EE7CE33355A24311BD0,
	WindmillController_OnDisable_mDAF50F89AB116A19F75AD91C088CF0B58F441365,
	WindmillController_StartStopStateChanged_m662296A93D8F31FC2608D8C3E89D1E3714B9561F,
	WindmillController_Update_mA8A7025A970E66000E92C858A97B07D1A73650C2,
	WindmillController__ctor_m2AE15DFF314EC3FE467821FB5F2EB1EE0F19EABC,
	OVROverlaySample_Start_m4266C95062A3A7F9DB99739F13F1767E8E135CDC,
	OVROverlaySample_Update_mE6B42EE1FD90945F73BB8A1E71C98D6FE45F8BAB,
	OVROverlaySample_ActivateWorldGeo_mE88CC5B6F95FF940DF64CC3E30A163299A0028AB,
	OVROverlaySample_ActivateOVROverlay_m0D8B1F2CE350FC1D6530827588176FED1E8B0E52,
	OVROverlaySample_ActivateNone_mA8A0AC683DA78AACC6DBB04E1BFCCAA41CC83D66,
	OVROverlaySample_TriggerLoad_mA4868A61FDAFE908F1178A852FAAC4DA4A614B42,
	OVROverlaySample_WaitforOVROverlay_mE2A11AA7618CAF92461A97CFFF9BF0163B14D463,
	OVROverlaySample_TriggerUnload_m6B1496A0AF1064FF3E131ED27D4BE41916FEAACA,
	OVROverlaySample_CameraAndRenderTargetSetup_mF5D8804C1E12AE7E92207ACE4AE38C9C4B12005B,
	OVROverlaySample_SimulateLevelLoad_m5792B7A4F133CB3538E9A9CF021C1EED354E47C4,
	OVROverlaySample_ClearObjects_m0B71842496007E962955E65310B2A4B15653E446,
	OVROverlaySample_RadioPressed_m67B72653B224FFE4E86C34D625F9B442D32BD19F,
	OVROverlaySample__ctor_mB44CEBA69BEAF1A6A58A099C565D834246303FEC,
	OVROverlaySample_U3CStartU3Eb__24_0_mBD67EE6633B4F08B23C48B5757BF037F011FA444,
	OVROverlaySample_U3CStartU3Eb__24_1_m9C707473B8AD3A3E9A2A5DE4CBB97FE7067691B9,
	OVROverlaySample_U3CStartU3Eb__24_2_m4676B4B0549094AF080DCCCC880399B388366734,
	U3CWaitforOVROverlayU3Ed__30__ctor_m2AB6E29E1D1F5472C16F001263DCA536B3140AB4,
	U3CWaitforOVROverlayU3Ed__30_System_IDisposable_Dispose_m1650460A7C7917454B04CB2BE659585291CBEA40,
	U3CWaitforOVROverlayU3Ed__30_MoveNext_mEC25CDDFCBF7B22EEB0CD01AE64AF805A2A11534,
	U3CWaitforOVROverlayU3Ed__30_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m615D0B2569205ABB59D00AC9BA50160C1554A6F2,
	U3CWaitforOVROverlayU3Ed__30_System_Collections_IEnumerator_Reset_mEEB99599B3CF4FC0F90F891156B2FDE7157003F3,
	U3CWaitforOVROverlayU3Ed__30_System_Collections_IEnumerator_get_Current_mD516520BDEAB4043256A81E49B7366A842120F09,
	Hand_Awake_m8481AE5F78D80F6D46DEB65B2B9D0182A56DF09F,
	Hand_Start_m9705EEDEA171DF9AF0D8E107F3E1D42EF11BC79B,
	Hand_OnDestroy_mD3C5A20383810964D0D12F41E40C185DE7AAA046,
	Hand_Update_mB561E1D8A8122B9C20C5C6A00B9A43F952A09F2A,
	Hand_UpdateCapTouchStates_mD04F599C0EB3A207AE420014E51B65DA66F5A49E,
	Hand_LateUpdate_mA7932BE84EA2D5A09A4637502E4E52CA4321B8E3,
	Hand_OnInputFocusLost_m8C6FE8A97BA23153CEFD5D8AB561781E43AB6E02,
	Hand_OnInputFocusAcquired_m57F1BE51C79CB9B267C1866F851C75E6B848FA1C,
	Hand_InputValueRateChange_m76AA4719D87E5D5C3BC14DBE98B145551F685784,
	Hand_UpdateAnimStates_m513AAE4141F73A2CF86B65C1E5C88804AC288D6E,
	Hand_CollisionEnable_m23E8B46BA537F57146805D86CE15089DB121554E,
	Hand__ctor_m7990A68D0B224E873A13E2ABFF28659D667951AE,
	U3CU3Ec__cctor_m3FFD2D2D03A3D619CC157C8E95E60BDA7529EF17,
	U3CU3Ec__ctor_m29502B502F048DD5DE6F011936D3ACF3E92D116E,
	U3CU3Ec_U3CStartU3Eb__28_0_m982333D324DA5B0E763CCC459B17E62E62201591,
	HandPose_get_AllowPointing_mC5B4C9CDBFCF4883AC67C1637C2D1FA5C5553D3D,
	HandPose_get_AllowThumbsUp_mAE1D1913C2FEE204F4A74A8E40135873C9DB6615,
	HandPose_get_PoseId_m676CB369677442E624811201F99B12E4F5BB8F8E,
	HandPose__ctor_mB1A903B2059D5FB6676ACBD06E475480BBC767C9,
	TouchController_Update_m19376C0269F927CC0CFD041646CEA78E2748D6C1,
	TouchController_OnInputFocusLost_m09A7F448B045C56328B5E92B8DE149BA7ADA065A,
	TouchController_OnInputFocusAcquired_m7B23CB3B2CAD8C70AE1D8711D16F06F3E9ADA4A9,
	TouchController__ctor_mBE4D0026D782C0768D1B286B617197245B28D9C6,
	AutoPoser_get_CollisionPose_m53A195A5D0B58CDB348FE0200B5416094DE61D5B,
	AutoPoser_get_CollisionDetected_m424853DE983C16E5CD7C404BDEC681D6BD381081,
	AutoPoser_Start_mD37529D47F7AA218149B6608A0D449D909C86917,
	AutoPoser_OnEnable_mE2DF53F8FAC08F26595885BABB62F43B9462F29B,
	AutoPoser_Update_mB85BAD41EB82143187912F99B454AD4AEE76BBA3,
	AutoPoser_UpdateAutoPose_mF7EC9331D7010E9283FB6C029819A034EFBBEC43,
	AutoPoser_UpdateAutoPoseOnce_m8638729C9E7321C8730951BC9BAA59AD3803BB53,
	AutoPoser_updateAutoPoseRoutine_m90FBA7BF00E27A385862143CF3E5E0195F3667C0,
	AutoPoser_GetAutoPose_m01AF7D57616463410EC9CC162F16312224087C83,
	AutoPoser_CopyHandDefinition_mD0FFB3774A76C0DD2AB4D59FB4AA5664194B6386,
	AutoPoser_GetJointCopy_m9B32137BD666D08178734AE797125BD8F042287B,
	AutoPoser_GetJointsCopy_m97D08F63064537F4C6E746380A79809710EB2A54,
	AutoPoser_GetThumbHit_mB2CD477DB4C47A67428D0A9D704A528567E25E10,
	AutoPoser_GetIndexHit_m345CDA7E370AD0DB7F70D525ED49C2913BE544EE,
	AutoPoser_GetMiddleHit_mC5793AAF881FC477BAE9EC36E39D1E064540B4B8,
	AutoPoser_GetRingHit_mED67C354CEAF74C80303807A833D0D7214138490,
	AutoPoser_GetPinkyHit_mDC40E961F3AB4C575BA2EA34E7F6E019CE5159B6,
	AutoPoser_LoopThroughJoints_mC5731F18648BD721E868E1082E3B5161CEECFFC1,
	AutoPoser_IsValidCollision_mE2562E3EFF79CE3DB93FFF23837B9C8EE156FD7A,
	AutoPoser_OnDrawGizmos_m91B3A9116E5346D42960108AEC8D05F354DC545D,
	AutoPoser_DrawJointGizmo_m0D40E5CAB74C54C8394AF1D1C6D06BB916F2873E,
	AutoPoser__ctor_m869B97BFF9C19D9EAA1F2032E9A56EC587C2B7A0,
	U3CupdateAutoPoseRoutineU3Ed__33__ctor_m6A2C843FB040DE88C7DC34B74B092A2A4D31DA49,
	U3CupdateAutoPoseRoutineU3Ed__33_System_IDisposable_Dispose_m2E0280435EEF17ADACF1570ABB3638E1B46FB85B,
	U3CupdateAutoPoseRoutineU3Ed__33_MoveNext_m53CCCC2CC2437DBCAB34374637472C5A40D79B67,
	U3CupdateAutoPoseRoutineU3Ed__33_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0CEAF8D9EBCE439B0014B0F69C4435CA7288AA62,
	U3CupdateAutoPoseRoutineU3Ed__33_System_Collections_IEnumerator_Reset_m6B038D82610F656F6095BD1DF732303C08126225,
	U3CupdateAutoPoseRoutineU3Ed__33_System_Collections_IEnumerator_get_Current_m83D4722A893B3775119B737FE11E6AFBAC6669B7,
	BoneMapping_Update_m62C65EDB90283A1A7946B299905631A28E29FE9E,
	BoneMapping_OnDrawGizmos_mD6CB298C3E743BF011CE2D1E15E7B64645FF607F,
	BoneMapping__ctor_m6B126A1C1CEBF09599FC858540461818BA5E8717,
	BoneObject__ctor_mDB4F39B60A78703F790F9F68E3DDC5EB925D7EA6,
	EditorHandle__ctor_mBC7223D07F9876479E16B5296CC15431884B1D4A,
	FingerJoint__ctor_m68BA194D4A79AEC18E06441DF6A013FC99D5812F,
	FingerTipCollider__ctor_mAD998E6EB83269EC40237F377F2857A4A76A8FE3,
	HandPose__ctor_mFEBC46B97C4BE59528B0929173223F6ED3BDB38A,
	HandPoseBlender_Start_mFD87E62D6E49CB82489517925312A8C7FB3AC479,
	HandPoseBlender_Update_m8A82E3CBC6D23931CAF57F9360C744BC18BB4B66,
	HandPoseBlender_UpdatePoseFromInputs_m59DE35BF96FBC0DBC3CDF90B9F48AEEDB6BAA8ED,
	HandPoseBlender_UpdateThumb_mCD192CF78255958BDCFABD83FAC43EF27345F4A5,
	HandPoseBlender_UpdateIndex_m0280E5CA85ADEFB422F58CDFD105098C523CFA3F,
	HandPoseBlender_UpdateMiddle_m0B4E71E3B771B6D5BFA3FE3DF01215DF8F6BA056,
	HandPoseBlender_UpdateRing_m85BD2A963EE9DFB486B94C1862F85F26DB7F5D01,
	HandPoseBlender_UpdatePinky_mD3D53BEB5388F660954882DC8FE5F307129A48D1,
	HandPoseBlender_UpdateGrip_m9DC2238312613CF966915B5325734E9966186977,
	HandPoseBlender_DoIdleBlendPose_m12E61753A8C6F1F3D3AA1E81501B86D206F7A70E,
	HandPoseBlender__ctor_mA4A84706694A17B391A75ED34090F5F97857F55B,
	HandPoseDefinition__ctor_m91C4BD177705AB3B7342B2BC748CD25ED9BF6335,
	HandPoser_get_HandPoseJoints_m2AEC7F46DF233F5DB97D87A577DC45B9F3A560C1,
	HandPoser_Start_m1083EE1A44356497AF777C1BA1EC020C9F462F5D,
	HandPoser_Update_mE51880905F74E541D4F96057488E2C424E74BDF1,
	HandPoser_CheckForPoseChange_mC3F6615E1D5C6808E57C194B32E20FBB97D68491,
	HandPoser_OnPoseChanged_mA37AA139B5B13CBA4566A91D81379F98527228AA,
	HandPoser_GetWristJoint_m3BB33DBEA69E743E2D91EA1A1D168EEB4D40A8D1,
	HandPoser_GetThumbJoints_mD3425A5A39218F3B48C7570827E6318AEE9F341C,
	HandPoser_GetIndexJoints_m081CD820EEB398FBE020B20480CE9BD290F7C685,
	HandPoser_GetMiddleJoints_m22619BAC6787D560B491648811A1D96252C9FFED,
	HandPoser_GetRingJoints_m16FAEFD13CC19EB7081B2333E7D8A9BD89580D37,
	HandPoser_GetPinkyJoints_mA86965AC39E8C5D454E0CC16CA25AE635A32B4A5,
	HandPoser_GetOtherJoints_mB512FF86BE2D716A0C4ABCE4D96AEA91B881E5D8,
	HandPoser_GetTotalJointsCount_m5F05599245BEAB393CC583AD84D3DC25BBA6BDF6,
	HandPoser_GetTip_mD2AD11303DFA081C63E2645E360D5B89A1A6DB95,
	HandPoser_GetThumbTip_mCE3E46AAA429DAD4C18F4D2AFC097F961011E8E1,
	HandPoser_GetIndexTip_m8DC3E563AA6BE09B3AA97626763200B4163D7772,
	HandPoser_GetMiddleTip_m8E9252264A2E11DFF24122B0C051B3E45CCD32B9,
	HandPoser_GetRingTip_mC76ACCEDE23BC94AB7A90F8D5F37AF1F039102B6,
	HandPoser_GetPinkyTip_m465FB6E50470882B3CFEC563B949E500A8C99CB8,
	HandPoser_GetFingerTipPositionWithOffset_m3080C5F3083D5BF593FA9CEF4ED4392D72BE9BBD,
	HandPoser_GetJointsFromTransforms_m4B9816B8ED419215CB5B5A6407452C1F886DE510,
	HandPoser_GetJointFromTransform_m83459A9B11F903C94FB38FE578ACEF667317525B,
	HandPoser_UpdateHandPose_m5E9BDBA4F0600FF48D962B0B4B5C493EDDEED4FB,
	HandPoser_UpdateHandPose_m64038681DB9E103EC8D6538EB1AD06A891C0344F,
	HandPoser_UpdateJoint_m4E436DBA91076C507B1263CA01EEF8D0FA72605A,
	HandPoser_UpdateJoint_mD59EFF395E575925A85D779F282D02BAA13B15BB,
	HandPoser_UpdateJoints_mA2D418B4617B2414FA30604A8BEDA1A82F02F188,
	HandPoser_UpdateJoints_mD32B654D3CCBF48FCE706D4318BD46E1A02D81A2,
	HandPoser_GetHandPoseDefinition_m998DB64ED81202AF9B60722E2A5C27395BE22D07,
	HandPoser_SavePoseAsScriptablObject_mF758F6A24C4F214774C1BC6CC1DB054A6D22B5D7,
	HandPoser_CreateUniquePose_mCDDA53EB42E6F945764B472EFE4FF69B5C6A8B07,
	HandPoser_GetHandPoseScriptableObject_mD9B6EB6AC2AE98C46EEC699481010702EA01AAAE,
	HandPoser_DoPoseUpdate_mD07B38F910DBA27226017B6410D5697D31C028D2,
	HandPoser_ResetEditorHandles_m9CD0A2A5AAE9BE066B26E65ED38621A7C658B2F5,
	HandPoser_OnDrawGizmos_mFAECD5070B7D5C96302B2A505384B85B236B7B3C,
	HandPoser__ctor_m7AC615054284FACA9252F9D75E3252375E6C6254,
	PoseableObject__ctor_mA9A89DA02C7AA1D38EC7D301C7F483F74FF20338,
	SampleHandController_Start_m2B5985694869C5081D8581218E8B35F434062903,
	SampleHandController_Update_m33CA5B88637EAF7ED8B6EAD4C96B268ACBC3A4D8,
	SampleHandController_DoHandControllerUpdate_m244DD8CA7C49574918D2A374322432719188CEC5,
	SampleHandController_SetCurrentlyHeldObject_m96212353322C65C6B8E3C0D8B6DED53F61B89AE6,
	SampleHandController_ClearCurrentlyHeldObject_m3C85CDA0C04F3A2C79CA4FC35127E652183CB65F,
	SampleHandController_ResetToIdleComponents_m1634EAC6DC9B72E3C58972A09BDBDC36980F94EA,
	SampleHandController_UpdateFingerInputs_m9B554A20FE3A026CDF865D2EEF41E1D7BE7E86B0,
	SampleHandController_DoHeldItemPose_m00DF00DD6B8E8BE72FDEF10B84AF26D4504EDB2C,
	SampleHandController_DisableContinousAutoPose_m284A4361441A1B6DB379BB4FC13A2426DCC5F3EC,
	SampleHandController_DoIdlePose_mEC7ECA71D1A28CFAD86CCC19F8E6DF0AB9BADB9F,
	SampleHandController_HoldingObject_mDF42B5805BAF61B220CEE3343184CC26A3C3A4E5,
	SampleHandController_correctValue_mABC1B5B250BA7ADABCBA5996C6CBEBCF3D7B5C06,
	SampleHandController_UpdateXRDevices_mDE680814E4721D1BA36BFB81C0140477F164DD4A,
	SampleHandController_getFeatureUsage_m523F7E748928E93D9E7421241B10BBEDAD8D34B4,
	SampleHandController_getFeatureUsage_mC75FE44A601F3469D82D5FBF9647EEC44DBC20FA,
	SampleHandController_GetLeftController_m51E0B51C2D8DB22740585B5B8A2CF57B17BDDE58,
	SampleHandController_GetRightController_mB1265E5B8005D8E30B7169E0E34B9762626C2806,
	SampleHandController_GetThumbIsNear_m312D69816A3A974FE9026A97997FDE9BC7AED500,
	SampleHandController_GetIndexIsNear_m3D1031451BAD7186ABE82E3028B53616934D7AB5,
	SampleHandController__ctor_m3844E5E50DC2C3F20527154F7C761EAFD0C515FC,
	SampleHandController__cctor_m8811E6DA2E421377D63D1E3100C361351839F413,
	SavePoseBinding_Start_m60D6F61483BCDCCE44F918E5361D9D06E268E9DA,
	SavePoseBinding_Update_m3634087E4371BCEDF46A606ED16A5152ABF26E5B,
	SavePoseBinding_OnGUI_m159E4108E69DE35C9F5431C6B0A6B93D7003E39F,
	SavePoseBinding__ctor_mC93F70B13598A47A8322A5C29DF54602A3B8CE6D,
	SkeletonVisualizer_OnApplicationQuit_mC23E40BDCB8522A0D3BCFF178324C99E55FD9ABB,
	SkeletonVisualizer_OnDestroy_mB1D7B5369871A5FE34984D164633168017D02845,
	SkeletonVisualizer_IsTipOfBone_m5BBBD86531D02365A975283B7FBC0AFFCD2C5597,
	SkeletonVisualizer_ResetEditorHandles_m3C997F258F70985BE0C5F9EA6C9DC1FA8596A11C,
	SkeletonVisualizer__ctor_mFB3C1F4FA841DE31D9740E6E663689E4FECAAD21,
	XRTrackedPoseDriver_Awake_mB79236506A2F6FD4038CF3F598A4C93C59BBFD27,
	XRTrackedPoseDriver_OnEnable_m2E9B51B20C74B145AA3E021FF81A6D37A27B8877,
	XRTrackedPoseDriver_OnDisable_m38BD85CA98BF0E6CEC0DAD2508DA8FD85A69BF29,
	XRTrackedPoseDriver_Update_mB724466A22AC89CDBF441E41340D1DFC37A90571,
	XRTrackedPoseDriver_FixedUpdate_mF122848E5ACD326B80147F3E22B89AE3E0B6A279,
	XRTrackedPoseDriver_RefreshDeviceStatus_mCCC33BF7DF6AA8977EBEDFEB0943E11B0136D446,
	XRTrackedPoseDriver_UpdateDevice_m9F23BA7BB23C4DF479BB93FD4D002AF5B3A92D6F,
	XRTrackedPoseDriver_OnBeforeRender_mE50AF2B475304742DFDF1F8EC592385A30C78659,
	XRTrackedPoseDriver_GetHMDLocalPosition_mE27D6B72554702A2127CC7024C5B7D18FEB77348,
	XRTrackedPoseDriver_GetHMD_m757BCEC47CFADCB44D8A474AD272B8BDAABB2F5C,
	XRTrackedPoseDriver_GetHMDLocalRotation_m10CBED20A4260441009477F4D85B793C739F94E9,
	XRTrackedPoseDriver_GetControllerLocalPosition_m7D0BF72D4CB8AA1EDE60BCE21F9829CE755249D3,
	XRTrackedPoseDriver_GetControllerLocalRotation_mB9749BB06C958A09B8E431598A43F6BDF72E8E04,
	XRTrackedPoseDriver_GetLeftController_mD7299D053D3928D0E46B71C98BC74DA556B2FC0B,
	XRTrackedPoseDriver_GetRightController_m844AD552503ECA2A110C745A7A6EE629726279DE,
	XRTrackedPoseDriver__ctor_m2E90E323D9C680556139032452BC3A130942656A,
	XRTrackedPoseDriver__cctor_m2F3F3F39D80091F4AF3EE180E057DC3C54C002F4,
	DemoCube_Start_m3EC0EB024C86C36241890E74D3DF80047466B60E,
	DemoCube_SetActive_mC5BE3B6872AF9C5808725F1B9A255B77A9811000,
	DemoCube_SetInactive_m329831D29C91F5570B6B19A1B9962C25E8925624,
	DemoCube_SetHovering_mBF79A1662019DAD48F8D319E9CA633AFCBB81BA0,
	DemoCube_ResetHovering_m0E31632A067838EBEA2377F92E08F426E9040B75,
	DemoCube_UpdateMaterial_mD60E030787D48A5C0E828D38B7F1AF614049B784,
	DemoCube__ctor_mBD6836260A62681F86D09B27D91EEC93B8EA4453,
	DemoScript_Start_mF8A0FE9B6D0FE12D489E178E0A6BBB7E78EC9BBD,
	DemoScript_Update_m68E4F2B56DA947A9ED1256E60941E08F21BB41A4,
	DemoScript_UpdateSliderText_m7B4B817E95F2162ED2104107CDB45BDE41A1C520,
	DemoScript_UpdateJoystickText_m657EEFE3D00E47F471463E27D2CEF810C4ED2F63,
	DemoScript_ResetGrabbables_m106266FA28E995ADFC12D504E508780AE1A0C3A4,
	DemoScript_GrabAmmo_mFCBF9A175A6E123FE4623BA704130B5A03B79771,
	DemoScript_ShootLauncher_mC15C73FD554C8179674C0B30C96F62EEEF0227ED,
	DemoScript_initGravityCubes_mB8070B527D3D593438D6A7C4C553A4A36B6F423A,
	DemoScript_rotateGravityCubes_m8EF9633B519781B6E4D942FECCF86C5DD3F1D8CE,
	DemoScript__ctor_m21F662BCB229EE6E67FEE09498C8EF333F9B9E2C,
	PosRot__ctor_mC103513C67D370DB2B0B5A1202AC94E57FDA8BEE,
	CharacterConstraint_Awake_mA1569C6CBFBAA9D5207D51A5EF719A67EF9A9A49,
	CharacterConstraint_Update_m8CC91407767950F4DC267028A9FE95BAA751BDC2,
	CharacterConstraint_CheckCharacterCollisionMove_mA80535E48514184E9528B063171EDCC68E6D96A0,
	CharacterConstraint__ctor_m97A50EBE5A3C6D59C8A919A6D51770E8B1B0796C,
	CharacterIK_Start_mB6F97291EBFF9F4733570D863444018D6589AF79,
	CharacterIK_Update_mB89C04DFB842CAF69CB387B57CEC6AD5B82A7C63,
	CharacterIK_OnAnimatorIK_mDEB6E20E5AFE09D800CC885DEB281991FD52789D,
	CharacterIK__ctor_m9924ECD9D858F11214678E4DFA176BC5A8F70ADC,
	CharacterYOffset_LateUpdate_mAD6C1F1F57659D702C731779E02CE9A9C6D3F1F9,
	CharacterYOffset__ctor_m4E137DD98CB306E3F3E95CDE7B554E9F44D04830,
	Climbable_Start_mED801A7C6ABC922FA53B18E81354A22E59FBEB6A,
	Climbable_GrabItem_mE7947126527CA589FB2204B651493EB298BF2139,
	Climbable_DropItem_m984FCD4FE2B24743A98B7D2F74ED3E84E56E195D,
	Climbable__ctor_m3B0477518B343FD8FB7784422E72722FD7C7A9C6,
	CollisionSound_Start_m71E4A80B529DA11855634235D15348E95DECF7A4,
	CollisionSound_OnCollisionEnter_mE68DC6020040D49D78CBAEBF90FF0060E7AC9BD3,
	CollisionSound_resetLastPlayedSound_m01EAB435EB3CB6429152143288BEBDC75A496144,
	CollisionSound__ctor_mFE73000BCAD2274F3A14635B07CAFB995EE804F9,
	ConstrainLocalPosition_Update_m1A34BDD9CA246E3145812045D75175C52DE999A3,
	ConstrainLocalPosition_doConstrain_mD01D66C5B2B24B029A6C5A7333349F2309D31638,
	ConstrainLocalPosition__ctor_mFDBC14EFD8ACD77117584FEB4146A3F935666848,
	DamageCollider_Start_mF6C8CCA5727CC018D54C6C8803AA60BC14542ACE,
	DamageCollider_OnCollisionEnter_mBA2C03ED77D1AD9076A5DF91E6E9629D98005D18,
	DamageCollider_OnCollisionEvent_m4BD7D7EB831A9D89A15B78780A32C7A1D9C2FF3D,
	DamageCollider__ctor_m24CC62C0185DED3F623D24E33DC1DD18C2982ACE,
	Damageable_Start_m04F8440585F350F7FBD0924D41581812ED1CA917,
	Damageable_DealDamage_m593470335966F15B7859AF34C75309FCFB7B6CB7,
	Damageable_DealDamage_m146215804E39668D8A4F7E3DE888987585024CC5,
	Damageable_DestroyThis_m1D13204E94B1AD6C73330B0A5B263F52CD6EA3D5,
	Damageable_RespawnRoutine_m98438AD3620986BB4C12670BEEC45A74EC698460,
	Damageable__ctor_m92C2FD92ECAD10D6D1F0E8CC24288D42DC9419E0,
	U3CRespawnRoutineU3Ed__22__ctor_m69D6950294EA9F9B77E24A393EA2074A3CF2628B,
	U3CRespawnRoutineU3Ed__22_System_IDisposable_Dispose_m9D7014422AC615050DD5938A4AEA282697036F6C,
	U3CRespawnRoutineU3Ed__22_MoveNext_mCCA13A1167CF403D99830E0A4981B201FF825389,
	U3CRespawnRoutineU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBF3FF1461C49FFDA560C68881CC0EEAF96C56AC5,
	U3CRespawnRoutineU3Ed__22_System_Collections_IEnumerator_Reset_mA73787DCBE03B96D4D07F7308F58141D08F94584,
	U3CRespawnRoutineU3Ed__22_System_Collections_IEnumerator_get_Current_m68AA77D0D5D6FC65A5B0E20A2014AEBC21DE8BF9,
	DestroyObjectWithDelay_Start_m4A7346D68C69344DBCE38CC27D18A44AA7BCBC0A,
	DestroyObjectWithDelay__ctor_m6B398C72F359B69BD9D0B1D136E5BE0CB6141715,
	FollowRigidbody_Start_m081B54F1C99F50853E84456E26D75F5C45392406,
	FollowRigidbody_FixedUpdate_m883D623857B87205C897DE0C3BFDBE528698BD23,
	FollowRigidbody__ctor_mDA874184052CE9E0FBB45D88373A9215DB6219B9,
	FollowTransform_Update_mEC8166ADAA77B379C0A961A4C5E9ADDE24D2FBBE,
	FollowTransform__ctor_m7A5BF699BF9182038B2A19E33F3052A364F686E9,
	GrabAction_OnGrab_m4736BBCA8BA58260EAB54A747A6DD7A2CBC25354,
	GrabAction__ctor_mC752DE22A025AA8F8B47B9F607D2382FEDF35E7F,
	GrabPointTrigger_Start_m37085176E4C9E8EBF4240CAA70E549D47BA73990,
	GrabPointTrigger_Update_mD1EC23573E0105BF9651C4A5BCA8F64895BB3A98,
	GrabPointTrigger_UpdateGrabPoint_mDB3C96865A32D562B4AFE160D5112D0210CBD305,
	GrabPointTrigger_OnTriggerEnter_m57E8927B35E49EEBE44736C85F4CBB83C99D74E2,
	GrabPointTrigger_OnTriggerExit_m718E4B5D37B5B306A10CB75FCC13DE86D3FBD494,
	GrabPointTrigger_setGrabber_mB6638561BA91E6A6B2FD4BDB8E9777968728E29A,
	GrabPointTrigger_ReleaseGrabber_m49FE5DCCD05C8A5876A403080051EE54E5639CCB,
	GrabPointTrigger__ctor_m96EB1E8B764FE13D879AA6DBAAB36BA561DBAF3E,
	GrabbableBezierLine_Start_m7EC00D491AE67F0920D5316885544CDBF4312D90,
	GrabbableBezierLine_LateUpdate_m8CB9FFD91C5A77E22A70714B49867BD5A2B249E7,
	GrabbableBezierLine_OnGrab_m79E6D087F682410489B7870D1420883A2D4F057E,
	GrabbableBezierLine_OnBecomesClosestGrabbable_m7C2195D08CE1A694FD937E56391939A87114366D,
	GrabbableBezierLine_OnNoLongerClosestGrabbable_m7557F4CBD127E1157F2B3B1066BD6B31E9CEDBF6,
	GrabbableBezierLine_OnBecomesClosestRemoteGrabbable_mA2E214808E60C880AB6C54E5F10FC0B6B1B599DC,
	GrabbableBezierLine_OnNoLongerClosestRemoteGrabbable_m7512D9177AC9151074B2E822974277702D0E1B3D,
	GrabbableBezierLine_HighlightItem_m1514F2D4658654AB5779CA2FB8AC8B1F823C2047,
	GrabbableBezierLine_UnhighlightItem_mD70696C561995F53F7A8302D7FFD70207B399FB7,
	GrabbableBezierLine_DrawBezierCurve_mA29E3067EB5FA28E82D53C4EE6D0EEB49D7764E3,
	GrabbableBezierLine__ctor_m7507DB9985C1EBD8F0CB4188EA79A54C2B3AB808,
	GrabbableHaptics_OnGrab_m92492E705873ADC68B655725E91476DD99F147D1,
	GrabbableHaptics_OnRelease_m2FC8080FBA3EBB59F56AEB13C84F7514A0634A69,
	GrabbableHaptics_OnBecomesClosestGrabbable_m20CE75F567E84F0B0DCC5DF2B6B50F2D4CC69F20,
	GrabbableHaptics_OnBecomesClosestRemoteGrabbable_mD66B08DC083C2875BA7A02674CC5134E48391C3A,
	GrabbableHaptics_doHaptics_mB18D35108539CE8F4A49737AFAB4E03397A1300D,
	GrabbableHaptics_OnCollisionEnter_mA3D1120EAD34EF8F651A7F4A0993287ED685A4AB,
	GrabbableHaptics__ctor_m3D075B87B86958ACF2BB3B8AE7931105D33857D8,
	GrabbableHighlight_OnGrab_m3D17F5360A135194ABE9690CA87F7BC7FE7DC59C,
	GrabbableHighlight_OnBecomesClosestGrabbable_mB8AC55A5D790B1D60406EA80ABC31193D6C0DB97,
	GrabbableHighlight_OnNoLongerClosestGrabbable_mA4147768E1583D699E3BFFE521BA90A385E5C491,
	GrabbableHighlight_OnBecomesClosestRemoteGrabbable_mAD422A92E1CE0CF3E025F070E8C9997F3D776ECC,
	GrabbableHighlight_OnNoLongerClosestRemoteGrabbable_mC3A601636A7233FA0AA808981D7544541FD6A157,
	GrabbableHighlight_HighlightItem_mF4B06B58B25E513913451A034A1CF731DBE5F944,
	GrabbableHighlight_UnhighlightItem_m174D6685B68657F06EFBD73ADD03D05CA8CE1A4D,
	GrabbableHighlight__ctor_mEB46B0AB3C512F9BB4362431632A2B7325088479,
	GrabbableHighlightMaterial_Start_mDC15A925FF8FDBCFD3C14A6A29E9ACE1100D3E49,
	GrabbableHighlightMaterial_OnGrab_mF579CB2DFBADCAA6EF3CEB7995FB4EF6EA027347,
	GrabbableHighlightMaterial_OnBecomesClosestGrabbable_mE6077DD4E01F9D52DA628FA461CCBAFA7A8D141F,
	GrabbableHighlightMaterial_OnNoLongerClosestGrabbable_mFF1FF0294C9787422634F0B44DB72AD68193E21D,
	GrabbableHighlightMaterial_OnBecomesClosestRemoteGrabbable_m8CE0FEF90F81E0EF07A4845B9C741BC58BD848C6,
	GrabbableHighlightMaterial_OnNoLongerClosestRemoteGrabbable_m68165125C1A1CBFE1C3311C97AD71B354C7298A3,
	GrabbableHighlightMaterial_HighlightItem_mAA5E04BD58CCDB777E898A3AC4AC6C519C210349,
	GrabbableHighlightMaterial_UnhighlightItem_m323C4E10B73594A184A33F67458A3E83CE2793F3,
	GrabbableHighlightMaterial__ctor_m4F7F3C30F2E141F3B49718EC7D27FF2CC31CB8FD,
	GrabbableRingHelper_Start_mDA24D827B24AD9D6CBF0BCABE8F75E1FDBCD74BB,
	GrabbableRingHelper__ctor_m1BE13978BA6C8511B044592C03E4EFED58076E3C,
	LineToTransform_Start_m763B49E6F81141D1C9097DEB9981638805421F2E,
	LineToTransform_LateUpdate_mFC88A01A1625435C484DA068F0DF2384DB7FC44C,
	LineToTransform_UpdateLine_m15A38E03378250BE16A0B51F1A59CD2B36FD0075,
	LineToTransform_OnDrawGizmos_m0BA6CDBE9A5AB2F0C0B429B6D35534F0B9ACF81C,
	LineToTransform__ctor_m84950AA66CE67E99203CE0B489CC503C6A945FDE,
	LookAtTransform_Update_mA6C604FE7F4EC8966F813AA16755925AD7440AAE,
	LookAtTransform_LateUpdate_mD810095C360DD2CBD9D1C842C20C64A998724E39,
	LookAtTransform_lookAt_m902A726AD4D12BA6E0F853B5950E90347DCB457B,
	LookAtTransform__ctor_mFADC1FE351A684889BD707EA8DD62C8BF0944427,
	PlaySoundOnGrab_OnGrab_m939FA1E32C71E78B3878C32DA71BBC22A3EC5D91,
	PlaySoundOnGrab__ctor_m099DE8154FD7B6A3948278F257C3078D33B08FAD,
	PunctureCollider_Start_m23D077FB975FEC89E31A2BA82E97666FF1404F78,
	PunctureCollider_FixedUpdate_m84CCB8A79496DBA69578096EF4798AE13A605A53,
	PunctureCollider_UpdatePunctureValue_m68939D5B98DA8E4D51AA8C41305DB9F467350FB9,
	PunctureCollider_MovePunctureUp_mEFF26B2796A1C023E543ACF28BEE48CDC60344C2,
	PunctureCollider_MovePunctureDown_m3EE635716B5D2FA4F0B892EF266A4DB4C50D40E4,
	PunctureCollider_CheckBreakDistance_m55752030C85948D77868FA5FAA5734717A43295A,
	PunctureCollider_CheckPunctureRelease_mF30B9EB4813DB023AE21CC233BD9677551C8DE31,
	PunctureCollider_AdjustJointMass_mA89A602589FB41C3BA9B4FDE3B8DBA3283231849,
	PunctureCollider_ApplyResistanceForce_m90924CD3DD46B9B320171C9864DD0C30B5DADD33,
	PunctureCollider_DoPuncture_m4D3F79F8C0E33FAFA23166E5C1227458C90A2E09,
	PunctureCollider_SetPenetration_m36A5312D41D0A65B398D1B52219F4EB336DE3508,
	PunctureCollider_ReleasePuncture_m06FE1333EC9FB1084295C70859A7F85A8B5B942E,
	PunctureCollider_CanPunctureObject_mA09A2ABBEFCBF6EC9C46685C3C37D761874A0DF9,
	PunctureCollider_OnCollisionEnter_m744DFC0B620F09ECD948DAADFA2317B1080E747D,
	PunctureCollider__ctor_m0FD116151DDF1EE2B0221FB749116D90714DC4AA,
	ReturnToSnapZone_Start_mD8949AAA83F5ED8435C85E2D9B41E457A1766521,
	ReturnToSnapZone_Update_m2C6BBA85099B2C47E3C563C6523A2421BF54282D,
	ReturnToSnapZone_moveToSnapZone_m0ECC6F390849EBA5CDD0C3E008E814A1EBC8BB43,
	ReturnToSnapZone__ctor_m4FA3F79F4DB7752E46B0B242D7C90F0097AEEDF6,
	RotateTowards_Update_m7A2896ACD2F19045D6BE5D29E926F8AC617EECFB,
	RotateTowards__ctor_m561E188D3A6F8405270984037261EEC0DA243134,
	RotateWithHMD_Start_m4985031D350A93616441FDDC15AF9572B3DCAED6,
	RotateWithHMD_LateUpdate_m4FF90B28FE400655898AAE17368918AAA4513E27,
	RotateWithHMD_UpdatePosition_m3BCA3C61B0E7D8518C16C7CB434152C6BCF052B7,
	RotateWithHMD__ctor_mC3890C2887ADCC6DD80CB6878F6D4F08EB252B33,
	ScaleBetweenPoints_Update_m76ED29EA52A36DB6E6C0A7035005C204EF10F890,
	ScaleBetweenPoints_LateUpdate_mEDFDF0A33785834A9776D073F2DC5CA0693497FF,
	ScaleBetweenPoints_doScale_m662588F63E1B3CDC3ADABE15D2AFA30ABD6994A3,
	ScaleBetweenPoints__ctor_m0D45EF03AB1AC4FFE1E32BA6D322ED7A4B18CF6B,
	ScreenFader_Awake_mAAC372FCE277400FD3D6E6B320FF9166FDB87007,
	ScreenFader_initialize_mC923BFB9D29EB8762357E7700F6037685AD5E0C3,
	ScreenFader_OnEnable_m8F43113B1747665901B6996BDE794A10074A4A10,
	ScreenFader_OnDisable_mC617118A44EA298356E5D82ABC0C1A32FA513CCC,
	ScreenFader_OnSceneLoaded_m37BD890B46551171AAD388CB7BB97B3A83FA9C39,
	ScreenFader_fadeOutWithDelay_mD86B773AE4D434860DADB74AF0DC6FE38B5A342D,
	ScreenFader_DoFadeIn_m062123ADE0427D57705C5BB77D5915962BFD12F0,
	ScreenFader_DoFadeOut_m3AFBFE78C945B7F199557001458A608D723EE2B4,
	ScreenFader_SetFadeLevel_mDE4FD12958A22767F7A5D58B937811311EF39EEC,
	ScreenFader_doFade_m862A3EF8A45B3051AFC5242A61D1C66AB78FE5BD,
	ScreenFader_updateImageAlpha_m6F946DFCB09EA85F8B613C8906530C9B9CF3363F,
	ScreenFader__ctor_m4A79D6C895C42101F43AF0007E4E2666F0ACBD03,
	U3CfadeOutWithDelayU3Ed__17__ctor_mE6A4698C3034BF181360C61E3F772E760AD0D8D9,
	U3CfadeOutWithDelayU3Ed__17_System_IDisposable_Dispose_mCCB0765AE3D78937291A5AE49BD9B1BB9632CAFF,
	U3CfadeOutWithDelayU3Ed__17_MoveNext_mE88C053E1E93978CDEFE793880939F8396CDB436,
	U3CfadeOutWithDelayU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBDEBF359EFD0B9866A9AD796FC7F818F175F44B1,
	U3CfadeOutWithDelayU3Ed__17_System_Collections_IEnumerator_Reset_m1EDA5F1320C288F543D7E60AD25ED9F7FCE69721,
	U3CfadeOutWithDelayU3Ed__17_System_Collections_IEnumerator_get_Current_mC8790369D8770293F8CCA82A75F9B622C6D1092D,
	U3CdoFadeU3Ed__21__ctor_mFF2CCA1CB7F3B22254145D85230E3D0C117A7B35,
	U3CdoFadeU3Ed__21_System_IDisposable_Dispose_m006CBCC7A347057E864B04CE50FDDE18EABE0B15,
	U3CdoFadeU3Ed__21_MoveNext_m69889B85F7D874ECCB3558DF5BC15B0EA64E9B2E,
	U3CdoFadeU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8C7BED05CF7923BCEFE48AF9BB50F6D275FE83F2,
	U3CdoFadeU3Ed__21_System_Collections_IEnumerator_Reset_mA4254373313176EA305CE175793B735FE7A4A4AF,
	U3CdoFadeU3Ed__21_System_Collections_IEnumerator_get_Current_m411E69641964C55BC4010C8C44D26FB95F257182,
	SnapZoneRingHelper_Start_m23D834E468F5D56B921ECF2376D47BAFCC6D0BB8,
	SnapZoneRingHelper_Update_m1AEF094E4051CDA8CDD8AEE3E7BCFE666059249C,
	SnapZoneRingHelper_checkIsValidSnap_m7EE8EA1EB6DE482C629A0964CA9B1F7D514FD093,
	SnapZoneRingHelper__ctor_m75521EA3485CFBEFF72748D162964C0233A5AD87,
	Tooltip_Start_mE38AA786D848E1561A5D11541E6356DC8287717B,
	Tooltip_Update_mFC24E5E457AD10B38952720326F9B641A8195E9A,
	Tooltip_UpdateTooltipPosition_m36A83BDB6D46B2C7429C60A1053450754CAB5C31,
	Tooltip__ctor_m3CB7B06DAB3CC4D19A406670FBC4DD5441F7CB3E,
	VelocityTracker_Start_mFCBC48FFADFD340097A92AEB8F0AC125EA57E656,
	VelocityTracker_FixedUpdate_mDBD3FE6AA7E3D15A17FF7E6C948723B1299ADDE1,
	VelocityTracker_UpdateVelocities_m2484908532B94E504FA54ACA216251E0A8BDC27A,
	VelocityTracker_UpdateVelocity_m0BCBA61741ABAA74DA27DE70AE8F50AF865E94B9,
	VelocityTracker_UpdateAngularVelocity_mA407FB6B3B8FE06D58AD5CD07E89A6707E48968A,
	VelocityTracker_GetVelocity_mD7827FCEB570FAAEA40983047B125B2E48CC7EAC,
	VelocityTracker_GetAveragedVelocity_m6036C953779EE2E07DFAFE5ACDAB8BA5CE7EB87A,
	VelocityTracker_GetAngularVelocity_mB3DE2D0DD8424EBE512EE82DCA57E85C32CB33DB,
	VelocityTracker_GetAveragedAngularVelocity_mF0F495C1648C6EB164CA8EA3D51B85587047385F,
	VelocityTracker_GetAveragedVector_mC5EE78C23B6C1798CF310EE662B7BDBC7E2C95F1,
	VelocityTracker__ctor_m8E5973F5BE65EEA2F042B02C398E1B09C9C80C12,
	ArmSwingLocomotion_get_VelocitySum_m9A00DCF3E39A58DA36726960379E60DA66AC5B34,
	ArmSwingLocomotion_Start_m71F71E7C2E7B1569ED875947D22FBC6A77CF8C0C,
	ArmSwingLocomotion_Update_m65696F14ECAA1C51CE865965D595ACD865CA5FB9,
	ArmSwingLocomotion_UpdateMovement_m726E83EA8D6217DDFEDA966DE483F46E4CA4E799,
	ArmSwingLocomotion_UpdateVelocities_m234795C427FB1AF60336CB9632B529D3DA764417,
	ArmSwingLocomotion_LeftInputReady_m304BED2FAC6BC9317D56D93AF35075844AE01E94,
	ArmSwingLocomotion_RightInputReady_m1C7742AF45FA73AF4BFEC15420B33C22D7025E34,
	ArmSwingLocomotion__ctor_mBB44CE0685C9FF82EC747224F7E60DC7B0BBB63D,
	BNGPlayerController_Start_mC4A4E9DF9F9AEB0417B18D9E475D9E8B5FFDFDBA,
	BNGPlayerController_Update_mE563FC1580FB30C97B0D16535D2092A3A4696443,
	BNGPlayerController_FixedUpdate_m118FF6D53309B3D3DF7DACB76735272D6F1F9EAA,
	BNGPlayerController_CheckPlayerElevationRespawn_mCA8A6EBE4AFB0E96639181CEBCDD374FF87FB9C4,
	BNGPlayerController_UpdateDistanceFromGround_m10BF5D47123AF4EC454A153845F68726ABE19EE6,
	BNGPlayerController_RotateTrackingSpaceToCamera_mC3B9DE8809C08DA7FC68876C70BAC1E4372947B2,
	BNGPlayerController_UpdateCameraRigPosition_mE4A1A4DA0C7284E54829EC29FDDA687A643A7EA7,
	BNGPlayerController_UpdateCharacterHeight_mC0697D60105CAACC24DC932DF8F47CA03776903A,
	BNGPlayerController_UpdateCameraHeight_mC753215A11095AFE519923F0C2C0FEAD6CC31103,
	BNGPlayerController_CheckCharacterCollisionMove_m4C0E587A65B36F69B92B3BF39A9F737DEE4AD193,
	BNGPlayerController_CheckRigidbodyCapsuleMove_m8740D2E41E2426E3EB4A19808340B2039A120DE7,
	BNGPlayerController_IsGrounded_m6971C747A2C7370505A68D626E31131E34D9437A,
	BNGPlayerController_OnClimbingChange_m8A5A7545D1DC74638D615A7E9E4A16E5CCE954C7,
	BNGPlayerController__ctor_mFDE3785D715242E8E7D653F1C50CF13A833605AB,
	Button_Start_m2EDEBB078BFBD9A56BEC947CF9E75295A5C9A666,
	Button_Update_m98F5C08C1541B59D8AECBD95586257C41CF6BF40,
	Button_GetButtonUpPosition_mAD229F48708202619364760DA3B4557E5C97AAD7,
	Button_GetButtonDownPosition_m6B9A5A51AA7EB8F89ECAC2E11B790B02BC2DB967,
	Button_OnButtonDown_m1BD11A554FC890DB805E12E595C31C9C835FE24B,
	Button_OnButtonUp_mEB88729BF0D91B57222321C3B2A95F69AEB1CC5C,
	Button_OnTriggerEnter_m7AEC71268FA897615C86AE49B56AB26B26E9B338,
	Button_OnTriggerExit_m9D91CB892299BD528B4598F222988CC203C6B62D,
	Button_OnDrawGizmosSelected_m2356F2EE120CA63412CE08A3C964698187F580A4,
	Button__ctor_m5F20BF9DD534AED16292D059DA5C84BF059E1B43,
	ControllerModelSelector_OnEnable_m660FA9702C50ED4644F274754E9D8F2386D3EDEF,
	ControllerModelSelector_UpdateControllerModel_m0E72CA18A0AE0BDE798CE6B77D9C1516B4F140FE,
	ControllerModelSelector_EnableChildController_m7EBC09DE873AC387A8B0381C4FCC39BCF07D3144,
	ControllerModelSelector_OnDisable_m7D7318A7D6994C94A454105EA269E87EAB86A5AE,
	ControllerModelSelector_OnApplicationQuit_m4A390A71CB85B11C85BD58662F91DE6D64AF84DD,
	ControllerModelSelector__ctor_m2FB425A768C65819142E96A0CBB1BCFC996A14C0,
	GrabPoint_UpdatePreviewTransforms_m0B2165BB6DC6D1C63D1B049954B25628052CBFAD,
	GrabPoint_UpdateHandPosePreview_m8B5C6302FDF1032ACC534241ECF1A09368EDED5D,
	GrabPoint_UpdateAutoPoserPreview_m6BBEC848EC274DCE354FE722883FB1C95F102372,
	GrabPoint_UpdateChildAnimators_mE232FB3EE90FD25FD27270C84F5D4E78A0BCD568,
	GrabPoint__ctor_mBE4D16187F0EF7BFB7D5D6EDD06AC0CF2E56DBE3,
	Grabbable_get_BeingHeldWithTwoHands_m3DAC984D14FBB33E0BF2036B531B8E5D21BCF3F7,
	Grabbable_get_HeldByGrabbers_mB3E16882EFD51B9F40D5BD24F5F01207124CA9AA,
	Grabbable_get_RemoteGrabbing_m16A2DA665EA7BB3F416EC5C8BF1644F2C2F5495B,
	Grabbable_get_OriginalScale_m9B19AD1DEEC7426B690B8411F045B55E4DF3A532,
	Grabbable_set_OriginalScale_m7FCE747EB1C49788546E552ABCBA224C221A751A,
	Grabbable_get_lastCollisionSeconds_mBF7F1D4BCBE4C738A07576FC4C8C6ADF26E85CDA,
	Grabbable_set_lastCollisionSeconds_m97DA7151E8F6E9790FCB7D2082F407359A97B908,
	Grabbable_get_lastNoCollisionSeconds_mF7FA6B3E0A03B7F11295FA04BF0FD3DD48098554,
	Grabbable_set_lastNoCollisionSeconds_m30D0E1CECB0890BC6819E5FEE31E2284BEEE24CC,
	Grabbable_get_RecentlyCollided_m2733EDCB2A47FF99B02F1147E5FCB3D743877B29,
	Grabbable_get_requestSpringTime_m47611550BCD10522CF270C53D3B014AED22387E9,
	Grabbable_set_requestSpringTime_m08C34AF8FD76EE0C8982E10A237F21AC99142162,
	Grabbable_get_grabPosition_mBB2405AFFCFE0C5322AEA75B4011DCC8C23DFCE7,
	Grabbable_get_GrabPositionOffset_m6ED3CE31AC735A6A238B94AB8CC08C95B6A3C9E8,
	Grabbable_get_GrabRotationOffset_m3051403B2AD53C48743FF306B069524235B3D883,
	Grabbable_get_grabTransform_mDC2EFFB018A7B1213C8C99EDD2F988FF66E47319,
	Grabbable_get_grabTransformSecondary_mE96FE5DD2219FA578ABEA7F6B1FA525445BA302C,
	Grabbable_get_CanBeMoved_mEBF6AD807ABBF3BAFDE9BB7E6B58C2EE3AF0C611,
	Grabbable_get_player_mD3C5F9F61DC448F11C366DE7AC381C231930C28B,
	Grabbable_get_FlyingToGrabber_mFF8A10085F3D418A2A8775EE7699F58C5AEA2543,
	Grabbable_get_DidParentHands_mF0E351F8FE4BFAC9E3AFEF876D950635757D34F8,
	Grabbable_Awake_m0A771E6A81B3DDFB0FB3F1C74C97778EEF83A4DD,
	Grabbable_Update_m4C33A838362EBC26CC3D53D4C0C2EF150A3819F4,
	Grabbable_FixedUpdate_m289F026EDC0196D4D854BAC4FCE16420AAFCE387,
	Grabbable_GetGrabberWithGrabPointOffset_m71AC439623F39CFA9A448859979B299B672BCA1B,
	Grabbable_GetGrabberWithOffsetWorldRotation_m71C96A5CDABC7C4506F2F9614835D1B6EEC5DE29,
	Grabbable_positionHandGraphics_m3850913BA843F928E9C61DFF39A898CD4CB3AFA9,
	Grabbable_IsGrabbable_m997F6E3753A6DCE1D6A26A36E625D82028C49ACC,
	Grabbable_UpdateRemoteGrab_m7E78ECA7E83F2B39FB622EF9F416FB4102081516,
	Grabbable_CheckRemoteGrabLinear_m57AD00989F1282952B3E6A4D6DFAD2C62447BCFC,
	Grabbable_CheckRemoteGrabVelocity_m549D9E22D66102F13D24A4E62ABF2BB75C056B43,
	Grabbable_InitiateFlick_m696A2064D593AC8DD62D50EA06CAE4F0656CF770,
	Grabbable_GetVelocityToHitTargetByTime_m2DFD923E24C36AFD07B136EBE94E298D3C9763D9,
	Grabbable_CheckRemoteGrabFlick_m4454EE3D7F8BE7CDEF23DB7792AE8E13122FFCEA,
	Grabbable_UpdateFixedJoints_m6C71682DC5A3A0D4501D818F06885F845E01272D,
	Grabbable_UpdatePhysicsJoints_mC6B06606B262B2A76833A5F7497F54A46F0CC40A,
	Grabbable_setPositionSpring_m0E060522B62823ABF7352CF13E02E7439DE877FD,
	Grabbable_setSlerpDrive_mB2C6541C8A0B8E39588EBAEE855A1CF6AE606527,
	Grabbable_GetGrabberVector3_mF1065B3DF1C14AA45485D27D6689434DEF895410,
	Grabbable_GetGrabberQuaternion_mC2569CFBC405F0F867DEC9B5EDBB419B94C160FF,
	Grabbable_moveWithVelocity_mDFF369846D10AC7EF418202200D85563D3550C48,
	Grabbable_rotateWithVelocity_mCE81DDC3153E4C12B851EDA1267516AB2EE50A99,
	Grabbable_GetGrabbersAveragedPosition_mFEFDA9A3A343F01310D2D17C0F9D8E260C77A760,
	Grabbable_GetGrabbersAveragedRotation_m0F3CAE0A24FAA9B0C70DECD5515D2B6CA9082769,
	Grabbable_UpdateKinematicPhysics_mD2DF5B23882AC29D8121E30F45B664704DF6C9A8,
	Grabbable_UpdateVelocityPhysics_m6A289A4BF217C990FAF3C0AD7511B77012D4A124,
	Grabbable_checkParentHands_m149A7CD11FE226346BA21E6E931A82BE67717405,
	Grabbable_canBeMoved_m80E78D8FC28841415A901C2A441EE7F995BE0B03,
	Grabbable_checkSecondaryLook_mF127E579F07C3ED5882A9E9A376F87C04CE62B9B,
	Grabbable_rotateGrabber_mA57A66550109DDF99A5164B0EEE21D25906DA028,
	Grabbable_GetGrabPoint_m83A0824B21D097CDBD62BF0237923253DC1D3B88,
	Grabbable_GrabItem_m9CB66185368B7434BF1464BDC987C77388F8B3E4,
	Grabbable_setupConfigJointGrab_m0291F5BEB8E722EB909C7FBEE419046825919778,
	Grabbable_setupFixedJointGrab_mA46C958D87378DD1D4D30F0AA753580E0ED00529,
	Grabbable_setupKinematicGrab_m69AA9B7F8DC0CD2825732C24939F9F7CF7C7955F,
	Grabbable_setupVelocityGrab_m1DD8C5E5E4881B0DEDFCD89F10FC3957356D8979,
	Grabbable_GrabRemoteItem_m50B7AE2497F7898717FD3FAEA0C545A21E409965,
	Grabbable_ResetGrabbing_m89E2677F90FD73DBC89CA11457FC6320149D2735,
	Grabbable_DropItem_mEFAFF6A597B3040204BE5ABCF90A74C80AB11DB0,
	Grabbable_clearLookAtTransform_m422AC60814B87213BED1E9BDE61FB680721B1B62,
	Grabbable_callEvents_m524680A6268E78D0FE4050E6197AE5542436DCFD,
	Grabbable_DropItem_m35EAFD02E34C19BB7E02077964C4856E88E65BFF,
	Grabbable_DropItem_m13C62518E7B3957E8938CD5CF84944586CC2C02E,
	Grabbable_ResetScale_mC49D5BCB43BFC6FDA0126F73BF1F8EBA7B92F981,
	Grabbable_ResetParent_m7F9AA9E1D12E9FFF3C2652287CAE7B1A2BC4C265,
	Grabbable_UpdateOriginalParent_m6EDD79D965D9AB995B66A591A64D3A3D354BC69C,
	Grabbable_UpdateOriginalParent_m051285106A39098C3FAC0ED4371431436BBDCAC5,
	Grabbable_GetControllerHand_m8CD072DBA6604E18EF3D7665065BC535A263386A,
	Grabbable_GetPrimaryGrabber_m63BB2F47CAD26232495D6E8A39633E27E1612026,
	Grabbable_GetClosestGrabber_mCAC30D4248928BFCF7385F87CE2840429C1AA889,
	Grabbable_GetClosestGrabPoint_m8DE1C2F88614F14D43531110C584DF7A4739FFA8,
	Grabbable_Release_mD669FA02038028187B8A91E7910EF367E84C221C,
	Grabbable_IsValidCollision_m1944EB6A501EB92FE8C52BEE451A3683F27F2A66,
	Grabbable_IsValidCollision_mF5D08297EA71AC961426FD3F5CFED0209D7BEF8A,
	Grabbable_parentHandGraphics_mAB917C0704D7790C1C0FBF098CB6FBDF89055602,
	Grabbable_setupConfigJoint_m0F25C0778DCB756888C72702052D46EA08B32733,
	Grabbable_removeConfigJoint_mD9A50031744C2E77EDEFFA7955357B1F7C4D925B,
	Grabbable_addGrabber_mA4EFEB17A105F32B85A15170DD62D425153E4D25,
	Grabbable_removeGrabber_m4DE10E2F63484923BDD4F1393E57F4DCA519B54E,
	Grabbable_movePosition_m64C7E68BEC4072762D15904A16B9F77DD514573A,
	Grabbable_moveRotation_m99114744F077B7176944786BD563E262C4853F53,
	Grabbable_getRemotePosition_m4A18A72F65F402CA915FCBE558D1A67E3B360F32,
	Grabbable_getRemoteRotation_mCCE58EB19018D6108E50BEB89B5D07AFE038CA16,
	Grabbable_filterCollisions_mCFE6BA43F956680A3438AE5BF718D25F3E3FCCBA,
	Grabbable_GetBNGPlayerController_m841FF78F9139D8F732499320B49A294C3436D6F4,
	Grabbable_RequestSpringTime_mA089D6C5C6DB2998EF9F124C1B3C1776A89B0C10,
	Grabbable_AddValidGrabber_mA164DDD0E08A76F6F7AB7AEC89F3A81DA1588395,
	Grabbable_RemoveValidGrabber_mA673F23FB019D142E55DC84A7E67EB776FFBB05A,
	Grabbable_SubscribeToMoveEvents_m74469140C5A493A49F9B3D41E64A0D16A9E6C07C,
	Grabbable_UnsubscribeFromMoveEvents_m7F997A86883B3EA36F2EFAE0647C2C710FF7E147,
	Grabbable_LockGrabbable_m458940515475256513E228CB7A98F3D6D99EC9A8,
	Grabbable_LockGrabbableWithRotation_m7F3E23B93A050813F1C941FB03A2381039A8B700,
	Grabbable_RequestLockGrabbable_m22C18DDC409A38113766BEAA0419C2D2F36D61B0,
	Grabbable_RequestUnlockGrabbable_m13479A09FB3084A71857893BEBD3146988476DA1,
	Grabbable_ResetLockResets_mA630724C6CC169D12AC103E379E1B6CA5ADF70C2,
	Grabbable_LockGrabbable_m0B7196BB74F0FC5F7D449DD743A836F6595ABB4C,
	Grabbable_UnlockGrabbable_m98A0C3AD2FFC4FE06C16C2FBBD7BAACB8345E69B,
	Grabbable_OnCollisionStay_m1F7020127DEFF0B39FC3EF43D750A7D8B544EED4,
	Grabbable_OnCollisionEnter_m30B58EFDA5E90709DCC394C6F80369239D2FEF1C,
	Grabbable_OnCollisionExit_mE55B730A11F0D6C6CB7F473743F642CE585A6EF8,
	Grabbable_OnApplicationQuit_m7075D5CE29205FBF60773CB8166DAAC02739A7E9,
	Grabbable_OnDestroy_mF1796ED5C1AFBDEA6E8FE9FFA216A654BD626E93,
	Grabbable_OnDrawGizmosSelected_m147D090E0B47ECBAF9325064C59E56D005BF1B86,
	Grabbable__ctor_mC6E92A845737C5C5A07E8009E1B6B85E7ADF634D,
	GrabbableChild__ctor_m230EE8AC38B17A31E2D8B7719F4D27A502B77232,
	GrabbableEvents_Awake_m16300D28B226E3B589C8D6975A19EFAE29374A1C,
	GrabbableEvents_OnGrab_m3A05CCC25783C467CCDB81C322DE87DF275D35A3,
	GrabbableEvents_OnRelease_m6BDD7C143E37849459BB1859FBFD5114A7BB0ABC,
	GrabbableEvents_OnBecomesClosestGrabbable_m6646B10925B1291A49C93FEDFC843C9BFB2C2CA7,
	GrabbableEvents_OnBecomesClosestGrabbable_m1F1244A0A1D864031B46C757A9227A5C9C0D4A1C,
	GrabbableEvents_OnNoLongerClosestGrabbable_m84FDE1FF9362141BF49712B24B9893CDF5707C9A,
	GrabbableEvents_OnNoLongerClosestGrabbable_mED434FAF6530B79F80BE4927248F8C55CAE40B3F,
	GrabbableEvents_OnBecomesClosestRemoteGrabbable_mBF87EA092E5885C6E13B479A7074DA00B00885D4,
	GrabbableEvents_OnBecomesClosestRemoteGrabbable_mA9894E92464EAB14B2532DAF25203D4DE7800F4A,
	GrabbableEvents_OnNoLongerClosestRemoteGrabbable_mA9439058D561DADE05C8747D41E3416C88052123,
	GrabbableEvents_OnNoLongerClosestRemoteGrabbable_m030E2169C9A89829D2AE1ABD30146ED817F7B44E,
	GrabbableEvents_OnGrip_mA4A69BBB36F64A289D267C1BD4F1C5FB50BAA745,
	GrabbableEvents_OnTrigger_mB19BD489E62C51248E465C43952B601FAC77E028,
	GrabbableEvents_OnTriggerDown_m80B1286DD9DFAD1CDFB944643C8A5866AF63BA94,
	GrabbableEvents_OnTriggerUp_mBE343B9EDA4BA874D59393935ECBEDD42A2CA2A9,
	GrabbableEvents_OnButton1_m876D37284D6FAB1B4863D2F85718CE96CA7A5818,
	GrabbableEvents_OnButton1Down_m85C0FABF7E09A3B0D534895621D66BDC0F47BD76,
	GrabbableEvents_OnButton1Up_mABB5D6348953192E56287D658F99ED19033D0625,
	GrabbableEvents_OnButton2_mE4ED9758FF318561AB14DE372C98F693E41FF128,
	GrabbableEvents_OnButton2Down_m9DD4C2009AFD65085C9D92A83EEF47DA98CED002,
	GrabbableEvents_OnButton2Up_m15E851F22E8B4FC73719C16C3C1A237E4CC2DE4E,
	GrabbableEvents_OnSnapZoneEnter_m566BB9919D73CAA6B0380D3A6E2BDE839410BBCA,
	GrabbableEvents_OnSnapZoneExit_mEA0085319033EB6C2B3F3ECF2B249CD85156EBBF,
	GrabbableEvents__ctor_m80801610DF42B99D03171148052B719B301D1BD1,
	FloatEvent__ctor_m8A9FA5A390670F2AD577D293CBDDAADE22853D51,
	FloatFloatEvent__ctor_m5BCF4F985CADC8E14994E5A01185AB6A5B570FF0,
	GrabberEvent__ctor_m789C6B162EB1B4D67000B926BF30C53EC7137204,
	GrabbableEvent__ctor_m02D8FF725C353C1AD06E5B1AD6A1BB678626A36F,
	RaycastHitEvent__ctor_m0BB0E55E0714170447616937DFAF6CEB1F117D1B,
	Vector2Event__ctor_m86573ED291DBEB689CBCB88C51852FC1ACEC0CBD,
	Vector3Event__ctor_m6CCC6493E1EF9F8DB74A2348251D601860EE1FB9,
	PointerEventDataEvent__ctor_m2C3BA6FF9A476FB2CA5EBA05ADA7FF383231A293,
	GrabbableUnityEvents_OnGrab_m992FD6332DAE9075F08A167A4D6B32719D49A697,
	GrabbableUnityEvents_OnRelease_m5AE17A470A11CFF11109D4D0BF7247A20B47E58F,
	GrabbableUnityEvents_OnBecomesClosestGrabbable_mEA4F7EF7F895436E7D8077447A605B5B3CD103A7,
	GrabbableUnityEvents_OnNoLongerClosestGrabbable_m7B1E01CCB83C397A978BEE1412DAD48E5F4BD152,
	GrabbableUnityEvents_OnBecomesClosestRemoteGrabbable_m20FFB1C1E55CE812A70F6F8B7940CA83EA8B0F20,
	GrabbableUnityEvents_OnNoLongerClosestRemoteGrabbable_mA2F662498E607E048B02B4FCE8CB34513FB787C2,
	GrabbableUnityEvents_OnGrip_m386F5C308E986AA1485381673A2B74298F663104,
	GrabbableUnityEvents_OnTrigger_m15882625E2F3A0E175E6AD7D4BC9C140C21D6385,
	GrabbableUnityEvents_OnTriggerDown_m1F1ABEC37952B4FD3D9C8165F3466C19627AA1B9,
	GrabbableUnityEvents_OnTriggerUp_mF4B8729B5E2B4BC495BB99262C3D2E4B701298CA,
	GrabbableUnityEvents_OnButton1_m727E035043C0A2F63B2F9A7E4B37931EF07E95F1,
	GrabbableUnityEvents_OnButton1Down_m1F0FA0B17A23A2D1900BABD179E308A6602266A5,
	GrabbableUnityEvents_OnButton1Up_m4736B453BD9852472F3F9F046F6F6172073D2810,
	GrabbableUnityEvents_OnButton2_mC6E2BF5812076792C4F6BE60617A16CF86649E9D,
	GrabbableUnityEvents_OnButton2Down_m939839E3A95E47A52EE5AE2CFEA96B4EB3CC729D,
	GrabbableUnityEvents_OnButton2Up_m61C4D718A250FB503C69417AD596DB8D92D687C4,
	GrabbableUnityEvents_OnSnapZoneEnter_m2601BAA82BAC722515106196D689E3B2623A72C0,
	GrabbableUnityEvents_OnSnapZoneExit_mD904BEC2C0DAF76DB800A0E177D637E8998D8794,
	GrabbableUnityEvents__ctor_m86AF32CE7330806B0EA387E6643357E82214C16E,
	GrabbablesInTrigger_Start_mFE2A6E4FFD35EA627D96B83CD4DD17630446A3B8,
	GrabbablesInTrigger_Update_mD8419DC552935002CD3C613E24472C5427457D68,
	GrabbablesInTrigger_updateClosestGrabbable_m97A03BD03B28251EFCF5BCC9516A1FF75BAC9DA2,
	GrabbablesInTrigger_updateClosestRemoteGrabbables_mEA75423C51F7730A5DB2CB74B9192F536B196F25,
	GrabbablesInTrigger_GetClosestGrabbable_m24BBA6A2A8EE2BBAD28B77C14E070312DAAB4A4D,
	GrabbablesInTrigger_CheckObjectBetweenGrabbable_m991D345C787EC4D6BE8906FE637B89589B13C3AF,
	GrabbablesInTrigger_GetValidGrabbables_m297F193E8A4C04979F9F610796FC5921F76BDCA3,
	GrabbablesInTrigger_isValidGrabbable_m0E6C113E2D8A0697AD8A9952E05F73F92167D93D,
	GrabbablesInTrigger_SanitizeGrabbables_m81D62F641097063F710E16B889603E4BD9045CC9,
	GrabbablesInTrigger_AddNearbyGrabbable_mB385FDD255A726B793CAA8ACF79FEA678BFB3727,
	GrabbablesInTrigger_RemoveNearbyGrabbable_m4497A4349870227E35762BEF55C96AEE34ECAB3B,
	GrabbablesInTrigger_RemoveNearbyGrabbable_m7D410E42E68CF798B6C98F8E1F82C88EFDF83B01,
	GrabbablesInTrigger_AddValidRemoteGrabbable_m7FE179B1CC64A27D08CA540ED3A2CE882DAEE5D5,
	GrabbablesInTrigger_RemoveValidRemoteGrabbable_m004DDC4387028CDBFA128A063019BF1B8B02FBA0,
	GrabbablesInTrigger_OnTriggerEnter_m95FF2C2D7AAD72222C1889D81315098D2BE48E8A,
	GrabbablesInTrigger_OnTriggerExit_m311247B15CC935FA17FA7372463C26D530A27E6B,
	GrabbablesInTrigger__ctor_mA9BC1CBFCDFEA0CC038B7F11C0800E23E30A8D91,
	Grabber_get_HoldingItem_mA918F4C86BA6A82D6616A34B4988B056E84735EC,
	Grabber_get_RemoteGrabbingItem_mD7DBFB29A517F55C3C62EB66CA791CD33CD4FADC,
	Grabber_get_GrabsInTrigger_mA382FE101CBE5E96DF5D671327B43540FCCA346E,
	Grabber_get_RemoteGrabbingGrabbable_m210788C57E8FBF9F6402AC83AAF5E6A2EBB4371F,
	Grabber_get_handsGraphicsGrabberOffset_mFAB29C48E66673C04802FFD831720CBCB5C9D0C3,
	Grabber_set_handsGraphicsGrabberOffset_m93905AB51A7DCF04E742557C86555E68418B5D3F,
	Grabber_get_handsGraphicsGrabberOffsetRotation_mEBB2C0304794B7D4D7183C6FD0E4983B2DDECDBF,
	Grabber_set_handsGraphicsGrabberOffsetRotation_mCF974D42B6106C73EC52147DC3FDA255D45FBD9A,
	Grabber_Start_m41C8A5AAC797D8B5397A97A0A7DA8FA99C0F0898,
	Grabber_Update_m50D9831F9AE1BB3C67DE6DFF4936E0CE877AAE90,
	Grabber_updateFreshGrabStatus_m387939703FDB04B51D944C8E6929ACC7F78AEBD6,
	Grabber_checkGrabbableEvents_m2C6C703950C97302558CCB65CB41A91E9F3D23A8,
	Grabber_InputCheckGrab_mB628A83862758A0C13333858D3E07788670BEF80,
	Grabber_GetInputDownForGrabbable_m2F5633EA96AB9619ACFBDFA210529C798128BD51,
	Grabber_getHoldType_m9B9B1F14E75FE624ABBA6FF668E5D1B02FC97A39,
	Grabber_GetGrabButton_m995CA336DC3BE522FD6FD95DD7D7EBBF2100B5F5,
	Grabber_getClosestOrRemote_mB5AB5E7602DE93918C6605DAE2F1130CB33884AE,
	Grabber_inputCheckRelease_m86F9773052F844DA41CA925DE98426DC505715C1,
	Grabber_getGrabInput_mA78C92796FB1DED24E53F375B230C5ED04E50ABF,
	Grabber_getToggleInput_mC876FB41DFC3B1A88EDB352F75F42052FF1145E3,
	Grabber_TryGrab_mB552AA0E4E68C5B66B2BE91A6A71DFABD725063F,
	Grabber_GrabGrabbable_mD44ED05776B563B6617C84B9F8F123950A70D186,
	Grabber_DidDrop_m510F5BE88AE92870D47F0E0D5BA01B59B4D9682C,
	Grabber_HideHandGraphics_m8150A6D7F45936CEFEB28C9F76324809695F6805,
	Grabber_ResetHandGraphics_m6063F04887618EEAC54E1718B2E4382A47272B3D,
	Grabber_TryRelease_m779EC399D0902860B3877358CC85F98A0A99DB1C,
	Grabber_resetFlyingGrabbable_m98D7A80A95A8363BFB12564DEA59A7B1A224E22A,
	Grabber_GetGrabberAveragedVelocity_m5D3B141B0AA79E40833871D7B5AB3232F0049B43,
	Grabber_GetGrabberAveragedAngularVelocity_mCA2AF0D698355D82C626B20DAE38E32FD4779D59,
	Grabber__ctor_m26535D953B7600FCCF91114A433319E5E6F46329,
	HandModelSelector_Start_m5435C32074C11B0B1F7F6A654C2662DEA26B1D1C,
	HandModelSelector_Update_m539C53836509024959A96BDD90C3C870687F3989,
	HandModelSelector_CacheHandModels_m1DC93D421AF8A3FEFDAC22FDB9D92757ABDACC25,
	HandModelSelector_ChangeHandsModel_m0EB1F7000CF7D5588670E8C9E84BC77CA62FC8F8,
	HandModelSelector__ctor_m8583284E734224FD6DAF623E0CA1FF012F6E1D33,
	HeadCollisionFade_Start_mF8F19CBF36E0B59ECDD88A5E3792DCE51B02156C,
	HeadCollisionFade_LateUpdate_m6F410A70B7DF7C276DCDC8CACAFB1CDD6C561E3D,
	HeadCollisionFade_OnCollisionEnter_mBC9DE278FEADB562DC8F426CC1566E3B3C6090B5,
	HeadCollisionFade_OnCollisionExit_m3340BB9C5E408E1B509FC7FC42C081672D036799,
	HeadCollisionFade__ctor_mCAD93DB2A9725DAC0AAFEA56CCFF0F6090CE237A,
	HeadCollisionMove_Start_mD2809DD9305868C7F4C5B2CA25193A3A4396836E,
	HeadCollisionMove_OnCollisionStay_m8C2D4AE4790BDA791575D2D8F02221AD854135EE,
	HeadCollisionMove_OnCollisionExit_mCF8BF2BAD2FFB72DB1B64EE7830CA8E6785FEFB1,
	HeadCollisionMove_PushBackPlayer_mEBEBB2B5F3AE37B78B064499C38ADC1BF6BD4066,
	HeadCollisionMove__ctor_mC4C557E3A3962987555875DA5F3AD0C071CFAA29,
	U3CPushBackPlayerU3Ed__8__ctor_m08C1160C1AD97D2B3E78428A0E2115815F1814D3,
	U3CPushBackPlayerU3Ed__8_System_IDisposable_Dispose_m0E90D8943D62FD3470C5A27805895B517AD8EDCB,
	U3CPushBackPlayerU3Ed__8_MoveNext_mA6F9566890C79654F3F7E7E392C961499C75C08E,
	U3CPushBackPlayerU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m61E82635C8C75A9B3882E0D696BB94EFC89C891D,
	U3CPushBackPlayerU3Ed__8_System_Collections_IEnumerator_Reset_m61BFC1A2BEB2E30EC0B565B496701E56408EE839,
	U3CPushBackPlayerU3Ed__8_System_Collections_IEnumerator_get_Current_m8EFAE279F2B03DC2418C0EEDB5ED8FB1B4D84904,
	HingeHelper_Start_m4F24E52D7522552A44353F7718A18909BCF1AE81,
	HingeHelper_Update_mE4E02727B805F19526BA43B661EBE98DBEE69B66,
	HingeHelper_OnSnapChange_m463459440948D61DAD49693CFEF844C5DA7FEC60,
	HingeHelper_OnRelease_mAFC29CFF6D0D271329CB8C6DCAF7DFEB8303D438,
	HingeHelper_OnHingeChange_mB60E4583045C4E593185C8439E03FC776F5B4914,
	HingeHelper_getSmoothedValue_m689E9C2C092CC1FEBE5A1482CA210AA594A67BE4,
	HingeHelper__ctor_m14F9CFB5F7A7625DBC0CDC54401249E7094DBE92,
	InputBridge_get_Instance_mDB8A03F2E5F5E928F3A274DE0BA381828B2D2A31,
	InputBridge_get_DownThreshold_m2081BEBD133900473D927E47BA7761EF2A5BA4FE,
	InputBridge_get_LoadedSDK_mCA5321762C1A21E05D77CCDA9A15EED63C205A26,
	InputBridge_set_LoadedSDK_m6547C440E945B93F54973ECFF38F55809A78E705,
	InputBridge_get_IsOculusDevice_mDCB5B70A79A11C56B73F121DA77AD2D91BB250DA,
	InputBridge_set_IsOculusDevice_mBB0509C77C3AD75330903A1B0960D16365CBEF56,
	InputBridge_get_IsOculusQuest_mD9CDF3DB23F8B270DB2C109525FE522A79D1C51A,
	InputBridge_set_IsOculusQuest_mD0AFB82BCD37E923041574FB3356433AF0A11756,
	InputBridge_get_IsHTCDevice_mCC833DD8884CB4B140782D3E42ED18F3BC9394DD,
	InputBridge_set_IsHTCDevice_m414FE787559A603A01D52A9283668D29BF28CE27,
	InputBridge_get_IsPicoDevice_mA7EAF3D69B5563F38FC2A8F8F697389B487CF328,
	InputBridge_set_IsPicoDevice_mC36E16FFEAEB0C3802DB4DC3C87AADFA78CA23E5,
	InputBridge_get_IsValveIndexController_m4586B3DD662D12BEEC0036B6EBC72853C1986C1E,
	InputBridge_set_IsValveIndexController_m9B7E917D503FBBB00FB5D5FF2D3595A46B1EA5CA,
	InputBridge_add_OnInputsUpdated_m8F00808E038DC995AB0FA7FBB8EC6499F720F64A,
	InputBridge_remove_OnInputsUpdated_mC3A15696ABD1F8BC6307E08780E1AB49EC598100,
	InputBridge_add_OnControllerFound_m1F7C0C9508154BC1A1CD3A173343FF509EF4BCAF,
	InputBridge_remove_OnControllerFound_mB435B677AC080F64A2594FB243DD80C171BAA2F1,
	InputBridge_Awake_m5980A05FB2A1414A50519DB50CF79A2EAC9FBAE6,
	InputBridge_Start_mA4C528622FFE046EC4E3011D43693CE229528791,
	InputBridge_OnEnable_mDB743EF29DBB4F02101BF98B8564745500252DF7,
	InputBridge_OnDisable_mED1F43BF82CBEA6D76754C4CAED6DF3F00F279D3,
	InputBridge_Update_m68B16C061206959DC7D0D69A6E6014E8269D6172,
	InputBridge_UpdateInputs_m2936DD153830EFBBC25E8786CA7A6AF98F36FE08,
	InputBridge_UpdateSteamInput_m87EDE0234E000212A60522A4291717D4FFEC84D6,
	InputBridge_UpdateXRInput_m450BC69DFED522F18481120DBC01BB7333F3E88D,
	InputBridge_UpdateUnityInput_mC98EB65A4B678AD6F35617F0CB341D17F7EB7BD7,
	InputBridge_CreateUnityInputActions_m211F9950ED065D049A0FCE39EAE472A068406BCA,
	InputBridge_EnableActions_m695A68C04D10029B209AD6B4363C0FC380CF9376,
	InputBridge_DisableActions_mF2399458D38AC102473F83D6DCB121DC429FDA27,
	InputBridge_CreateInputAction_mD42D618DF2610E26A6654538D3E5D6C3FEDD52AF,
	InputBridge_UpdateOVRInput_mB83BD672F9BA7AEC060656F21B46C227E69C92EE,
	InputBridge_UpdatePicoInput_mF5BFE136091A62D906F2D108ABA320C161DAE14C,
	InputBridge_UpdateDeviceActive_mF5116FE517FABFB13F3820C4F98040092A2F0F9D,
	InputBridge_correctValue_m71A24B32AA42AAE99AC6AC534359325E79E7EE9D,
	InputBridge_GetControllerBindingValue_mDDE0E9F0AD34D8CC626E3D9C6F3506772888BCBD,
	InputBridge_GetGrabbedControllerBinding_mDFB5639E819F2A9DBE9D6D967331CE44A5F0E7DF,
	InputBridge_GetInputAxisValue_mF6A968A61135C8FEBF2E9FE7339293CA331A2CF7,
	InputBridge_ApplyDeadZones_mD0918C38EEBC9124E4919094215BAC98C9A291DB,
	InputBridge_onDeviceChanged_m2F69D8E65B501CDA5DB96A7D0EA968044D6C5201,
	InputBridge_setDeviceProperties_mD577F7D76A64774DD96A34A230480CD0C5AA7808,
	InputBridge_GetSupportsXRInput_m1015BCECA4DF0B9C4F8568740710FD6156394211,
	InputBridge_GetSupportsIndexTouch_m519170D5C143D8595967E8F16002A7600BE568D9,
	InputBridge_GetLoadedSDK_m05B420FF161A3DBE133180D917AFAC0EA15DDCF3,
	InputBridge_GetSupportsThumbTouch_mB5BAB7B0590A9AB38C64D058594EADA898D3D5B0,
	InputBridge_GetIsOculusDevice_m5645AED5E336DDC6D5EE7F24EFE3621A021EFDC7,
	InputBridge_GetIsOculusQuest_m173A1586EE231E7A767AA8270FB08DBBE65C0375,
	InputBridge_GetIsHTCDevice_mA0EBE6FE00069E731D12D66C3B180754F793C9D7,
	InputBridge_GetIsPicoDevice_mB9979C6BDB6A9782B5FC5922A6886DFD1B0FF0C0,
	InputBridge_GetHMD_m12F10F1BA64538C8CA9331BB8398C07C179586E1,
	InputBridge_GetHMDName_m0E5284E5E6E4F9BC534D9CB917CD0E65519FE42F,
	InputBridge_GetHMDLocalPosition_m49F666478367CE8AFB43E428ACCC19EADA2D6559,
	InputBridge_GetHMDLocalRotation_mCCA2E96090AC7492F724A02E613A692DCF9FB148,
	InputBridge_GetLeftController_m4F440B48E2CE2B815C5F9239370FB993926AAB8F,
	InputBridge_GetRightController_mAF423700F997707B8608D24953E79A85974F7A99,
	InputBridge_GetControllerLocalPosition_m8BED058A73B3C685F0D6AB58CB96DD299782F7B1,
	InputBridge_GetControllerLocalRotation_m5F41D9D401C9F923373C56959172520622A71CC2,
	InputBridge_GetControllerType_m70BE983CADCB5B7FD4E80E50F899683E6EE025B1,
	InputBridge_GetControllerVelocity_mBF5EB84F3E2D9E2B31700393BCB44E2DA32102CF,
	InputBridge_GetControllerAngularVelocity_m36E61CAFA4BF927264D030A0E0CA7C7B1B244FC9,
	InputBridge_GetControllerName_m839686D9E3462DA104B0EC43AA9AED9AB2E78877,
	InputBridge_GetIsValveIndexController_m0C8CE2F94C6DEE092295553458FA6F00B13AAA21,
	InputBridge_getFeatureUsage_m32A73379A81829E1A48EB5116D6530AB864B146F,
	InputBridge_getFeatureUsage_m9DD72820A9B6414D8967DD8C0F4333EE46A903EF,
	InputBridge_getFeatureUsage_mA10C64E3CF1855636758162B808A7C0208E6EA58,
	InputBridge_getFeatureUsage_m42872AD344088A3D67E248713FA1422B35AF56CF,
	InputBridge_SetTrackingOriginMode_mBC52018EFDFA97C54E12B913EDF97E82521BDCE7,
	InputBridge_changeOriginModeRoutine_m0B400D890F0C20C44F747CAC345381F6950F0810,
	InputBridge_VibrateController_m91C479D60EEC4DD53CF7104757B209FE866020E4,
	InputBridge_Vibrate_m6A2F789AEF61524FB32629E5EFA09C498B61D219,
	InputBridge__ctor_m2B7A25A6EC21AEBFD3BCFB0845699648BCDCFFD6,
	InputBridge__cctor_m168EDC4B851DBFB39F9BE36EC0CA2363934484BA,
	InputsUpdatedAction__ctor_mC03BEFABA11960D878D93F24B66A3C4D8FE9BC1C,
	InputsUpdatedAction_Invoke_mD52710CEC5DDCAB294DE2B0862B622A3D6F410DA,
	InputsUpdatedAction_BeginInvoke_mFCAA1268FA5D19025F97DC91DF7DB36B95EFE96D,
	InputsUpdatedAction_EndInvoke_m10A789C1C18D695BBF6AEF959CBC352148E8FD43,
	ControllerFoundAction__ctor_m597CA54F761EF1EBED8CCDAAC320551DE53AD48D,
	ControllerFoundAction_Invoke_m1E0C4BD3BA15EBBED1C73D99B1A562F07116331F,
	ControllerFoundAction_BeginInvoke_mAF4304154B7C7C5389798CD3A83D88194E9A80D1,
	ControllerFoundAction_EndInvoke_mDE0C70E719B5543A4351D7789562E6C7DD485151,
	U3CchangeOriginModeRoutineU3Ed__178__ctor_m0154E8F7E7C48C34BEE3C52F5C942A4326CEBD06,
	U3CchangeOriginModeRoutineU3Ed__178_System_IDisposable_Dispose_mC8372952199F3333B013CE1C0549F5496ED8F688,
	U3CchangeOriginModeRoutineU3Ed__178_MoveNext_mF924084F9F296566536DB7ED300D762825DC52F5,
	U3CchangeOriginModeRoutineU3Ed__178_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2863071A07D2B34FA46102AFC732308D5A23F1C9,
	U3CchangeOriginModeRoutineU3Ed__178_System_Collections_IEnumerator_Reset_m87FF77EBC47F08283E39EE2812F487DE05F7AC28,
	U3CchangeOriginModeRoutineU3Ed__178_System_Collections_IEnumerator_get_Current_m5BF5C46F43F0E09BD6EB09EE9DE186BCDA2588D8,
	U3CVibrateU3Ed__180__ctor_m72198B9A716A339729494625E91499FB486A5A2A,
	U3CVibrateU3Ed__180_System_IDisposable_Dispose_m1A3D42AD89DD04172E168797023E68CFCD301B29,
	U3CVibrateU3Ed__180_MoveNext_mF3D514AFEBBB86BFDAAC85016882B554B5458AFE,
	U3CVibrateU3Ed__180_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m52022956AD93A835A26B10247FCB43954547928D,
	U3CVibrateU3Ed__180_System_Collections_IEnumerator_Reset_m8006401B424DFCB494ED2037BC961C65FEBB89A8,
	U3CVibrateU3Ed__180_System_Collections_IEnumerator_get_Current_m3D0B18794FDFCAF1600FF960E27DFF60CFD44101,
	JoystickControl_Start_m1445DCAF3ED72538CD63CB7ACFEFF07C0CB89A08,
	JoystickControl_Update_mF9E9AD26E93A11E95216A622399C461188354E0A,
	JoystickControl_FixedUpdate_mBDC6F9432905A27BCD47512876F84BB0B7B522DF,
	JoystickControl_doJoystickLook_m2A3A22F4BA878C4BFAA84E61A9DBD1D8AAB3CD59,
	JoystickControl_OnJoystickChange_m1BF16DDE49BB06ED9579E665B505D4852F1C99C8,
	JoystickControl_OnJoystickChange_m57914546F0EAA6E117337F12B5CA51718FD610E4,
	JoystickControl__ctor_mED8CDC4B85D5AD9685C2DE07B1F1E8F53C65522A,
	JoystickVehicleControl_Update_mA4A542866AEBCDD6CD7E07F5F6D717C2FC56D47E,
	JoystickVehicleControl_CallJoystickEvents_mE91FADA140ACDD9ED01006ECEC843DF2B678435D,
	JoystickVehicleControl_OnJoystickChange_m79E94087854207DA2AC1AA9E92ED78CA7ADABCDB,
	JoystickVehicleControl_OnJoystickChange_m5B4562BD133B96F957A4D85E227EB3CDE21848D0,
	JoystickVehicleControl__ctor_mAD07E831FFF7E44D7A35C5239C1A413987C8CCA4,
	Lever_Start_mF6BD3FF0A1E598EAE55516AC87F8A5F28D08DE80,
	Lever_Awake_m9D4893FF25C1AD946567FF9E01C5E19096F86515,
	Lever_Update_mC0D422FD574A0B68961A53721C5944CFF61D9A55,
	Lever_GetAnglePercentage_m05401E2186E31F8B6C504E9DB52CA139A687ABD1,
	Lever_FixedUpdate_m38A8C59C66AFA0A56F7BF9F48BCAE6BC84883085,
	Lever_doLeverLook_mCBB37EDBD406EC547C9B5F5330B72CBE05BCB9A3,
	Lever_SetLeverAngle_m0A4314CFAA7A6846D218D02D6D990600D5C546DD,
	Lever_OnLeverChange_m9DE75905AE1E5870C636CB4A034E628A26B6A024,
	Lever_OnLeverDown_mF1BC34C08B96042B2063FD0D1225AAB1BE11DA6F,
	Lever_OnLeverUp_m4BFE33D0F16B4363FF0F082EB18C2DE829ED0E1B,
	Lever__ctor_mF8BD3C1EBA5AB75FC339D942AD210654EEF66760,
	LocomotionManager_get_SelectedLocomotion_m26B47CB9E812333C47C1520E9E85BE2D0885BA78,
	LocomotionManager_Start_mBC6667D05E4A4E8CB01B090BF7AC1553AB406828,
	LocomotionManager_Update_mFC83AA50CDE19A539FAF68A94E8C43B107BA251B,
	LocomotionManager_CheckControllerToggleInput_mA6F79880F7BD82CB6F9A8F359F6BDB2A2A99C847,
	LocomotionManager_OnEnable_m24C944EBBCF2890B8741C3BEF66C6396287B38DE,
	LocomotionManager_OnDisable_mFB9662766C1F2E4ABB76F279F3CC6E2B5511F9BC,
	LocomotionManager_OnLocomotionToggle_mE059366592694E10105E89C02CD2EFF3AC7494CB,
	LocomotionManager_LocomotionToggle_mF621EFCA898B252B0833C646D96DD0A8BBAA4366,
	LocomotionManager_UpdateTeleportStatus_m48C5E981174D32EAAED5244BE1B829BF6E1A3EB4,
	LocomotionManager_ChangeLocomotion_mCBB8F576E09992D74600C2796D7E2043D3515CC6,
	LocomotionManager_ChangeLocomotionType_m0503D36C3B8139538E7D9E720D326C5494C7BB21,
	LocomotionManager_toggleTeleport_m50256E600D35B417498941D5F11A6DABCD4E9A65,
	LocomotionManager_toggleSmoothLocomotion_m380AD027FB28C5B5F961D46572698629B1E5824F,
	LocomotionManager_ToggleLocomotionType_mC374E1A9D57E5767BCEED219A5E1D2615B1E4807,
	LocomotionManager__ctor_mF3D07E7728C3408BD4722554E233FC4716FD51FF,
	PlayerClimbing_get_IsRigidbodyPlayer_m36525AA9D6978819DDBBEF8AA7982D593668E831,
	PlayerClimbing_Start_m5B72AFF8C0B7963D30417304F9C7769977260D4F,
	PlayerClimbing_LateUpdate_m255945F6B5575F59206E642BAF724A0ACC21A60D,
	PlayerClimbing_AddClimber_mEB03045DE55A2CB924B25B6579C9F112B9945C74,
	PlayerClimbing_RemoveClimber_mC9BD75CB41216D2FEEC65DF04607B53BE8A665DB,
	PlayerClimbing_GrippingAtLeastOneClimbable_m3ED015E6A4C212F8924C5458151DCF3B8E5CC32A,
	PlayerClimbing_checkClimbing_m100386B3F13EC9A3D10E34342673173764EC37D0,
	PlayerClimbing_DoPhysicalClimbing_mC18F3ECB6F02A7B0B0891E1DAD9FE23BC4BED1CA,
	PlayerClimbing_onGrabbedClimbable_m53BB40E07C60A3ABA899A4ADB64F2113BEA54286,
	PlayerClimbing_onReleasedClimbable_m5A065889B781F6E6D97CA2595FABC0FA5EA1A7CA,
	PlayerClimbing__ctor_m753AA9613150B8919770397E353E94E53BD54C78,
	PlayerGravity_Start_mB4879FC67F338329DC92BAC8CB41DBFF20E1F3BF,
	PlayerGravity_LateUpdate_m40AEF37F58E1FFB633C2FCCDD042C950FE0A7611,
	PlayerGravity_FixedUpdate_m93E133642A4D9D04AA42C6DEFD799CF780122404,
	PlayerGravity_ToggleGravity_m0642CE5BADFABC298650446A8C3FDCCD357639A3,
	PlayerGravity__ctor_m29F80F82ABD1A4E673A3B75F519D080BD274A1B0,
	PlayerMovingPlatformSupport_Start_m267915A4D7274B8EFC6C6875BCED0E2E185AEF4B,
	PlayerMovingPlatformSupport_Update_mCB5820A5887AAADAD18A9A94978D25E639B498C3,
	PlayerMovingPlatformSupport_FixedUpdate_mF3C9EC3E7AD27ADE6A89C5C0736CDAA708D357F2,
	PlayerMovingPlatformSupport_CheckMovingPlatform_m972A47D29BEF03159118DA063EFB6D6EC789CE11,
	PlayerMovingPlatformSupport_UpdateCurrentPlatform_m042D9C90F5F597FBC98CF8F28542446C0D00D45C,
	PlayerMovingPlatformSupport_UpdateDistanceFromGround_m9CB47BACB358D98FFEF0D9425C52BB7E686B86A3,
	PlayerMovingPlatformSupport__ctor_m88AA9551118D68B718DBA5349B8C74E35EA942D2,
	PlayerRotation_add_OnBeforeRotate_mEBD292D98627CE23F9F67B7025F1B8239CD6FB79,
	PlayerRotation_remove_OnBeforeRotate_mB3A8283F33E2EF4C12433F1FDF8212DA4C437922,
	PlayerRotation_add_OnAfterRotate_mEC3DA6A47940598800713029A4104155EFBBFCF5,
	PlayerRotation_remove_OnAfterRotate_m962D0EB3785FE39DD113185D335853374F1AA117,
	PlayerRotation_Update_mC37460909BF57BC8808C87477EB6775A6CA1474B,
	PlayerRotation_GetAxisInput_mEA1E8D826BE9BA5C48F6AD8F33808A395901383C,
	PlayerRotation_DoSnapRotation_m05F34440F2920C585DBF38AB537AD529BDA300C6,
	PlayerRotation_RecentlySnapTurned_mA56719EB793972496259A4EC9619477E3DB2468F,
	PlayerRotation_DoSmoothRotation_m132E2B2506AE89F1DFBF1BEF9F981F0EC6DD9C1C,
	PlayerRotation__ctor_m3C3ED11159D38C5EB6FAAD3F37E89515290277AD,
	OnBeforeRotateAction__ctor_m733AFE124B79AFE2A5F18219AE40CB41AF22AE15,
	OnBeforeRotateAction_Invoke_mE17E479B50EDB533F7C46FB54A09A3E9C0F92E8C,
	OnBeforeRotateAction_BeginInvoke_m38EE8CEFA50ABAD2864A5FB8EABC13CD6329AC96,
	OnBeforeRotateAction_EndInvoke_m60AC27054049FB3256ADDC10325532441FA5A109,
	OnAfterRotateAction__ctor_mF81E6E61007FA10DA844E619EE262BF5C8E09DF6,
	OnAfterRotateAction_Invoke_m2892DBBEC5077E3D6AA38367477B23B67DC2E6E8,
	OnAfterRotateAction_BeginInvoke_m01B0EDF386CD90B800B1E0607E463C9A2856C38D,
	OnAfterRotateAction_EndInvoke_m5DA489D12DB803F09337CC925E89C1A08B7A3836,
	PlayerTeleport_get_teleportTransform_mF214F00D3A564CB60EFB3907B48B390F99EEB7FD,
	PlayerTeleport_get_handedThumbstickAxis_m8DDD505688A7823B032F27A6E8AB2A67BDE486BB,
	PlayerTeleport_get_AimingTeleport_m85DAE3B89D4351A52C8F485562FB040FB7DBC757,
	PlayerTeleport_add_OnBeforeTeleportFade_mD9ED8AAAEBF1D615A1C4F116DFFE8AE7187DF32E,
	PlayerTeleport_remove_OnBeforeTeleportFade_m22C92A686C7A96F6FA38FEB4E2CA4BB89EA65103,
	PlayerTeleport_add_OnBeforeTeleport_m70BFF304DD515BE9C43646663E60843CBE6BF3C9,
	PlayerTeleport_remove_OnBeforeTeleport_mED9B21414F6EE5B7359FF95C5596DF4FD8A8F90F,
	PlayerTeleport_add_OnAfterTeleport_m5C206A4B41BF8B7FE56BDD50F80A5059ABB68611,
	PlayerTeleport_remove_OnAfterTeleport_mC1004A815D0FB89D47E1FAABDE7FCCDFEFB69601,
	PlayerTeleport_Start_m2AEA5DA25BCBE32486884EB3E5F701DA938335A8,
	PlayerTeleport_OnEnable_m7CAC35C718CF398F44F9C103FFDD25A39D2D4E71,
	PlayerTeleport_setupVariables_m44E8A8237F6B82E8B691853E0FE36E419AD138ED,
	PlayerTeleport_LateUpdate_mFB2E314FADF810CF1F2FC5027C811BFA39C9C74A,
	PlayerTeleport_DoCheckTeleport_mF73FA56DFA031E6A7CE1D430D4E2DC9B519C46E4,
	PlayerTeleport_TryOrHideTeleport_m4F12F580C61DE99A284A44673C2E0FA6079CD327,
	PlayerTeleport_EnableTeleportation_mAC3EB704B839833292ECB4C1F6BAAED9596D3BE6,
	PlayerTeleport_DisableTeleportation_m2BB19E8462CCA888E464812CD9CE5E6F5AD847D7,
	PlayerTeleport_calculateParabola_m7830B1D508B592FFFA661FDAEBF2C7BACCD94F9F,
	PlayerTeleport_teleportClear_mFB066D26C47C8308F9FED6A833CBB7E249260227,
	PlayerTeleport_hideTeleport_m7D12D06234E6956ADB659CE9B3871C332FCD5AEA,
	PlayerTeleport_updateTeleport_m5882305B823F43A024B9E40375C179020DF20953,
	PlayerTeleport_rotateMarker_mF6A76873B151A4D86A45215239F7720959A46CEB,
	PlayerTeleport_tryTeleport_mC7BC5335B3DD3203E5ED0217B9910D71CD733BA1,
	PlayerTeleport_BeforeTeleportFade_m22F5853197AE551C15771D828B43395806303100,
	PlayerTeleport_BeforeTeleport_mAE6BEC5C19D2A9D82F74DF25DF1FC07BF6D1AC8C,
	PlayerTeleport_AfterTeleport_m3F78343E6D6BC3263150A3DC17FAD2CAA2470889,
	PlayerTeleport_doTeleport_m32D4C4948C071595A1BF2F3B9E4263CEB3FF366B,
	PlayerTeleport_TeleportPlayer_mE95F215801FD5C67CBB55FCC44605E201A94CAD1,
	PlayerTeleport_TeleportPlayerToTransform_m1EF265F993EB67586D737E652C6474BFCE10127B,
	PlayerTeleport_KeyDownForTeleport_mEE22B5590752D600C91167C9171D36B42FE084BF,
	PlayerTeleport_KeyUpFromTeleport_mDC31A6CABC63A5C761FD059D00B920FB692F17F7,
	PlayerTeleport_OnDrawGizmosSelected_m3F138F284660F7DBA9244484DE1AF891F5817F34,
	PlayerTeleport__ctor_m9CD166A3231AA4C35293C9BE8056C9EBB8CF17E6,
	OnBeforeTeleportFadeAction__ctor_m43DF800553234853DC5F69AB626A64102D995A70,
	OnBeforeTeleportFadeAction_Invoke_m014033EBB10CFC8EDAD1C2CD5DAAAC1B2E2DEF7F,
	OnBeforeTeleportFadeAction_BeginInvoke_mC50EE42F6F07916E953AFBDDC64E22283B27DFE8,
	OnBeforeTeleportFadeAction_EndInvoke_mDA25DB39CE6782D067ABB57FF66648318447F90E,
	OnBeforeTeleportAction__ctor_m869BB218A0C8C0F7A7D02EB4B5748152FA1E3339,
	OnBeforeTeleportAction_Invoke_m3229BD04682D45A05DB23CCC353B37341530823F,
	OnBeforeTeleportAction_BeginInvoke_mAAA468D615EE3DF33B5F05EBCB94FB189F93C95A,
	OnBeforeTeleportAction_EndInvoke_m974572E1B0C427CD88CC416BF3F2826A8D36E05D,
	OnAfterTeleportAction__ctor_m144D46179D12B9E7B77909A360A614F1BC94C83E,
	OnAfterTeleportAction_Invoke_m0DE0EABCAAA58D51C3E2C6971658DAE0215438D6,
	OnAfterTeleportAction_BeginInvoke_mB81CBB51D9CC63BD1C580175B17B40A763086CEA,
	OnAfterTeleportAction_EndInvoke_mABEA5AE907CECDC60241DF05D874F33E15142FDF,
	U3CdoTeleportU3Ed__83__ctor_mC7CB68F3EB217879E4D20BCCF0BF912D803A165C,
	U3CdoTeleportU3Ed__83_System_IDisposable_Dispose_mDDAD4DD996A850B509B088B6605CC5A483051E72,
	U3CdoTeleportU3Ed__83_MoveNext_m95097F669BA8A851FBE5FCA3E8134D2F9EEC7601,
	U3CdoTeleportU3Ed__83_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB84B30B02B24E4C9FA947EBD401F66EAB4EEADB1,
	U3CdoTeleportU3Ed__83_System_Collections_IEnumerator_Reset_mEC6E8F25628B1167BA69BF5492410C162D82E3D6,
	U3CdoTeleportU3Ed__83_System_Collections_IEnumerator_get_Current_mA6360FC5779A20C9E159B61FC2767947ED31A751,
	PointerEvents_OnPointerClick_mA0597E704B3636E00D96D2659E305E29FEB18799,
	PointerEvents_OnPointerEnter_mC73A248D7EE074DC9837AD1A5C9D40E0D672C124,
	PointerEvents_OnPointerExit_mC1B23407FA76BFB4CABF4E8EA78A6D7743F4C629,
	PointerEvents_OnPointerDown_m7FF27624FA5D42DAB92DFB5F35ECFF040D05601B,
	PointerEvents_OnPointerUp_m61E064680AD74E710C3188C53A5C955968F224C2,
	PointerEvents_DistanceExceeded_mAC16DF23DE2415EF79DF917CB28267A2649A3236,
	PointerEvents__ctor_m5AD08409C0295E23405D5D68B3F5349411737D75,
	RemoteGrabber_Start_m9BACF630D3D2E00B5C0B7F67BC688335410CABF3,
	RemoteGrabber_Update_m3E43533A6AC51EF00CA3ABDF9E6C2B0E112DE75A,
	RemoteGrabber_ObjectHit_m34EDCA7C5C82A0D615DCB085661C65BBA0D239A0,
	RemoteGrabber_RemovePreviousHitObject_m144838D42809F2153CEDADE1723ABC2F4A5005B3,
	RemoteGrabber_OnTriggerEnter_m82AAE00CED45B2AD2C9208C4E473ED31F796A054,
	RemoteGrabber_OnTriggerExit_m2F68FCC7CBFFBCB309147965DC07A14BA8EC0079,
	RemoteGrabber__ctor_m8FA0B5E749F4F50F5FA6A6C0FF02241AF9A73BCC,
	Slider_get_SlidePercentage_mDD5557043C513F08DD1400CC1F4C6910DFC94BBC,
	Slider_Start_m992F0EDA809BBEFDD6B47AA7664C3EAC52A29DD1,
	Slider_Update_m29CA9A4B42B98E1784B763B715F8C526987765EA,
	Slider_OnSliderChange_mCD5E819F417B3B71A33C92F288DA678A65A95843,
	Slider__ctor_m4DBA547A5B37B65FD5F31678E4750AA050B57F5F,
	SmoothLocomotion_add_OnBeforeMove_mAD779D5B2C7DE5561B09E2155664BC9EB66CF780,
	SmoothLocomotion_remove_OnBeforeMove_m6E98D62F6DF0372C8B1B2FC6278D3FB020E13B47,
	SmoothLocomotion_add_OnAfterMove_m1615DD7DEF761D1DF017DE9C45AAD8F0BC12AC4A,
	SmoothLocomotion_remove_OnAfterMove_m644B9E0CF62BEF150AC825DC7A76EBF01BF777E3,
	SmoothLocomotion_Update_mBBE01A976A9D3E1AF5FB20961DD06CAFA0996CF6,
	SmoothLocomotion_FixedUpdate_mB7B1E63602EDE31D59C65F39C7E844A1784C87B7,
	SmoothLocomotion_CheckControllerReferences_mB4CA93B93A1A68942838F6807D873FFF82E10A87,
	SmoothLocomotion_UpdateInputs_m154451C47A85B4038D98B7596D5CDE626120ABD1,
	SmoothLocomotion_DoRigidBodyJump_m4BFB14CEBC44F45C4C3E4C9072E70513ED7DCF04,
	SmoothLocomotion_GetMovementAxis_m0A4B174246AC36B8F7DB845F5E5DA0F0873F5337,
	SmoothLocomotion_MoveCharacter_mC2EE8A27F7CD299D94DCAA73181F9540F15A9D64,
	SmoothLocomotion_MoveRigidCharacter_m14016A2626D5F344E57456E8194C9B30E49A8BE1,
	SmoothLocomotion_MoveRigidCharacter_m41D7C1EECFB4FC0017426B1EDF0B78B8023ED45A,
	SmoothLocomotion_MoveCharacter_mD848740DDDF05DBC5A7BD947A51AA78886F5345B,
	SmoothLocomotion_CheckJump_m55C3B3E4CB67B2C55AA04026BCE17DC69291A4C1,
	SmoothLocomotion_CheckSprint_m310A3538226035911A29786C2F390084B7FBA4CB,
	SmoothLocomotion_IsGrounded_m3BD6160FDAEB468C426EE749CCC3D93E42AD889C,
	SmoothLocomotion_SetupCharacterController_mA70A22FFA62DBF8E6D8D5495D7295D635DA4BFB7,
	SmoothLocomotion_SetupRigidbodyPlayer_m3D2FB68A2B2F3073387145BC04454076B8E2B073,
	SmoothLocomotion_EnableMovement_m270F5F4BE1EF0D8D2F4A84FA745F27F2518B0257,
	SmoothLocomotion_DisableMovement_m28B0CBB7A70152CA216F2789BD97B29239EA5EED,
	SmoothLocomotion_OnCollisionStay_m3851EC1329C51267B652898C5C6ACD5574DCF7F3,
	SmoothLocomotion__ctor_m69F34A9C73372C67D41955079BF686A83FFB663E,
	OnBeforeMoveAction__ctor_m4CB0B5517A245C08D1D742818D1054EB74A30A86,
	OnBeforeMoveAction_Invoke_mEB85846742941F2CF0C05939DDD9FC39B2CC0311,
	OnBeforeMoveAction_BeginInvoke_mFA47CBDD8F0408F59EF0EAF68018913E4EA0427A,
	OnBeforeMoveAction_EndInvoke_mF139491E3F291413611517A33C0825CE32B2301E,
	OnAfterMoveAction__ctor_m5D5909C587EEC3AE83879A54A8B494FA32D9AC0F,
	OnAfterMoveAction_Invoke_mC5D6A5932EF93FECE667AD90A05BD9E858577468,
	OnAfterMoveAction_BeginInvoke_mAF8D34335BA2FED7FE9E19110B55B50F94277DCD,
	OnAfterMoveAction_EndInvoke_m0D9D13E9A77D7D875DEE5CD95BBBEEA82A1EA494,
	SnapZone_Start_mC415999D6A25E21F1C4335249EA6B0B97942C76D,
	SnapZone_Update_m6CB16F4F10AB31F35E3B2CE41DF3CE1D12894B03,
	SnapZone_getClosestGrabbable_mAE5222BC1F161BA94DF3403716E931436EBE6AF4,
	SnapZone_GrabGrabbable_m8CC39A7003ACB96F4375D38C71A17878B0646A20,
	SnapZone_disableGrabbable_m01395286E18A87EC2F528D18E0130239CA2D6891,
	SnapZone_GrabEquipped_m19762FCD313B38D14A109AB937AAB7FB60214229,
	SnapZone_CanBeRemoved_mC46BBB09A9EB63EF2190CA048E4AB85E42E51956,
	SnapZone_ReleaseAll_m44658CA73C0B328B5EDB279805402699D1BE662F,
	SnapZone__ctor_m27C8333659D2722A92CA9C4C349D153A549E1CF9,
	SnapZoneOffset__ctor_m4C5C5A6864B47BDFC8150F521A2B15E1153F6A94,
	SnapZoneScale__ctor_m9512AB05C931E2E458A0840FE1EC2EEAC412E81E,
	SteeringWheel_get_Angle_m7D5BD425D256B54294131C5F1B3059FE8DD20C18,
	SteeringWheel_get_RawAngle_m91A3726644A4CF5F7467800BFC6FA084C362D43B,
	SteeringWheel_get_ScaleValue_m4232332170D69C42B135BF0B56449F28C5A33B76,
	SteeringWheel_get_ScaleValueInverted_mCDE9E0A126617919A9536CC10DD258F7FD38ED74,
	SteeringWheel_get_AngleInverted_mE81DFB9BA027FF6A5168DA2323B1DBFFAA5081FE,
	SteeringWheel_get_PrimaryGrabber_mF3D5F3A9AB11A26C81CA07F617A7F22F0835AA7E,
	SteeringWheel_get_SecondaryGrabber_m9A1638D742BA5372EDD5BCAC26FFCBD1D383712B,
	SteeringWheel_Update_mE38521DA886E5163496914F207DCF909D83D7E36,
	SteeringWheel_UpdateAngleCalculations_mCC23DA8519DD1400E518F91DF4E218CA1B5DAD0E,
	SteeringWheel_GetRelativeAngle_mE28475C7A21349F427E55044713568F42A231321,
	SteeringWheel_ApplyAngleToSteeringWheel_m4F31382FDBD3F7C7D8229BF2A4BE2FFDB2F7456D,
	SteeringWheel_UpdatePreviewText_mC55BED46B93B01C0945E685B4A059C7AB64EACBF,
	SteeringWheel_CallEvents_m41A5165FE40E5F7DE53E536F7E89D7DE1CB64E48,
	SteeringWheel_OnGrab_m3537B7970BC77B6BDE7AB94B6E23E9259AC542C1,
	SteeringWheel_ReturnToCenterAngle_mBD2628731E08658D13096B234C7AE5E2D945383C,
	SteeringWheel_GetPrimaryGrabber_m6161DAC4F9D0C81366A82DFFE80E0621CEA8A22F,
	SteeringWheel_GetSecondaryGrabber_m007B5CA44DDC1E897AFA4D62EE2BE8FF85B56881,
	SteeringWheel_UpdatePreviousAngle_m8C8822E655133FD8EF773B340104E24927D6F4AC,
	SteeringWheel_GetScaledValue_m23BE2DEBB3321D27E0F8355DA027A35E6F8B5381,
	SteeringWheel__ctor_mA7E943C394B9CCC7B5819BDEFBEA803550DFC3B3,
	TrackedDevice_Awake_m94E6306A4B7D265F127E909C5159E6EE9A591C70,
	TrackedDevice_OnEnable_mB762C7B02E310D85F047075B609668B2DBD43498,
	TrackedDevice_OnDisable_mBA7AF6EA9CAF616A3227312979390BBCD2EABA4D,
	TrackedDevice_Update_mF1295DFC84BDD17EEA3D6D1E01B9E81D359F3585,
	TrackedDevice_FixedUpdate_m1F072E07B54554E27C5A366049052DA7E830DC01,
	TrackedDevice_RefreshDeviceStatus_m19E111F81F1A8E599B84BEF97EDB03C948570016,
	TrackedDevice_UpdateDevice_m6B0BB134FF4A6F9CA306E56A728F55C385A374A3,
	TrackedDevice_OnBeforeRender_m207F3444CC3B818688FCAB95E2EDA84336862D1C,
	TrackedDevice__ctor_m653C53AE71D4992C56358CBB79184639D9C52B2C,
	Arrow_Start_mACE000AD183FE265E758D93549FCAEDE3AF85C24,
	Arrow_FixedUpdate_m96DFEA74E387C0D4F7970417F02996473C003196,
	Arrow_ShootArrow_mF6F56567CF1DB0D6EE58CAA0B53EF14230C07134,
	Arrow_QueueDestroy_m4FD78102E06AC8862CA1C90A897FEDEFE0EAC12D,
	Arrow_ReEnableCollider_m0F70A05EFEDFFE32C191651FED9B4B456E75732E,
	Arrow_OnCollisionEnter_m835D0E1D37222AE76FAE92C41444C10449B897C1,
	Arrow_tryStickArrow_m033EB379024793DAEC0D0D1A9C8329D964D44CDC,
	Arrow_playSoundInterval_m1144E89FCC32A6CC508721E57C8B3050F4D4B3A4,
	Arrow__ctor_mB43957529F437443848F41C7E9AE2B6666535E81,
	U3CQueueDestroyU3Ed__14__ctor_m922A344B5DCA80DED43AE5CC16AECA5446543B78,
	U3CQueueDestroyU3Ed__14_System_IDisposable_Dispose_m9025E988B1CF725AC796F368CFE79A5E368C40D5,
	U3CQueueDestroyU3Ed__14_MoveNext_mE210CCA7AF752E12371C1414422CD6E143054FAF,
	U3CQueueDestroyU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m21AB1125753459BA0322D4BA486A032E61FC33D2,
	U3CQueueDestroyU3Ed__14_System_Collections_IEnumerator_Reset_mA7BA7204A429BE8B1C8D8B2B9C0F45F36C912459,
	U3CQueueDestroyU3Ed__14_System_Collections_IEnumerator_get_Current_mB583407068D8C0B99FD8D209755D6729019AEA3B,
	U3CReEnableColliderU3Ed__15__ctor_mE3FC175189E7FC481725563B65732AD1A89F50B1,
	U3CReEnableColliderU3Ed__15_System_IDisposable_Dispose_m28C7779546BF806504E78B1F9A9413D72C041099,
	U3CReEnableColliderU3Ed__15_MoveNext_mF79E629D706251472C6175491227892588A37636,
	U3CReEnableColliderU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFDC4EA4610B1AD32F42F582397407D360C069A1C,
	U3CReEnableColliderU3Ed__15_System_Collections_IEnumerator_Reset_m20D294F8767255E36FD654721876BA0BE40699B7,
	U3CReEnableColliderU3Ed__15_System_Collections_IEnumerator_get_Current_mED43E716116B9AFCF45581A30EA8AE86AA635ACC,
	ArrowGrabArea_Start_mE827A10C7B60A39F880F16CF9AB52A0FC2A3983B,
	ArrowGrabArea_OnTriggerEnter_m7CD004B0C43D14D0E35DBD24F5C17A7C8B74EBED,
	ArrowGrabArea_OnTriggerExit_mEC366B949998F0E41D3A1643F60FEECF29FE1523,
	ArrowGrabArea__ctor_m05124CA3E40339C63292F72362E071FCD1705B05,
	AutoGrabGrabbable_OnBecomesClosestGrabbable_m5E2CD463F0B9BFEF686D17876B82D69702872287,
	AutoGrabGrabbable__ctor_mE3EC80BF7CE854F653497000FE7EE4D5B0DE67ED,
	Bow_get_DrawPercent_mBDD2C6608A01C96B5E12C05244878CE03DA02865,
	Bow_set_DrawPercent_m17BCBAA4346B06BBC1D0216E8200F3E90F1A8A84,
	Bow_Start_m9D0D06BFCC61BAA15AEE3C95DAB83B5C3F0C5D81,
	Bow_Update_m5E5F1668FE1128C0AC561D7C47F9873F02CA6700,
	Bow_getArrowRest_m624FA23820E53CD03FFE2AA07EDF0F72DB20E71D,
	Bow_canGrabArrowFromKnock_m84A1321BF13B12E5C14CEFD0556E8E37BC66F9DD,
	Bow_getGrabArrowInput_m82015C94D7AE41F9328E950952F10144F2F70EC3,
	Bow_getGripInput_m67270EC70AA2D1F591CC7AB1A4B5CDC32668A15D,
	Bow_getTriggerInput_m47BC7A14649E753414D49F56E6C8285D7F0B9CD4,
	Bow_setKnockPosition_m76559A5BC41A252A42104E539E2D9976F69B9F75,
	Bow_checkDrawSound_mA44801D915D394630923B46F4776BFCABA877D22,
	Bow_updateDrawDistance_m5EA7C796FDE83F1AA532D96D9AA94F240E4ECF2F,
	Bow_checkBowHaptics_mBF3894B2410F12AD09250948CDD20E80297DC4ED,
	Bow_resetStringPosition_mCD1878FB440B8FB26FBFB7CE0DDE0F29BA658737,
	Bow_alignArrow_m1C1B466B7862443373F6B104BC13BF124B5090CE,
	Bow_alignBow_mF845696336235C0598842E0DBD6A120208279F34,
	Bow_ResetBowAlignment_m14D5E4731B733CFDD76A5704D6257ACA88D71EB2,
	Bow_GrabArrow_m5EDAF162BAC2372358E551D4229DC71313B504FA,
	Bow_ReleaseArrow_mAC9F6C0B6A4195F35AC902DE370B24E056308D3D,
	Bow_OnRelease_mA42A363984A7D17E02F8DAA5123A4BE1C08CE253,
	Bow_resetArrowValues_mAD7BFF3496FF983E98D93F9993234A25C58C4027,
	Bow_playSoundInterval_mFF24C8B922FE92EF41DEE6D2F38332F31976DA79,
	Bow_playBowDraw_mAB05F95A93820CDE0BA8FE1BF311649AC20ECD65,
	Bow_playBowRelease_mC3A710EFF59CEAA91FA8B51D6750ABECC305F02D,
	Bow__ctor_m951BBDA0CAADAB88F232E69A416015153ABA6C19,
	Bow_U3CcheckBowHapticsU3Eb__43_0_mD0A0833A82F341279AA29E82AB578B22114EFEC1,
	DrawDefinition_get_DrawPercentage_m0AED0E7C8352ECA58AF2AB0AE30E09D119C59582,
	DrawDefinition_set_DrawPercentage_mA2C4167F9F224F9D8A751D921D7F9CA7FAA1E33C,
	DrawDefinition_get_HapticAmplitude_mE2209B8ACF279D6EDAA148EA49FB273AA0DE7A5B,
	DrawDefinition_set_HapticAmplitude_m06887EBAF0FA54B8BD9D291A2F3F34137F821BB7,
	DrawDefinition_get_HapticFrequency_m91963595ECE0CA71C8D428D311A7D86926FF8B18,
	DrawDefinition_set_HapticFrequency_m9A734EC004E29CBFD8DD10085F279022979CD3E0,
	DrawDefinition__ctor_mA2A66B7D89E40377508CF90B606D9EE4099783C4,
	BowArm_Start_m7E5917F8A4EDCB129047ED01728416FCB9B19176,
	BowArm_Update_mE5D893716591C6DDC36A6E5B505AE0995C562C93,
	BowArm__ctor_m4EF8D498B9AFB4FFDB5B518D6D00B77E6A9A5D2E,
	BulletHole_Start_mC54745D50973B60543474E27AD576C7F23A996F1,
	BulletHole_TryAttachTo_m7EE9F913CB1FFDC013216BDD13448E36E6F34C45,
	BulletHole_transformIsEqualScale_mF65371E8F6489A2E4F0B214D23BA50E94878B839,
	BulletHole_DestroySelf_mD05C735B904461DEE908F0FDF37D19612F27AD52,
	BulletHole__ctor_m5FFCA6934A65784C9E8E464D81D5C7580CD2F490,
	CalibratePlayerHeight_Start_mAB3A842A49CFDFF603D79E0DBF8EE091F56F4AA4,
	CalibratePlayerHeight_CalibrateHeight_mA5DD0F54DF03F859CBF273930206259269C8A186,
	CalibratePlayerHeight_CalibrateHeight_m8C57C0D63C220F1DDF55E35ECC5B7EA643617118,
	CalibratePlayerHeight_ResetPlayerHeight_m32DC98C98D7E6A1E94F594FFE7A57DDD48FD52F0,
	CalibratePlayerHeight_GetCurrentPlayerHeight_mF145DBAE01C900DEF963170118CB33F9E7BF1AD9,
	CalibratePlayerHeight_SetInitialOffset_m0C95591906687F3EF68307AAB58877D60B8D3723,
	CalibratePlayerHeight_setupInitialOffset_m9394FF545D22A46926BBFA5C68F2394C232D41B8,
	CalibratePlayerHeight__ctor_m2500F880D743AC5ED5A9316D385BA188B20231D5,
	CalibratePlayerHeight_U3CStartU3Eb__5_0_mB706AC833CD8D8F38F4F975A9C8C754E59CBAE67,
	U3CsetupInitialOffsetU3Ed__11__ctor_mD7F1EB16000D56A59AF5AD7EA59A659751CB2E84,
	U3CsetupInitialOffsetU3Ed__11_System_IDisposable_Dispose_m88A359114955415273E8ABE95838D980E17A1C22,
	U3CsetupInitialOffsetU3Ed__11_MoveNext_m3C4ACEEE590FA0E7B8B49F943182AB69A1459E32,
	U3CsetupInitialOffsetU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m13C3A6E2AEF617A3901F844FA128480E0E6EC564,
	U3CsetupInitialOffsetU3Ed__11_System_Collections_IEnumerator_Reset_mFC402F746E7B773FD0900952870B6DE59927B4A3,
	U3CsetupInitialOffsetU3Ed__11_System_Collections_IEnumerator_get_Current_mDD4F10151E03A4EBCD5A51B19E51512F0278E2EB,
	CustomCenterOfMass_Start_mB9FC7963FDAF4219AC72C9B77F65F8243F7101DF,
	CustomCenterOfMass_SetCenterOfMass_m82E4F98426956AB9DF8EC19EFFE4ACFC28F4ECAD,
	CustomCenterOfMass_getThisCenterOfMass_m62E50D006F2B7226FAC6D8248025945B28294DA8,
	CustomCenterOfMass_OnDrawGizmos_mB796C717A58F974F3C4F63A3CA6B3B5C130C3AAA,
	CustomCenterOfMass__ctor_m15D2031B9C5EA367819170BDD52A4C9CBA361908,
	DrawerSound_OnDrawerUpdate_m73C9C4D9D8EA79FA50447E02DADD9302C9020FDD,
	DrawerSound__ctor_m19D538C794741DCE169D19512051733F9A11122E,
	Explosive_DoExplosion_mFF220F821E0445F3352D398ED269FE3722DD7B7C,
	Explosive_explosionRoutine_m567F948B0AA00884D57487E5BA31BB6CF1846A22,
	Explosive_dealDelayedDamaged_m4AFCB6647FAAB4AADAF76A05B0484A707DAFC9BA,
	Explosive_OnDrawGizmosSelected_m164D43FD92EE29B88BA4F06CD182CDBD5B574541,
	Explosive__ctor_m7552584D30FC2DCBBF1CED3ED9CFCFD11BFBC45E,
	U3CexplosionRoutineU3Ed__6__ctor_m4D2C0AE2979426A68085DD2D67C2523DC9D22F0E,
	U3CexplosionRoutineU3Ed__6_System_IDisposable_Dispose_m61BF921FBD39692E4F3B47362ED457A002E06B87,
	U3CexplosionRoutineU3Ed__6_MoveNext_m8E1CE453031BB0723722A11727CFCBEA0699027E,
	U3CexplosionRoutineU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0E290262394572A563762816CF297A8C1D705CD0,
	U3CexplosionRoutineU3Ed__6_System_Collections_IEnumerator_Reset_m066FD066123C85628E1A57D1C874A54757983522,
	U3CexplosionRoutineU3Ed__6_System_Collections_IEnumerator_get_Current_m0E6107B06543A67A36F43A5AA2D5772D706CAB0B,
	U3CdealDelayedDamagedU3Ed__7__ctor_m7B98EC93310F920F79F326050F9719FBABE62BF2,
	U3CdealDelayedDamagedU3Ed__7_System_IDisposable_Dispose_m58B49841EE49D730E6581279F45813A7565E3F71,
	U3CdealDelayedDamagedU3Ed__7_MoveNext_mE9F4E293497DB94BDC8326C997A846846A279B4D,
	U3CdealDelayedDamagedU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB5012D37CE1E7B6FBC3AA13D9EADC6F27D948976,
	U3CdealDelayedDamagedU3Ed__7_System_Collections_IEnumerator_Reset_mE80C6FE5262AEB6EBDDE0B19D7425018F6B3152B,
	U3CdealDelayedDamagedU3Ed__7_System_Collections_IEnumerator_get_Current_m8638F2DCFA1C9C87DC77B7B27AD1BED7B2076D53,
	ExtensionMethods_GetDown_m5A0B33F9B42B036DD7D6BC7B5E977DBD0090B82E,
	FPSText_Start_m5A9E4FBF80912EF14B782495DF169D754F348554,
	FPSText_Update_mC90A6D7A9AF5874C36C1021B4B35D68464C7E2C4,
	FPSText_OnGUI_m1B678EF8177CC715464D1EC56EF3B1B1F16863C2,
	FPSText__ctor_mBF8B285D21AAE9E714C43B46787723ABD45102C1,
	Flashlight_Start_m265B0F0FDE1DAF53DF6D4E29239218828176267A,
	Flashlight_OnTrigger_mAF8A2B53A2CB89754AB1548AC77FFB13C384B432,
	Flashlight_OnTriggerUp_mE4226B596A18244455F65658D848C30B068E501D,
	Flashlight__ctor_m13C9931920C72C38F06C9064D2C0C03E2B489B61,
	GrappleShot_Start_mBAA3EEB4B9FD27E566A8A3503968D68BFBAF380F,
	GrappleShot_LateUpdate_m34C3F1CCCAA0929DB587F95224039BF1F504EB73,
	GrappleShot_OnTrigger_m53462BFD6F0CD5630BBC8FCFF5014CBFE9562291,
	GrappleShot_updateGrappleDistance_m0235D09E0E10C35443C2BBCD23EC27259D6AF789,
	GrappleShot_OnGrab_m9E7617878B6DBE2266F1233B14DE8C161D1EA7B0,
	GrappleShot_OnRelease_mD926B210B95C7BC4E349DF5B3CC85F350D4AAB4F,
	GrappleShot_onReleaseGrapple_mB6EBBB0232735EF07157B95A60143F641F331F27,
	GrappleShot_drawGrappleHelper_mCABB9DB5F6610D22CB45BA44170BCB00DD2B8FEB,
	GrappleShot_drawGrappleLine_mCDFFB7A5D1A70B6B395805C46C3F2E8B481B4F5D,
	GrappleShot_hideGrappleLine_mA753CECEEB84617D1935DF3888EF735CEEA43D4E,
	GrappleShot_showGrappleHelper_m6A7E2FAEB57BE6EB5128C99F5409EF31D6C4D8CC,
	GrappleShot_hideGrappleHelper_mFD118BE1C008E9D1A0968C2456E60840188B4C80,
	GrappleShot_reelInGrapple_mE8229D69408856FEE14CF8D60670C1A0E8422467,
	GrappleShot_shootGrapple_m3D72B56DEAE9BA0EC4D900251FB640D16FD46414,
	GrappleShot_dropGrapple_m704F5C25242EF60BC41FC699BD71987B65199E00,
	GrappleShot_changeGravity_m8A73ABA947D4FB33A74BC65118C682BB2CB1F083,
	GrappleShot__ctor_m8392AB16FFE0677FE377EBF2AA09EE79070EF0ED,
	HandJet_Start_mACD37866AFF3FD4FAE52DFAE2EB9E0226259A9E4,
	HandJet_OnTrigger_mE30D9C2A6EA64D7C79BA611FEC4E3993E588FEAC,
	HandJet_FixedUpdate_m5B6BF672A632D813288BF15AB7D991519BF6C790,
	HandJet_OnGrab_mE78D6F6380C4B0B14C7881A679153EA500950D86,
	HandJet_ChangeGravity_mD641F4E05D94A30B9F5B85F7E0266D17D1C2BBA5,
	HandJet_OnRelease_mA3A5D574B6BB92102E8CE71164F0DF61938D46E8,
	HandJet_doJet_m4359124626F45527F9B344D7148EC14077DB50E9,
	HandJet_stopJet_m3C19D05A06FDDB87F0A3A2DC5A4D0D454E83DD9F,
	HandJet_OnTriggerUp_m673EABAE1AAD9ED4849AB766074874353D0E64A3,
	HandJet__ctor_m440AC77EBD0A842724DFA4409D6816F1EA826582,
	HandModelSwitcher_Start_mE8EBE22FE29F15D135CB0CB079A0D638B22137A8,
	HandModelSwitcher_OnTriggerEnter_mB3A551B0A490A89EFD57D48BD9AE0D40EE2457BB,
	HandModelSwitcher__ctor_mB7A09AFD2290D0214273156E8CBC8A124B760653,
	IKDummy_Start_m073A6328A79C7E3F773255DD248E8D1470CA1592,
	IKDummy_SetParentAndLocalPosRot_mF40BB8432A8DB412DEC1881237B55AE9C9C329C0,
	IKDummy_LateUpdate_m4D4072365D6FD9D549C9DBA57C983FFE02B8797E,
	IKDummy_OnAnimatorIK_mDB045590843411F78C6A7EA244B293C6B8B3C6BC,
	IKDummy__ctor_m89334DCF79821A9F3C2364733766E86FE994D377,
	LaserPointer_Start_mCE5E0C21CD0961CB5B11BEC3B63253C38ACCF558,
	LaserPointer_LateUpdate_m76170C22F264D9A97FF5DEF27046BCFEFA3BD6DA,
	LaserPointer__ctor_mC3E7F5691E9DA94AF1E05A35185D23776CC08A98,
	LaserSword_Start_m818B5E765788C598CAAE1233D163F7237002BED6,
	LaserSword_Update_m3915F29453AF8D03011B487B9DD42751E592E487,
	LaserSword_OnTrigger_m18DA6DFE14F95DC886003C47AEC7B9E8EAE85A16,
	LaserSword_checkCollision_mCF875F460D470383E013FFA41BF16F740EF6EF55,
	LaserSword_OnDrawGizmosSelected_m9AFCB27121A775D23D3D2EFAD9203EF1343EEFEB,
	LaserSword__ctor_m88D6E10518842ADF9E23F5234911857ADB9629E8,
	LiquidWobble_Start_mB7657D43F8C86FFD27097F1C452A9CC5CEABBF72,
	LiquidWobble_Update_m256077E9EEC35186E659B8A5BDFA295AA2182FB7,
	LiquidWobble__ctor_m0D7DD52927DCB0FB0C78063752C1155C0F81EAAD,
	Marker_OnGrab_m955881271C7EF7E885232632B2AEF6C6F2F3291D,
	Marker_OnRelease_m221D02CA988DAA3D29A9CE466E7D448BC0AA838D,
	Marker_WriteRoutine_mB1692C2867273C5C381E3C043A0FA6EA1A305471,
	Marker_InitDraw_mA37F4EE7D70851BB2D2CE08F488298BAF720CD01,
	Marker_DrawPoint_mEE35C86054025A7B9E50AFF4249AE561481EAC56,
	Marker_OnDrawGizmosSelected_m5EBFBCB72BB0FABF62B956A9C155687221C952AC,
	Marker__ctor_mC14FEDBA3F69B7949BDBA203F7C49469CC187742,
	U3CWriteRoutineU3Ed__18__ctor_m9904A28786A35DF7C404C1C7AD3CD193AC99CBD0,
	U3CWriteRoutineU3Ed__18_System_IDisposable_Dispose_m44F04732D353CE0DA94B609AB77B944B0ECB4C26,
	U3CWriteRoutineU3Ed__18_MoveNext_m72DEF6A5A33AC0038E2EF4981D45770930AD6136,
	U3CWriteRoutineU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m425C562E9D830B31722521E69EF4FFFBD79F8DF5,
	U3CWriteRoutineU3Ed__18_System_Collections_IEnumerator_Reset_mD0493F5470C504CF1063DDD4150D993895EEB667,
	U3CWriteRoutineU3Ed__18_System_Collections_IEnumerator_get_Current_m1E2AF7A6404A9780D08C3883F31DCB29947B5BA9,
	MoveToWaypoint_Start_m7D61C3D54EA8F356BC20CCD0402BE467A02CB53B,
	MoveToWaypoint_Update_m906260C7936DCDD48119DA15CF9DC78D0DE40556,
	MoveToWaypoint_FixedUpdate_mB220EF2F1A671349EE31A9C9DABFF0113291810F,
	MoveToWaypoint_movePlatform_m978FF3DDAFC42486F7B1B3A544FE88A64D0E65FB,
	MoveToWaypoint_resetDelayStatus_m6BD947719C7913894783DD152F99D4948EE5F967,
	MoveToWaypoint__ctor_m5946D0C4B3C7986439A61F971E717790125AA686,
	MovingPlatform_Update_mDD6E84121C69F0691BEE435EBA8EEBEE328AC5E6,
	MovingPlatform__ctor_mAD14B3DED4729F1BAB73910AB5DE02EC4558D15C,
	PlayerScaler_Update_m0FE18A240032BA39CCFA3B66241F222D50B5149C,
	PlayerScaler__ctor_mD939A5A79AF6799D25FBA13DE84469184434E53E,
	ProjectileLauncher_Start_m474367587C569265BB9D0E862B338206F471AC08,
	ProjectileLauncher_ShootProjectile_m90FC3B650EAA0ABD84A5C043FAC375F57B939219,
	ProjectileLauncher_ShootProjectile_mEB905A0BE3EC08A15EE5E66DF701792729A8C33A,
	ProjectileLauncher_SetForce_mE558C5D25BD4D8FC2022F9D6178005A65A1F363A,
	ProjectileLauncher_GetInitialProjectileForce_m4B7E79533C87E984AACAEFD0DA02B8470B6627A0,
	ProjectileLauncher__ctor_m60461DFF1548AA5291460DF742E48D865AE08C3D,
	SceneLoader_LoadScene_mEC58A5D5CFC9B2E72C783E0E11875C97F6D708A4,
	SceneLoader_FadeThenLoadScene_m820E1C36C708034F9FD87A355A5D4BD0CEFE9033,
	SceneLoader__ctor_m761B6D58B22486514F85F6DB1ED7DCEF624A8DC4,
	U3CFadeThenLoadSceneU3Ed__6__ctor_m2DA8CFEABA4B0554851069E82E403638B4A01D32,
	U3CFadeThenLoadSceneU3Ed__6_System_IDisposable_Dispose_mF8486D193B55BD97D418F3E4D0721A60092DEE80,
	U3CFadeThenLoadSceneU3Ed__6_MoveNext_m49B07F099BB7822FE4B35C2DAF55E4D70DC6CB11,
	U3CFadeThenLoadSceneU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7F1E153895F5133C628633D98A08D8E49D71584C,
	U3CFadeThenLoadSceneU3Ed__6_System_Collections_IEnumerator_Reset_m6B5C1BFC6A7D1CDD2CC37D849D385E724CDED218,
	U3CFadeThenLoadSceneU3Ed__6_System_Collections_IEnumerator_get_Current_mEFBEBF6A54C6FDA2ED7A4654A603A98221F49038,
	SlidingDoorMover_Update_mA7E680F08859A08A54C17FC8155EFDAFDAC0C09F,
	SlidingDoorMover_SetTargetPosition_mEF646D285C1A7AC7B9D3105FDAA18A6EA86ADD44,
	SlidingDoorMover__ctor_mCB5FB879BED01ED95C701ADE6FDC55D6A193C5AB,
	TimeController_get_TimeSlowing_mE05BB36E99ECF89E9642B4F9B0ACEC7EE6756FE9,
	TimeController_Start_m3E4DC4D16CF851EC9D89B0A175D303DE93E31E37,
	TimeController_Update_m66920FC371F3A6010E24D693FD32AFCADC1A77B7,
	TimeController_SlowTimeInputDown_mF6D2E0675347A32195A9EC2B2697A69FFD4B8177,
	TimeController_SlowTime_m192B560DEE339BB0CD0500BF44347A98FB1EB3EB,
	TimeController_ResumeTime_m8F3D10DA86373E9FF44BFC7D16B5A270D7DB84D1,
	TimeController_resumeTimeRoutine_m5C89FA34B6C20682DE459FB6CB9FEB3EB53B6E8C,
	TimeController__ctor_m1028A4580949600A4ED8E49BC0B217B8ADC54142,
	U3CresumeTimeRoutineU3Ed__20__ctor_m65C0FF97BEC41FE13889278D1F6996452AD6CD91,
	U3CresumeTimeRoutineU3Ed__20_System_IDisposable_Dispose_m26A8386A8E0614746DABD0FCE0A86172FB88CF33,
	U3CresumeTimeRoutineU3Ed__20_MoveNext_m1639C3A5EC163F99BF08C45B1DCA04F71EE48009,
	U3CresumeTimeRoutineU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAF56F1DB7AA08D37725CA739AF28CED5D6062E1F,
	U3CresumeTimeRoutineU3Ed__20_System_Collections_IEnumerator_Reset_m48B923886A3DDDDE0932EA96C177A6BD17432EFE,
	U3CresumeTimeRoutineU3Ed__20_System_Collections_IEnumerator_get_Current_mA9E4F21D6FB9DD3A982938F6782208FA8326DDE4,
	ToggleActiveOnInputAction_OnEnable_mD6CD99FFC292031EE47138F0E93C1194733C4F55,
	ToggleActiveOnInputAction_OnDisable_mA35261BEC25B24998F4E8F681A3D3536C7218A69,
	ToggleActiveOnInputAction_ToggleActive_m2FAD5969C167B527450761CD9F5D7857678BC87B,
	ToggleActiveOnInputAction__ctor_mE5749C9B881DCD36B4D875DD0ACE54B9DE3B9E90,
	VREmulator_Start_mAE4D19B47622F4F9ACE3D2F7ACB9B91D9DC10BC9,
	VREmulator_OnBeforeRender_m7DF6991EA153AD6BA32114EF33249984892D0B21,
	VREmulator_onFirstActivate_m264D820F440E488B1AA689D6DB19F3F2D3EC4425,
	VREmulator_Update_mF1744B74AD47239F6915D748EA96B87335C1A7B3,
	VREmulator_HasRequiredFocus_mB5A7E31DE1AF3B9868B54AA39812AD9B871A525D,
	VREmulator_CheckHeadControls_m534CB28C5E377172C2C331ABC0D32B9AA23BBD10,
	VREmulator_UpdateInputs_m09401723DCE17CB83AEBF7784BE859ADA3F0B48D,
	VREmulator_CheckPlayerControls_mA5F8119F4D589EB29024B9A1FBC1F75EF8B3BE70,
	VREmulator_FixedUpdate_m341E851A83D37D6F1228FCC44DBDFA9944B42854,
	VREmulator_UpdateControllerPositions_mE5B410AE71577AD02246C38334AE4EF248EAC02B,
	VREmulator_checkGrabbers_m00AE43E56EB5C9BF13D1CC64D4F53F68BAA99E39,
	VREmulator_ResetHands_m8176D448295C0B1987968E9C36778C69A45AF369,
	VREmulator_ResetAll_m4B0EDF806AC98BB637E40EE421F5AD385B7A1812,
	VREmulator_OnEnable_m13D2F0F79A89A6B3EE7893682F4B8BD1269077F4,
	VREmulator_OnDisable_m6CD6C6E9778EDDE3867158D8E0EC5CB72B00E6AF,
	VREmulator_OnApplicationQuit_m3E37F3A70F683FCD1DD1CEE396323D53B9EAA7B1,
	VREmulator__ctor_m0C518E5807793308EBCD167DC042E36875E9ED0A,
	VehicleController_Start_m029C203F2FB131A1CCBA142E1600046660B843CB,
	VehicleController_Update_m9413D15BC68E308AE8E966D677C8EF529A888AE9,
	VehicleController_CrankEngine_mB5692F3718414BA2C77661F95762F32463C2AB35,
	VehicleController_crankEngine_m863D282917C9F6421BC13EEBCDA9779B1BBF77ED,
	VehicleController_CheckOutOfBounds_mFCEB783E485BDAC806C7FF551920CB6106EB80C3,
	VehicleController_GetTorqueInputFromTriggers_mFC4925DE3A27E5349C2EB9BD9F5A54F733BC380A,
	VehicleController_FixedUpdate_m7BA18855C26FC426977BCBED3FF37780FB77C08F,
	VehicleController_UpdateWheelTorque_mDAD282B68D2CEE69A655DF7824A8ED4EB01A5F65,
	VehicleController_SetSteeringAngle_m9F8F82CF46DD2A32134FFCD19C561476B4E6FD56,
	VehicleController_SetSteeringAngleInverted_m4220208DA9800CFEFF9FDCD8FFBAB7FAAA01AF27,
	VehicleController_SetSteeringAngle_m1DC81C75995163ED9136A6E69B02E3F2E9F1E807,
	VehicleController_SetSteeringAngleInverted_mEFC7938199B5A78196FEB11C96D8A8F2433060BA,
	VehicleController_SetMotorTorqueInput_mBE41CB8019D8BF73643503B300B953EB33626485,
	VehicleController_SetMotorTorqueInputInverted_mA992C59AF072EC6E408AEE593BBCD624CB50BD19,
	VehicleController_SetMotorTorqueInput_m4251871AF3877523025EC97977BF9CF1BB1E3BB8,
	VehicleController_SetMotorTorqueInputInverted_mEA3FC18ADE50ECEDCC9250E4431B4A0A54453BDB,
	VehicleController_UpdateWheelVisuals_mB062B5578A39B69AFB03F2FDDB0A6B3CEAE765CE,
	VehicleController_UpdateEngineAudio_m6881E649462EC9D4F4878813DC512F656E6231D3,
	VehicleController_OnCollisionEnter_m094C2A3C3166BE95F0BE43666DB98E8619ED470C,
	VehicleController_correctValue_mE72A3C04FB169E3EC84C5E87A8D44D311465BB2A,
	VehicleController__ctor_m9C5EE513E0072158FB05E42549BEB3DF07F90655,
	U3CcrankEngineU3Ed__24__ctor_mA54C3A1469980669C05FEFC77BAB39C3D7ED01F0,
	U3CcrankEngineU3Ed__24_System_IDisposable_Dispose_mFBE64A7A2A024A4F9FA419FA23419D36D61C771E,
	U3CcrankEngineU3Ed__24_MoveNext_m4F370B6045BA52D9E435731BC222C0FC8E18B2F9,
	U3CcrankEngineU3Ed__24_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6A80CC9E37CCCADA4FACA1273DA7B40FC55F9BAC,
	U3CcrankEngineU3Ed__24_System_Collections_IEnumerator_Reset_mAF5E0AA65246A693A6071E2E88DE56059742D3B9,
	U3CcrankEngineU3Ed__24_System_Collections_IEnumerator_get_Current_m73B404E26B55FCF851B9174BCCBFB8AA22E14F46,
	WheelObject__ctor_m219A7030ADC46E86FE8A75195B9AE2D3951D24B8,
	Waypoint_OnDrawGizmosSelected_mCF8F7727D5D7019517BC9637BF72CAB00EB96C19,
	Waypoint__ctor_mC4BDB821098252CC99743B52CC626CF775FBACB2,
	Zipline_Start_mD0D47AE92DDABBF8A5477E63A30B231E61705589,
	Zipline_Update_mAB404E24C3C5E6F58E0D08609A81A7A0BF158639,
	Zipline_OnDrawGizmosSelected_mD0A97503A8D42FE0EC36A2E6879AB5EDBA1F26D4,
	Zipline_OnTrigger_m91C3AADC46115752C760EBB944379675AE20F31B,
	Zipline_OnButton1_m72FAB550E0356A65531533D6EC8EE583A1674EC8,
	Zipline_OnButton2_m1EBE05A782D01AF95B44A0CDB84F58FFA58A1092,
	Zipline_moveTowards_m0961825A8807B9801F2A67CE7A148638E43479DB,
	Zipline__ctor_mC58D65142B817A4C963CAEB0181EF78F7D078FA2,
	ControllerOffsetHelper_Start_m784EC7C763C4780B6271F35C85C3B6E4E2326D25,
	ControllerOffsetHelper_checkForController_m97D83C5BFE0F23CA6E10AF5842E0E7370973947A,
	ControllerOffsetHelper_OnControllerFound_m5C3BC19C5B624F9643C2C409318FDB75093C728A,
	ControllerOffsetHelper_GetControllerOffset_m49AE7F40B38476D0DB8EC443217E7A83E3DD40C3,
	ControllerOffsetHelper_DefineControllerOffsets_mF631CD0DB3619FA93439D65D65EAF12E9585D675,
	ControllerOffsetHelper_GetOpenXROffset_mD7125EF48DF532E88112081242D7E34549BDB16F,
	ControllerOffsetHelper__ctor_mBAB737F606A71C8A711DDE6247DC90B51FF62818,
	ControllerOffsetHelper_U3CGetControllerOffsetU3Eb__9_0_m56C8631CC1373F9DC0F7901546F3CA31095F1BF6,
	U3CcheckForControllerU3Ed__7__ctor_m3CCE9882260856E301FA46909325EEF1D7F5BF41,
	U3CcheckForControllerU3Ed__7_System_IDisposable_Dispose_m41BE73D23FBF7747B408E822BACA44B732A9E88D,
	U3CcheckForControllerU3Ed__7_MoveNext_m0C53DAB3426DA90917A51A289C214FE9B02C2EC1,
	U3CcheckForControllerU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2D9C45AA1B8D94A2875E694FC44B0A11E4A57A77,
	U3CcheckForControllerU3Ed__7_System_Collections_IEnumerator_Reset_m3E48252A4C90D45F08C6068DA6CC8CFF1A7CD5BC,
	U3CcheckForControllerU3Ed__7_System_Collections_IEnumerator_get_Current_m456877EEDD6515ECE058343280D3BC37754E5270,
	ControllerOffset_get_ControllerName_mAFB63DED6372D0101D243F4C40321838650822FF,
	ControllerOffset_set_ControllerName_m91AA379DD0B198061E0C4C05C732A0A33FA74684,
	ControllerOffset_get_LeftControllerPositionOffset_mF93F4E6F3B22A1FD1DC0225A524A17491FBCB8D9,
	ControllerOffset_set_LeftControllerPositionOffset_mB22EB34739B09182D54268F58D96F753F5F069DD,
	ControllerOffset_get_RightControllerPositionOffset_mFE97FF8F13EAFC47F4C5D78B0B12E0554F3CFC4C,
	ControllerOffset_set_RightControllerPositionOffset_m2BCB7872DFC9BA42E74E9630AED8DBF73181F060,
	ControllerOffset_get_LeftControllerRotationOffset_mA060B1BBCEFDF0C31F9B2D56797E5AEDD7856DAF,
	ControllerOffset_set_LeftControllerRotationOffset_m7741A534932CF187E41B6A10E261874AEB03B73C,
	ControllerOffset_get_RightControlleRotationOffset_mAC98B24D3AE8398971446111F2BFBC9A684242E0,
	ControllerOffset_set_RightControlleRotationOffset_m9C3C1F4E5FC4A3E1F1D0BD09512FB258C2AE92EF,
	ControllerOffset__ctor_m0B84AB4AD82A894F9939AB773E640A2D8891F756,
	DetachableLimb_DoDismemberment_m16A05DA438472684EDEBF2D46C6702F3A9F5D9E9,
	DetachableLimb_ReverseDismemberment_m3000EF1934BFF88DA405E54CA2D48B03B900A09B,
	DetachableLimb__ctor_mBEF4A41606A88305FDC3BF174347B75119636937,
	DoorHelper_Start_m95108467000AA8B4E3D494F796A9F41984EBD5FB,
	DoorHelper_Update_m95D3F7BB838DAC85C6F2AC709FDF897F9B236605,
	DoorHelper__ctor_m2CCC5F53D176ABBA52222F902672BA402BCE7171,
	GrabberArea_Update_mA4E8125C2BF8BECF9B3ECC932DEF6BD36D085518,
	GrabberArea_GetOpenGrabber_m0AC6B333A39F133EF7D7428F5CCEDF076E6D3CB6,
	GrabberArea_OnTriggerEnter_m7113E53D902BA769736AD0A7F4CC2F7D8E45B7D4,
	GrabberArea_OnTriggerExit_mF2E21A8B3822E7186B79CE71D3BBB5C26296E553,
	GrabberArea__ctor_m88E05D41289F2A97E081FEBAA8846AFFDB665B45,
	HandCollision_Start_mD2124BF6CC2B87ADF3D52D2275B4AD23C2BA8DD7,
	HandCollision_Update_m345506931BA6238B8CA089EB2637088EEB8FBE59,
	HandCollision__ctor_m7AFED6F155ED06DDE28224209F3262FD3BE14C50,
	HandController_get_offsetPosition_m2AC840F59F3F39BDB88C9C0A7AA73223669F72F9,
	HandController_get_offsetRotation_m3C118555EEDD4EA94B3D3D96738ED1C00E3C01D3,
	HandController_Start_m59B94EEF13EDE9FFDAACF0E2D0A5492B62EE1033,
	HandController_Update_mAB83F37CF8C4C707E2FAB83EB67A34054975434D,
	HandController_UpdateHeldObjectState_mBA81BD09837E2577516457D96967E679C387C7DC,
	HandController_UpdateIdleState_mE22C4C38973928ED21369DA01D5726740C8EE2A1,
	HandController_HoldingObject_m2C13BEE93C0D49E70E55A6D184E1959D24F5019A,
	HandController_CheckForGrabChange_mABB278D91A5652825378CAA7EB8DA4FD30E1C2F9,
	HandController_OnGrabChange_m2D26DBA9D7DC93B8498E59515BDA8E0E7DCC8C25,
	HandController_OnGrabDrop_mD2FF1E10A0D861C0F6C87FB517FC5162D4900938,
	HandController_SetHandAnimator_m34E82355A4E0C927DA07E87AC692C30D480E6332,
	HandController_UpdateFromInputs_m41827FBEAE91E8EF680BAFD282DD6EC2E8376002,
	HandController_UpdateAnimimationStates_mFD746541C50DF9EB5645A3A160C8778402E052BB,
	HandController_setAnimatorBlend_m7A057B0515E7F0E94FB6AF84AB6A599E3D13E657,
	HandController_IsAnimatorGrabbable_m2E24880A7C081A720F87ADEA1914E1F0C637C62C,
	HandController_UpdateHandPoser_mC5AFE8D7F41A0CF426B0000A4925B46DB1763E90,
	HandController_IsHandPoserGrabbable_m7DA472A5B1B37E9E9E68BCC1C46FDE9AB1F41BFC,
	HandController_UpdateHandPoserIdleState_m67F9A758AB0CD38FA2AD1D0672832B6DCFF30F34,
	HandController_UpdateIndexFingerBlending_m43BAEFE240B3CCF5EBF0520F918428FDE19CDD02,
	HandController_SetupPoseBlender_m2EC1B0B72E4CBEA7227199414A0EDC8E1F1E4D2A,
	HandController_GetDefaultOpenPose_mF65B0B371C9D163BABBC854D2E0FE03FDAD3F230,
	HandController_GetDefaultClosedPose_m21252D099BE81FAF79AAAC974DBC0229953052BB,
	HandController_EnableHandPoser_m00A624686DDA95BACB7892316E26B39C3E6B06FF,
	HandController_EnableAutoPoser_m4794689C8721FB16AFA1FF72099BA6B1308A3749,
	HandController_DisablePoseBlender_m54F3490B925AC12E0F7C39A1233F538F051D1B4F,
	HandController_DisableAutoPoser_mAAF974038E55AB937A35065C331F4EB22A046DD0,
	HandController_IsAutoPoserGrabbable_mF3B2554F3323996F0B9EA16DB085629B224383FD,
	HandController_EnableHandAnimator_m1D80D5D6990E7DB7890D28CEBF2AE0F709B79B80,
	HandController_DisableHandAnimator_m696926FD81D4364F6D1C56DB163E5C920463F3B2,
	HandController_OnGrabberGrabbed_m61E6E493A22E6E6485FCE1F95C89549828DE530A,
	HandController_UpdateCurrentHandPose_m2DA775E36FEEFC30007391A76211E1B04C3C0EEE,
	HandController_OnGrabberReleased_m3C8D3E9C5608A9D3F671D4620F58D4CE90014F38,
	HandController__ctor_mC4F1014DE6B1AF277F54BAA96453DA676E2BAEB6,
	HandPhysics_get_HoldingObject_mDB1B275317DC6DFDFFDCAEB0609CD0C42DDB7C39,
	HandPhysics_Start_m92F320AB071BD9A20A2ABCAA673DCA2EAFB3115D,
	HandPhysics_Update_mBAE9BC4AE96C24465996842C2F20C8E4F0EE158C,
	HandPhysics_FixedUpdate_m70ADCCC0C977FFB31FA9F66158B83CD9FE4CDE37,
	HandPhysics_initHandColliders_mB4D7BBEC86087DA6511A2369DB21E897970D73BC,
	HandPhysics_checkRemoteCollision_mD7361C59ECD04B0C36A233C0256C830BADED6141,
	HandPhysics_drawDistanceLine_m23F044DC914857FD1099D846BF78B4A9D38C02C7,
	HandPhysics_checkBreakDistance_m6CC454BEF50BB6DA52266A8BFB71AB9BD2227EA8,
	HandPhysics_updateHandGraphics_mEE1CD2728F86343294CF649736C0245CD4EFA346,
	HandPhysics_UnignoreAllCollisions_m0EFF25FF369079E5EA0A002E7F387A1BB3F34997,
	HandPhysics_IgnoreGrabbableCollisions_mD923E9AC08CC51B6A983086772963ABDB5BE0341,
	HandPhysics_DisableHandColliders_m3A5873E2329042EA6BAFD17F2E0C6A60795BC442,
	HandPhysics_EnableHandColliders_mC33FB7EBD8A19046AC5A18394189EBC64B644F33,
	HandPhysics_OnGrabbedObject_mC67D85FAAA671CDFD32E9A1C547F47F9AA0445C3,
	HandPhysics_LockLocalPosition_mF23373F08E07FAE9082C9C16C333CD4E6D6E175D,
	HandPhysics_UnlockLocalPosition_mF9980C48388DBDCE870CF77B7B985F694A12D172,
	HandPhysics_OnReleasedObject_mE1F002815A2C76C70BF4569B53BEB31F71B32446,
	HandPhysics_OnEnable_mA2EC662A570C6DB2A0A5EC3C3E88664133FD47F1,
	HandPhysics_LockOffset_m7A971F1C12598FCFCA8B7D7E01D1558C3A0BC502,
	HandPhysics_UnlockOffset_mC342DEC1514B64C0027F41FA47E8986CEFF055F7,
	HandPhysics_OnDisable_m01568D27A14993B085F4A3F64D42FC86F39127C7,
	HandPhysics_OnCollisionStay_m7BFE91F8E3158380AA97C639D8A4A58801100295,
	HandPhysics_IsValidCollision_mAB6AFE78071E144F45A08C050500D27D9BAF0DEB,
	HandPhysics__ctor_mC31905D5A3AC1866B5936B1D1FF3D67945EB5874,
	U3CUnignoreAllCollisionsU3Ed__31__ctor_m2A10E1CA75FF9308987B6C5C61689174039128DB,
	U3CUnignoreAllCollisionsU3Ed__31_System_IDisposable_Dispose_m3C24DF416C6BFD2FA4A404C0096653630AE7F95A,
	U3CUnignoreAllCollisionsU3Ed__31_MoveNext_m69BEB12669E385880E0B9F1157D9020C06A1EFA2,
	U3CUnignoreAllCollisionsU3Ed__31_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m666CF651E265F21CE8038BE1F77DFEBD07361BB3,
	U3CUnignoreAllCollisionsU3Ed__31_System_Collections_IEnumerator_Reset_m102F18A1E01590EAF327BF4956777283BD1E21AD,
	U3CUnignoreAllCollisionsU3Ed__31_System_Collections_IEnumerator_get_Current_mFA9906E663EA03CBFF3613BB631011D12BE85CD6,
	HandRepresentationHelper_Update_m4B7478FBE0A162FDE71BA8D88D4CA9C04274D069,
	HandRepresentationHelper__ctor_m7387CDD14C92B023FA614ECDB7BC5C2035C98758,
	HandleGFXHelper_Start_mEBA5A2DFF5400199879C68877DF840577E2C4900,
	HandleGFXHelper_Update_m91083572E255174490B02AA3E90C385A57D05284,
	HandleGFXHelper__ctor_m94D39FFD7FE1E26C18AAD1D3B01335725A4728D2,
	HandleHelper_Start_mDC219E507E5DD1A0568C163071750084E55E2D08,
	HandleHelper_FixedUpdate_mFDA4E9B5B344187517303A1FB0F0C78A3D705529,
	HandleHelper_OnCollisionEnter_m21006EA5A0C4EAC12BD537599554B81C21C2FE09,
	HandleHelper_doRelease_m2E98AF5F84F9C9B5A91398B5B6273E456518AAD3,
	HandleHelper__ctor_mBFF1BFA91CF42662D8C271D16211F88D9898A4BC,
	U3CdoReleaseU3Ed__10__ctor_mA65B8960D309F96EBCA523AFE925048DDF62AD60,
	U3CdoReleaseU3Ed__10_System_IDisposable_Dispose_m4FFC1159EAEC4CEC81C0EE34AEF01E9F4D22669E,
	U3CdoReleaseU3Ed__10_MoveNext_mEFF6E684DA16C18BBB49A95F0A4AE2FCE2B613C6,
	U3CdoReleaseU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m39A9D07ECF872B1E1DEB7AD669C20C92778F77B8,
	U3CdoReleaseU3Ed__10_System_Collections_IEnumerator_Reset_m7FFDDB0EBBC4E4513E8C96FDD50899200C712F8D,
	U3CdoReleaseU3Ed__10_System_Collections_IEnumerator_get_Current_mFB033D473EF9FA59C6B52A12ED4C2137690F20AE,
	IgnoreColliders_Start_m6ADA9ABD9ADE4DF555319410C6600D494C37A870,
	IgnoreColliders__ctor_m62214857F9AC233A2DB82AB777B23255239C96ED,
	InvalidTeleportArea__ctor_mA658A29683901676B958A92A7640B55143760152,
	JointBreaker_Start_mE8C810443921B30FAE16133042393B81A13441BB,
	JointBreaker_Update_m3ABC0474C8A3234946256DE5B0C7CA0D158F2CC7,
	JointBreaker_BreakJoint_m1E4D4CB659FF9E044D0AA89A0B40999741B3CBB5,
	JointBreaker__ctor_m7021C024CC4A3B73301713115C73A01DA54B2899,
	JointHelper_Start_m65766EA2418DB0790739F6688CFCBFDBCC73268B,
	JointHelper_lockPosition_mD6996F016043796F6EDFEFA3B7CB57C35E068CAA,
	JointHelper_LateUpdate_m2569F8F2CE0BEDADA1422497B9702B983291F6EF,
	JointHelper_FixedUpdate_mDD02387183B9E311D0093599E6A4F1A41FCB40FD,
	JointHelper__ctor_mF0B4B998F37B412A5D6A8473A64C37EEB91FF95F,
	RagdollHelper_Start_m45ACD86AEE0BA7931419739DB83926FA524127CF,
	RagdollHelper__ctor_m8AF5C2E5C68181E85E843E56A9195A5763F649A6,
	RingHelper_Start_m9B4FA89DC6578D49C4BA2DA3834C088AE20AC464,
	RingHelper_Update_mB1D686781689430C7D3788C197273CD569058EDF,
	RingHelper_AssignCamera_m3530C1A5AFD87038EA5FB30486F1AA472DED98F5,
	RingHelper_AssignGrabbers_mD76B64441D0F1034C5D6CD88B7F7EEAD7423F829,
	RingHelper_getSelectedColor_m5D98ECE615250DAF5CE1CFE0BF9D6EA86E47D91A,
	RingHelper__ctor_m348A3C055F421D59B36BB75C370892075FDD39F2,
	ScaleMaterialHelper_Start_mB583982C1F887B02E183E295A55D7DCFC2ACCDB2,
	ScaleMaterialHelper_updateTexture_m4367C00350E17E41CD23EF3A08B416F12F13B506,
	ScaleMaterialHelper_OnDrawGizmosSelected_mFED377B26C6C3C3EE83D6F8B15CED65CCACD162C,
	ScaleMaterialHelper__ctor_m55557AF8A6E1ED1EA1A9212C5FB48A91D596BC9C,
	StaticBatch_Start_m0EF6D24E64DCFF0EEA6B6FD2490AED1410A17F4C,
	StaticBatch__ctor_m0D16A6689B36BB1520BC2E6A08A849B9BF1ECDC5,
	TeleportDestination__ctor_mEA19F056F0275B56CC66D42FEA6DEB6FA6759677,
	TeleportPlayerOnEnter_OnTriggerEnter_m17DB864460699473831F881C36B8FEDB9B12CAAF,
	TeleportPlayerOnEnter__ctor_m93B56FCA3C3BAD816CBA25531B68C8B936F48287,
	UITrigger__ctor_mC0B136AFAB16B0E094832633F668BB2144DC803C,
	VRIFGrabpointUpdater_Start_m91AA5C7EE6961B26ABA9ED5E5BFEFDBB41AC2D3A,
	VRIFGrabpointUpdater_ApplyGrabPointUpdate_m5CC314D536D8E52247B99E7E6AF8DF117010E5C2,
	VRIFGrabpointUpdater__ctor_m8B01A9B3AA73462F27C7F6D082970154E80209AD,
	UIButtonCollider_Awake_m81FD2F2F31A0D4E65D13B1707879B78A742AB2D3,
	UIButtonCollider_Update_mD930D32BDDBB3F5E23031B49A9A6ED7DD1A076B4,
	UIButtonCollider_OnTriggerEnter_m4F0FAED675F2617E87C511EDC4D52FB7A9188954,
	UIButtonCollider_OnTriggerExit_m80818CE2B98BBEA522ACD2CF2302BF2B1AECB803,
	UIButtonCollider__ctor_m9C5F99FE2840119790170343B1A9E41850B3C575,
	UICanvasGroup_ActivateCanvas_mE4A23C4A8D611A5B3374A69A1C29C449970AFBBF,
	UICanvasGroup__ctor_m6E692768DDA62FBC6C5D8FD625D9DB121C531CFA,
	UIPointer_Awake_m69730CED7472ABF03AE719475280B59E3F2A545D,
	UIPointer_OnEnable_m75186E8A057F0FAD0A7BBFE07302BE3224D7F75A,
	UIPointer_updateUITransforms_mA366E947FAFCAF93E70F3C77A978853560970C5C,
	UIPointer_Update_m0E1E89304AF18D85D1450B6CE0D650C5996F3FA8,
	UIPointer_UpdatePointer_m686699772361C814A637E197979F573477D66B07,
	UIPointer_HidePointer_m4C8909B1181A0F43156BDA44826DFE643D162A88,
	UIPointer__ctor_m022152C2BFEED4E05660C0D6CC693D3160EA6B34,
	VRCanvas_Start_m425CA3D5E2C82A9F9BD1E3E0C2E32CF03E7F145F,
	VRCanvas__ctor_mCC3E4C15A9167965CD1656E978C346FFFF039C06,
	VRKeyboard_Awake_m4FA667BE4963019A398DE69D5D4B51557BE398E2,
	VRKeyboard_PressKey_m7AC3EFB8BE01EA20161BA43D0E3B0235FCE334B8,
	VRKeyboard_UpdateInputField_m88E94DED3C3F62D515280D26F466AA8E909F0004,
	VRKeyboard_PlayClickSound_m7AAB1ADDE7C4BA56B56DE7777226965B164D4AC1,
	VRKeyboard_MoveCaretUp_mC7DC6ADFDCED8B66798430C32692F4D1731C396C,
	VRKeyboard_MoveCaretBack_m007150DB3DA196E5F0A8E86C50A0FE2F4EA1C92F,
	VRKeyboard_ToggleShift_mFF607A7489041670F017A9D71A3C5767E0AFBED7,
	VRKeyboard_IncreaseInputFieldCareteRoutine_m6502D7D720C5F164BD769F8852EB2B8E74C6D75D,
	VRKeyboard_DecreaseInputFieldCareteRoutine_m1AE305D903A46471AE40965482FA7DAA92B6ED44,
	VRKeyboard_AttachToInputField_m807384747F2012A7097CC6B2B2D495EB1252D22D,
	VRKeyboard__ctor_m80D7318B57CFC0DFD1AC8C4A9903DCD0667748CF,
	U3CIncreaseInputFieldCareteRoutineU3Ed__11__ctor_m45F042DB132B6DF8CC147FDDF8B02E03B2E69AD3,
	U3CIncreaseInputFieldCareteRoutineU3Ed__11_System_IDisposable_Dispose_m753F2576F5FFFE17A49DB23E241BC3715C76BAFD,
	U3CIncreaseInputFieldCareteRoutineU3Ed__11_MoveNext_mE93B47EA93BF8239D17F0D1B88A76D13644E2B15,
	U3CIncreaseInputFieldCareteRoutineU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m027B2AE78004B9227EF7B596B088452BA0506F45,
	U3CIncreaseInputFieldCareteRoutineU3Ed__11_System_Collections_IEnumerator_Reset_mCDC66CF75060732FFF41A1C54806F156E45174F0,
	U3CIncreaseInputFieldCareteRoutineU3Ed__11_System_Collections_IEnumerator_get_Current_mF6B3BC8C0401976684AAACA4580A4F40282A1F6C,
	U3CDecreaseInputFieldCareteRoutineU3Ed__12__ctor_m4EC051CEA1251B15D7F5FD2E8CD1E297649734CB,
	U3CDecreaseInputFieldCareteRoutineU3Ed__12_System_IDisposable_Dispose_m61DC59CB10351BAFA8DA3B280DBD64E8344F5A01,
	U3CDecreaseInputFieldCareteRoutineU3Ed__12_MoveNext_mB022D5DC7BBA39076CFC1167C3CE93A841674C5C,
	U3CDecreaseInputFieldCareteRoutineU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0D074637B927F2BCBD5AAA7F13E26EFED969C94E,
	U3CDecreaseInputFieldCareteRoutineU3Ed__12_System_Collections_IEnumerator_Reset_m5FFC85AC31E2F5C5291BEB8C716ECE9FD9323A6D,
	U3CDecreaseInputFieldCareteRoutineU3Ed__12_System_Collections_IEnumerator_get_Current_mE1D1C7AF887E5381AC6476E00F326D9F1A156838,
	VRKeyboardKey_Awake_m28CD209323E3C9D7C068A3A5F5C294676074CFBA,
	VRKeyboardKey_ToggleShift_m41293C588928EAB1F1B97114F8FDE2B537965BEE,
	VRKeyboardKey_OnKeyHit_m29546821E7F2F8D4EC850F46D6F852AF940FFC36,
	VRKeyboardKey_OnKeyHit_m7AE7990A83375AA05360C1CA72595792107E255A,
	VRKeyboardKey__ctor_m5ED75C83D5FDD45C8DA029A303D95EE5430B6D73,
	VRTextInput_Awake_mCE93F9DA1311C52BAF5998F9AD8F1519717F6E95,
	VRTextInput_Update_m9DB15E7E8646FD90C120B06AB063F76CCFDD6033,
	VRTextInput_OnInputSelect_mF6BA8DF36B007EE85DACD8F1C8E63F061C296FF0,
	VRTextInput_OnInputDeselect_mC32A631D9CA35A0526F627AE761B9D4E790D62A6,
	VRTextInput_Reset_mC6E8627D3E684407796A0587425DEE7AC8CFCAD7,
	VRTextInput__ctor_mCA2ACB71D192B82BAA43B1A183EA9DE93F969F82,
	VRUISystem_get_EventData_m5A84EEA6F5D734D4C9539572D3BDE525ED349A0F,
	VRUISystem_set_EventData_m286CCFF96EDF05E2E710837DA5366C52F0DE985E,
	VRUISystem_get_Instance_mD97758F84664BD80358C3F209DAF624AC03AB271,
	VRUISystem_Awake_mE570A47F4660F1A4FAC583D9E15781433EE88213,
	VRUISystem_initEventSystem_mB5328099BC9B8BF1C5318F0A481F7CA3622F53A4,
	VRUISystem_Start_m89AF6C31A81A5B760FAA97E81B7A8F3A5DF02FB0,
	VRUISystem_init_mD83A8F25C7803E1EB3C37F576F8B940FA5C2701F,
	VRUISystem_Process_m263CD7E5F478263B41838A36CF8BC36AF6F49A23,
	VRUISystem_DoProcess_mED1D34586060101246C22A21203C9C18EE2A75D5,
	VRUISystem_InputReady_m602B31B91B2F56E1EE834E177002B96B926F50C9,
	VRUISystem_CameraCasterReady_m78B3FBCEF38FEFAF1057B24AFFE45C62EAFBE197,
	VRUISystem_PressDown_m9B83D446799E2E88EF8E8ACAE13506129A084F0F,
	VRUISystem_Press_m53029C32D12D8B5335D7BC7F4A6C94F2C2E98F01,
	VRUISystem_Release_m06F5FE7BDD007127775C0ABA4F2A8A00452A0B11,
	VRUISystem_ClearAll_mDEFA603EC2E05C64DCE9F19A4C8E75F7B14B525B,
	VRUISystem_SetPressingObject_m2CE936DE4472A598DD2B4F894284852B8F99BDEC,
	VRUISystem_SetDraggingObject_m90926D2DD41FEA267E5BF99238073F67B76AE249,
	VRUISystem_SetReleasingObject_mED8745FF8E87A6BB87BA1170C707221A48A48B0A,
	VRUISystem_AssignCameraToAllCanvases_mA3192DDE80F4D69CCDCFACA440D15C47018800AD,
	VRUISystem_AddCanvas_mFC80E6C080E6D984A86E25C22A93DC3B4B3812A1,
	VRUISystem_AddCanvasToCamera_mCEF6752EA7D6607A53ECCF3BBDB669FDF5ACE426,
	VRUISystem_UpdateControllerHand_m2968AA6B926CBFA66837C0A7D213D380FF716D02,
	VRUISystem__ctor_m960EB3E8F8DE3F80469BA34474231112326A1FE7,
	DestroyIfPlayMode_Start_m4155B2B69AF5CD3FB19905A1780F826CEEB3ABA2,
	DestroyIfPlayMode__ctor_m5672C42AA14759A21472D99990F9CA533542C69E,
	VRUtils_get_Instance_m7AA00DAB70FF60A93B3C8222C3B6DD442133E99D,
	VRUtils_Awake_mA2FA53CBD383EC57BE89A35F3DD3F82846B43695,
	VRUtils_Log_mF963ECBBBB5A6CABAC21BD5E5FC583095288C2D3,
	VRUtils_Warn_mDE28781732E0FACE75E11D01174093A2F6D05A4D,
	VRUtils_Error_m5B26706E7145B386AE953F4A36E585A471CFE2EA,
	VRUtils_VRDebugLog_m23CAC007AC1BBB4BE53104AFDE089863160E4A99,
	VRUtils_CullDebugPanel_m131011AE3BC4F1D3F91EF6A742511F5A6A79092E,
	VRUtils_PlaySpatialClipAt_m140591815557106467AA58422E69E29BF485C110,
	VRUtils_getRandomizedPitch_mD74E3A5095FE6A7B2317D66BB96CE4733631E086,
	VRUtils__ctor_m93F61EBDE0496A70F55A582C16D949DE58691AC7,
	AmmoDispenser_Update_m901FCC52EBA687048145AECA907BEEA442417B1E,
	AmmoDispenser_grabberHasWeapon_m8B0F3515833FEA634371CAE2FBBAAC474957BE3D,
	AmmoDispenser_GetAmmo_m41B36145FBD124FB4393226DFFE3BA0BB16BE0F3,
	AmmoDispenser_GrabAmmo_m1AB82B8376B93734A4F6220FDC3E49A70B41EC4A,
	AmmoDispenser_AddAmmo_mD67F600B1865CF8805112DFA09C9E9E41920F0BF,
	AmmoDispenser__ctor_mBB04FDC8FB56E9F4ABF604164682083C608715EB,
	AmmoDisplay_OnGUI_m71405B97DF64F2743577FE20E53E5E1891C0CA6D,
	AmmoDisplay__ctor_mCAE6A3AE79AE3E6D4E8C7D71819D155E7179E9C6,
	Bullet__ctor_m3EBF0BF131781B03E5D1C1C4FF2638C53AB7C6AB,
	BulletInsert_OnTriggerEnter_m7F38113116276027D0D1CA756CAD858263AEE7F1,
	BulletInsert__ctor_m85443F15FA81372CA6A80EA16F300CDC320F1227,
	MagazineSlide_Awake_m014D8D73686AC5EF57081BDC4677F0D564096E9B,
	MagazineSlide_LateUpdate_m2C13213B534D229D0717DAB4A9352D9C66D7A206,
	MagazineSlide_recentlyEjected_mA5D634FD19F6F55D09BD713A71DBA195E17FE8D7,
	MagazineSlide_moveMagazine_m9A2E09EE24BDAD50829C7D65AC0AF4C5074A9F5E,
	MagazineSlide_CheckGrabClipInput_m4C4DAA1362FC59524C01305DBA825D65D4717DCE,
	MagazineSlide_attachMagazine_m8A8AB649456AA4D12E902E4AF0416215873E6C0D,
	MagazineSlide_detachMagazine_m7D8887A4D85BB507DD32005D64DDB2E96B5FA37E,
	MagazineSlide_EjectMagazine_m2709F5A1A5FC2C22089EF8996ACDD8711332487F,
	MagazineSlide_EjectMagRoutine_m6022B8E32541B3664144306DC60E8996BBD5BB45,
	MagazineSlide_OnGrabClipArea_m42230BC8B39B05F71F9CC41E6887A6A63EA7D25C,
	MagazineSlide_AttachGrabbableMagazine_m7ABCCB4A8A9D0C9967D03998FB1ED0C1703461E2,
	MagazineSlide_OnTriggerEnter_m65D03749757B7A76FD565AD9E1DCF0F64EDA9AB5,
	MagazineSlide__ctor_m10C52E9B2ACE443C457EB00B6620FAE5BD017164,
	U3CEjectMagRoutineU3Ed__23__ctor_mCE7F658BA8248261C8000AF40944BF1971C009B9,
	U3CEjectMagRoutineU3Ed__23_System_IDisposable_Dispose_mD68AF0496811F6B740C49533CAE290664A795E02,
	U3CEjectMagRoutineU3Ed__23_MoveNext_m1343CA23BAA6A6AD1540DF0E902AE4550DA1B122,
	U3CEjectMagRoutineU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3729222694C223FB4C4FC3C4BBE6B4E5A7ADA406,
	U3CEjectMagRoutineU3Ed__23_System_Collections_IEnumerator_Reset_mC9DF130CF70466992D1DB8A6853DC629E70FE8F5,
	U3CEjectMagRoutineU3Ed__23_System_Collections_IEnumerator_get_Current_m30511E60DF6A16C65C27AE3B16CCDDD281552375,
	Projectile_OnCollisionEnter_m4D3CA4D3251E8266A0ABC47FE6313E82A9D9D6C7,
	Projectile_OnCollisionEvent_mC77ED3BD49049F6E1A0222BA85168F9D2581FDD9,
	Projectile_DoHitFX_mA0256AB6391A12E1CDC0E35106BC2B9845BEA3F0,
	Projectile_MarkAsRaycastBullet_m98F6BB130FCA221113C6BF714774C84CEB8FA20D,
	Projectile_DoRayCastProjectile_m069BAAA00C4E49745AC2823A934449999C830214,
	Projectile_CheckForRaycast_m4D77A42F4A68CAB3B3D044A53062D37BE870E83E,
	Projectile__ctor_m4A0B1D74F24D70ACDDA7E7CD7EED4D47237E24B9,
	U3CCheckForRaycastU3Ed__13__ctor_mC8B8B6CBB6E308BF6629949DD45747112B5A720B,
	U3CCheckForRaycastU3Ed__13_System_IDisposable_Dispose_m4B0839B513CBAF8FB2E6E6A10ECF14AB5B9FDDA4,
	U3CCheckForRaycastU3Ed__13_MoveNext_mF927828E0D81015DD8A735BDD7069B30E218CC8D,
	U3CCheckForRaycastU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9757C356C93330958C419B749E72E85965B3CC8C,
	U3CCheckForRaycastU3Ed__13_System_Collections_IEnumerator_Reset_m9F9B273D801025BC8C295F4BDBEDF36D3F86BBEC,
	U3CCheckForRaycastU3Ed__13_System_Collections_IEnumerator_get_Current_m6400D4F08273B0D122BAB28599A7DB4EF949E719,
	RaycastWeapon_Start_m088DFE9F2B526CEB43CF2F83F9194D1A669B8D3D,
	RaycastWeapon_OnTrigger_mF74F921FFAAB156C4B31D00B79FB5EE13CA45468,
	RaycastWeapon_checkSlideInput_m20F1A6BA9768F5E5EE8D0C3F41C9554326B1A3E8,
	RaycastWeapon_checkEjectInput_m26FBAC88544A099E974D4D493FCEA186745D129A,
	RaycastWeapon_CheckReloadInput_m3C3E54083FA0E8318E6ADB93CCBB05582A7F360A,
	RaycastWeapon_UnlockSlide_mD4D9770411F18BB33DEE7E4E31BA4551A192B6D7,
	RaycastWeapon_EjectMagazine_m53393216FF73326DF70C16C9411D95DA011451A2,
	RaycastWeapon_Shoot_mD5A0B076589011DACC03DFE8352D0079A0F14F1C,
	RaycastWeapon_ApplyRecoil_m4DC24E444FE98E06C5DC8A6CF786E70820D4D4DE,
	RaycastWeapon_OnRaycastHit_m7CA11082405451F12F4A37BF650E4121C614346F,
	RaycastWeapon_ApplyParticleFX_m5EF741E5AEB6CCB9C8554283DE6338B55EEB0AE3,
	RaycastWeapon_OnAttachedAmmo_m49BF500072A8DFF66D29AA0A28939D7F88DE9D2B,
	RaycastWeapon_OnDetachedAmmo_m761A55E303947FBB2E6BF46FC485F52DAFDE7746,
	RaycastWeapon_GetBulletCount_m2D711368E575D9A566318FE5E4121BA6B144A379,
	RaycastWeapon_RemoveBullet_m5B553B4C590FAD68289EDE3AF879727AC1E01934,
	RaycastWeapon_Reload_m859D580D1A61C6FE845E378F92FACB567E443CDC,
	RaycastWeapon_updateChamberedBullet_m7B841D186844650E44EE06ADA6619D0B35204128,
	RaycastWeapon_chamberRound_m246285E40A67D03A38A05B8D5E3752CABA8D8798,
	RaycastWeapon_randomizeMuzzleFlashScaleRotation_m983625E82643A8C2A1B66274F8235717E25983F0,
	RaycastWeapon_OnWeaponCharged_m29A7994808890365D515CC660836B6A8671A26CA,
	RaycastWeapon_ejectCasing_m7E64983B2DD5DF3E3BF354CF75078ACEF1659DB3,
	RaycastWeapon_doMuzzleFlash_mA343CADBC9EE1F5827E8CA9BBBCA5CCC5E85729C,
	RaycastWeapon_animateSlideAndEject_m97EF32D4C5C0ED7030A7000D55816D52812C210A,
	RaycastWeapon__ctor_mB3EF17D286241C8EBA55B14374A37D723A67B8F5,
	U3CdoMuzzleFlashU3Ed__74__ctor_m67E6347BEC0A0F4002C32A2E749DCAF584309801,
	U3CdoMuzzleFlashU3Ed__74_System_IDisposable_Dispose_mB16A9F9BD2F6FFEE36C8F4FB93BE273BFCEBF4CD,
	U3CdoMuzzleFlashU3Ed__74_MoveNext_mCEDC6E3116DC6B428BAA75AA73149B8FC6772139,
	U3CdoMuzzleFlashU3Ed__74_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA0762CD160AC327A9DC2A88ABA8B88E53DF24762,
	U3CdoMuzzleFlashU3Ed__74_System_Collections_IEnumerator_Reset_m11DD22F570A901853A7993A4BC7927727A901323,
	U3CdoMuzzleFlashU3Ed__74_System_Collections_IEnumerator_get_Current_mE2188F32B79B4D054792B57F09C0D67195294CEE,
	U3CanimateSlideAndEjectU3Ed__75__ctor_mA8093F159CB825247734E8AC6AD554830B08D42C,
	U3CanimateSlideAndEjectU3Ed__75_System_IDisposable_Dispose_mCEE099200CB96B8FD835DE34A1B4886D972D210B,
	U3CanimateSlideAndEjectU3Ed__75_MoveNext_m526CDF4B343FBCE904EB46D56C8B7B9063EB042C,
	U3CanimateSlideAndEjectU3Ed__75_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0F88EBA6310003E92C1937AC9B6BD47112D0CAAC,
	U3CanimateSlideAndEjectU3Ed__75_System_Collections_IEnumerator_Reset_m368E7B2A30F22F1A327B5670D1AAE3B5BC05044F,
	U3CanimateSlideAndEjectU3Ed__75_System_Collections_IEnumerator_get_Current_mFA299CD3E0642A512C7E9061E3498F1A5F763E45,
	WeaponSlide_Start_mF6BF252F43AC40B39B722BF13380919D566E9F35,
	WeaponSlide_OnEnable_m9ED0E47730F234BA8A4013325A5D7002437F56F1,
	WeaponSlide_OnDisable_m96CED0CF3128AF706B3138709ECDFFC0B98A40E8,
	WeaponSlide_Update_m6BE600BBF2C50177CD5A839880BB453AD1205DD0,
	WeaponSlide_FixedUpdate_m65652DFB47852BED64C8581DDDD0C5406033919E,
	WeaponSlide_LockBack_mAEAB34CD45422460C2536852514D2E7CD0187C88,
	WeaponSlide_UnlockBack_mA64D9EB4A8838F60290465A8CC8B15E4EF75B5DB,
	WeaponSlide_onSlideBack_m0899932CDEFD7373FEE5D30EAD7BF05ED06DEAA9,
	WeaponSlide_onSlideForward_mCD127C11DB2CA2FB3E6F43F2BAA28ED7500C15DD,
	WeaponSlide_LockSlidePosition_mE49A25A669BBBB8B7877EBD9E73E419C8E9A670A,
	WeaponSlide_UnlockSlidePosition_mC1EFAB88298DA02606F04C29D039B3042C4F2E04,
	WeaponSlide_UnlockSlideRoutine_m4BD07AAEB5D32AEB6CC63ED3B95A95DCAD8CF66A,
	WeaponSlide_playSoundInterval_m44C26D3857EB4BB80E55545999A623E24DE832BC,
	WeaponSlide__ctor_m588FE1906E723B9B301A5BAA2DC6B7BE98D74D4D,
	U3CUnlockSlideRoutineU3Ed__27__ctor_mFF7CE62005AD460AABE81DC1535FEA27ECA66239,
	U3CUnlockSlideRoutineU3Ed__27_System_IDisposable_Dispose_m9815711712F3427AF3D1D96F4AB356FCEAAFC99F,
	U3CUnlockSlideRoutineU3Ed__27_MoveNext_mA77DD403B3F851F683818AE6526F1817B60AFBA3,
	U3CUnlockSlideRoutineU3Ed__27_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0FB3490EBBD9949B6FE38D486580A3D01DF13303,
	U3CUnlockSlideRoutineU3Ed__27_System_Collections_IEnumerator_Reset_mC5540BFFE9AE9B7B6E750DB99D5EA556BA01BF51,
	U3CUnlockSlideRoutineU3Ed__27_System_Collections_IEnumerator_get_Current_m7D17E8CE3BFD434D3902C46A78D4683EB93AF186,
};
static const int32_t s_InvokerIndices[2748] = 
{
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	2441,
	364,
	1937,
	199,
	3688,
	1272,
	743,
	749,
	1937,
	4012,
	5151,
	2443,
	5151,
	1939,
	4101,
	2443,
	4101,
	1274,
	4101,
	2443,
	4133,
	1290,
	4101,
	2443,
	4989,
	1939,
	2993,
	5151,
	5151,
	5151,
	4133,
	5151,
	4012,
	5151,
	4012,
	5151,
	4012,
	5151,
	5151,
	4071,
	5151,
	4071,
	5045,
	5151,
	5151,
	1560,
	4101,
	5151,
	2519,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	4989,
	2897,
	5151,
	5151,
	893,
	893,
	5045,
	4071,
	4101,
	4101,
	888,
	5091,
	5077,
	4101,
	530,
	4101,
	5151,
	5151,
	4101,
	2051,
	5151,
	5151,
	5151,
	4101,
	4101,
	5077,
	4101,
	4101,
	4101,
	4101,
	4101,
	4101,
	4101,
	5077,
	4101,
	4101,
	5077,
	4101,
	4101,
	5077,
	4101,
	4101,
	5077,
	4101,
	4101,
	5077,
	4101,
	4101,
	5077,
	4101,
	4101,
	5151,
	5147,
	5091,
	2518,
	5151,
	5151,
	5077,
	4101,
	5151,
	4071,
	5151,
	4989,
	5077,
	5151,
	5077,
	4071,
	5151,
	4989,
	5077,
	5151,
	5077,
	4071,
	5151,
	4989,
	5077,
	5151,
	5077,
	4071,
	5151,
	4989,
	5077,
	5151,
	5077,
	4071,
	5151,
	4989,
	5077,
	5151,
	5077,
	4071,
	5151,
	4989,
	5077,
	5151,
	5077,
	4071,
	5151,
	4989,
	5077,
	5151,
	5077,
	4101,
	4101,
	4101,
	4101,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	4101,
	5151,
	4101,
	5151,
	4101,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	4101,
	4989,
	4012,
	5151,
	5151,
	5151,
	5151,
	4101,
	4101,
	5151,
	5151,
	888,
	5151,
	5151,
	5151,
	5077,
	5077,
	5045,
	4003,
	5151,
	5151,
	4071,
	5151,
	4989,
	5077,
	5151,
	5077,
	4071,
	5151,
	4989,
	5077,
	5151,
	5077,
	5077,
	4101,
	5045,
	4003,
	5151,
	5151,
	5045,
	4003,
	5151,
	5151,
	4101,
	5151,
	5151,
	5077,
	5151,
	5151,
	1963,
	5151,
	4071,
	5151,
	4989,
	5077,
	5151,
	5077,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5077,
	5151,
	5151,
	5151,
	5077,
	4101,
	5151,
	5151,
	4101,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5077,
	5151,
	1697,
	2657,
	5151,
	4071,
	5151,
	4989,
	5077,
	5151,
	5077,
	5151,
	1697,
	2657,
	5151,
	5151,
	1697,
	5151,
	1697,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5077,
	5151,
	4071,
	5151,
	4989,
	5077,
	5151,
	5077,
	5151,
	5151,
	5151,
	5077,
	5151,
	4071,
	5151,
	4989,
	5077,
	5151,
	5077,
	7841,
	7841,
	7819,
	7819,
	7839,
	7840,
	7839,
	7839,
	7839,
	7840,
	7713,
	6614,
	7879,
	7879,
	7879,
	7721,
	7704,
	7716,
	7879,
	4101,
	4101,
	4101,
	5151,
	5045,
	4071,
	4101,
	5151,
	4989,
	4012,
	5046,
	4072,
	5046,
	4072,
	5151,
	2993,
	5151,
	5077,
	2446,
	5151,
	5151,
	4072,
	5151,
	4133,
	5151,
	4012,
	5151,
	4071,
	5151,
	4989,
	5077,
	5151,
	5077,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	4133,
	4072,
	5151,
	4012,
	5151,
	7696,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	1937,
	2441,
	5151,
	5151,
	4101,
	1507,
	4133,
	5151,
	5151,
	5151,
	4101,
	4101,
	4101,
	4101,
	5151,
	5151,
	5151,
	5077,
	5151,
	5151,
	5151,
	5151,
	5077,
	5151,
	4071,
	5151,
	4989,
	5077,
	5151,
	5077,
	5151,
	5151,
	5077,
	5151,
	5151,
	7715,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	2433,
	5151,
	5151,
	1939,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	4989,
	5151,
	4989,
	4012,
	5151,
	7879,
	5151,
	5151,
	4071,
	5151,
	5151,
	5151,
	5151,
	5151,
	4071,
	5151,
	1920,
	1223,
	5151,
	4071,
	5151,
	4989,
	5077,
	5151,
	5077,
	4071,
	5151,
	4989,
	5077,
	5151,
	5077,
	5151,
	5151,
	5151,
	5151,
	4012,
	5151,
	5151,
	5151,
	5151,
	2446,
	4071,
	4071,
	5151,
	1224,
	5151,
	4071,
	5151,
	4989,
	5077,
	5151,
	5077,
	4071,
	5151,
	4168,
	5151,
	2443,
	4071,
	1244,
	4101,
	2443,
	5151,
	1939,
	4101,
	2443,
	4168,
	1304,
	4101,
	5151,
	5151,
	5151,
	436,
	4071,
	1555,
	5151,
	5151,
	5151,
	1462,
	5151,
	5151,
	5151,
	5151,
	1523,
	5151,
	5077,
	2517,
	1523,
	2044,
	2030,
	5151,
	4101,
	5151,
	4071,
	5151,
	4989,
	5077,
	5151,
	5077,
	5151,
	5151,
	5151,
	5151,
	5151,
	4168,
	4168,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	4071,
	5151,
	3699,
	3699,
	4133,
	4133,
	4133,
	4012,
	4168,
	4012,
	5151,
	5151,
	4071,
	5151,
	4989,
	5077,
	5151,
	5077,
	4071,
	5151,
	4989,
	5077,
	5151,
	5077,
	5151,
	5151,
	5151,
	4071,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	4012,
	1560,
	3699,
	5151,
	4071,
	5151,
	4989,
	5077,
	5151,
	5077,
	5151,
	5151,
	5151,
	5151,
	2897,
	5151,
	271,
	5151,
	5077,
	5151,
	5151,
	5151,
	4071,
	5151,
	4989,
	5077,
	5151,
	5077,
	5151,
	5151,
	5151,
	5151,
	5151,
	5147,
	5147,
	4168,
	5147,
	4168,
	5077,
	4101,
	5151,
	5151,
	1549,
	1941,
	108,
	5151,
	5151,
	5151,
	5151,
	6860,
	1010,
	5151,
	271,
	481,
	5151,
	5151,
	5077,
	5151,
	5151,
	5151,
	5151,
	4012,
	5151,
	5151,
	5151,
	4101,
	5151,
	4012,
	5151,
	2433,
	2433,
	4071,
	5151,
	4989,
	5077,
	5151,
	5077,
	5077,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	4012,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	4101,
	5151,
	5151,
	5151,
	2443,
	5151,
	1939,
	4101,
	5151,
	5151,
	4278,
	2526,
	7715,
	5151,
	4101,
	5151,
	4071,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	7721,
	5151,
	5151,
	5151,
	7556,
	5151,
	2484,
	4101,
	4133,
	5151,
	2512,
	4101,
	2057,
	5151,
	4101,
	4012,
	5151,
	5151,
	5151,
	7847,
	7524,
	5151,
	5151,
	5151,
	4989,
	4989,
	4989,
	4989,
	2967,
	2967,
	3817,
	4071,
	2245,
	5151,
	4101,
	4012,
	5151,
	5077,
	4101,
	5151,
	4071,
	5151,
	4989,
	5077,
	5151,
	5077,
	5151,
	5151,
	5151,
	5151,
	5151,
	4989,
	4012,
	5151,
	2446,
	2519,
	5151,
	4016,
	5151,
	7879,
	4989,
	4012,
	4989,
	4012,
	5151,
	5151,
	5151,
	4989,
	4012,
	5151,
	5151,
	5151,
	1550,
	7531,
	1596,
	1596,
	4012,
	4101,
	5151,
	4101,
	4101,
	5151,
	5151,
	4071,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	4101,
	4101,
	5151,
	5151,
	5045,
	5147,
	5045,
	4071,
	5151,
	1456,
	1494,
	76,
	5151,
	1660,
	1660,
	1660,
	5151,
	5077,
	4101,
	5077,
	4101,
	5045,
	5151,
	5151,
	5077,
	5077,
	5045,
	1003,
	5077,
	4101,
	5077,
	4101,
	5077,
	4101,
	5077,
	4101,
	5077,
	4101,
	5077,
	4101,
	5077,
	4101,
	5077,
	4101,
	5077,
	4101,
	5077,
	4101,
	7847,
	7715,
	5151,
	5151,
	5077,
	5151,
	5151,
	6947,
	4989,
	5151,
	4071,
	5151,
	4989,
	5077,
	5151,
	5077,
	5077,
	5077,
	5077,
	5045,
	4101,
	4101,
	4101,
	4101,
	4101,
	4101,
	4101,
	4101,
	4101,
	1494,
	5151,
	5151,
	5151,
	5151,
	464,
	7847,
	7715,
	7715,
	5151,
	7879,
	5151,
	1935,
	2433,
	5151,
	4071,
	5151,
	4989,
	5151,
	5077,
	5151,
	5077,
	7847,
	4101,
	4101,
	5151,
	1651,
	1651,
	2433,
	5151,
	5045,
	5045,
	4989,
	4989,
	4012,
	5151,
	5077,
	5151,
	5151,
	5151,
	5077,
	2446,
	5151,
	5151,
	4071,
	5151,
	4989,
	5077,
	5151,
	5077,
	5077,
	4101,
	4989,
	4012,
	4989,
	4012,
	5110,
	4133,
	5151,
	4101,
	5151,
	1496,
	5077,
	4989,
	4012,
	5045,
	5045,
	4989,
	5147,
	4168,
	5147,
	4168,
	5077,
	5077,
	2446,
	5151,
	4989,
	4012,
	5151,
	4870,
	5151,
	5151,
	5151,
	5151,
	5077,
	4101,
	4989,
	4012,
	4989,
	4012,
	4989,
	4989,
	4989,
	5151,
	2446,
	5045,
	5045,
	4989,
	4989,
	4012,
	5151,
	5151,
	5151,
	5147,
	5077,
	2993,
	5077,
	1951,
	1951,
	2446,
	5151,
	5151,
	4989,
	4012,
	4989,
	4012,
	5151,
	5077,
	4101,
	4101,
	5151,
	5683,
	5151,
	4989,
	4012,
	4989,
	4012,
	5151,
	4101,
	4101,
	5151,
	5151,
	4101,
	4101,
	4101,
	4101,
	4101,
	4101,
	4101,
	4101,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5147,
	5077,
	5151,
	4071,
	5151,
	4989,
	5077,
	5151,
	5077,
	5045,
	4071,
	5151,
	5151,
	4101,
	5151,
	7879,
	5110,
	4133,
	5110,
	4133,
	5045,
	4071,
	5045,
	5077,
	5110,
	3761,
	5110,
	5151,
	2482,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	4101,
	4101,
	4101,
	5151,
	5151,
	5077,
	5151,
	4071,
	5151,
	4989,
	5077,
	5151,
	5077,
	5110,
	5151,
	5151,
	5151,
	5110,
	4133,
	5110,
	4133,
	5151,
	1532,
	5151,
	5151,
	5151,
	5151,
	7879,
	5151,
	5151,
	4101,
	5151,
	5151,
	3699,
	2435,
	4012,
	5151,
	4071,
	5151,
	4989,
	5077,
	5151,
	5077,
	5151,
	5151,
	5151,
	5151,
	3681,
	3758,
	5151,
	5151,
	5151,
	5151,
	5110,
	5151,
	5151,
	5151,
	5151,
	4071,
	5151,
	4989,
	5077,
	5151,
	5077,
	5110,
	4133,
	5151,
	3699,
	5151,
	4133,
	5151,
	5151,
	2517,
	4989,
	4012,
	5151,
	5151,
	2054,
	3699,
	1276,
	2433,
	5151,
	4071,
	5151,
	4989,
	5077,
	5151,
	5077,
	4071,
	5151,
	4989,
	5077,
	5151,
	5077,
	5151,
	5151,
	5151,
	4101,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5077,
	5151,
	5151,
	5151,
	5151,
	1507,
	5151,
	4101,
	4101,
	4101,
	4071,
	5151,
	4989,
	5077,
	5151,
	5077,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	1975,
	5151,
	4012,
	5151,
	7879,
	5151,
	2993,
	4989,
	4989,
	5045,
	5151,
	5151,
	5151,
	5151,
	5151,
	5077,
	4989,
	5151,
	5151,
	5151,
	4012,
	5151,
	5077,
	5077,
	3696,
	3696,
	3696,
	2993,
	2993,
	2993,
	2993,
	2993,
	523,
	2993,
	5151,
	1011,
	5151,
	4071,
	5151,
	4989,
	5077,
	5151,
	5077,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	4133,
	4133,
	4133,
	4133,
	4133,
	4133,
	5151,
	5151,
	5151,
	5077,
	5151,
	5151,
	5151,
	5151,
	5077,
	5077,
	5077,
	5077,
	5077,
	5077,
	5077,
	5045,
	3696,
	5077,
	5077,
	5077,
	5077,
	5077,
	7078,
	3696,
	3696,
	2433,
	2433,
	458,
	476,
	1504,
	1508,
	5077,
	4101,
	4101,
	5077,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	4101,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	4989,
	3761,
	5151,
	1976,
	1621,
	5035,
	5035,
	4989,
	4989,
	5151,
	7879,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	2993,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5147,
	5035,
	5091,
	3823,
	3729,
	5035,
	5035,
	5151,
	7879,
	5151,
	4101,
	4101,
	4101,
	4101,
	5151,
	5151,
	5151,
	5151,
	4133,
	2484,
	5151,
	4101,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	4101,
	4101,
	5151,
	5151,
	4101,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	4101,
	4101,
	5151,
	5151,
	4133,
	273,
	5151,
	3699,
	5151,
	4071,
	5151,
	4989,
	5077,
	5151,
	5077,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	4101,
	5151,
	5151,
	5151,
	4101,
	4101,
	4101,
	4101,
	5151,
	5151,
	5151,
	5151,
	4101,
	4101,
	4101,
	4101,
	4101,
	4101,
	5151,
	1046,
	5151,
	4101,
	5151,
	4071,
	4071,
	4071,
	4101,
	5151,
	4101,
	4071,
	4071,
	4071,
	4071,
	5151,
	5151,
	5151,
	5151,
	4101,
	4071,
	4071,
	4071,
	4071,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	4101,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	2456,
	4133,
	5151,
	2993,
	4101,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	2476,
	3699,
	5151,
	5151,
	4133,
	1944,
	4133,
	5151,
	4071,
	5151,
	4989,
	5077,
	5151,
	5077,
	4071,
	5151,
	4989,
	5077,
	5151,
	5077,
	5151,
	5151,
	4989,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5147,
	5147,
	5147,
	5147,
	3824,
	5151,
	5110,
	5151,
	5151,
	5151,
	5151,
	4989,
	4989,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	4168,
	4989,
	5151,
	5151,
	5151,
	5151,
	5147,
	5147,
	5151,
	5151,
	4101,
	4101,
	5151,
	5151,
	5151,
	5151,
	4071,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	4989,
	5077,
	4989,
	5147,
	4168,
	5110,
	4133,
	5110,
	4133,
	4989,
	5110,
	4133,
	5147,
	5147,
	5147,
	5077,
	5077,
	4989,
	5077,
	5077,
	4989,
	5151,
	5151,
	5151,
	2022,
	3730,
	4101,
	4989,
	5151,
	5151,
	5151,
	5151,
	864,
	5151,
	5151,
	5151,
	2484,
	2484,
	2021,
	1964,
	5151,
	5151,
	5147,
	5091,
	5151,
	5151,
	4101,
	4989,
	5151,
	4012,
	5077,
	4101,
	2441,
	2441,
	2441,
	2441,
	4101,
	5151,
	1483,
	5151,
	4101,
	4101,
	2051,
	5151,
	5151,
	4101,
	5151,
	3448,
	5077,
	5077,
	3696,
	2519,
	2993,
	2993,
	4101,
	4101,
	5151,
	4101,
	4101,
	4168,
	4116,
	3824,
	3730,
	5151,
	5077,
	4133,
	4101,
	4101,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	1421,
	5151,
	4101,
	4101,
	4101,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	4101,
	5151,
	4071,
	4101,
	4071,
	4101,
	4071,
	4101,
	4071,
	4101,
	4133,
	4133,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	4101,
	5151,
	4071,
	4071,
	4071,
	4071,
	4133,
	4133,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	1264,
	1698,
	3696,
	1656,
	3696,
	2446,
	2446,
	4101,
	2446,
	2446,
	4101,
	4101,
	5151,
	4989,
	4989,
	5077,
	5077,
	5147,
	4168,
	5147,
	4168,
	5151,
	5151,
	5151,
	5151,
	4989,
	2993,
	3448,
	3448,
	5077,
	4989,
	3758,
	2967,
	4989,
	4101,
	5151,
	5151,
	5151,
	5151,
	5151,
	5147,
	5147,
	5151,
	5151,
	5151,
	5151,
	2187,
	5151,
	5151,
	5151,
	4101,
	4101,
	5151,
	5151,
	4101,
	4101,
	5077,
	5151,
	4071,
	5151,
	4989,
	5077,
	5151,
	5077,
	5151,
	5151,
	4133,
	5151,
	4133,
	3761,
	5151,
	7847,
	5110,
	5045,
	4071,
	4989,
	4012,
	4989,
	4012,
	4989,
	4012,
	4989,
	4012,
	4989,
	4012,
	7715,
	7715,
	7715,
	7715,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	1271,
	5151,
	5151,
	5151,
	3761,
	2967,
	1629,
	3817,
	1394,
	4063,
	5151,
	4989,
	4989,
	5045,
	4989,
	4989,
	4989,
	4989,
	4989,
	5035,
	5077,
	5147,
	5091,
	5035,
	5035,
	3823,
	3729,
	5045,
	3823,
	3823,
	5077,
	4989,
	1366,
	1621,
	2010,
	2017,
	4071,
	3688,
	1030,
	775,
	5151,
	7879,
	2443,
	5151,
	1939,
	4101,
	2443,
	5151,
	1939,
	4101,
	4071,
	5151,
	4989,
	5077,
	5151,
	5077,
	4071,
	5151,
	4989,
	5077,
	5151,
	5077,
	5151,
	5151,
	5151,
	5151,
	2484,
	4166,
	5151,
	5151,
	5151,
	2484,
	4166,
	5151,
	5151,
	5151,
	5151,
	3761,
	5151,
	5151,
	4133,
	4133,
	5151,
	5151,
	5151,
	5045,
	5151,
	5151,
	5151,
	5151,
	5151,
	4206,
	5151,
	5151,
	2187,
	4071,
	4012,
	4012,
	5151,
	5151,
	4989,
	5151,
	5151,
	2446,
	4101,
	4989,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	4012,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	7715,
	7715,
	7715,
	7715,
	5151,
	5110,
	4133,
	4989,
	4133,
	5151,
	2443,
	5151,
	1939,
	4101,
	2443,
	5151,
	1939,
	4101,
	5077,
	5145,
	4989,
	7715,
	7715,
	7715,
	7715,
	7715,
	7715,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	4989,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	1305,
	2517,
	4101,
	4989,
	4989,
	5151,
	5151,
	2443,
	5151,
	1939,
	4101,
	2443,
	5151,
	1939,
	4101,
	2443,
	5151,
	1939,
	4101,
	4071,
	5151,
	4989,
	5077,
	5151,
	5077,
	4101,
	4101,
	4101,
	4101,
	4101,
	2993,
	5151,
	5151,
	5151,
	4101,
	5151,
	4101,
	4101,
	5151,
	5110,
	5151,
	5151,
	4133,
	5151,
	7715,
	7715,
	7715,
	7715,
	5151,
	5151,
	5151,
	5151,
	5151,
	5145,
	5151,
	4168,
	5151,
	4168,
	4989,
	4989,
	4989,
	5151,
	5151,
	5151,
	5151,
	4101,
	5151,
	2443,
	5151,
	1939,
	4101,
	2443,
	5151,
	1939,
	4101,
	5151,
	5151,
	5077,
	4101,
	4101,
	4101,
	4989,
	5151,
	5151,
	5151,
	5151,
	5110,
	5110,
	5110,
	5110,
	5110,
	5077,
	5077,
	5151,
	5151,
	1994,
	4133,
	5151,
	5151,
	4101,
	5151,
	5077,
	5077,
	4133,
	1375,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	4168,
	5077,
	5077,
	4101,
	4101,
	2484,
	5151,
	4071,
	5151,
	4989,
	5077,
	5151,
	5077,
	4071,
	5151,
	4989,
	5077,
	5151,
	5077,
	5151,
	4101,
	4101,
	5151,
	4101,
	5151,
	5110,
	4133,
	5151,
	5151,
	5077,
	4989,
	5110,
	3758,
	3758,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	4101,
	5151,
	5151,
	5151,
	1535,
	5151,
	5151,
	5151,
	2993,
	5110,
	4133,
	5110,
	4133,
	5110,
	4133,
	5151,
	5151,
	5151,
	5151,
	5151,
	4101,
	2993,
	5151,
	5151,
	5151,
	5151,
	4133,
	5151,
	5110,
	5151,
	5077,
	5151,
	4206,
	4071,
	5151,
	4989,
	5077,
	5151,
	5077,
	5151,
	4168,
	5147,
	5151,
	5151,
	4133,
	5151,
	5151,
	5077,
	1940,
	5151,
	5151,
	4071,
	5151,
	4989,
	5077,
	5151,
	5077,
	4071,
	5151,
	4989,
	5077,
	5151,
	5077,
	7344,
	5151,
	5151,
	5151,
	5151,
	5151,
	4133,
	5151,
	5151,
	5151,
	5151,
	4133,
	5151,
	4101,
	5151,
	5151,
	5151,
	5151,
	5151,
	2517,
	5151,
	4133,
	5151,
	5151,
	4012,
	5151,
	5151,
	4133,
	5151,
	4101,
	4012,
	5151,
	4133,
	5151,
	5151,
	5151,
	5151,
	4101,
	5151,
	5151,
	1939,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	4133,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	4101,
	5151,
	5077,
	1042,
	403,
	5151,
	5151,
	4071,
	5151,
	4989,
	5077,
	5151,
	5077,
	5151,
	5151,
	5151,
	4133,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	3699,
	5151,
	4133,
	5110,
	5151,
	4101,
	5077,
	5151,
	4071,
	5151,
	4989,
	5077,
	5151,
	5077,
	5151,
	4133,
	5151,
	4989,
	5151,
	5151,
	4989,
	5151,
	5151,
	5077,
	5151,
	4071,
	5151,
	4989,
	5077,
	5151,
	5077,
	5151,
	5151,
	4206,
	5151,
	5151,
	5151,
	5151,
	5151,
	4989,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5077,
	5151,
	5151,
	5151,
	5151,
	4133,
	4133,
	4166,
	4166,
	4133,
	4133,
	4166,
	4166,
	4101,
	5151,
	4101,
	3761,
	5151,
	4071,
	5151,
	4989,
	5077,
	5151,
	5077,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	4133,
	5151,
	5151,
	2514,
	5151,
	5151,
	5077,
	5151,
	3696,
	5151,
	5077,
	5151,
	2993,
	4071,
	5151,
	4989,
	5077,
	5151,
	5077,
	5077,
	4101,
	5147,
	4168,
	5147,
	4168,
	5147,
	4168,
	5147,
	4168,
	5151,
	4101,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5077,
	4101,
	4101,
	5151,
	5151,
	5151,
	5151,
	5147,
	5147,
	5151,
	5151,
	5151,
	5151,
	4989,
	5151,
	4101,
	5151,
	5151,
	5151,
	5151,
	1030,
	4989,
	5151,
	4989,
	5151,
	5151,
	4989,
	5077,
	5077,
	5151,
	4012,
	5151,
	5151,
	4989,
	5151,
	5151,
	4101,
	5151,
	4101,
	5151,
	4989,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5077,
	2433,
	5151,
	5151,
	4101,
	5151,
	5151,
	4101,
	5151,
	5151,
	5151,
	5151,
	4101,
	2993,
	5151,
	4071,
	5151,
	4989,
	5077,
	5151,
	5077,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	4101,
	5077,
	5151,
	4071,
	5151,
	4989,
	5077,
	5151,
	5077,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	4992,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	4101,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	4101,
	4101,
	5151,
	4071,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	4101,
	4101,
	5151,
	5151,
	5151,
	5151,
	5077,
	5077,
	4101,
	5151,
	4071,
	5151,
	4989,
	5077,
	5151,
	5077,
	4071,
	5151,
	4989,
	5077,
	5151,
	5077,
	5151,
	5151,
	5151,
	4101,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5077,
	4101,
	7847,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	4989,
	4989,
	5151,
	5151,
	5151,
	5151,
	4101,
	4101,
	4101,
	4101,
	4101,
	2446,
	4071,
	5151,
	5151,
	5151,
	7847,
	5151,
	4101,
	4101,
	4101,
	2435,
	5151,
	375,
	3761,
	5151,
	5151,
	2993,
	5077,
	4101,
	4101,
	5151,
	5151,
	5151,
	5151,
	4101,
	5151,
	5151,
	5151,
	4989,
	4168,
	5151,
	5151,
	5077,
	5151,
	3696,
	4101,
	2446,
	4101,
	5151,
	4071,
	5151,
	4989,
	5077,
	5151,
	5077,
	4101,
	4101,
	1552,
	5151,
	5151,
	5077,
	5151,
	4071,
	5151,
	4989,
	5077,
	5151,
	5077,
	5151,
	4133,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	4120,
	1552,
	5151,
	5151,
	5045,
	5151,
	5151,
	5151,
	5151,
	5151,
	4012,
	5151,
	5077,
	5077,
	5151,
	4071,
	5151,
	4989,
	5077,
	5151,
	5077,
	4071,
	5151,
	4989,
	5077,
	5151,
	5077,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5151,
	5077,
	1535,
	5151,
	4071,
	5151,
	4989,
	5077,
	5151,
	5077,
};
static const Il2CppTokenRangePair s_rgctxIndices[8] = 
{
	{ 0x060001AF, { 0, 5 } },
	{ 0x060001B0, { 5, 5 } },
	{ 0x060001B1, { 10, 1 } },
	{ 0x060001B2, { 11, 1 } },
	{ 0x060001B3, { 12, 1 } },
	{ 0x060001B4, { 13, 1 } },
	{ 0x060001B5, { 14, 1 } },
	{ 0x060001B6, { 15, 1 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[16] = 
{
	{ (Il2CppRGCTXDataType)3, 39245 },
	{ (Il2CppRGCTXDataType)1, 968 },
	{ (Il2CppRGCTXDataType)1, 512 },
	{ (Il2CppRGCTXDataType)2, 512 },
	{ (Il2CppRGCTXDataType)2, 968 },
	{ (Il2CppRGCTXDataType)3, 39695 },
	{ (Il2CppRGCTXDataType)3, 39682 },
	{ (Il2CppRGCTXDataType)3, 39699 },
	{ (Il2CppRGCTXDataType)3, 39697 },
	{ (Il2CppRGCTXDataType)3, 39701 },
	{ (Il2CppRGCTXDataType)3, 39686 },
	{ (Il2CppRGCTXDataType)3, 39685 },
	{ (Il2CppRGCTXDataType)3, 39688 },
	{ (Il2CppRGCTXDataType)3, 39687 },
	{ (Il2CppRGCTXDataType)3, 39689 },
	{ (Il2CppRGCTXDataType)3, 39690 },
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	2748,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	8,
	s_rgctxIndices,
	16,
	s_rgctxValues,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
