﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Boolean UnityEngine.XR.XRSettings::get_isDeviceActive()
extern void XRSettings_get_isDeviceActive_m6E7EEA4B9934BE29CB1B908E382F8575ADB9E003 (void);
// 0x00000002 System.Boolean UnityEngine.XR.XRSettings::get_showDeviceView()
extern void XRSettings_get_showDeviceView_m507C4F64862A1493C1F6806FAF710735970E92DB (void);
// 0x00000003 System.Void UnityEngine.XR.XRSettings::set_showDeviceView(System.Boolean)
extern void XRSettings_set_showDeviceView_m59EBFADF9E46153547D1C31142905A8FE7A22908 (void);
// 0x00000004 System.Int32 UnityEngine.XR.XRSettings::get_eyeTextureWidth()
extern void XRSettings_get_eyeTextureWidth_m58A1989DD2F55FCB2C92A712AEA493B6EEA3C55E (void);
// 0x00000005 System.Int32 UnityEngine.XR.XRSettings::get_eyeTextureHeight()
extern void XRSettings_get_eyeTextureHeight_mD4F91CDDB07756E9BF313DEFD3D74DFC6737E1A2 (void);
// 0x00000006 System.Single UnityEngine.XR.XRSettings::get_renderViewportScale()
extern void XRSettings_get_renderViewportScale_mE9D69964D237C95A40BB6D265EF7701219C13A55 (void);
// 0x00000007 System.Single UnityEngine.XR.XRSettings::get_renderViewportScaleInternal()
extern void XRSettings_get_renderViewportScaleInternal_mFAE96B6D6B0E1E4A517515AF2EB4E2482EB3EE4F (void);
// 0x00000008 System.String UnityEngine.XR.XRSettings::get_loadedDeviceName()
extern void XRSettings_get_loadedDeviceName_mFCE6E2901F4E9D598EB91C5255CA5353A69A34B4 (void);
// 0x00000009 System.Single UnityEngine.XR.XRDevice::get_refreshRate()
extern void XRDevice_get_refreshRate_mACEE096DCAED4CC52B623645229CA2E2991E79E9 (void);
// 0x0000000A System.Void UnityEngine.XR.XRDevice::DisableAutoXRCameraTracking(UnityEngine.Camera,System.Boolean)
extern void XRDevice_DisableAutoXRCameraTracking_mDD0AB20F29D7CF3991BE0A0EBF00F14C4EF77D63 (void);
// 0x0000000B System.Void UnityEngine.XR.XRDevice::InvokeDeviceLoaded(System.String)
extern void XRDevice_InvokeDeviceLoaded_m9B8D07480B85A337BE2B947204E50E41149DC77E (void);
// 0x0000000C System.Void UnityEngine.XR.XRDevice::.cctor()
extern void XRDevice__cctor_mB1E8BACD04DBC82739EBC4FC70714E71B9C7F33B (void);
static Il2CppMethodPointer s_methodPointers[12] = 
{
	XRSettings_get_isDeviceActive_m6E7EEA4B9934BE29CB1B908E382F8575ADB9E003,
	XRSettings_get_showDeviceView_m507C4F64862A1493C1F6806FAF710735970E92DB,
	XRSettings_set_showDeviceView_m59EBFADF9E46153547D1C31142905A8FE7A22908,
	XRSettings_get_eyeTextureWidth_m58A1989DD2F55FCB2C92A712AEA493B6EEA3C55E,
	XRSettings_get_eyeTextureHeight_mD4F91CDDB07756E9BF313DEFD3D74DFC6737E1A2,
	XRSettings_get_renderViewportScale_mE9D69964D237C95A40BB6D265EF7701219C13A55,
	XRSettings_get_renderViewportScaleInternal_mFAE96B6D6B0E1E4A517515AF2EB4E2482EB3EE4F,
	XRSettings_get_loadedDeviceName_mFCE6E2901F4E9D598EB91C5255CA5353A69A34B4,
	XRDevice_get_refreshRate_mACEE096DCAED4CC52B623645229CA2E2991E79E9,
	XRDevice_DisableAutoXRCameraTracking_mDD0AB20F29D7CF3991BE0A0EBF00F14C4EF77D63,
	XRDevice_InvokeDeviceLoaded_m9B8D07480B85A337BE2B947204E50E41149DC77E,
	XRDevice__cctor_mB1E8BACD04DBC82739EBC4FC70714E71B9C7F33B,
};
static const int32_t s_InvokerIndices[12] = 
{
	7819,
	7819,
	7704,
	7839,
	7839,
	7868,
	7868,
	7847,
	7868,
	7150,
	7715,
	7879,
};
extern const CustomAttributesCacheGenerator g_UnityEngine_VRModule_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_UnityEngine_VRModule_CodeGenModule;
const Il2CppCodeGenModule g_UnityEngine_VRModule_CodeGenModule = 
{
	"UnityEngine.VRModule.dll",
	12,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_UnityEngine_VRModule_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
